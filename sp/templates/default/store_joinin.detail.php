<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/custom.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/styles/nyroModal.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">
    $(document).ready(function(){
        $('a[cbtype="nyroModal"]').nyroModal();

        $('#btn_fail').on('click', function() {
            if($('#joinin_message').val() == '') {
                $('#validation_message').text('심사 의견을 입력하세요');
                $('#validation_message').show();
                return false;
            } else {
                $('#validation_message').hide();
            }
            if(confirm('정말 거절하시겠습니까?')) {
                $('#verify_type').val('fail');
                $('#form_store_verify').submit();
            }
        });
        $('#btn_pass').on('click', function() {
            var valid = true;
            $('[cbtype="commis_rate"]').each(function(commis_rate) {
                rate = $(this).val();
                if(rate == '') {
                    valid = false;
                    return false;
                }

                var rate = Number($(this).val());
                if(isNaN(rate) || rate < 0 || rate >= 100) {
                    valid = false;
                    return false;
                }
            });
            if(valid) {
                $('#validation_message').hide();
                if(confirm('정말 통과 신청을 하시겠습니까?')) {
                    $('#verify_type').val('pass');
                    $('#form_store_verify').submit();
                }
            } else {
                $('#validation_message').text('수수료율을 입력하세요');
                $('#validation_message').show();
            }
        });
    });
</script>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['store'];?></h3>
      <ul class="tab-base">
        <li><a href="index.php?act=store&op=store"><span><?php echo $lang['manage'];?></span></a></li>
        <li><a href="index.php?act=store&op=store_joinin" ><span><?php echo $lang['pending'];?></span></a></li>
        <li><a href="index.php?act=store&op=reopen_list" ><span>연장계약</span></a></li>
        <li><a href="index.php?act=store&op=store_bind_class_applay_list" ><span>경영항목신청</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span><?php echo $output['joinin_detail_title'];?></span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
    <thead>
      <tr>
        <th colspan="20">회사 및 연락처정보</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th class="w150">회사이름：</th>
        <td colspan="20"><?php echo $output['joinin_detail']['company_name'];?></td>
      </tr>
      <tr>
        <th>거주지역：</th>
        <td><?php echo $output['joinin_detail']['company_address'];?></td>
        <th>회사상세주소：</th>
        <td colspan="20"><?php echo $output['joinin_detail']['company_address_detail'];?></td>
      </tr>
      <tr>
        <th>회사전화：</th>
        <td><?php echo $output['joinin_detail']['company_phone'];?></td>
        <th>인원수：</th>
        <td><?php echo $output['joinin_detail']['company_employee_count'];?>&nbsp;인</td>
        <th>등록금액：</th>
        <td><?php echo $output['joinin_detail']['company_registered_capital'];?>&nbsp;만원 </td>
      </tr>
      <tr>
        <th>연락처이름：</th>
        <td><?php echo $output['joinin_detail']['contacts_name'];?></td>
        <th>연락처：</th>
        <td><?php echo $output['joinin_detail']['contacts_phone'];?></td>
        <th>이메일：</th>
        <td><?php echo $output['joinin_detail']['contacts_email'];?></td>
      </tr>
    </tbody>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
    <thead>
      <tr>
        <th colspan="20">사업자등록증(복사본)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th class="w150">사업자번호：</th>
        <td><?php echo $output['joinin_detail']['business_licence_number'];?></td></tr><tr>
      </tr>
      <tr>
        <th>경영범위：</th>
        <td colspan="20"><?php echo $output['joinin_detail']['business_sphere'];?></td>
      </tr>
      <tr>
        <th>사업자등록증<br />
전자버전：</th>
        <td colspan="20"><a cbtype="nyroModal"  href="<?php echo getStoreJoininImageUrl($output['joinin_detail']['business_licence_number_electronic']);?>"> <img src="<?php echo getStoreJoininImageUrl($output['joinin_detail']['business_licence_number_electronic']);?>" alt="" /> </a></td>
      </tr>
    </tbody>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
    <thead>
      <tr>
        <th colspan="20">정산계좌정보：</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th class="w150">계좌이름：</th>
        <td><?php echo $output['joinin_detail']['settlement_bank_account_name'];?></td>
      </tr>
      <tr>
        <th>계좌번호：</th>
        <td><?php echo $output['joinin_detail']['settlement_bank_account_number'];?></td>
      </tr>
      <tr>
        <th>은행명：</th>
        <td><?php echo $output['joinin_detail']['settlement_bank_name'];?></td>
      </tr>
    </tbody>
    
  </table>
  <form id="form_store_verify" action="index.php?act=store&op=store_joinin_verify" method="post">
    <input id="verify_type" name="verify_type" type="hidden" />
    <input name="member_id" type="hidden" value="<?php echo $output['joinin_detail']['member_id'];?>" />
	<input type="hidden" name="u_phone" value="<?php echo $output['joinin_detail']['contacts_phone'];?>" />
    <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
      <thead>
        <tr>
          <th colspan="20">업체 운영정보</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="w150">판매자 아이디：</th>
          <td><?php echo $output['joinin_detail']['seller_name'];?></td>
        </tr>
        <tr>
          <th class="w150">업체명：</th>
          <td><?php echo $output['joinin_detail']['store_name'];?></td>
        </tr>
        <tr>
          <th>업체등급：</th>
          <td><?php echo $output['joinin_detail']['sg_name'];?>（개통비용：<?php echo $output['joinin_detail']['sg_price'];?> 원/년）</td>
        </tr>
        <tr>
          <th class="w150">개통기한：</th>
          <td><?php echo $output['joinin_detail']['joinin_year'];?> 년</td>
        </tr>
        <tr>
          <th>업체카테고리：</th>
          <td><?php echo $output['joinin_detail']['sc_name'];?>（오픈보정금：<?php echo $output['joinin_detail']['sc_bail'];?> 원）</td>
        </tr>
        <tr>
          <th>결제총금액：</th>
          <td>
          <?php if(intval($output['joinin_detail']['joinin_state']) === 10) {?>
          <input type="text" value="<?php echo $output['joinin_detail']['paying_amount'];?>" name="paying_amount" /> 원
          <?php } else { ?>
          <?php echo $output['joinin_detail']['paying_amount'];?> 원
          <?php } ?>
          </td>
        </tr>
        <tr>
          <th>경영항목：</th>
          <td colspan="2"><table border="0" cellpadding="0" cellspacing="0" id="table_category" class="type">
              <thead>
                <tr>
                  <th>카테고리1</th>
                  <th>카테고리2</th>
                  <th>카테고리3</th>
                  <th>비율</th>
                </tr>
              </thead>
              <tbody>
                <?php $store_class_names = unserialize($output['joinin_detail']['store_class_names']);?>
                <?php if(!empty($store_class_names) && is_array($store_class_names)) {?>
                <?php $store_class_commis_rates = explode(',', $output['joinin_detail']['store_class_commis_rates']);?>
                <?php for($i=0, $length = count($store_class_names); $i < $length; $i++) {?>
                <?php list($class1, $class2, $class3) = explode(',', $store_class_names[$i]);?>
                <tr>
                  <td><?php echo $class1;?></td>
                  <td><?php echo $class2;?></td>
                  <td><?php echo $class3;?></td>
                  <td>
                <?php if(intval($output['joinin_detail']['joinin_state']) === 10) {?>
                  <input type="text" cbtype="commis_rate" value="<?php echo $store_class_commis_rates[$i];?>" name="commis_rate[]" class="w100" /> %
                <?php } else { ?>
                <?php echo $store_class_commis_rates[$i];?> %
                <?php } ?>
                </td>
                </tr>
                <?php } ?>
                <?php } ?>
                </tbody>
        </table></td>
    </tr>
   <?php if(in_array(intval($output['joinin_detail']['joinin_state']), array(STORE_JOIN_STATE_NEW, STORE_JOIN_STATE_PAY))) { ?>
    <tr>
        <th>심사의견：</th>
        <td colspan="2"><textarea id="joinin_message" name="joinin_message"></textarea></td>
    </tr>
    <?php } ?>
    </tbody>
    </table>
   <?php if(in_array(intval($output['joinin_detail']['joinin_state']), array(STORE_JOIN_STATE_NEW, STORE_JOIN_STATE_PAY))) { ?>
    <div id="validation_message" style="color:red;display:none;"></div>
    <div><a id="btn_fail" class="btn" href="JavaScript:void(0);"><span>거절</span></a> <a id="btn_pass" class="btn" href="JavaScript:void(0);"><span>통과</span></a></div>
    <?php } ?>
  </form>
</div>
