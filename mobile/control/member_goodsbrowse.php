<?php
/**
 * 我的收藏
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class member_goodsbrowseControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 收藏列表
     */
    public function browse_listOp() {
        $model = Model('goods_browse');
        //商品分类缓存
        $gc_list = Model('goods_class')->getGoodsClassForCacheModel();
        //查询浏览记录
        $where = array();
        $where['member_id'] = $this->member_info['member_id'];
        $gc_id = intval($_GET['gc_id']);
        if ($gc_id > 0){
            $where['gc_id_'.$gc_list[$gc_id]['depth']] = $gc_id;
        }
        $browselist_tmp = $model->getGoodsbrowseList($where, '', 20, 0, 'browsetime desc');
        $page_count = $model->gettotalpage();
        $browselist = array();
        foreach ((array)$browselist_tmp as $k=>$v){
            $browselist[$v['goods_id']] = $v;
        }
        //查询商品信息
        $browselist_new = array();
        if ($browselist){
            $goods_list_tmp = Model('goods')->getGoodsList(array('goods_id' => array('in', array_keys($browselist))), 'goods_id, goods_name, goods_promotion_price,goods_promotion_type, goods_marketprice, goods_image, store_id, gc_id, gc_id_1, gc_id_2, gc_id_3');
            $goods_list = array();
            foreach ((array)$goods_list_tmp as $v){
                $goods_list[$v['goods_id']] = $v;
            }
            foreach ($browselist as $k=>$v){
                if ($goods_list[$k]){
                    $tmp = array();
                    $tmp = $goods_list[$k];
                    $tmp["browsetime"] = $v['browsetime'];                    
                    $tmp["goods_image_url"] = cthumb($v['goods_image'], 60);                    
                    if (date('Y-m-d',$v['browsetime']) == date('Y-m-d',time())){
                        $tmp['browsetime_day'] = '今天';
                    } elseif (date('Y-m-d',$v['browsetime']) == date('Y-m-d',(time()-86400))){
                        $tmp['browsetime_day'] = '昨天';
                    } else {
                        $tmp['browsetime_day'] = date('Y年m月d日',$v['browsetime']);
                    }
                    $tmp['browsetime_text'] = $tmp['browsetime_day'].date('H:i',$v['browsetime']);
                    $browselist_new[] = $tmp;
                }
            }
        }

        output_data(array('goodsbrowse_list' => $browselist_new), mobile_page($page_count));
    }

    /**
     * 删除浏览历史
     */
    public function browse_clearallOp(){
        $return_arr = array();
        $model = Model('goods_browse');
        if (trim($_POST['goods_id']) == 'all') {
            if ($model->delGoodsbrowse(array('member_id'=>$this->member_info['member_id']))){
                $return_arr = array('done'=>true);
            } else {
                output_error('删除失败');
            }
        } elseif (intval($_POST['goods_id']) >= 0) {
            $goods_id = intval($_POST['goods_id']);
            if ($model->delGoodsbrowse(array('member_id'=>$this->member_info['member_id'],'goods_id'=>$goods_id))){
                $return_arr = array('done'=>true);
            } else {
            output_error('删除失败');
            }
        } else {
            output_error('参数错误');
        }
        output_data('1');
    }

    /**
     * 添加收藏
     */
    public function favorites_addOp() {
        $goods_id = intval($_POST['goods_id']);
        if ($goods_id <= 0){
            output_error('参数错误');
        }

        $favorites_model = Model('favorites');

        //判断是否已经收藏
        $favorites_info = $favorites_model->getOneFavorites(array('fav_id'=>$goods_id,'fav_type'=>'goods','member_id'=>$this->member_info['member_id']));
        if(!empty($favorites_info)) {
            output_error('您已经收藏了该商品');
        }

        //判断商品是否为当前会员所有
        $goods_model = Model('goods');
        $goods_info = $goods_model->getGoodsInfoByID($goods_id);
        $seller_info = Model('seller')->getSellerInfo(array('member_id'=>$this->member_info['member_id']));
        if ($goods_info['store_id'] == $seller_info['store_id']) {
            output_error('您不能收藏自己发布的商品');
        }

        //添加收藏
        $insert_arr = array();
        $insert_arr['member_id'] = $this->member_info['member_id'];
        $insert_arr['fav_id'] = $goods_id;
        $insert_arr['fav_type'] = 'goods';
        $insert_arr['fav_time'] = TIMESTAMP;
        $result = $favorites_model->addFavorites($insert_arr);

        if ($result){
            //증가收藏数量
            $goods_model->editGoodsById(array('goods_collect' => array('exp', 'goods_collect + 1')), $goods_id);
            output_data('1');
        }else{
            output_error('收藏失败');
        }
    }

    /**
     * 删除收藏
     */
    public function favorites_delOp() {
        $fav_id = intval($_POST['fav_id']);
        if ($fav_id <= 0){
            output_error('参数错误');
        }

        $model_favorites = Model('favorites');

        $condition = array();
        $condition['fav_id'] = $fav_id;
        $condition['member_id'] = $this->member_info['member_id'];
        $model_favorites->delFavorites($condition);
        output_data('1');
    }

}
