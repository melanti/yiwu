
(function($, angular, _, APP){

  APP.modules.ctrl.tableSearchThreeDModalCtrl = tableSearchThreeDModalCtrl;

  /*
   start	목록 시작 위치	정수	선택
   count	목록에 포함할 데이터 갯수	정수	선택
   from	요청 주체 ( 'admin' or NULL ) 검색 방식에 영향을 줌	문자열	선택

   - td_name	3D의상 이름으로 검색 ( 전 구간 유사 검색 )	문자열	선택
   - td_code	3D의상 코드로 검색 ( 앞 구간 유사 검색 )	문자열	선택
   - org_id	제휴처 번호로 검색	문자열	선택
   - org_name	제휴처 이름으로 검색	문자열	선택
   - cat_id	클라이언트의 상품 번호로 검색	문자열	선택
   - td_sex	3D 의상 성별로 검색 ( woman, man )	문자열	선택
   - td_status	3D 의상 상태로 검색 ( ready, activated, disabled, deleted )	문자열	선택
   - make_category_web_id	WEB에서 DB와 연동 할 때만 사용하는 3D 의상 제작 분류 번호 ( 주의: CAT, Mirror, APP에서 사용하는 제작분류번호와 다름 )	문자열	선택
   - make_category_name	제작 분류 이름 ( WomCloTop001 )	문자열	선택
   - period_reg_start	3D 의상 등록 기간으로 검색 시, 시작 일시 (YYYY-MM-DD HH:II:SS)	문자열	선택
   - period_reg_end	3D 의상 등록 기간으로 검색 시, 마감 일시 (YYYY-MM-DD HH:II:SS)	문자열	선택
  */

  //tableSearchThreeDCtrl
  function tableSearchThreeDModalCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO,  OperationTableManager, ThreeDTableManager , COMMON_UTIL){
    var vm = this,
        _dateList = COMMON_UTIL.getCompareDateList(),
      _isLoadShop = false,
      watchTimer,
      watchTimerDateOpt,
      _stDat = COMMON_UTIL.getTodayDateInTimezone(),
      _endDat = COMMON_UTIL.getTodayDateInTimezone();

    vm.parseInt = window.parseInt;
    vm.userData = window.APP.info.user;
    vm.options;

    vm.query_opt='td_name';
    vm.query = '';

    vm.make_category_db_id_arr=[];
    vm.make_category_db2_id_arr=[];
    vm.make_category_db_id_01='';
    vm.make_category_db_id_02='';

    vm.convertedData
    vm.make_category_arr=[];
    vm.make_category_cur_depth=0;
    vm.make_category_max_depth=4;
    vm.curlang=APP.info.user.language.toLowerCase();
    if(vm.curlang==='zh' || vm.curlang==='zh-cn' || vm.curlang==='zh-tw'){
      vm.curlang='zh-cn';
    }

    vm.org_id_arr=[];
    vm.cat_arr=[];

    vm.td_sex_all=true;
    vm.td_sex_woman=true;
    vm.td_sex_man=true;
    vm.td_sex_kids=true;

    vm.order='registerd_date';

    vm.td_name = '';
    vm.td_code = '';
    vm.org_id = '';
    vm.org_name = '';
    vm.cat_id = '';
    vm.td_sex = 'Woman,Man,Kid';
    vm.td_status = 'ready,activated,disabled';
    vm.make_category_db_id = '';
    vm.make_category_name = '';
    vm.period_reg_start='';
    vm.period_reg_end='';


    vm.stDate = _stDat;
    vm.endDate = _endDat;
    vm.stDateOptions = {
      changeYear:false,
      changeMonth:false,
      yearRange:'1900:-0',
      dateFormat:'yy-mm-dd',
      onClose:_onCloseStDateOpt
    };
    vm.endDateOptions = {
      changeYear:false,
      changeMonth:false,
      yearRange:'1900:-0',
      dateFormat:'yy-mm-dd',
      onClose:_onCloseEndDateOpt
    };

    vm.period_unit ='all';  // today, 3days, week, 1month, 3month, 1year, all, custom
    // ( YYYY-MM-DD HH:II:SS )


    vm.getTotal;
    vm.getStart;
    vm.getCount;
    vm.setSearchOpt;
    vm.updatePage;

    vm.changePeriod = _changePeriod;
    vm.updateList = _updateList;
    vm.onChangeQueryOpt = _onChangeQueryOpt;
    vm.onChangeOrgId = _onChangeOrgId;
    vm.onChangeMakeCategoryDbId01 = _onChangeMakeCategoryDbId01;
    vm.onChangeCateNmList = _onChangeCateNmList;

    _init();

    function _init(){
      console.log('search init');
      if(vm.userData.account_type==='admin'){}else{
        _isLoadShop = true;
        //vm.org_id = String(vm.userData.org_id);
        vm.org_id_arr = [{org_id:String(vm.userData.org_id,10), org_name:vm.userData.org_name}];
        //_getCatlist({org_id:vm.userData.org_id}); //이전 category API
      }

      _getCateCsvList()
        .then(
          function(__sucData){
            console.log('_getCateCsvList __sucData: ' ,__sucData);
            vm.convertedData = __sucData.data.results[0].data.concat();
            vm.make_category_max_depth = __sucData.data.results[0].total;

            for(var i=0; i<vm.make_category_max_depth; ++i){
              if(i===0){
                vm.make_category_arr.push( _.extend({cur_code:'', index:(function(idx){return idx;})(i)},{data:(function(idx){return (_.filter(vm.convertedData, function(ele){return (ele.DEPTH===idx && ele.ENABLE==='y');}))})(i)  } ) );
              }else{
                vm.make_category_arr.push( _.extend({cur_code:'', index:(function(idx){return idx;})(i)},{data:[] } ) );
              }

            }


            vm.setSearchOpt({options:_getCurSearchOptData()});
            PubSub.subscribe('ItemsSearchOpt:change',_onChangeTableData);

            // console.log('Man depth0: ' , (_.filter(vm.convertedData, function(ele){return (ele.DEPTH===0);})) );
            // console.log('Man depth1.length: ' , (_.filter(vm.convertedData, function(ele){return (ele.DEPTH===1);}).length) );
            // console.log('Man depth2: ' , (_.filter(vm.convertedData, function(ele){return (ele.DEPTH===2 && ele.DEPTH_0==='Wom' && ele.DEPTH_1==='Clo');})) );


          },
          function(__errData){
            console.log('_getCateCsvList __errData: ' ,__errData);
          }
        );

      //_get3dCateList({parent_cate_id:'', depth:1});

      _registWatch();
      vm.period_unit ='all';
      watchTimer = $timeout(function(){
        _updatePeriodStEndValue( $('#st_date').val(), $('#end_date').val() );
      }, 150);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // watch callback
    function _registWatch(){

      $scope.$watch( 'tableSearchCtrl.period_unit', _watchPeriodUnit);
      $scope.$watch( 'tableSearchCtrl.query_opt', _watchQueryOpt);
    }

    function _watchPeriodUnit(__newVal , __oldVal){
      //console.log('__watchPeriodUnit: ' , __newVal , __oldVal);

      vm.period_reg_start='';
      vm.period_reg_end='';
      COMMON_UTIL.cancelTimeout(watchTimer);

      if(__newVal==='custom'){
        watchTimer = $timeout(function(){
          _updatePeriodStEndValue( $('#st_date').val(), $('#end_date').val() );
          $scope.$apply();
        }, 300);
      }else{
        COMMON_UTIL.convertToDateByPeriodUnit(__newVal , vm.stDateOptions , vm.endDateOptions);
        watchTimer = $timeout(function(){
          $('#end_date').val(vm.endDateOptions.setDate);
          _updatePeriodStEndValue( $('#st_date').val(), vm.endDateOptions.setDate );
          $scope.$apply();
        }, 300);
      }

    }

    function _onChangeCateNmList(__idx){
      // console.log('_onChangeCateNmList: ' ,__idx);
      vm.make_category_cur_depth = __idx;

      for( var i=__idx+1, iTotal=vm.make_category_max_depth; i<iTotal; ++i ){
        vm.make_category_arr[i].cur_code = '';
        if(i===__idx+1){
          //list depth가 변한것의 바로 다음 depth일때
          // console.log('i:: ',i);
          // console.log('i code:: ',vm.make_category_arr[i].cur_code);

          if( vm.make_category_arr[i-1] ){
            if( vm.make_category_arr[i-1].cur_code==='' ){
              vm.make_category_arr[i].data=[];
            }else{
              //vm.convertedData

              vm.make_category_arr[i].data=( (
                function(idx){
                  return _.filter(
                    vm.convertedData,
                    function(ele){
                      return (ele.CODE.indexOf(vm.make_category_arr[__idx].cur_code)===0 && ele.DEPTH===idx && ele.ENABLE=='y');
                    }
                  );
                }
              )(i) ).concat();
            }
          }else{
          }

        }else{
          //list depth가 변한것의 depth일때
          vm.make_category_arr[i].data=[];
        }
      }

      if(vm.make_category_arr.length>0){
        for( var i=vm.make_category_arr.length-1; i>=0; --i ){
          if(i===0){
            vm.make_category_name = vm.make_category_arr[i].cur_code;
          }else{
            if( vm.make_category_arr[i].cur_code !='' ){
              vm.make_category_name = vm.make_category_arr[i].cur_code;
              break;
            }
          }

        }
      }

    }

    function _watchQueryOpt(__newVal , __oldVal){
      //console.log('_watchQueryOpt');
      if(__newVal===__oldVal){
      }else{
        _resetQueryData(__newVal);
        if(vm.query_opt=='org_id'){
          vm.cat_arr=[];
          vm.cat_id = '';
          vm.query='';

          if(vm.userData.account_type==='admin'){
            if(vm.org_id_arr.length<1){
              _getShopList();
            }
          }else{
            vm.org_id = String(vm.userData.org_id);
            vm.org_id_arr = [{org_id:String(vm.userData.org_id,10), org_name:vm.userData.org_name}];
            _getCatlist({org_id:vm.org_id});
          }

        }else{

        }

      }
    }

    function _onChangeTableData(topic, data){
      _resetSearthCate(data);

      vm.td_sex_all=data.options.td_sex_all;
      vm.td_sex_woman=data.options.td_sex_woman;
      vm.td_sex_man=data.options.td_sex_man;
      vm.td_sex_kids=data.options.td_sex_kids;
      vm.td_sex = data.options.td_sex;
      vm.order = data.options.order;

      vm.make_category_db_id = data.options.make_category_db_id;
      vm.make_category_db_id_01 = data.options.make_category_db_id_01;
      vm.make_category_db_id_02 = data.options.make_category_db_id_02;
      vm.make_category_db2_id_arr=data.options.make_category_db2_id_arr;
      vm.org_id_arr = data.options.org_id_arr;
      vm.cat_arr = data.options.cat_arr;
      vm.cat_id = data.options.cat_id;
      vm.period_unit = data.options.period_unit;



      vm.make_category_name = data.options.make_category_name;
      vm.make_category_arr =  _.map(data.options.make_category_arr.concat() , function(val,key,list){
        return {
          cur_code:val.cur_code,
          index:val.index,
          data:val.data.concat()
        };
      });


      vm.make_category_cur_depth = data.options.make_category_cur_depth;

      if(vm.period_unit=='all'){
      }else{
        if(vm.period_reg_start !='') vm.stDate = data.options.stDate;
        if(vm.period_reg_end !='') vm.endDate = data.options.endDate;
      }

      $scope.$apply();
      console.log('data.options.cateData: ' , data.options.cateData);

    }



    ////////////////////////////////////////////////////////////////////////////////
    // external method
    function _changePeriod(__period_unit){
      //console.log('_changePeriod: ' , __period_unit);
      vm.period_unit =__period_unit;
    }

    function _updateList(){
      //console.log('tableSearchThreeDCtrl _updateList');
      //TODO
      //setup된 data를 모아서 'tableComponentThreeDCtrl'(table_component_threeD_ctrl.js)의 vm을 setup한다.
      vm.setSearchOpt({options:_getCurSearchOptData()});
      vm.updatePage({options:{start:0, count:vm.getCount()}});
    }

    function _onChangeQueryOpt(){
      //console.log('_onChangeQueryOpt');

    }

    function _onChangeOrgId(){
      _getCatlist({org_id:vm.org_id});
    }


    function _onChangeMakeCategoryDbId01(){
      vm.make_category_db2_id_arr=[];
      vm.make_category_db_id_02='';

      if(vm.make_category_db_id_01 ==''){
      }else{
        _get3dCateList({parent_cate_id:vm.make_category_db_id_01});
      }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // internal method
    function _updatePeriodStEndValue(__stDate , __endDate){
      //console.log('_updatePeriodStEndValue __stDate: ' , __stDate , __endDate);

      vm.period_reg_start = __stDate;
      vm.period_reg_end = __endDate;
    }

    function _getShopList(){
      //TODO
      //API change
      if(_isLoadShop) return false;
      if(vm.userData.account_type==='admin'){

        OperationTableManager.exports
          .getOrgList()
          .then(
            function(__sucData){
              _isLoadShop = true;
              if(__sucData.results && __sucData.results.length>0) vm.org_id_arr = __sucData.results;

            },
            function(__errData){
              _isLoadShop = true;
              alert('입점업체 정보를 가져오지 못했습니다');  //'입점업체 정보를 가져오지 못했습니다'
              console.log('tableSearchThreeDCtrl _getShopList ERROR: ', __errData);
            }
          );
      }else{
      }
    }

    function _getCatlist(__data){
      OperationTableManager.exports
        .updateTableData({org_id:__data.org_id, service:'cat', start:0, count:1000})
        .then(
          function(__sucData){
            vm.cat_id = '';
            vm.cat_arr = [];
            if(__sucData.results && __sucData.results.length>0) vm.cat_arr = __sucData.results;

          },
          function(__errData){
            vm.cat_id = '';
            vm.cat_arr = [];
            alert( 'CAT 입점업체 정보를 가져오지 못했습니다'); //'CAT 입점업체 정보를 가져오지 못했습니다'
            console.log('tableSearchThreeDCtrl _getCatlist ERROR: ', __errData);
          }
        );
    }



    function _get3dCateList(__options){
      /*sex	all(default), man, woman	String
       parent_cate_id	parent category id (2차 카테고리 호출시 사용)	Integer
       show*/

      var tmpSex = __options.sex || 'all';
      var tmpParent_cate_id = __options.parent_cate_id || '';

      ThreeDTableManager.exports
        .get3dCateList({sex:tmpSex, parent_cate_id:tmpParent_cate_id})
        .then(
          function(__sucData){
            if(__options.depth==1){
              vm.make_category_db_id_arr=__sucData.results;

            }else{
              vm.make_category_db2_id_arr=__sucData.results;
            }
          },
          function(__errData){
            alert('3D 제작 카테고리 정보를 가져오지 못했습니다.');  //'3D 제작 카테고리 정보를 가져오지 못했습니다.'
            console.log('_get3dCateList: ' , __errData);
          }
        );
    }


    function _resetSearthCate(data){
      //상단 검색결과 setup
      vm.query='';
      vm.query_opt=data.options.query_opt;

      $timeout(function(){
        switch(vm.query_opt){
          case 'td_name':
            vm.query=data.options.td_name;
            break;
          case 'td_code':
            vm.query=data.options.td_code;
            break;
          case 'org_id':
            vm.org_id=data.options.org_id;
            _getShopList();
            break;
        }
      }, 200);

    }


    ////////////////////////////////////////////////////////////////////////////////
    // internal method (datepicker method)
    function _onCloseStDateOpt(__d){
      //console.log('stDateOptions: ' , __d);
      COMMON_UTIL.cancelTimeout(watchTimerDateOpt);
      watchTimerDateOpt = $timeout(function(){
        _changeStDateUpdate(__d, $('#end_date').val());
        $scope.$apply();
      }, 300);
    }

    function _onCloseEndDateOpt(__d){
      COMMON_UTIL.cancelTimeout(watchTimerDateOpt);
      watchTimerDateOpt = $timeout(function(){
        _changeStDateUpdate($('#st_date').val(), __d);
        $scope.$apply();
      }, 300);
    }

    function _changeStDateUpdate(__std, __end){
      var chkObjSt = COMMON_UTIL.compareList(_dateList,'dateStr',__std);
      var chkObjEnd = (_dateList[0].dateStr==__end)?true:false;
      if(chkObjSt && chkObjEnd){
        vm.period_unit = chkObjSt.period_unit;
      }else{
        vm.period_unit='custom';
        vm.period_reg_start=$('#st_date').val();
        vm.period_reg_end=$('#end_date').val();

      }
    }

    function _resetQueryData(__queryOpt){
      vm.query='';
      vm.td_name='';
      vm.td_code='';
      vm.org_id='';
      vm.org_name='';
      vm.cat_id='';
      vm.td_sex='';
      vm.order='registerd_date';
      vm.td_status='activated';
      vm.make_category_db_id='';
      vm.make_category_name='';
      vm.make_category_cur_depth = 0;

      _.each(vm.make_category_arr, function(val, key, list){
        val.cur_code = '';
        if( val.index !==0 ) val.data = [];
      });

    }

    function _getCurSearchOptData(){
      if(vm.query_opt=='org_id'){}else{
        vm[vm.query_opt] = (vm.query);
      }

      /*if(vm.td_sex_all){
        vm.td_sex='';
      }else{
        if(vm.td_sex_woman && !vm.td_sex_man){
          vm.td_sex='woman';
        }else if(!vm.td_sex_woman && vm.td_sex_man){
          vm.td_sex='man';
        }else if(vm.td_sex_woman && vm.td_sex_man){
          vm.td_sex='';
        }else if(!vm.td_sex_woman && !vm.td_sex_man){
          vm.td_sex='';
        }
      }*/

      var tmpSexData = [];
      if(vm.td_sex_woman){
        tmpSexData.push('Woman');
      }
      if(vm.td_sex_man){
        tmpSexData.push('Man');
      }
      if(vm.td_sex_kids){
        tmpSexData.push('Kid');
      }

      if(vm.td_sex_woman && vm.td_sex_man && vm.td_sex_kids){
        tmpSexData =[];
      }

      vm.td_sex = encodeURIComponent(tmpSexData.join(','));

      if( vm.make_category_db_id_02 !='' ){
        vm.make_category_db_id = vm.make_category_db_id_02;
      }else{
        vm.make_category_db_id = vm.make_category_db_id_01;
      }

      /*
       vm._state.cateData.convertedData = vm.convertedData;
       vm._state.cateData.make_category_arr = vm.make_category_arr;
       vm._state.cateData.make_category_max_depth = vm.make_category_cur_depth;
      */

      var tmpMakeCateArr = (function(){
        var tmpArr = [];
        _.each(vm.make_category_arr , function(val,key,list){
          tmpArr.push({
            cur_code:val.cur_code,
            index:val.index,
            data:val.data.concat()
          });
        });
        return tmpArr;
      })();

      return {
        make_category_arr:tmpMakeCateArr,
        make_category_cur_depth:vm.make_category_cur_depth,
        from:'admin',
        query_opt:vm.query_opt,
        order:vm.order,
        td_name:vm.td_name,
        td_code:vm.td_code,
        org_id:vm.org_id,
        org_name:vm.org_name,
        cat_id:vm.cat_id,
        td_sex:vm.td_sex,
        td_sex_all:vm.td_sex_all,
        td_sex_woman:vm.td_sex_woman,
        td_sex_man:vm.td_sex_man,
        td_sex_kids:vm.td_sex_kids,
        td_status:vm.td_status,
        org_id_arr:vm.org_id_arr,
        cat_arr:vm.cat_arr,
        stDate:vm.stDate,
        endDate:vm.endDate,
        make_category_db2_id_arr:vm.make_category_db2_id_arr,
        make_category_db_id:vm.make_category_db_id,
        make_category_db_id_01:vm.make_category_db_id_01,
        make_category_db_id_02:vm.make_category_db_id_02,
        make_category_name:vm.make_category_name,
        period_unit:vm.period_unit,
        period_reg_start:(vm.period_unit=='all')?'':vm.period_reg_start,
        period_reg_end:(vm.period_unit=='all')?'':vm.period_reg_end
      };
    }


    function _getCateCsvList(){
      //GET_3D_CATEGORY_LIST


      return XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_MAKE_CATE_URL
        });


    }


  }

  tableSearchThreeDModalCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'OperationTableManager', 'ThreeDTableManager' , 'COMMON_UTIL'];
  tableSearchThreeDModalCtrl.bindToController = {
    getTotal:'&',
    getCount:'&',
    getStart:'&',
    setSearchOpt:'&',
    updatePage:'&'
  };
  tableSearchThreeDModalCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableSearchCtrl:true});
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
