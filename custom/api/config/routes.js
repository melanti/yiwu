/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

var corsObj = {
    origin: '*',
    allRoutes:true,
    methods:'GET,PUT,POST,DELETE,HEAD,OPTIONS',
    headers: 'Content-Type, Authorization, Pragma, Allow, Cache-Control, Connection, Content-Length, Date, Etag, set-cookie, X-Powered-By'
};

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/


  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/
  '*': function(req, res, next) {
    if(process.env.NODE_ENV === "local" || process.env.NODE_ENV === "development") {
      LogService.logRequest(req,res);
    }

    /*if(req.param('account_uuid')) {
      AuthService.checkCode(req,res,next);
    } else {*/
      next();
    /*}*/
  },

  /* Category */
  '/category/list': {
    controller: 'category',
    action: 'list',
    cors: corsObj
  },

  '/category/getCateCSV':{
      controller: 'category',
      action: 'getCateCSV',
      cors: corsObj
  },


  '/category/getMakeCateUrl': {
    controller: 'category',
    action: 'getMakeCateUrl',
    cors: corsObj
  },

  /* Brand */
  '/brand/list': {
    controller: 'brand',
    action: 'list',
    cors: corsObj
  },

  /* VerifyCode */
  'POST /verifyCode/update': {
    controller: 'verifyCode',
    action: 'updateVerifyCode'
  },
  'POST /verifyCode/check': {
    controller: 'verifyCode',
    action: 'checkExistVerifyCode'
  },

  /* Fitting */
  'POST /fitting/defaultVideos': {
    controller: 'fitting',
    action: 'defaultVideos'
  },
  'POST fitting/setVideo': {
    controller: 'fitting',
    action: 'setVideo'
  },
  'fitting/getVideo': {
    controller: 'fitting',
    action: 'getVideo'
  },

  'POST /account/login': {
    controller: 'account',
    action: 'login'
  },

  /* 3d cloth  */
  '/td/list': {
    controller: 'td',
    action: 'list',
    cors: corsObj
  },


  'POST /td/set':{
    controller: 'TdController',
    action: 'set',
    cors: corsObj
  },


  '/td/:td_id': {
    controller: 'td',
    action: 'get'
  },

  'POST /item/attachTd':{
    controller: 'item',
    action: 'attachTd',
    cors: corsObj
  },

  /* Item */
  '/item/list': {
    controller: 'item',
    action: 'list',
    cors: corsObj
  },
  'POST /item/chkAvailability': {
    controller: 'item',
    action: 'chkAvailability'
  },
  '/item/:item_id': {
    controller: 'item',
    action: 'get',
    cors: corsObj
  },

  /* Package */
  '/package/chkInfo' :{
    controller : 'package',
    action : 'checkInfo'
  },
  '/v2/package/chkInfo' :{
    controller : 'package',
    action : 'checkInfoV2'
  },
  '/package/checkBlendVersion' :{
    controller : 'package',
    action : 'checkBlendShapeVersion'
  },
  
  //TEST
  '/test/test_upload': {
    controller: 'test',
    action: 'test_upload'
  },

  '/test/upload/td': {
    controller: 'test',
    action: 'upload_td'
  },


  /* 연습용 컨트롤러 추가  */  
  '/test/study': {
    controller: 'test',
    action: 'study'
  },

  /* 연습용 컨트롤러 추가  */  
  '/test/test': {
    controller: 'test',
    action: 'test'
  },

  /* 배너광고 리스트 */
  '/mobile/adv_list': {
    controller: 'mobile',
    action: 'adv_list'
  },

  '/log/*': {
      controller: 'log',
      action: 'get'
  }

};
