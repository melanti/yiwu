<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 共有文字
 */

/**
 * 列表
 */
$lang['store_consult_reply']	= '咨询/回复';
$lang['store_consult_list_goods']			= '商品';
$lang['store_consult_list_group']			= '团购';
$lang['store_consult_list_consule']			= '咨询';
$lang['store_consult_list_reply_consult']	= '문의답변';
$lang['store_consult_list_reply']			= '답변';
$lang['store_consult_list_edit_reply']		= '답변수정';
$lang['store_consult_list_consult_content']	= '문의내용';
$lang['store_consult_list_consult_member']	= '문의자';
$lang['store_consult_list_consult_time']	= '문의시간';
$lang['store_consult_list_reply_time']  	= '답변시간';
$lang['store_consult_list_my_reply']		= '업체답변';
$lang['store_consult_drop_success']			= '문의 정보가 성공적으로 삭제되었습니다.';
$lang['store_consult_drop_fail']			= '문의 정보 삭제 실패';
$lang['store_consult_reply_input_content']	= '내용을 입력하세요';
$lang['store_consult_reply_input_content_tip']	= '내용은 255자 이내로 입력하세요';
$lang['store_consult_save_info']	= '문의정보';
$lang['store_consult_save_success']	= '성공';
$lang['store_consult_save_fail']	= '실패';
/**
 * 我的咨询
 */
$lang['store_my_consult_seller_reply']	= '업체답변';
