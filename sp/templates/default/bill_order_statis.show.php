<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['bill_manage'];?>정산관리</h3>
		<ul class="tab-base">
		<li><a href="index.php?act=bill"><span>정산관리</span></a></li>
		<li><a class="current" href="JavaScript:void(0);"><span><?php echo !empty($_GET['os_month']) ? $_GET['os_month'].'기' : null;?> 업체명세서리스트</span></a></li>
		</ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="get" action="index.php" target="" name="formSearch" id="formSearch">
    <input type="hidden" name="act" value="bill" />
    <input type="hidden" name="op" value="show_statis" />
    <input type="hidden" name="os_month" value="<?php echo $_GET['os_month'];?>">
    <table class="tb-type1 noborder search">
      <tbody>
        <tr>
          <th>업체ID/이름</th>
          <td><input class="txt-short" type="text" value="<?php echo $_GET['query_store'];?>" name="query_store" id="query_store"/></td>
          <th>계산서상태</th>
          <td>
          <select name="bill_state">
          <option><?php echo L('nc_please_choose');?></option>
          <option <?php if ($_GET['bill_state'] == BILL_STATE_CREATE) {?>selected<?php } ?> value="<?php echo BILL_STATE_CREATE;?>">계산서 발금완료</option>
          <option <?php if ($_GET['bill_state'] == BILL_STATE_STORE_COFIRM) {?>selected<?php } ?> value="<?php echo BILL_STATE_STORE_COFIRM;?>">업체 확인완료</option>
          <option <?php if ($_GET['bill_state'] == BILL_STATE_SYSTEM_CHECK) {?>selected<?php } ?> value="<?php echo BILL_STATE_SYSTEM_CHECK?>">플랫폼 심사완료 </option>
          <option <?php if ($_GET['bill_state'] == BILL_STATE_SUCCESS) {?>selected<?php } ?> value="<?php echo BILL_STATE_SUCCESS?>">정산완료</option>
          </select>
          </td>
          <td><a href="javascript:void(0);" id="ncsubmit" class="btn-search " title="<?php echo $lang['nc_query'];?>">&nbsp;</a>
          </td>
        </tr>
      </tbody>
    </table>
  </form>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li>업체 계산서의 상세내용입니다, 보기를 클릭하여 해당 주문의 상세정보를 확인할 수 있습니다.</li>
            <li>계산서 처리 흐름도: 시스텀 계산서 발금 > 업체 확인 > 플랫폼 심사 > 입금 이상 4개의 계단을 거치게됩니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <div style="text-align:right;"><a class="btns" href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&op=export_bill"><span><?php echo $lang['nc_export'];?>CSV</span></a></div>
  <table class="table tb-type2 nobdb">
    <thead>
      <tr class="thead">
        <th>계산서번호</th>
        <th class="align-center">시작시간</th>
        <th class="align-center">마감날자</th>
        <th class="align-center">주문금액</th>
        <th class="align-center">배송료</th>
        <th class="align-center">수수료금액</th>
        <th class="align-center">환불금액</th>
        <th class="align-center">환불수수료</th>
        <th class="align-center">업체비용</th>
        <th class="align-center">이번결제금액</th>
        <th class="align-center">발급날자</th>
        <th class="align-center">계산서상태</th>
        <th class="align-center">업체</th>
        <th class="align-center"><?php echo $lang['nc_handle'];?></th>
      </tr>
    </thead>
    <tbody>
      <?php if(count($output['bill_list'])>0){?>
      <?php foreach($output['bill_list'] as $bill_info){?>
      <tr class="hover">
        <td><?php echo $bill_info['ob_no'];?></td>
        <td class="nowrap align-center"><?php echo date('Y-m-d',$bill_info['ob_start_date']);?></td>
        <td class="nowrap align-center"><?php echo date('Y-m-d',$bill_info['ob_end_date']);?></td>
        <td class="align-center"><?php echo number_format($bill_info['ob_order_totals_ko']);?>원</td>
        <td class="align-center"><?php echo number_format($bill_info['ob_shipping_totals_ko']);?>원</td>
        <td class="align-center"><?php echo number_format($bill_info['ob_commis_totals_ko']);?>원</td>        
        <td class="align-center"><?php echo number_format($bill_info['ob_order_return_totals_ko']);?>원</td>        
        <td class="align-center"><?php echo number_format($bill_info['ob_commis_return_totals_ko']);?>원</td>       
        <td class="align-center"><?php echo number_format($bill_info['ob_store_cost_totals_ko']);?>원</td>       
        <td class="align-center"><?php echo number_format($bill_info['ob_result_totals_ko']);?>원</td>
        <td class="align-center"><?php echo date('Y-m-d',$bill_info['ob_create_date']);?></td>
        <td class="align-center"><?php echo billStateKo($bill_info['ob_state']);?></td>
        <td class="align-center"><?php echo $bill_info['ob_store_name'].'<br/>id:'.$bill_info['ob_store_id'];?></td>
        <td class="align-center">
        <a href="index.php?act=bill&op=show_bill&ob_no=<?php echo $bill_info['ob_no'];?>"><?php echo $lang['nc_view'];?></a>
        </td>
      </tr>
      <?php }?>
      <?php }else{?>
      <tr class="no_data">
        <td colspan="15"><?php echo $lang['nc_no_record'];?></td>
      </tr>
      <?php }?>
    </tbody>
    <tfoot>
      <tr class="tfoot">
        <td colspan="15" id="dataFuncs"><div class="pagination"> <?php echo $output['show_page'];?> </div></td>
      </tr>
    </tfoot>
  </table>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript">
$(function(){
    $('#bill_month').datepicker({dateFormat:'yy-mm'});
    $('#ncsubmit').click(function(){
    	$('#formSearch').attr('target','_self');
    	$('input[name="op"]').val('show_statis');$('#formSearch').submit();
    });
});
</script> 
