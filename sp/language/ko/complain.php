<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * 导航菜单
 */
$lang['complain_new_list'] = '새신고';
$lang['complain_handle_list'] = '중재중';
$lang['complain_appeal_list'] = '상소중';
$lang['complain_talk_list'] = '상담중';
$lang['complain_finish_list'] = '닫힘';
$lang['complain_subject_list'] = '신고주제';

/**
 * 导航菜单
 */
$lang['complain_manage_title'] = '신고관리';
$lang['complain_submit'] = '신고처리';
$lang['complain_setting'] = '신고설정';

$lang['complain_state_new'] = '새신고';
$lang['complain_state_handle'] = '중재중';
$lang['complain_state_appeal'] = '상소중';
$lang['complain_state_talk'] = '상담중';
$lang['complain_state_finish'] = '닫힘';
$lang['complain_subject_list'] = '신고주제';

$lang['complain_pic'] = '이미지';
$lang['complain_pic_view'] = '이미지 보기';
$lang['complain_pic_none'] = '등록된 이미지가 없습니다.';
$lang['complain_detail'] = '신고상세';
$lang['complain_message'] = '신고정보';
$lang['complain_evidence'] = '신고증거';
$lang['complain_evidence_upload'] = '신고증거 업로드';
$lang['complain_content'] = '신고내용';
$lang['complain_accuser'] = '신고자';
$lang['complain_accused'] = '신고된 미니샵';
$lang['complain_admin'] = '관리자';
$lang['complain_unknow'] = '알수없음';
$lang['complain_datetime'] = '신고시간';
$lang['complain_goods'] = '신고된 상품';
$lang['complain_goods_name'] = '상품이름';
$lang['complain_state'] = '신고상태';
$lang['complain_progress'] = '신고 프로세스';
$lang['complain_handle'] = '신고처리';
$lang['complain_subject_content'] = '신고주제';
$lang['complain_subject_select'] = '신고주제를 선택하세요';
$lang['complain_subject_desc'] = '신고주제 설명';
$lang['complain_subject_add'] = '주제추가';
$lang['complain_appeal_detail'] = '상소상세';
$lang['complain_appeal_message'] = '상소정보';
$lang['complain_appeal_content'] = '상소내용';
$lang['complain_appeal_datetime'] = '상소시간';
$lang['complain_appeal_evidence'] = '상소증거';
$lang['complain_appeal_evidence_upload'] = '상소증거 업로드';
$lang['complain_state_inprogress'] = '진행중';
$lang['complain_state_finish'] = '완성됨';
$lang['final_handle_detail'] = '처리상세';
$lang['final_handle_message'] = '처리결과';
$lang['final_handle_datetime'] = '처리시간';
$lang['order_detail'] = '주문상세';
$lang['order_message'] = '주문정보';
$lang['order_state'] = '주문상태';
$lang['order_sn'] = '주문번호';
$lang['order_datetime'] = '주문시간';
$lang['order_price'] = '주문금액';
$lang['order_discount'] = '할인';
$lang['order_voucher_price'] = '사용한 쿠폰 금액';
$lang['order_voucher_sn'] = '쿠폰번호';
$lang['order_buyer_message'] = '소비자정보';
$lang['order_seller_message'] = '미니샵정보';
$lang['order_shop_name'] = '미니샵이름';
$lang['order_buyer_name'] = '소비자이름';
$lang['order_state_cancel'] = '취소됨';
$lang['order_state_unpay'] = '미지불';
$lang['order_state_payed'] = '결제됨';
$lang['order_state_send'] = '배송됨';
$lang['order_state_receive'] = '도착됨';
$lang['order_state_commit'] = '커밋됨';
$lang['order_state_verify'] = '확인됨';
$lang['complain_time_limit'] = '신고유효시간';
$lang['complain_time_limit_desc'] = '단위는 일, 주문완료후 부터 몇일내로 신고할 수 있습니다.';

$lang['refund_message']	= '환불정보';
$lang['refund_order_refund']	= '환불금액 확인';

/**
 * 提示信息
 */
$lang['confirm_delete'] = '정말 삭제하시겠습니까?';
$lang['complain_content_error'] = '신고내용은 100자 이내로 입력하세요';
$lang['appeal_message_error'] = '신고내용은 100자 이내로 입력하세요';
$lang['complain_pic_error'] = '이미지는 반드시 jpg여야 합니다.';
$lang['complain_time_limit_error'] = '신고 유효시간은 반드시 숫자로 입력하세요';
$lang['complain_subject_content_error'] = '신고주제를 50자 이내로 입력하세요';
$lang['complain_subject_desc_error'] = '신고주제 설명을 100자이내로 입력하세요';
$lang['complain_subject_type_error'] = '신고주제 유형을 선택하세요';
$lang['complain_subject_add_success'] = '신고주제 추가 성공';
$lang['complain_subject_add_fail'] = '신고주제 추가 실패';
$lang['complain_subject_delete_success'] = '신고주제 삭제 성공';
$lang['complain_subject_delete_fail'] = '신고주제 삭제 실패';
$lang['complain_setting_save_success'] = '신고설정 저장 성공';
$lang['complain_setting_save_fail'] = '신고설정 저장 실패';
$lang['complain_goods_select'] = '신고된 상품을 선택하세요';
$lang['complain_submit_success'] = '신고 성공';
$lang['complain_close_confirm'] = '정말 본 신고를 닫으시겠습니까?';
$lang['appeal_submit_success'] = '소송 성공';
$lang['talk_detail'] = '상담상세';
$lang['talk_null'] = '상담을 입력하세요';
$lang['talk_none'] = '상담 내역이 없습니다.';
$lang['talk_list'] = '상담 기록';
$lang['talk_send'] = '상담 발표';
$lang['talk_refresh'] = '상담 새로고침';
$lang['talk_send_success'] = '상담 발송 성공';
$lang['talk_send_fail'] = '상담 발송 실패';
$lang['talk_forbit_success'] = '상담 차단 성공';
$lang['talk_forbit_fail'] = '상담 차단 실패';
$lang['complain_verify_success'] = '신고 심사 성공';
$lang['complain_verify_fail'] = '신고 심사 실패';
$lang['complain_close_success'] = '신고 닫기 성공';
$lang['complain_close_fail'] = '신고 닫기 실패';
$lang['talk_forbit_message'] =  '<본 대화는 관리자에 의해 차단되었습니다>';
$lang['final_handle_message_error'] = '의견 처리는 255자 이내로 입력하세요';
$lang['final_handle_message'] = '의견추리';
$lang['handle_submit'] = '중재발표';
$lang['complain_repeat'] = '본 주문을 이미 신고하였습니다, 처리될때까지 기다려주세요';
$lang['verify_submit_message'] = '본 신고 심사하기';


/**
 * 文本
 */
$lang['complain_text_select'] = '-선택-';
$lang['complain_text_handle'] = '설정';
$lang['complain_text_detail'] = '상세';
$lang['complain_text_submit'] = '발표';
$lang['complain_text_pic'] = '이미지';
$lang['complain_text_num'] = '수량';
$lang['complain_text_price'] = '가격';
$lang['complain_text_problem'] = '질문설명';
$lang['complain_text_say'] = '에게';
$lang['complain_text_verify'] = '심사';
$lang['complain_text_close'] = '신고닫기';
$lang['complain_text_forbit'] = '차단';
$lang['complain_help1']='신고 유효 시간내 소비자는 주문에 신고가 됩니다, 신고는 관리자페이지에서 설정할 수 있습니다.';
$lang['complain_help2']='신고 유효 시간은 시스템 설정에서 설정할 수 있습니다.';
$lang['complain_help3']='상세보기를 눌러 신고에 대한 심사를 할 수 있습니다. 신고 완료후 신고된 미니샵를 소송할 수 있습니다. 최종 심사는 관리자가 진행합니다.';