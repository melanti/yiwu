<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncsc-form-default">
  <dl>
    <dt>발송시간<?php echo $lang['nc_colon']; ?></dt>
    <dd>
      <p><?php echo date('Y-m-d H:i:d', $output['msg_list']['sm_addtime']);?></p>
    </dd>
  </dl>
  <dl>
    <dt>메시지 내용<?php echo $lang['nc_colon'];?></dt>
    <dd>
      <p><?php echo $output['msg_list']['sm_content']; ?></p>
    </dd>
  </dl>
  <div class="bottom pt10 pb10"><a class="ncsc-btn" href="javascript:void(0);" onclick="CUR_DIALOG.close();">창 닫기</a></div>
</div>
