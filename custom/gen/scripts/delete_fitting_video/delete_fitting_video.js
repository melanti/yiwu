var Q = require('q');
var s3Handler = require('./s3Handler.js');
var fileService = require('./FileService.js');

module.exports = {
    deleteFittingVideo: deleteFittingVideo
};

function deleteFittingVideo(conn) {
    var start_second, end_second;
    start_second = new Date().getTime() / 1000;

    console.log('=========================================Start!!!=======================================');
    var cntPerProcess = 10;
    var data = {};
    Q.fcall(function() {
        return getExpiredListCnt(conn);
    })
    .then(function (resultCnt) {
        if(!resultCnt) {
            resultCnt = 0;
        }
        return deleteFittingVideoLoop(conn, resultCnt, cntPerProcess);
    })
    .catch(function (err) {
        console.log('error ::::: ' + JSON.stringify(err, null, 4));
        process.exit();
    })
    .done(function () {
        console.log('\n\n=========================================Done!!!=======================================');
        end_second = new Date().getTime() / 1000;
        console.log('::::::: SUMMARY ::::::');
        console.log("TOTAL : " + (end_second - start_second));
        process.exit();
    });
}

function getExpiredListCnt(conn) {
    var deferred = Q.defer();
    var qry = "select count(*) as cnt " +
              "  from sv_fitv " +
              " where fitv_status = 'activated' " +
              "   and date(date_add(regist_date, interval 1 month)) < date(now()) ";

    conn.query(qry, function(err, result) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(result[0].cnt);
        }
    });

    return deferred.promise;
}

function deleteFittingVideoLoop(conn, resultCnt, cntPerProcess) {
    var deferred = Q.defer();
    var total = Math.floor(resultCnt / cntPerProcess);
    var cntArr = [];
    if((resultCnt % cntPerProcess) > 0) {
        total += 1;
    }

    for (var i = 0; i < total; i++) {
        cntArr.push(i+1);
    }


    var promises = cntArr.reduce(function(promise, cnt) {
        return promise.then(function() {

            return doDeleteFittingVideo(conn, total, cnt, cntPerProcess, resultCnt);
        });
    }, Q.resolve());

    promises
    .then(function(cnt) {
        if(!cnt) {
            cnt = 0;
        }
        console.log('\n\n[[' + cnt + ' / ' + total + ' , ' + resultCnt + ']]--------------------------[Process done]------------------------------');
        deferred.resolve();
    })
    .catch(function(err) {
        console.log('[[Fail]]--------------------------[Process fail]------------------------------');
        console.log(err);
        deferred.reject(err);
    });

    return deferred.promise;
}

function doDeleteFittingVideo(conn, total, cnt, cntPerProcess, resultCnt) {
    var deferred = Q.defer();

    var start_second, end_second,
        start_second1, end_second1,
        start_second2, end_second2,
        start_second3, end_second3;

    var data = {
        videos: []
    };

    start_second = new Date().getTime() / 1000;
    start_second1 = new Date().getTime() / 1000;

    console.log('\n\n[[' + cnt + ' / ' + total + ' , ' + resultCnt + ']]--------------------------[Start S3 delete]------------------------------');

    Q.fcall(function () {
        return getExpiredList(conn, data, cntPerProcess);
    })
    .then(function () {

        end_second2 = new Date().getTime() / 1000;
        start_second3 = new Date().getTime() / 1000;

        /*return s3DeleteObjects(data);*/
        return sftpDeleteObjects(data);
    })
    .then(function () {

        end_second1 = new Date().getTime() / 1000;
        start_second2 = new Date().getTime() / 1000;

        return updateFitv(conn, data);
    })
    .then(function () {
        end_second3 = new Date().getTime() / 1000;
        end_second = new Date().getTime() / 1000;

        /* 5. done */
        console.log('::::::: SUMMARY ::::::');
        console.log("total : " + (end_second - start_second));
        console.log("1.getExpiredList : " + (end_second1 - start_second1));
        console.log("2.updateFitv : " + (end_second2 - start_second2));
        console.log("3.s3DeleteObjects : " + (end_second3 - start_second3));

    })
    .catch(function (err) {
        deferred.reject(err);
    })
    .done(function () {
        deferred.resolve(cnt);
    });

    return deferred.promise;
}

function getExpiredList(conn, data, cntPerProcess) {
    var deferred = Q.defer();
    var qry = "select fitv_code as fitv_cd, fitv_sex as fitv_sex " +
              "  from sv_fitv " +
              " where fitv_status = 'activated' " +
              "   and date(date_add(regist_date, interval 1 month)) < date(now()) " +
              " limit 0, ?";

    conn.query(qry, [cntPerProcess], function (err, results) {
        if (err) {
            deferred.reject(err);
        } else {
            data.videos = results;
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function updateFitv(conn, data) {
    var deferred = Q.defer();
    var videos = data.videos;
    var params = [];
    var qry = "update sv_fitv set fitv_status='deleted' where fitv_code in (";

    for (var i = 0, length = videos.length; i < length; i++) {
        if(i === length -1) {
            qry += "?)";
        } else {
            qry += "?,";
        }
        params.push(videos[i].fitv_cd);
    }

    //console.log(JSON.stringify(videos, null, 4));

    conn.query(qry, params, function (err, results) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function s3DeleteObjects(data) {
    var deferred = Q.defer();

    var videos = data.videos;
    var S3 = new s3Handler('data');

    var promises = videos.reduce(function(promise, video) {
        return promise.then(function () {
            var target = "USER/" + video.fitv_sex + "/" + video.fitv_cd;
            return doS3DeleteObjects(S3, target, video);
        });
    }, Q.resolve());

    promises
    .then(function() {
        console.log('\ns3 delete done...');
        deferred.resolve();
    })
    .catch(function(err) {
        console.log('\ns3 delete fail...' + err);
        deferred.resolve();
    });

    return deferred.promise;
}

function doS3DeleteObjects(S3, target, video) {
    var deferred = Q.defer();
    S3.deleteFolder(target, function (err, result) {
        console.log('\n[s3 delete detail]');
        console.log('1.sex :: ' + video.fitv_sex);
        console.log('2.code :: ' + video.fitv_cd);
        console.log('3.target :: ' + target);
        if (err) {
            console.log('4.error :: ' + JSON.parse(err, null, 4));
            deferred.reject(err);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function sftpDeleteObjects(data) {
    return Q.all([ fileService.deleteFittingFolders( data ) ]);
}