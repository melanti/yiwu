<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 兑换주문功能公用
 */
$lang['admin_pointorder_unavailable']	 		= '포인트 및 포인트 교환 기능이 시스템에서 꺼져있는 상태입니다.';
$lang['admin_pointorder_parameter_error']		= '변수 오류';
$lang['admin_pointorderd_record_error']			= '잘못된 정보기록';
$lang['admin_pointorder_userrecord_error']		= '잘못된 유저 정보';
$lang['admin_pointorder_goodsrecord_error']		= '잘못된 선물 정보';
$lang['admin_pointprod_list_title']				= '선물리스트';
$lang['admin_pointprod_add_title']				= '선물추가';
$lang['admin_pointorder_list_title']			= '교환리스트';
$lang['admin_pointorder_ordersn']				= '교환번호';
$lang['admin_pointorder_membername']			= '회원명칭';
$lang['admin_pointorder_payment']				= '결제방식';
$lang['admin_pointorder_orderstate']			= '상태';
$lang['admin_pointorder_exchangepoints']		= '포인트교환';
$lang['admin_pointorder_shippingfee']			= '운비';
$lang['admin_pointorder_addtime']				= '교환시간';
$lang['admin_pointorder_shipping_code']			= '믈류번호';
$lang['admin_pointorder_shipping_time']			= '발송시간';
$lang['admin_pointorder_gobacklist']			= '목록으로 가기';
/**
 * 兑换信息상태
 */
$lang['admin_pointorder_state_submit']			= '주문완료';
$lang['admin_pointorder_state_waitpay']			= '결제대기';
$lang['admin_pointorder_state_canceled']		= '취소됨';
//$lang['admin_pointorder_state_waitfinish']	= '待完成';
$lang['admin_pointorder_state_paid']			= '결제완료';
$lang['admin_pointorder_state_confirmpay']		= '확인대기';
$lang['admin_pointorder_state_confirmpaid']		= '입금확인';
$lang['admin_pointorder_state_waitship']		= '배송대기';
$lang['admin_pointorder_state_shipped']			= '배송완료';
$lang['admin_pointorder_state_waitreceiving']	= '수취대기';
$lang['admin_pointorder_state_finished']		= '완료';
$lang['admin_pointorder_state_unknown']			= '알수없음';
/**
 * 兑换信息리스트
 */
$lang['admin_pointorder_changefee']					= '운비조정';
$lang['admin_pointorder_changefee_success']			= '운비조정 성공';
$lang['admin_pointorder_changefee_freightpricenull']= '운비를 추가하세요';
$lang['admin_pointorder_changefee_freightprice_error']= '운비가격은 숫자여야 하며 0내지 그이상이여야 합니다.';
$lang['admin_pointorder_cancel_tip1']				= '선물교환정보 취소';
$lang['admin_pointorder_cancel_tip2']				= '포인트증가';
$lang['admin_pointorder_cancel_title']				= '환전을 취소 하시겠습니까?';
$lang['admin_pointorder_cancel_confirmtip']			= '정보교환을 취소 하시겠습니까?';
$lang['admin_pointorder_cancel_success']			= '성공취소';
$lang['admin_pointorder_cancel_fail']				= '실패취소';
$lang['admin_pointorder_confirmpaid']				= '수취확인';
$lang['admin_pointorder_confirmpaid_confirmtip']	= '자금교환정보를 받은것에 확인하십니까?';
$lang['admin_pointorder_confirmpaid_success']		= '성공확인';
$lang['admin_pointorder_confirmpaid_fail']			= '식패확인';
$lang['admin_pointorder_ship_title']				= '발송';
$lang['admin_pointorder_ship_modtip']				= '물류수정하기';
$lang['admin_pointorder_ship_code_nullerror']		= '물류번호를 추가 하십시오.';
$lang['admin_pointorder_ship_success']				= '정보설정 성공';
$lang['admin_pointorder_ship_fail']					= '정보설정 실패';
$lang['admin_pointorder_shipping_timetip']			= '참고: 기본값은 현재 시간입니다.';
$lang['admin_pointorder_shipping_description']		= '설명';
/**
 * 兑换信息삭제
 */
$lang['admin_pointorder_del_success']		= '삭제성공';
$lang['admin_pointorder_del_fail']			= '삭제실패';
/**
 * 兑换信息详细
 */
$lang['admin_pointorder_info_title']			= '교환정보상세';
$lang['admin_pointorder_info_ordersimple']		= '교환정보';
$lang['admin_pointorder_info_orderdetail']		= '교환상세';
$lang['admin_pointorder_info_memberinfo']		= '회원정보';
$lang['admin_pointorder_info_memberemail']		= '회원Email';
$lang['admin_pointorder_info_ordermessage']		= '메모';
$lang['admin_pointorder_info_paymentinfo']		= '결제정보';
$lang['admin_pointorder_info_paymenttime']		= '결제시간';
$lang['admin_pointorder_info_paymentmessage']	= '결제메모';
$lang['admin_pointorder_info_shipinfo']			= '수취인 및 배송정보';
$lang['admin_pointorder_info_shipinfo_truename']= '배송인';
$lang['admin_pointorder_info_shipinfo_areainfo']= '거주지';
$lang['admin_pointorder_info_shipinfo_zipcode']= '우편번호';
$lang['admin_pointorder_info_shipinfo_telphone']= '전화번호';
$lang['admin_pointorder_info_shipinfo_mobphone']= '휴대폰번호';
$lang['admin_pointorder_info_shipinfo_address']= '상세주소';
$lang['admin_pointorder_info_shipinfo_description']	= '배송설명';
$lang['admin_pointorder_info_prodinfo']				= '사은품정보';
$lang['admin_pointorder_info_prodinfo_exnum']		= '교환수량';

$lang['pay_bank_user']			= '입금자명';
$lang['pay_bank_bank']			= '입금은행';
$lang['pay_bank_account']		= '입금계좌';
$lang['pay_bank_num']			= '입금금액';
$lang['pay_bank_date']			= '입금날자';
$lang['pay_bank_extend']		= '기타';
$lang['pay_bank_order']			= '입금번호';
$lang['pay_bank_bank_tips']		= '(상세한 은행명을 기재해주세요)';