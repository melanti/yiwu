<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['notice_index_member_list_null']	= '회원목록을 비워두실 수 없습니다.';
$lang['notice_index_store_grade_null']	= '미니샵등급은 비워두실 수 없습니다.';
$lang['notice_index_title_null']		= '공고 리스트는 비워두실 수 없습니다.';
$lang['notice_index_content_null']		= '공고 내용은 비워두실 수 없습니다.';
$lang['notice_index_batch_int']			= '개별 발송 수량은 숫자여야만 합니다.';
$lang['notice_index_member_error']		= '회원정보가 잘못됐을시 다시 작업하시기 바랍니다.';
$lang['notice_index_sending']			= '발송중';
$lang['notice_index_send_succ']			= '발송성공';
$lang['notice_index_member_notice']		= '회원통지';
$lang['notice_index_send']				= '발송통지';
$lang['notice_index_send_type']			= '발송유형';
$lang['notice_index_spec_member']		= '지정회원';
$lang['notice_index_all_member']		= '전체회원';
$lang['notice_index_smtp_incomplate']	= 'SMTP정보설정 불완전함';
$lang['notice_index_smtp_close']		= '메일기능 설정되어 있지 않음';
$lang['notice_index_spec_store_grade']	= '미니샵 클래스 지정';
$lang['notice_index_all_store']			= '전체미니샵';
$lang['notice_index_member_list']		= '회원목록';
$lang['notice_index_member_tip']		= '각 행에 사용자 이름 기입';
$lang['notice_index_store_grade']		= '미니샵 클래스';
$lang['notice_index_store_tip']			= 'Ctrl버튼을 누른상태에서 여러개 옵션을 선택가능';
$lang['notice_index_batch']				= '일괄 발송 수량';
$lang['notice_index_batch_tip']			= '일괄발송통지수 수량이 많을시 시간 초과로 인해 실행 종료 될수 있습니다.따라서 초과하지 않는 것이 좋습니다.';
$lang['notice_index_send_method']		= '발송방식';
$lang['notice_index_message']			= '편지보내기';
$lang['notice_index_email']				= '메일보내기';
$lang['notice_index_title']				= '제목알림';
$lang['notice_index_content']			= '내용알림';
$lang['notice_index_member_error']		= '지정회원발송 회원명은 비워두실 수 없으며 한줄에 아이디 하나 씩 입니다';
$lang['notice_index_help1']				= '개별 발송,일차전송알림작업이 자동으로 일괄 처리로 진행될 시 개별발송 통지수를 설정하실 수 있습니다.';