<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['promotion_unavailable'] = '상품 프로모션 기능이 미시동 상태입니다.';

$lang['state_new'] = '새로신청';
$lang['state_verify'] = '심사하였습니다.';
$lang['state_cancel'] = '취소하였습니다.';
$lang['state_verify_fail'] = '심사 실패';
$lang['mansong_quota_state_activity'] = '정상';
$lang['mansong_quota_state_cancel'] = '취소';
$lang['mansong_quota_state_expire'] = '실패';
$lang['mansong_state_unpublished'] = '미선포';
$lang['mansong_state_published'] = '선포';
$lang['mansong_state_cancel'] = '취소하였습니다.';
$lang['all_state'] = '전체';
$lang['mansong_price'] = '사은품 프로모션 가격';
$lang['mansong_price_explain'] = '구매 단위는 월(30일)이며, 구매 후 판매자는 구매 주 내에 사은품 프로모션 프로모션를 등록 할 수 있습니다.';
$lang['mansong_price_error'] = '비워두실 수 없으며 최소 0보다 큰 정수를 기입하셔야만 합니다.';

$lang['store_name'] = '미니샵명';
$lang['mansong_name'] = '프로모션명';
$lang['mansong_detail'] = '프로모션 디테일';
$lang['start_time'] = '시작시간';
$lang['end_time'] = '종료시간';



$lang['promotion_mansong'] = '사은품 프로모션';
$lang['mansong_list'] = '프로모션';
$lang['mansong_apply'] = '패키지 상품 신청 관리';
$lang['mansong_quota'] = '패키지 리스트';
$lang['mansong_setting'] = '설정';
$lang['apply_quantity'] = '신청수량';
$lang['apply_date'] = '신청일자';
$lang['mansong_quota_start_time'] = '시작시간';
$lang['mansong_quota_end_time'] = '종료시간';
$lang['level_price'] = '단일주문이 차면';
$lang['level_discount'] = ' 현금지급 ';
$lang['gift_name'] = '선물지급';


$lang['mansong_apply_cancel_success'] = '성공취소 신청';
$lang['mansong_apply_cancel_fail'] = '실패 취소 신청';
$lang['mansong_apply_verify_success'] = '사은품 프로모션 신청 심사 성공,프로모션 패키지 발송 완료';
$lang['mansong_quota_cancel_success'] = '사은품 프로모션 패키지 취소 성공';
$lang['mansong_quota_cancel_fail'] = '사은품 프로모션 패키지 취소 실패';

$lang['confirm_verify'] = '심사하시겠습니까?';
$lang['confirm_cancel'] = '취소하시겠습니까?';
$lang['setting_save_success'] = '설정저장 성공';
$lang['setting_save_fail'] = '설정저장 실패';
$lang['text_normal'] = '정상';
$lang['text_level'] = '등급';
$lang['text_level_condition'] = '일정한 지출금액 이상';
$lang['text_reduce'] = '절약';
$lang['text_yuan'] = '원';
$lang['text_cash'] = '현금';
$lang['text_give'] = '사은품';
$lang['text_gift'] = '선물';
$lang['text_link'] = '링크';
$lang['text_new_level'] = '등급추가';
$lang['text_remark'] = '메세지';
$lang['text_not_join'] = '미참여';

$lang['mansong_apply_list_help1'] = '판매자가 사은품 프로모션패키지를 신청하면 심사후 시스템이 자동으로 편지를 통해 판매자에게 알립니다.';
$lang['mansong_quota_list_help1'] = '판매자의 금액 프로모션 패키지 목록';
$lang['mansong_quota_list_help2'] = '"취소" 하시게되면 복구 할 수 없습니다. 신중히 선택해 주세요.';
$lang['mansong_list_help1'] = '판매자가 발표한 사은품 프로모션목록';
$lang['mansong_list_help2'] = '"취소" 하시게되면 복구 할 수 없습니다. 신중히 선택해 주세요.';
$lang['mansong_list_help3'] = '상세링크를 클릭후 프로모션 세부정보를 확인하세요';

