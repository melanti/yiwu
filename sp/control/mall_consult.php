<?php
/**
 * 平台客观문의관리
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');

class mall_consultControl extends SystemControl{
    public function __construct(){
        parent::__construct();
    }

    /**
     * 문의관리
     */
    public function indexOp(){
        $condition = array();
        if(chksubmit()){
            $member_name = trim($_GET['member_name']);
            if($member_name != ''){
                $condition['member_name'] = array('like', '%' . $member_name . '%');
                Tpl::output('member_name', $member_name);
            }
            $mct_id = intval($_GET['mctid']);
            if ($mct_id > 0) {
                $condition['mct_id'] = $mct_id;
                Tpl::output('mctid', $mct_id);
            }
        }
        $model_mallconsult = Model('mall_consult');
        $consult_list = $model_mallconsult->getMallConsultList($condition,'*', 10);
        Tpl::output('show_page',$model_mallconsult->showpage());
        Tpl::output('consult_list',$consult_list);



        // 문의유형리스트
        $type_list = Model('mall_consult_type')->getMallConsultTypeList(array(), 'mct_id,mct_name', 'mct_id');
        Tpl::output('type_list', $type_list);

        // 답변상태
        $state = array('0'=>'미답변', '1'=>'답변완료');
        Tpl::output('state', $state);

        Tpl::showpage('mall_consult.index');
    }

    /**
     * 답변문의
     */
    public function consult_replyOp() {
        $model_mallconsult = Model('mall_consult');
        if (chksubmit()) {
            $mc_id = intval($_POST['mc_id']);
            $reply_content = trim($_POST['reply_content']);
            if ($mc_id <= 0 || $reply_content == '') {
                showMessage(L('param_error'));
            }
            $update['is_reply'] = 1;
            $update['mc_reply'] = $reply_content;
            $update['mc_reply_time'] = TIMESTAMP;
            $update['admin_id'] = $this->admin_info['id'];
            $update['admin_name'] = $this->admin_info['name'];
            $result = $model_mallconsult->editMallConsult(array('mc_id' => $mc_id), $update);
            if ($result) {
                $consult_info = $model_mallconsult->getMallConsultInfo(array('mc_id' => $mc_id));
                // 发送회원消息
                $param = array();
                $param['code'] = 'consult_mall_reply';
                $param['member_id'] = $consult_info['member_id'];
                $param['param'] = array(
                    'consult_url' => urlShop('member_mallconsult', 'mallconsult_info', array('id' => $mc_id))
                );
                QueueClient::push('sendMemberMsg', $param);

                showMessage('답변성공', urlAdmin('mall_consult', 'index'));
            } else {
                showMessage('답변실패');
            }
        }
        $id = intval($_GET['id']);
        if ($id <= 0) {
            showMessage(L('param_error'));
        }

        $consult_info = $model_mallconsult->getMallConsultDetail($id);
        Tpl::output('consult_info', $consult_info);
        Tpl::showpage('mall_consult.reply');
    }

    /**
     * 삭제고객센터문의
     */
    public function del_consultOp(){
        $id = $_GET['id'];
        if($id <= 0){
            showMessage(Language::get('nc_common_del_fail'));
        }
        $result = Model('mall_consult')->delMallConsult(array('mc_id' => $id));
        if($result){
            $this->log('삭제고객센터문의'.'[ID:'.$id.']',null);
            showMessage(Language::get('nc_common_del_succ'));
        }else{
            showMessage(Language::get('nc_common_del_fail'));
        }
    }

    /**
     * 批量삭제고객센터문의
     */
    public function del_consult_batchOp(){
        $ids = $_POST['id'];
        if(empty($ids)){
            showMessage(Language::get('nc_common_del_fail'));
        }
        $result = Model('mall_consult')->delMallConsult(array('mc_id' => array('in', $ids)));
        if($result){
            $this->log('삭제고객센터문의'.'[ID:'.implode(',', $ids).']',null);
            showMessage(Language::get('nc_common_del_succ'));
        }else{
            showMessage(Language::get('nc_common_del_fail'));
        }
    }

    /**
     * 문의유형리스트
     */
    public function type_listOp() {
        $model_mct = Model('mall_consult_type');
        if (chksubmit()) {
            $mctid_array = $_POST['del_id'];
            if (!is_array($mctid_array)) {
                showMessage(L('param_error'));
            }
            foreach ($mctid_array as $val){
                if (!is_numeric($val)) {
                    showMessage(L('param_error'));
                }
            }

            $result = $model_mct->delMallConsultType(array('mct_id' => array('in', $mctid_array)));

            if ($result) {
                $this->log('삭제고객센터문의유형 ID:'.implode(',', $mctid_array), 1);
                showMessage(L('nc_common_del_succ'));
            } else {
                $this->log('삭제고객센터문의유형 ID:'.implode(',', $mctid_array), 0);
                showMessage(L('nc_common_del_fail'));
            }
        }
        $type_list = $model_mct->getMallConsultTypeList(array(), 'mct_id,mct_name,mct_sort');
        Tpl::output('type_list', $type_list);
        Tpl::showpage('mall_consult.type_list');
    }

    /**
     * 추가문의유형
     */
    public function type_addOp() {
        if (chksubmit()) {
            // 验证
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["mct_name"], "require"=>"true", "message"=>'请填写문의유형명'),
                array("input"=>$_POST["mct_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'请正确填写문의유형정렬'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage(Language::get('error').$error,'','','error');
            }
            $insert = array();
            $insert['mct_name'] = trim($_POST['mct_name']);
            $insert['mct_introduce'] = $_POST['mct_introduce'];
            $insert['mct_sort'] = intval($_POST['mct_sort']);
            $result = Model('mall_consult_type')->addMallConsultType($insert);
            if ($result){
                $this->log('추가문의유형',1);
                showMessage(L('nc_common_save_succ'), urlAdmin('mall_consult', 'type_list'));
            }else {
                $this->log('추가문의유형',0);
                showMessage(L('nc_common_save_fail'));
            }
        }
        Tpl::showpage('mall_consult.type_add');
    }

    /**
     * 수정문의유형
     */
    public function type_editOp() {
        $model_mct = Model('mall_consult_type');
        if (chksubmit()) {
            // 验证
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                    array("input"=>$_POST["mct_name"], "require"=>"true", "message"=>'请填写문의유형명'),
                    array("input"=>$_POST["mct_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'请正确填写문의유형정렬'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage(Language::get('error').$error,'','','error');
            }
            $where = array();
            $where['mct_id'] = intval($_POST['mct_id']);
            $update = array();
            $update['mct_name'] = trim($_POST['mct_name']);
            $update['mct_introduce'] = $_POST['mct_introduce'];
            $update['mct_sort'] = intval($_POST['mct_sort']);
            $result = $model_mct->editMallConsultType($where, $update);
            if ($result){
                $this->log('수정고객센터문의유형 ID:'.$where['mct_id'],1);
                showMessage(L('nc_common_op_succ'), urlAdmin('mall_consult', 'type_list'));
            }else {
                $this->log('수정고객센터문의유형 ID:'.$where['mct_id'],0);
                showMessage(L('nc_common_op_fail'));
            }
        }

        $mct_id = intval($_GET['mct_id']);
        if ($mct_id <= 0) {
            showMessage(L('param_error'));
        }
        $mct_info = $model_mct->getMallConsultTypeInfo(array('mct_id' => $mct_id));
        Tpl::output('mct_info', $mct_info);
        Tpl::showpage('mall_consult.type_edit');
    }

    /**
     * 삭제문의유형
     */
    public function type_delOp() {
        $mct_id = intval($_GET['mct_id']);
        if ($mct_id <= 0) {
            showMessage(L('param_error'));
        }
        $result = Model('mall_consult_type')->delMallConsultType(array('mct_id' => $mct_id));
        if ($result) {
            $this->log('삭제고객센터문의유형 ID:'.$mct_id, 1);
            showMessage(L('nc_common_del_succ'));
        } else {
            $this->log('삭제고객센터문의유형 ID:'.$mct_id, 0);
            showMessage(L('nc_common_del_fail'));
        }
    }
}
