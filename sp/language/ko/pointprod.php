<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 积分선물功能公用
 */
$lang['admin_pointprodp']	 		= '선물';
$lang['admin_pointprod_unavailable']	 		= '포인트 중심 미시동 상태입니다, 자동시동여부';
$lang['admin_pointprod_parameter_error']		= '오류';
$lang['admin_pointprod_record_error']			= '기록정보 오류';
$lang['admin_pointprod_userrecord_error']		= '회원정보 오류';
$lang['admin_pointprod_goodsrecord_error']		= '선물정보 오류';
$lang['admin_pointprod_list_title']			= '선물리스트';
$lang['admin_pointprod_add_title']			= '선물추가';
$lang['admin_pointprod_state']				= '상태';
$lang['admin_pointprod_show_up']			= '진열';
$lang['admin_pointprod_show_down']			= '미진열';
$lang['admin_pointprod_commend']			= '추천';
$lang['admin_pointprod_forbid']				= '판매금지';
$lang['admin_pointprod_goods_name']			= '선물이름';
$lang['pointprod_help1']					= '포인트 교환 기능은 우선 포인트 기능을 시동(운영 -> 기본설정) 하셔야합니다, 선물은 포인트 중심에서 노출되여 소비자가 교환할 수 있습니다.';
$lang['admin_pointprod_goods_points']		= '포인트교환';
$lang['admin_pointprod_goods_price']		= '선물원가';
$lang['admin_pointprod_goods_storage']		= '재고';
$lang['admin_pointprod_goods_view']			= '보기';
$lang['admin_pointprod_salenum']			= '판매완료';
$lang['admin_pointprod_yes']				= '예';
$lang['admin_pointprod_no']					= '아니오';
$lang['admin_pointprod_delfail']			= '삭제실패';
$lang['admin_pointorder_list_title']		= '교환리스트';
/**
 * 추가
 */
$lang['admin_pointprod_baseinfo']		= '선물기본정보';
$lang['admin_pointprod_goods_image']	= '선물 이미지';
$lang['admin_pointprod_goods_tag']		= '선물태그';
$lang['admin_pointprod_goods_serial']	= '선물번호';
$lang['admin_pointprod_requireinfo']	= '교환요구';
$lang['admin_pointprod_limittip']		= '매 회원 교환 수량 제한';
$lang['admin_pointprod_limit_yes']		= '제한';
$lang['admin_pointprod_limit_no']		= '무제한';
$lang['admin_pointprod_limitnum']		= '매 회원 교환 수량 제한';
$lang['admin_pointprod_freightcharge']	= '배송료 부담방식';
$lang['admin_pointprod_freightcharge_saler']	= '판매자';
$lang['admin_pointprod_freightcharge_buyer']	= '소비자';
$lang['admin_pointprod_freightprice']	= '배송료가격';
$lang['admin_pointprod_limittimetip']		= '교환 시간 제한';
$lang['admin_pointprod_limittime_yes']		= '제한';
$lang['admin_pointprod_limittime_no']		= '무제한';
$lang['admin_pointprod_starttime']	= '시작시간';
$lang['admin_pointprod_endtime']	= '마감시간';
$lang['admin_pointprod_time_day']	= '일';
$lang['admin_pointprod_time_hour']	= '시';
$lang['admin_pointprod_stateinfo']	= '상태설정';
$lang['admin_pointprod_isshow']	= '진열여부';
$lang['admin_pointprod_iscommend']	= '추천여부';
$lang['admin_pointprod_isforbid']	= '판매금지여부';
$lang['admin_pointprod_forbidreason']= '판매금지원인';
$lang['admin_pointprod_seoinfo']	= 'SEO설정';
$lang['admin_pointprod_seokey']		= '키워드';
$lang['admin_pointprod_otherinfo']		= '기타설정';
$lang['admin_pointprod_sort']		= '선물정렬';
$lang['admin_pointprod_sorttip']		= '주의: 작은순으로 정렬';
$lang['admin_pointprod_seodescription']		= 'SEO설명';
$lang['admin_pointprod_descriptioninfo']	= '선물설명';
$lang['admin_pointprod_uploadimg']	= '이미지 업로드';
$lang['admin_pointprod_uploadimg_more']	= '일괄업로드';
$lang['admin_pointprod_uploadimg_common']	= '일반업로드';
$lang['admin_pointprod_uploadimg_complete']	= '보유이미지';
$lang['admin_pointprod_uploadimg_add']	= '삽입';
$lang['admin_pointprod_uploadimg_addtoeditor']	= '삽입수정기';
$lang['admin_pointprod_add_goodsname_error']	= '선물이름을 입력하세요';
$lang['admin_pointprod_add_goodsprice_null_error']	= '선물 원가를 입력하세요';
$lang['admin_pointprod_add_goodsprice_number_error']	= '선물 원가는 반드시 0보다 큰 숫자로 입력하세요';
$lang['admin_pointprod_add_goodspoint_null_error']	= '교환 포인트를 입력하세요';
$lang['admin_pointprod_add_goodspoint_number_error']	= '교환 포인트는 반드시 0보다 큰 숫자로 입력하세요';
$lang['admin_pointprod_add_goodsserial_null_error']	= '선물번호를 입력하세요';
$lang['admin_pointprod_add_storage_null_error']	    = '선물재고를 입력하세요';
$lang['admin_pointprod_add_storage_number_error']	= '선물재고는 반드시 0보다 큰 숫자로 입력하세요';
$lang['admin_pointprod_add_limitnum_error']			= '매 회원 교환 수량 제한을 입력하세요';
$lang['admin_pointprod_add_limitnum_digits_error']	= '회원 교환 수량 제한은 반드시 0보다 큰 숫자를 입력하세요';
$lang['admin_pointprod_add_freightprice_null_error']		= '배송료 가격을 입력하세요';
$lang['admin_pointprod_add_freightprice_number_error']		= '배송료 가격은 반드시 0보단 큰 숫자로 입력하세요';
$lang['admin_pointprod_add_limittime_null_error']		= '시작시간 및 마감시간을 추가하세요';
$lang['admin_pointprod_add_sort_null_error']		= '선물정렬을 입력하세요';
$lang['admin_pointprod_add_sort_number_error']		= '선물정렬은 반드시 0보다 큰 숫자로 입력하세요';
$lang['admin_pointprod_add_upload']		= '업로드';
$lang['admin_pointprod_add_upload_img_error']		= '이미지 허용 확장명: png,gif,jpeg,jpg';
$lang['admin_pointprod_add_iframe_uploadfail']		= '업로드 실패';
$lang['admin_pointprod_add_success']		= '선물 추가 성공';
/**
 * 업데이트
 */
$lang['admin_pointprod_edit_success']		= '선물 업데이트 성공';
$lang['admin_pointprod_edit_addtime']		= '가입시간';
$lang['admin_pointprod_edit_viewnum']		= '힛트수';
$lang['admin_pointprod_edit_salenum']		= '판매수량';
/**
 * 삭제
 */
$lang['admin_pointprod_del_success']		= '삭제성공';
$lang['admin_pointprod_del_fail']			= '삭제실패';