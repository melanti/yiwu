(function($, angular, _, APP){

  if( $(document.body).attr('data-page-id')=='item-registration' ||  $(document.body).attr('data-page-id')=='item-modification' ){
    bootstrapItemRegistration();
  }

  function bootstrapItemRegistration(){
    angular.module('App')
      .directive( 'itemRegistration', ['XHR', 'PubSub', 'URL_INFO', itemRegistrationDirective] );

    angular.module('App')
      .directive( 'threedRegistration', ['XHR', 'PubSub', 'URL_INFO', threeDRegistrationDirective] );

    angular.module('App')
      .directive( 'tdPrevModal', ['XHR', 'PubSub', 'URL_INFO', tdPrevModal] );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( (parseInt(APP.info.user.auth.item,10)>=1 && window.location.href.indexOf('confirmation')!==-1) || (parseInt(APP.info.user.auth.item,10)==2 && (window.location.href.indexOf('registration')!==-1) || (window.location.href.indexOf('modification')!==-1)) ){
          //if( parseInt(APP.info.user.auth.item,10)>=2 ){
            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }
        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }



  function itemRegistrationDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      controller: ['$scope', '_', '$q', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL', 'BrandTableManager', 'CategoryManager', 'AccOrgListTableManager', 'ThreeDTableManager', APP.modules.ctrl.itemRegistrationCtrl],
      //controller: ['$scope', '_', '$q',  '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL', 'BrandTableManager', 'CategoryManager', 'OperationTableManager', 'ThreeDTableManager', APP.modules.ctrl.itemRegistrationCtrl],
      controllerAs: 'itemRegistrationCtrl',
      bindToController: {
        pageType: '@',
        itemId: '@'
      },
      link: function (scope, elem, attrs, ctrl, transclude) {
        ctrl.type = attrs.type || '';
        transclude(scope, function(clone, scope) {
          elem.find('.threeD_registration_popup').empty().append(clone);
        });
        elem.find("#_image_upload").bind('change', function(e){
          var file = e.target.files[0];
          var _name = file.name;
          var _format = _name.slice(_name.lastIndexOf('.') + 1).toLowerCase();
          if (_format.match(/jpg|png|gif|jpge/) === null) {
            e.target.value = null;
            alert(APP.i18n['items.regist.alert.regist_img2']);
            return;
          }
          var URL = window.URL.createObjectURL(file);
          scope.itemRegistrationCtrl.itemListImage = URL;
          scope.itemRegistrationCtrl._state.thumbnail = file;
          scope.itemRegistrationCtrl.drop_thumbnail_flag = false;
          e.target.value = null;
          scope.$apply();
        });
        scope.$watch('itemRegistrationCtrl.display_start_date_str', function(newValue){
          elem.find("#st_date").val(newValue);
        });
        scope.$watch('itemRegistrationCtrl.display_end_date_str', function(newValue){
          elem.find("#end_date").val(newValue);
        });
      },
      templateUrl: 'item_registration.html'
    };
  }



  function threeDRegistrationDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      controller: ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', APP.modules.ctrl.threeDRegistrationCtrl],
      controllerAs: 'threeDRegistrationCtrl',
      bindToController: {
      },
      require: '^itemRegistration',
      link: function (scope, elem, attrs, ctrl) {
        //PubSub.trigger('tableComponent:bootstrap',{tableSearchCtrl:true});
      },
      templateUrl: 'threeD_registration.html'

    };
  }

  function tdPrevModal(XHR, PubSub, URL_INFO){

    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', APP.modules.ctrl.tdPrevModalCtrl],
      controllerAs: 'tdPrevModalCtrl',
      bindToController: {
      },
      link: function ($scope, elem, attrs, ctrl) {
        $('#tdPreviewModal').on('show show.bs.modal', function() {
          //console.log('show.bs.modal');
          //$scope.$apply();
        });

        $('#tdPreviewModal').on('shown shown.bs.modal', function() {
           //console.log('shown.bs.modal');
          $scope.$apply();
        });

        $('#tdPreviewModal').on('hide hide.bs.modal', function() {
          //console.log('hide.bs.modal');
          //$scope.$apply();
        });

        $('#tdPreviewModal').on('hidden hidden.bs.modal', function() {
           //console.log('hidden.bs.modal');
          $scope.$apply();
        });

      },
      templateUrl: 'td_prev_modal.html'
    };

  }

})(window.jQuery, window.angular, window._, window.APP);
