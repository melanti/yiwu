<?php defined('InCNBIZ') or exit('Access Invalid!'); ?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>자영업 업체</h3>
      <ul class="tab-base">
        <li><a href="javascript:;" class="current"><span>관리</span></a></li>
        <li><a href="index.php?act=ownshop&op=add"><span>추가</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="get" name="formSearch" id="formSearch">
  <input type="hidden" name="act" value="ownshop" />
  <input type="hidden" name="op" value="list" />
  <table class="tb-type1 noborder search">
  <tbody>
    <tr>
      <th><label for="store_name">업체</label></th>
      <td><input type="text" value="<?php echo $output['store_name']; ?>" name="store_name" id="store_name" class="txt" /></td>
      <td>
        <a href="javascript:void(0);" id="ncsubmit" class="btn-search " title="<?php echo $lang['nc_query'];?>">&nbsp;</a>
<?php if ($output['store_name'] != '') { ?>
        <a href="index.php?act=ownshop&op=list" class="btns" title="<?php echo $lang['nc_cancel_search'];?>"><span><?php echo $lang['nc_cancel_search'];?></span></a>
<?php } ?>
      </td>
    </tr>
  </tbody>
  </table>
  </form>
   <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts']; ?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
          <ul>
            <li>자영업으로 된 업체를 추가,수정,삭제 관리할 수 있습니다.</li>
            <li>미연동된 전체 상품 항목을 자영업 업체는 사용할 수 있습니다.</li>
            <li>이미 발표된 자영업 업체의 상품은 삭제할 수 없습니다.</li>
            <li>자영업 업체를 삭제시 해당 이미지 및 계정을 동시에 삭제되므로 참고하시길 바랍니다.</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
  <form method="post" id="store_form">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <thead>
        <tr class="thead">
          <th>업체명</th>
          <th>업체아이디</th>
          <th>점장판매자계정</th>
          <th class="align-center">상태</th>
          <th class="align-center">모든연동항목</th>
          <th class="align-center">조작</th>
        </tr>
      </thead>
<?php if (empty($output['store_list'])) { ?>
      <tbody>
        <tr class="no_data">
          <td colspan="15"><?php echo $lang['nc_no_record'];?></td>
        </tr>
      </tbody>
<?php } else { ?>
      <tbody>
<?php foreach($output['store_list'] as $k => $v) { ?>
        <tr class="">
          <td>
            <a href="<?php echo urlShop('show_store','index', array('store_id'=>$v['store_id'])); ?>" >
              <?php echo $v['store_name']; ?>
            </a>
          </td>
          <td><?php echo $v['member_name']; ?></td>
          <td><?php echo $v['seller_name']; ?></td>
          <td class="align-center w72">
            <?php echo $v['store_state'] ? $lang['open'] : $lang['close']; ?>
          </td>
          <td class="align-center w120"><?php echo $v['bind_all_gc'] ? '예' : '否'; ?></td>
          <td class="align-center w200">
            <a href="index.php?act=ownshop&op=edit&id=<?php echo $v['store_id']; ?>">수정</a>
<?php if (!$v['bind_all_gc']) { ?>
            |
            <a href="index.php?act=ownshop&op=bind_class&id=<?php echo $v['store_id']; ?>">경영항목</a>
<?php } ?>
<?php if (empty($output['storesWithGoods'][$v['store_id']])) { ?>
            |
            <a href="index.php?act=ownshop&op=del&id=<?php echo $v['store_id']; ?>" onclick="return confirm('본 조작은 복구가 불가능합니다, 정말 삭제하시겠습니까?');">삭제</a>
<?php } ?>
          </td>
        </tr>
<?php } ?>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td></td>
          <td colspan="16">
            <div class="pagination"><?php echo $output['page']; ?></div></td>
        </tr>
      </tfoot>
<?php } ?>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.edit.js" charset="utf-8"></script>
<script>
$(function(){
    $('#ncsubmit').click(function(){
        $('input[name="op"]').val('store');$('#formSearch').submit();
    });
});
</script>
