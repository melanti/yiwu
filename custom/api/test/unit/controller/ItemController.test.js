var request = require('supertest');
var should = require('should');
var Q = require('q');

var tmpVersions,
	lib_net,
	ItemController,
	RequestService,
	CommonService;

if (process.env['MODE'] == 'ALL_DEBUG') {
	// Operation Test
	startTest();
}
// terminal -> NODE_ENV=local MODE=debug LOG_LEVEL=$1 mocha test/unit/controller/Item*.js

startTest();

function startTest() {
	describe('=== C. ITEM 테스트 케이스 ===\n', function () {
		testC0();
		//testC1();
		testC2();
	});
}

function testC0() {
	describe('=== A.0 모듈 설정 === \n', function () {
		it('A.0.1 assignModule', function (done) {
			assignBasicModule();
			assignAccountCtrlModule();
			done();
		});
	});
}

function testC1() {
	describe('ItemController - list ', function () {
		describe('[show]', function () {
			it('1. success : not param', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?'+'_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}

					//여기서 should로  값체킹 (https://shouldjs.github.io/)
					//should가 틀리면 자동적으로 error 던짐
					//맞으면 done()
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(should.not.exist(res.body.debug));

					/*
					for(var i in res.body.results){
						res.body.results[i].should.property('item_thumbnail_url');
					}
					*/

					done();
				});
			});
			it('2. success : debug', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);
					done();
				});
			});
			it('3. success : debug, thumbnail', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug,thumbnail'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					/*
				 for(var i in res.body.results){
					 sails.log.debug(res.body.results[i].should.property('item_thumbnail_url'));
				 }
				*/
					done();
				});
			});
			it('4. success : debug, thumbnail, categories', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug,thumbnail,categories'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					/*
					for(var i in res.body.results){
						sails.log.debug(res.body.results[i].should.property('item_thumbnail_url'));
						sails.log.debug(res.body.results[i].should.property('category_list'));
					}
					*/
					done();
				});
			});
			it('5. success : debug, thumbnail, categories, td_id', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug,thumbnail,categories,td_id'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					/*
					for(var i in res.body.results){
						sails.log.debug(res.body.results[i].should.property('item_thumbnail_url'));
						sails.log.debug(res.body.results[i].should.property('category_list'));
						sails.log.debug(res.body.results[i].should.property('td_ids'));
					}
					*/
					done();
				});
			});

			it('6. success : debug, thumbnail, categories, td_id, td_list', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug,thumbnail,categories,td_id,td_list'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);


					sails.log.debug(res.body.results[0].item_thumbnail_url);
					sails.log.debug(res.body.results[0].category_list);
					sails.log.debug(res.body.results[0].td_ids);
					sails.log.debug(res.body.results[0].td_list[0].td_fitting_files.package.url);


					done();
				});
			});
		});	// end show

		describe('[account_uuid]', function () {
			it('1. success : 1 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&account_uuid=1'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : 2 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&account_uuid=2'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('3. success : 3 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&account_uuid=3'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end account_uuid
		describe('[start, count]', function () {
			it('1. success : count 5 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&count=5'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : start 2 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&start=2'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('3. success : start 2 count 2 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&start=2&count=2'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end count

		describe('[item_ids]', function () {
			it('1. success : item_ids 8 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_ids=8'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : item_ids 8,12 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_ids=8,12'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('3. success : item_ids 2 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_ids=2'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('4. success : item_ids 8,9 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_ids=8,9'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end item_ids
		describe('[item_uuid]', function () {
			it('1. success : item_uuid 5 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_uuid=5'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : item_uuid 15 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_uuid=15'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end item_uuid
		describe('[item_name]', function () {
			it('1. success : item_name 원피스 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_name=black'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : item_name 쟈켓 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_name=aa'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end item_uuid
		describe('[origin_item_id]', function () {
			it('1. success : origin_item_id 1 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&origin_item_id=1'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);
					sails.log.debug(res.body.results);

					done();
				});
			});
			it('2. success : origin_item_id 13 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&origin_item_id=13'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);
					sails.log.debug(res.body.results);

					done();
				});
			});
		});	// end origin_item_id
		describe('[td_name]', function () {
			it('1. success : td_name test5 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&td_name=test5'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);
					sails.log.debug(res.body.results)

					done();
				});
			});
			it('2. success : td_name test2 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&td_name=test2'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);
					sails.log.debug(res.body.results);

					done();
				});
			});
		});	// end td_name
		describe('[td_code]', function () {
			it('1. success : td_name WomCloTop019000006 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&td_code=WomCloTop019000006'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);
					sails.log.debug(res.body.results)

					done();
				});
			});
			it('2. success : td_code WomCloTop019000013 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&td_code=WomCloTop019000013'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);
					sails.log.debug(res.body.results);

					done();
				});
			});
		});	// end td_code
		describe('[org_id]', function () {
			it('1. success : org_id 2 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&org_id=2'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : org_id 13 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&org_id=13'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end org_id
		describe('[org_name]', function () {
			it('1. success : org_name mini ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&org_name=mini'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : org_name abc ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&org_name=abc'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end org_id
		describe('[brand_id]', function () {
			it('1. success : brand_id 8 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&brand_id=8'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : brand_id 55 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&brand_id=55'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end brand_id
		describe('[item_cate_id]', function () {
			it('1. success : item_cate_id 4 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_cate_id=4'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : item_cate_id 55 ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&item_cate_id=55'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end item_cate_id
		describe('[period_reg_start]', function () {
			it('1. success : period_reg_start, period_reg_end ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&period_reg_start=2016-08-04 00:00:00&period_reg_end=2016-08-04 23:59:59'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
			it('2. success : period_reg_start, period_reg_end  ', function (done) {
				request(sails.hooks.http.app)
				.get(RequestService.API_URL.GET_ITEM_LIST + '?show=debug&period_reg_start=2016-08-15 00:00:00&period_reg_end=2016-08-15 23:59:59'+'&_='+(new Date().getTime()))
				.set('Accept', 'application/json, text/plain, */*')
				.set('Content-Type', 'application/json;charset=UTF-8')
				.end(function(err, res) {
					if(err) {
						sails.log.debug('err: ', err);
						throw err;
					}
					sails.log.debug(res.body.should.property('info'));
					sails.log.debug("results length : " + res.body.results.length);
					sails.log.debug(res.body.debug);

					done();
				});
			});
		});	// end period_reg_start, period_reg_end

	});
}

function testC2() {
	describe('ItemController - detail ', function () {
		it('1. success : 1', function (done) {
			request(sails.hooks.http.app)
			.get(RequestService.API_URL.GET_ITEM + '/1?'+'_='+(new Date().getTime()))
			.set('Accept', 'application/json, text/plain, */*')
			.set('Content-Type', 'application/json;charset=UTF-8')
			.end(function(err, res) {
				if(err) {
					sails.log.debug('err: ', err);
					throw err;
				}

				sails.log.debug(res.body);


				done();
			});
		});
		it('2. success : 1 ', function (done) {
			request(sails.hooks.http.app)
			.get(RequestService.API_URL.GET_ITEM + '/1?show=debug'+'&_='+(new Date().getTime()))
			.set('Accept', 'application/json, text/plain, */*')
			.set('Content-Type', 'application/json;charset=UTF-8')
			.end(function(err, res) {
				if(err) {
					sails.log.debug('err: ', err);
					throw err;
				}

				sails.log.debug(res.body);

				done();
			});
		});
		it('3. error : 1 , account_uuid = 1 ', function (done) {
			request(sails.hooks.http.app)
			.get(RequestService.API_URL.GET_ITEM + '1/?account_uuid=1&show=debug'+'&_='+(new Date().getTime()))
			.set('Accept', 'application/json, text/plain, */*')
			.set('Content-Type', 'application/json;charset=UTF-8')
			.end(function(err, res) {
				if(err) {
					sails.log.debug('err: ', err);
					throw err;
				}
				sails.log.debug(res.body.should.property('info'));
				sails.log.debug("results length : " + res.body.results.length);
				sails.log.debug(res.body.debug);

				/*
				 for(var i in res.body.results){
				 sails.log.debug(res.body.results[i].should.property('item_thumbnail_url'));
				 }
				 */
				done();
			});
		});
	});
}

function assignBasicModule() {
	tmpVersions = require('../../../package.json');
	lib_net = require('../../../api/controllers/libs/network.js');
	RequestService = require('../../util/RequestService.js');
	CommonService = require('../../util/CommonService.js');
}

function assignAccountCtrlModule() {
	ItemController = require('../../../api/controllers/ItemController.js');
}
