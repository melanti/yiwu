<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 공유링크 语言包
 */

$lang['del_share_link_ok'] = '공유 링크 삭제 성공.';
$lang['please_choose_del_share_link'] = '삭제할 내용을 선택하세요!';
$lang['share_name_no_null'] = '공유 이름을 입력하세요';
$lang['interface_address_no_null'] = 'API주소를 입력하세요';
$lang['sort_input_only_number'] = '정렬은 숫자만 입력 가능합니다.';
$lang['back_share_link_list'] = '돌아가기';
$lang['countinue_add_share_link'] = '공유 링크 계속 추가';
$lang['add_share_link_ok'] = '공유링크 추가 성공.';
$lang['add_share_link_fail'] = '공유링크 추가 실패.';
$lang['update_share_link_ok'] = '공유링크 수정 성공.';
$lang['update_share_link_fail'] = '공유링크 수정 실패.';
$lang['re_edit_share_link'] = '본 공유링크 재수정';

$lang['share_link'] = '링크공유';
$lang['manage'] = '관리';
$lang['share_name'] = '명칭공유';
$lang['category'] = '카테고리';
$lang['logo'] = '이미지표지';
$lang['interface_address'] = '인터페이스 주소';
$lang['operation'] = '설정';
$lang['share'] = '공유';
$lang['click_edit'] = '클릭후 편집';
$lang['are_you_sure_del_it'] = '삭제하시겠습니까?';
$lang['favorites'] = '즐겨찾기 추가';
$lang['logo_formart_err'] = '잘못된logo확장명 입니다，gif,jpeg,jpg,png으로 해주세요';
$lang['upload_recommended'] = '반드시는 아니지만，16x16픽셀의 로고 이미지 사이즈 를 권장합니다.';
$lang['interface_address_notice'] = '인터페이스 주소 중';
$lang['said_share_title'] = '타이틀을 공유합니다';
$lang['said_share_address'] = '주소를 공유합니다';
$lang['example'] = '예를 들어';
$lang['share_link_help1']='링크공유를 추가하거나 편집,삭제 하실수 있으십니다.';
$lang['share_link_help2']='인터페이스 주소는 공유사이트에서 제공합니다.';