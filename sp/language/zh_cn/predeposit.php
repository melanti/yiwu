<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 적립금功能公用
 */
$lang['admin_predeposit_no_record']	 		= '부합하는 조건이 없는 기록';
$lang['admin_predeposit_unavailable']	 	= '현금잔고 기능을 설정하지 않으셨습니다. 지금 현금잔고 설정 페이지로 이동합니다.';
$lang['admin_predeposit_parameter_error']	= '입력오류';
$lang['admin_predeposit_record_error']		= '기록정보오류';
$lang['admin_predeposit_userrecord_error']	= '회원정보오류';
$lang['admin_predeposit_membername']			= '회원명';
$lang['admin_predeposit_addtime']				= '추가시간';
$lang['admin_predeposit_maketime']				= '발표시간';
$lang['admin_predeposit_changetime']			= '변경시간';
$lang['admin_predeposit_apptime']				= '신청시간';
$lang['admin_predeposit_checktime']				= '심사시간';
$lang['admin_predeposit_paytime']				= '지불시간';
$lang['admin_predeposit_payment']				= '결제방식';
$lang['admin_predeposit_payed']				= '지불상태 변경';
$lang['admin_predeposit_addtime_to']			= '까지';
$lang['admin_predeposit_screen']				= '선별조건';
$lang['admin_predeposit_paystate']				= '지불상태';
$lang['admin_predeposit_recordstate']			= '상태기록';
$lang['admin_predeposit_backlist']				= '목록으로 돌아가기';
$lang['admin_predeposit_adminname']				= '설정 관리자';
$lang['admin_predeposit_adminremark']			= '관리자 비고';
$lang['admin_predeposit_memberremark']			= '회원비고';
$lang['admin_predeposit_remark']				= '비고';
$lang['admin_predeposit_shortprice_error']		= '현금잔고 부족 사용자,현금잔고 정보를 확인하세요.';
$lang['admin_predeposit_pricetype']				= '현금잔고 유형';
$lang['admin_predeposit_pricetype_available']	= '가용금액';
$lang['admin_predeposit_pricetype_freeze']		= '동결금액';
$lang['admin_predeposit_price']					= '금액';
$lang['admin_predeposit_sn']					= '충전번호';
$lang['admin_predeposit_cs_sn'] 				= '현금인출 번호';
$lang['admin_predeposit_cash_check'] 			= '심사';
$lang['admin_predeposit_cash_pay'] 				= '지불상태 수정';
$lang['admin_predeposit_enuth_error'] 				= '사용가능한 금액 부족';
$lang['admin_predeposit_check_tips'] 				= '심사후,동일한 가격의 회원 예치금은 동결됩니다.재무 지불 환경으로 넘어 가시겠습니까?';
/**
 * 충전功能公用
 */
$lang['admin_predeposit_rechargelist']				= '충전관리';
$lang['admin_predeposit_rechargewaitpaying']		= '미지불';
$lang['admin_predeposit_rechargepaysuccess']		= '지불완료';
$lang['admin_predeposit_rechargestate_auditing']	= '심사중';
$lang['admin_predeposit_rechargestate_completed']	= '완성';
$lang['admin_predeposit_rechargestate_closed']		= '닫음';
$lang['admin_predeposit_recharge_onlinecode']		= '온라인 거래 코드';
$lang['admin_predeposit_recharge_price']			= '충전금액';
$lang['admin_predeposit_recharge_huikuanname']		= '송금인 성명';
$lang['admin_predeposit_recharge_huikuanbank']		= '송금은행';
$lang['admin_predeposit_recharge_huikuandate']		= '송금날짜';
$lang['admin_predeposit_recharge_memberremark']		= '회원비고';
$lang['admin_predeposit_recharge_help1']			= '';
$lang['admin_predeposit_recharge_help2']			= '충전금액을 확인했는데도 미지불 상태로 되어있으면 수동으로 지불 상태를 변경하실 수 있습니다.';
$lang['admin_predeposit_recharge_searchtitle']			= '조건상영';
/**
 * 충전信息수정
 */
$lang['admin_predeposit_recharge_edit_logdesc']		= '회원 적립 지불상태 변경';
$lang['admin_predeposit_recharge_edit_success']		= '적립정보 수정 성공';
$lang['admin_predeposit_recharge_edit_fail']		= '적립정보 수정 실패';
$lang['admin_predeposit_recharge_edit_state']		= '상태수정';
$lang['admin_predeposit_recharge_notice']		= '관리자만 보기';
/**
 * 충전信息삭제
 */
$lang['admin_predeposit_recharge_del_success']		= '충전정보 삭제 성공';
$lang['admin_predeposit_recharge_del_fail']		= '충전정보 삭제 실패';
/**
 * 인출功能公用
 */
$lang['admin_predeposit_cashmanage']			= '출금 관리';
$lang['admin_predeposit_cashwaitpaying']		= '지불대기중';
$lang['admin_predeposit_cashpaysuccess']		= '지불성공';
$lang['admin_predeposit_cashstate_auditing']	= '심사중';
$lang['admin_predeposit_cashstate_completed']	= '완성';
$lang['admin_predeposit_cashstate_closed']		= '닫음';
$lang['admin_predeposit_cash_price']			= '출금금액';
$lang['admin_predeposit_cash_shoukuanname']			= '입금자명';
$lang['admin_predeposit_cash_shoukuanbank']			= '입금은행';
$lang['admin_predeposit_cash_shoukuanaccount']		= '입금계좌번호';
$lang['admin_predeposit_cash_remark_tip1']			= '관리자만 보기 가능';
$lang['admin_predeposit_cash_remark_tip2']			= '비고정보는 예치금상세보기 페이지에서 볼수 있으며 회원과 관리자 모두 확인가능';
$lang['admin_predeposit_cash_help1']			= '미지불 상태의 인출서는 인출서의 지불 상태를 수정 하실 수 있습니다.';
$lang['admin_predeposit_cash_help2']			= '삭제를 클릭하시면 미지불 인출서를 삭제하실 수 있습니다. ';
$lang['admin_predeposit_cash_confirm']			= '현금 잔고를 구매자 출금 계좌로 이체 하시겠습니까 ?';
/**
 * 인출信息삭제
 */
$lang['admin_predeposit_cash_del_success']	= '출금정보 삭제 성공';
$lang['admin_predeposit_cash_del_fail']		= '출금 정보 삭제 실패';
$lang['admin_predeposit_cash_del_reducefreezelogdesc']		= '회원 출금 기록삭제 성공함으로 현금잔고 동결 금액을 줄일수 있습니다.';
$lang['admin_predeposit_cash_del_adddesc']	= '회원 출금 기록 삭제 성공 하면 예치금 금액 증가합니다.';
/**
 * 인출信息수정
 */
$lang['admin_predeposit_cash_edit_reducefreezelogdesc']	= '회원 인출기록 상태 수정 및 금액 동결';
$lang['admin_predeposit_cash_edit_success']		= '인출정보 수정 성공';
$lang['admin_predeposit_cash_edit_fail']		= '인출정보 수정 실패';
$lang['admin_predeposit_cash_edit_state']		= '인출정보 상태 수정';
/**
 * 手动修改
 */
$lang['admin_predeposit_artificial'] 	= '수동편집';
$lang['admin_predeposit_artificial_membername_error'] 	= '회원정보 오류, 아이디를 재입력하세요';
$lang['admin_predeposit_artificial_membernamenull_error'] 	= '아이디를 입력하세요';
$lang['admin_predeposit_artificial_pricenull_error'] 		= '금액을 입력하세요';
$lang['admin_predeposit_artificial_pricemin_error'] 		= '금액은 0보다 큰 숫자를 입력하세요';
$lang['admin_predeposit_artificial_shortprice_error']		= '금액不足,会员当前사용가능금액为';
$lang['admin_predeposit_artificial_shortfreezeprice_error']	= '금액不足,会员当前동결금액为';
$lang['admin_predeposit_artificial_success']				= '회원 적립금 수정 성공';
$lang['admin_predeposit_artificial_fail']					= '회원 적립금 수정 실패';
$lang['admin_predeposit_artificial_operatetype']			= '증가/차감유형';
$lang['admin_predeposit_artificial_operatetype_add']		= '증가';
$lang['admin_predeposit_artificial_operatetype_reduce']		= '차감';
$lang['admin_predeposit_artificial_member_tip_1']			= '회원';
$lang['admin_predeposit_artificial_member_tip_2']			= ', 사용 가능한 적립금';
$lang['admin_predeposit_artificial_member_tip_3']			= ', 동결된 적립금';
$lang['admin_predeposit_artificial_notice']					= '사용 가능 금액 및 동결 금액을 수정할 수 있습니다.';
/**
 * 出入명세 
 */
$lang['admin_predeposit_log_help1']			= '현금잔고 입/출금 상세 내역 입니다.';
$lang['admin_predeposit_log_stage'] 	= '유형';
$lang['admin_predeposit_log_stage_recharge']	= '충전';
$lang['admin_predeposit_log_stage_cash']		= '출금';
$lang['admin_predeposit_log_stage_order']		= '소비';
$lang['admin_predeposit_log_stage_artificial']	= '수동수정';
$lang['admin_predeposit_log_stage_system']		= '시스템';
$lang['admin_predeposit_log_stage_income']	= '수입';
$lang['admin_predeposit_log_desc']		= '내용';
?>