<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="breadcrumb"><span class="icon-home"></span><span><a href="<?php echo SHOP_SITE_URL;?>">홈</a></span> <span class="arrow">></span> <span>신규입점신청</span> </div>
<div class="main">
  <div class="sidebar">
    <div class="title">
      <h3>신규입점신청</h3>
    </div>
    <div class="content">
                  <dl>
        <dt class="<?php echo $output['sub_step'] == 'step0' ? 'current' : '';?>"> <i class="hide"></i>입점약관동의</dt>
      </dl>
      <dl show_id="0">
        <dt onclick="show_list('0');" style="cursor: pointer;"> <i class="show"></i>업체정보등록</dt>
        <dd>
          <ul>
            <li class="<?php echo $output['sub_step'] == 'step1' ? 'current' : '';?>"><i></i>회사정보</li>
            <li class="<?php echo $output['sub_step'] == 'step2' ? 'current' : '';?>"><i></i>재무정보</li>
            <li class="<?php echo $output['sub_step'] == 'step3' ? 'current' : '';?>"><i></i>운영정보</li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt class="<?php echo $output['sub_step'] == 'pay' ? 'current' : '';?>"> <i class="hide"></i>계약및연장</dt>
      </dl>
      <dl>
        <dt> <i class="hide"></i>오픈</dt>
      </dl>
    </div>
    <div class="title">
      <h3>연락방식</h3>
    </div>
    <div class="content">
      <ul>
                <li>전화：<?php echo C('site_phone');?></li>
                <li>메일：<?php echo C('site_email');?></li>
      </ul> 
    </div>
  </div>
  <div class="right-layout">
    <div class="joinin-step">
      <ul>
        <li class="step1 <?php echo $output['sub_step'] >= 'step0' ? 'current' : '';?><?php echo $output['sub_step'] == 'pay' ? 'current' : '';?>"><span>입점약관동의</span></li>
        <li class="<?php echo $output['sub_step'] >= 'step1' ? 'current' : '';?><?php echo $output['sub_step'] == 'pay' ? 'current' : '';?>"><span>회사정보</span></li>
        <li class="<?php echo $output['sub_step'] >= 'step2' ? 'current' : '';?><?php echo $output['sub_step'] == 'pay' ? 'current' : '';?>"><span>재무정보</span></li>
        <li class="<?php echo $output['sub_step'] >= 'step3' ? 'current' : '';?><?php echo $output['sub_step'] == 'pay' ? 'current' : '';?>"><span>운영정보</span></li>
        <li class="<?php echo $output['sub_step'] >= 'step4' ? 'current' : '';?><?php echo $output['sub_step'] == 'pay' ? 'current' : '';?>"><span>계약및연장</span></li>
        <li class="step6"><span>오픈</span></li>
      </ul>
    </div>
    <div class="joinin-concrete">
      
<!-- 协议 -->
<?php require('store_joinin_c2c_apply.'.$output['sub_step'].'.php'); ?>
   </div>
  </div>
</div>