<?php defined('InCNBIZ') or exit('Access Invalid!'); ?>

  <div class="tabmenu">
    <?php include template('layout/submenu');?>
  </div>
  <div class="ncsc-form-default">
    <form id="add_form" action="index.php?act=store_voucher&op=quotaadd" method="post">
    	<input type="hidden" name="form_submit" value="ok"/>
      <dl>
        <dt><i class="required">*</i><?php echo '패키지구매수량'.$lang['nc_colon'];?></dt>
        <dd><input id="quota_quantity" name="quota_quantity" type="text" class="text w50"/><em class="add-on">개월</em><span></span>
          <p class="hint">구매단위 : 월(30일), 한번 구매시 최대 12개월. 해당 구매기간 내 상품권 이벤트를 등록하실 수 있습니다.</p>
          <p class="hint"><?php echo sprintf($lang['voucher_apply_add_tip2'],C('promotion_voucher_price'));?>,<?php echo sprintf($lang['voucher_apply_add_tip3'],C('promotion_voucher_storetimes_limit'));?></p>
        <p class="hint"><strong style="color: red">관련 비용은 정산시 차감됩니다.</strong></p>          
        </dd>
      </dl>
      <div class="bottom">
          <label class="submit-border"><input id="submit_button" type="submit" value="완료"  class="submit"></label>
      </div>
    </form>
  </div>
<script>
$(document).ready(function(){
    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span');
            error_td.append(error);
        },
    	submitHandler:function(form){
            var unit_price = parseInt(<?php echo C('promotion_voucher_price');?>);
            var quantity = parseInt($("#quota_quantity").val());
            var price = unit_price * quantity;
            showDialog('<?php echo $lang['voucher_apply_add_confirm1'];?>'+price+'<?php echo $lang['voucher_apply_add_confirm2'];?>', 'confirm', '', function(){
            	ajaxpost('add_form', '', '', 'onerror');
            });
    	},
        rules : {
            quota_quantity : {
                required : true,
                digits : true,
                min : 1,
                max : 12
            }
        },
        messages : {
            quota_quantity : {
            	required : '<i class="icon-exclamation-sign"></i><?php echo $lang['voucher_apply_num_error'];?>', 
            	digits : '<i class="icon-exclamation-sign"></i><?php echo $lang['voucher_apply_num_error'];?>', 
            	min : '<i class="icon-exclamation-sign"></i><?php echo $lang['voucher_apply_num_error'];?>',
            	max : '<i class="icon-exclamation-sign"></i><?php echo $lang['voucher_apply_num_error'];?>'
            }
       	}
    });
});
</script> 
