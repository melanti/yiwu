<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['nc_common_pselect']	=  '-선택-';


/**
 * 信息텍스트
 */
$lang['error']				= '요청 처리중 오류 발생:<br />';
$lang['homepage']			= '메인';
$lang['cur_location']		= '현재위치';
$lang['miss_argument']		= '파라미터 부족';
$lang['invalid_request']	= '불법 방문';
$lang['param_error']		= '파라미터 오류';
$lang['wrong_checkcode']	= '인증코드 오류';
$lang['no_record']			= '등록된 기록이 없습니다.';
$lang['nc_record']			= '기록';
$lang['currency']			= '￥';
$lang['nc_colon']      = '：';
$lang['currency_zh']			= '원';
$lang['ten_thousand']       = '万';
$lang['close']                	= 'CLOSE';
$lang['open']                 	= 'OPEN';
$lang['nc_yes']				= '예';
$lang['nc_no']				= '아니오';
$lang['nc_more']				= '더보기';
$lang['nc_default']			= '기본';
//调试模式语言包
$lang['nc_debug_current_page']			= "현재페이지";
$lang['nc_debug_request_time']			= "요청시간";
$lang['nc_debug_execution_time']		= "로딩시간";
$lang['nc_debug_memory_consumption']	= "메모리";
$lang['nc_debug_request_method']		= "요청방법";
$lang['nc_debug_communication_protocol']= "통신협의";
$lang['nc_debug_user_agent']			= "회원대리";
$lang['nc_debug_session_id']			= "세션ID";
$lang['nc_debug_logging']				= "로그기록";
$lang['nc_debug_logging_1']				= "개로그";
$lang['nc_debug_logging_2']				= "등록된 기록이 없습니다.";
$lang['nc_debug_load_files']			= "파일로딩";
$lang['nc_debug_trace_title']			= "페이지 Trace정보";

//조작 알림
$lang['nc_common_op_succ'] 	= '완료';
$lang['nc_common_op_fail'] 	= '실패';
$lang['nc_common_del_succ'] = '삭제성공';
$lang['nc_common_del_fail'] = '삭제실패';
$lang['nc_common_save_succ'] = '저장성공';
$lang['nc_common_save_fail'] = '저장실패';
$lang['nc_succ'] = '성공';
$lang['nc_fail'] = '실패';

//站外分享接口
$lang['nc_shareset_qqweibo'] 		= 'QQ WEIBO';
$lang['nc_shareset_sinaweibo'] 		= 'SINA WEIBO';

/**
 * header中的텍스트
 */
$lang['nc_hello']	= "Hello";
$lang['nc_logout']	= "로그아웃";
$lang['nc_guest']	= "손님";
$lang['nc_login']	= "로그인";
$lang['nc_register']	= "회원가입";
$lang['nc_seller'] = '업체중심';
$lang['nc_user_center']	= "마이페이지";
$lang['nc_message']		= "시스템 쪽지";
$lang['nc_help_center']	= "도움중심";
$lang['nc_more_links']	= "링크 더보기";
$lang['nc_index']		= "메인";
$lang['nc_cart']			= "장바구니";
$lang['nc_kindof_goods']	= "가지 상품";
$lang['nc_favorites']		= "나의 즐겨찾기";
$lang['nc_nothing']			= "무";
$lang['nc_filter'] = '필터';
$lang['nc_all_goods'] = '전체상품';
$lang['nc_search'] = '검색';
$lang['nc_publish'] = '발표';
$lang['nc_download'] = '다운로드';

$lang['nc_buying_goods']			= "이미 구매한 상품";
$lang['nc_mysns']					= "나의공간";
$lang['nc_myfriends']				= "나의친구";
$lang['nc_selled_goods']			= "이미 판매된 상품";
$lang['nc_selling_goods']			= "진열상품";
$lang['nc_mystroe']					= "나의미니샵";
$lang['nc_favorites_goods']			= "즐겨찾기한 상품";
$lang['nc_favorites_stroe']			= "즐겨찾기한 미니샵";
$lang['nc_cart_no_goods']			= "장바구니에 등록된 상품이 없습니다.";
$lang['nc_check_cart']				= "장바구니 보기";
$lang['nc_accounts_goods']			= "결제상품";
$lang['nc_goods_num_one']			= "총";
$lang['nc_goods_num_two']			= "가지 상품, 총금액";
$lang['nc_sign_multiply']			= "×";


/**
 * 翻页
 **/
$lang['first_page'] = '처음';
$lang['last_page'] = '끝';
$lang['pre_page'] = '이전';
$lang['next_page'] = '다음';

$lang['nc_none_input'] 	= '입력하세요';
$lang['nc_current_edit'] 	= '수정중';

$lang['nc_add']				= '추가';
$lang['nc_new']				= '추가';
$lang['nc_update']			= '업데이트';
$lang['nc_edit']			= '수정';
$lang['nc_del']				= '삭제';

$lang['nc_cut']				= '짜르기';
$lang['download_lang']      = '데이터 분리 다운로드';

$lang['order_log_cancel'] = '주문을 취소하였습니다.';
$lang['order_log_receive_paye'] = '입금을 확인하였습니다.';

// settings
$lang['setting_member_logo_tip'] = "회원센터 LOGO, 개인홈피 등 페이지에 노출, 추천 사이즈: 200X40픽셀";
$lang['setting_site_logo_tip'] = "사이트 LOGO, 사용자 페이지 상단에 노출, 추천 사이즈: 240X60픽셀";
$lang['setting_seller_logo'] = "판매관리자Logo";
$lang['setting_seller_logo_tip'] = "셀러센터 LOGO, 업체 페이지 상단에 노출, 추천 사이즈: 150X40픽셀";

$lang['setting_cur'] = "환율설정";
$lang['setting_cur_tip'] = "설정하신 환율은 상품 올릴시에만 적용됩니다.";

$lang['setting_language'] = "언어설정";
$lang['setting_seller_center'] = "몰인몰기능";

$lang['site_lang_ko'] = "한국어";
$lang['site_lang_cn'] = "中文版";

$lang['site_lang_tip'] = "판매관리자 및 시스템 관리자 언어설정.";
$lang['site_scm_tip'] = "몰인몰 기능 사용 여부.";

$lang['waybill_list'] = "리스트";
$lang['waybill_add'] = "추가";
$lang['waybill_design'] = "디자인";
$lang['waybill_save_templates'] = "송장템플릿 저장";
$lang['waybill_notemplates'] = "송장템플릿이 존재하지 않습니다.";
$lang['waybill_save_design'] = "송장템플릿 저장디자인";
$lang['waybill_del_templates'] = "송장템플릿 삭제";

  $lang['waybill_add_0'] = '页面导航';
  $lang['waybill_add_1'] = '运单模板';
  $lang['waybill_add_2'] = '模板名称';
  $lang['waybill_add_3'] = '运单模板名称，最多10个字';
  $lang['waybill_add_4'] = '物流公司';
  $lang['waybill_add_5'] = '模板对应的物流公司';
  $lang['waybill_add_6'] = '宽度';
  $lang['waybill_add_7'] = '运单宽度，单位为毫米(mm)';
  $lang['waybill_add_8'] = '高度';
  $lang['waybill_add_9'] = '运单高度，单位为毫米(mm)';
  $lang['waybill_add_10'] = '上偏移量';
  $lang['waybill_add_11'] = '运单模板上偏移量，单位为毫米(mm)';
  $lang['waybill_add_12'] = '左偏移量';
  $lang['waybill_add_13'] = '运单模板左偏移量，单位为毫米(mm)';
  $lang['waybill_add_14'] = '模板图片';
  $lang['waybill_add_15'] = '请上传扫描好的运单图片，图片尺寸必须与快递单实际尺寸相符';
  $lang['waybill_add_16'] = '启用';
  $lang['waybill_add_17'] = '请首先设计并测试模板然后再启用，启用后商家可以使用';
  $lang['waybill_add_18'] = '模板名称不能为空';
  $lang['waybill_add_19'] = '模板名称最多10个字';
  $lang['waybill_add_20'] = '宽度不能为空';
  $lang['waybill_add_21'] = '宽度必须为数字';
  $lang['waybill_add_22'] = '高度不能为空';
  $lang['waybill_add_23'] = '高度必须为数字';
  $lang['waybill_add_24'] = '上偏移量不能为空';
  $lang['waybill_add_25'] = '上偏移量必须为数字';
  $lang['waybill_add_26'] = '左偏移量不能为空';
  $lang['waybill_add_27'] = '左偏移量必须为数字';
  $lang['waybill_add_28'] = '图片不能为空';
  $lang['waybill_add_29'] = '图片类型不正确';

  $lang['goods_class_edit_0'] = '分类图片';
  $lang['goods_class_edit_1'] = '只有第一级分类可以上传图片，建议用16px * 16px，超出后自动隐藏';
  $lang['goods_class_edit_2'] = '发布虚拟商品';
  $lang['goods_class_edit_3'] = '关联到子分类';
  $lang['goods_class_edit_4'] = '允许';
  $lang['goods_class_edit_5'] = '勾选允许发布虚拟商品后，在发布该分类的商品时可选择交易类型为“虚拟兑换码”形式';
  $lang['goods_class_edit_6'] = '分佣比例';
  $lang['goods_class_edit_8'] = '必须为0-100的整数';
  $lang['goods_class_edit_9'] = '不更改所属分类（更改下拉';
  $lang['goods_class_edit_10'] = '注意：切换不要把顶级分类整体移动到其它分类下';
  $lang['goods_class_edit_11'] = '按钮先执行验证再提交表单';
  $lang['goods_class_edit_12'] = '标记类型时候修改 修改为';
  $lang['goods_class_edit_13'] = '类型搜索';

  $lang['goods_class_add_0'] = '分类图片';
  $lang['goods_class_add_1'] = '只有第一级分类可以上传图片，建议用16px * 16px，超出后自动隐藏';
  $lang['goods_class_add_2'] = '发布虚拟商品';
  $lang['goods_class_add_3'] = '关联到子分类';
  $lang['goods_class_add_4'] = '允许';
  $lang['goods_class_add_5'] = '勾选允许发布虚拟商品后，在发布该分类的商品时可选择交易类型为“虚拟兑换码”形式';
  $lang['goods_class_add_6'] = '分佣比例';
  $lang['goods_class_add_8'] = '必须为0-100的整数';
  $lang['goods_class_add_9'] = '不更改所属分类（更改下拉';
  $lang['goods_class_add_10'] = '注意：切换不要把顶级分类整体移动到其它分类下';
  $lang['goods_class_add_11'] = '按钮先执行验证再提交表单';
  $lang['goods_class_add_12'] = '标记类型时候修改 修改为';
  $lang['goods_class_add_13'] = '类型搜索';

  $lang['brand_add_0'] = '名称首字母';
  $lang['brand_add_1'] = '商家发布商品快捷搜索品牌使用';
  $lang['brand_add_2'] = '展示方式';
  $lang['brand_add_3'] = '图片';
  $lang['brand_add_4'] = '文字';
  $lang['brand_add_5'] = '在“全部品牌”页面的展示方式，如果设置为“图片”则显示该品牌的“品牌图片标识”，如果设置为“文字”则显示该品牌的“品牌名';
  $lang['brand_add_6'] = '裁剪图片后返回接收函数';
  $lang['brand_add_7'] = '上传失败';
  $lang['brand_add_8'] = '请填写正确首字母';

  $lang['brand_edit_5'] = '在“全部品牌”页面的展示方式，如果设置为“图片”则显示该品牌的“品牌图片标识”，如果设置为“文字”则显示该品牌的“品牌名';
  $lang['brand_edit_1'] = '商家发布商品快捷搜索品牌使用';
  $lang['brand_edit_6'] = '按钮先执行验证再提交表单';
  $lang['brand_edit_7'] = '编辑分类时清除分类信息';
  $lang['brand_edit_9'] = '请填写正确首字母';
  $lang['brand_edit_0'] = '名称首字母';
  $lang['brand_edit_2'] = '展示方式';
  $lang['brand_edit_8'] = '上传失败';
  $lang['brand_edit_4'] = '文字';
  $lang['brand_edit_3'] = '图片';

$lang['goods_close_text'] = '위반내림이유';
$lang['goods_verify_text'] = '미통과이유';
$lang['goods_verify'] = '심사통과';
$lang['goods_index_verifytext'] = '심사';

