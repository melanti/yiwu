<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['login_index_title_01']					= '시스템';
$lang['login_index_title_02']					= '관리자 플랫폼';
$lang['login_index_title_03']					= '쇼핑몰&nbsp;&nbsp;|&nbsp;&nbsp;CMS&nbsp;&nbsp;|&nbsp;&nbsp;SNS&nbsp;&nbsp;|&nbsp;&nbsp;WEi몰&nbsp;&nbsp;|&nbsp;&nbsp;통계';
$lang['login_index_username_null']				= '아이디를 입력하세요';
$lang['login_index_password_null']				= '비밀번호를 입력하세요';
$lang['login_index_checkcode_null']				= '인증코드를 입력하세요';
$lang['login_index_checkcode_wrong']			= '인증번호 오류, 다시 입력하세요';
$lang['login_index_not_admin']					= '관리자 신분이 아닙니다.';
$lang['login_index_username_password_wrong']	= '비밀번호 오류';
$lang['login_index_need_login']					= '로그인후 본 기능을 사용할 수 있습니다.';
$lang['login_index_username']					= '이름';
$lang['login_index_password']					= '비밀번호';
$lang['login_index_password_pattern']			= '6자 이상의 비밀번호를 입력하세요';
$lang['login_index_checkcode']					= '인증코드';
$lang['login_index_checkcode_pattern']			= '인증번호는 4자입니다.';
$lang['login_index_close_checkcode']			= '닫기';
$lang['login_index_change_checkcode']			= '새로고침';
$lang['login_index_button_login']				= '로그인';
$lang['login_index_CnBiz']						= 'Cnbiz';