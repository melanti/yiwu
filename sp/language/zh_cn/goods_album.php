<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 이미지空间
 */
$lang['g_album_manage']			= '이미지공간';
$lang['g_album_list']			= '앨범리스트';
$lang['g_album_pic_list']		= '이미지 리스트';
$lang['g_album_keyword']		= '미니샵 ID 혹은 이름을 입력하세요';
$lang['g_album_del_tips']		= '앨범 삭제후 앨범내 모든 이미지가 삭제됩니다.';
$lang['g_album_fmian']			= '봉면';
$lang['g_album_one']			= '앨범';
$lang['g_album_shop']			= '미니샵';
$lang['g_album_pic_count']		= '이미지수';
$lang['g_album_pic_one']		= '이미지';
