var conf = require("../../libs/config.js").get(),
    mysql = require(conf.db.name),
    executor = require("./delete_fitting_video.js"),
    conn = mysql.createConnection({
        connectionLimit: 10,
        host: conf.db.host,
        port: conf.db.port,
        database: conf.db.database,
        user: conf.db.user,
        password: conf.db.password
    });

conn.connect();

executor.deleteFittingVideo(conn);


