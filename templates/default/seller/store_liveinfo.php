<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert alert-block mt10">
  <ul>
    <li>1. "E-쿠폰 공동구매" 기능을 사용하시려면, 오프라인 매장이 있으셔야 합니다. 아래 내용을 입력하여 주십시오.</li>
    <li>2. 설정 후의 오프라인 상점 정보는 "E-쿠폰 공동구매"의 상세페이지 우측에 있는 미니샵소개 부분에 나타나게 됩니다. 하지만 온라인 상점 정보에는 영향을 끼치지 않습니다.</li>
    <li>3. 아래 설명들을 읽어보시면서 상세하게 주의하여 입력해 주십시오.</li>
  </ul>
</div>
<div class="ncsc-form-default">
  <form id="add_form" action="index.php?act=store_live&op=store_live" method="post" >
    <dl>
      <dt>교환번호생성어<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input class="w50 text" name="store_vrcode_prefix" type="text" maxlength="3" value="<?php echo $output['store']['store_vrcode_prefix'];?>" maxlength="30"  />
        <span></span>
        <p class="hint">교환번호 설정의 일부분 입니다. 서로 다른 미니샵간의 교환번호를 구별하기 위해 사용되며 교환 번호 사용의 안정성을 높이게 됩니다. 최대 3자의 알파벳 혹은 숫자만 가능합니다.</p>
      </dd>
    </dl>
    <dl>
      <dt>오프라인상점명<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input class="w200 text" name="live_store_name" type="text" id="live_store_name" value="<?php echo $output['store']['live_store_name'];?>" maxlength="30"  />
        <span></span>
        <p class="hint">오프라인 상점 명칭은 오프라인 "E-쿠폰 공동구매" 활동에서만 사용됩니다. 온라인미니샵명에는 영향을 끼치지 않으며 최대 30자까지 입력가능합니다. 입력하시지 않으시면 온라인 미니샵명과 동일하게 보여지게 됩니다." </p>
      </dd>
    </dl>
    <dl>
      <dt>오프라인상점 전화번호<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input class="w200 text" name="live_store_tel" type="text" id="live_store_tel" value="<?php echo $output['store']['live_store_tel'];?>" maxlength="30"  />
        <span></span>
        <p class="hint">"E-쿠폰 공동구매"활동 중, 구매자와 판매자가 연락을 진행할 때 사용되기 때문에 정확히 입력하여주시기 바랍니다.</p>
      </dd>
    </dl>
    <dl>
      <dt>오프라인상점 주소<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input class=" w500 text" name="live_store_address" type="text" id="live_store_address" value="<?php echo $output['store']['live_store_address'];?>" maxlength="30"  />
        <span></span>
        <p class="hint">오프라인 매장의 실제 주소가 있으시다면, 입력하여 저장하여 주시고 지도에 표시하여 주시기 바랍니다. 구매자들의 상품 구매 혹은 교환시 방문할 때 사용되게됩니다. </p>
        <div id="container" class="w500 h200 mt10"></div>

      </dd>
    </dl>
    <dl>
      <dt>교통정보：</dt>
      <dd>
        <textarea class="textarea w500 h50" name="live_store_bus"><?php echo $output['store']['live_store_bus'];?></textarea>
         <p class="hint">오프라인 상점 주변의 교통 정보를 남겨주세요. 비어두면 보여지지 않습니다.</p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="submit" class="submit" value="완료">
      </label>
    </div>
  </form>
</div>
</div>

<script type="text/javascript">
var cityName = '';
var address = '<?php echo str_replace("'",'"',$output['store']['live_store_address']);?>';
var store_name = '<?php echo str_replace("'",'"',$output['store']['live_store_name']);?>';
var map = "";
var localCity = "";
var opts = {width : 150,height: 50,title : "상점명:"+store_name}
function initialize() {
	map = new BMap.Map("container");
	localCity = new BMap.LocalCity();

	map.enableScrollWheelZoom();
	map.addControl(new BMap.NavigationControl());
	map.addControl(new BMap.ScaleControl());
	map.addControl(new BMap.OverviewMapControl());
	localCity.get(function(cityResult){
	  if (cityResult) {
	  	var level = cityResult.level;
	  	if (level < 13) level = 13;
	    map.centerAndZoom(cityResult.center, level);
	    cityResultName = cityResult.name;
	    if (cityResultName.indexOf(cityName) >= 0) cityName = cityResult.name;
	    	    	getPoint();
	    	  }
	});
}

function loadScript() {
	var script = document.createElement("script");
	script.src = "http://api.map.baidu.com/api?v=1.2&callback=initialize";
	document.body.appendChild(script);
}
function getPoint(){
	var myGeo = new BMap.Geocoder();
	myGeo.getPoint(address, function(point){
	  if (point) {
	    setPoint(point);
	  }
	}, cityName);
}
function setPoint(point){
	  if (point) {
	    map.centerAndZoom(point, 16);
	    var marker = new BMap.Marker(point);
	    var infoWindow = new BMap.InfoWindow("상점주소:"+address, opts);
			marker.addEventListener("click", function(){
			   this.openInfoWindow(infoWindow);
			});
	    map.addOverlay(marker);
			marker.openInfoWindow(infoWindow);
	  }
}
$(function(){
	loadScript();
});

</script>
