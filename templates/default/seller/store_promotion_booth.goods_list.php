<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>

<?php if ($output['isOwnShop']) { ?>
  <a class="ncsc-btn ncsc-btn-green" href="javascript:void(0);" cnbiztype="select_goods"><i class="icon-plus-sign"></i>상품추가</a>

<?php } else { ?>

  <?php if(empty($output['booth_quota'])) { ?>
  <a class="ncsc-btn ncsc-btn-acidblue" href="<?php echo urlShop('store_promotion_booth', 'booth_quota_add');?>" title="패키지구매"><i class="icon-money"></i>패키지구매</a>
  <?php } else { ?>
  <?php if ($output['booth_quota']['booth_state'] == 1) {?>
  <a class="ncsc-btn ncsc-btn-green" href="javascript:void(0);" cnbiztype="select_goods" style="right:100px"><i class="icon-plus-sign"></i>상품추가</a>
  <?php } ?>
  <a class="ncsc-btn ncsc-btn ncsc-btn-acidblue" href="<?php echo urlShop('store_promotion_booth', 'booth_renew');?>"><i class="icon-money"></i>패키지갱신</a>
  <?php } ?>
<?php } ?>

</div>

<?php if ($output['isOwnShop']) { ?>
<div class="alert alert-block mt10">
  <ul>
    <li>1. 추천상품은 해당상품 카테고리의 상품 리스트 좌측에 보여지게 됩니다.</li>
    <?php if (C('promotion_booth_goods_sum') != 0) {?>
    <li>2. 최대<?php echo C('promotion_booth_goods_sum');?>개의 상품을 추천하실 수 있습니다.</li>
    <?php }?>
  </ul>
</div>
<?php } else { ?>
<!-- 有可用套餐，发布活动 -->
<div class="alert alert-block mt10">
<?php if (empty($output['booth_quota']) || $output['booth_quota']['booth_state'] == 0) {?>
  <strong>패키지를 구매하지 않으셨거나 유효기간이 만료되었습니다. 패키지를 구매/갱신 하여주십시오.</strong>
<?php } else {?>
  <strong>패키지유효기간<?php echo $lang['nc_colon'];?></strong> <strong style=" color:#F00;"><?php echo date('Y-m-d H:i:s',$output['booth_quota']['booth_quota_endtime']);?></strong>
<?php }?>
  <ul>
    <li>1. 패키지 구매와 패키지 갱신을 클릭하시면 패키지의 구매 혹은 갱신이 가능합니다.</li>
    <li>2. <strong style="color: red">관련 비용은 정산시 차감됩니다.</strong></li>
    <li>3. 추천상품은 해당상품 카테고리의 상품 리스트 좌측에 보여지게 됩니다.</li>
    <?php if (C('promotion_booth_goods_sum') != 0) {?>
    <li>4. 최대<?php echo C('promotion_booth_goods_sum');?>개의 상품을 추천하실 수 있습니다.</li>
    <?php }?>
  </ul>
</div>
<?php } ?>

<?php if ($output['isOwnShop'] || (!empty($output['booth_quota']) && $output['booth_quota']['booth_state'] == 1)) { ?>
<!-- 商品搜索 -->
<div nvtype="div_goods_select" class="div-goods-select" style="display: none;">
    <table class="search-form">
      <tr><th class="w150"><strong>1단계 : 상품검색</strong></th><td class="w160"><input cnbiztype="search_goods_name" type="text w150" class="text" name="goods_name" value=""/></td>
        <td class="w70 tc"><label class="submit-border"><input cnbiztype="btn_search_goods" type="button" value="검색" class="submit"/></label></td><td class="w10"></td><td><p class="hint">검색어를 입력하지 않으시면 미니샵 내 모든 상품들이 보여지게 됩니다.</p></td>
      </tr>
    </table>
  <div cnbiztype="div_goods_search_result" class="search-result"></div>
  <a cnbiztype="btn_hide_goods_select" class="close" href="javascript:void(0);">X</a> </div>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w10"></th>
      <th class="w50"></th>
      <th class="tl">상품명</th>
      <th class="w180">가격</th>
      <th class="w110">편집</th>
    </tr>
  </thead>

  <tbody cnbiztype="choose_goods_list">
    <tr cnbiztype="tr_no_promotion" style="display:none;">
      <td colspan="20" class="norecord"><div class="no-promotion"><i class="zw"></i><span>현재 추천상품이 없습니다. 추천상품 리스트의 상품을 추가하여 주십시오.</span></div></td>
    </tr>
    <?php if(!empty($output['goods_list'])) { ?>
    <?php foreach($output['goods_list'] as $key=>$val){?>
    <?php
    $ncat = explode("|",$output['goodsclass_list'][$val['gc_id']]['gc_name']);
    $newCat = !empty($ncat[1]) ? $ncat[1] : $output['goodsclass_list'][$val['gc_id']]['gc_name'];
    ?>
    <tr class="bd-line">
      <td></td>
      <td><div class="pic-thumb"><a href="<?php echo $val['url'];?>" target="black"><img src="<?php echo $val['goods_image'];?>"/></a></div></td>
      <td class="tl">
        <dl class="goods-name">
          <dt><a href="<?php echo $val['url'];?>" target="_blank"><?php echo $val['goods_name_ko'];?></a></dt>
          <dd><?php echo $newCat;?></dd>
        </dl>
      </td>
      <td class="goods-price"><?php echo number_format($val['goods_price_ko']);?>원</td>
      <td class="nscs-table-handle">
        <span><a class="btn-red" href='javascript:void(0);' cnbiztype="del_choosed" data-param="{gid:<?php echo $val['goods_id'];?>}"><i class="icon-trash"></i><p>삭제</p></a></span></td>
    </tr>
    <?php }?>
    <?php } ?>
  </tbody>
</table>
<?php }else{?>
<!-- 没有可用套餐，购买 -->
<table class="ncsc-default-table ncsc-promotion-buy">
  <tbody>
    <tr>
      <td colspan="20" class="norecord"><div class="no-promotion"><i class="zw"></i><span>아직 패키지를 구매하지 않으셨거나 해당 판매 이벤트가 종료되었습니다. <br />패키지를 구매하시고, 이벤트 리스트를 다시 확인하여 주십시오.</span></div></td>
    </tr>
  </tbody>
</table>
<?php }?>
<script>
$(function(){
    // 验证是否已经选择商品
    choosed_goods();

    // 显示搜索框
    $('a[cnbiztype="select_goods"]').click(function(){
        $('div[nvtype="div_goods_select"]').show();
    });
    // 隐藏搜索框
    $('a[cnbiztype="btn_hide_goods_select"]').click(function(){
        $('div[nvtype="div_goods_select"]').hide();
    });

    // 搜索商品
    $('input[cnbiztype="btn_search_goods"]').click(function(){
        _url = '<?php echo urlShop('store_promotion_booth', 'booth_select_goods');?>';
        $('div[cnbiztype="div_goods_search_result"]').html('').load(_url, {'goods_name':$('input[cnbiztype="search_goods_name"]').val()});
    });
    $('div[nvtype="div_goods_select"]').on('click', '.demo', function(){
        $('div[cnbiztype="div_goods_search_result"]').load($(this).attr('href'));
        return false;
    });

    // 选择商品
    $('div[nvtype="div_goods_select"]').on('click', 'a[cnbiztype="a_choose_goods"]', function(){
        _url = '<?php echo urlShop('store_promotion_booth', 'choosed_goods');?>';
        eval('var data_str = ' + $(this).attr('data-param'));
        $.getJSON(_url, {gid : data_str.gid}, function(data){
            if (data.result == 'true') {
                // 插入数据
                $('<tr class="bd-line"></tr>')
                    .append('<td></td>')
                    .append('<td><div class="pic-thumb"><a target="_blank" href="' + data.url + '"><img src="' + data.goods_image + '"></a></div></td>')
                    .append('<td class="tl"><dl class="goods-name"><dt><a target="_blank" href="' + data.url + '">' + data.goods_name + '</a></dt><dd>' + data.gc_name_ko + '</dd></dl></td>')
                    .append('<td>' + addCommas(data.goods_price_ko) + '원</td>')
                    .append('<td class="nscs-table-handle"><span><a class="btn-red" href="javascript:void(0);" data-param="{gid:'+ data.goods_id +'}" cnbiztype="del_choosed"><i class="icon-trash"></i><p>삭제</p></a></span></td>')
                    .appendTo('tbody[cnbiztype="choose_goods_list"]');
                // 验证是否已经选择商品
                choosed_goods();
            } else {
                showError(data.msg);
            }
        });
    });

    // 삭제商品
    $('tbody[cnbiztype="choose_goods_list"]').on('click','a[cnbiztype="del_choosed"]', function(){
        $this = $(this);
        _url = '<?php echo urlShop('store_promotion_booth', 'del_choosed_goods');?>';
        eval('var data_str = ' + $(this).attr('data-param'));
        $.getJSON(_url, {gid : data_str.gid}, function(data){
            if (data.result == 'true') {
                $this.parents('tr:first').fadeOut("slow",function(){
                    $(this).remove();
                    choosed_goods();
                });
            } else {
                showErroe(data.msg);
            }
        });
    });
});

// 验证是否已经选择商品
function choosed_goods() {
    if ($('tbody[cnbiztype="choose_goods_list"]').children('tr').length == 1) {
        $('tr[cnbiztype="tr_no_promotion"]').show();
    } else {
        $('tr[cnbiztype="tr_no_promotion"]').hide();
    }
}
</script>