<?php
defined('InCNBIZ') or exit('Access Invalid!');

// index
$lang['promotion_unavailable']                          = '기획 기능이 켜져 있지 않습니다.';
$lang['bundling_quota']			= '패키지 리스트';
$lang['bundling_list']				= '프로모션';
$lang['bundling_setting']			= '설정';
$lang['bundling_gold_price']		             = '특별 패키지 가격';
$lang['bundling_sum']				= '특별 패키지 등록 수량';
$lang['bundling_goods_sum']			= '각 패키지 당 추가 상품 수량';
$lang['bundling_price_prompt']		= '구매 단위는 월(30일)이며, 구매 후 판매자는 구매 주 내에 사은품 프로모션 프로모션를 등록 할 수 있습니다.';
$lang['bundling_sum_prompt']		= '미니샵가 등록할수 있는 특별 패키지 수량을"0" 으로 제한하면 "무제한"을 뜻합니다.';
$lang['bundling_goods_sum_prompt']	= '각 패키지 당 상품을 추가 할 수 있는 최대 수량은 "5" 입니다.';
$lang['bundling_price_error']		             = '1 이상의 정수를 입력해 주세요.';
$lang['bundling_sum_error']			= '0 이상의 정수를 입력해 주세요.';
$lang['bundling_goods_sum_error']	             = '1~5 사이의 정수를 입력해 주세요.';
$lang['bundling_update_succ']		= '업데이트 성공';
$lang['bundling_update_fail']		               = '수정실패';

$lang['bundling_state_all']			= '전체';
$lang['bundling_state_1']			= 'OPEN';
$lang['bundling_state_0']			= 'CLOSE';


// 活动리스트
$lang['bundling_quota_list_prompts']= '판매자가 구매한 특별패키지 프로모션 리스트';
$lang['bundling_quota_store_name']	= '미니샵명';

// 活动수정
$lang['bundling_quota_endtime_required']	= '종료시간을 추가해 주십시오';
$lang['bundling_quota_endtime_dateValidate']= '종료시간은 시작시간 보다 커야 합니다';
$lang['bundling_quota_store_name']			= '미니샵명';
$lang['bundling_quota_quantity']			= '구매수량';
$lang['bundling_quota_starttime']			= '시작시간';
$lang['bundling_quota_endtime']				= '종료시간';
$lang['bundling_quota_endtime_tips']		= '만약 상태를 켜기로 설정 하셨다면 종료 시간은 지금시간과 같을수 없으며 적거나 같을시 켜실수 없습니다.';
$lang['bundling_quota_state_tips']			= '켜기 상태로 설정 후 종료 시간은 지금 시간보다 적거나 같을 수 없습니다.';
$lang['bundling_quota_prompts']				= '매개 미니샵의 프로모션 패키지 정보를 확인 취소 가능';

// 套餐리스트
$lang['bundling_name']						= '프로모션명';
$lang['bundling_price']						= '프로모션 판매 가격';
$lang['bundling_goods_count']				= '상품수량';
