<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['promotion_unavailable'] = '상품 프로모션 기능이 미시동 상태입니다.';
$lang['gold_unavailable'] = '金币功能尚未시동，正在跳转到金币설정 。。。';

$lang['state_new'] = '신규신청';
$lang['state_verify'] = '심사완성';
$lang['state_cancel'] = '취소완성';
$lang['state_verify_fail'] = '심사실패';
$lang['xianshi_state_unpublished'] = '미발표';
$lang['xianshi_state_published'] = '등록완';
$lang['xianshi_state_cancel'] = '취소완료';
$lang['all_state'] = '전체';


$lang['promotion_xianshi'] = '기간한정세일';
$lang['xianshi_list'] = '프로모션';
$lang['xianshi_apply'] = '패키지 신청 관리';
$lang['xianshi_quota'] = '패키지 리스트';
$lang['xianshi_setting'] = '설정';
$lang['apply_quantity'] = '신청수량 ';
$lang['apply_date'] = '신청날짜 ';
$lang['xianshi_quota_start_time'] = '시작시간';
$lang['xianshi_quota_end_time'] = '종료시간';
$lang['xianshi_quota_times_limit'] = '프로모션 제한 수';
$lang['xianshi_quota_times_published'] = '등록한 프로모션 수';
$lang['xianshi_quota_times_publish'] = '남은 프로모션 수';
$lang['xianshi_quota_goods_limit'] = '프로모션 상품 수 제한';


$lang['goods_name'] = '상품명';
$lang['goods_store_price'] = '상품가격';
$lang['store_name'] = '미니샵명';
$lang['xianshi_name'] = '프로모션명';
$lang['xianshi_detail'] = '프로모션 세부정보';
$lang['start_time'] = '시작 시간';
$lang['end_time'] = '종료 시간';
$lang['xianshi_discount'] = '세일';
$lang['xianshi_buy_limit'] = '1인당 구매제한';

$lang['xianshi_price'] = '기간한정세일 프로모션 가격';
$lang['xianshi_price_explain'] = '구매 단위는 월(30일)이며, 구매 후 판매자는 구매 주 내에 사은품 프로모션 프로모션를 등록 할 수 있습니다.';
$lang['xianshi_price_error'] = '0보다 큰 정수를 입력해 주세요.';
$lang['xianshi_times_limit'] = '매월 프로모션 수 제한 ';
$lang['xianshi_times_limit_explain'] = '매월 최대로 등록 할  수 있는 기간한정세일 패키지 수량';
$lang['xianshi_times_limit_error'] = '프로모션 수량은 반드시 0보다 큰 정수여야 합니다.';
$lang['xianshi_goods_limit'] = '각 프로모션 당 상품 수량 제한';
$lang['xianshi_goods_limit_explain'] = '각 기간한정세일 프로모션 당 상품 수량 제한 ';
$lang['xianshi_goods_limit_error'] = '상품수는 반드시 0보다큰 정수 여야 합니다.';
$lang['xianshi_apply_drop_notification'] = '기간한정세일 패키지 구매신청이 관리자에 의해서 삭제 됐습니다.';
$lang['xianshi_apply_verify_notification'] = '기간한정세일 패키지 구매신청이 관리자에 심사에 의해 통과 되었습니다.';
$lang['xianshi_apply_cancel_notification'] = '기간한정세일 패키지 구매신청이 관리자에 의해서 의해 취소 되었습니다.';
$lang['xianshi_apply_cancel_success'] = '신청취소 성공';
$lang['xianshi_apply_cancel_fail'] = '신청 취소 실패';
$lang['xianshi_apply_verify_success'] = '기간한정세일 신청 심사 성공, 프로모션 패키지는 지불 완료';
$lang['xianshi_apply_verify_fail'] = '기간한정세일 신청 심사 실패, 잔액이 부족합니다';
$lang['xianshi_apply_verify_success_glog_desc'] = '기간한정세일 신청 심사 구매%s개월, 단가 %s금액,총금액%s금액';
$lang['xianshi_apply_verify_success_message'] = '기간한정세일 프로모션를 성공적으로 구매하였습니다.%s개월,단가%s금액,총지불%s금액,시간은 심사후부터 계산합니다.';
$lang['xianshi_apply_verify_fail_message'] = '구매하신 기간한정세일 심사가 실패하였습니다,잔액을 확인하세요';
$lang['xianshi_quota_cancel_success'] = '패키지 취소 성공';
$lang['xianshi_quota_cancel_fail'] = '패키지 취소 실패';

$lang['confirm_verify'] = '심사 하시겠습니까?';
$lang['confirm_cancel'] = '취소 하시겠습니까?';
$lang['setting_save_success'] = '저장 성공';
$lang['setting_save_fail'] = '저장 실패';
$lang['text_normal'] = '정상';

$lang['xianshi_apply_list_help1'] = '기간한정세일 프로모션 신청에 대한 심사 결과 메세지는 자동으로 판매자에게 발송 됩니다.';
$lang['xianshi_apply_list_help2'] = '판매자 잔액 부족시 심사 통과 실패';
$lang['xianshi_quota_list_help1'] = '판매자의 타임 세일 패키지 목록';
$lang['xianshi_quota_list_help2'] = '"취소" 하시게되면 복구 할 수 없습니다. 신중히 선택해 주세요.';
$lang['xianshi_list_help1'] = '판매자가 등록한 기간한정세일 프로모션 리스트 입니다.';
$lang['xianshi_list_help2'] = '"취소" 하시게되면 복구 할 수 없습니다. 신중히 선택해 주세요.';
$lang['xianshi_list_help3'] = '상세링크 클릭후 프로모션 상세 페이지를 확인하세요.';


