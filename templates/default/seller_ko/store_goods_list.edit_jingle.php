<div class="eject_con">
  <div id="warning" class="alert alert-error"></div>
  <form method="post" action="<?php echo urlShop('store_goods_online', 'edit_jingle');?>" id="jingle_form">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="commonid" value="<?php echo $_GET['commonid']; ?>" />
    <dl>
      <dt>상품광고키워드：</dt>
      <dd>
        <input type="text" class="text w300" name="g_jingle" id="g_jingle" value="" />
        <p class="hint">입력하지 않으시면 모든 광고키워드란은 비워있게 되므로, 신중히 입력해 주세요.</p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input type="submit" class="submit" value="완료"/></label>
    </div>
  </form>
</div>
<script>
$(function(){
    $('#jingle_form').validate({
        errorLabelContainer: $('#warning'),
        invalidHandler: function(form, validator) {
               $('#warning').show();
        },
        submitHandler:function(form){
            ajaxpost('jingle_form', '', '', 'onerror');
        },
        rules : {
            g_jingle : {
                maxlength: 50
            }
        },
        messages : {
            g_jingle : {
                maxlength: '<i class="icon-exclamation-sign"></i>최대 50자 까지 입력 가능합니다.'
            }
        }
    });
});
</script> 
