<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/shop_custom.css" rel="stylesheet" type="text/css">
<div class="ncsc-path"><i class="icon-desktop"></i>셀러센터<i class="icon-angle-right"></i>미니샵<i class="icon-angle-right"></i미니샵디자인<i class="icon-angle-right"></i>페이지설정</div>
<div class="ncsc-decoration-layout">
  <div class="ncsc-decoration-menu" id="waypoints">
    <div class="title"><i class="icon"></i>
      <h3>디자인 설정</h3>
      <h5메인 템플릿 설정</h5>
    </div>
    <ul class="menu">
      <li><a id="btn_edit_background" href="javascript:void(0);"><i class="background"></i>배경편집</a></li>
      <li><a id="btn_edit_head" href="javascript:void(0);"><i class="head"></i>상단편집</a></li>
      <li><a id="btn_add_block" href="javascript:void(0);"><i class="block"></i>레이아웃추가</a></li>
      <li><a id="btn_preview" href="<?php echo urlShop('store_decoration', 'decoration_preview', array('decoration_id' => $_GET['decoration_id']));?>" target="_blank"><i class="preview"></i>미리보기</a></li>
      <li><a id="btn_close" href="javascript:void(0);"><i class="close"></i>완료</a></li>
    </ul>
    <div class="faq">페이지에 추가되는 이미지는 1180px 넓이로 편집해야합니다.<br> 
"레이아웃 추가"를 선택하신 후 각 유형을 선택하여 설정해 주세요.<br>
"미리보기"로 확인하신 후 저장하셔야 완료됩니다.</div>
  </div>
  <div id="store_decoration_content" style="<?php echo $output['decoration_background_style'];?>">
    <div id="decoration_banner" class="ncsl-nav"> </div>
    <div id="decoration_nav" class="ncsl-nav">
      <div class="ncs-nav">
        <ul>
          <li class="active"><a href="javascript:void(0);"><span>미니샵<i></i></span></a></li>
          <li><a href="javascript:void(0);"><span>공지사항<i></i></span></a></li>
        </ul>
      </div>
    </div>
    <div id="store_decoration_area" class="store-decoration-page">
      <?php if(!empty($output['block_list']) && is_array($output['block_list'])) {?>
      <?php foreach($output['block_list'] as $block) {?>
      <?php require('store_decoration_block.php');?>
      <?php } ?>
      <?php } ?>
    </div>
  </div>
</div>
<!-- 背景编辑对话框 -->
<div id="dialog_edit_background" class="eject_con dialog-decoration-edit" style="display:none;">
  <dl>
    <dt>배경색：</dt>
    <dd>
      <input id="txt_background_color" class="text w80" type="text" name="" value="<?php echo $output['decoration_setting']['background_color'];?>" maxlength="7">
      <p class="hint">배경색은 색상표에 등록된 코드(#XXXXXX)를 입력해 주시면 되고, 빈칸일 경우 기본 흰색이 적용됩니다.</p>
    </dd>
  </dl>
  <dl>
    <dt>배경이미지：</dt>
    <dd>
      <div class="ncsc-upload-btn"> <a href="javascript:void(0);"><span>
        <input type="file" hidefocus="true" size="1" class="input-file" id="file_background_image" name="file"/>
        </span>
        <p><i class="icon-upload-alt"></i>업로드</p>
        </a> </div>
      <div id="div_background_image" <?php if(empty($output['decoration_setting']['background_image'])) { echo "style='display:none;'";} ?> class="background-image-thumb"> <img id="img_background_image" src="<?php echo $output['decoration_setting']['background_image_url'];?>" alt="">
        <input id="txt_background_image" type="hidden" name="" value="<?php echo $output['decoration_setting']['background_image'];?>">
        <a id="btn_del_background_image" class="del" href="javascript:void(0);" title="석ㄱ재">X</a></div>
    </dd>
  </dl>
  <dl>
    <dt>배경이미지위치：</dt>
    <dd>
      <input id="txt_background_position_x" class="text w40" type="text" value="<?php echo $output['decoration_setting']['background_position_x'];?>"><label class="add-on">X</label>
      &#12288;&#12288;
      <input id="txt_background_position_y" class="text w40" type="text" value="<?php echo $output['decoration_setting']['background_position_y'];?>"><label class="add-on">Y</label>
      <p class="hint">이미지 시작위치</p>
    </dd>
  </dl>
  <dl>
    <dt>배열방식 ：</dt>
    <dd>
      <?php $repeat = $output['decoration_setting']['background_image_repeat'];?>
      <input id="input_no_repeat" type="radio" value="no-repeat" name="background_repeat" <?php if(empty($repeat) || $repeat == 'no-repeat') {echo 'checked';}?>>
      <label for="input_no_repeat">중복안함</label>
      <input id="input_repeat" type="radio" value="repeat" name="background_repeat" <?php if($repeat == 'repeat') {echo 'checked';}?>>
      <label for="input_repeat">바둑판식배열</label>
      <input id="input_repeat_x" type="radio" value="repeat-x" name="background_repeat" <?php if($repeat == 'repeat-x') {echo 'checked';}?>>
      <label for="input_repeat_x">x축 타일식배열</label>
      <input id="input_repeat_y" type="radio" value="repeat-y" name="background_repeat" <?php if($repeat == 'repeat-y') {echo 'checked';}?>>
      <label for="input_repeat_y">y축 타일식배열</label>
    </dd>
  </dl>
  <dl>
    <dt>배경고정：</dt>
    <dd>
      <input id="txt_background_attachment" class="text w80" type="text" value="<?php echo $output['decoration_setting']['background_attachment'];?>">
      <p class="hint">스크롤을 내릴 때 배경을 고정할지의 여부에 대해 입력해주세요. <br>ex) "scroll" / "fixed" </p>
    </dd>
  </dl>
  <div class="bottom">
    <label class="submit-border"><a id="btn_save_background" class="submit" href="javascript:void(0);">저장</a></label>
  </div>
</div>
<!-- 头部编辑对话框 -->
<div id="dialog_edit_head" class="eject_con dialog-decoration-edit" style="display:none;">
  <div id="dialog_edit_head_tabs">
    <ul>
      <li><a href="#dialog_edit_head_tabs_1">내비게이션</a></li>
      <li><a href="#dialog_edit_head_tabs_2">상단이미지</a></li>
    </ul>
    <div id="dialog_edit_head_tabs_1">
      <dl>
        <dt>노출여부：</dt>
        <dd>
          <label for="decoration_nav_display_true">
            <input id="decoration_nav_display_true" type="radio" class="radio" value="true" name="decoration_nav_display" <?php if(empty($output['decoration_nav']) || $output['decoration_nav']['display'] == 'true') { echo 'checked'; }?>>
            노출</label>
          <label for="decoration_nav_display_false">
            <input id="decoration_nav_display_false" type="radio" class="radio" value="false" name="decoration_nav_display" <?php if($output['decoration_nav']['display'] == 'false') { echo 'checked'; }?>>
            미노출</label>
          <p class="hint">"상단 바로가기메뉴"는 미니샵 메인에서 설정해놓으신 해당 페이지로 바로 접속할 수 있도록 합니다. <br>노출/미노출의 여부를 선택하시고, 선택하지 않으시면 기본적으로 노출로 설정됩니다.</p>
        </dd>
      </dl>
      <dl>
        <dt>내비게이션 템플릿：</dt>
        <dd>
          <textarea id="decoration_nav_style" class="w400 h100"><?php echo $output['decoration_nav']['style'];?></textarea>
          <p> <a id="btn_default_nav_style" class="ncsc-btn-mini" href="javascript:void(0);"><i class="icon-refresh"></i>돌아가기</a> </p>
          <p class="hint">내비게이션은 CSS파일에  상응하며, 편집후 문제가 생겼을 경우 <br>"돌아가기"를 클릭해 기본값으로 돌아갈 수 있습니다.</p>
        </dd>
      </dl>
      <div class="bottom">
        <label class="submit-border"><a id="btn_save_decoration_nav" class="submit" href="javascript:void(0);">저장</a></label>
      </div>
    </div>
    <div id="dialog_edit_head_tabs_2">
      <dl>
        <dt>노출여부：</dt>
        <dd>
          <label for="decoration_banner_display_true">
            <input id="decoration_banner_display_true" type="radio" class="radio" value="true" name="decoration_banner_display" <?php if(empty($output['decoration_banner']['display']) || $output['decoration_banner']['display'] == 'true') { echo 'checked'; }?>>
            노출</label>
          <label for="decoration_banner_display_false">
            <input id="decoration_banner_display_false" type="radio" class="radio" value="false" name="decoration_banner_display" <?php if($output['decoration_banner']['display'] == 'false') { echo 'checked'; }?>>
            미노출</label>
          <p class="hint"> "상단이미지"는 미니샵의 가장  상단에 위치하게 됩니다.</p>
        </dd>
      </dl>
      <dl>
        <dt>이미지：</dt>
        <dd>
          <div id="div_banner_image" <?php if(empty($output['decoration_banner']['image'])) { echo "style='display:none;'";} ?> class="background-image-thumb"> <img id="img_banner_image" src="<?php echo $output['decoration_banner']['image_url'];?>" alt="">
            <input id="txt_banner_image" type="hidden" name="" value="<?php echo $output['decoration_banner']['image'];?>">
            <a id="btn_del_banner_image" class="del" href="javascript:void(0);" title="삭제">X</a> </div>
          <div class="ncsc-upload-btn"> <a href="javascript:void(0);"> <span>
            <input type="file" hidefocus="true" size="1" class="input-file" id="file_decoration_banner" name="file"/>
            </span>
            <p><i class="icon-upload-alt"></i>업로드</p>
            </a> </div>
          <p class="hint">TIP : 넓이 1200px, JPG/GIF/PNG형식 파일</p>
        </dd>
      </dl>
      <div class="bottom">
        <label class="submit-border"><a id="btn_save_decoration_banner" class="submit" href="javascript:void(0);">저장</a></label>
      </div>
    </div>
  </div>
</div>
<!-- 选择模块对话框 -->
<div id="dialog_select_module" class="dialog-decoration-module" style="display:none;">
  <ul>
    <li><a cnbiztype="btn_show_module_dialog" data-module-type="slide" href="javascript:void(0);"><i class="slide"></i>
      <dl>
        <dt>이미지/슬라이드</dt>
        <dd>이미지/슬라이드 추가</dd>
      </dl>
      </a></li>
    <li><a cnbiztype="btn_show_module_dialog" data-module-type="hot_area" href="javascript:void(0);"><i class="hotarea"></i>
      <dl>
        <dt>이미지맵</dt>
        <dd>이미지+주소 연결</dd>
      </dl>
      </a></li>
    <li> <a cnbiztype="btn_show_module_dialog" data-module-type="goods" href="javascript:void(0);"><i class="goods"></i>
      <dl>
        <dt>미니샵상품</dt>
        <dd>진열상품 선택</dd>
      </dl>
      </a> </li>
    <li> <a cnbiztype="btn_show_module_dialog" data-module-type="html" href="javascript:void(0);"><i class="html"></i>
      <dl>
        <dt>직접입력</dt>
        <dd>에디터를 이용해 <br>직접 html 편집</dd>
      </dl>
      </a> </li>
  </ul>
</div>
<!-- 自定义模块编辑对话框 -->
<div id="dialog_module_html" class="eject_con dialog-decoration-edit" style="display:none;">
  <div class="alert">
    <ul>
        <li>1. 편집 완료된 인터넷 페이지 파일 내용을 복사해서 에디터에 붙여넣거나, 바로 에디터에 작성하시면 됩니다.。</li>
      <li>2. 웹DIY편집을 기본으로 하고, 아래 메뉴중 첫 번째 메뉴를 클릭하시면 html 코드로 편집하실 수 있습니다. css파일은 Style="..."형식으로 직접 html 내용안에 넣으실 수 있습니다.。</li>
    </ul>
    </ul>
  </div>
  <textarea id="module_html_editor" name="module_html_editor" style=" width:1016px; height:400px; visibility:hidden;"></textarea>
  <div class="bottom">
    <label class="submit-border"><a id="btn_save_module_html" class="submit" href="javascript:void(0);">저장</a></label>
  </div>
</div>
<!-- 幻灯模块编辑对话框 -->
<div id="dialog_module_slide" class="eject_con dialog-decoration-edit" style="display:none;">
  <div class="alert">
    <ul>
      <li>1. 이미지 풀스크린 여부를 선택하고, <strong class="orange">사진의 높이를 꼭 설정해 주세요.</strong>그렇지않으면 표시가 불가능합니다.</li>
      <li>2. 단일 업로드 업로드시 기본적으로<strong>“이미지링크”</strong>형식으로 나타나게되며, 여러장의 이미지 업로드시<strong>“슬라이드”</strong>형식으로 나타납니다.</li>
    </ul>
  </div>
  <div id="module_slide_html" class="slide-upload-thumb">
    <ul class="module-slide-content">
    </ul>
  </div>
  <h4>관련설정：</h4>
  <dl class="display-set">
    <dt>노출여부：</dt>
    <dd><span>풀스크린
      <input id="txt_slide_full_width" type="checkbox" class="checkobx" name="">
      </span><span><strong class="orange">*</strong> 높이
      <input id="txt_slide_height" type="text" class="text w40" value=""><em class="add-on">px</em></span>
      <p><a id="btn_add_slide_image" class="ncsc-btn mt5" href="javascript:void(0);"><i class="icon-plus"></i>이미지추가</a></p>
    </dd>
  </dl>
  <div id="div_module_slide_upload" style="display:none;">
    <form action="">
      <dl>
        <dt>업로드：</dt>
        <dd>
          <div id="div_module_slide_image" class="module-upload-image-preview"></div>
          <div class="ncsc-upload-btn"> <a href="javascript:void(0);"> <span>
            <input type="file" hidefocus="true" size="1" class="input-file" name="file" id="file"  cnbiztype="btn_module_slide_upload"/>
            </span>
            <p><i class="icon-upload-alt"></i>업로드</p>
            </a> </div>
          <p class="hint">너비 1000px의 JPG/GIF/PNG형식 파일</p>
        </dd>
      </dl>
      <dl>
        <dt>이미지링크：</dt>
        <dd>
          <input id="module_slide_url" class="text w400" type="text">
          <p class="hint">http:// 로시작되는 링크를 입력해주세요. (선택사항)</p>
          <p class="mt5"><a id="btn_save_add_slide_image" class="ncsc-btn ncsc-btn-acidblue" href="javascript:void(0);">추가</a> <a id="btn_cancel_add_slide_image" class="ncsc-btn ncsc-btn-orange" href="javascript:void(0);">취소</a></p>
        </dd>
      </dl>
    </form>
  </div>
  <div class="bottom">
    <label class="submit-border"><a id="btn_save_module_slide" class="submit" href="javascript:void(0);">저장</a></label>
  </div>
</div>
<!-- 图片热点模块编辑对话框 -->
<div id="dialog_module_hot_area" class="eject_con dialog-decoration-edit" style="display:none;">
  <div class="alert">
    <ul>
      <li>1. 업로드된 이미지 범위내에서 마우스를 그래그하여 구간을 선택하시고,<br>그 구역 안에 http://형식의 링크주소를 추가하신 후, "링크추가"를 클릭하여 완료해 주세요.</li>
      <li>2. 이미 추가한 링크주소를 수정하실 수 있으며, 링크구역을 수정 하실 때에는<br>삭제->저장 후 다시 추가하여 주시면 됩니다.</li>
    </ul>
  </div>
  <div id="div_module_hot_area_image" class="hot-area-image" style="position: relative;"></div>
  <ul id="module_hot_area_select_list" class="hot-area-select-list">
  </ul>
  <h4>기타설정：</h4>
  <form action="">
    <dl>
      <dt>업로드：</dt>
      <dd>
        <div class="ncsc-upload-btn"> <a href="javascript:void(0);"> <span>
          <input type="file" hidefocus="true" size="1" class="input-file" name="file" id="file"  cnbiztype="btn_module_hot_area_upload"/>
          </span>
          <p><i class="icon-upload-alt"></i>업로드</p>
          </a> </div>
        <p class="hint">JPG/GIF/PNG형식 파일/ 높이 최대 400px 의 범위내로 자유롭게 자르실 수 있습니다.。</p>
      </dd>
    </dl>
  </form>
  <dl>
    <dt>링크구역설정：</dt>
    <dd>
      <input id="module_hot_area_url" class="text w400" type="text" />
      <a id="btn_module_hot_area_add" class="ncsc-btn ml5" href="javascript:void(0);"><i class="icon-anchor"></i>링크추가</a>
      <p class="hint">링크구역을 클릭하면 이동하게 될 "http://"형식의 주소를 입력해 주세요.</p>
    </dd>
  </dl>
  <div class="bottom">
    <label class="submit-border"><a id="btn_save_module_hot_area" class="submit" href="javascript:void(0);">저장</a></label>
  </div>
</div>
<!-- 商品模块编辑对话框 -->
<div id="dialog_module_goods" class="eject_con dialog-decoration-edit" style="display:none;">
  <div class="alert">
    <ul>
      <li>1. 미니샵에서 판매하고 있는 상품을 검색하셔서 "추가"하여 주세요. "저장"을 눌러주셔야 완료됩니다.</li>
      <li>2. 10개 이상의 상품을 선택하였을 경우, 상단에는 선택항목이 다 보여지지 않지만, 추가된 항목들에<br>마우스를 올리시고 밑으로 내리시면 나머지 선택된 항목들을 보실 수 있습니다. </li>
    </ul>
  </div>
  <div id="decorationGoods">
    <ul id="div_module_goods_list" class="goods-list">
    </ul>
  </div>
  <h4 class="mt10">진열상품 선택</h4>
  <div class="decoration-search-goods">
    <div class="search-bar">상품키워드：
      <input id="txt_goods_search_keyword" type="text" class="text w200 vm" name="">
      <a id="btn_module_goods_search" class="ncsc-btn" href="javascript:void(0);">검색</a><span class="ml10 orange">TIP： 키워드를 입력하지 않고 검색을 클릭하시면 판매중인 모든상품의 리스트가 나타납니다.</span></div>
    <div id="div_module_goods_search_list"></div>
  </div>
  <div class="bottom"><label class="submit-border"><a id="btn_save_module_goods" class="submit" href="javascript:void(0);">저장</a></label></div>
</div>
<!-- 幻灯模板 --> 
<script id="template_module_slide_image_list" type="text/html">
<li data-image-name="<%=image_name%>" data-image-url="<%=image_url%>" data-image-link="<%=image_link%>">
<span><img src="<%=image_url%>"></span>
<a cnbiztype="btn_del_slide_image" href="javascript:void(0);" title="삭제">X</a>
</li>
</script> 
<!-- 热点块控制模板 --> 
<script id="template_module_hot_area_list" type="text/html">
<li data-hot-area-link="<%=link%>" data-hot-area-position="<%=position%>">
<i></i>
<p>링크구역<%=index%></p>
<p><a cnbiztype="btn_module_hot_area_select" data-hot-area-position="<%=position%>" class="ncsc-btn-mini ncsc-btn-acidblue" href="javascript:void(0);">선택</a>
<a data-index="<%=index%>" cnbiztype="btn_module_hot_area_del" class="ncsc-btn-mini ncsc-btn-red" href="javascript:void(0);">삭제</a></p>
</li>
</script> 
<!-- 热点块标识模板 --> 
<script id="template_module_hot_area_display" type="text/html">
<div class="store-decoration-hot-area-display" style="width:<%=width%>px;height:<%=height%>px;position:absolute;left:<%=left%>px;top:<%=top%>px;border:1px solid #cccccc;" id="hot_area_display_<%=index%>">링크구역<%=index%></div>
</li>
</script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/template.min.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/kindeditor/kindeditor-min.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/kindeditor/lang/zh_CN.js" charset="utf-8"></script>
<link media="all" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/imgareaselect-animated.css" type="text/css" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/jquery.imgareaselect.min.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script> 
<script type="text/javascript"> 
    //定义api常量
    var DECORATION_ID = <?php echo $_GET['decoration_id'];?>;
    var URL_DECORATION_ALBUM_UPLOAD = '<?php echo urlShop('store_decoration', 'decoration_album_upload');?>';
    var URL_DECORATION_BACKGROUND_SETTING_SAVE = '<?php echo urlShop('store_decoration', 'decoration_background_setting_save');?>';
    var URL_DECORATION_NAV_SAVE = '<?php echo urlShop('store_decoration', 'decoration_nav_save');?>';
    var URL_DECORATION_BANNER_SAVE = '<?php echo urlShop('store_decoration', 'decoration_banner_save');?>';
    var URL_DECORATION_BLOCK_ADD = '<?php echo urlShop('store_decoration', 'decoration_block_add');?>';
    var URL_DECORATION_BLOCK_DEL = '<?php echo urlShop('store_decoration', 'decoration_block_del');?>';
    var URL_DECORATION_BLOCK_SAVE = '<?php echo urlShop('store_decoration', 'decoration_block_save');?>';
    var URL_DECORATION_BLOCK_SORT = '<?php echo urlShop('store_decoration', 'decoration_block_sort');?>';
    var URL_DECORATION_GOODS_SEARCH = '<?php echo urlShop('store_decoration', 'goods_search');?>';
    var LOADING_IMAGE = '<?php echo SHOP_TEMPLATES_URL . DS . 'images/loading.gif';?>';
    var POSHYTIP = {
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'top',
        alignY: 'left',
        offsetX: -300,
        offsetY: -5,
        allowTipHover: false
    };


    $(document).ready(function(){
        //浮动导航  waypoints.js
        $("#waypoints").waypoint(function(event, direction) {
            $(this).parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });

        //商品模块已选商品滚动条
        $('#decorationGoods').perfectScrollbar();

		//title提示
    	$('.tip').poshytip(POSHYTIP);
    });		

</script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/decoration/store_decoration.js" charset="utf-8"></script> 
