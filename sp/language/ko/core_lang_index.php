<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * core简体语言包
 */
$lang['please_check_your_url_arg'] = 'URL주소를 확인하세요, 오류코드：';

$lang['error_info'] = '오류정보';
$lang['solution'] = '해결방안';
$lang['please_login_in'] = '로그인하세요';
$lang['CnBiz_bbs'] = '공식사이트';
$lang['found_answer'] = ', 문제 발생시 우리에게 연락주세요.';
$lang['company_name'] = '韩中友谊';

$lang['please_check_your_system_chmod'] = '시스템 설정 정보를 캐쉬파일에 쓰기 불가입니다, 폴더 권한 확인 후 다시 시도해주세요!';
$lang['please_check_your_system_chmod_area'] = '지역 파일 쓰기 불가입니다, 폴더 권한 확인 후 다시 시도해주세요!';
$lang['please_check_your_cache_type'] = '캐쉬 유형이 옳바른지 확인해주세요!';
$lang['please_check_your_system_chmod_goods'] = '상품 유형을 캐쉬파일에 쓰기 불가입니다, 폴더 권한 확인 후 다시 시도해주세요!';
$lang['please_check_your_system_chmod_ad'] = '광고정보를 캐쉬 파일에 쓰기 불가입니다, 폴더 권한 확인 후 다시 시도해주세요!';
$lang['please_check_your_system_chmod_adv'] = '광고위치 정보를 캐쉬 파일에 쓰기 불가입니다, 폴더 권한 확인 후 다시 시도해주세요!';


$lang['first_page'] = '처음';
$lang['last_page'] = '끝';
$lang['pre_page'] = '이전';
$lang['next_page'] = '다음';

$lang['cant_find_temporary_files'] = '임시 파일을 찾을 수 없습니다, 임시 폴더가 존재 하는지 및 쓰기가 가능한지 확인하세요';
$lang['upload_file_size_none'] = '파일 유형이 아닙니다.';
$lang['upload_file_size_cant_over'] = '업로드 최대 용량';
$lang['upload_file_fail'] = '파일 업로드 실패: 카피 설정 권한이 없습니다.';
$lang['upload_file_size_over'] = '파일 크기가 시스템 설정을 초과하였습니다.';
$lang['upload_file_is_not_complete'] = '일부 파일이 업로드 되었습니다.';
$lang['upload_file_is_not_uploaded'] = '업로드 된 파일이 없습니다.';
$lang['upload_dir_chmod'] = '임시 폴더를 찾을 수 없습니다.';
$lang['upload_file_write_fail'] = '파일 읽기 실패';
$lang['upload_file_mkdir'] = '폴더 만들기(';
$lang['upload_file_mkdir_fail'] = ')실패';
$lang['upload_file_dir'] = '디렉토리(';
$lang['upload_file_dir_cant_touch_file'] = ')에 파일을 생성할 수 없습니다, 권한 확인 후 다시 시도해주세요';

$lang['upload_image_px'] = '픽셀';
$lang['image_allow_ext_is'] = '허용되지 않은 확장명 파일입니다, 허용유형: ';
$lang['upload_image_is_not_image'] = '이미지 파일이 아닙니다.';
$lang['upload_image_mime_error'] = '이미지 파일 유형이 아닙니다.';
$lang['upload_file_attack'] = '허용되지 않은 파일입니다.';

$lang['order_state_cancel'] = '취소됨';
$lang['order_state_new'] = '결제대기';
$lang['order_state_pay'] = '발송대기';
$lang['order_state_send'] = '수취대기';
$lang['order_state_success'] = '거래완료';
$lang['order_state_eval'] = '평가됨';








