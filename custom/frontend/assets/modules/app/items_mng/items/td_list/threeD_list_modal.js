(function($, angular, _, APP){

  var tableComponentDirName = 'threedTableModal';



  window.bootstrapThreeDList = bootstrapThreeDList;

  function bootstrapThreeDList(){

    angular.module('App')
      .directive( tableComponentDirName, tableComponentThreeDDirective );

    angular.module('App')
      .directive( 'tableSearch', tableSearchThreeDDirective );

    angular.module('App')
      .directive( 'tableBody', tableBodyThreeDDirective );

    angular.module('App')
      .directive( 'tdPrevModalSmall', tdPrevModalSmall );


    angular.bootstrap( $('html'), ['App']);
      console.log('bootstrap init');

  }



  function tableComponentThreeDDirective(XHR, ItemsTableManager, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.ctrl.tableComponentThreeDModalCtrl,
      controllerAs: 'tableComponentThreeDCtrl',
      bindToController: APP.modules.ctrl.tableComponentThreeDModalCtrl.bindToController,
      link: APP.modules.ctrl.tableComponentThreeDModalCtrl.link(PubSub),
      templateUrl: 'table_component_basic.html'
    };
  }
  tableComponentThreeDDirective.$inject =['XHR', 'ItemsTableManager', 'PubSub', 'URL_INFO'];



  function tableSearchThreeDDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableSearchThreeDModalCtrl,
      controllerAs: 'tableSearchCtrl',
      bindToController: APP.modules.ctrl.tableSearchThreeDModalCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tableSearchThreeDModalCtrl.link(PubSub),
      templateUrl: 'table_search_threeD_popup.html'
    };
  }
  tableSearchThreeDDirective.$inject =['XHR', 'PubSub', 'URL_INFO'];


  function tableBodyThreeDDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableBodyThreeDModalCtrl,
      controllerAs: 'tableBodyCtrl',
      bindToController: APP.modules.ctrl.tableBodyThreeDModalCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tableBodyThreeDModalCtrl.link(PubSub),
      templateUrl: 'table_body_threeD_popup.html'
    };
  }
  tableBodyThreeDDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];


  function tdPrevModalSmall(XHR, PubSub, URL_INFO){

    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tdPrevModalSmallCtrl,
      controllerAs: 'tdPrevModalCtrl',
      bindToController: APP.modules.ctrl.tdPrevModalSmallCtrl.bindToController,
      link: APP.modules.ctrl.tdPrevModalSmallCtrl.link(),
      templateUrl: 'td_prev_modal_small.html'
    };

  }
  tdPrevModalSmall.$inject = ['XHR', 'PubSub', 'URL_INFO'];



})(window.jQuery, window.angular, window._, window.APP);
