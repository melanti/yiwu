<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>고객센터</h3>
      <ul class="tab-base">
        <li><a href="<?php echo urlAdmin('mall_consult', 'index');?>"><span>고객센터 문의 리스트</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>플랫폼 문의유형</span></a></li>
        <li><a href="<?php echo urlAdmin('mall_consult', 'type_add');?>"><span>유형추가</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
          <li>구매자가 고객센터와 연락시 선택되는 유형.</li>
          <li>문의시 유형은 필수선택 사항입니다.</li>
        </ul></td>
      </tr>
    </tbody>
  </table>
  <form method="post" name="form1">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <?php if(!empty($output['type_list'])){ ?>
      <thead>
        <tr class="thead">
          <th class="w24"></th>
          <th class="w48 sort">정렬</th>
          <th>문의유형명</th>
          <th class="w96 align-center">조작</th>
        </tr>
      </thead>
      <?php foreach($output['type_list'] as $value){ ?>
      <tbody>
        <tr>
          <td><input type="checkbox" class="checkitem" value="<?php echo $value['mct_id'];?>" name="del_id[]" /></td>
          <td><?php echo $value['mct_sort'];?></td>
          <td><?php echo $value['mct_name'];?></td>
          <td class="align-center"><a href="<?php echo urlAdmin('mall_consult', 'type_edit', array('mct_id' => $value['mct_id']));?>">수정</a>&nbsp;|&nbsp;<a href="<?php echo urlAdmin('mall_consult', 'type_del', array('mct_id' => $value['mct_id']));?>">삭제</a></td>
        </tr>
      </tbody>
      <?php }?>
      <?php }else{?>
      <tbody>
        <tr class="no_data">
          <td colspan="20"><?php echo $lang['nc_no_record'];?></td>
        </tr>
      </tbody>
      <?php }?>
      <tfoot>
        <?php if(!empty($output['type_list'])){?>
        <tr class="tfoot">
          <td><input type="checkbox" class="checkall" id="checkallBottom"></td>
          <td colspan="16"><label for="checkallBottom"><?php echo $lang['nc_select_all']; ?></label>
            &nbsp;&nbsp;<a href="JavaScript:void(0);" class="btn" onclick="document.form1.submit()"><span><?php echo $lang['nc_del'];?></span></a>
          </td>
        </tr>
        <?php }?>
      </tfoot>
    </table>
  </form>
</div>

