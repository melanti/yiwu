<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>SCM 로그인</title>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<link href="/sp/templates/default/css/login.css" rel="stylesheet" type="text/css">

<script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js" type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.tscookie.js" type="text/javascript" type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>

<script language="JavaScript" type="text/javascript">
$(document).ready(function() {
  //得到焦点
  $("#password").focus(function(){
    $("#left_hand").animate({
      left: "150",
      top: " -38"
    },{step: function(){
      if(parseInt($("#left_hand").css("left"))>140){
        $("#left_hand").attr("class","left_hand");
      }
    }}, 2000);
    $("#right_hand").animate({
      right: "-64",
      top: "-38px"
    },{step: function(){
      if(parseInt($("#right_hand").css("right"))> -70){
        $("#right_hand").attr("class","right_hand");
      }
    }}, 2000);
  });
  //失去焦点
  $("#password").blur(function(){
    $("#left_hand").attr("class","initial_left_hand");
    $("#left_hand").attr("style","left:100px;top:-12px;");
    $("#right_hand").attr("class","initial_right_hand");
    $("#right_hand").attr("style","right:-112px;top:-12px");
  });

    //인증번호 변경
    function change_seccode() {
        $('#codeimage').attr('src', 'index.php?act=seccode&op=makecode&nchash=<?php echo $output['nchash'];?>&t=' + Math.random());
        $('#captcha').select();
    }

    $('[cbtype="btn_change_seccode"]').on('click', function() {
        change_seccode();
    });

    //登陆表单验证
    $("#form_login").validate({
        errorPlacement:function(error, element) {
            element.prev(".repuired").append(error);
        },
        onkeyup: false,
        rules:{
            seller_name:{
                required:true
            },
            password:{
                required:true
            },
            captcha:{
                required:true,
                remote:{
                    url:"index.php?act=seccode&op=check&nchash=<?php echo $output['nchash'];?>",
                    type:"get",
                    data:{
                        captcha:function() {
                            return $("#captcha").val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                            change_seccode();
                        }
                    }
                }
            }
        },
        messages:{
            seller_name:{
                required:"<i class='icon-exclamation-sign'></i>아이디를 입력하세요"
            },
            password:{
                required:"<i class='icon-exclamation-sign'></i>비밀번호를 입력하세요"
            },
            captcha:{
                required:"<i class='icon-exclamation-sign'></i>인증번호를 입력하세요",
                remote:"<i class='icon-frown'></i>인증번호가 틀렸습니다"
            }
        }
    });
  //Hide Show verification code
    $("#hide").click(function(){
        $(".code").fadeOut("slow");
    });
    $("#captcha").focus(function(){
        $(".code").fadeIn("fast");
    });

});
</script>
</head>
<body>
    <div class="top_div">      
    </div>
    <div class="s_login">
      <h2 style="background:#e44b50; padding: 10px 0 10px 0; color:white">SCM - 판매관리자</h2>
<!--       <div class="ani">
        <div class="tou"></div>
        <div class="initial_left_hand" id="left_hand"></div>
        <div class="initial_right_hand" id="right_hand"></div>
      </div> -->
      <form id="form_login" action="index.php?act=seller_login&op=login" method="post" >
        <?php Security::getToken();?>
        <input name="nchash" type="hidden" value="<?php echo $output['nchash'];?>" />
        <input type="hidden" name="form_submit" value="ok" />        
          <p class="mailid">
            <span class="u_logo"></span>
            <input class="ipt" type="text" name="seller_name" autocomplete="off" placeholder="아이디" autofocus value="demo"> 
              </p>
          <p class="passwd">
            <span class="p_logo"></span>         
            <input class="ipt" id="password" type="password" name="password" autocomplete="off" placeholder="비밀번호" value="123456">   
          </p>

          <div class="s_login_btn">
                    <div class="submit"><span>
          <div class="code">
                <div class="code-img"><a href="javascript:void(0)" cbtype="btn_change_seccode"><img src="index.php?act=seccode&op=makecode&nchash=<?php echo $output['nchash'];?>" name="codeimage" border="0" id="codeimage"></a></div>
                <a href="JavaScript:void(0);" id="hide" class="close" title="<?php echo $lang['login_index_close_checkcode'];?>"><i></i></a> <a href="JavaScript:void(0);" class="change" cbtype="btn_change_seccode" title="<?php echo $lang['login_index_change_checkcode'];?>"><i></i></a> </div>
          <p class="fl">
                <input type="text" name="captcha" id="captcha" autocomplete="off" placeholder="인증코드" class="codeipt" maxlength="4" size="10" />
              </p>
              <p class="fr">
                        <input type="submit" class="l_btn" value="로그인">
                      </p>
                         </span>
             </div>
            </div>
           
      </form>
        <div class="bottom">
    <h5>&copy; 2015-<?php echo date("Y",time());?> <?php echo C('site_name');?>. All rights reserved.</h5>
  </div>

      </div>


      

</body>
</html>
