<div class="eject_con">
<div id="warning"></div>
<?php if ($output['order_info']) {?>

  <form id="changeform" method="post" action="index.php?act=store_deliver&op=delay_receive&order_id=<?php echo $output['order_info']['order_id']; ?>">
    <input type="hidden" name="form_submit" value="ok" />
    <dl>
      <dt><?php echo '주문번호'.$lang['nc_colon'];?></dt>
      <dd><span class="num"><?php echo $output['order_info']['order_sn']; ?></span></dd>
    </dl>
    <dl>
      <dt><?php echo '구매자'.$lang['nc_colon'];?></dt>
      <dd><?php echo $output['order_info']['buyer_name']; ?></dd>
    </dl>
    <dl>
      <dt><?php echo '최대도착시간'.$lang['nc_colon'];?></dt>
      <dd><?php echo date('Y-m-d H:i:s',$output['order_info']['delay_time']);?><br/>만약 해당 시간이 지나서도 구매자가 "상품수령"을 확인하지 않았을시, 시스템에서 자동으로 수령 상태로 바뀌게 됩니다.</dd>
    </dl>
    <dl>
      <dt><?php echo '연기'.$lang['nc_colon'];?></dt>
      <dd>
      <select name="delay_date">
      <option value="5">5</option>
      <option value="10">10</option>
      <option value="15">15</option>
      </select> 일
      </dd>
    </dl>
    <div class="bottom">
        <label class="submit-border"><input type="submit" class="submit" id="confirm_button" value="확인" /></label>
    </div>
  </form>
<?php } else { ?>
<p style="line-height:80px;text-align:center">해당 주문이 존재하지 않습니다. 번호를 정확히 입력해주세요.</p>
<?php } ?>
</div>
<script type="text/javascript">
$(function(){
    $('#changeform').validate({
    	errorLabelContainer: $('#warning'),
        invalidHandler: function(form, validator) {
           var errors = validator.numberOfInvalids();
           if(errors){ $('#warning').show();}else{ $('#warning').hide(); }
        },
     	submitHandler:function(form){
    		ajaxpost('changeform', '', '', 'onerror'); 
    	},    
	    rules : {
        	order_amount : {
	            required : true,
	            number : true
	        }
	    },
	    messages : {
	    	order_amount : {
	    		required : '<?php echo $lang['store_order_modify_price_gpriceerror'];?>',
            	number : '<?php echo $lang['store_order_modify_price_gpriceerror'];?>'
	        }
	    }
	});
});
</script>