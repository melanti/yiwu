<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<!-- S setp -->
<?php if ($output['edit_goods_sign']) {?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<?php } else {?>
<ul class="add-goods-step">
  <li class="current"><i class="icon icon-list-alt"></i>
    <h6>STEP.1</h6>
    <h2>EXCEL파일 등록</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-ok-circle"></i>
    <h6>STEP.2</h6>
    <h2>등록 완료</h2>
  </li>
</ul>
<?php }?>
<!--S 分类选择区域분류선택구역-->
<div class="wrapper_search">
<form method="post" action="index.php?act=store_goods_import&op=add_step_two" enctype="multipart/form-data">
  <div class="wp_sort" style="height:100% !important;">
         <input  type="file" name="file_stu" style="width:700px; border:0px; height:40px; line-height:40px;" />
  </div>
  <div class="wp_confirm">
      <div class="bottom">
      <label class="submit-border"><input type="submit"  value="상품 등록하기" class="submit"style=" width: 200px;" /></label>
      </div>
  </div>
</form>
</div>
