var SiteUrl = "http://"+window.location.host;
var ApiUrl =  "http://"+window.location.host+"/mobile";
var pagesize = 10;
var WapSiteUrl ="http://"+window.location.host+"/wap";
var IOSSiteUrl = "http://"+window.location.host+"/wap";
var AndroidSiteUrl = "http://"+window.location.host+"/wap";

// auto url detection
(function() {
    var m = /^(https?:\/\/.+)\/wap/i.exec(location.href);
    if (m && m.length > 1) {
        SiteUrl = m[1] + '/';
        ApiUrl = m[1] + '/mobile';
        WapSiteUrl = m[1] + '/wap';
    }
})();
