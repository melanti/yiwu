var moment = require('moment');
var network = require('../controllers/libs/network.js');
module.exports = {
    logRequest: _logRequest
};

function _logRequest(req, res) {
    var route = _.extend({},{},sails.config.routes[req.method.toUpperCase() + ' ' + req.url.split('?')[0]]);
    if(!route.controller) {
        route = _.extend({},{},sails.config.routes[req.url.split('?')[0]]);
    }
    sails.log.debug('===========[New request!!]===========');
    sails.log.debug('time :', moment().format('YYYY-MM-DD HH:mm:ss dddd'));
    sails.log.debug('url :', req.url);
    sails.log.debug('method :', req.method);
    sails.log.debug('ip :', network.get_ip(req));
    sails.log.debug('controller :', route.controller);
    sails.log.debug('action :', route.action);
    sails.log.debug('content-length :', req.headers["content-length"]);
    sails.log.debug('content-type :', req.headers["content-type"]);
    sails.log.debug('params :', JSON.stringify(req.allParams(), null, 4));
    sails.log.debug('=====================================\n\n');
}