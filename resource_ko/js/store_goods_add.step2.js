$(function(){
    // 취소回车提交表单
    $('input').keypress(function(e){
        var key = window.event ? e.keyCode : e.which;
        if (key.toString() == "13") {
         return false;
        }
    });
    // 添加店铺分类
    $("#add_sgcategory").unbind().click(function(){
        $(".sgcategory:last").after($(".sgcategory:last").clone(true).val(0));
    });
    // 选择店铺分类
    $('.sgcategory').unbind().change( function(){
        var _val = $(this).val();       // 记录选择的值
        $(this).val('0');               // 已选择值清零
        // 验证是否已经选择
        if (!checkSGC(_val)) {
            alert('이미 선택된 카테고리입니다, 다른 카테고리를 선택하세요');
            return false;
        }
        $(this).val(_val);              // 重新赋值
    });
    
    /* 商品图片ajax上传 */
    $('#goods_image').fileupload({
        dataType: 'json',
        url: SITEURL + '/index.php?act=store_goods_add&op=image_upload&upload_type=uploadedfile',
        formData: {name:'goods_image'},
        add: function (e,data) {
        	$('img[cnbiztype="goods_image"]').attr('src', SHOP_TEMPLATES_URL + '/images/loading.gif');
            data.submit();
        },
        done: function (e,data) {
            var param = data.result;
            if (typeof(param.error) != 'undefined') {
                $('img[cnbiztype="goods_image"]').attr('src',DEFAULT_GOODS_IMAGE);
            } else {
                $('input[cnbiztype="goods_image"]').val(param.name);
                $('img[cnbiztype="goods_image"]').attr('src',param.thumb_name);
            }
        }
    });

    /* ajax打开图片空间 */
    // 商品主图使用
    $('a[cnbiztype="show_image"]').unbind().ajaxContent({
        event:'click', //mouseover
        loaderType:"img",
        loadingMsg:SHOP_TEMPLATES_URL+"/images/loading.gif",
        target:'#demo'
    }).click(function(){
        $(this).hide();
        $('a[cnbiztype="del_goods_demo"]').show();
    });
    $('a[cnbiztype="del_goods_demo"]').unbind().click(function(){
        $('#demo').html('');
        $(this).hide();
        $('a[cnbiztype="show_image"]').show();
    });
    // 商品描述使用
    $('a[cnbiztype="show_desc"]').unbind().ajaxContent({
        event:'click', //mouseover
        loaderType:"img",
        loadingMsg:SHOP_TEMPLATES_URL+"/images/loading.gif",
        target:'#des_demo'
    }).click(function(){
        $(this).hide();
        $('a[cnbiztype="del_desc"]').show();
    });
    //ko
    $('a[cnbiztype="show_desc_ko"]').unbind().ajaxContent({
        event:'click', //mouseover
        loaderType:"img",
        loadingMsg:SHOP_TEMPLATES_URL+"/images/loading.gif",
        target:'#des_demo_ko'
    }).click(function(){
        $(this).hide();
        $('a[cnbiztype="del_desc_ko"]').show();
    });


    $('a[cnbiztype="del_desc"]').click(function(){
        $('#des_demo').html('');
        $(this).hide();
        $('a[cnbiztype="show_desc"]').show();
    });
    //ko
    $('a[cnbiztype="del_desc_ko"]').click(function(){
        $('#des_demo_ko').html('');
        $(this).hide();
        $('a[cnbiztype="show_desc_ko"]').show();
    });


    $('#add_album').fileupload({
        dataType: 'json',
        url: SITEURL+'/index.php?act=store_goods_add&op=image_upload',
        formData: {name:'add_album'},
        add: function (e,data) {
            $('i[cnbiztype="add_album_i"]').removeClass('icon-upload-alt').addClass('icon-spinner icon-spin icon-large').attr('data_type', parseInt($('i[cnbiztype="add_album_i"]').attr('data_type'))+1);
            data.submit();
        },
        done: function (e,data) {
            var _counter = parseInt($('i[cnbiztype="add_album_i"]').attr('data_type'));
            _counter -= 1;
            if (_counter == 0) {
                $('i[cnbiztype="add_album_i"]').removeClass('icon-spinner icon-spin icon-large').addClass('icon-upload-alt');
                $('a[cnbiztype="show_desc"]').click();
            }
            $('i[cnbiztype="add_album_i"]').attr('data_type', _counter);
        }
    });

    //ko
    $('#add_album_ko').fileupload({
        dataType: 'json',
        url: SITEURL+'/index.php?act=store_goods_add&op=image_upload',
        formData: {name:'add_album_ko'},
        add: function (e,data) {
            $('i[cnbiztype="add_album_i_ko"]').removeClass('icon-upload-alt').addClass('icon-spinner icon-spin icon-large').attr('data_type', parseInt($('i[cnbiztype="add_album_i_ko"]').attr('data_type'))+1);
            data.submit();
        },
        done: function (e,data) {
            var _counter = parseInt($('i[cnbiztype="add_album_i_ko"]').attr('data_type'));
            _counter -= 1;
            if (_counter == 0) {
                $('i[cnbiztype="add_album_i_ko"]').removeClass('icon-spinner icon-spin icon-large').addClass('icon-upload-alt');
                $('a[cnbiztype="show_desc_ko"]').click();
            }
            $('i[cnbiztype="add_album_i_ko"]').attr('data_type', _counter);
        }
    });

    /* ajax打开图片空间 end */
    
    // 商品属性
    attr_selected();
    $('select[cbtype="attr_select"]').change(function(){
        id = $(this).find('option:selected').attr('cbtype');
        name = $(this).attr('attr').replace(/__NC__/g,id);
        $(this).attr('name',name);
    });
    
    // 修改规格名称
    $('dl[cnbiztype="spec_group_dl"]').on('click', 'input[type="checkbox"]', function(){
        pv = $(this).parents('li').find('span[cnbiztype="pv_name"]');
        if(typeof(pv.find('input').val()) == 'undefined'){
            pv.html('<input type="text" maxlength="20" class="text" value="'+pv.html()+'" />');
        }else{
            pv.html(pv.find('input').val());
        }
    });
    
    $('span[cnbiztype="pv_name"] > input').live('change',function(){
        change_img_name($(this));       // 修改相关的颜色名称
        into_array();           // 将选中的规格放入数组
        goods_stock_set();      // 生成库存配置
    });
    

    // 运费部分显示隐藏
    $('input[cnbiztype="freight"]').click(function(){
            $('input[cnbiztype="freight"]').nextAll('div[cnbiztype="div_freight"]').hide();
            $(this).nextAll('div[cnbiztype="div_freight"]').show();
    });
    
    // 商品所在地
    var area_select = $("#province_id");
    areaInit(area_select,0);//初始化地区
    $("#province_id").change(function (){
        // 삭제后面的select
        $(this).nextAll("select").remove();
        if (this.value > 0){
            var text = $(this).get(0).options[$(this).get(0).selectedIndex].text;
            var area_id = this.value;
            var EP = new Array();
            EP[1]= true;EP[2]= true;EP[9]= true;EP[22]= true;EP[34]= true;EP[35]= true;
            if(typeof(nc_a[area_id]) != 'undefined'){//数组存在
                var areas = new Array();
                var option = "";
                areas = nc_a[area_id];
            if (typeof(EP[area_id]) == 'undefined'){
                option = "<option value='0'>"+text+"(*)</option>";
            }
            $("<select name='city_id' id='city_id'>"+option+"</select>").insertAfter(this);
                for (var i = 0; i <areas.length; i++){
                    $(this).next("select").append("<option value='" + areas[i][0] + "'>" + areas[i][1] + "</option>");
                }
            }
        }
     });
    
    // 定时发布时间
    $('#starttime').datepicker({dateFormat: 'yy-mm-dd'});
    $('input[name="g_state"]').click(function(){
        if($(this).attr('cnbiztype') == 'auto'){
            $('#starttime').removeAttr('disabled').css('background','');
            $('#starttime_H').removeAttr('disabled').css('background','');
            $('#starttime_i').removeAttr('disabled').css('background','');
        }else{
            $('#starttime').attr('disabled','disabled').css('background','#E7E7E7 none');
            $('#starttime_H').attr('disabled','disabled').css('background','#E7E7E7 none');
            $('#starttime_i').attr('disabled','disabled').css('background','#E7E7E7 none');
        }
    });
    
    // 计算折扣
    $('input[name="g_price_ko"],input[name="g_marketprice_ko"]').change(function(){
        discountCalculator();
    });
    
    /* AJAX添加规格值 */
    // 添加规格
    $('a[cnbiztype="specAdd"]').click(function(){
        var _parent = $(this).parents('li:first');
        _parent.find('div[cnbiztype="specAdd1"]').hide();
        _parent.find('div[cnbiztype="specAdd2"]').show();
        _parent.find('input').focus();
    });
    // 취소
    $('a[cnbiztype="specAddCancel"]').click(function(){
        var _parent = $(this).parents('li:first');
        _parent.find('div[cnbiztype="specAdd1"]').show();
        _parent.find('div[cnbiztype="specAdd2"]').hide();
        _parent.find('input').val('');
    });
    // 提交
    $('a[cnbiztype="specAddSubmit"]').click(function(){
        var _parent = $(this).parents('li:first');
        eval('var data_str = ' + _parent.attr('data-param'));
        var _input = _parent.find('input');
        _parent.find('div[cnbiztype="specAdd1"]').show();
        _parent.find('div[cnbiztype="specAdd2"]').hide();
        $.getJSON(data_str.url, {gc_id : data_str.gc_id , sp_id : data_str.sp_id , name : _input.val()}, function(data){
            if (data.done) {
                _parent.before('<li><span cnbiztype="input_checkbox"><input type="checkbox" name="sp_val[' + data_str.sp_id + '][' + data.value_id + ']" cbtype="' + data.value_id + '" value="' +_input.val()+ '" /></span><span cnbiztype="pv_name">' + _input.val() + '</span></li>');
                _input.val('');
            }
        });
    });
    // 修改规格名称
    $('input[cnbiztype="spec_name"]').change(function(){
        eval('var data_str = ' + $(this).attr('data-param'));
        if ($(this).val() == '') {
            $(this).val(data_str.name);
        }
        $('th[cnbiztype="spec_name_' + data_str.id + '"]').html($(this).val());
    });
    // 批量设置价格、库存、预警值
    $('.batch > .icon-edit').click(function(){
        $('.batch > .batch-input').hide();
        $(this).next().show();
    });
    $('.batch-input > .close').click(function(){
        $(this).parent().hide();
    });
    $('.batch-input > .ncsc-btn-mini').click(function(){
        var _value = $(this).prev().val();
        var _type = $(this).attr('data-type');
        if (_type == 'price_ko') {
            _value = parseInt(_value);  //number_format(_value, 2);
        }else if (_type == 'weight') {
            _value = parseFloat(_value);  //number_format(_value, 2);
        } else {
            _value = parseInt(_value);
        }
        if (isNaN(_value)) {
            _value = 0;
        }
        if (_type == 'price_ko') {
            $('input[data_type="' + _type + '" ]').val(_value);
            $('input[data_type="price" ]').val(Math.ceil(_value/SYS_CUR));
        }
        else if (_type == 'marketprice_ko') {
            $('input[data_type="' + _type + '" ]').val(_value);
            $('input[data_type="marketprice" ]').val(Math.ceil(_value/SYS_CUR));
        }
        else if (_type == 'costprice_ko') {
            $('input[data_type="' + _type + '" ]').val(_value);
            $('input[data_type="costprice" ]').val(Math.ceil(_value/SYS_CUR));
        }
        else
        {
            $('input[data_type="' + _type + '" ]').val(_value);
        }
        $(this).parent().hide();
        $(this).prev().val('');
        if (_type == 'price_ko') {
            computePrice();
        }
        if (_type == 'marketprice_ko') {
            computemPrice();
        }
        if (_type == 'costprice_ko') {
            computecPrice();
        }
        if (_type == 'weight') {
            computeWight();
        }
        if (_type == 'stock') {
            computeStock();
        }
    });
    
    /* AJAX选择品牌 */
    // 根据首字母查询
    $('.letter[cnbiztype="letter"]').find('a[data-letter]').click(function(){
        var _url = $(this).parents('.brand-index:first').attr('data-url');
        var _tid = $(this).parents('.brand-index:first').attr('data-tid');
        var _letter = $(this).attr('data-letter');
        var _search = $(this).html();
        $.getJSON(_url, {type : 'letter', tid : _tid, letter : _letter}, function(data){
            insertBrand(data, _search);
        });
    });
    // 根据关键字查询
    $('.search[cnbiztype="search"]').find('a').click(function(){
        var _url = $(this).parents('.brand-index:first').attr('data-url');
        var _tid = $(this).parents('.brand-index:first').attr('data-tid');
        var _keyword = $('#search_brand_keyword').val();
        $.getJSON(_url, {type : 'keyword', tid : _tid, keyword : _keyword}, function(data){
            insertBrand(data, _keyword);
        });
    });
    // 选择品牌
    $('ul[cnbiztype="brand_list"]').on('click', 'li', function(){
        $('#b_id').val($(this).attr('data-id'));
        $('#b_name').val($(this).attr('data-name'));
        $('.ncsc-brand-select > .ncsc-brand-select-container').hide();
    });
    //搜索品牌列表滚条绑定
    $('div[cnbiztype="brandList"]').perfectScrollbar();
    $('select[name="b_id"]').change(function(){
        getBrandName();
    });
    $('input[name="b_name"]').focus(function(){
        $('.ncsc-brand-select > .ncsc-brand-select-container').show();
    });
    
    //Ajax提示
    $('.tip').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'left',
        alignY: 'top',
        offsetX: 5,
        offsetY: -78,
        allowTipHover: false
    });
    $('.tip2').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'right',
        alignY: 'center',
        offsetX: 5,
        offsetY: 0,
        allowTipHover: false
    });

    /* 虚拟控制 */
    // 虚拟商品有效期
    $('#g_vindate').datepicker({dateFormat: 'yy-mm-dd', minDate: new Date()});
    $('[name="is_gv"]').change(function(){
        if ($('#is_gv_1').prop("checked")) {
            $('#is_fc_0').click();          // 虚拟商品不能发布F码，취소选择F码
            $('#is_presell_0').click();     // 虚拟商品不能设置预售，취소选择预售
            $('[cnbiztype="virtual_valid"]').show();
            $('[cnbiztype="virtual_null"]').hide();
        } else {
            $('[cnbiztype="virtual_valid"]').hide();
            $('[cnbiztype="virtual_null"]').show();
            $('#g_vindate').val('');
            $('#g_vlimit').val('');
        }
    });
    
    /* F码控制 */
    $('[name="is_fc"]').change(function(){
        if ($('#is_fc_1').prop("checked")) {
            $('[cnbiztype="fcode_valid"]').show();
        } else {
            $('[cnbiztype="fcode_valid"]').hide();
            $('#g_fccount').val('');
            $('#g_fcprefix').val('');
        }
    });
    
    /* 预售控制 */
    // 预售--发货时间
    $('#g_deliverdate').datepicker({dateFormat: 'yy-mm-dd', minDate: new Date()});
    $('[name="is_presell"]').change(function(){
        if ($('#is_presell_1').prop("checked")) {
            $('[cnbiztype="is_presell"]').show();
        } else {
            $('[cnbiztype="is_presell"]').hide();
        }
    });
    
    /* 预约预售控制 */
    // 预约--出售时间
    $('#g_saledate').datepicker({dateFormat: 'yy-mm-dd', minDate: new Date()});
    $('[name="is_appoint"]').change(function(){
        if ($('#is_appoint_1').prop("checked")) {
            $('[cnbiztype="is_appoint"]').show();
        } else {
            $('[cnbiztype="is_appoint"]').hide();
        }
    });
    
    /* 手机端 商品描述 */
    // 显示隐藏控制面板
    $('div[cnbiztype="mobile_pannel"]').on('click', '.module', function(){
        mbPannelInit();
        $(this).siblings().removeClass('current').end().addClass('current');
    });
    // 위로
    $('div[cnbiztype="mobile_pannel"]').on('click', '[cnbiztype="mp_up"]', function(){
        var _parents = $(this).parents('.module:first');
        _rs = mDataMove(_parents.index(), 0);
        if (!_rs) {
            return false;
        }
        _parents.clone().insertBefore(_parents.prev()).end().remove();
        mbPannelInit();
    });
    // 아래로
    $('div[cnbiztype="mobile_pannel"]').on('click', '[cnbiztype="mp_down"]', function(){
        var _parents = $(this).parents('.module:first');
        _rs = mDataMove(_parents.index(), 1);
        if (!_rs) {
            return false;
        }
        _parents.clone().insertAfter(_parents.next()).end().remove();
        mbPannelInit();
    });
    // 삭제
    $('div[cnbiztype="mobile_pannel"]').on('click', '[cnbiztype="mp_del"]', function(){
        var _parents = $(this).parents('.module:first');
        mDataRemove(_parents.index());
        _parents.remove();
        mbPannelInit();
    });
    // 수정
    $('div[cnbiztype="mobile_pannel"]').on('click', '[cnbiztype="mp_edit"]', function(){
        $('a[cnbiztype="meat_cancel"]').click();
        var _parents = $(this).parents('.module:first');
        var _val = _parents.find('.text-div').html();
        $(this).parents('.module:first').html('')
            .append('<div class="content"></div>').find('.content')
            .append('<div class="ncsc-mea-text" cnbiztype="mea_txt"></div>')
            .find('div[cnbiztype="mea_txt"]')
            .append('<p id="meat_content_count" class="text-tip">')
            .append('<textarea class="textarea valid" data-old="' + _val + '" cnbiztype="meat_content">' + _val + '</textarea>')
            .append('<div class="button"><a class="ncsc-btn ncsc-btn-blue" cnbiztype="meat_edit_submit" href="javascript:void(0);">확인</a><a class="ncsc-btn ml10" cnbiztype="meat_edit_cancel" href="javascript:void(0);">취소</a></div>')
            .append('<a class="text-close" cnbiztype="meat_edit_cancel" href="javascript:void(0);">X</a>')
            .find('#meat_content_count').html('').end()
            .find('textarea[cnbiztype="meat_content"]').unbind().charCount({
                allowed: 500,
                warning: 50,
                counterContainerID: 'meat_content_count',
                firstCounterText:   '남은 글자수:',
                endCounterText:     '자',
                errorCounterText:   '초과됨'
            });
    });
    // 수정提交
    $('div[cnbiztype="mobile_pannel"]').on('click', '[cnbiztype="meat_edit_submit"]', function(){
        var _parents = $(this).parents('.module:first');
        var _c = _parents.find('textarea[cnbiztype="meat_content"]').val();
        var _cl = _c.length;
        if (_cl == 0 || _cl > 500) {
            return false;
        }
        _data = new Object;
        _data.type = 'text';
        _data.value = _c;
        _rs = mDataReplace(_parents.index(), _data);
        if (!_rs) {
            return false;
        }
        _parents.html('').append('<div class="tools"><a cnbiztype="mp_up" href="javascript:void(0);">위로</a><a cnbiztype="mp_down" href="javascript:void(0);">아래로</a><a cnbiztype="mp_edit" href="javascript:void(0);">수정</a><a cnbiztype="mp_del" href="javascript:void(0);">삭제</a></div>')
            .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
            .append('<div class="cover"></div>');

    });
    // 수정关闭
    $('div[cnbiztype="mobile_pannel"]').on('click', '[cnbiztype="meat_edit_cancel"]', function(){
        var _parents = $(this).parents('.module:first');
        var _c = _parents.find('textarea[cnbiztype="meat_content"]').attr('data-old');
        _parents.html('').append('<div class="tools"><a cnbiztype="mp_up" href="javascript:void(0);">위로</a><a cnbiztype="mp_down" href="javascript:void(0);">아래로</a><a cnbiztype="mp_edit" href="javascript:void(0);">수정</a><a cnbiztype="mp_del" href="javascript:void(0);">삭제</a></div>')
        .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
        .append('<div class="cover"></div>');
    });
    // 初始化控制面板
    mbPannelInit = function(){
        $('div[cnbiztype="mobile_pannel"]')
            .find('a[cnbiztype^="mp_"]').show().end()
            .find('.module')
            .first().find('a[cnbiztype="mp_up"]').hide().end().end()
            .last().find('a[cnbiztype="mp_down"]').hide();
    }
    // 添加文字按钮，显示文字输入框
    $('a[cnbiztype="mb_add_txt"]').click(function(){
        $('div[cnbiztype="mea_txt"]').show();
        $('a[cnbiztype="meai_cancel"]').click();
    });
    $('div[cnbiztype="mobile_editor_area"]').find('textarea[cnbiztype="meat_content"]').unbind().charCount({
        allowed: 500,
        warning: 50,
        counterContainerID: 'meat_content_count',
        firstCounterText:   '남은 글자수:',
        endCounterText:     '자',
        errorCounterText:   '초과됨'
    });
    // 关闭 文字输入框按钮
    $('a[cnbiztype="meat_cancel"]').click(function(){
        $(this).parents('div[cnbiztype="mea_txt"]').find('textarea[cnbiztype="meat_content"]').val('').end().hide();
    });
    // 提交 文字输入框按钮
    $('a[cnbiztype="meat_submit"]').click(function(){
        var _c = toTxt($('textarea[cnbiztype="meat_content"]').val());
        var _cl = _c.length;
        if (_cl == 0 || _cl > 500) {
            return false;
        }
        _data = new Object;
        _data.type = 'text';
        _data.value = _c;
        _rs = mDataInsert(_data);
        if (!_rs) {
            return false;
        }
        $('<div class="module m-text"></div>')
            .append('<div class="tools"><a cnbiztype="mp_up" href="javascript:void(0);">위로</a><a cnbiztype="mp_down" href="javascript:void(0);">아래로</a><a cnbiztype="mp_edit" href="javascript:void(0);">수정</a><a cnbiztype="mp_del" href="javascript:void(0);">삭제</a></div>')
            .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
            .append('<div class="cover"></div>').appendTo('div[cnbiztype="mobile_pannel"]');
        
        $('a[cnbiztype="meat_cancel"]').click();
    });
    // 添加图片按钮，显示图片空间文字
    $('a[cnbiztype="mb_add_img"]').click(function(){
        $('a[cnbiztype="meat_cancel"]').click();
        $('div[cnbiztype="mea_img"]').show().load('index.php?act=store_album&op=pic_list&item=mobile');
    });
    // 关闭 图片选择
    $('div[cnbiztype="mobile_editor_area"]').on('click', 'a[cnbiztype="meai_cancel"]', function(){
        $('div[cnbiztype="mea_img"]').html('');
    });
    // 插图图片
    insert_mobile_img = function(data){
        _data = new Object;
        _data.type = 'image';
        _data.value = data;
        _rs = mDataInsert(_data);
        if (!_rs) {
            return false;
        }
        $('<div class="module m-image"></div>')
            .append('<div class="tools"><a cnbiztype="mp_up" href="javascript:void(0);">위로</a><a cnbiztype="mp_down" href="javascript:void(0);">아래로</a><a cnbiztype="mp_rpl" href="javascript:void(0);">교체</a><a cnbiztype="mp_del" href="javascript:void(0);">삭제</a></div>')
            .append('<div class="content"><div class="image-div"><img src="' + data + '"></div></div>')
            .append('<div class="cover"></div>').appendTo('div[cnbiztype="mobile_pannel"]');
        
    }
    // 교체图片
    $('div[cnbiztype="mobile_pannel"]').on('click', 'a[cnbiztype="mp_rpl"]', function(){
        $('a[cnbiztype="meat_cancel"]').click();
        $('div[cnbiztype="mea_img"]').show().load('index.php?act=store_album&op=pic_list&item=mobile&type=replace');
    });
    // 插图图片
    replace_mobile_img = function(data){
        var _parents = $('div.m-image.current');
        _parents.find('img').attr('src', data);
        _data = new Object;
        _data.type = 'image';
        _data.value = data;
        mDataReplace(_parents.index(), _data);
    }
    // 插入数据
    mDataInsert = function(data){
        _m_data = mDataGet();
        _m_data.push(data);
        return mDataSet(_m_data);
    }
    // 数据移动 
    // type 0위로  1아래로
    mDataMove = function(index, type) {
        _m_data = mDataGet();
        _data = _m_data.splice(index, 1);
        if (type) {
            index += 1;
        } else {
            index -= 1;
        }
        _m_data.splice(index, 0, _data[0]);
        return mDataSet(_m_data);
    }
    // 数据移除
    mDataRemove = function(index){
        _m_data = mDataGet();
        _m_data.splice(index, 1);     // 삭제数据
        return mDataSet(_m_data);
    }
    // 교체数据
    mDataReplace = function(index, data){
        _m_data = mDataGet();
        _m_data.splice(index, 1, data);
        return mDataSet(_m_data);
    }
    // 获取数据
    mDataGet = function(){
        _m_body = $('input[name="m_body"]').val();
        if (_m_body == '') {
            var _m_data = new Array;
        } else {
            eval('var _m_data = ' + _m_body);
        }
        return _m_data;
    }
    // 设置数据
    mDataSet = function(data){
        var _i_c = 0;
        var _i_c_m = 20;
        var _t_c = 0;
        var _t_c_m = 5000;
        var _sign = true;
        $.each(data, function(i, n){
            if (n.type == 'image') {
                _i_c += 1;
                if (_i_c > _i_c_m) {
                    alert(_i_c_m+'장의 이미지만 선택가능합니다.');
                    _sign = false;
                    return false;
                }
            } else if (n.type == 'text') {
                _t_c += n.value.length;
                if (_t_c > _t_c_m) {
                    alert(_t_c_m+'자까지 입력가능합니다.');
                    _sign = false;
                    return false;
                }
            }
        });
        if (!_sign) {
            return false;
        }
        $('span[cnbiztype="img_count_tip"]').html('<em>' + (_i_c_m - _i_c) + '</em>장 이미지를 더 선택할 수 있습니다.');
        $('span[cnbiztype="txt_count_tip"]').html('<em>' + (_t_c_m - _t_c) + '</em>자를 더 입력할 수있습니다.');
        _data = JSON.stringify(data);
        $('input[name="m_body"]').val(_data);
        return true;
    }

    //ko 모바일
    // 显示隐藏控制面板
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', '.module', function(){
        mbPannelInit_ko();
        $(this).siblings().removeClass('current').end().addClass('current');
    });
    // 위로
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', '[cnbiztype="mp_up_ko"]', function(){
        var _parents = $(this).parents('.module:first');
        _rs = mDataMove_ko(_parents.index(), 0);
        if (!_rs) {
            return false;
        }
        _parents.clone().insertBefore(_parents.prev()).end().remove();
        mbPannelInit_ko();
    });
    // 아래로
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', '[cnbiztype="mp_down_ko"]', function(){
        var _parents = $(this).parents('.module:first');
        _rs = mDataMove_ko(_parents.index(), 1);
        if (!_rs) {
            return false;
        }
        _parents.clone().insertAfter(_parents.next()).end().remove();
        mbPannelInit_ko();
    });
    // 삭제
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', '[cnbiztype="mp_del_ko"]', function(){
        var _parents = $(this).parents('.module:first');
        mDataRemove_ko(_parents.index());
        _parents.remove();
        mbPannelInit_ko();
    });
    // 수정
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', '[cnbiztype="mp_edit_ko"]', function(){
        $('a[cnbiztype="meat_cancel_ko"]').click();
        var _parents = $(this).parents('.module:first');
        var _val = _parents.find('.text-div').html();
        $(this).parents('.module:first').html('')
            .append('<div class="content"></div>').find('.content')
            .append('<div class="ncsc-mea-text" cnbiztype="mea_txt"></div>')
            .find('div[cnbiztype="mea_txt"]')
            .append('<p id="meat_content_count" class="text-tip">')
            .append('<textarea class="textarea valid" data-old="' + _val + '" cnbiztype="meat_content">' + _val + '</textarea>')
            .append('<div class="button"><a class="ncsc-btn ncsc-btn-blue" cnbiztype="meat_edit_submit_ko" href="javascript:void(0);">확인</a><a class="ncsc-btn ml10" cnbiztype="meat_edit_cancel" href="javascript:void(0);">취소</a></div>')
            .append('<a class="text-close" cnbiztype="meat_edit_cancel" href="javascript:void(0);">X</a>')
            .find('#meat_content_count').html('').end()
            .find('textarea[cnbiztype="meat_content"]').unbind().charCount({
                allowed: 500,
                warning: 50,
                counterContainerID: 'meat_content_count',
                firstCounterText:   '입력자수:',
                endCounterText:     '자',
                errorCounterText:   '초과됨'
            });
    });
    // 수정提交
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', '[cnbiztype="meat_edit_submit_ko"]', function(){
        var _parents = $(this).parents('.module:first');
        var _c = _parents.find('textarea[cnbiztype="meat_content_ko"]').val();
        var _cl = _c.length;
        if (_cl == 0 || _cl > 500) {
            return false;
        }
        _data = new Object;
        _data.type = 'text';
        _data.value = _c;
        _rs = mDataReplace(_parents.index(), _data);
        if (!_rs) {
            return false;
        }
        _parents.html('').append('<div class="tools"><a cnbiztype="mp_up_ko" href="javascript:void(0);">위로</a><a cnbiztype="mp_down_ko" href="javascript:void(0);">아래로</a><a cnbiztype="mp_edit_ko" href="javascript:void(0);">수정</a><a cnbiztype="mp_del_ko" href="javascript:void(0);">삭제</a></div>')
            .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
            .append('<div class="cover"></div>');

    });
    // 수정关闭
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', '[cnbiztype="meat_edit_cancel_ko"]', function(){
        var _parents = $(this).parents('.module:first');
        var _c = _parents.find('textarea[cnbiztype="meat_content_ko"]').attr('data-old');
        _parents.html('').append('<div class="tools"><a cnbiztype="mp_up_ko" href="javascript:void(0);">위로</a><a cnbiztype="mp_down_ko" href="javascript:void(0);">아래로</a><a cnbiztype="mp_edit_ko" href="javascript:void(0);">수정</a><a cnbiztype="mp_del_ko" href="javascript:void(0);">삭제</a></div>')
        .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
        .append('<div class="cover"></div>');
    });
    // 初始化控制面板
    mbPannelInit_ko = function(){
        $('div[cnbiztype="mobile_pannel_ko"]')
            .find('a[cnbiztype^="mp_"]').show().end()
            .find('.module')
            .first().find('a[cnbiztype="mp_up_ko"]').hide().end().end()
            .last().find('a[cnbiztype="mp_down_ko"]').hide();
    }
    // 添加文字按钮，显示文字输入框
    $('a[cnbiztype="mb_add_txt_ko"]').click(function(){
        $('div[cnbiztype="mea_txt_ko"]').show();
        $('a[cnbiztype="meai_cancel"]').click();
    });
    $('div[cnbiztype="mobile_editor_area_ko"]').find('textarea[cnbiztype="meat_content_ko"]').unbind().charCount({
        allowed: 500,
        warning: 50,
        counterContainerID: 'meat_content_count_ko',
        firstCounterText:   '남은 글자수:',
        endCounterText:     '자',
        errorCounterText:   '초과됨'
    });
    // 关闭 文字输入框按钮
    $('a[cnbiztype="meat_cancel_ko"]').click(function(){
        $(this).parents('div[cnbiztype="mea_txt_ko"]').find('textarea[cnbiztype="meat_content_ko"]').val('').end().hide();
    });
    // 提交 文字输入框按钮
    $('a[cnbiztype="meat_submit_ko"]').click(function(){
        var _c = toTxt($('textarea[cnbiztype="meat_content_ko"]').val());
        var _cl = _c.length;
        if (_cl == 0 || _cl > 500) {
            return false;
        }
        _data = new Object;
        _data.type = 'text';
        _data.value = _c;
        _rs = mDataInsert_ko(_data);
        if (!_rs) {
            return false;
        }
        $('<div class="module m-text"></div>')
            .append('<div class="tools"><a cnbiztype="mp_up_ko" href="javascript:void(0);">위로</a><a cnbiztype="mp_down_ko" href="javascript:void(0);">아래로</a><a cnbiztype="mp_edit_ko" href="javascript:void(0);">수정</a><a cnbiztype="mp_del_ko" href="javascript:void(0);">삭제</a></div>')
            .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
            .append('<div class="cover"></div>').appendTo('div[cnbiztype="mobile_pannel_ko"]');
        
        $('a[cnbiztype="meat_cancel_ko"]').click();
    });
    // 添加图片按钮，显示图片空间文字
    $('a[cnbiztype="mb_add_img_ko"]').click(function(){
        $('a[cnbiztype="meat_cancel_ko"]').click();
        $('div[cnbiztype="mea_img_ko"]').show().load('index.php?act=store_album&op=pic_list&item=mobileko');
    });
    // 关闭 图片选择
    $('div[cnbiztype="mobile_editor_area_ko"]').on('click', 'a[cnbiztype="meai_cancel"]', function(){
        $('div[cnbiztype="mea_img_ko"]').html('');
    });
    // 插图图片
    insert_mobile_img_ko = function(data){
        _data = new Object;
        _data.type = 'image';
        _data.value = data;
        _rs = mDataInsert_ko(_data);
        if (!_rs) {
            return false;
        }
        $('<div class="module m-image"></div>')
            .append('<div class="tools"><a cnbiztype="mp_up" href="javascript:void(0);">위로</a><a cnbiztype="mp_down_ko" href="javascript:void(0);">아래로</a><a cnbiztype="mp_rpl_ko" href="javascript:void(0);">교체</a><a cnbiztype="mp_del_ko" href="javascript:void(0);">삭제</a></div>')
            .append('<div class="content"><div class="image-div"><img src="' + data + '"></div></div>')
            .append('<div class="cover"></div>').appendTo('div[cnbiztype="mobile_pannel_ko"]');
        
    }
    // 교체图片
    $('div[cnbiztype="mobile_pannel_ko"]').on('click', 'a[cnbiztype="mp_rpl_ko"]', function(){
        $('a[cnbiztype="meat_cancel_ko"]').click();
        $('div[cnbiztype="mea_img_ko"]').show().load('index.php?act=store_album&op=pic_list&item=mobile&type=replace');
    });
    // 插图图片
    replace_mobile_img_ko = function(data){
        var _parents = $('div.m-image.current');
        _parents.find('img').attr('src', data);
        _data = new Object;
        _data.type = 'image';
        _data.value = data;
        mDataReplace(_parents.index(), _data);
    }
    // 插入数据
    mDataInsert_ko = function(data){
        _m_data = mDataGet();
        _m_data.push(data);
        return mDataSet_ko(_m_data);
    }
    // 数据移动 
    // type 0위로  1아래로
    mDataMove_ko = function(index, type) {
        _m_data = mDataGet();
        _data = _m_data.splice(index, 1);
        if (type) {
            index += 1;
        } else {
            index -= 1;
        }
        _m_data.splice(index, 0, _data[0]);
        return mDataSet_ko(_m_data);
    }
    // 数据移除
    mDataRemove_ko = function(index){
        _m_data = mDataGet();
        _m_data.splice(index, 1);     // 삭제数据
        return mDataSet_ko(_m_data);
    }
    // 교체数据
    mDataReplace_ko = function(index, data){
        _m_data = mDataGet();
        _m_data.splice(index, 1, data);
        return mDataSet_ko(_m_data);
    }
    // 获取数据
    mDataGet_ko = function(){
        _m_body = $('input[name="m_body_ko"]').val();
        if (_m_body == '') {
            var _m_data = new Array;
        } else {
            eval('var _m_data = ' + _m_body);
        }
        return _m_data;
    }
    // 设置数据
    mDataSet_ko = function(data){
        var _i_c = 0;
        var _i_c_m = 20;
        var _t_c = 0;
        var _t_c_m = 5000;
        var _sign = true;
        $.each(data, function(i, n){
            if (n.type == 'image') {
                _i_c += 1;
                if (_i_c > _i_c_m) {
                    alert(_i_c_m+'장의 이미지만 선택가능합니다.');
                    _sign = false;
                    return false;
                }
            } else if (n.type == 'text') {
                _t_c += n.value.length;
                if (_t_c > _t_c_m) {
                    alert(_t_c_m+'자까지 입력가능합니다.');
                    _sign = false;
                    return false;
                }
            }
        });
        if (!_sign) {
            return false;
        }
        $('span[cnbiztype="img_count_tip"]').html('<em>' + (_i_c_m - _i_c) + '</em>장 이미지를 더 선택할 수 있습니다.');
        $('span[cnbiztype="txt_count_tip"]').html('<em>' + (_t_c_m - _t_c) + '</em>자를 더 입력할 수있습니다.');
        _data = JSON.stringify(data);
        $('input[name="m_body_ko"]').val(_data);
        return true;
    }

    // 转码
    toTxt = function(str) {
        var RexStr = /\<|\>|\"|\'|\&/g
        str = str.replace(RexStr, function(MatchStr) {
            switch (MatchStr) {
            case "<":
                return "&lt;";
                break;
            case ">":
                return "&gt;";
                break;
            case "\"":
                return "&quot;";
                break;
            case "'":
                return "&#39;";
                break;
            case "&":
                return "&amp;";
                break;
            default:
                break;
            }
        })
        return str;
    }
});
// 计算商品库存
function computeStock(){
    // 库存
    var _stock = 0;
    $('input[data_type="stock"]').each(function(){
        if($(this).val() != ''){
            _stock += parseInt($(this).val());
        }
    });
    $('input[name="g_storage"]').val(_stock);
}

// 计算价格
function computePrice(){
    // 计算最低价格
    var _price = 0;var _price_sign = false;
    $('input[data_type="price_ko"]').each(function(){
        if($(this).val() != '' && $(this)){
            if(!_price_sign){
                _price = parseFloat($(this).val());
                _price_sign = true;
            }else{
                _price = (parseFloat($(this).val())  > _price) ? _price : parseFloat($(this).val());
            }
        }
    });
    $('input[name="g_price_ko"]').val(parseFloat(_price));
    $('input[name="g_price"]').val(Math.ceil(_price/SYS_CUR));

    discountCalculator();       // 计算折扣
}

// 计算市场价格
function computemPrice(){
    // 计算最低价格
    var _price = 0;var _price_sign = false;
    $('input[data_type="marketprice_ko"]').each(function(){
        if($(this).val() != '' && $(this)){
            if(!_price_sign){
                _price = parseFloat($(this).val());
                _price_sign = true;
            }else{
                _price = (parseFloat($(this).val())  > _price) ? _price : parseFloat($(this).val());
            }
        }
    });
    $('input[name="g_marketprice_ko"]').val(parseFloat(_price));
    $('input[name="g_marketprice"]').val(Math.ceil(_price/SYS_CUR));

    discountCalculator();       // 计算折扣
}

// 计算原价格
function computecPrice(){
    // 计算最低价格
    var _price = 0;var _price_sign = false;
    $('input[data_type="costprice_ko"]').each(function(){
        if($(this).val() != '' && $(this)){
            if(!_price_sign){
                _price = parseFloat($(this).val());
                _price_sign = true;
            }else{
                _price = (parseFloat($(this).val())  > _price) ? _price : parseFloat($(this).val());
            }
        }
    });
    $('input[name="g_costprice_ko"]').val(parseFloat(_price));
    $('input[name="g_costprice"]').val(Math.ceil(_price/SYS_CUR));

    //discountCalculator();       // 计算折扣
}

// 计算原价格
function computeWight(){
    // 计算最低价格
    var _price = 0;var _price_sign = false;
    $('input[data_type="weight"]').each(function(){
        if($(this).val() != '' && $(this)){
            if(!_price_sign){
                _price = $(this).val();
                _price_sign = true;
            }else{
                _price = ($(this).val()  > _price) ? _price : $(this).val();
            }
        }
    });
    $('input[name="g_weight"]').val(parseFloat(_price));

    //discountCalculator();       // 计算折扣
}

// 计算折扣
function discountCalculator() {
    var _price = parseFloat($('input[name="g_price"]').val());
    var _marketprice = parseFloat($('input[name="g_marketprice"]').val());
    if((!isNaN(_price) && _price != 0) && (!isNaN(_marketprice) && _marketprice != 0)){
        var _discount = parseInt(_price/_marketprice*100);
        $('input[name="g_discount"]').val(_discount);
    }
}

//获得商品名称
function getBrandName() {
    var brand_name = $('select[name="b_id"] > option:selected').html();
    $('input[name="b_name"]').val(brand_name);
}
//修改相关的颜色名称
function change_img_name(Obj){
     var S = Obj.parents('li').find('input[type="checkbox"]');
     S.val(Obj.val());
     var V = $('tr[cnbiztype="file_tr_'+S.attr('cbtype')+'"]');
     V.find('span[cnbiztype="pv_name"]').html(Obj.val());
     V.find('input[type="file"]').attr('name', Obj.val());
}
// 商品属性
function attr_selected(){
    $('select[cbtype="attr_select"] option:selected').each(function(){
        id = $(this).attr('cbtype');
        name = $(this).parents('select').attr('attr').replace(/__NC__/g,id);
        $(this).parents('select').attr('name',name);
    });
}
// 验证店铺分类是否重复
function checkSGC($val) {
    var _return = true;
    $('.sgcategory').each(function(){
        if ($val !=0 && $val == $(this).val()) {
            _return = false;
        }
    });
    return _return;
} 
/* 插入商品图片 */
function insert_img(name, src) {
    $('input[cnbiztype="goods_image"]').val(name);
    $('img[cnbiztype="goods_image"]').attr('src',src);
}

/* 插入수정器 */
function insert_editor(file_path) {
    KE.appendHtml('goods_body', '<img src="'+ file_path + '">');
}

function setArea(area1, area2) {
    $('#province_id').val(area1).change();
    $('#city_id').val(area2);
}

// 插入品牌
function insertBrand(param, search) {
    $('div[cnbiztype="brandList"]').show();
    $('div[cnbiztype="noBrandList"]').hide();
    var _ul = $('ul[cnbiztype="brand_list"]');
    _ul.html('');
    if ($.isEmptyObject(param)) {
        $('div[cnbiztype="brandList"]').hide();
        $('div[cnbiztype="noBrandList"]').show().find('strong').html(search);
        return false;
    }
    $.each(param, function(i, n){
        $('<li data-id="' + n.brand_id + '" data-name="' + n.brand_name + '"><em>' + n.brand_initial + '</em>' + n.brand_name + '</li>').appendTo(_ul);
    });

    //搜索品牌列表滚条绑定
    $('div[cnbiztype="brandList"]').perfectScrollbar('update');
}