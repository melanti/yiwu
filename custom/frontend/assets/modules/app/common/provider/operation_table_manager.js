(function($, angular, _){

  angular.module('App.operation')
    .service('OperationTableManager', ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', 'COMMON_UTIL', 'APP_INFO_DATA', OperationTableManager]);

  function OperationTableManager(_, $q , $timeout, $http, XHR, API_URL_INFO, COMMON_UTIL, APP_INFO_DATA){
    var vm = this;

    var COUNT_RANGE = 5,
        VIEW_RANGE_LIST = ['10','20','30','50','100'];

    vm._state = {
      tableData: {
        info: {
          start: 0,
          count: '20',
          total: 0
        },
        results: []
      }
    };

    vm.exports = {
      COUNT_RANGE:COUNT_RANGE,
      VIEW_RANGE_LIST:VIEW_RANGE_LIST,
      init:_init,
      getCountRange:function(){return COUNT_RANGE;},
      getViewRangeList:function(){return VIEW_RANGE_LIST;},
      getTableData:_getTableData,
      getOrgList:_getOrgList,
      getTableStateInfo:_getTableStateInfo,
      updateTableData:_updateTableData,
      editAccount:_editAccount,
      deleteRow:_deleteRow
    };

    function _init(){
    }

    function _editAccount(tableRowData){
      var actionURL = (APP_INFO_DATA.gURL)+'/operation/partner/registration';
      COMMON_UTIL.reqForm( actionURL, 'post',  tableRowData);

    }

    function _updateTableData(__data){
      var deferred = $q.defer();

      var _url = API_URL_INFO.GET_SERVICE_ACCOUNT_LIST_OLD
        + '?start=' + __data.start
        + '&count=' + __data.count
        + '&show=debug,contact';
      __data.hasOwnProperty('org_id') && (_url += ('&org_id=' + __data.org_id));
      __data.hasOwnProperty('service') && (_url += ('&service=' + __data.service));
      XHR
        .REQ({
          method: 'get',
          url: _url
          //data: {start: __data.start, count: __data.count}
        })
        .then(
          function(__sucData){
            if(__sucData.data.info){
              if( __sucData.data.info.start==null || __sucData.data.info.start=='null' )  {
                __sucData.data.info.start = parseInt(__data.start);
              }
              if( __sucData.data.info.count==null || __sucData.data.info.count=='null' )  {

              }
              __sucData.data.info.count = parseInt(__data.count);
            }

            _setTableData(__sucData.data);
            deferred.resolve(_getTableData());
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _deleteRow(__data){
      /*var deferred = $q.defer();

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.UPDATE_CHANGE_STATUS_MULTI,
          data: {account_uuids: __data.account_uuids, status:'deleted', show:'debug,contact'}
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;*/

       var deferred = $q.defer();

       XHR
         .REQ({
             method: 'post',
             url: API_URL_INFO.CHANGE_STATUS,
             data: {ids: __data.account_uuids, kind:'account', status:'deleted', show:'debug,contact'}
           })
           .then(
             function(__sucData){
              deferred.resolve(__sucData.data);
             },
             function(__errData){
              deferred.reject(__errData);
             }
           );

       return deferred.promise;
    }

    function _getOrgList(__data){
      var deferred = $q.defer();

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_ORG_LIST_OLD+'?show=debug'
          //data: {start: __data.start, count: __data.count}
        })
        .then(
          function(__sucData){
            __sucData.data.results.unshift({org_id:APP.info.user.org_id, org_name:APP.info.user.org_name});
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _getTableData(){
      return vm._state.tableData;
    }

    function _getTableStateInfo(__key){
      return vm._state.tableData.info[__key];
    }

    function _setTableData(__data){
      if(__data){
        vm._state.tableData = _.extend({}, vm._state.tableData, __data);

      }else{
        vm._state.tableData = _.extend({}, vm._state.tableData);
      }
    }

  }


})(window.jQuery, window.angular, window._);
