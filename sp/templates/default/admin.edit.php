<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['nc_limit_manage'];?></h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="admin_form" method="post" action='index.php?act=admin&op=admin_edit&admin_id=<?php echo $output['admininfo']['admin_id'];?>'>
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
        <tr class="noborder">
          <td colspan="2" class="required"><label><?php echo $lang['admin_index_username'];?>:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><?php echo $output['admininfo']['admin_name'];?></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label for="new_pw"><?php echo $lang['admin_edit_admin_pw']; ?>:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input id="new_pw" name="new_pw" class="txt" type="password"></td>
           <td class="vatop tips"><?php echo $lang['admin_edit_pwd_tip1'];?></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><labe for="new_pw2"><?php echo $lang['admin_edit_admin_pw2']; ?>:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input id="new_pw2" name="new_pw2" class="txt" type="password"></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="gadmin_name"><?php echo $lang['gadmin_name'];?>:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
          <select name="gid">
          <?php foreach((array)$output['gadmin'] as $v){?>
          <option <?php if ($v['gid'] == $output['admininfo']['admin_gid']) echo 'selected';?> value="<?php echo $v['gid'];?>"><?php echo $v['gname'];?></option>
          <?php }?>
          </select>
          </td>
          <td class="vatop tips"><?php echo $lang['admin_add_gid_tip'];?></td>
        </tr>        
        <tr class="noborder">
          <td colspan="2" class="required"><label class="" for="trans_type">번역자유형:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
          <select name="trans_type" id="trans_type">
            <option value="">선택하세요</option>
            <option value="1"<?php if(!empty($output['admininfo']['admin_trans_type']) && $output['admininfo']['admin_trans_type']=='1'):?> selected<?php endif;?>>번역</option>
            <option value="2"<?php if(!empty($output['admininfo']['admin_trans_type']) && $output['admininfo']['admin_trans_type']=='2'):?> selected<?php endif;?>>디자인</option>
            <option value="3"<?php if(!empty($output['admininfo']['admin_trans_type']) && $output['admininfo']['admin_trans_type']=='3'):?> selected<?php endif;?>>번역디자인</option>
          </select>
          </td>
          <td class="vatop tips"></td>
        </tr>

        <tr class="noborder">
          <td colspan="2" class="required"><label class="" for="trans_price">번역자가격:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
          <input type="text" id="trans_price" name="trans_price" class="txt" value="<?php echo $output['admininfo']['admin_trans_price'];?>"></td>
          <td class="vatop tips"></td>
        </tr>

        <tr class="noborder">
          <td colspan="2" class="required"><label class="" for="trans_memo">번역자메모:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
          <textarea cols="60" rows="6" id="trans_memo" name="trans_memo" class="txtarea"><?php echo $output['admininfo']['admin_trans_memo'];?></textarea></td>
          <td class="vatop tips"></td>
        </tr>
        <tr class="noborder">
          <td colspan="2" class="required"><label class="" for="trans_type">번역활성여부:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
          <input type="radio" id="trans_status" name="trans_status" class="radio" value="1"<?php if(!empty($output['admininfo']['admin_trans_status']) && $output['admininfo']['admin_trans_status']=='1'):?> checked<?php endif;?>><label for="trans_type">활성화</label>
          <input type="radio" id="trans_status" name="trans_status" class="radio" value="2"<?php if(!empty($output['admininfo']['admin_trans_status']) && $output['admininfo']['admin_trans_status']=='2'):?> checked<?php endif;?>><label for="trans_type">비활성화</label>
          </td>
          <td class="vatop tips"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="2" ><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){$("#submitBtn").click(function(){
    if($("#admin_form").valid()){
     $("#admin_form").submit();
	}
	});
});
$(document).ready(function(){
	$("#admin_form").validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        rules : {
        	new_pw : {
				minlength: 6,
				maxlength: 20
            },
            new_pw2 : {
				minlength: 6,
				maxlength: 20,
				equalTo: '#new_pw'
            },
            gid : {
                required : true
            }
        },
        messages : {
        	new_pw : {
				minlength: '<?php echo $lang['admin_add_password_max'];?>',
				maxlength: '<?php echo $lang['admin_add_password_max'];?>'
            },
            new_pw2 : {
				minlength: '<?php echo $lang['admin_add_password_max'];?>',
				maxlength: '<?php echo $lang['admin_add_password_max'];?>',
				equalTo:   '<?php echo $lang['admin_edit_repeat_error'];?>'
            },
            gid : {
                required : '<?php echo $lang['admin_add_gid_null'];?>',
            }
        }
	});
});
</script>