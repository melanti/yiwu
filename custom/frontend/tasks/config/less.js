/**
 * `less`
 *
 * ---------------------------------------------------------------
 *
 * Compile your LESS files into a CSS stylesheet.
 *
 * By default, only the `assets/styles/importer.less` is compiled.
 * This allows you to control the ordering yourself, i.e. import your
 * dependencies, mixins, variables, resets, etc. before other stylesheets)
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-less
 *
 */
var sails = require('sails');
var envRootpath = require('../../config/env/'+process.env.NODE_ENV).urlinfo.img;
var tpmVersions = require('../../package.json');

//sails.log.debug('process.env.url.img: ', process.env.NODE_ENV );
//sails.log.debug('envRootpath: ', envRootpath );

// var sails =require('sails: ', process.env.url.img);
module.exports = function(grunt) {



  grunt.config.set('less', {
    dev: {
      options:{
        compress:true,
        modifyVars: {
          globalImgPath:"'"+envRootpath+"'",
          ver:tpmVersions.version
        }
      },
      files: [{
        expand: true,
        cwd: 'assets/styles/',
        src: ['importer.less'],
        dest: '.tmp/public/styles/',
        ext: '.css'
      }]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
};
