<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['brand_index_brand']		= '브랜드';
$lang['brand_index_to_audit']	= '심사대기';
$lang['brand_index_name']		= '브랜드이름';
$lang['brand_index_class']		= '소속카테고리';
$lang['brand_index_class_tips']	= '카테고리 선택, 최상 밑 하위카테고리를 선택할 수 있습니다.';
$lang['brand_index_upload_tips']= '브랜드 LOGO사이즈는 150X50, 비율3:1로 설정하세요';
$lang['brand_index_recommend_tips']	= '추천한 이미지는 전체 브랜드 리스트 상단에 노출됩니다.';
$lang['brand_index_pic_sign']	= '브랜드 이미지 표식';
$lang['brand_index_help1']		= '점장이 상품 추가시 브랜드를 선택할 수 있습니다, 사용자는 해당 브랜드로 상품을 검색할 수 도 있습니다.';
$lang['brand_index_help2']		= '추천된 브랜드는 프론트 추천 브랜데 노출됩니다.';
$lang['brand_index_help3']		= '브랜드 리스트 페이지에서 유형별로 정렬됩니다.';
/**
 * 추가브랜드
 */
$lang['brand_add_name_null']	= '브랜드 이름을 입력하세요';
$lang['brand_add_sort_int']		= '정렬은 숫자만 입력가능합니다.';
$lang['brand_add_back_to_list']	= '돌아가기';
$lang['brand_add_again']		= '계속 브랜드 추가';
$lang['brand_add_support_type']	= '허용양식';
$lang['brand_add_if_recommend']	= '추천여부';
$lang['brand_add_update_sort']	= '낮은 순으로 0~255 이내 숫자로 정렬됩니다.';
$lang['brand_add_name_exists']	= '본 브랜드 이름은 이미 존재합니다.';
/**
 * 수정브랜드
 */
$lang['brand_edit_again']	= '본 브랜드 재수정';
/**
 * 브랜드申请
 */
$lang['brand_apply_pass']	= '브랜드 심사통과';
$lang['brand_apply_passed']	= '신청한 브랜드가 심시되었습니다.';
$lang['brand_apply_wrong']	= '적합한 브랜드가 아닙니다.';
$lang['brand_apply_invalid_argument']	= '상태오류!';
$lang['brand_apply_handle_ensure']		= '정말 진행하시겠습니까?';
$lang['brand_apply_fail']		= '신청한 브랜드 심사실패';
$lang['brand_apply_paramerror']		= '상태 파라미터 오류!';
$lang['brand_apply_brandparamerror']		= '브랜드 파라미터 오류!!';