<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * model简体语言包
 */
$lang['month_jan'] = '1월';
$lang['month_feb'] = '2월';
$lang['month_mar'] = '3월';
$lang['month_apr'] = '4월';
$lang['month_may'] = '5월';
$lang['month_jun'] = '6월';
$lang['month_jul'] = '7월';
$lang['month_aug'] = '8월';
$lang['month_sep'] = '9월';
$lang['month_oct'] = '10월';
$lang['month_nov'] = '11월';
$lang['month_dec'] = '12월';

$lang['order_state_submitted'] = '주문완료';
$lang['order_state_pending_payment'] = '결제대기';
$lang['order_state_canceled'] = '취소됨';
$lang['order_state_paid'] = '결제완료';
$lang['order_state_to_be_shipped'] = '배송대기';
$lang['order_state_shipped'] = '배송완료';
$lang['order_state_be_receiving'] = '수취대기';
$lang['order_state_completed'] = '완료';
$lang['order_state_to_be_evaluated'] = '평가대기';
$lang['order_state_to_be_confirmed'] = '확인대기';
$lang['order_state_confirmed'] = '확인완료';
$lang['order_state_unknown'] = '알수없음';
$lang['order_state_null'] = '없음';
$lang['order_state_operator'] = '시스템';
$lang['order_admin_operator'] = '시스템 관리자';