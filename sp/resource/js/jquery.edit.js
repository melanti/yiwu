$(document).ready(function(){
    var url = window.location.search;
    var params  = url.substr(1).split('&');
    var act = '';
    var op  = '';
    for(var j=0; j < params.length; j++)
    {
        var param = params[j];
        var arr   = param.split('=');
        if(arr[0] == 'act')
        {
            act = arr[1];
        }
        if(arr[0] == 'op')
        {
            sort = arr[1];
        }
    }
	//给需要修改的位置추가修改行为
	$('span[nc_type="inline_edit"]').click(function(){
		var s_value  = $(this).text();
		var s_name   = $(this).attr('fieldname');
		var s_id     = $(this).attr('fieldid');
		var req      = $(this).attr('required');
		var type     = $(this).attr('datatype');
		var max      = $(this).attr('maxvalue');
		var ajax_branch      = $(this).attr('ajax_branch');
		$('<input type="text">')
                        .attr({value:s_value})
                        .insertAfter($(this))
                        .focus()
                        .select()
                        .keyup(function(event){
                        if(event.keyCode == 13)
                        {
                            if(req)
                            {
                                if(!required($(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(type)
                            {
                                if(!check_type(type,$(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(max)
                            {
                                if(!check_max($(this).attr('value'),s_value,max,$(this)))
                                {
                                    return;
                                }
                            }
                            $(this).prev('span').show().text($(this).attr("value"));
							//branch ajax 分支
							//id 修改내용索引标识
							//column 修改字段名
							//value 修改내용
                            $.get('index.php?act='+act+'&op=ajax',{branch:ajax_branch,id:s_id,column:s_name,value:$(this).attr('value')},function(data){
                                if(data === 'false')
                                {
                                    alert('이름이 이미 존재합니다, 다른 이름으로 설정하세요');
                                    $('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
                                    return;
                                }
                            });
                            $(this).remove();
                        }
                    })
					.blur(function(){
					if(req)
					{
						if(!required($(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(type)
					{
						if(!check_type(type,$(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(max)
					{
						if(!check_max($(this).attr('value'),s_value,max,$(this)))
						{
							return;
						}
					}
					$(this).prev('span').show().text($(this).attr('value'));
					$.get('index.php?act='+act+'&op=ajax',{branch:ajax_branch,id:s_id,column:s_name,value:$(this).attr('value')},function(data){
						if(data === 'false')
							{
								alert('이름이 이미 존재합니다, 다른 이름으로 설정하세요');
								$('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
								return;
							}
					});
					$(this).remove();
				});
		$(this).hide();
	});


		$('span[nc_type="inline_edit_textarea"]').click(function(){
		var s_value  = $(this).text();
		var s_name   = $(this).attr('fieldname');
		var s_id     = $(this).attr('fieldid');
		var req      = $(this).attr('required');
		var type     = $(this).attr('datatype');
		var max      = $(this).attr('maxvalue');
		var ajax_branch      = $(this).attr('ajax_branch_textarea');
		$('<textarea>')
                        .attr({value:s_value})
                        .appendTo($(this).parent())
                        .focus()
                        .select()
                        .keyup(function(event){
                        if(event.keyCode == 13)
                        {
                            if(req)
                            {
                                if(!required($(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(type)
                            {
                                if(!check_type(type,$(this).attr('value'),s_value,$(this)))
                                {
                                    return;
                                }
                            }
                            if(max)
                            {
                                if(!check_max($(this).attr('value'),s_value,max,$(this)))
                                {
                                    return;
                                }
                            }
                            $(this).prev('span').show().text($(this).attr("value"));
							//branch ajax 分支
							//id 修改내용索引标识
							//column 修改字段名
							//value 修改내용
                            $.get('index.php?act='+act+'&op=ajax',{branch:ajax_branch,id:s_id,column:s_name,value:$(this).attr('value')},function(data){
                                if(data === 'false')
                                {
                                    alert('이름이 이미 존재합니다, 다른 이름으로 설정하세요');
                                    $('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
                                    return;
                                }
                            });
                            $(this).remove();
                        }
                    })
					.blur(function(){
					if(req)
					{
						if(!required($(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(type)
					{
						if(!check_type(type,$(this).attr('value'),s_value,$(this)))
						{
							return;
						}
					}
					if(max)
					{
						if(!check_max($(this).attr('value'),s_value,max,$(this)))
						{
							return;
						}
					}
					$(this).prev('span').show().text($(this).attr('value'));
					$.get('index.php?act='+act+'&op=ajax',{branch:ajax_branch,id:s_id,column:s_name,value:$(this).attr('value')},function(data){
						if(data === 'false')
							{
								alert('이름이 이미 존재합니다, 다른 이름으로 설정하세요');
								$('span[fieldname="'+s_name+'"][fieldid="'+s_id+'"]').text(s_value);
								return;
							}
					});
					$(this).remove();
				});
		$(this).hide();
	});

	//给需要修改的이미지추가异步修改行为
	$('img[nc_type="inline_edit"]').click(function(){
		var i_id    = $(this).attr('fieldid');
		var i_name  = $(this).attr('fieldname');
		var i_src   = $(this).attr('src');
		var i_val   = ($(this).attr('fieldvalue'))== 0 ? 1 : 0;
		var ajax_branch      = $(this).attr('ajax_branch');

		$.get('index.php?act='+act+'&op=ajax',{branch:ajax_branch,id:i_id,column:i_name,value:i_val},function(data){
		if(data == 'true')
			{
				if(i_val == 0)
				{
					$('img[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'src':i_src.replace('enabled','disabled'),'fieldvalue':i_val});
				}
				else
				{
					$('img[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'src':i_src.replace('disabled','enabled'),'fieldvalue':i_val});
				}
			}
		});
	});

	$('a[nc_type="inline_edit"]').live('click', function() {
		var i_id    = $(this).attr('fieldid');
		var i_name  = $(this).attr('fieldname');
		var i_src   = $(this).attr('src');
		var i_val   = ($(this).attr('fieldvalue'))== 0 ? 1 : 0;
		var ajax_branch      = $(this).attr('ajax_branch');

		$.get('index.php?act='+act+'&op=ajax',{branch:ajax_branch,id:i_id,column:i_name,value:i_val},function(data){
		if(data == 'true')
			{
				if(i_val == 0){
					$('a[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'class':('enabled','disabled'),'title':('시동','닫힘'),'fieldvalue':i_val});
				}else{
					$('a[fieldid="'+i_id+'"][fieldname="'+i_name+'"]').attr({'class':('disabled','enabled'),'title':('닫힘','시동'),'fieldvalue':i_val});
				}
			}else{
				alert('요청실패');
			}
		});
	});
    //给每个可수정的小이미지的父원素추가可수정제목 $('img[nc_type="inline_edit"]').parent().attr('title','可수정');

    //给리스트有정렬行为的列추가鼠标手型效果
    $('span[nc_type="order_by"]').hover(function(){$(this).css({cursor:'pointer'});},function(){});

});
//检查提交내용的必须项
function required(str,s_value,jqobj)
{
	if(str == '')
	{
		jqobj.prev('span').show().text(s_value);
		jqobj.remove();
		alert('此项不能为空');
		return 0;
	}
return 1;
}
//检查提交내용的유형예否合法
function check_type(type, value, s_value, jqobj)
{
	if(type == 'number')
	{
		if(isNaN(value))
		{
		jqobj.prev('span').show().text(s_value);
		jqobj.remove();
		alert('숫자만 입력하세요');
		return 0;
		}
	}
	if(type == 'int')
	{
		var regu = /^-{0,1}[0-9]{1,}$/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert('정수만 입력하세요');
			return 0;
		}
	}
	if(type == 'pint')
	{
		var regu = /^[0-9]+$/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert('정수만 입력하세요');
			return 0;
		}
	}
	if(type == 'zint')
	{
		var regu = /^[1-9]\d*$/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert('정수만 입력하세요');
			return 0;
		}
	}
		if(type == 'discount')
	{
		var regu = /[1-9]|0\.[1-9]|[1-9]\.[0-9]/;
		if(!regu.test(value))
		{
			jqobj.prev('span').show().text(s_value);
			jqobj.remove();
			alert('0.1~9.9사이의 숫자를 입력하세요');
			return 0;
		}
	}
	return 1;
}
//检查所填项的最大值
function check_max(str,s_value,max,jqobj)
{
	if(parseInt(str) > parseInt(max))
	{
		jqobj.prev('span').show().text(s_value);
		jqobj.remove();
		alert('최소최대:'+max);
		return 0;
	}
	return 1;
}


//新的inline_edit调用方法
//javacript
//$('span[nc_type="class_sort"]').inline_edit({act: 'microshop',op: 'update_class_sort'});
//html
//<span nc_type="class_sort" column_id="<?php echo $val['class_id'];?>" title="<?php echo $lang['nc_editable'];?>" class="editable tooltip"><?php echo $val['class_sort'];?></span>
//php
//$result = array();
//$result['result'] = FALSE;/TURE
//$result['message'] = '错误';
//echo json_encode($result);

(function($) {
 $.fn.inline_edit= function(options) {
     var settings = $.extend({}, {open: false}, options);
     return this.each(function() {
         $(this).click(onClick);
     });

     function onClick() {
         var span = $(this);
         var old_value = $(this).html();
         var column_id = $(this).attr("column_id");
         var s_name   = $(this).attr('fieldname');
         $('<input type="text">')
         .insertAfter($(this))
         .focus()
         .select()
         .val(old_value)
         .blur(function(){
             var new_value = $(this).attr("value");
             if(new_value != '') {
                 $.get('index.php?act='+settings.act+'&op='+settings.op,{branch:s_name,id:column_id,value:new_value},function(data){
                     data = $.parseJSON(data);
                     if(data.result) {
                         span.show().text(new_value);
                     } else {
                         span.show().text(old_value);
                         if (typeof(data.message) != 'undefined') alert(data.message);
                     }
                 });
             } else {
                 span.show().text(old_value);
             }
             $(this).remove();
         })
         $(this).hide();
     }
}
})(jQuery);

(function($) {
 $.fn.inline_edit_confirm = function(options) {
     var settings = $.extend({}, {open: false}, options);
     return this.each(function() {
         $(this).click(onClick);
     });

     function onClick() {
         var $span = $(this);
         var old_value = $(this).text();
         var column_id = $(this).attr("column_id");
         var $input = $('<input type="text">');
         var $btn_submit = $('<a class="inline-edit-submit" href="JavaScript:;">확인</a>');
         var $btn_cancel = $('<a class="inline-edit-cancel" href="JavaScript:;">취소</a>');
         var s_name   = $(this).attr('fieldname');

         $input.insertAfter($span).focus().select().val(old_value);
         $btn_submit.insertAfter($input);
         $btn_cancel.insertAfter($btn_submit);
         $span.hide();

         $btn_submit.click(function(){
             var new_value = $input.attr("value");
             if(new_value !== '' && new_value !== old_value) {
                 $.post('index.php?act=' + settings.act + '&op=' + settings.op, {branch:s_name,id:column_id, value:new_value}, function(data) {
                     data = $.parseJSON(data);
                     if(data.result) {
                         $span.text(new_value);
                     } else {
                         if (typeof(data.message) != 'undefined') alert(data.message);
                     }
                 });
             }
             show();
         });

         $btn_cancel.click(function() {
             show();
         });

         function show() {
             $span.show();
             $input.remove();
             $btn_submit.remove();
             $btn_cancel.remove();
         }
     }
};
})(jQuery);


