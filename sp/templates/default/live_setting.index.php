<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>线下抢설정</h3>
      <ul class="tab-base">
        <li><a href="JavaScript:void(0);" class="current"><span>线下抢설정</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table id="prompt" class="table tb-type2">
    <tbody>
      <tr class="space odd">
        <th colspan="12" class="nobg"> <div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span> </div>
        </th>
      </tr>
      <tr class="odd">
        <td><ul>
            <li>본 슬라이드는 공동구매 이벤트 페이지의 상단에 노출됩니다, 최대 4장 이미지까지 가능.</li>
            <li>추천 사이즈는 970X300픽셀인 jpg/gif/png유형의 이미지입니다.</li>
            <li>이미지 업로드후 "http://사이트주소..."형식으로 입력하세요.</li>
			<li>비우기는 해당 이미지를 삭제합니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form id="live_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
        <tr>
          <td class="required"><label>슬라이드 이미지1:</label></td>
          <td><label>링크주소:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><span class="type-file-show"><img class="show_image" src="<?php echo ADMIN_TEMPLATES_URL;?>/images/preview.png">
            <div class="type-file-preview"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_LIVE.DS.$output['list_setting']['live_pic1'];?>"></div>
            </span><span class="type-file-box">
            <input type='text' name='textfield1' id='textfield1' class='type-file-text' />
            <input type='button' name='button' id='button' value='' class='type-file-button' />
            <input name="live_pic1" type="file" class="type-file-file" id="live_pic1" size="30" hidefocus="true" />
            </span></td>
          <td class="vatop"><input type="text" name="live_link1" class="w200" value="<?php echo $output['list_setting']['live_link1']?>"></td>
        </tr>
        <tr>
          <td class="required"><label>슬라이드 이미지2:</label></td>
          <td><label>링크주소:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><span class="type-file-show"><img class="show_image" src="<?php echo ADMIN_TEMPLATES_URL;?>/images/preview.png">
            <div class="type-file-preview"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_LIVE.DS.$output['list_setting']['live_pic2'];?>"></div>
            </span><span class="type-file-box">
            <input type='text' name='textfield2' id='textfield2' class='type-file-text' />
            <input type='button' name='button' id='button' value='' class='type-file-button' />
            <input name="live_pic2" type="file" class="type-file-file" id="live_pic2" size="30" hidefocus="true" />
            </span></td>
          <td class="vatop tips"><input type="text" name="live_link2" class="w200" value="<?php echo $output['list_setting']['live_link2']?>"></td>
        </tr>
        <tr>
          <td class="required"><label>슬라이드 이미지3:</label></td>
          <td><label>링크주소:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><span class="type-file-show"><img class="show_image" src="<?php echo ADMIN_TEMPLATES_URL;?>/images/preview.png">
            <div class="type-file-preview"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_LIVE.DS.$output['list_setting']['live_pic3'];?>"></div>
            </span><span class="type-file-box">
            <input type='text' name='textfield3' id='textfield3' class='type-file-text' />
            <input type='button' name='button' id='button' value='' class='type-file-button' />
            <input name="live_pic3" type="file" class="type-file-file" id="live_pic3" size="30" hidefocus="true" />
            </span></td>
          <td class="vatop tips"><input type="text" name="live_link3" class="w200" value="<?php echo $output['list_setting']['live_link3']?>"></td>
        </tr>
        <tr>
          <td class="required"><label>슬라이드 이미지4:</label></td>
          <td><label>링크주소:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><span class="type-file-show"><img class="show_image" src="<?php echo ADMIN_TEMPLATES_URL;?>/images/preview.png">
            <div class="type-file-preview"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_LIVE.DS.$output['list_setting']['live_pic4'];?>"></div>
            </span><span class="type-file-box">
            <input type='text' name='textfield4' id='textfield4' class='type-file-text' />
            <input type='button' name='button' id='button' value='' class='type-file-button' />
            <input name="live_pic4" type="file" class="type-file-file" id="live_pic4" size="30" hidefocus="true" />
            </span></td>
          <td class="vatop tips"><input type="text" name="live_link4" class="w200" value="<?php echo $output['list_setting']['live_link4']?>"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2">
			<a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a>
			<a href="JavaScript:void(0);" class="btn" id="clearBtn"><span>비우기</span></a>
		  </td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.edit.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script> 
<script>
//按钮先执行验证再提交表单
$(function(){
	// 이미지js
	$("#live_pic1").change(function(){$("#textfield1").val($("#live_pic1").val());});
	$("#live_pic2").change(function(){$("#textfield2").val($("#live_pic2").val());});
	$("#live_pic3").change(function(){$("#textfield3").val($("#live_pic3").val());});
	$("#live_pic4").change(function(){$("#textfield4").val($("#live_pic4").val());});

	$('#live_form').validate({
        errorPlacement: function(error, element){
            error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        success: function(label){
            label.addClass('valid');
        },
        rules : {
            live_link1: {
                url : true
            },
			live_link2:{
				url : true	
			},
			live_link3:{
				url : true	
			},
			live_link4:{
				url : true	
			},
        },
        messages : {
            live_link1: {
                url : '정확한 링크주소를 입력하세요'
            },
			live_link2:{
				url : '정확한 링크주소를 입력하세요'
			},
			live_link3:{
				url : '정확한 링크주소를 입력하세요'
			},
			live_link4:{
				url : '정확한 링크주소를 입력하세요'
			},
        }
    });

	$('#clearBtn').click(function(){
		$.ajax({
			type:'get',
			url:'index.php?act=live_setting&op=clear',
			dataType:'json',
			success:function(result){
				if(result.result){
					alert('비우기 성공');
					location.reload();
				}	
			}
		});		
	});

	$("#submitBtn").click(function(){
		$("#live_form").submit();
	});
});
</script> 
