<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['article_index_del_succ']	= '문장 삭제 성공.';
$lang['article_index_choose']	= '삭제할 내용을 선택하세요';
$lang['article_index_manage']	= '게시글관리';
$lang['article_index_title']	= '제목';
$lang['article_index_class']	= '문장 카테고리';
$lang['article_index_show']		= '노출여부';
$lang['article_index_addtime']	= '작성시간';
$lang['article_index_help1']	= '시스템 문장과 다름으로 문장 리스트 페이지에서 확인 가능합니다.';
/**
 * 추가문장
 */
$lang['article_add_url']		= '링크';
$lang['article_add_url_tip']			= '링크주소 입력후 문장 제목을 클릭하면 바로 해당 링크로 이동합니다, 내용은 노출되지 않습니다, 링크주소에는 반드시 http://시작되어야합니다.';
$lang['article_add_show']		= '노출';
$lang['article_add_class']		= '소속카테고리';
$lang['article_add_content']	= '문장내용';
$lang['article_add_upload']		= '이미지 업로드';
$lang['article_add_batch_upload']	= '일괄업로드';
$lang['article_add_normal_upload']	= '일반업로드';
$lang['article_add_uploaded']		= '보유이미지';
$lang['article_add_insert']			= '삽입';
$lang['article_add_title_null']		= '문장제목을 입력하세요';
$lang['article_add_class_null']		= '문장카테고리를 선택하세요';
$lang['article_add_url_wrong']		= '정확한 링크주소를 입력하세요';
$lang['article_add_content_null']	= '문장내용을 입력하세요';
$lang['article_add_sort_int']		= '문장정렬은 반드시 숫자로 입력하세요';
$lang['article_add_del_fail']		= '삭제실패';
$lang['article_add_img_wrong']      = '이미지 허용 확장명:png,gif,jpeg,jpg';
$lang['article_add_tolist']      = '돌아가기';
$lang['article_add_continueadd']      = '계속 문장추가';
$lang['article_add_ok']      = '문장추가 성공.';
$lang['article_add_fail']      = '문장추가 실패.';
/**
 * 문장수정
 */
$lang['article_edit_back_to_list']	= '돌아가기';
$lang['article_edit_edit_again']	= '본 문장 재수정';
$lang['article_edit_succ']			= '문장수정 성공';
$lang['article_edit_fail']			= '문장수정 실패';
/**
 * iframe上传
 */
$lang['article_iframe_upload']		= '업로드';
$lang['article_iframe_uploadfail']		= '업로드 실패';

