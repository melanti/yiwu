
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.org.list.ctrl.storeAdminListSearchCtrl');
  APP.modules.org.list.ctrl.storeAdminListSearchCtrl = storeAdminListSearchCtrl;

  function storeAdminListSearchCtrl( $scope, $q, _, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL, $sce ){
    var ctrl = this;

    ctrl.reqModel = {
      search_type:0, // { 0 | 1 | 2 | 3 } ( 전체 | 운영업체 명칭 | 입점업체 명칭 | 담당자명 )
      search_keyword:'',
      parent_org_id:APP.info.user.parent_org_id,
      status:2 //0 : activated, 1 :disabled, 2 : all
    };

    ctrl.filter = {
      orgListArr:[]
    };


    ctrl.getTotal;
    ctrl.getStart;
    ctrl.getCount;
    ctrl.getReqModel;
    ctrl.setReqModel;
    ctrl.getFilter;
    ctrl.setFilter;
    ctrl.updatePage;
    ctrl.updateList = _updateList;

    _init();

    function _init(){
      _registWatcher();
      ctrl.setReqModel({options:_.extend({},ctrl.reqModel)});
    }

    function _registWatcher(){
      $scope.$on('$destroy', function () {
      });

      $scope.$watch( 'tableSearchCtrl.options.query_opt', function _watchQueryOpt(__newVal , __oldVal){
        if(__newVal===__oldVal){
        }else{
          ctrl.reqModel.search_keyword = '';
        }
      });

      // PubSub.subscribeOnce('tableComponent:bootstrap:complete', function _completeBootstrap(topic, data){
      //   if(data.success){
      //   }
      // });

      PubSub.subscribe('table:change',function _onChangeTableData(topic, data){
        console.log('_onChangeTableData: ' , data);
        ctrl.reqModel = _.extend({},data.reqModel);
        ctrl.filter = _.extend({},data.filter);
      });
    }

    function _updateList(){
      if( ctrl.reqModel.search_type !==0 && ctrl.reqModel.search_keyword==='' ){
        alert('검색어를 입력해주세요.');
        $('#search_keyword').focus();
        return false;
      }

      ctrl.setReqModel({options:_.extend({},ctrl.reqModel)});
      ctrl.setFilter({options:_.extend({},ctrl.filter)});
      ctrl.updatePage({options:{start:0}});
    }


  }
  storeAdminListSearchCtrl.$inject = ['$scope', '$q', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL', '$sce'];
  storeAdminListSearchCtrl.bindToController = {
    getTotal:'&',
    getStart:'&',
    getCount:'&',
    getReqModel:'&',
    setReqModel:'&',
    getFilter:'&',
    setFilter:'&',
    updatePage:'&'
  };
  storeAdminListSearchCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableSearchCtrl:true});
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
