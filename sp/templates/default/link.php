<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['link_index_del_succ']	= '파트너 삭제 성공.';
$lang['link_index_choose_del']	= '삭제할 내용을 선택하세요';
$lang['link_index_partner']		= '파트너';
$lang['link_index_title']		= '제목';
$lang['link_index_link']		= '링크';
$lang['link_index_pic_sign']	= '이미지 표식';
$lang['link_pic']	            = '이미지링크';
$lang['link_word']	            = '텍스트링크';
$lang['link_all']               = '전체';
$lang['link_help1']				= '파트너 관리에서 해당 정보를 수정/보기/삭제할 수 있습니다.';
$lang['link_help2']				= '검색창에 전체/이미지/텍스트 탭으로 검색을 시도할 수 있습니다.';
/**
 * 추가合作伙伴
 */
$lang['link_add_title_null']	= '파트너 제목을 입력하세요';
$lang['link_add_url_wrong']		= '정확한 링크주소를 입력하세요';
$lang['link_add_sort_int']		= '정렬은 숫자만 가능합니다.';
$lang['link_add_back_to_list']	= '돌아가기';
$lang['link_add_again']			= '계속 추가';
$lang['link_add_succ']			= '파트너 추가 성공';
$lang['link_add_fail']			= '파트너 추가 실패';
$lang['link_add_name']			= '파트너 이름';
$lang['link_add_href']			= '파트너 링크주소';
$lang['link_add_sign']			= '파트너 주이미지';
$lang['link_add_tosign']		= '현재는 텍스트 링크, 파트너사 로고가 있을시 다시 업로드해주세요';
$lang['link_add_sort_tip']		= '작은순으로 정렬';
$lang['link_add_url_null']		= '파트너 링크주소를 입력하세요';
$lang['link_add_pic_sign_error']	= '이미지 허용 확장명:gif, jpg, jpeg, png';
/**
 * 수정合作伙伴
 */
$lang['link_edit_again']	= '본 파트너 재수정';
$lang['link_edit_succ']		= '파트너 수정 성공';
$lang['link_edit_fail']		= '파트너 수정 실패';