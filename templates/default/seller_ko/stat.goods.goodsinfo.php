<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if($output['stat_msg']){ ?>
	<div class="alert alert-info mt10" style="clear:both;"><?php echo $output['stat_msg'];?></div>
<?php } else {?>
<div class="alert alert-info mt10" style="clear:both;">
	<h6 class="w tc bolder">“<?php echo $output['goods_info']['goods_name'];?>”추세표</h6><br>
	<ul class="mt5">
    <li>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="전일부터 최근 30일 간의 유효한 주문서의 총 금액" class="tip icon-question-sign"></i>
    		최근 30일 주문 금액：<strong><?php echo $output['stat_count']['ordergamount'];?>원</strong>
    	</span>
		<span class="w210 fl h30" style="display:block;">
			<i title="전일부터 최근 30일 간의 유효한 주문서의 상품 총 수량" class="tip icon-question-sign"></i>
			최근 30일 주문 상품 수량：<strong><?php echo $output['stat_count']['ordergoodsnum'];?></strong>
		</span>
		<span class="w210 fl h30" style="display:block;">
			<i title="전일부터 최근 30일 간의 유효한 주문서의 총 주문서 수량" class="tip icon-question-sign"></i>
			최근 30일 주문량：<strong><?php echo $output['stat_count']['ordernum'];?></strong>
		</span>
    </li>
    </ul>
    <div style="clear:both;"></div>
</div>

<div id="container_ordergamount"></div>
<div id="container_ordergoodsnum"></div>
<div id="container_ordernum"></div>

<script type="text/javascript">
$(function(){
	$('#container_ordergamount').highcharts(<?php echo $output['stat_json']['ordergamount'];?>+'원');
	$('#container_ordergoodsnum').highcharts(<?php echo $output['stat_json']['ordergoodsnum'];?>);
	$('#container_ordernum').highcharts(<?php echo $output['stat_json']['ordernum'];?>);

	//Ajax提示
    $('.tip').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'top',
        offsetY: 5,
        allowTipHover: false
    });
});
</script>
<?php } ?>