<?php
/**
 * 我的商城
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class member_indexControl extends mobileMemberControl {

	public function __construct(){
		parent::__construct();
	}

    /**
     * 我的商城
     */
    public function indexOp() {
        $member_info = array();


        $model_member = Model('member');

        $member_grade = $model_member->getMemberGradeArr();

        $level_name = ($t = $model_member->getOneMemberGrade($this->member_info['member_exppoints'], false, $member_grade))?$t['level_name']:'';

        $member_info['user_name'] = $this->member_info['member_name'];
        $member_info['avatar'] = getMemberAvatarForID($this->member_info['member_id']);
        $member_info['level_name'] = $level_name;
        $member_info['favorites_store'] = (int) Model('favorites')->getStoreFavoritesCountByStoreId('',$this->member_info['member_id']);
        $member_info['favorites_goods'] = (int) Model('favorites')->getGoodsFavoritesCountByGoodsId('',$this->member_info['member_id']);
        $member_info['point'] = $this->member_info['member_points'];
        $member_info['predepoit'] = $this->member_info['available_predeposit'];
        $member_info['order_nopay_count'] = Model('order')->getOrderCountByID('buyer',$this->member_info['member_id'],"NewCount");
        $member_info['order_noreceipt_count'] = Model('order')->getOrderCountByID('buyer',$this->member_info['member_id'],"SendCount");
        $member_info['order_notakes_count'] = 0;
        $member_info['order_noeval_count'] = Model('order')->getOrderCountByID('buyer',$this->member_info['member_id'],"EvalCount");

        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['seller_state'] = array('lt','3');
        $member_info['return'] = Model('refund_return')->getRefundReturn($condition);

        output_data(array('member_info' => $member_info));
    }
    /**
     * 我的商城
     */
    public function my_assetOp() {
        $fields = !empty($_GET['fields']) ? $_GET['fields'] : "";
        if($fields=="available_rc_balance")
        {
            $total = $this->member_info['available_rc_balance'];
        }
        else if($fields=="point")
        {
            $total = $this->member_info['member_points'];
        }
        else
        {
            $total = $this->member_info['available_predeposit'];
        }
        output_data(array($fields=> $total));
    }

}
