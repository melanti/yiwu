<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.mousewheel.js"></script>
<!--[if lt IE 9]>
  <script src="<?php echo RESOURCE_SITE_URL;?>/js/json2.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_goods_add.step2.js"></script>

<!-- td popup js start -->

<!-- ANGULAR templates -->
<script type="text/ng-template" id="threeD_dash_board.html">

  <div class="three-items-list-cont" data-c="{{threeDDashBoardCtrl.items.total}}">

    <div class="three-items-list-wrap" ng-if="threeDDashBoardCtrl.type=='item'">
      <table class="table table-striped table-bordered table-dash-board">
        <thead>
        <tr>
          <th class="table-thead-th-all" >전체</th>
          <th class="table-thead-th-threed-regist">3d 의상등록</th>
          <th class="table-thead-th-threed-unregist">3d 의상미등록</th>
        </tr>
        </thead>

        <tbody>
        <tr >
          <td>
            <span class="name">{{threeDDashBoardCtrl.items.total}} 건</span>
          </td>

          <td>
            <span class="name">{{threeDDashBoardCtrl.items.register}} 건</span>
          </td>

          <td>
            <span class="name">{{threeDDashBoardCtrl.items.not_register}} 건</span>
          </td>
        </tr>

        </tbody>
      </table>
    </div>

    <div class="three-items-list-wrap" ng-if="threeDDashBoardCtrl.type=='threed'">
      <table class="table table-striped table-bordered table-dash-board">
        <thead>
        <tr>
          <th class="table-thead-th-all" >전체</th>
          <th class="table-thead-th-threed-regist">Woman</th>
          <th class="table-thead-th-threed-unregist">Man</th>
        </tr>
        </thead>

        <tbody>

        <tr >
          <td>
            <span class="name">{{threeDDashBoardCtrl.threed.total}} 건</span>
          </td>

          <td>
            <span class="name">{{threeDDashBoardCtrl.threed.woman}} 건</span>
          </td>

          <td>
            <span class="name">{{threeDDashBoardCtrl.threed.man}} 건</span>
          </td>
        </tr>
        </tbody>
      </table>
    </div>


    <div class="three-items-list-btn" ng-if="threeDDashBoardCtrl.type=='item'">
      <a href="/items_mng/items/registration" class="btn btn-primary btn-lg" style="">상품등록</a>
    </div>


  </div>

</script>


<script type="text/ng-template" id="table_component_basic.html">
  <!-- table-list_table end -->
  <div class="list_table"></div>
</script>

<script type="text/ng-template" id="table_search_threeD_popup.html">
  <div class="list-form-wrapper" style="margin-top:30px;">

    <div class="list-form-inner">
      <table class="table list_form" style="margin-bottom:0px;">
        <thead style="">
        <tr>
          <th style="width:18%"></th>
          <th style="width:82%"></th>
        </tr>
        </thead>

        <tbody>

        <tr >
          <td class="list_form_label">검색 구분</td>
          <td>
            <div class="form-inline">
              <select
                  ng-model="tableSearchCtrl.query_opt"
                  ng-change="tableSearchCtrl.onChangeQueryOpt()"
                  class="form-control" name="query_opt" id="query_opt">
                <optgroup label="">
                  <option value="td_name">의상명</option>
                  <option value="td_code">의상코드</option>
                </optgroup>
              </select>

              <select
                  ng-show="(tableSearchCtrl.query_opt=='org_id')"
                  ng-model="tableSearchCtrl.org_id"
                  ng-change="tableSearchCtrl.onChangeOrgId()"
                  class="form-control" name="org_id" id="org_id">

                <option ng-if="tableSearchCtrl.userData.account_type==='admin'" value="">--입점업체 전체--</option>
                <option
                    ng-repeat="opt in tableSearchCtrl.org_id_arr track by $index"
                    ng-bind="opt.org_name"
                    data-org_name="{{opt.org_name}}"
                    data-org_id="{{opt.org_id}}"
                    value="{{opt.org_id}}"
                ></option>
              </select>

              <select
                  ng-show="(tableSearchCtrl.query_opt=='org_id') && (tableSearchCtrl.cat_arr.length>0)"
                  ng-model="tableSearchCtrl.cat_id"
                  class="form-control" name="cat_id" id="cat_id">

                <option value="">--CAT 제작업체 선택--</option>
                <option
                    ng-repeat="opt in tableSearchCtrl.cat_arr track by $index"
                    ng-bind="opt.login_id"
                    data-account_id="{{opt.account_id}}"
                    value="{{opt.account_id}}"
                ></option>
              </select>


              <input
                  ng-show="(tableSearchCtrl.query_opt!='org_id')"
                  ng-model="tableSearchCtrl.query"
                  name="query"
                  id="query"
                  type="text" class="form-control search_class_txt">
            </div>
          </td>
        </tr>



        <tr >
          <td class="list_form_label">성인 성별 구분</td>
          <td>
            <div class="form-inline">

              <!--<label class="check-inline">-->
              <!--<input type="checkbox" ng-model="tableSearchCtrl.td_sex_all" ng-true-value="true" ng-false-value="false" id="td_sex_all" value="td_sex_all"> 입점업체 전체-->
              <!--</label>-->

              <label class="check-inline">
                <input type="checkbox" ng-model="tableSearchCtrl.td_sex_woman" ng-true-value="true" ng-false-value="false" id="td_sex_woman" value="td_sex_woman"> 여성
              </label>

              <label class="check-inline">
                <input type="checkbox" ng-model="tableSearchCtrl.td_sex_man" ng-true-value="true" ng-false-value="false" id="td_sex_man" value="td_sex_man"> 남성
              </label>

              <label class="check-inline">
                <input type="checkbox" ng-model="tableSearchCtrl.td_sex_kids" ng-true-value="true" ng-false-value="false" id="td_sex_kids" value="td_sex_kids"> 아동
              </label>
            </div>
          </td>
        </tr>

        <tr >
          <td class="list_form_label">제작 카테고리</td>
          <td>
            <div class="form-inline">

              <select
                  ng-repeat="li in tableSearchCtrl.make_category_arr track by $index"
                  ng-model="li.cur_code"
                  ng-change="tableSearchCtrl.onChangeCateNmList(li.index)";
                  ng-class="{visible_hidden:(li.data.length===0)}"
                  class="form-control" name="cate_id{{li.index}}" id="cate_id{{li.index}}" data-idx="{{li.index}}">
                <option value="" ng-if="$index===0">-전체선택-</option>
                <option value="" ng-if="$index!==0">-{{(li.index+1)}}차분류선택-</option>
                <option
                    ng-repeat="chli in li.data track by $index"
                    value="{{chli.CODE}}">{{ (chli[tableSearchCtrl.curlang]!=='')?(chli[tableSearchCtrl.curlang]):(chli.CODE) }}</option>
              </select>

            </div>
          </td>
        </tr>

        <tr >
          <td class="list_form_label">의상제작일</td>
          <td>
            <div class="form-inline">
              <button ng-click="tableSearchCtrl.changePeriod('today')" type="button" class="btn btn-primary btn-sm" ng-class="{'btn-date-all':(tableSearchCtrl.period_unit=='today')}" data-vaule="today">하루</button>
              <button ng-click="tableSearchCtrl.changePeriod('3days')" type="button" class="btn btn-primary btn-sm" ng-class="{'btn-date-all':(tableSearchCtrl.period_unit=='3days')}"  data-vaule="3days">3일</button>
              <button ng-click="tableSearchCtrl.changePeriod('week')" type="button" class="btn btn-primary btn-sm" ng-class="{'btn-date-all':(tableSearchCtrl.period_unit=='week')}"  data-vaule="week">7일</button>
              <button ng-click="tableSearchCtrl.changePeriod('1month')" type="button" class="btn btn-primary btn-sm" ng-class="{'btn-date-all':(tableSearchCtrl.period_unit=='1month')}"  data-vaule="1month">1개월</button>
              <button ng-click="tableSearchCtrl.changePeriod('3month')" type="button" class="btn btn-primary btn-sm" ng-class="{'btn-date-all':(tableSearchCtrl.period_unit=='3month')}"  data-vaule="3month">3개월</button>
              <button ng-click="tableSearchCtrl.changePeriod('1year')" type="button" class="btn btn-primary btn-sm" ng-class="{'btn-date-all':(tableSearchCtrl.period_unit=='1year')}"  data-vaule="1year">1년</button>
              <button ng-click="tableSearchCtrl.changePeriod('all')" type="button" class="btn btn-primary btn-sm" ng-class="{'btn-date-all':(tableSearchCtrl.period_unit=='all')}"  data-vaule="all">전체</button>
            </div>
          </td>
        </tr>

        <tr >
          <td class="list_form_label"></td>
          <td>
            <div class="form-inline">
              <div class="form-group">
                <label class="sr-only" for="st_date">2016-01-01</label>
                <input
                    ng-model="tableSearchCtrl.stDate"
                    ui-date="tableSearchCtrl.stDateOptions"
                    type="text" class="form-control" id="st_date" name="st_date" placeholder="yyyy-mm-dd">
              </div>
              <span class="form-group" style="margin:0 10px;padding-top:8px;">~</span>
              <div class="form-group">
                <label class="sr-only" for="end_date">2016-01-01</label>
                <input
                    ng-model="tableSearchCtrl.endDate"
                    ui-date="tableSearchCtrl.endDateOptions"
                    type="text" class="form-control" id="end_date" name="end_date" placeholder="yyyy-mm-dd">
              </div>
            </div>
          </td>
        </tr>



        </tbody>
      </table>
    </div>


    <div class="list-form-search-btn">
      <a class="btn btn-primary btn-lg" ng-click="tableSearchCtrl.updateList()">검색</a></p>
    </div>

    <div  style="width:70%;margin-top:30px;">
      <select
          ng-model="tableSearchCtrl.order"
          ng-change="tableSearchCtrl.updateList()"
          style="width:150px;float:right;height:30px;"
          class="form-control ng-pristine ng-untouched ng-valid" name="order" id="order">
        <optgroup label="">
          <option value="registerd_date">최신등록순</option>
          <option value="name">3D의상명순</option>
        </optgroup>
      </select>
    </div>


  </div>


</script>

<script type="text/ng-template" id="table_body_threeD_popup.html">

  <div class="table_cont" style="max-height: 322px;width:70%;border: 1px solid #dddddd;margin-top: 10px;float: left;">

    <table class="table table-hover no-margin td_popup_body_table" style="min-width:300px;">
      <thead>
      <tr>
        <th style="width:4%">No</th>
        <th style="width:17%">제작 카테고리</th>
        <th style="width:12%">3D의상썸네일</th>
        <th style="width:12%">3D의상코드</th>
        <th style="width:23%">3D의상명</th>
        <th style="width:10%">성별</th>
        <th style="width:15%">설명</th>
        <th style="width:8%">의상제작일</th>

      </tr>
      </thead>

      <tbody
          ng-show="tableBodyCtrl.getTotal()>0"
      >

      <tr class="clickable-row"
          ng-repeat="tableRow in tableBodyCtrl.getResults() track by $index"
          data-id="{{tableRow.id}}"
          ng-click="tableBodyCtrl.showTdPrevModal(tableRow,$index);"
      >

        <td>
          <span class="name" >{{tableBodyCtrl.getTotal()-(tableBodyCtrl.getCount()*(tableBodyCtrl.getStart()/tableBodyCtrl.getCount()) )-$index}}</span>
        </td>


        <td>
          <span class="name" ng-bind="tableRow.make_category_route"></span>
        </td>

        <td>
                      <span class="name">
                        <div class="thumbnail_img_cont" style="margin:0 auto;">
                          <img ng-show="(!tableRow.td_fitting_files.thumbnail.url)" class="bg_img" src="/app/common/no-image.jpg?_=" alt="{{tableRow.name}}" />
                          <img ng-show="(tableRow.td_fitting_files && tableRow.td_fitting_files.thumbnail && tableRow.td_fitting_files.thumbnail.url)" class="thumbnail_img" ng-src="{{tableRow.td_fitting_files.thumbnail.url}}?_=<%=sails.config.ver%>" alt="{{tableRow.name}}" />
                        </div>
                      </span>
        </td>

        <td>
          <span class="name" ng-bind="tableRow.code"></span>
        </td>

        <td>
          <span class="name" ng-bind="tableRow.name"></span>
          <!--<a class="thumbnail_img_desc" href="" ng-click="tableBodyCtrl.showTdPrevModal(tableRow);" >{{tableRow.name}}</a>-->
        </td>

        <td>
          <span class="name" ng-bind="tableRow.sex"></span>
        </td>


        <td>
          <span class="name" ng-bind="tableRow.description"></span>
        </td>

        <td>
          <span class="name" ng-bind="tableBodyCtrl.renderRegistSection(tableRow.registerd_datetime)"></span>
        </td>


      </tr>


      </tbody>

      <tbody ng-show="tableBodyCtrl.getTotal()<1">
      <tr>
        <td colspan="10" style="text-align: center;">검색된 상품이 없습니다.</td>
      </tr>
      </tbody>

    </table>
  </div>


</script>

<script  type="text/ng-template" id="td_prev_modal_small.html">

  <div class="td_preview_modal_container">

    <!-- Modal -->
    <div class="" id="tdPreviewModal">
      <div class="">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
            <div class="modal-body-inner">
              <div
                  ng-class="{'img_show':($index==tdPrevModalCtrl.curIdx)}"
                  ng-repeat="li in tdPrevModalCtrl.imgArr track by $index"
                  class="td_img_cont">

                <div class="td_img" style="background:url({{li}}) no-repeat center center; background-size:cover;background-repeat: no-repeat;width: 100%;height: 100%;"></div>
                <!--<img class="td_img" ng-src="{{li}}" alt="">-->
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <ul class="pager">
              <li>
                <a
                    ng-click="tdPrevModalCtrl.showPrev()"
                    href="">Previous</a>
              </li>
              <li>
                <a
                    ng-click="tdPrevModalCtrl.showNext()"
                    href="">Next</a>
              </li>
            </ul>
          </div>

        </div>
      </div>
    </div>

  </div>

</script>


<!--COMMON_DATA (mandatory)-->
<script>

  var APP = APP || {};

  APP.modules={
    directive:{
      util:{}
    },
    ctrl:{}
  };

  APP.createNS = function (namespace) {
    var nsparts = namespace.split(".");
    var parent = APP;

    if (nsparts[0] === "APP") {
      nsparts = nsparts.slice(1);
    }

    for (var i = 0; i < nsparts.length; i++) {
      var partname = nsparts[i];
      if (typeof parent[partname] === "undefined") {
        parent[partname] = {};
      }
      parent = parent[partname];
    }
    return parent;
  };

  //link 수정
  APP.URL_INFO = {
    "menu":{"HOME":{"name":"HOME","link":"#"},
      "LOGIN":{"name":"LOGIN","link":"#"},
      "LOGOUT":{"name":"LOGOUT","link":"#"},
      "OPR_MNG_ORG_ADMIN_CONFIRMATION":{"name":"운영업체 정보보기","link":"#"},
      "OPR_MNG_ORG_ADMIN_MODIFICATION":{"name":"운영업체 정보수정","link":"#"},
      "OPR_MNG_ORG_STORE_LIST":{"name":"입점업체 관리","link":"#"},
      "OPR_MNG_ORG_STORE_REGISTRATION":{"name":"입점업체 등록","link":"#"},
      "OPR_MNG_ORG_STORE_MODIFICATION":{"name":"입점업체 수정","link":"#"},
      "OPR_MNG_ORG_STORE_CONFIRMATION":{"name":"입점업체 보기","link":"#"},
      "OPR_MNG_ACC_ORG_LIST":{"name":"업체 계정관리","link":"#"},
      "OPR_MNG_ACC_ORG_REGISTRATION":{"name":"업체 계정등록","link":"#"},
      "OPR_MNG_ACC_ORG_MODIFICATION":{"name":"업체 계정수정","link":"#"},
      "OPR_MNG_ACC_ORG_CONFIRMATION":{"name":"업체 계정보기","link":"#"},
      "ITEMS_MNG_CATEGORY":{"name":"분류관리","link":"#"},
      "ITEMS_MNG_CATEGORY_BRAND":{"name":"브랜드관리","link":"#"},
      "ITEMS_MNG_CATEGORY_BRAND_REGISTRATION":{"name":"브랜드등록","link":"#"},
      "ITEMS_MNG_CATEGORY_BRAND_MODIFICATION":{"name":"브랜드수정","link":"#"},
      "ITEMS_MNG_CATEGORY_BRAND_CONFIRMATION":{"name":"브랜드조회","link":"#"},
      "ITEMS_MNG_ITEMS_LIST":{"name":"상품목록","link":"#"},
      "ITEMS_MNG_ITEMS_REGISTRATION":{"name":"상품등록","link":"#"},
      "ITEMS_MNG_ITEMS_MODIFICATION":{"name":"상품수정","link":"#"},
      "ITEMS_MNG_ITEMS_CONFIRMATION":{"name":"상품보기","link":"#"},
      "ITEMS_MNG_ITEMS_BATCH_REGISTRATION":{"name":"상품일괄등록","link":"#"},
      "ITEMS_MNG_ITEMS_TD_LIST":{"name":"3d상품목록","link":"#"}}
  };

  //link 수정
  APP.MENU_DATA = {
    "cur_gnb_id":"items_mng",
    "cur_lnb_idx":1,
    "cur_lnb_id":"ITEMS_MNG_ITEMS_TD_LIST",
    "menu":[
      {
        "id":"opr_mng",
        "name":"운영관리",
        "link":"",
        "isHide":false,
        "children":[
          {
            "name":"업체정보 관리",
            "isHide":false,
            "children":[
              {
                "isHide":false,
                "id":"OPR_MNG_ORG_ADMIN_MODIFICATION",
                "name":"운영업체 정보관리",
                "link":"",
              },
              {
                "id":"OPR_MNG_ORG_STORE_LIST",
                "isHide":false,
                "name":"입점업체 관리",
                "link":""
              }
            ]
          },

          {
            "name":"계정정보 관리",
            "isHide":false,
            "children":[
              {"id":"OPR_MNG_ACC_ORG_LIST","isHide":false,"name":"업체 계정관리","link":"#"}
            ]
          }
        ]
      },

      {
        "id":"items_mng",
        "name":"상품관리",
        "link":"",
        "isHide":false,
        "children":[
          {
            "name":"분류정보 관리",
            "isHide":false,
            "children":[
              {"id":"ITEMS_MNG_CATEGORY","name":"분류관리","isHide":false,"link":""},
              {"id":"ITEMS_MNG_CATEGORY_BRAND","name":"브랜드관리","isHide":false,"link":""}
            ]
          },

          {
            "name":"상품정보 관리",
            "isHide":false,
            "children":[
              {"id":"ITEMS_MNG_ITEMS_LIST","name":"상품목록","link":""},
              {"id":"ITEMS_MNG_ITEMS_REGISTRATION","isHide":false,"name":"상품등록","link":""},
              {"id":"ITEMS_MNG_ITEMS_TD_LIST","name":"3d상품목록","isHide":false,"link":""}
            ]
          }
        ]
      }
    ]
  };


  // TODO
  //


  APP.info={
    gURL:window.location.protocol+'//'+window.location.host+'/'+'kr', //수정
    gHost:"<?php echo $output['node_api_site_url'];?>", //수정
    gAPIHost:"<?php echo $output['node_api_site_url'];?>", //수정
    gAPIPort:"", //수정
    gHost:"<?php echo $output['node_api_site_url'];?>", //수정
    gLocale:"kr", //수정
    gVersion:"0.0.110", //수정
    imgURL:"http://dev-img.fitnshop.com/images", //수정
    gRegion:"",
    user:{},
    API_FRONT_URL:{},
    table:{
      TABLE_VIEW_RANGE_LIST:['20','40','80','100']
    }
  };

/*

  if( window.location.host.indexOf('localhost')===-1 ){
      if(window.location.host.indexOf('dev')!==-1){
          APP.info.gAPIHost = 'http://dev-mall-api.fitnshop.com';  //dev
      }else if(window.location.host.indexOf('stg')!==-1){
          APP.info.gAPIHost = 'http://stg-mall-api.fitnshop.com';  //stg
      }else{
          APP.info.gAPIHost = 'http://mall-api.fitnshop.com';  //prd
      }
  }else{
      APP.info.gAPIHost = 'http://localhost';  //localtest
      APP.info.gAPIPort = '1337'; //localtest
  }*/


  APP.info.user.account_uuid
  APP.info.user = {
    "account_uuid":"<?=$_SESSION['member_id'];?>",   //수정
    "account_uuid_hash":"",  //수정
    "account_id_hash":"",  //수정
    "purchase_url":"",   //수정
    "client_name": "",   //수정
    "account_type": "admin",
    "login_id": "",  //수정
    "parent_org_id": parseInt("222",10),   //수정
    "org_id": parseInt("222",10),  //수정
    "org_name": "",  //수정
    "contact_name": "",
    "contact_phone": "",
    "contact_mail": "",
    "contact_homepage": "",
    "email_id": "",
    "status": "activated",   //수정
    "language": "KO",  //수정
    "currency": "KRW",   //수정
    "timezone": "+09:00"   //수정
  }

  APP.info.user.timezonecnt = _getCurTimeZone();

  function _getCurTimeZone(){
    var curTimeZone = APP.info.user.timezone;
    if(curTimeZone=='null' || curTimeZone=='undefined' || curTimeZone==null || curTimeZone==undefined || curTimeZone==''){
      curTimeZone = -9;
    }else{
      if( APP.info.user.timezone.substr(0,1)=='+' ){
        curTimeZone =  -1*parseInt(APP.info.user.timezone.substr(1,2));
      }else if( APP.info.user.timezone.substr(0,1)=='-' ){
        curTimeZone = parseInt(APP.info.user.timezone.substr(1,2));
      }else{
        curTimeZone = parseInt(APP.info.user.timezone.substr(0,2));
      }
    }

    return curTimeZone;
  }


  //수정
  APP.info.user.auth = {
    "account_uuid":"","majorOrganization":2,"minorOrganization":2,"masterAccount":2,"organizationAccount":2,
    "license":2,"category":2,"brand":2,"tdItem":2,"item":2,"batchItem":2,"defaultVideo":2,"mirrorVideo":2,"organizationVideo":2
  };


  //수정
  //API_FRONT_URL

  // angular => API_URL_INFO
  APP.info.API_FRONT_URL = {
    GET_3DITEMS_LIST:APP.info.gAPIHost+'/td/list',
    GET_MAKE_CATE_URL:APP.info.gAPIHost+'/category/getCateCSV?account_uuid='+(APP.info.user.account_uuid)
  };


  APP.i18n = {
  };

  APP.setup403Page = function(__container){
    $(document.body).attr('data-page-id','403');
    $(__container || '.content_wrapper').html('<div class="list_table_wrapper"> <div page-location="" title="403 Forbidden" location="" class="form_header" style=""><h3 class="form_header_title" style="">403 Forbidden</h3></div><h4>이 페이지에 접근권한이 없습니다.</h4></div>');
  }

  APP.createNavModel = function createNavModel(__locale, __auth, __NAV_MODEL, __session){
    //TODO : refactorying

    console.log('__auth.majorOrganization: ' ,__auth.majorOrganization);
    console.log('__auth.minorOrganization: ' ,__auth.minorOrganization);
    console.log('__auth.organizationAccount: ' ,__auth.organizationAccount);
    console.log('__auth.category: ' ,__auth.category);
    console.log('__auth.brand: ' ,__auth.brand);
    console.log('__auth.item: ' ,__auth.item);
    console.log('__auth.tdItem: ' ,__auth.tdItem);


    var majorOrganization = (is.existy(__auth) && is.existy(__auth.majorOrganization))?parseInt(__auth.majorOrganization,10):1;

    var tmpOprMngLink = '/' + (__locale) + ((majorOrganization>=2)? __NAV_MODEL.menu.OPR_MNG_ORG_ADMIN_MODIFICATION.link:__NAV_MODEL.menu.OPR_MNG_ORG_ADMIN_CONFIRMATION.link);
    var tmpOprMngStoreLink = '/' + (__locale) + __NAV_MODEL.menu.OPR_MNG_ORG_STORE_LIST.link;
    var tmpParamStoreLink = '';

    if(__session.account_type==='shop') {
      tmpParamStoreLink = '?account_uuid='+(__session.account_uuid)+'&org_id='+(__session.org_id);
      tmpOprMngStoreLink = '/' + (__locale) + __NAV_MODEL.menu.OPR_MNG_ORG_STORE_MODIFICATION.link+tmpParamStoreLink;
      if( parseInt(__auth.minorOrganization,10)===1 ){
        tmpOprMngStoreLink = '/' + (__locale) + __NAV_MODEL.menu.OPR_MNG_ORG_STORE_CONFIRMATION.link+tmpParamStoreLink;
      }
      tmpOprMngLink = tmpOprMngStoreLink;
    }

    var menuArr = [
      {
        id:'opr_mng',
        name:'운영관리',
        link: tmpOprMngLink,
        children:[
          {name:'업체정보 관리',
            isHide:( ((parseInt(__auth.majorOrganization,10)<1) && (parseInt(__auth.minorOrganization,10)<1))?true:false ),
            children:[
              (function(){

                return {
                  isHide:((__session.account_type==='shop')?true:false),
                  id:'OPR_MNG_ORG_ADMIN_MODIFICATION',
                  name:'운영업체 정보관리',
                  link: tmpOprMngLink
                };
              })(),
              {
                id:'OPR_MNG_ORG_STORE_LIST',
                isHide:((parseInt(__auth.minorOrganization,10)<1)?true:false),
                name:__NAV_MODEL.menu.OPR_MNG_ORG_STORE_LIST.name,
                link:tmpOprMngStoreLink
              }

            ]},
          {name:'계정정보 관리',
            isHide:((parseInt(__auth.organizationAccount,10)<1)?true:false),
            children:[
              {
                id:'OPR_MNG_ACC_ORG_LIST',
                isHide:((parseInt(__auth.organizationAccount,10)<1)?true:false),
                name:__NAV_MODEL.menu.OPR_MNG_ACC_ORG_LIST.name,
                link:'/' + (__locale) + __NAV_MODEL.menu.OPR_MNG_ACC_ORG_LIST.link
              }
            ]}
        ]
      },

      {
        id:'items_mng',
        name: '상품관리',
        link: '/' + (__locale) + __NAV_MODEL.menu.ITEMS_MNG_ITEMS_LIST.link,

        children:[
          {name:'분류정보 관리',
            isHide:((__session.account_type==='shop' || ((parseInt(__auth.category,10)<1) && (parseInt(__auth.brand,10)<1)))?true:false),
            children:[
              {
                id:'ITEMS_MNG_CATEGORY',
                name:__NAV_MODEL.menu.ITEMS_MNG_CATEGORY.name,
                isHide:((__session.account_type==='shop' || (parseInt(__auth.category,10)<1))?true:false),
                link:'/' + (__locale) + __NAV_MODEL.menu.ITEMS_MNG_CATEGORY.link
              },
              {
                id:'ITEMS_MNG_CATEGORY_BRAND',
                name:__NAV_MODEL.menu.ITEMS_MNG_CATEGORY_BRAND.name,
                isHide:((__session.account_type==='shop' || (parseInt(__auth.brand,10)<1))?true:false),
                link:'/' + (__locale) + __NAV_MODEL.menu.ITEMS_MNG_CATEGORY_BRAND.link
              }

            ]},
          {name:'상품정보 관리',
            isHide:( ((parseInt(__auth.item,10)<1) && (parseInt(__auth.tdItem,10)<1))?true:false ),
            children:[
              {
                id:'ITEMS_MNG_ITEMS_LIST',
                name:__NAV_MODEL.menu.ITEMS_MNG_ITEMS_LIST.name,
                isHide:((parseInt(__auth.item,10)<1)?true:false),
                link:'/' + (__locale) + __NAV_MODEL.menu.ITEMS_MNG_ITEMS_LIST.link
              },
              {
                id:'ITEMS_MNG_ITEMS_REGISTRATION',
                isHide:((parseInt(__auth.item,10)<=1)?true:false),
                name:__NAV_MODEL.menu.ITEMS_MNG_ITEMS_REGISTRATION.name,
                link:'/' + (__locale) + __NAV_MODEL.menu.ITEMS_MNG_ITEMS_REGISTRATION.link
              },
              {
                id:'ITEMS_MNG_ITEMS_TD_LIST',
                name:__NAV_MODEL.menu.ITEMS_MNG_ITEMS_TD_LIST.name,
                isHide:((parseInt(__auth.tdItem,10)<1)?true:false),
                link:'/' + (__locale) + __NAV_MODEL.menu.ITEMS_MNG_ITEMS_TD_LIST.link
              }
            ]}
        ]
      }
    ];

    console.log('LNB' , menuArr);

    return menuArr;
  }

</script>


<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/angular_base_module/cnbiz_module_not_jquery.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/angular_base_module/production.min.js" charset="utf-8"></script>
<!-- td popup js end -->


<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<style type="text/css">
#fixedNavBar { filter:progid:DXImageTransform.Microsoft.gradient(enabled='true',startColorstr='#CCFFFFFF', endColorstr='#CCFFFFFF');background:rgba(255,255,255,0.8); width: 90px; margin-left: 510px; border-radius: 4px; position: fixed; z-index: 999; top: 172px; left: 50%;}
#fixedNavBar h3 { font-size: 12px; line-height: 24px; text-align: center; margin-top: 4px;}
#fixedNavBar ul { width: 80px; margin: 0 auto 5px auto;}
#fixedNavBar li { margin-top: 5px;}
#fixedNavBar li a { font-size: 12px; line-height: 20px; background-color: #F5F5F5; color: #999; text-align: center; display: block;  height: 20px; border-radius: 10px;}
#fixedNavBar li a:hover { color: #FFF; text-decoration: none; background-color: #27a9e3;}
</style>



<!-- td popup css -->
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/angular_base_module/production.min.css"  />
<style>

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding:4px;
  }

  .td_popup_wrapper{
    position: absolute;
    display: none;
    width:90%;
    z-index:10000;
    border-radius: 8px;
    border:2px solid #dddddd;
    -webkit-box-shadow: 10px 10px 48px -5px rgba(0,0,0,0.4);
    -moz-box-shadow: 10px 10px 48px -5px rgba(0,0,0,0.4);
    box-shadow: 10px 10px 48px -5px rgba(0,0,0,0.4);
  }

  .opacity_bg{
    position: absolute;
    display: block;
    width:100%;
    height:100%;
    background-color: rgba(255,255,255,1);
    border-radius: 8px;
  }

  .template_table_list table tr td, .org_admin_registration table tr td, .org_admin_modification table tr td, .org_store_registration table tr td, .org_store_modification table tr td, .org_admin table tr td, .org_store table tr td{
    height: 24px;
  }

  .list-form-wrapper .list-form-inner label {
    margin-right: 30px;
  }

  .list-form-inner input[type=text]{
    height:32px;
  }

  .thumbnail_prev_wrapper{
    float: right;
    display: block;
    position: relative;
    width: 28%;
  }

  .thumbnail_prev_cont{
    position: relative;
    display: block;
    top: 0;
    left: 0;
    width: 100%;
    height: auto;
  }

  .thumbnail_prev_img{

  }

  .btn-xs, .btn-sm{
    width:56px;
    height:30px;
    border-width:0;
  }

  .td_popup_body_table{
    font-size:12px;
  }

  .td_preview_modal_container .modal-content{
    margin-top:10px;
    width:100% !important;
    height:385px;
    min-width: 0 !important;
    box-shadow: none !important;
    -webkit-box-shadow: none !important;
  }

  .td_preview_modal_container .modal-content .modal-body .modal-body-inner .td_img_cont{
    width:100%;
    height:100% !important;
    background-color: black;
  }

  .list-form-wrapper .form-inline{display:inline-flex;}

  .modal-body-inner{
    min-height: 328px !important;
  }

  .modal-footer{
    padding:0 !important;
    border-top:0;
    margin-top:-21px;
  }

  .pager{
    margin:0 0 !important;
  }

  body .list-form-search-btn .btn{
      height:auto;
  }

  .on_td{
    background-color: #f1f1f1;
  }

  .ui-datepicker{
    width:250px;
  }

  .td_preview_modal_container .modal-content .modal-body{
      pointer-events: none;
      width:auto;
  }


</style>

<div id="fixedNavBar">
<h3>퀵메뉴</h3>
  <ul>
    <li><a id="demo1Btn" href="#demo1" class="demoBtn">기본정보</a></li>
    <li><a id="demo6Btn" href="#demo6" class="demoBtn">3D정보</a></li>
    <li><a id="demo2Btn" href="#demo2" class="demoBtn">상세내용</a></li>
    <li><a id="demo3Btn" href="#demo3" class="demoBtn">출시예정 상품</a></li>
    <li><a id="demo4Btn" href="#demo4" class="demoBtn">배송정보</a></li>
    <li><a id="demo5Btn" href="#demo5" class="demoBtn">기타정보</a></li>
  </ul>
</div>
<?php if ($output['edit_goods_sign']) {?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<?php } else {?>
<ul class="add-goods-step">
  <li><i class="icon icon-list-alt"></i>
    <h6>STEP.1</h6>
    <h2>카테고리 선택</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li class="current"><i class="icon icon-edit"></i>
    <h6>STEP.2</h6>
    <h2>상세내용 입력</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-camera-retro "></i>
    <h6>STEP.3</h6>
    <h2>이미지 업로드</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-ok-circle"></i>
    <h6>STEP.4</h6>
    <h2>등록 완료</h2>
  </li>
</ul>
<?php }?>
<div class="item-publish">
  <form method="post" id="goods_form" action="<?php if ($output['edit_goods_sign']) { echo urlShop('store_goods_online', 'edit_save_goods');} else { echo urlShop('store_goods_add', 'save_goods');}?>">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="commonid" value="<?php echo $output['goods']['goods_commonid'];?>" />
    <input type="hidden" name="type_id" value="<?php echo $output['goods_class']['type_id'];?>" />
    <input type="hidden" name="ref_url" value="<?php echo $_GET['ref_url'] ? $_GET['ref_url'] : getReferer();?>" />
    <input type="hidden" name="g_cur" value="<?php echo C('site_cur');?>" />
    <div class="ncsc-form-goods">
      <h3 id="demo1">상품기본정보</h3>
      <dl>
        <dt><?php echo '카테고리'.$lang['nc_colon'];?></dt>
        <dd id="gcategory"> <?php echo $output['goods_class']['gc_tag_name_ko'];?> <a class="ncsc-btn" href="<?php if ($output['edit_goods_sign']) { echo urlShop('store_goods_online', 'edit_class', array('commonid' => $output['goods']['goods_commonid'], 'ref_url' => getReferer())); } else { echo urlShop('store_goods_add', 'add_step_one'); }?>">수정</a>
          <input type="hidden" id="cate_id" name="cate_id" value="<?php echo $output['goods_class']['gc_id'];?>" class="text" />
          <input type="hidden" name="cate_name" value="<?php echo $output['goods_class']['gc_tag_name'];?>" class="text"/>
          <input type="hidden" name="cate_name_ko" value="<?php echo $output['goods_class']['gc_tag_name_ko'];?>" class="text"/>
        </dd>
      </dl>
      <dl>
        <dt class="required"><i class="required">*</i>브랜드<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <select name="b_id">
            <option value="0">선택하세요</option>
            <?php if(is_array($output['brand_list']) && !empty($output['brand_list'])){?>
            <?php foreach($output['brand_list'] as $val) { ?>
            <option value="<?php echo $val['brand_id']; ?>" <?php if ($val['brand_id'] == $output['goods']['brand_id']) { ?>selected="selected"<?php } ?>><?php echo $val['brand_name']; ?></option>
            <?php } ?>
            <?php }?>
          </select>
          <a uri="index.php?act=store_brand&op=brand_add" dialog_width="580" dialog_id="my_goods_brand_apply" dialog_title="브랜드신청" nc_type="dialog" class="ncsc-btn" href="javascript:void(0)">추가</a>
          <input type="hidden" name="b_name" value="<?php echo $output['goods']['brand_name'];?>" />
          <span></span>
          <p class="hint">브랜드로 검색하는 중국소비자 비중으로 반드시 선택해주시길 바랍니다.<br />위 리스트에 브랜드가 없으시면 옆의 추가버튼으로 추가후 상품등록을 진행해주시길 바랍니다.</p>
        </dd>
      </dl>
      <dl>
        <dt class="required"><i class="required">*</i>상품명(한국어)<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input name="g_name_ko" type="text" class="text w400" value="<?php echo $output['goods']['goods_name_ko']; ?>" />
          <span></span>
          <p class="hint">한국어 상품명은 소비자가 주문시 판매 관리자에서 한글로 보여주기 위해서입니다.</p>
        </dd>
      </dl>
      <dl>
        <dt class="required"><?php echo "상품명(중국어)".$lang['nc_colon'];?></dt>
        <dd>
          <input name="g_name" type="text" class="text w400" value="<?php echo $output['goods']['goods_name']; ?>" />
          <span></span>
          <p class="hint">중국어 상품명은 소비자 페이지에서 노출됩니다.</p>
        </dd>
      </dl>
      <dl>
        <dt class="required">간략설명(한국어)<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input name="g_jingle_ko" type="text" class="text w400" value="<?php echo $output['goods']['goods_jingle_ko']; ?>" />
          <span></span>
          <p class="hint">한국어 간략설명은 판매관리자 페이지에서만 노출됩니다.</p>
        </dd>
      </dl>
      <dl>
        <dt class="required">간략설명(중국어)<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input name="g_jingle" type="text" class="text w400" value="<?php echo $output['goods']['goods_jingle']; ?>" />
          <span></span>
          <p class="hint">중국어 간략설명은 상품 상세 페이지 상품명 아래에 노출됩니다.<br />설명의 인기에 따라 중국 빠이뚜,구글 검색엔진의 상단 위치에 노출될 확율이 큽니다.</p>
        </dd>
      </dl>



      <dl>
        <dt nc_type="no_spec">무게<?php echo $lang['nc_colon'];?></dt>
        <dd nc_type="no_spec">
          <input name="g_weight" id="g_weight" value="<?php echo $output['goods']['goods_weight']; ?>" type="text"  class="text w60" /><em class="add-on"><strong>Kg</strong></em>
          <p class="hint">국제 배송으로 인해 무게 설정은 아주 중요한 단계이므로 기본 무게에 뽁뽁이 무게까지 포함해서 조금 추가하시길 권장합니다.</p>
        </dd>
      </dl>
      <dl>
        <dt nc_type="no_spec"><i class="required">*</i>판매가<?php echo $lang['nc_colon'];?><br />환율(<strong style="color:red; font-weight:normal;"><?php echo C('site_cur');?></strong>)</dt>
        <dd nc_type="no_spec">
          <input name="g_price_ko" id="g_price_ko" value="<?php if(!empty($output['goods']['goods_price_ko'])){ echo floor($output['goods']['goods_price_ko']);}; ?>" type="text"  class="text w60" /><em class="add-on"><strong>원</strong></em>
          <input name="g_price" id="g_price" readonly="readonly" style="background:#E7E7E7 none;" value="<?php if(!empty($output['goods']['goods_price'])){ echo floor($output['goods']['goods_price']);}; ?>" type="text"  class="text w60" /><em class="add-on"><strong>위안</strong></em>
          <span></span>
          <p class="hint">판매가는 실제 유이한궈에서 판매되는 가격입니다.</p>
        </dd>
      </dl>
      <dl>
        <dt>소비자가<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input name="g_marketprice_ko" id="g_marketprice_ko" value="<?php if(!empty($output['goods']['goods_marketprice_ko'])){ echo floor($output['goods']['goods_marketprice_ko']);}; ?>" type="text" class="text w60" /><em class="add-on"><strong>원</strong></em>

          <input name="g_marketprice" id="g_marketprice" readonly="readonly" style="background:#E7E7E7 none;" value="<?php if(!empty($output['goods']['goods_marketprice'])){ echo floor ($output['goods']['goods_marketprice']);}; ?>" type="text"  class="text w60" /><em class="add-on"><strong>위안</strong></em>
          <p class="hint">소비자 가격은 실제 매장에서 판매하는 가격을 입력하여 싸게 구매한다는 느낌을 유도하기 위해서입니다.</p>
        </dd>
      </dl>
      <dl>
        <dt>공급가<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input name="g_costprice_ko" id="g_costprice_ko" value="<?php if(!empty($output['goods']['goods_costprice_ko'])){ echo floor($output['goods']['goods_costprice_ko']);}; ?>" type="text" class="text w60" /><em class="add-on"><strong>원</strong></em>
          <input name="g_costprice" id="g_costprice" readonly="readonly" style="background:#E7E7E7 none;" value="<?php if(!empty($output['goods']['goods_costprice'])){ echo floor($output['goods']['goods_costprice']);}; ?>" type="text"  class="text w60" /><em class="add-on"><strong>위안</strong></em>
          <p class="hint">공급 가격은 사업자가 구매하는 모든 상품의 공급가를 뜻하며 실제 공급가를 입력하여 주기시 바랍니다.  입력은 선택 사항이며, 입력하시더라도 페이지 화면 상에는 노출되지 않습니다.</p>
        </dd>
      </dl>
      <input name="g_discount" value="<?php echo $output['goods']['goods_discount']; ?>" type="hidden"/>
      <?php if(is_array($output['spec_list']) && !empty($output['spec_list'])){?>
      <?php $i = '0';?>
      <?php foreach ($output['spec_list'] as $k=>$val){?>
      <dl nc_type="spec_group_dl_<?php echo $i;?>" cnbiztype="spec_group_dl" class="spec-bg" <?php if($k == '1'){?>spec_img="t"<?php }?>>
        <dt>
          <input name="sp_name[<?php echo $k;?>]" type="hidden" class="text w60 tip2 tr" title="기본카테고리 명칭을 등록하여 주십시오. (최대 4자)" value="<?php if (isset($output['goods']['spec_name'][$k])) { echo $output['goods']['spec_name'][$k].'|'.$output['goods']['spec_name_ko'][$k];} else {echo $val['sp_name'].'|'.$val['sp_name_ko'];}?>" maxlength="100" cnbiztype="spec_name" data-param="{id:<?php echo $k;?>,name:'<?php echo $val['sp_name'].'|'.$val['sp_name_ko'];?>'}"/>
          <?php if (isset($output['goods']['spec_name'][$k])) { echo $output['goods']['spec_name_ko'][$k];} else {echo $val['sp_name_ko'];}?>
          <?php echo $lang['nc_colon']?></dt>
        <dd <?php if($k == '1'){?>cnbiztype="sp_group_val"<?php }?>>
          <ul class="spec">
            <?php if(is_array($val['value'])){?>
            <?php foreach ($val['value'] as $v) {?>
            <li><span cnbiztype="input_checkbox">
              <input type="checkbox" value="<?php echo $v['sp_value_name'];?>" nc_type="<?php echo $v['sp_value_id'];?>" <?php if($k == '1'){?>class="sp_val"<?php }?> name="sp_val[<?php echo $k;?>][<?php echo $v['sp_value_id']?>]">
              </span><span cnbiztype="pv_name"><?php echo $v['sp_value_name'];?></span></li>
            <?php }?>
            <?php }?>
            <li data-param="{gc_id:<?php echo $output['goods_class']['gc_id'];?>,sp_id:<?php echo $k;?>,url:'<?php echo urlShop('store_goods_add', 'ajax_add_spec');?>'}">
              <div cnbiztype="specAdd1"><a href="javascript:void(0);" class="ncsc-btn" cnbiztype="specAdd"><i class="icon-plus"></i>옵션추가</a></div>
              <div cnbiztype="specAdd2" style="display:none;"><input class="text w60" type="text" placeholder="옵션명" maxlength="20"><a href="javascript:void(0);" cnbiztype="specAddSubmit" class="ncsc-btn ncsc-btn-acidblue ml5 mr5">확인</a><a href="javascript:void(0);" cnbiztype="specAddCancel" class="ncsc-btn ncsc-btn-orange">취소</a></div>
            </li>
          </ul>
        </dd>
      </dl>
      <?php $i++;?>
      <?php }?>
      <?php }?>
      <dl nc_type="spec_dl" class="spec-bg" style="display:none">
        <dt>옵션설정<?php echo $lang['nc_colon'];?></dt>
        <dd class="spec-dd">
          <table border="0" cellpadding="0" cellspacing="0" class="spec_table">
            <thead>
              <?php if(is_array($output['spec_list']) && !empty($output['spec_list'])){?>
              <?php foreach ($output['spec_list'] as $k=>$val){?>
            <th cnbiztype="spec_name_<?php echo $k;?>"><?php if (isset($output['goods']['spec_name'][$k])) { echo $output['goods']['spec_name_ko'][$k];} else {echo $val['sp_name_ko'];}?></th>
              <?php }?>
              <?php }?>
              <th class="w100">무게
                <div class="batch"><i class="icon-edit" title="일괄설정"></i>
                  <div class="batch-input" style="display:none;">
                    <h6>일괄가격설정：</h6>
                    <a href="javascript:void(0)" class="close">X</a>
                    <input name="" type="text" class="text price" />
                    <a href="javascript:void(0)" class="ncsc-btn-mini" data-type="weight">설정</a><span class="arrow"></span></div>
                </div></th>
              <th class="w100">공급가
                <div class="batch"><i class="icon-edit" title="일괄설정"></i>
                  <div class="batch-input" style="display:none;">
                    <h6>일괄가격설정：</h6>
                    <a href="javascript:void(0)" class="close">X</a>
                    <input name="" type="text" class="text price" />
                    <a href="javascript:void(0)" class="ncsc-btn-mini" data-type="costprice_ko">설정</a><span class="arrow"></span></div>
                </div></th>
              <th class="w100">소비자가
                <div class="batch"><i class="icon-edit" title="일괄설정"></i>
                  <div class="batch-input" style="display:none;">
                    <h6>일괄가격설정：</h6>
                    <a href="javascript:void(0)" class="close">X</a>
                    <input name="" type="text" class="text price" />
                    <a href="javascript:void(0)" class="ncsc-btn-mini" data-type="marketprice_ko">설정</a><span class="arrow"></span></div>
                </div></th>
              <th class="w100"><span class="red">*</span>가격
                <div class="batch"><i class="icon-edit" title="일괄설정"></i>
                  <div class="batch-input" style="display:none;">
                    <h6>일괄가격설정：</h6>
                    <a href="javascript:void(0)" class="close">X</a>
                    <input name="" type="text" class="text price" />
                    <a href="javascript:void(0)" class="ncsc-btn-mini" data-type="price_ko">설정</a><span class="arrow"></span></div>
                </div></th>
              <th class="w60"><span class="red">*</span>재고
                <div class="batch"><i class="icon-edit" title="일괄설정"></i>
                  <div class="batch-input" style="display:none;">
                    <h6>일괄재고설정：</h6>
                    <a href="javascript:void(0)" class="close">X</a>
                    <input name="" type="text" class="text stock" />
                    <a href="javascript:void(0)" class="ncsc-btn-mini" data-type="stock">설정</a><span class="arrow"></span></div>
                </div></th>
              <th class="w100">상품코드</th>
                </thead>
            <tbody nc_type="spec_table">
            </tbody>
          </table>
        </dd>
      </dl>
      <dl>
        <dt nc_type="no_spec"><i class="required">*</i><?php echo "재고".$lang['nc_colon'];?></dt>
        <dd nc_type="no_spec">
          <input name="g_storage" value="<?php echo $output['goods']['g_storage']; ?>" type="text" class="text w60" />
          <span></span>
          <p class="hint">실제 재고를 사용하셔서 소비자가 수취확인시 재고량 감소 혹은 반품에 대한 재고관리를 할 수 있습니다.<br />재고가 부족시 고객님의 이메일 및 문자로 재고 부족이라는 메세지가 발송됩니다.</p>
        </dd>
      </dl>
      <dl>
        <dt nc_type="no_spec">상품코드<?php echo $lang['nc_colon'];?></dt>
        <dd nc_type="no_spec">
          <p>
            <input name="g_serial" value="<?php echo $output['goods']['goods_serial']; ?>" type="text"  class="text"  />
          </p>
          <p class="hint">판매자가 상품에 대한 유일한 상품 SUK코드입니다, 소비자 페이지에는 노출되지 않습니다.</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>이미지<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <div class="ncsc-goods-default-pic">
            <div class="goodspic-uplaod">
              <div class="upload-thumb"> <img cnbiztype="goods_image" src="<?php echo thumb($output['goods'], 240);?>"/> </div>
              <p class="hint">업로드한 이미지는 상품의 메인이미지가 되며, JPG/GIF/PNG형식 파일 업로드 혹은 앨범에서 선택 가능합니다.<br>사이즈는 <font color="red"> 800 X 800px</font>이상이 적당하며, 크기는 <font color="red"> 1M</font>를 초과하지 않는 정사각의 이미지가 가능합니다. <br>업로드 후 사진은 자동으로 앨범의 기본카테고리에 저장됩니다.<?php printf($lang['store_goods_step2_description_two'],intval(C('image_max_filesize'))/1024);?></p>
              <input type="hidden" name="image_path" id="image_path" cnbiztype="goods_image" value="<?php echo $output['goods']['goods_image']?>" />
              <span></span>
              <div class="handle">
                <div class="ncsc-upload-btn"> <a href="javascript:void(0);"><span>
                  <input type="file" hidefocus="true" size="1" class="input-file" name="goods_image" id="goods_image">
                  </span>
                  <p><i class="icon-upload-alt"></i>업로드</p>
                  </a> </div>
                <a class="ncsc-btn" cnbiztype="show_image" href="<?php echo urlShop('store_album', 'pic_list', array('item'=>'goods'));?>"><i class="icon-picture"></i>앨범선택</a> <a href="javascript:void(0);" cnbiztype="del_goods_demo" class="ncsc-btn ml5" style="display: none;"><i class="icon-circle-arrow-up"></i>닫기</a></div>
            </div>
          </div>
          <div id="demo"></div>
        </dd>
      </dl>

    <dl>
      <dt><i class="required">*</i>HS코드： </dt>
      <dd>
<?php if(!empty($output['goods']['hscodeinf']['article_id']) && $output['goods']['hscodeinf']['article_id']>0):?>
      <div cnbiztype="hscode_info" class="selected-group-goods " style="display:block; width:780px !important;">
<?php else:?>
      <div cnbiztype="hscode_info" class="selected-group-goods " style="display:none; width:780px !important;">

<?php endif;?>
<style type="text/css">
.selectedtable{
border:1px solid #cad9ea;
color:#666;
}
.selectedtable th {
background-repeat:repeat-x;
height:30px;
font-weight:bold;
}
.selectedtable td,.selectedtable th{
border:1px solid #cad9ea;
padding:0 1em 0;
}
.selectedtable tr.alter{
background-color:#f5fafe;
} 
</style>

  <table class="selectedtable" width="100%">
  <col width="4%" />
  <col width="15%" />
  <col />
  <col width="35%" />
    <tr>
      <td><i class="icon-check" style="color:red;"></i></td>
      <td id="chscode_title"><?php echo $output['goods']['hscodeinf']['article_title'];?></td>
      <td id="chscode_name"><?php echo $output['goods']['hscodeinf']['article_abstract'];?></td>
      <td id="chscode_des"><?php echo $output['goods']['hscodeinf']['article_content'];?></td>
    </tr>
</table>
</div>
      <a href="javascript:void(0);" id="btn_show_search_goods" class="ncsc-btn ncsc-btn-acidblue">HS코드 검색</a>
      <input id="hscode_number" name="hscode_number" type="hidden" value="<?php echo $output['goods']['hscodeinf']['article_id'];?>"/>
      <span></span>
      <div id="div_search_goods" class="div-goods-select mt10" style="display: none;">
          <table class="search-form">
              <tr>
                  <td class="w70" style="padding-left:10px;">
                      <select name="hsclass" id="hsclass">
                        <option value="0">카테고리를 선택하세요</option>
                        <?php foreach($output['hscode_category'] as $val):?>
                        <option value="<?php echo $val['class_id'];?>"><?php echo $val['class_name'];?></option>
                      <?php endforeach;?>
                      </select>
                  </td>              
                  <td class="w160" style="padding-left:10px;">
                      <input id="search_hsname" type="text w150" class="text" name="hscode_name" value=""/>
                  </td>
                  <td class="w70 tc">
                      <a href="javascript:void(0);" id="btn_search_hscode" class="ncsc-btn"/><i class="icon-search"></i>검색</a></td>
                    <td class="w10"></td>
                    <td>
                    </td>
                </tr>
            </table>
            <div id="div_hscode_search_result" class="search-result" style="width:760px; margin-top:15px;"></div>
            <a id="btn_hide_search_goods" class="close" href="javascript:void(0);">X</a>
        </div>
        </dd>
    </dl>
      <dl>
        <dt>성별/성인/아동</dt>
        <dd>
          <ul class="ncsc-form-radio-list" style="margin: 0;">
            <li>
              <label><input type="radio" class="radio" name="gender" value="woman"><span>여(성인)</span></label>
            </li>
            <li>
              <label><input type="radio" class="radio" name="gender" value="man"><span>남(성인)</span></label>
            </li>
            <li>
              <label><input type="radio" class="radio" name="gender" value="unisex"><span>아동</span></label>
            </li>
            <li class="unisex" style="display: none;">
              <span>(</span> &nbsp;
              <label><input type="checkbox" name="man_kids" value=""> 남아</label>
            </li>
            <li class="unisex" style="display: none;">
              <label><input type="checkbox" name="woman_kids" value=""> 여아</label>
              &nbsp;<span>)</span>
            </li>
          </ul>
        </dd>
      </dl>

      <h3 id="demo6">3D 의상정보</h3>
        <table class="ncsc-default-table">
          <thead>
          <tr>
            <th style="width: 200px;">3D의상코드</th>
            <th>3D의상명</th>
            <th style="width: 130px;">3D의상썸네일</th>
            <th style="width: 130px;">제작일</th>
            <th style="width: 100px;">대표지정</th>
            <th style="width: 64px;">관리</th>
          </tr>
          </thead>
          <tbody id="td_table">

          <?php if (count($output['3dc'])>0) { ?>
            <?php foreach ($output['3dc'] as $key => $value) { ?>
              <?php foreach ($output['3dc'][$key]->td_list as $k => $v) { ?>
              <tr style='border-bottom: 1px solid #e6e6e6;'>
                <td style='width: 200px;'><?php echo $output['3dc'][$key]->td_list[$k]->code ?>
                  <input type=hidden name='td_code' value="<?php echo $output['3dc'][$key]->td_list[$k]->code ?>"/>
                </td>
                <td><?php echo $output['3dc'][$key]->td_list[$k]->name ?></td>
                <td style='width: 130px;'>
                  <img src="<?php echo $output['3dc'][$key]->td_list[$k]->td_fitting_files->thumbnail->url ?>"
                       alt='3d thumbnail image' style='width: 106px; height: 106px'/></td>
                <td style='width: 130px;'><?php echo @date('Y-m-d', $output['3dc'][$key]->td_list[$k]->regist_date) ?></td>
                <td style='width: 100px;'>
                  <input type='radio' name='repre' value="<?php echo $output['3dc'][$key]->td_list[$k]->id ?>"/></td>
                <td style='width: 64px;'>
                  <a id="td_remove_<?php echo $output['3dc'][$key]->td_list[$k]->id ?>" class='ncsc-btn'
                     href='javascript:remove(<?php echo $output['3dc'][$key]->td_list[$k]->id ?>)'>삭제</a>
                </td>
              </tr>
            <?php } ?>
            <?php } ?>
          <?php } else { ?>
            <tr>
              <td colspan="20" class="norecord"><div class=""><span>내용이 없습니다.</span></div></td>
            </tr>
          <?php } ?>

          </tbody>
        </table>
        <a href="javascript:void(0);"
           id="td_add"
           class="ncsc-btn ncsc-btn-acidblue"
           style="float: right; margin: 10px 10px 10px 0;">등록</a>
      <input type="hidden" name="td_ids" id="td_ids" value="">
      <input type="hidden" name="td_rep_ids" id="td_rep_ids" value=""/>
      <div style="clear: both;"></div>


      <h3 id="demo2">상세설명</h3>

      <?php if(is_array($output['attr_list']) && !empty($output['attr_list'])){?>
      <dl>
        <dt><?php echo "상품속성".$lang['nc_colon']; ?></dt>
        <dd>
          <?php foreach ($output['attr_list'] as $k=>$val){?>
          <span class="mr30">
          <label class="mr5"><?php echo $val['attr_name_ko']?></label>
          <input type="hidden" name="attr[<?php echo $k;?>][name]" value="<?php echo $val['attr_name']?>" />
          <?php if(is_array($val) && !empty($val)){?>
          <select name="" attr="attr[<?php echo $k;?>][__NC__]" nc_type="attr_select">
            <option value='무제한' nc_type='0'>무제한</option>
            <?php foreach ($val['value'] as $v){?>
            <option value="<?php echo $v['attr_value_name']?>" <?php if(isset($output['attr_checked']) && in_array($v['attr_value_id'], $output['attr_checked'])){?>selected="selected"<?php }?> nc_type="<?php echo $v['attr_value_id'];?>"><?php echo $v['attr_value_name_ko'];?></option>
            <?php }?>
          </select>
          <?php }?>
          </span>
          <?php }?>
        </dd>
      </dl>
      <?php }?>
      <dl>
        <dt>상품내용<?php echo $lang['nc_colon'];?></dt>
        <dd id="ncProductDetails">
          <div class="tabs">
            <ul class="ui-tabs-nav">
              <li class="ui-tabs-selected"><a href="#panel-1"><i class="icon-desktop"></i> 웹</a></li>
              <li class="selected"><a href="#panel-2"><i class="icon-mobile-phone"></i>모바일</a></li>
            </ul>
            <div id="panel-1" class="ui-tabs-panel">
              <?php showEditor('g_body',$output['goods']['goods_body'],'100%','480px','visibility:hidden;',"false",$output['editor_multimedia']);?>
              <div class="hr8">
                <div class="ncsc-upload-btn"> <a href="javascript:void(0);"><span>
                  <input type="file" hidefocus="true" size="1" class="input-file" name="add_album_ko" id="add_album_ko" multiple="multiple">
                  </span>
                  <p><i class="icon-upload-alt" data_type="0" cnbiztype="add_album_i_ko"></i>업로드</p>
                  </a> </div>
                <a class="ncsc-btn mt5" cnbiztype="show_desc_ko" href="index.php?act=store_album&op=pic_list&item=des"><i class="icon-picture"></i>앨범에서선택</a> <a href="javascript:void(0);" cnbiztype="del_desc_ko" class="ncsc-btn mt5" style="display: none;"><i class=" icon-circle-arrow-up"></i>앨범 닫기</a> </div>
              <p id="des_demo_ko"></p>
            </div>
            <div id="panel-2" class="ui-tabs-panel ui-tabs-hide">
              <div class="ncsc-mobile-editor">
                <div class="pannel">
                  <div class="size-tip">모바일에서 노출되는 내용입니다.</div>
                  <div class="control-panel" cnbiztype="mobile_pannel_ko">
                    <?php if (!empty($output['goods']['mb_body_ko'])) {?>
                    <?php foreach ($output['goods']['mb_body_ko'] as $val) {?>
                    <?php if ($val['type'] == 'text') {?>
                    <div class="module m-text">
                      <div class="tools"><a cnbiztype="mp_up_ko" href="javascript:void(0);">위로가기</a><a cnbiztype="mp_down_ko" href="javascript:void(0);">아래로가기
                      </a><a cnbiztype="mp_edit_ko" href="javascript:void(0);">편집</a><a cnbiztype="mp_del_ko" href="javascript:void(0);">삭제</a></div>
                      <div class="content">
                        <div class="text-div"><?php echo $val['value'];?></div>
                      </div>
                      <div class="cover"></div>
                    </div>
                    <?php }?>
                    <?php if ($val['type'] == 'image') {?>
                    <div class="module m-image">
                      <div class="tools"><a cnbiztype="mp_up_ko" href="javascript:void(0);">위로가기</a><a cnbiztype="mp_down_ko" href="javascript:void(0);">아래로가기</a><a cnbiztype="mp_rpl_ko" href="javascript:void(0);">교체</a><a cnbiztype="mp_del_ko" href="javascript:void(0);">삭제</a></div>
                      <div class="content">
                        <div class="image-div"><img src="<?php echo $val['value'];?>"></div>
                      </div>
                      <div class="cover"></div>
                    </div>
                    <?php }?>
                    <?php }?>
                    <?php }?>
                  </div>
                  <div class="add-btn">
                    <ul class="btn-wrap">
                      <li><a href="javascript:void(0);" cnbiztype="mb_add_img_ko"><i class="icon-picture"></i>
                        <p>이미지</p>
                        </a></li>
                      <li><a href="javascript:void(0);" cnbiztype="mb_add_txt_ko"><i class="icon-font"></i>
                        <p>텍스트</p>
                        </a></li>
                    </ul>
                  </div>
                </div>
                <div class="explain">
                  <dl>
                    <dt style="width:100%; text-align:left;">1. 기본사항: </dt>
                    <dd>1) 이미지+텍스트, 이미지 최대 20장, 글자수 최대 5000자；</dd>
                    <dd>모든 사진은 상품과 관련된 사진만 업로드 하여 주십시오.</dd>
                  </dl><dl>
                    <dt style="width:100%; text-align:left;">2. 이미지:<br /></dt>
                    <dd>1) 폭 480~620 px, 높이 최대 960px；</dd>
                    <dd>2) 가능한 파일：JPG\JEPG\GIF\PNG；</dd>
                    <dd>ex) 폭이 480,높이 960의  JPG 사진 업로드</dd>
                  </dl><dl>
                    <dt style="width:100%; text-align:left;">3. 텍스트:</dt>
                    <dd>（1）최대 500자, 구두점, 특수문자는 한 글자에 해당；</dd>
                    <dd>너무 많은 글자를 입력하시면 뚜렷해 보이지 않을 수도 있습니다.</dd>
                  </dl>
                </div>
              </div>
              <div class="ncsc-mobile-edit-area" cnbiztype="mobile_editor_area_ko">
                <div cnbiztype="mea_img_ko" class="ncsc-mea-img" style="display: none;"></div>
                <div class="ncsc-mea-text" cnbiztype="mea_txt_ko" style="display: none;">
                  <p id="meat_content_count_ko" class="text-tip"></p>
                  <textarea class="textarea valid" cnbiztype="meat_content_ko"></textarea>
                  <div class="button"><a class="ncsc-btn ncsc-btn-blue" cnbiztype="meat_submit_ko" href="javascript:void(0);">확인</a><a class="ncsc-btn ml10" cnbiztype="meat_cancel_ko" href="javascript:void(0);">취소</a></div>
                  <a class="text-close" cnbiztype="meat_cancel_ko" href="javascript:void(0);">X</a>
                </div>
              </div>
              <input name="m_body" autocomplete="off" type="hidden" value='<?php echo $output['goods']['mobile_body'];?>'>
            </div>
          </div>
        </dd>
      </dl>
      <dl>
        <dt>템플릿：</dt>
        <dd> <span class="mr50">
          <label>상단양식</label>
          <select name="plate_top">
            <option>선택하세요</option>
            <?php if (!empty($output['plate_list'][1])) {?>
            <?php foreach ($output['plate_list'][1] as $val) {?>
            <option value="<?php echo $val['plate_id']?>" <?php if ($output['goods']['plateid_top'] == $val['plate_id']) {?>selected="selected"<?php }?>><?php echo $val['plate_name'];?></option>
            <?php }?>
            <?php }?>
          </select>
          </span> <span class="mr50">
          <label>하단양식</label>
          <select name="plate_bottom">
            <option>선택하세요</option>
            <?php if (!empty($output['plate_list'][0])) {?>
            <?php foreach ($output['plate_list'][0] as $val) {?>
            <option value="<?php echo $val['plate_id']?>" <?php if ($output['goods']['plateid_bottom'] == $val['plate_id']) {?>selected="selected"<?php }?>><?php echo $val['plate_name'];?></option>
            <?php }?>
            <?php }?>
          </select>
          </span> </dd>
      </dl>

      <!--F S-->
      <h3 id="demo3">출시예정 상품</h3>
      <!-- 只有可发布虚拟商品才会显示 S -->
      <?php if ($output['goods_class']['gc_virtual'] == 1) {?>
      <dl class="special-01">
        <dt>E-쿠폰 상품<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li>
              <input type="radio" name="is_gv" value="1" id="is_gv_1" <?php if ($output['goods']['is_virtual'] == 1) {?>checked<?php }?>>
              <label for="is_gv_1">예</label>
            </li>
            <li>
              <input type="radio" name="is_gv" value="0" id="is_gv_0" <?php if ($output['goods']['is_virtual'] == 0) {?>checked<?php }?>>
              <label for="is_gv_0">아니오</label>
            </li>
          </ul>
          <p class="hint vital">*E-쿠폰 상품은 기타 프로모션으로 설정할 수 없습니다.</p>
        </dd>
      </dl>
      <dl class="special-01" cnbiztype="virtual_valid" <?php if ($output['goods']['is_virtual'] == 0) {?>style="display:none;"<?php }?>>
        <dt><i class="required">*</i>유효기간<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input type="text" name="g_vindate" id="g_vindate" class="w80 text" value="<?php if($output['goods']['is_virtual'] == 1 && !empty($output['goods']['virtual_indate'])) { echo date('Y-m-d', $output['goods']['virtual_indate']);}?>"><em class="add-on"><i class="icon-calendar"></i></em>
          <span></span>
          <p class="hint">E-쿠폰 상품 교환 유효기간, 만기후 상품은 구매할 수 없으며 전자코드도 사용할 수 없습니다.</p>
        </dd>
      </dl>
      <dl class="special-01" cnbiztype="virtual_valid" <?php if ($output['goods']['is_virtual'] == 0) {?>style="display:none;"<?php }?>>
        <dt><i class="required">*</i>E-쿠폰 상품 구매제한<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input type="text" name="g_vlimit" id="g_vlimit" class="w80 text" value="<?php if ($output['goods']['is_virtual'] == 1) {echo $output['goods']['virtual_limit'];}?>">
          <span></span>
          <p class="hint">1-10이내 숫자를 입력하세요, E-쿠폰 상품은 최대 10개까지 구매제한됩니다.</p>
        </dd>
      </dl>
      <dl class="special-01" cnbiztype="virtual_valid" <?php if ($output['goods']['is_virtual'] == 0) {?>style="display:none;"<?php }?>>
        <dt>만기후 환불여부<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li>
              <input type="radio" name="g_vinvalidrefund" id="g_vinvalidrefund_1" value="1" <?php if ($output['goods']['virtual_invalid_refund'] ==1) {?>checked<?php }?>>
              <label for="g_vinvalidrefund_1">예</label>
            </li>
            <li>
              <input type="radio" name="g_vinvalidrefund" id="g_vinvalidrefund_0" value="0" <?php if ($output['goods']['virtual_invalid_refund'] == 0) {?>checked<?php }?>>
              <label for="g_vinvalidrefund_0">아니오</label>
            </li>
          </ul>
          <p class="hint">교환코드가 만기후 환불 신청 여부입니다.</p>
        </dd>
      </dl>
      <?php }?>
      <!-- 只有可发布虚拟商品才会显示 E --> 
      <!-- F码商品专有项 S -->
      <dl class="special-02" cnbiztype="virtual_null" <?php if ($output['goods']['is_virtual'] == 1) {?>style="display:none;"<?php }?>>
        <dt>F-코드<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li>
              <input type="radio" name="is_fc" id="is_fc_1" value="1" <?php if ($output['goods']['is_fcode'] == 1) {?>checked<?php }?>>
              <label for="is_fc_1">예</label>
            </li>
            <li>
              <input type="radio" name="is_fc" id="is_fc_0" value="0" <?php if ($output['goods']['is_fcode'] == 0) {?>checked<?php }?>>
              <label for="is_fc_0">아니오</label>
            </li>
          </ul>
          <p class="hint vital">*F-코드 상품은 프로모션에 참석할 수 없습니다.<br />F-코드는 소비자한테 유일한 16자리 코드를 판매하여 문자로 발급한뒤 해당 매장에서 실제 상품을 우선으로 교환할 수 있는 기능입니다.</p>
        </dd>
      </dl>
      <dl class="special-02" cnbiztype="fcode_valid" <?php if ($output['goods']['is_fcode'] == 0) {?>style="display:none;"<?php }?>>
        <dt>
          <?php if (!$output['edit_goods_sign']) {?>
          <i class="required">*</i>
          <?php }?>
          F-코드 생성수<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input type="text" name="g_fccount" id="g_fccount" class="w80 text" value="">
          <span></span>
          <p class="hint">100이내의 유일한 고유번호를 생성할 수량을 입력하세요. 수정시 F-코드 새롭게 생성됩니다.</p>
        </dd>
      </dl>
      <dl class="special-02" cnbiztype="fcode_valid" <?php if ($output['goods']['is_fcode'] == 0) {?>style="display:none;"<?php }?>>
        <dt>
          <?php if (!$output['edit_goods_sign']) {?>
          <i class="required">*</i>
          <?php }?>
          F-코드 확장<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input type="text" name="g_fcprefix" id="g_fcprefix" class="w80 text" value="">
          <span></span>
          <p class="hint">해당 상품에 대한 고유번호를 분리하기 위해 3-5이내의 영문을 입력하여 식별하시길 바랍니다.</p>
        </dd>
      </dl>
      <?php if ($output['goods']['is_fcode'] == 1) {?>
      <dl class="special-02" cnbiztype="fcode_valid">
        <dt>
            <a class="ncsc-btn-mini ncsc-btn-red" href="<?php echo urlShop('store_goods_online', 'download_f_code_excel', array('commonid' => $val['goods_commonid']));?>">F-코드 다운로드</a>&nbsp;&nbsp;F-코드<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <?php if (!empty($output['fcode_array'])) {?>
            <?php foreach ($output['fcode_array'] as $val) {?>
            <li><?php echo $val['fc_code']?>(
              <?php if ($val['fc_state'] == 1) {?>
              사용
              <?php } else {?>
              미사용
              <?php }?>
              )</li>
            <?php }?>
            <?php }?>
          </ul>
        </dd>
      </dl>
      <?php }?>
      <!-- F码商品专有项 E --> 
      <!-- 预售商品 S -->
      <dl class="special-03" cnbiztype="virtual_null" <?php if ($output['goods']['is_virtual'] == 1) {?>style="display:none;"<?php }?>>
        <dt>예약<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li>
              <input type="radio" name="is_presell" id="is_presell_1" value="1" <?php if($output['goods']['is_presell'] == 1) {?>checked<?php }?>>
              <label for="is_presell_1">예</label>
            </li>
            <li>
              <input type="radio" name="is_presell" id="is_presell_0" value="0" <?php if($output['goods']['is_presell'] == 0) {?>checked<?php }?>>
              <label for="is_presell_0">아니오</label>
            </li>
          </ul>
          <p class="hint vital">*예약상품은 프로모션에 참석할 수 없습니다.<br />날자를 지정하여 미리 예약을 받은 후 해당 날자에 배송하는 기능입니다.</p>
        </dd>
      </dl>
      <dl class="special-03" cnbiztype="is_presell" <?php if ($output['goods']['is_presell'] == 0) {?>style="display:none;"<?php }?>>
        <dt><i class="required">*</i>배송날자<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input type="text" name="g_deliverdate" id="g_deliverdate" class="w80 text" value="<?php if ($output['goods']['presell_deliverdate'] > 0) {echo date('Y-m-d', $output['goods']['presell_deliverdate']);}?>"><em class="add-on"><i class="icon-calendar"></i></em>
          <span></span>
          <p class="hint">업체 배송시간.</p>
        </dd>
      </dl>
      <!-- 预售商品 E --> 



      <!--transport info begin-->
      <h3 id="demo4">배송정보</h3>
      <input type="hidden" name="province_id" value="35" />
      <input type="hidden" name="city_id" value="45055" />

      <dl>
        <dt>배송비<?php echo $lang['nc_colon']; ?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li>
              <input id="freight_0" cnbiztype="freight" name="freight" class="radio" type="radio" <?php if (intval($output['goods']['transport_id']) == 0) {?>checked="checked"<?php }?> value="0">
              <label for="freight_0">고정금액</label>
              <div cnbiztype="div_freight" <?php if (intval($output['goods']['transport_id']) != 0) {?>style="display: none;"<?php }?>>
                <input id="g_freight" class="w50 text" nc_type='transport' type="text" value="<?php echo ceil($output['goods']['goods_freight_ko']);?>" name="g_freight"><em class="add-on"><strong>원</strong></em>
                <input id="g_freight_ko" class="w50 text" nc_type='transport' type="hidden" value="<?php printf('%.2f', floatval($output['goods']['goods_freight_ko']));?>" name="g_freight_ko">
              </div>
            </li>
            <li>
              <input id="freight_1" cnbiztype="freight" name="freight" class="radio" type="radio" <?php if (intval($output['goods']['transport_id']) != 0) {?>checked="checked"<?php }?> value="1">
              <label for="freight_1">고급설정</label>
              <div cnbiztype="div_freight" <?php if (intval($output['goods']['transport_id']) == 0) {?>style="display: none;"<?php }?>>
                <input id="transport_id" type="hidden" value="<?php echo $output['goods']['transport_id'];?>" name="transport_id">
                <input id="transport_title" type="hidden" value="<?php echo $output['goods']['transport_title'];?>" name="transport_title">
                <span id="postageName" class="transport-name" <?php if ($output['goods']['transport_title'] != '') {?>style="display: inline-block;"<?php }?>><?php echo $output['goods']['transport_title'];?></span><a href="JavaScript:void(0);" onclick="window.open('index.php?act=store_transport&type=select')" class="ncsc-btn" id="postageButton"><i class="icon-truck"></i>템플릿 설정</a> </div>
            </li>
          </ul>
          <p class="hint">배송비를 0으로 설정시 소비자 페이지에서 배송비 무료로 노출됩니다.<br />고정금액: 고정금액 설정시 무게에 상관없이 지정된 금액으로 배송료가 노출됩니다.<br />고급설정: 템플릿 설정 페이지에서 확인해주세요.</p>
        </dd>
      </dl>
      <!--transport info end-->

      <h3 id="demo5">기타정보</h3>
      <dl>
        <dt><?php echo "미니샵카테고리".$lang['nc_colon'];?></dt>
        <dd><span class="new_add"><a href="javascript:void(0)" id="add_sgcategory" class="ncsc-btn">추가</a> </span>
          <?php if (!empty($output['store_class_goods'])) { ?>
          <?php foreach ($output['store_class_goods'] as $v) { ?>
          <select name="sgcate_id[]" class="sgcategory">
            <option value="0">선택하세요</option>
            <?php foreach ($output['store_goods_class'] as $val) { ?>
            <option value="<?php echo $val['stc_id']; ?>" <?php if ($v==$val['stc_id']) { ?>selected="selected"<?php } ?>><?php echo $val['stc_name']; ?></option>
            <?php if (is_array($val['child']) && count($val['child'])>0){?>
            <?php foreach ($val['child'] as $child_val){?>
            <option value="<?php echo $child_val['stc_id']; ?>" <?php if ($v==$child_val['stc_id']) { ?>selected="selected"<?php } ?>>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $child_val['stc_name']; ?></option>
            <?php }?>
            <?php }?>
            <?php } ?>
          </select>
          <?php } ?>
          <?php } else { ?>
          <select name="sgcate_id[]" class="sgcategory">
            <option value="0">선택하세요</option>
            <?php if (!empty($output['store_goods_class'])){?>
            <?php foreach ($output['store_goods_class'] as $val) { ?>
            <option value="<?php echo $val['stc_id']; ?>"><?php echo $val['stc_name']; ?></option>
            <?php if (is_array($val['child']) && count($val['child'])>0){?>
            <?php foreach ($val['child'] as $child_val){?>
            <option value="<?php echo $child_val['stc_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $child_val['stc_name']; ?></option>
            <?php }?>
            <?php }?>
            <?php } ?>
            <?php } ?>
          </select>
          <?php } ?>
          <p class="hint">미니샵 카테고리는 소비자가 고객님의 미니샵에 방문시 노출되는 카테고리입니다. 
            <br>카테고리 카테고리는 "셀러센터-미니샵-미니샵카테고리"에서 설정가능합니다.</p>
        </dd>
      </dl>

      <dl>
        <dt>상품등록<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li>
              <label>
                <input name="g_state" value="1" type="radio" <?php if (empty($output['goods']) || $output['goods']['goods_state'] == 1 || $output['goods']['goods_state'] == 10) {?>checked="checked"<?php }?> />
                진열 </label>
            </li>
            <li>
              <label>
                <input name="g_state" value="0" type="radio" cnbiztype="auto" />
                예약진열 </label>
              <input type="text" class="w80 text" name="starttime" disabled="disabled" style="background:#E7E7E7 none;" id="starttime" value="<?php echo date('Y-m-d');?>" />
              <select disabled="disabled" style="background:#E7E7E7 none;" name="starttime_H" id="starttime_H">
                <?php foreach ($output['hour_array'] as $val){?>
                <option value="<?php echo $val;?>" <?php $sign_H = 0;if($val>=date('H') && $sign_H != 1){?>selected="selected"<?php $sign_H = 1;}?>><?php echo $val;?></option>
                <?php }?>
              </select>
              시
              <select disabled="disabled" style="background:#E7E7E7 none;" name="starttime_i" id="starttime_i">
                <?php foreach ($output['minute_array'] as $val){?>
                <option value="<?php echo $val;?>" <?php $sign_i = 0;if($val>=date('i') && $sign_i != 1){?>selected="selected"<?php $sign_i = 1;}?>><?php echo $val;?></option>
                <?php }?>
              </select>
              분 </li>
            <li>
              <label>
                <input name="g_state" value="0" type="radio" <?php if (!empty($output['goods']) && $output['goods']['goods_state'] == 0) {?>checked="checked"<?php }?> />
                미진열 </label>
            </li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt>미니샵추천<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li>
              <label>
                <input name="g_commend" value="1" <?php if (empty($output['goods']) || $output['goods']['goods_commend'] == 1) { ?>checked="checked" <?php } ?> type="radio" />
                예</label>
            </li>
            <li>
              <label>
                <input name="g_commend" value="0" <?php if (!empty($output['goods']) && $output['goods']['goods_commend'] == 0) { ?>checked="checked" <?php } ?> type="radio"/>
                아니오</label>
            </li>
          </ul>
          <p class="hint">고객님의 미니샵 페이지 상단에 추천 상품으로 진열됩니다.</p>
        </dd>
      </dl>
    </div>
    <div class="bottom tc hr32">
      <label class="submit-border">
        <input type="submit" class="submit" value="<?php if ($output['edit_goods_sign']) {echo '완료';} else {?>다음, 상품 이미지 올리기<?php }?>" />
      </label>
    </div>
  </form>
</div>

<!-- td popup START-->
<div class="td_popup_wrapper">
  <div class="opacity_bg"></div>

  <div class="content_wrapper" style="margin:0 30px;">
    <div class="list_table_wrapper">

      <threed-table-modal
          bootstrap-ctrl="tableSearchCtrl,tableBodyCtrl"
      >
        <table-search
            get-start="tableComponentThreeDCtrl._state.tableData.info.start"
            get-total="tableComponentThreeDCtrl._state.tableData.info.total"
            get-count="tableComponentThreeDCtrl._state.tableData.info.count"
            set-search-opt="tableComponentThreeDCtrl.exports.setSearchOpt(options)"
            update-page="tableComponentThreeDCtrl.exports.updatePage(options)"
        ></table-search>

        <table-body
            get-start="tableComponentThreeDCtrl._state.tableData.info.start"
            get-total="tableComponentThreeDCtrl._state.tableData.info.total"
            get-count="tableComponentThreeDCtrl._state.tableData.info.count"
            get-results="tableComponentThreeDCtrl._state.tableData.results"

            get-cur-data="tableComponentThreeDCtrl.exports.getCurData()"
            set-cur-data="tableComponentThreeDCtrl.exports.setCurData(options)"
            get-cur-idx="tableComponentThreeDCtrl.exports.getCurIdx()"
            set-cur-idx="tableComponentThreeDCtrl.exports.setCurIdx(options)"
        >
        </table-body>

        <div class="thumbnail_prev_wrapper">
          <td-prev-modal-small
          ></td-prev-modal-small>
        </div>

        <div class="list-form-search-btn" style="height:0;">
          <a class="btn btn-primary btn-lg" style="float:left;margin-left: 27%;margin-bottom: 20px;margin-top: 20px;" ng-click="tableComponentThreeDCtrl.exports.hidePopup()">취소</a>
          <a class="btn btn-primary btn-lg" style="float:right;margin-right: 20%;margin-bottom: 20px;margin-top: 20px;" ng-click="tableComponentThreeDCtrl.exports.sendData()">등록</a>
        </div>

      </threed-table-modal>


    </div>  <!-- .list_table_wrapper END-->
  </div>  <!-- .content_wrapper END-->
</div>
<!-- td popup END-->

<script type="text/javascript">
$(function(){

    //电脑端手机端tab切换
  $(".tabs").tabs();
  
  //Ajax提示
    $('.tip').poshytip({
      className: 'tip-yellowsimple',
      showTimeout: 1,
      alignTo: 'target',
      alignX: 'left',
      alignY: 'top',
      offsetX: 5,
      offsetY: -78,
      allowTipHover: false
    });
    $('.tip2').poshytip({
      className: 'tip-yellowsimple',
      showTimeout: 1,
      alignTo: 'target',
      alignX: 'right',
      alignY: 'center',
      offsetX: 5,
      offsetY: 0,
      allowTipHover: false
    });
})

var SITEURL = "<?php echo SHOP_SITE_URL; ?>";
var DEFAULT_GOODS_IMAGE = "<?php echo thumb(array(), 60);?>";
var SHOP_RESOURCE_SITE_URL = "<?php echo SHOP_RESOURCE_SITE_URL;?>";
var SYS_CUR = <?php echo C('site_cur');?>;

$(function(){
  //위안 계산
  $('#g_price_ko').bind('input propertychange',function(){
      $('#g_price').val(Math.ceil($(this).val()/<?php echo C('site_cur');?>));
      if(parseFloat($('#g_marketprice_ko').val())<parseFloat($(this).val()) || isNaN(parseFloat($('#g_marketprice_ko').val())))
      {
        $('#g_marketprice_ko').val($(this).val());
        $('#g_marketprice').val(Math.ceil($(this).val()/<?php echo C('site_cur');?>));

      }
  });

  $('#g_marketprice_ko').bind('input propertychange',function(){
      $('#g_marketprice').val(Math.ceil($(this).val()/<?php echo C('site_cur');?>));
  });


  $('#g_costprice_ko').bind('input propertychange',function(){
      $('#g_costprice').val(Math.ceil($(this).val()/<?php echo C('site_cur');?>));
  });



    $('#goods_form').validate({
        errorPlacement: function(error, element){
            $(element).nextAll('span').append(error);
        },
        <?php if ($output['edit_goods_sign']) {?>
        submitHandler:function(form){
            ajaxpost('goods_form', '', '', 'onerror');
        },
        <?php }?>
        rules : {
            g_name_ko : {
                required    : true,
                minlength   : 3,
                maxlength   : 50
            },
            g_jingle : {
                maxlength   : 50
            },
            g_marketprice : {
                required    : false,
                number      : true,
                min         : 0.00,
                max         : 9999999
            },
            g_costprice : {
                number      : true,
                min         : 0.00,
                max         : 9999999
            },
            g_price_ko : {
                required    : true,
                number      : true,
                min         : 0.01,
                max         : 9999999,
                checkPrice  : true
            },
            g_marketprice_ko : {
                required    : false,
                number      : true,
                min         : 0.00,
                max         : 9999999
            },
            g_costprice_ko : {
                number      : true,
                min         : 0.00,
                max         : 9999999
            },
            g_weight : {
                number      : true,
                min         : 0.00,
                max         : 255
            },
            g_storage  : {
                required    : true,
                digits      : true,
                min         : 1,
                max         : 9999999999
            },
            image_path : {
                required    : true
            }
        },
        messages : {
            g_name_ko  : {
                required    : '<i class="icon-exclamation-sign"></i>상품명을 입력하세요.',
                minlength   : '<i class="icon-exclamation-sign"></i>상품명은 최소 3자, 최대 50자로 입력하세요.',
                maxlength   : '<i class="icon-exclamation-sign"></i>상품명은 최소 3자, 최대 50자로 입력하세요.'
            },
            g_jingle : {
                maxlength   : '<i class="icon-exclamation-sign"></i>간략설명은 최대 50자입니다.'
            },
            g_price_ko : {
                required    : '<i class="icon-exclamation-sign"></i>판매가를 입력하세요.',       
                number      : '<i class="icon-exclamation-sign"></i>판매가는 반드시 숫자로 입력하세요.',
                min         : '<i class="icon-exclamation-sign"></i>0.01~9999999사이의 숫자를 입력하세요.',
                max         : '<i class="icon-exclamation-sign"></i>0.01~9999999사이의 숫자를 입력하세요.'
            },
            g_marketprice : {
                required    : '<i class="icon-exclamation-sign"></i>시장가를 입력하세요.',       
                number      : '<i class="icon-exclamation-sign"></i>시장가는 반드시 숫자로 입력하세요.',
                min         : '<i class="icon-exclamation-sign"></i>0.01~9999999사이의 숫자를 입력하세요.',
                max         : '<i class="icon-exclamation-sign"></i>0.01~9999999사이의 숫자를 입력하세요.'
            },
            g_costprice : {
                number      : '<i class="icon-exclamation-sign"></i>공급가는 반드시 숫자로 입력하세요.',
                min         : '<i class="icon-exclamation-sign"></i>0.01~9999999사이의 숫자를 입력하세요.',
                max         : '<i class="icon-exclamation-sign"></i>0.01~9999999사이의 숫자를 입력하세요.'
            },
            g_storage : {
                required    : '<i class="icon-exclamation-sign"></i>재고 수량을 입력하세요.',
                digits      : '<i class="icon-exclamation-sign"></i>재고 수량은 반드시 숫자로 입력하세요.',
                min         : '<i class="icon-exclamation-sign"></i>0~999999999사이의 숫자를 입력하세요.',
                max         : '<i class="icon-exclamation-sign"></i>0~999999999사이의 숫자를 입력하세요.'
            },
            image_path : {
                required    : '<i class="icon-exclamation-sign"></i>상품 이미지를 업로드하세요.'
            }
        }
    });
    <?php if (isset($output['goods'])) {?>
  setTimeout("setArea(<?php echo $output['goods']['areaid_1'];?>, <?php echo $output['goods']['areaid_2'];?>)", 1000);
  <?php }?>
});
// 按规格存储规格值数据
var spec_group_checked = [<?php for ($i=0; $i<$output['sign_i']; $i++){if($i+1 == $output['sign_i']){echo "''";}else{echo "'',";}}?>];
var str = '';
var V = new Array();

<?php for ($i=0; $i<$output['sign_i']; $i++){?>
var spec_group_checked_<?php echo $i;?> = new Array();
<?php }?>

$(function(){
  $('dl[cnbiztype="spec_group_dl"]').on('click', 'span[cnbiztype="input_checkbox"] > input[type="checkbox"]',function(){
    into_array();
    goods_stock_set();
  });

  // 提交后不没有填写的价格或库存的库存配置设为默认价格和0
  // 库存配置隐藏式 里面的input加上disable属性
  $('input[type="submit"]').click(function(){
    $('input[data_type="price"]').each(function(){
      if($(this).val() == ''){
        $(this).val($('input[name="g_price"]').val());
      }
    });
    $('input[data_type="stock"]').each(function(){
      if($(this).val() == ''){
        $(this).val('0');
      }
    });
    if($('dl[nc_type="spec_dl"]').css('display') == 'none'){
      $('dl[nc_type="spec_dl"]').find('input').attr('disabled','disabled');
    }
  });
  
});

// 将选中的规格放入数组
function into_array(){
<?php for ($i=0; $i<$output['sign_i']; $i++){?>
    
    spec_group_checked_<?php echo $i;?> = new Array();
    $('dl[nc_type="spec_group_dl_<?php echo $i;?>"]').find('input[type="checkbox"]:checked').each(function(){
      i = $(this).attr('nc_type');
      v = $(this).val();
      c = null;
      if ($(this).parents('dl:first').attr('spec_img') == 't') {
        c = 1;
      }
      spec_group_checked_<?php echo $i;?>[spec_group_checked_<?php echo $i;?>.length] = [v,i,c];
    });

    spec_group_checked[<?php echo $i;?>] = spec_group_checked_<?php echo $i;?>;

<?php }?>
}

// 生成库存配置
function goods_stock_set(){
    //  店铺价格 商品库存改为只读
    $('input[name="g_price_ko"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_marketprice_ko"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_costprice_ko"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_weight"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_serial"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_storage"]').attr('readonly','readonly').css('background','#E7E7E7 none');

    $('dl[nc_type="spec_dl"]').show();
    str = '<tr>';
    <?php recursionSpec(0,$output['sign_i']);?>
    if(str == '<tr>'){
        //  店铺价格 商品库存取消只读
        $('input[name="g_price_ko"]').removeAttr('readonly').css('background','');
        $('input[name="g_marketprice_ko"]').removeAttr('readonly').css('background','');
        $('input[name="g_costprice_ko"]').removeAttr('readonly').css('background','');
        $('input[name="g_weight"]').removeAttr('readonly').css('background','');
        $('input[name="g_serial"]').removeAttr('readonly').css('background','');
        $('input[name="g_storage"]').removeAttr('readonly').css('background','');
        $('dl[nc_type="spec_dl"]').hide();
    }else{
        $('tbody[nc_type="spec_table"]').empty().html(str)
            .find('input[nc_type]').each(function(){
                try{$(this).val(V[s]);}catch(ex){$(this).val('');};
                if($(this).attr('data_type') == 'price' && $(this).val() == ''){
                    $(this).val($('input[name="g_price"]').val());
                }
                if($(this).attr('data_type') == 'stock' && $(this).val() == ''){
                    $(this).val('0');
                }
            }).end()
            .find('input[data_type="stock"]').change(function(){
                computeStock();    // 库存计算
            }).end()
            .find('input[data_type="price_ko"]').bind('input propertychange',function(){

              /************************ 위안 가격 설정 **************/
              var mss = $(this).attr('nc_type').split("|");
              var t_price = $('input[nc_type="'+mss[0]+'|price"]');
              var _cprice = Math.ceil($(this).val() / SYS_CUR);
              t_price.val(_cprice);

              computePrice();     // 价格计算
            }).end()
            .find('input[data_type="marketprice_ko"]').bind('input propertychange',function(){

              /************************ 위안 시장 가격 설정 **************/
              var mss = $(this).attr('nc_type').split("|");
              var t_price = $('input[nc_type="'+mss[0]+'|marketprice"]');
              var _cprice = Math.ceil($(this).val() / SYS_CUR);
              t_price.val(_cprice);

              computemPrice();     // 价格计算
            }).end()
            .find('input[data_type="costprice_ko"]').bind('input propertychange',function(){

              /************************ 위안 시장 가격 설정 **************/
              var mss = $(this).attr('nc_type').split("|");
              var t_price = $('input[nc_type="'+mss[0]+'|costprice"]');
              var _cprice = Math.ceil($(this).val() / SYS_CUR);
              t_price.val(_cprice);

              computecPrice();     // 价格计算
            }).end()
            .find('input[nc_type]').change(function(){
                s = $(this).attr('nc_type');
                V[s] = $(this).val();
            });
    }
}

<?php 
/**
 * 
 * 
 *  生成需要的js循环。递归调用  PHP
 * 
 *  形式参考 （ 2个规格）
 *  $('input[type="checkbox"]').click(function(){
 *      str = '';
 *      for (var i=0; i<spec_group_checked[0].length; i++ ){
 *      td_1 = spec_group_checked[0][i];
 *          for (var j=0; j<spec_group_checked[1].length; j++){
 *              td_2 = spec_group_checked[1][j];
 *              str += '<tr><td>'+td_1[0]+'</td><td>'+td_2[0]+'</td><td><input type="text" /></td><td><input type="text" /></td><td><input type="text" /></td>';
 *          }
 *      }
 *      $('table[class="spec_table"] > tbody').empty().html(str);
 *  });
 */
function recursionSpec($len,$sign) {
    if($len < $sign){
        echo "for (var i_".$len."=0; i_".$len."<spec_group_checked[".$len."].length; i_".$len."++){td_".(intval($len)+1)." = spec_group_checked[".$len."][i_".$len."];\n";
        $len++;
        recursionSpec($len,$sign);
    }else{
        echo "var tmp_spec_td = new Array();\n";
        for($i=0; $i< $len; $i++){
            echo "tmp_spec_td[".($i)."] = td_".($i+1)."[1];\n";
        }
        echo "tmp_spec_td.sort(function(a,b){return a-b});\n";
        echo "var spec_bunch = 'i_';\n";
        for($i=0; $i< $len; $i++){
            echo "spec_bunch += tmp_spec_td[".($i)."];\n";
        }
        echo "str += '<input type=\"hidden\" name=\"spec['+spec_bunch+'][goods_id]\" nc_type=\"'+spec_bunch+'|id\" value=\"\" />';";
        for($i=0; $i< $len; $i++){
            echo "if (td_".($i+1)."[2] != null) { str += '<input type=\"hidden\" name=\"spec['+spec_bunch+'][color]\" value=\"'+td_".($i+1)."[1]+'\" />';}";
            echo "str +='<td><input type=\"hidden\" name=\"spec['+spec_bunch+'][sp_value]['+td_".($i+1)."[1]+']\" value=\"'+td_".($i+1)."[0]+'\" />'+td_".($i+1)."[0]+'</td>';\n";
        }
        echo "str +='<td><input class=\"text price\" type=\"text\" name=\"spec['+spec_bunch+'][weight]\" data_type=\"weight\" nc_type=\"'+spec_bunch+'|weight\" value=\"\" /><em class=\"add-on\">Kg</em></td><td><input class=\"text price\" type=\"text\" name=\"spec['+spec_bunch+'][costprice_ko]\" data_type=\"costprice_ko\" nc_type=\"'+spec_bunch+'|costprice_ko\" value=\"\" /><em class=\"add-on\">원</em><input class=\"text price\" type=\"hidden\" name=\"spec['+spec_bunch+'][costprice]\" data_type=\"costprice\" nc_type=\"'+spec_bunch+'|costprice\" value=\"\" /></td><td><input class=\"text price\" type=\"text\" name=\"spec['+spec_bunch+'][marketprice_ko]\" data_type=\"marketprice_ko\" nc_type=\"'+spec_bunch+'|marketprice_ko\" value=\"\" /><em class=\"add-on\">원</em><input class=\"text price\" type=\"hidden\" name=\"spec['+spec_bunch+'][marketprice]\" data_type=\"marketprice\" nc_type=\"'+spec_bunch+'|marketprice\" value=\"\" /></td><td><input class=\"text price\" type=\"text\" name=\"spec['+spec_bunch+'][price_ko]\" data_type=\"price_ko\" nc_type=\"'+spec_bunch+'|price_ko\" value=\"\" /><em class=\"add-on\">원</em><input class=\"text price\" type=\"hidden\" name=\"spec['+spec_bunch+'][price]\" data_type=\"price\" nc_type=\"'+spec_bunch+'|price\" value=\"\" /></td><td><input class=\"text stock\" type=\"text\" name=\"spec['+spec_bunch+'][stock]\" data_type=\"stock\" nc_type=\"'+spec_bunch+'|stock\" value=\"\" /></td><td><input class=\"text sku\" type=\"text\" name=\"spec['+spec_bunch+'][sku]\" nc_type=\"'+spec_bunch+'|sku\" value=\"\" /></td></tr>';\n";
        for($i=0; $i< $len; $i++){
            echo "}\n";
        }
    }
}

?>


<?php if (!empty($output['goods']) && $_GET['class_id'] <= 0 && !empty($output['sp_value']) && !empty($output['spec_checked'])){?>
//  编辑商品时处理JS
$(function(){
  var E_SP = new Array();
  var E_SPV = new Array();
  <?php
  $string = '';
  foreach ($output['spec_checked'] as $v) {
    $string .= "E_SP[".$v['id']."] = '".$v['name']."';";
  }
  echo $string;
  echo "\n";
  $string = '';
  foreach ($output['sp_value'] as $k=>$v) {
    $string .= "E_SPV['{$k}'] = '{$v}';";
  }
  echo $string;
  ?>
  V = E_SPV;
  $('dl[nc_type="spec_dl"]').show();
  $('dl[cnbiztype="spec_group_dl"]').find('input[type="checkbox"]').each(function(){
    //  店铺价格 商品库存改为只读
    $('input[name="g_price_ko"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_marketprice_ko"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_costprice_ko"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_weight"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_serial"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    $('input[name="g_storage"]').attr('readonly','readonly').css('background','#E7E7E7 none');
    s = $(this).attr('nc_type');
    if (!(typeof(E_SP[s]) == 'undefined')){
      $(this).attr('checked',true);
      v = $(this).parents('li').find('span[cnbiztype="pv_name"]');
      if(E_SP[s] != ''){
        $(this).val(E_SP[s]);
        v.html('<input type="text" maxlength="20" value="'+E_SP[s]+'" />');
      }else{
        v.html('<input type="text" maxlength="20" value="'+v.html()+'" />');
      }
      change_img_name($(this));     // 修改相关的颜色名称
    }
  });

    into_array(); // 将选中的规格放入数组
    str = '<tr>';
    <?php recursionSpec(0,$output['sign_i']);?>
    if(str == '<tr>'){
        $('dl[nc_type="spec_dl"]').hide();
        $('input[name="g_price_ko"]').removeAttr('readonly').css('background','');
        $('input[name="g_marketprice_ko"]').removeAttr('readonly').css('background','');
        $('input[name="g_costprice_ko"]').removeAttr('readonly').css('background','');
        $('input[name="g_weight"]').removeAttr('readonly').css('background','');
        $('input[name="g_storage"]').removeAttr('readonly').css('background','');
    }else{
        $('tbody[nc_type="spec_table"]').empty().html(str)
            .find('input[nc_type]').each(function(){
                s = $(this).attr('nc_type');
                try{$(this).val(E_SPV[s]);}catch(ex){$(this).val('');};
            }).end()
            .find('input[data_type="stock"]').change(function(){
                computeStock();    // 库存计算
            }).end()
            .find('input[data_type="price_ko"]').bind('input propertychange',function(){

              /************************ 위안 가격 설정 (비고) **************/
              var mss = $(this).attr('nc_type').split("|");
              var t_price = $('input[nc_type="'+mss[0]+'|price"]');
              var _cprice = Math.ceil($(this).val() / SYS_CUR);
              t_price.val(_cprice);

              computePrice();     // 价格计算
            }).end()
            .find('input[data_type="marketprice_ko"]').bind('input propertychange',function(){

              /************************ 위안 시장 가격 설정 (비고) **************/
              var mss = $(this).attr('nc_type').split("|");
              var t_price = $('input[nc_type="'+mss[0]+'|marketprice"]');
              var _cprice = Math.ceil($(this).val() / SYS_CUR);
              t_price.val(_cprice);

              computemPrice();     // 价格计算
            }).end()
            .find('input[data_type="costprice_ko"]').bind('input propertychange',function(){

              /************************ 위안 시장 가격 설정 (비고) **************/
              var mss = $(this).attr('nc_type').split("|");
              var t_price = $('input[nc_type="'+mss[0]+'|costprice"]');
              var _cprice = Math.ceil($(this).val() / SYS_CUR);
              t_price.val(_cprice);

              computecPrice();     // 价格计算
            }).end()
            .find('input[type="text"]').change(function(){
                s = $(this).attr('nc_type');
                V[s] = $(this).val();
            });
    }
});
<?php }?>

    $('#btn_show_search_goods').on('click', function() {
        $('#div_search_goods').show();
    });
    //搜索商品
    $('#btn_search_hscode').on('click', function() {
        var url = "<?php echo urlShop('store_hscode', 'search_hscode');?>";
        url += '&' + $.param({hsclass: $('#hsclass').val()});
        url += '&' + $.param({hscode_name: $('#search_hsname').val()});
        $('#div_hscode_search_result').load(url);
    });

    $('#div_hscode_search_result').on('click', 'a.demo', function() {
        $('#div_hscode_search_result').load($(this).attr('href'));
        return false;
    });

    //选择商品
    $('#div_hscode_search_result').on('click', '[cnbiztype="btn_add_hscode_article"]', function() {
        var hid = $(this).attr('data-hscode-id');
        $.get('<?php echo urlShop('store_hscode', 'hscode_info');?>', {hid: hid}, function(data) {
            if(data.result) {
                $('#hscode_number').val(data.article_id);
                $('#chscode_title').text(data.article_title);
                $('#chscode_name').text(data.article_abstract);
                $('#chscode_des').text(data.article_content);
                $('[cnbiztype="hscode_info"]').show();
                $('#div_search_goods').hide();
            } else {
                showError(data.message);
            }
        }, 'json');
    });



var td_ids = []; // 3d의상 id들
var td_rep_ids = ""; //대표의상 id
<?php if (count($output['3dc']) > 0) { ?>
<?php foreach ($output['3dc'] as $k=>$val){?>
<?php foreach ($output['3dc'][$k]->td_list as $key=>$v){?>

td_ids.push(<?php echo $output['3dc'][$k]->td_list[$key]->id ?>);
if(<?php echo $output['3dc'][$k]->td_list[$key]->rep_yn ?> == 1){
  td_rep_ids = <?php echo $output['3dc'][$k]->td_list[$key]->id ?>;
  $("input[value=<?php echo $output['3dc'][$k]->td_list[$key]->id?>]" ).click();
}
<?php } ?>
<?php } ?>
<?php } ?>

/**
 * popup 3d data listen
 */
$(window).on('ATTACH_3D_DATA', function (e, data) {
  console.log('data: ', data);
  var td_list = data.data;
  var _arr = "";
  for (var i in td_list) {
    var t = td_list[i];
    if (td_ids.indexOf(t.id) > -1) { // 이미있는 의상이면 skip
      continue;
    }

    try{
        _arr += [
            "<tr style='border-bottom: 1px solid #e6e6e6;'>",
            "<td style='width: 200px;'>" + t.code + " <input type=hidden name='td_code' value= " + t.code + "/></td>",
            "<td>" + t.name + "</td>",
            "<td style='width: 130px;'><img src='" + ( (t.td_fitting_files&&t.td_fitting_files.thumbnail)? t.td_fitting_files.thumbnail.url : '') + "' alt='3d thumbnail image' style='width: 106px; height: 106px'/></td>",
            "<td style='width: 130px;'>" + t.registerd_datetime + "</td>",
            "<td style='width: 100px;'><input type='radio' name='repre' value=" + t.id + " /></td>",
            "<td style='width: 64px;'><a id='td_remove_" + t.id + "' class='ncsc-btn' href='javascript:remove(" + t.id + ")'>삭제</a></td>",
            "</tr>"
        ].join('');
    }catch(e){
        _arr='';
    }

    td_ids.push(t.id);
  }
  $(".norecord").parent().remove();
  $("#td_table").append(_arr).find('input[type=radio]').on('click', function (e) {
    td_rep_ids = this.value;
  });
});

/**
 * 3D 대표의상 선택
 */
$("#td_table").find('input[type=radio]').on('click', function (e) {
  td_rep_ids = this.value;
});

/**
 * 3D 의상 화면에서 제거
 */
function remove(id) {
  $("#td_remove_" + id).parent().parent().remove();
  /* 대표의상 제거 */
  if (td_rep_ids.toString().indexOf(id) > -1) {
    td_rep_ids = "";
  }
  /* 3d의상 id 제거 */
  if (td_ids.indexOf(id) > -1) {
    td_ids.splice(td_ids.indexOf(id), 1);
  }
}

/**
 *  3D 등록버튼 클릭시
 */
$("#td_add").on('click', function () {
  // popup 호출
    $('.td_popup_wrapper').css({opacity:0});
    $(window).trigger('OPEN_3D_POPUP');
    _resizePopup();
    $('.td_popup_wrapper').css({opacity:1});
});


$(window).resize(function(){
    _resizePopup();
});

function _resizePopup(){
    console.log('_resizePopup');
    if( $('.td_popup_wrapper')[0] ){
        $('.td_popup_wrapper').css({
            width:parseInt(( $(window).width()*0.8 ),10)+'px' });
        $('.td_popup_wrapper').css({
            top:parseInt((  $('#td_add').position().top- $('.td_popup_wrapper').height()/2 ),10)+'px',
            left: -($('.td_popup_wrapper').parent().offset().left)+ (parseInt((  ($(window).width()-$('.td_popup_wrapper').width())/2 ),10))+'px'
        });
    }
}

/**
 * submit
 */
$("#goods_form").on('submit', function () {
  /* 3D의상이 있을 경우 대표의상 validation */
  if (td_ids.length > 0 && td_rep_ids.length === 0) {
    alert("대표의상을선택해주세요");
    return false;
  }
  $("#td_ids").val(td_ids);
  $("#td_rep_ids").val(td_rep_ids);
  return true;
});

$("input[name=man_kids]").on('click', function () {
  var $man_kids = $("input[name=man_kids]");
  if ($man_kids.is(":checked")) {
    $man_kids.val('man');
  } else {
    $("input[name=man_kids]").val('');
  }
});
$("input[name=woman_kids]").on('click', function () {
  var $woman_kids = $("input[name=woman_kids]");
  if ($woman_kids.is(":checked")) {
    $woman_kids.val('woman');
  } else {
    $woman_kids.val('');
  }
});

/**
 * 성별/성인/아동 초기값
 */

var _goods_sex = "<?php echo $output['goods']['goods_sex'] ?>";
var _goods_kids = "<?php echo $output['goods']['goods_kids'] ?>";
if (_goods_sex !== '' && _goods_kids !== '') {
  if (_goods_kids == 0) {
    $("input[value=" + _goods_sex + "]").attr('checked', true);
//    $("input[name=gender]").val(_goods_sex);
  } else {
    $("input[value=unisex]").attr('checked', true);
    $(".unisex").show();
//    $("input[name=gender]").val('unisex');
    if (_goods_kids === 'unisex') {
      $("input[name=man_kids]").attr('checked', true);
      $("input[name=woman]").attr('checked', true);
    } else {
      $("input[name=" + _goods_sex + "_kids]").attr('checked', true);
    }
    $("input[name=man_kids]").is(":checked") && $("input[name=man_kids]").val('man');
    $("input[name=woman_kids]").is(":checked") && $("input[name=woman_kids]").val('woman');
  }

} else {
  $("input[value=woman]").attr('checked', true);
}

/**
 * 성별/성인/아동 선택 시
 */
$("input[name=gender]").on('click', function () {
  if(this.value==='unisex'){
    $(".unisex").show();
  }else{
    $(".unisex").hide();
    $("input[name=man_kids]").attr('checked', false) ;
    $("input[name=woman_kids]").attr('checked', false) ;
  }
});

</script>
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/scrolld.js"></script>
<script type="text/javascript">$("[id*='Btn']").stop(true).on('click', function (e) {e.preventDefault();$(this).scrolld();})</script>
<script>
  $(window).load(function(){
    setTimeout(function(){
      window.bootstrapThreeDList();
    },100);
  });
</script>