// dependencies external library
var Q = require("q");
var lib_vc = require("./libs/verifyCode.js");
var fs = require('fs');
var unzip = require('unzip2');
var is = require("is_js");

// dependencies internal module(and service)
var UUID = require("./libs/uuid.js");
var debug = require("./libs/debug.js");
var chkData = require("./libs/chkData.js");
var SQLService = require('../services/SQLService');
var FileService = require('../services/FileService');
var ErrorHandler = require('../services/ErrorHandler');
var ValidatorService = require('../services/ValidatorService');
var QueryService = require('../services/QueryService');

module.exports = {
  list: _list,
  get : _get,
  attachTd:_attachTd,
  chkAvailability: _chkAvailability
};

function _list(req, res){
  var debug = require("./libs/debug.js");
  var param = {},
      query = {},
      argQuery = [],
      strQuery = '',
      result_wrap = {},
      error = {},
      status;

  var url = {},
      path = {};

  /* Using calculation to item */
  var total = 0, td_register = 0, td_not_register = 0, man = 0, woman = 0, unisex = 0, none_sex = 0;

  /* Using to making JSON data */
  var store_id, td_list, td_fitting_files;

  /* Using to making common where cause */
  var item = [];

  url.purchase = sails.config.url.site;
  url.site = sails.config.url.site;
  url.file = sails.config.ftp.domain;

  path.purchase = '/wap/tmpl/product_detail.html?goods_id=';
  path.site = '/data/upload/store/goods/';
  path.file = '/DAT/';

  /* ---setting request param---
  * account_uuid : member_id
  * from : unnecessary
  * item_uuid : goods_id
  * origin_item_id : goods_commonid
  * include_cate_family_flag : unnecessary
  * include_cate_unassigned_flad : unnecessary
  * sale_type : unnecessary
  * */
  param.account_uuid = req.param("account_uuid");
  param.start = req.param("start");
  param.count = req.param("count");
  param.item_ids = req.param("item_ids");
  param.item_uuid = req.param("item_uuid");
  param.item_name = req.param("item_name");
  param.origin_item_id = req.param("origin_item_id");
  param.item_sex = req.param("item_sex");
  param.td_name = req.param("td_name");
  param.td_code = req.param("td_code");
  param.org_id = req.param("org_id");
  param.org_name = req.param("org_name");
  param.brand_id = req.param("brand_id");
  param.item_cate_id = req.param("item_cate_id");
  param.item_status = req.param("item_status");
  param.td_reg_flag = req.param("td_reg_flag");
  param.period_reg_start = ParamHandler.checkTimeFormatISO(req.param("period_reg_start"));
  param.period_reg_end = ParamHandler.checkTimeFormatISO(req.param("period_reg_end"));
  param.period_upd_start = ParamHandler.checkTimeFormatISO(req.param("period_upd_start"));
  param.period_upd_end = ParamHandler.checkTimeFormatISO(req.param("period_upd_end"));
  param.order = req.param("order");
  param.order_type = req.param("order_type");
  param.show = req.param("show");

  /* check param */
  if( param.period_reg_start === false || param.period_reg_end === false || param.period_upd_start === false || param.period_upd_end === false ){
    status = ErrorHandler.INVALID_PARAM.status;
    error.code = ErrorHandler.INVALID_PARAM.code;
    error.index = 0;
    return res.send(status, debug.wrap(req, res, status, error));
  }


  /* total query  = total query + inner query */
  query.total_select = "select count(commonid) total, count(td_list) td, item_sex sex ";
  query.total_from =     "from sv_item_detail ";
  query.total_group =   "group by item_sex ";

  /* detail query = detail query + inner query */
  query.detail_select = "select commonid id, ";
  query.detail_select +=       "code, ";
  query.detail_select +=       "origin_item_id, ";
  query.detail_select +=       "item_serial, ";
  query.detail_select +=       "name, ";
  query.detail_select +=       "item_sex, ";
  query.detail_select +=       "item_kids, ";
  query.detail_select +=       "purchase_url, ";
  query.detail_select +=       "item_price, ";
  query.detail_select +=       "currency, ";

  if(param.show && param.show.indexOf("thumbnail") > -1){
    query.detail_select +=     "item_thumbnail_url, ";
  }

  query.detail_select +=       "display_yn, ";
  query.detail_select +=       "display_start_item, ";
  query.detail_select +=       "display_end_item, ";
  query.detail_select +=       "org_name, ";
  query.detail_select +=       "brand_id, ";
  query.detail_select +=       "brand_name, ";
  query.detail_select +=       "from_unixtime(registerd_datetime, '%Y-%m-%d %H:%i:%s') registerd_datetime, ";
  query.detail_select +=       "from_unixtime(updated_datetime, '%Y-%m-%d %H:%i:%s') updated_datetime, ";
  query.detail_select +=       "sale_type ";

  if(param.show &&  param.show.indexOf("categories") > -1){
    query.detail_select +=               ", ";
    query.detail_select +=     "category_list ";
  }
  if(param.show &&  param.show.indexOf("td_id") > -1) {
    query.detail_select +=               ", ";
    query.detail_select +=     "td_ids ";
  }
  if(param.show && param.show.indexOf("td_list") > -1) {
    query.detail_select +=               ", ";
    query.detail_select +=     "td_list ";
  }


  query.detail_from =   "from sv_item_detail ";
  query.detail_limit = "";
  query.detail_order = "";

  /* common where cause - total_query, detail_query */
  query.where = 'where org_id = ';
  query.common_where = "";

  /* inner common where cause - item_ids */
  if(param.item_ids){
    query.common_where += 'and find_in_set(commonid, ?) ';
    argQuery.push(param.item_ids);
  }

  /* inner common where cause - item_name, item_uuid, origin_item_id */
  if(param.item_name){
    item.push('and name like ? ');
    argQuery.push("%" + param.item_name + "%");
  }

  if(param.item_uuid){
    item.push('and commonid like ? ');
    argQuery.push("%" + param.item_uuid + "%");
  }

  if(param.origin_item_id){
    item.push('and origin_item_id like ? ');
    argQuery.push("%" + param.origin_item_id + "%");
  }

  if(item.length > 1){
    query.common_where += "and (" + item.join("").replace(/and/g, "or").replace("or", "") + ") ";
  }else if(item.length === 1){
    query.common_where += item[0];
  }

  /* inner common where cause - item_sex */
  if(param.item_sex){
    query.common_where += "and item_sex = ? ";
    argQuery.push(param.item_sex.toLowerCase());
  }

  /* inner common where cause - td_name */
  if(param.td_name){
    query.common_where += "and td_name like ? ";
    argQuery.push("%" + param.td_name + "%");
  }

  /* inner common where cause - td_code */
  if(param.td_code){
    query.common_where += "and td_code like ? ";
    argQuery.push("%" + param.td_code + "%");
  }

  /* inner common where cause - org_id */
  if(param.org_id){
    query.common_where += "and org_id = ? ";
    argQuery.push(param.org_id);
  }

  /* inner common where cause - org_name */
  if(param.org_name){
    query.common_where += "and org_name like ? ";
    argQuery.push("%" + param.org_name + "%");
  }

  /* inner common where cause - brand_id */
  if(param.brand_id){
    query.common_where += "and brand_id = ? ";
    argQuery.push(param.brand_id);
  }

  /* inner common where cause - item_cate_id */
  if(param.item_cate_id){
    query.common_where += "and find_in_set(?, item_cate_id) ";
    argQuery.push(param.item_cate_id);
  }

  /* inner common where cause - item_status */
  if(param.item_status){
    if(param.item_status = "activated"){
      query.common_where += "and brand_st = ? ";
      argQuery.push(param.item_status);
    }
    query.common_where += "and find_in_set(item_status, ?) ";
    argQuery.push(param.item_status);
  }

  /* inner common where cause - td_reg_flag */
  if(param.td_reg_flag){
    switch (param.td_reg_flag){
      case "registerd" :
        query.common_where += "and td_ids is not null ";
        break;
      case "not_registerd" :
        query.common_where += "and td_ids is null ";
        break;
    }
  }

  /* inner common where cause - period_reg_start, period_reg_end */
  if(param.period_reg_start){
    query.common_where += "and registerd_datetime >= unix_timestamp(?) ";
    argQuery.push(param.period_reg_start);
  }
  if(param.period_reg_end){
    query.common_where += "and registerd_datetime <= unix_timestamp(?) ";
    argQuery.push(param.period_reg_end);
  }

  /* inner common where cause - period_upd_start, period_upd_end */
  if(param.period_upd_start){
    query.common_where += "and updated_datetime >= unix_timestamp(?) ";
    argQuery.push(param.period_upd_start);
  }
  if(param.period_upd_end){
    query.common_where += "and updated_datetime <= unix_timestamp(?) ";
    argQuery.push(param.period_upd_end);
  }

  query.common_where += "and item_kids = 0 ";

  /* detail query - group by cause */
  query.detail_group = 'group by commonid ';

  /* detail query - order by cause */
  if(param.order){
    switch (param.order){
      case "registerd_date" :
        query.detail_order = "order by registerd_datetime ";
        break;
      case "price" :
        query.detail_order = "order by item_price ";
        break;
      case "item_name" :
      case "name" :
        query.detail_order = "order by name ";
        break;
    }

    if(param.order_type && param.order_type.toLowerCase() === "desc"){
      query.detail_order += 'desc ';
    }
  }

  /* detail query - limit cause */
  if(param.start || param.count){
    param.start = parseInt(param.start, 10) || 0 ;
    param.count = parseInt(param.count, 10) || 10 ;

    query.detail_limit = "limit ?, ? ";
    argQuery.push(param.start, param.count);

  }

  /* get store info query */
  query.select_store = "select store_id ";
  query.from_store =     "from sv_seller ";
  query.where_store =   "where member_id= ? ";

  //sails.log(query.common_where);
  //sails.log(argQuery);

  async.waterfall([
    function(callback){
      if(!param.account_uuid){
        query.where  = ""; 
        store_id = "";
        return callback(null);
      }
      
      strQuery = query.select_store + query.from_store + query.where_store;

      DB.query(strQuery, param.account_uuid, function (err, results) {
        if (err) {
          status = 500;
          return res.send(status, debug.wrap(req, res, status, err));
        } else {
          if(results.length === 0){
            status = ErrorHandler.NOT_FOUND_ORG_NAME.status;
            error.code = ErrorHandler.NOT_FOUND_ORG_NAME.code;
            error.index = 0;
            return res.send(status, debug.wrap(req, res, status, error));
          }

          store_id = results[0].store_id;

          callback(null);
        }
      });
    },
    function(callback){
  
      if(store_id === ""){
        query.common_where = query.common_where.replace("and ", "where ");
      }
      
      strQuery = query.total_select + query.total_from +
                 query.where + store_id + " " +
                 query.common_where +
                 query.total_group ;

      DB.query(strQuery, argQuery, function (err, results) {
        if(err){
          status = 500;
          return res.send(status, debug.wrap(req, res, status, err));
        }else{
          for(var i in results){
            total += results[i].total;
            td_register += results[i].td;
            switch (results[i].sex){
              case 'man' :
                man = results[i].total;
                break;
              case 'woman' :
                woman = results[i].total;
                break;
              case 'unisex' :
                unisex = results[i].total;
                break;
              default :
                none_sex = results[i].total;
                break;
            } // end switch
          } // end for

          td_not_register = total - td_register;

          result_wrap.info = {};
          result_wrap.info.start = param.start || 0;
          result_wrap.info.count = param.count || 0;
          result_wrap.info.total = total;

          result_wrap.info.sex = {};
          result_wrap.info.sex.man = man;
          result_wrap.info.sex.woman = woman;
          result_wrap.info.sex.unisex = unisex;
          result_wrap.info.sex.undefined = none_sex;

          result_wrap.info.td = {};
          result_wrap.info.td.register = td_register;
          result_wrap.info.td.not_register = td_not_register;

          callback(null);
        }
      });
    },
    function(callback){
      strQuery = query.detail_select +
                 query.detail_from +
                 query.where + store_id + " " +
                 query.common_where +
                 query.detail_group +
                 query.detail_order +
                 query.detail_limit;

      DB.query(strQuery, argQuery, function (err, results) {
        if(err){
          status = 500;
          return res.send(status, debug.wrap(req, res, status, err));
        }else{
          for(var i in results){
            results[i].purchase_url = url.purchase + path.purchase + results[i].purchase_url;
            if(results[i].item_thumbnail_url) results[i].item_thumbnail_url = url.site + path.site + results[i].item_thumbnail_url;
            if(results[i].category_list) results[i].category_list = JSON.parse(results[i].category_list);
            if(results[i].td_list) results[i].td_list = JSON.parse(results[i].td_list);

            td_list = results[i].td_list;

            for(var j in td_list){
              td_fitting_files = td_list[j].td_fitting_files;
              for(var k in td_fitting_files){
                td_fitting_files[k].url = url.file + path.file + td_fitting_files[k].url;
              }
            }

          }
          result_wrap.results = results;
          callback(null);
        }
      });
    }
  ], function(err){
    if(err){
      status = ErrorHandler.UNDEFINED_ERR.status;
      error.code = ErrorHandler.UNDEFINED_ERR.code;
      error.index = 0;
      return res.send(status, debug.wrap(req, res, status, error));
    }
  
    status = 200;
    return res.send(status, debug.wrap(req, res, status, result_wrap));
  });
}

function _get(req, res){
  var debug = require("./libs/debug.js"),
      chk_data = require("./libs/chkData.js");

  var param = {},
    query = {},
    argQuery = [],
    strQuery = '',
    result_wrap = {},
    error = {},
    status;

  var url = {},
    path = {};

  /* Using to making JSON data */
  var td_list, td_fitting_files;

  url.purchase = sails.config.url.site;
  url.site = sails.config.url.site;
  url.file = sails.config.ftp.domain;

  path.purchase = '/wap/tmpl/product_detail.html?goods_id=';
  path.site = '/data/upload/store/goods/';
  path.file = '/DAT/';

  param.account_uuid = req.param("account_uuid");
  param.item_id = req.param("item_id");
  param.show = req.param("show");

  chk_data.chkParam(req, res, [param.item_id ], function() {
    /* detail query = detail query + inner query */
    query.detail_select = "select commonid id, ";
    query.detail_select +=       "code, ";
    query.detail_select +=       "origin_item_id, ";
    query.detail_select +=       "item_serial, ";
    query.detail_select +=       "purchase_url, ";
    query.detail_select +=       "name item_name, ";
    query.detail_select +=       "item_sex, ";
    query.detail_select +=       "item_kids, ";
    query.detail_select +=       "item_price, ";
    query.detail_select +=       "currency, ";
    query.detail_select +=       "item_thumbnail_url, ";
    query.detail_select +=       "item_status status, ";
    query.detail_select +=       "display_yn, ";
    query.detail_select +=       "sale_type, ";
    query.detail_select +=       "display_start_item, ";
    query.detail_select +=       "display_end_item, ";
    query.detail_select +=       "org_name, ";
    query.detail_select +=       "brand_id, ";
    query.detail_select +=       "brand_name, ";
    query.detail_select +=       "from_unixtime(registerd_datetime, '%Y-%m-%d %H:%i:%s') registerd_datetime, ";
    query.detail_select +=       "from_unixtime(updated_datetime, '%Y-%m-%d %H:%i:%s') updated_datetime, ";
    query.detail_select +=       "category_list, ";
    query.detail_select +=       "td_list ";
    query.detail_from =    "from sv_item_detail ";

    if(param.item_id){
      query.where = "where commonid = ? ";
      argQuery.push(param.item_id);
    }

    query.org_where = 'and org_id = ? ';


    /* get store info query */
    query.select_store = "select store_id ";
    query.from_store =     "from sv_seller ";
    query.where_store =   "where member_id= ? ";

    //sails.log(query.common_where);
    //sails.log(argQuery);

    async.waterfall([
      function(callback){
        if(!param.account_uuid){
          query.org_where  = "";
          return callback(null);
        }

        strQuery = query.select_store + query.from_store + query.where_store;

        DB.query(strQuery, param.account_uuid, function (err, results) {
          if (err) {
            status = 500;
            return res.send(status, debug.wrap(req, res, status, err));
          } else {
            if(results.length === 0){
              status = ErrorHandler.NOT_FOUND_ORG_NAME.status;
              error.code = ErrorHandler.NOT_FOUND_ORG_NAME.code;
              error.index = 0;
              return res.send(status, debug.wrap(req, res, status, error));
            }

            argQuery.push(results[0].store_id);

            callback(null);
          }
        });
      },
      function(callback){
        strQuery = query.detail_select +
          query.detail_from +
          query.where +
          query.org_where ;

        DB.query(strQuery, argQuery, function (err, results) {
          if(err){
            status = 500;
            return res.send(status, debug.wrap(req, res, status, err));
          }else{
            if(results.length === 0){
              status = ErrorHandler.NOT_FOUND_DATA.status;
              error.code = ErrorHandler.NOT_FOUND_DATA.code;
              error.index = 0;
              return res.send(status, debug.wrap(req, res, status, error));
            }

            for(var i in results){
              results[i].purchase_url = url.purchase + path.purchase + results[i].purchase_url;
              if(results[i].item_thumbnail_url) results[i].item_thumbnail_url = url.site + path.site + results[i].item_thumbnail_url;
              if(results[i].category_list) results[i].category_list = JSON.parse(results[i].category_list);
              if(results[i].td_list) results[i].td_list = JSON.parse(results[i].td_list);

              td_list = results[i].td_list;

              for(var j in td_list){
                td_fitting_files = td_list[j].td_fitting_files;
                for(var k in td_fitting_files){
                  td_fitting_files[k].url = url.file + path.file + td_fitting_files[k].url;
                }
              }

            }
            result_wrap.results = results;
            callback(null);
          }
        });
      }
    ], function(err){
      if(err){
        status = ErrorHandler.UNDEFINED_ERR.status;
        error.code = ErrorHandler.UNDEFINED_ERR.code;
        error.index = 0;
        return res.send(status, debug.wrap(req, res, status, error));
      }

      status = 200;
      return res.send(status, debug.wrap(req, res, status, result_wrap));
    });
  });
}

function _attachTd(req, res){
  sails.log.debug( '_attachTd allParams: ',req.allParams() );

  var ATATCH_TASK_MODEL = {
    ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY,
    params:req.allParams(),
    data:{
      goods_id:'',
      goods_commonid:'',
      goods_name:'',

      store_id:'',
      store_name:'',

      brand_id:'',
      brand_name:''
    }
  };



  if( !ATATCH_TASK_MODEL.params.td_ids || ATATCH_TASK_MODEL.params.td_ids=='undefined' || ATATCH_TASK_MODEL.params.td_ids=='null'){
    ATATCH_TASK_MODEL.params.td_ids = '';
  }

  if( !ATATCH_TASK_MODEL.params.td_rep_ids || ATATCH_TASK_MODEL.params.td_rep_ids=='undefined' || ATATCH_TASK_MODEL.params.td_rep_ids=='null'){
    ATATCH_TASK_MODEL.params.td_rep_ids = '';
  }

  Q.fcall(_taskValidateParams)
      .then(_taskSideEffect(function(results){
      }))
      .then(_taskCheckVerifyCode)
      .then(_taskSideEffect(function(results){
      }))

      //1.  Check Store
      .then(_taskCheckStoreAndSeller)
      .then(
          _taskSideEffect(function(res){
            sails.log.debug('_taskCheckStoreAndSeller res: ' , res);
            if( res[0].length<1 ){
                throw {code:'ER_WRONG_ACCOUNT_UUID', status:403};
            }else{
              ATATCH_TASK_MODEL.data.store_id = res[0][0].store_id;
            }
          })
      )

      //2.  Check Goods
      .then(_taskCheckGoods)
      .then(
          _taskSideEffect(function(res){
            sails.log.debug('_taskCheckGoods 2: ' , res);
            var resObj;
            if(res[0][0]){
                resObj = res[0][0];

                ATATCH_TASK_MODEL.data.goods_id = resObj.goods_id;
                ATATCH_TASK_MODEL.data.goods_commonid = resObj.goods_commonid;
                ATATCH_TASK_MODEL.data.goods_name = resObj.goods_name;
                ATATCH_TASK_MODEL.data.store_id = resObj.store_id;
                ATATCH_TASK_MODEL.data.store_name = resObj.store_name;
                ATATCH_TASK_MODEL.data.brand_id = resObj.brand_id;
                ATATCH_TASK_MODEL.data.brand_name = resObj.brand_name;
            }else{
                throw {code:'ER_WRONG_ITEM_ID', status:403};
            }


            sails.log.debug('ATATCH_TASK_MODEL: ' , ATATCH_TASK_MODEL.data);

          })
      )

      //3.  Check 3DC
      .then(_taskCheckHaving3DC)
      .then(
          _taskSideEffect(function(res){
            sails.log.debug('_taskCheckHaving3DC 3: ' , res);
            sails.log.debug('ATATCH_TASK_MODEL.params.td_ids: ' , ATATCH_TASK_MODEL.params.td_ids);
            sails.log.debug('ATATCH_TASK_MODEL.params.td_rep_ids: ' , ATATCH_TASK_MODEL.params.td_rep_ids);

            if(ATATCH_TASK_MODEL.params.td_ids=='' && ATATCH_TASK_MODEL.params.td_rep_ids==''){
              sails.log.debug('EMPTY::::');
            }else{
              if( parseInt(res[0][0].having_3dc)<1 ){
                  throw {code:'ER_WRONG_TD_REP_ID', status:403};
              }
            }

          })
      )

      //4.  replace
      .then(_taskRegist3DCExposure)
      .then(
          _taskSideEffect(function(res){
            sails.log.debug('_taskRegist3DCExposure validate: ' , res);

          })
      )

      //5.  delete
      .then(_taskDelete3DCExposure)
      .then(
          _taskSideEffect(function(res){
            sails.log.debug('_taskDelete3DCExposure validate: ' , res);

          })
      )


      .then(_taskAllSettled)
      .catch(_taskDefaultError);                  // ** default error excute

  function _taskValidateParams(){
    sails.log.debug('ATATCH_TASK_MODEL.params:::: ' , ATATCH_TASK_MODEL.params);

    return ValidatorService.validatePromise(
        {
          data:_.omit(ATATCH_TASK_MODEL.params, ['show']),
          validator:[
            {key:'account_uuid' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
            {key:'secret' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
            {key:'verify_code' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
            {key:'item_id' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }}
            // {key:'td_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
            // {key:'td_rep_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }}
          ]
        });
  }

  function _taskCheckVerifyCode(){
    console.log( '_taskCheckVerifyCode:::: '  );

    return lib_vc
        .chkVerifyCodeOnPromise({
          key:ATATCH_TASK_MODEL.params.account_uuid,
          verify_code:ATATCH_TASK_MODEL.params.verify_code,
          account_uuid:ATATCH_TASK_MODEL.params.account_uuid,
          secret:ATATCH_TASK_MODEL.params.secret
        });
  }

  function _taskCheckStoreAndSeller(){
    return SQLService
        .queryOnPromise(
            ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_SELLER,
            [ATATCH_TASK_MODEL.params.account_uuid]
        );
  }

  function _taskCheckGoods(){
    return SQLService
        .queryOnPromise(
            ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_GOODS,
            [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.params.item_id]
        );
  }

  function _taskCheckHaving3DC(){
    return SQLService
        .queryOnPromise(
            ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_3DC,
            [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.params.td_rep_ids]
        );
  }

  function _taskRegist3DCExposure(){
    var td_id_arr = ATATCH_TASK_MODEL.params.td_ids.split(',');
    var added_qry = (_.map(td_id_arr, function(v){
      return [
        "(",
        "'",ATATCH_TASK_MODEL.data.goods_id,"'",',',
        "'",ATATCH_TASK_MODEL.data.goods_commonid,"'",',',
        "'",ATATCH_TASK_MODEL.data.goods_name,"'",',',
        "'",ATATCH_TASK_MODEL.data.store_id,"'",',',
        "'",ATATCH_TASK_MODEL.data.store_name,"'",',',
        "'",ATATCH_TASK_MODEL.data.brand_id,"'",',',
        "'",ATATCH_TASK_MODEL.data.brand_name,"'",',',
        "'",v,"'",',',
        ((parseInt(v)===parseInt(ATATCH_TASK_MODEL.params.td_rep_ids))?'true':'false'),',',
        'unix_timestamp(now())',
        ")"
      ].join('');

    }) ).join(',');

    return SQLService
        .queryOnPromise(
            ATATCH_TASK_MODEL.ATTACH_TD_QRY.REPLACE_SV_3DV_EXPOSURE+added_qry,
            []
        );
  }

  function _taskDelete3DCExposure(){
    return SQLService
        .queryOnPromise(
            ATATCH_TASK_MODEL.ATTACH_TD_QRY.DELETE_SV_3DV_EXPOSURE,
            [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.data.goods_id, ATATCH_TASK_MODEL.params.td_ids]
        );
  }

  function _taskAllSettled(){
    sails.log.debug('_taskAllSettled arguments: ' , arguments);

    return res.send(
        200,
        debug.wrap(
            req, res, 200,
            {
              fieldCount: 0,
              affectedRows: 1,
              serverStatus: 2,
              warningCount: 0,
              message: "",
              protocol41: true
            }
        )
    );
  }

  function _taskDefaultError(__err){
    sails.log.debug('_taskDefaultError ERROR: ' , __err);

      var err_code = __err.code || 'ERR_FAIL';
      var tmpStatus = __err.status || 500;
      var idx = __err.index || null;
      var message = __err.message || null;
      var key = (err_code==='ER_BAD_BLANK_ERROR' || err_code==='ER_UNDEFINED_PARAM_ERROR')?__err.key:null;

      var resultObj = {code:err_code};
      if(idx) resultObj.index = idx;
      if(key) resultObj.key = key;
      if(message) resultObj.message = message;

      res.status(tmpStatus);
      return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), resultObj ));
  }


}



//util fnc
function _taskSideEffect(func){
  return function(){
    if(func) func(arguments);
    return arguments;
  };
}

function _taskMapData(mapData){
  return function(){
    if(mapData){
      if(typeof mapData==='function'){
        return mapData(arguments);
      }else{
        return mapData
      }
    }else{
      return arguments;
    }
  };
}


function _invokePromise(fnc, valFnc){
  return function(results){
    return Q.fapply(fnc, valFnc(results) );
  }
}


function _chkAvailability(req, res) {
  var debug = require("./libs/debug.js"),
  item_ids = req.param("item_ids") || "",
  rtn_val = {},
  qry = "";

  chkData.chkParam(req, res, [item_ids], function() {
    qry += "SELECT commonid AS item_id ";
    qry += "  FROM sv_item_detail ";
    qry += " WHERE item_status<>'activated' ";
    qry += "   AND commonid IN (" + SQLService.escape(item_ids) + ")";

    DB.query(qry, [item_ids], function(err, results) {
      if (err) {
        status = 500;
        return res.send(status, debug.wrap(req, res, status, err));
      } else {
        //sails.log(_.pluck(results, 'ITEM_NO').join(","));
        rtn_val.not_available_item_ids = results;
        status = 200;
        return res.send(status, debug.wrap(req, res, status, rtn_val));
      }
    });
  });
}













































