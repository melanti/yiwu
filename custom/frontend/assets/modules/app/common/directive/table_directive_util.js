(function($, angular, _, APP){

  APP.modules.directive.util.checkBoxHelper = _checkBoxHelper;


  function _checkBoxHelper(scope, elem, attrs, ctrl, watchModel){
    //console.log('_checkBoxHelper: ', _checkBoxHelper);

    var $chkBoxAll = $(elem).find('.table-row-check-box-all'),
        $tbody = $(elem).find('tbody'),
        unbindWatcher;

    unbindWatcher = scope.$watchCollection(watchModel, function(__newVal, __oldVal){
      if(__newVal===__oldVal){}else{
        _resetCheckbox();
      }
    });

    scope.$on("$destroy", function() {
      if(unbindWatcher) unbindWatcher();
      $chkBoxAll.off('change');
      $tbody.off('change');
    });

    $chkBoxAll.on('change', _changeChkBoxAll);
    $tbody.on('change', '.table-row-check-box', _changeChkBox);

    function _changeChkBoxAll(__e){
      if($(__e.target).prop('checked')){
        //모두선택
        $tbody.find('.table-row-check-box').prop('checked', true);
      }else{
        //모두해제
        $tbody.find('.table-row-check-box').prop('checked', false);
      }
    }

    function _changeChkBox(__e){
      if($(__e.target).prop('checked')){
        //선택
        if( _getCheckBoxCheckedLen() == _getCheckBoxAllLen() ){
          $chkBoxAll.prop('checked', true);
        }
      }else{
        //해제
        $chkBoxAll.prop('checked', false);
      }
    }

    function _getCheckBoxCheckedLen(){
      return $tbody.find('.table-row-check-box:checked').length;
    }

    function _getCheckBoxAllLen(){
      return $tbody.find('.table-row-check-box').length;
    }

    function _resetCheckbox(){
      $('.table-row-check-box').each(function(){
        $(this).prop('checked', false);
        $(this).attr('checked', false);
      });

      $('.table-row-check-box-all').prop('checked', false);
      $('.table-row-check-box-all').attr('checked', false);

    }
  }


})(window.jQuery, window.angular, window._, window.APP);
