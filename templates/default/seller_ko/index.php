<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncsc-index">

                <?php if(C('site_scm')):?>
  <div class="top-container">
    <div class="basic-info">
      <dl class="ncsc-seller-info">
        <dt class="seller-name">
          <h3><?php echo $_SESSION['seller_name']; ?></h3>
          <h5>(사용자ID ：<?php echo $_SESSION['member_name']; ?>)</h5>
        </dt>
        <dd class="store-logo">
          <p><img src="<?php echo getStoreLogo($output['store_info']['store_label'],'store_logo');?>"/></p>
          <a href="<?php echo urlShop('store_setting', 'store_setting');?>"><i class="icon-edit"></i>미니샵 설정</a> </dd>
        <dd class="seller-permission">관리권한：<strong>관리자</strong></dd>
        <dd class="seller-last-login">최근 접속시간：<strong><?php echo $_SESSION['seller_last_login_time'];?></strong> </dd>
        <dd class="store-name">미니샵명：<a href="<?php echo urlShop('show_store', 'index', array('store_id' => $_SESSION['store_id']), $output['store_info']['store_domain']);?>" ><?php echo $output['store_info']['store_name'];?></a></dd>
        <dd class="store-grade">미니샵등급：<strong>다이아몬드 등급</strong></dd>
        <dd class="store-validity">유효기간：<strong><?php echo $output['store_info']['store_end_time_text'];?>
          <?php if ($output['store_info']['reopen_tip']) {?>
          <a href="index.php?act=store_info&op=reopen">지금갱신</a>
          <?php } ?>
          </strong> </dd>
      </dl>
<?php if (!checkPlatformStore()) { ?>
      <div class="detail-rate">
        <h5> <strong>미니샵평점</strong> 업종대비 </h5>
        <ul>
          <?php  foreach ($output['store_info']['store_credit_ko'] as $value) {?>
          <li> <?php echo $value['text'];?><span class="credit"><?php echo $value['credit'];?> 점</span> <span class="<?php echo $value['percent_class'];?>"><i></i><?php echo $value['percent_text'];?><em><?php echo $value['percent'];?></em></span> </li>
          <?php } ?>
        </ul>
      </div>
<?php } ?>
    </div>
  </div>
              <?php endif;?>

  
  <div class="seller-cont">
    <div class="container type-a">
      <div class="hd">
        <h3>미니샵/상품 진행현황</h3>
        <h5>나의 상품을 조회/관리/수정 하실 수 있습니다.</h5>
      </div>
      <div class="content">
        <dl class="focus">
          <dt>상품등록공간：</dt>
          <dd title="등록완료/나의 상품을 조회/관리/수정 하실 수 있습니다."><em id="nc_goodscount">0</em>&nbsp;/&nbsp;
            <?php if ($output['store_info']['grade_goodslimit'] != 0){ echo $output['store_info']['grade_goodslimit'];} else { echo '제한없음';} ?>
          </dd>
          <dt><?php echo '앨범사용공간'.$lang['nc_colon'];?></dt>
          <dd><em id="nc_imagecount">0</em>&nbsp;/&nbsp;<?php echo $output['store_info']['grade_albumlimit'] > 0 ? $output['store_info']['grade_albumlimit'] : '제한없음'; ?></dd>
        </dl>
        <ul>
          <li><a href="index.php?act=store_goods_online&op=index">판매중 <strong id="nc_online"></strong></a></li>
          <?php if (C('goods_verify')) {?>
          <li><a href="index.php?act=store_goods_offline&op=index&type=wait_verify&verify=10" title="등록 대기중인 상품">등록대기 <strong id="nc_waitverify"></strong></a></li>
          <li><a href="index.php?act=store_goods_offline&op=index&type=wait_verify&verify=0" title="등록">등록실패<strong id="nc_verifyfail"></strong></a></li>
          <?php }?>
          <li><a href="index.php?act=store_goods_offline&op=index">등록완료 <strong id="nc_offline"></strong></a></li>
          <li><a href="index.php?act=store_goods_offline&op=index&type=lock_up">진열불가상품<strong id="nc_lockup"></strong></a></li>
          <li><a href="index.php?act=store_consult&op=consult_list&type=to_reply">문의답변대기  <strong id="nc_consult"></strong></a></li>
        </ul>
      </div>
    </div>
    <div class="container type-b">
      <div class="hd">
        <h3>알림</h3>
        <h5></h5>
      </div>
      <div class="content">
        <ul>
          <li><?php echo $output['show_txt'];?></li>
        </ul>
        <dl>
          <dt>연락처</dt>
          <?php
			if(is_array($output['phone_array']) && !empty($output['phone_array'])) {
				foreach($output['phone_array'] as $key => $val) {
			?>
          <dd><?php echo '전화'.($key+1).$lang['nc_colon'];?><?php echo $val;?></dd>
          <?php
				}
			}
			 ?>
          <dd>E-MAIL : <?php echo C('site_email');?></dd>
        </dl>
      </div>
    </div>
    <div class="container type-a">
      <div class="hd">
        <h3>긴급처리 사항</h3>
        <h5>각 단계별 진행 상태 현황 </h5>
      </div>
      <div class="content">
        <dl class="focus">
          <dt>최근거래 :  </dt>
          <dd><a href="index.php?act=store_order">리스트 <strong id="nc_progressing"></strong></a></dd>
          <dt>신고 :  </dt>
          <dd><a href="index.php?act=store_complain&select_complain_state=1">신고관리 <strong id="nc_complain"></strong></a></dd>
        </dl>
        <ul>
          <li><a href="index.php?act=store_order&op=index&state_type=state_new">입금대기중 <strong id="nc_payment"></strong></a></li>
          <li><a href="index.php?act=store_order&op=index&state_type=state_pay">발송요청 <strong id="nc_delivery"></strong></a></li>
          <li><a href="index.php?act=store_refund&lock=2"> <?php echo '판매전환불';?> <strong id="nc_refund_lock"></strong></a></li>
          <li><a href="index.php?act=store_refund&lock=1"> <?php echo '판매후환불';?> <strong id="nc_refund"></strong></a></li>
          <li><a href="index.php?act=store_return&lock=2"> <?php echo '판매전반품';?> <strong id="nc_return_lock"></strong></a></li>
          <li><a href="index.php?act=store_return&lock=1"> <?php echo '판매후반품';?> <strong id="nc_return"></strong></a></li>
          <li><a href="index.php?act=store_bill&op=index&bill_state=1"> <?php echo '발주확인';?> <strong id="nc_bill_confirm"></strong></a></li>
        </ul>
      </div>
    </div>
    <div class="container type-c">
      <div class="hd">
        <h3>판매실적</h3>
        <h5>주기별 통계에 따른 주문건수 및 주문금액 </h5>
      </div>
      <div class="content">
        <table class="ncsc-default-table">
          <thead>
            <tr>
              <th class="w50">항목</th>
              <th>주문건수</th>
              <th class="w100">주문금액</th>
            </tr>
          </thead>
          <tbody>
            <tr class="bd-line">
              <td>매출량/일</td>
              <td><?php echo empty($output['daily_sales']['ordernum']) ? '--' : $output['daily_sales']['ordernum'];?></td>
              <td><?php echo empty($output['daily_sales']['orderamount']) ? '--' : number_format($output['daily_sales']['orderamount'])."원";?></td>
            </tr>
            <tr class="bd-line">
              <td>매출량/월</td>
              <td><?php echo empty($output['monthly_sales']['ordernum']) ? '--' : $output['monthly_sales']['ordernum'];?></td>
              <td><?php echo empty($output['monthly_sales']['orderamount']) ? '--' : number_format($output['monthly_sales']['orderamount'])."원";?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="container type-c h500">
      <div class="hd">
        <h3>항목별 매출 순위</h3>
        <h5>30일 이내 최고 인기 상품 파악 및 재고 확보 </h5>
      </div>
      <div class="content">
        <table class="ncsc-default-table">
          <thead>
            <tr>
              <th>순위</th>
              <th class="tl" colspan="2">상품정보</th>
              <th>판매량</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($output['goods_list']) {?>
            <?php  $i = 0;foreach ($output['goods_list'] as $val) {$i++?>
            <tr class="bd-line">
              <td class="w50"><strong><?php echo $i;?></strong></td>
              <td class="w60"><div class="pic-thumb"><a href="<?php echo urlShop('goods', 'index', array('goods_id' => $val['goods_id']))?>" target="_blank"><img alt="<?php echo $val['goods_name'];?>" src="<?php echo thumb($val, '60');?>"></a></div></td>
              <td><dl class="goods-name">
                  <dt><a href="<?php echo urlShop('goods', 'index', array('goods_id' => $val['goods_id']))?>" target="_blank"><?php echo $val['goods_name'];?></a></dt>
                </dl></td>
              <td class="w60"><?php echo $val['goodsnum'];?></td>
            </tr>
            <?php }?>
            <?php }?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="container type-d h500">
      <div class="hd">
        <h3>프로모션관리</h3>
        <h5>광고/부가서비스를 통한 매출량 증가</h5>
      </div>
      <div class="content">
        <?php if (C('groupbuy_allow') == 1){ ?>
        <dl class="tghd">
          <dt class="p-name"> <a href="index.php?act=store_groupbuy">공동구매</a></dt>
          <dd class="p-ico"></dd>
          <dd class="p-hint">
            <?php if ($output['isOwnShop'] || !empty($output['groupquota_info'])) {?>
            <i class="icon-ok-sign"></i>사용
            <?php } else {?>
            <i class="icon-minus-sign"></i>사용안함
            <?php }?>
          </dd>
          <dd class="p-info">공동구매를 통한 상품 거래량/방문자를 증가</dd>
          <?php if (!empty($output['groupquota_info'])) {?>
          <dd class="p-point">(갱신：<?php echo date('Y-m-d', $output['groupquota_info']['end_time']);?>)</dd>
          <?php }?>
        </dl>
        <?php } ?>
        <?php if (intval(C('promotion_allow')) == 1){ ?>
        <dl class="xszk">
          <dt class="p-name"><a href="index.php?act=store_promotion_xianshi&op=xianshi_list">기간한정세일</a></dt>
          <dd class="p-ico"></dd>
          <dd class="p-hint"><span>
            <?php if ($output['isOwnShop'] || !empty($output['xianshiquota_info'])) {?>
            <i class="icon-ok-sign"></i>사용
            <?php } else {?>
            <i class="icon-minus-sign"></i>사용안함
            <?php }?>
            </span></dd>
          <dd class="p-info">규정된 시간과 상품에 한해서 할인 제공 </dd>
          <?php if (!empty($output['xianshiquota_info'])) {?>
          <dd class="p-point">(갱신：<?php echo date('Y-m-d', $output['xianshiquota_info']['end_time']);?>)</dd>
          <?php }?>
        </dl>
        <dl class="mjs">
          <dt class="p-name"><a href="index.php?act=store_promotion_mansong&op=mansong_list">금액별 사은품 증정</a></dt>
          <dd class="p-ico"></dd>
          <dd class="p-hint"><span>
            <?php if ($output['isOwnShop'] || !empty($output['mansongquota_info'])) {?>
            <i class="icon-ok-sign"></i>사용
            <?php } else {?>
            <i class="icon-minus-sign"></i>사용안함
            <?php }?>
            </span></dd>
          <dd class="p-info">일정 구매 금액 이상 사은품 증정 및 할인 제공</dd>
          <?php if (!empty($output['mansongquota_info'])) {?>
          <dd class="p-point">(갱신：<?php echo date('Y-m-d', $output['mansongquota_info']['end_time']);?>)</dd>
          <?php }?>
        </dl>
        <dl class="zhxs">
          <dt class="p-name"><a href="index.php?act=store_promotion_bundling&op=bundling_list">할인세트</a></dt>
          <dd class="p-ico"></dd>
          <dd class="p-hint"><span>
            <?php if ($output['isOwnShop'] || !empty($output['binglingquota_info'])) {?>
            <i class="icon-ok-sign"></i>사용
            <?php } else {?>
            <i class="icon-minus-sign"></i>사용안함
            <?php }?>
            </span></dd>
          <dd class="p-info">실용적인 세트상품으로 할인 및 상품구매 유도</dd>
          <?php if (!empty($output['binglingquota_info'])) {?>
          <dd class="p-point">(갱신：<?php echo date('Y-m-d', $output['binglingquota_info']['bl_quota_endtime']);?>)</dd>
          <?php }?>
        </dl>
        <dl class="tjzw">
          <dt class="p-name"><a href="index.php?act=store_promotion_booth&op=booth_list">메인광고</a></dt>
          <dd class="p-ico"></dd>
          <dd class="p-hint"><span>
            <?php if ($output['isOwnShop'] || !empty($output['boothquota_info'])) {?>
            <i class="icon-ok-sign"></i>사용
            <?php } else {?>
            <i class="icon-minus-sign"></i>사용안함
            <?php }?>
            </span></dd>
          <dd class="p-info">특정 상품에 대한 주요 프로모션 및 광고 진행 </dd>
          <?php if (!empty($output['boothquota_info'])) {?>
          <dd class="p-point">(갱신：<?php echo date('Y-m-d', $output['boothquota_info']['booth_quota_endtime']);?>)</dd>
          <?php }?>
        </dl>
        <?php } ?>
        <?php if (C('voucher_allow') == 1){?>
        <dl class="djq">
          <dt class="p-name"><a href="index.php?act=store_voucher">쿠폰</a></span></dt>
          <dd class="p-ico"></dd>
          <dd class="p-hint"><span>
            <?php if ($output['isOwnShop'] || !empty($output['voucherquota_info'])) {?>
            <i class="icon-ok-sign"></i>사용
            <?php } else {?>
            <i class="icon-minus-sign"></i>사용안함
            <?php }?>
            </span></dd>
          <dd class="p-info">쇼핑몰 내 통합적으로 사용할 수 있는 쿠폰 및 현금처럼 쓸 수있는 상품권</dd>
          <?php if (!empty($output['voucherquota_info'])) {?>
          <dd class="p-point">(갱신：<?php echo date('Y-m-d', $output['voucherquota_info']['quota_endtime']);?>)</dd>
          <?php }?>
        </dl>
        <?php }?>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
	var timestamp=Math.round(new Date().getTime()/1000/60);//异步URL一分钟变化一次
    $.getJSON('index.php?act=seller_center&op=statistics&rand='+timestamp, null, function(data){
        if (data == null) return false;
        for(var a in data) {
            if(data[a] != 'undefined' && data[a] != 0) {
                var tmp = '';
                if (a != 'goodscount' && a != 'imagecount') {
                    $('#nc_'+a).parents('a').addClass('num');
                }
                $('#nc_'+a).html(data[a]);
            }
        }
    });
});
</script>
