// dependencies external library
var Q = require("q");
var lib_vc = require("./libs/verifyCode.js");
var fs = require('fs');
var unzip = require('unzip2');
var is = require("is_js");

// dependencies internal module(and service)
var UUID = require("./libs/uuid.js");
var debug = require("./libs/debug.js");
var SQLService = require('../services/SQLService');
var FileService = require('../services/FileService');
var ErrorHandler = require('../services/ErrorHandler');
var ValidatorService = require('../services/ValidatorService');
var QueryService = require('../services/QueryService');
/*
 - Sails는 app init시, service를 전역객체로 자동등록
 - test 시에는 dependency module를 import해야함.
 */
//var PHPUnserialize = require('php-unserialize');


// export modules
module.exports = {
    attachTd:_attachTd,
    test_upload:view_test_upload,   //view_ prefix
    upload_td:upload_td,
    list:_test3dList,
    study:_study,
    test: _test,
    test_test: _test_test
};

function _test(req, res) {

    //sails.log.debug( '_attachTd allParams: ',req.allParams() );
    //var status = 500;
    //return res.send(status, 'aaaa');

    // 컨트롤러 내부에서 사용할 전역변수 생성
    var ATATCH_TASK_MODEL = {
      ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY
    };

   Q.fcall(_study1)   // DB에서 배너광고 데이터 생성
   .then ( // php에서 serialize함수로 처리된 데이터를 unserialize시키기
        function (adv_data) {
            /*
            var PHPUnserialize = require('php-unserialize');
            var array_unserialize_adv_data = PHPUnserialize.unserialize(adv_data[0].item_data);
            return array_unserialize_adv_data;
            */

            var obj = {};
            obj.name = 'john';
            obj.home = 'usa';
            /*
            obj.str = function() {
                var output = '';
                for (var i in obj) {
                    if (i != 'str') {
                        output += i + ':' + this[i] + '\n';
                    }
                }
                return output;
            };
*/

            if (is.existy(obj.name)) {
                console.log('값이 존재함');
            }

            console.log(req.ip);


            //console.log(output);
            
            var status = 200;
            return res.send(status, 'protocol '+ req.protocol + 'hostname ' + req.get('hostname') + ' querystring ' + req.originalUrl);
            

        }
    );

    /*
    .then ( // unserialize된 데이터를 가공하여 광고데이터만 리스트 배열로 생성
        function (array_unserialize_adv_data) {
            var res = [];
            var n = 0;
            var adv_item_list = array_unserialize_adv_data.item;
            for (i in adv_item_list) {
                res[n] = adv_item_list[i];
                n++;
                //console.log(adv_item_list[i].data);
            }
            return res;
        }
    )
    .then ( // status 값과 함께 결과값을  json타입으로 레핑한후 리턴  
        function (array_adv_data) {

            var result = {
                            code: 200,
                            datas: {
                                adv_list: array_adv_data
                            }
                         };

            return res.send(200, result);
        }
    )
    .catch(_taskDefaultError); // 오류발생시 처리
*/

    // DB에서 배너광고 데이터 생성 
    function _study1() {
        return 'abcd';
        /*
        return SQLService.queryOnPromise(
                    ATATCH_TASK_MODEL.ATTACH_TD_QRY.STUDY_BANNER,
                    []
        );  
        */
    }  

    function _taskDefaultError(__err){
        sails.log.debug('_taskDefaultError ERROR: ' , __err);

        var err_code = __err.code || 'ERR_FAIL';
        var tmpStatus = __err.status || 500;
        var idx = __err.index || null;
        var message = __err.message || null;
        var key = (err_code==='ER_BAD_BLANK_ERROR' || err_code==='ER_UNDEFINED_PARAM_ERROR')?__err.key:null;

        var resultObj = {code:err_code};
        if(idx) resultObj.index = idx;
        if(key) resultObj.key = key;
        if(message) resultObj.message = message;

        res.status(tmpStatus);
        return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), resultObj ));
  }
}



function _test_test(req, res) {

    //sails.log.debug( '_attachTd allParams: ',req.allParams() );
    //var status = 500;
    //return res.send(status, 'aaaa');

    // 컨트롤러 내부에서 사용할 전역변수 생성
    var ATATCH_TASK_MODEL = {
      ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY
    };

   Q.fcall(_study1)   // DB에서 배너광고 데이터 생성
   .then ( // php에서 serialize함수로 처리된 데이터를 unserialize시키기
        function (adv_data) {
            var PHPUnserialize = require('php-unserialize');
            var array_unserialize_adv_data = PHPUnserialize.unserialize(adv_data[0].item_data);
            return array_unserialize_adv_data;
        }
    )
    .then ( // unserialize된 데이터를 가공하여 광고데이터만 리스트 배열로 생성
        function (array_unserialize_adv_data) {
            var res = [];
            var n = 0;
            var adv_item_list = array_unserialize_adv_data.item;
            for (i in adv_item_list) {
                res[n] = adv_item_list[i];
                n++;
                //console.log(adv_item_list[i].data);
            }
            return res;
        }
    )
    .then ( // status 값과 함께 결과값을  json타입으로 레핑한후 리턴  
        function (array_adv_data) {

            var result = {
                            code: 200,
                            datas: {
                                adv_list: array_adv_data
                            }
                         };

            return res.send(200, result);
        }
    )
    .catch(_taskDefaultError); // 오류발생시 처리

    // DB에서 배너광고 데이터 생성 
    function _study1() {
        return SQLService.queryOnPromise(
                    ATATCH_TASK_MODEL.ATTACH_TD_QRY.STUDY_BANNER,
                    []
        );  
    }  

    function _taskDefaultError(__err){
        sails.log.debug('_taskDefaultError ERROR: ' , __err);

        var err_code = __err.code || 'ERR_FAIL';
        var tmpStatus = __err.status || 500;
        var idx = __err.index || null;
        var message = __err.message || null;
        var key = (err_code==='ER_BAD_BLANK_ERROR' || err_code==='ER_UNDEFINED_PARAM_ERROR')?__err.key:null;

        var resultObj = {code:err_code};
        if(idx) resultObj.index = idx;
        if(key) resultObj.key = key;
        if(message) resultObj.message = message;

        res.status(tmpStatus);
        return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), resultObj ));
  }
}


/*
  1. 등록자 및 업체 체크
 - 등록자 : cat 권한이 있는 계정이 아니다. 에러
 - 업체 : cat 권한이 있는 계정이 속한 업체 (이후부터 업체)

select a.store_id ( 1.store_id )
from sv_seller a
left join sv_seller_group b on a.seller_group_id = b.group_id
where (a.is_admin = 1 or find_in_set('cat_login', b.limits))
and a.member_id = 'account_uuid';




 2. 상품체크
 - 결과값이 없는 경우 : 등록된 상품이 아니다. 에러
 - 결과값이 있는 경우 : sv_3dc_exposure에 등록

select a.goods_id, a.goods_commonid, a.goods_name, a.store_id, b.store_name, a.brand_id, c.brand_name
( 2.goods_id, 2.goods_commonid, 2.goods_name, 2.store_id, 2.store_name, 2.brand_id, 2.brand_name )
from sv_goods a
left join sv_store b on a.store_id = b.store_id
left join sv_brand c on a.brand_id = c.brand_id
where a.store_id = '1.store_id'
and a.goods_id = 'item_id';




 3. 매핑할 3D 상품 LIST  체크
 - 업체에 3D 상품이 속하여 있는 지 확인

select if(count(*) > 0, true, false) having_3dc
from sv_3dc
where store_id = 1
and find_in_set(3dc_no, 'td_ids');




	4. 2,3 에서 검색한 내용으로 등록

replace into sv_3dc_exposure
(3dc_no, goods_id, goods_commonid, goods_name, store_id, store_name, brand_id, brand_name, 3dc_representative_yn, regist_date)
(select 3dc_no,
    '2.goods_id' ,
    '2.goods_commonid' ,
    '2.goods_name' ,
    '2.store_id' ,
    '2.store_name' ,
    '2.brand_id' ,
    '2.brand_name' ,
    (str_rep_td : value에 따라서 선택
     if(3dc_no in ('str_rep_td'), true, false),
     false
     )
    unix_timestamp(now())
from sv_3dc
where store_id = '1.store_id'
and find_in_set(3dc_no, 'td_ids')
);




	5. 과거 등록 내용 삭제

delete
    from sv_3dc_exposure
where store_id = '1.store_id'
and goods_id = '2.goods_id'
and not find_in_set(3dc_no, 'td_ids');


*/
/* 연습용 컨트롤러 2 */
/*
function _study1(req, res) {

    sails.log.debug( '_attachTd allParams: ',req.allParams() );

    var ATATCH_TASK_MODEL = {
        ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY,
        params:req.allParams(),
        data:{
            goods_id:'',
            goods_commonid:'',
            goods_name:'',

            store_id:'',
            store_name:'',

            brand_id:'',
            brand_name:''
        }
    };

      Q.fcall(mytest1)
      .then( // mytest에서 return으로 전달한 값받아서 처리
          function(value) { // mytest에서 리턴값받기
              console.log(value);
              var list = {};
              //sails.log.debug( 'list: ', UPLOAD_TASKS_MODEL );

              var strQuery = "select * ";
              strQuery += "from sv_mb_special_item ";
              strQuery += " where item_type = 'adv_list'";
              var data = {};
              data.code = 200;
              data.datas = {};
              data.datas.adv_list = {};
              data.datas.adv_list.item = [];

              //return res.send(data.code, value);
              DB.query(strQuery, function (err, results) {
                if (err) {
                    status = 500;
                    return res.send('db error');
                } else {
                    status = 200;
                }

                //var unseri = PHPUnserialize.unserialize(results[0].item_data);
                data.datas.adv_list.item = unseri['item'];
                return res.send(status, unseri);                

              });

      });


      function mytest1() {
          var PHPUnserialize = require('php-unserialize');
          console.log("In mytest");

          var UPLOAD_TASKS_MODEL = {
              UPLOAD_TD_QRY: QueryService.UPLOAD_TD_QRY
          };
          var deferred = Q.defer();
          var id = 'admin';
          
          //사용법
          //SQLService.queryOnPromise(query, 쿼리에 전달하는 파라미터들);


         // return SQLService.queryOnPromise(
         //   UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_ADMIN,
         //   [id]
         // );

        var data = {};
        data.code = '200';
        data.datas = {};
        
        //DB.query(strQuery, function (err, results) {
        //  if (err) {
        //      status = 500;
        //      return res.send('db error');
        //  } else {
        //      status = 200;
        //  }

          var unseri = PHPUnserialize.unserialize(results[0].item_data);
          data.datas.adv_list.item = unseri['item'];

          //var result_keys = {};
          //var j = 0;
          //for (i in unseri) {
          //    result_keys[j] = i;
          //    j++;
          //}


          //return res.send(PHPUnserialize.unserialize('a:0:{}'));
          //return res.send(PHPUnserialize.unserialize(results[0].item_data));
          //return res.send(status, PHPUnserialize.unserialize(results[0]));
          //return res.send(results[0].item_data);
          //return res.send(status, unseri);
          return data.datas.adv_list.item;
        });
        
        return 'aaa';
      }
      
    }
*/





/* 연습용 컨트롤러 */
function _study(req, res) {

    //var chkData = require("./libs/chkData.js");
    sails.log.debug( '_attachTd allParams: ',req.allParams() );
    var status = 500;

    //return res.send(status, 'aaaa');

    var ATATCH_TASK_MODEL = {
      ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY,
      params:req.allParams(),
      data:{
        goods_id:'chasc',
        goods_commonid:'',
        goods_name:'',

        store_id:'',
        store_name:'',

        brand_id:'',
        brand_name:''
      }
    };


    Q.fcall(_study1)   // 파라미터 검증
    .then (
        function (value) {

          //var val1 = 'a:3:{s:24:"s0_05259886054442602.jpg";a:3:{s:5:"image";s:24:"s0_05259886054442602.jpg";s:4:"type";s:3:"url";s:4:"data";s:21:"http://www.fxgear.net";}';
          console.log('object property: ' + ATATCH_TASK_MODEL.params.item_type);
          console.log('object property: ' + ATATCH_TASK_MODEL.data.goods_id);

          //var aaa = PHPUnserialize.unserialize(value[0].item_data);

         // var len = 0;
          // 이미지 키값들 출력시키기

          //var result_advlist = {};
          //for (i in aaa['item']) {
              //result_advlist[] = aaa['item'][i];
              //console.log("array length: " + i);
              //console.log("array length: " + aaa['item'][i].data);
          //}

          
          //var res = [];
          //var n = 0;
          //var item_list = aaa.item;
          //for(i in item_list) {
          //  console.log("adv_list key " + i);
          //  res[n] = item_list[i];
          // n++;
          //}
          //return res;
          return value;

    });
    /*
    .then(
        function (value) {   // DB 데이터생성 및 출력

            //return res.send(status, results);

            
            //var PHPUnserialize = require('php-unserialize');
            //var unseri = PHPUnserialize.unserialize(value);

            //var val1 = 'a:3:{s:24:"s0_05259886054442602.jpg";a:3:{s:5:"image";s:24:"s0_05259886054442602.jpg";s:4:"type";s:3:"url";s:4:"data";s:21:"http://www.fxgear.net";}';

            //var aaa = PHPUnserialize.unserialize(value[0]['item_data']);

            //var len = 0;
            // 이미지 키값들 출력시키기

            //var result_advlist = {};
           // for (i in aaa['item']) {
                //result_advlist[] = aaa['item'][i];
                //console.log("array length: " + i);
                //console.log("array length: " + aaa['item'][i].data);
            //}



            //var res = [];
            //var n = 0;
            //var item_list = aaa.item;
            //for(i in item_list) {
            //	console.log("adv_list key " + i);
            	//res[n] = item_list[i];
            	//n++;
            //}
            //console.log(res);
            

            
            //광고 데이터만 출력시키기

            // 데이터뽑아올 쿼리
           // STUDY_BANNER: [
           //     "select * ",
           //     "from sv_mb_special_item ",
            //    " where item_type = 'adv_list'"
            //].join('')

           // var aaa = PHPUnserialize.unserialize(value[0].item_data);

            // 임의의 키값으로 값 출력하기
            //for (i in aaa['item']) {
            //    console.log("array length: " + i);
            //    console.log("array length: " + aaa['item'][i].data);
            //}


            //console.log(aaa['item'][i].image);
            //console.log(aaa['item'][i].url);
            //console.log(aaa['item'][i].data);
            

            //value[0].item_data
            //
            //   item => {
            //            '*.jpg': {image: '*.jpg', type: 'url', data: 'http://www.naver.com'},
            //            '*.jpg': {image: '*.jpg', type: 'url', data: 'http://www.naver.com'},
            //            '*.jpg': {image: '*.jpg', type: 'url', data: 'http://www.naver.com'}
            //           }

            // var aaa = value[0].item_data;
           // aaa['item'] = {
            //                '*.jpg': {image: '*.jpg', type: 'url', data: 'http://www.naver.com'},
            //                '*.jpg': {image: '*.jpg', type: 'url', data: 'http://www.naver.com'},
            //                '*.jpg': {image: '*.jpg', type: 'url', data: 'http://www.naver.com'}
             //             }


            var result = {
              code: 200,
              datas: {
                adv_list: value
              }
            };


            //return res.send(status, aaa['item']);
            return res.send(status, result);
            //return value;
    });
    */

    //.catch(_taskDefaultError);                  // ** default error excute    
    //.then(_study2)
    //.catch(_taskDefaultError);

    function _study1() {

        //sails.log.debug( 'global val: ', ATTACH_TD_QRY1:QueryService.ATTACH_TD_QRY);

        /*
        var ATATCH_TASK_MODEL = {
            ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY
        };
        */

        /*
        return SQLService
            .queryOnPromise(
                ATATCH_TASK_MODEL.ATTACH_TD_QRY.STUDY_BANNER,
                []
            );
            */
        return 'abcd';    
      //return ATATCH_TASK_MODEL.ATTACH_TD_QRY.STUDY_ADMIN;
    }

    /*
    function _study2() {
        return res.send('200', data);
    }
    */

} // end of _study controller


function _attachTd(req, res){
    /*
     account_uuid	client uuid	String
     secret	client secret data (in the client login session data)	String
     verify_code	client verification code(be made by client_uuid)	String
     item_id	item id	Integer
     td_ids	comma(,) delimited 3D Cloth ids	String
     td_rep_ids	comma(,) delimited represent 3D Cloth ids	String
     show	for viewing some information(debug ...)

     http://git.fxgear.net/webdev/fitnshop/wikis/attach3Ddata2Item
    */

    var ATATCH_TASK_MODEL = {
        ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY,
        params:req.allParams(),

        data:{
            goods_id:'',
            goods_commonid:'',
            goods_name:'',

            store_id:'',
            store_name:'',

            brand_id:'',
            brand_name:''
        }

    };


    Q.fcall(_taskValidateParams)// 1. parameter 검증
        .spread(_taskCheckVerifyCode)
        .spread(_taskCheckStoreAndSeller)
        .spread(_taskSideEffect(function(_arg){
            console.log('_taskCheckStoreAndSeller _taskSideEffect _arg: ' , _arg);
        }))
        .spread(_taskCheckGoods)
        .spread(_taskCheckHaving3DC)
        .spread(_taskRegist3DCExposure)
        .spread(_taskDeleteOld3DCExposure)
        .spread(_taskAllSettled)
        .catch(_taskDefaultError);



    function _taskSideEffect(func){
        return function(){
            func(arguments);
            return arguments;
        }
    }

    function _taskValidateParams(){
        return Q.all([
            ValidatorService.validatePromise(
                {
                    data:_.omit(ATATCH_TASK_MODEL.params,['show']),
                    passingData:ATATCH_TASK_MODEL,
                    validator:[
                        {key:'account_uuid' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'secret' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'verify_code' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'item_id' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'td_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'td_rep_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }}
                    ]
                }
            )
        ]);
    }

    function _taskCheckVerifyCode(){
        console.log( '_taskCheckVerifyCode:::: '  );

        return Q.all([
            lib_vc
                .chkVerifyCodeOnPromise({
                    key:ATATCH_TASK_MODEL.params.account_uuid,
                    verify_code:ATATCH_TASK_MODEL.params.verify_code,
                    account_uuid:ATATCH_TASK_MODEL.params.account_uuid,
                    secret:ATATCH_TASK_MODEL.params.secret
                })
        ]);
    }

    function _taskCheckStoreAndSeller(){
        /*
         1. 등록자 및 업체 체크
         - 등록자 : cat 권한이 있는 계정이 아니다. 에러
         - 업체 : cat 권한이 있는 계정이 속한 업체 (이후부터 업체)

         select a.store_id ( 1.store_id )
         from sv_seller a
         left join sv_seller_group b on a.seller_group_id = b.group_id
         where (a.is_admin = 1 or find_in_set('cat_login', b.limits))
         and a.member_id = 'account_uuid';

         ATATCH_TASK_MODEL = {
         ATTACH_TD_QRY
        */

        return SQLService
            .queryOnPromise(
                ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_SELLER,
                [ATATCH_TASK_MODEL.params.account_uuid]
            );

    }


    function _taskCheckGoods(){
        /*
         2. 상품체크
         - 결과값이 없는 경우 : 등록된 상품이 아니다. 에러
         - 결과값이 있는 경우 : sv_3dc_exposure에 등록

         select a.goods_id, a.goods_commonid, a.goods_name, a.store_id, b.store_name, a.brand_id, c.brand_name
         ( 2.goods_id, 2.goods_commonid, 2.goods_name, 2.store_id, 2.store_name, 2.brand_id, 2.brand_name )
         from sv_goods a
         left join sv_store b on a.store_id = b.store_id
         left join sv_brand c on a.brand_id = c.brand_id
         where a.store_id = '1.store_id'
         and a.goods_id = 'item_id';

         */
        return SQLService
            .queryOnPromise(
                ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_GOODS,
                [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.params.item_id]
            );
    }

    function _taskCheckHaving3DC(){
        /*

         3. 매핑할 3D 상품 LIST  체크
         - 업체에 3D 상품이 속하여 있는 지 확인

         select if(count(*) > 0, true, false) having_3dc
         from sv_3dc
         where store_id = 1
         and find_in_set(3dc_no, 'td_ids');
         */

        return SQLService
            .queryOnPromise(
                ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_3DC,
                [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.params.td_ids]
            );
    }

    function _taskRegist3DCExposure(){
        /*

         4. 2,3 에서 검색한 내용으로 등록
         */
    }

    function _taskDeleteOld3DCExposure(){
        /*

         4. 2,3 에서 검색한 내용으로 등록
         */
    }

    function _taskAllSettled(){

    }

    function _taskDefaultError(__err){
        sails.log.debug('upload_td ERROR: ' , __err);

        var err_code = 'ERR_FAIL';
        var tmpStatus = 500;

        //  TODO : ERROR format 정규화 및 response 처리

        try{
            if( __err.stack.indexOf('ER_EMPTY_FILE')!==-1 ){
                tmpStatus = 400;
                err_code = 'ER_EMPTY_FILE';
            }
        }catch(e){
        }

        res.status(tmpStatus);
        return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), {code:err_code} ));
    }

}

function view_test_upload( req, res ){
    var tmpPort = process.env.PORT || req.port;
    if(process.env['NODE_ENV']!=='local') tmpPort=80;
    return res.view('test_upload', {imgURL:req.protocol+'://'+req.host+':'+tmpPort});
}

function upload_td( req, res ){
    console.log( 'upload_td allParams: ', req.allParams() );

    // Context Object about Upload TD Tasking ( * Promise(Q) 사용시 dependiency 되는 Context 객체 )
    var UPLOAD_TASKS_MODEL ={
        //Dependency Object
        UPLOAD_TD_QRY:QueryService.UPLOAD_TD_QRY,
        req:req,
        res:res,
        params:req.allParams(),

        //local file upload info
        zip_src:null,
        unzip_uuid:'',
        unzip_path:null,
        upload_req_list:null,
        upload_req_file:null,
        upload_max_bytes:100000000,
        fitting_zip_file:null,
        files_hash:null,

        //goods info
        goods_commonid:'',
        goods_name:'',
        goods_sex:'',
        goods_kids:'',

        //store info
        store_id:'',
        store_name:'',

        //brand info
        brand_id:'',
        brand_name:'',

        //3D info
        cate_no:'',
        representative_yn:'',
        td_name:'',
        td_no:'',
        td_id:'',
        td_cd:'',

        td_reg_result:null //성공시 마지막에 보낼 값
    };

    //promise tasks
    Q.fcall(_taskValidateParams)                    // 1. parameter 검증
        .spread(_taskCheckVerifyCode)               // 2. verify code 체킹
        .spread(_taskZipFileUpload)                 // 3. zip file upload
        .spread(_taskMakeDir)                       // 4. upzip위한 directory 생성
        .spread(_taskUploadFittingFilesInLocal)     // 5. 생성된 폴더에 unzip
        .spread(_taskMakeFilesHash)                 // 6. unzip한 파일들의 hash 코그 생성
        .spread(_taskCheckMemberAndGetStoreInfo)    // 7. 멤버 체킹 , store 정보 fetch
        .spread(_taskChkCateName)                   // 8. cate_no fetch
        .spread(_taskInsert3DC)                     // 9. 3d정보 insert
        .spread(_taskSelect3DC)                     // 10. insert한 3d정보 확인
        .spread(_taskSelectItemsInfo)               // 11. item, brand, 성별키즈정보 fetch
        // 12. parameter(make_category_name)의 성별과 item의 성별정보 체킹 (*)
        .spread(_taskUploadFiles)                   // 13. uplaod  SFTP
        .spread(_taskReplace3DDataQuery)            // 14. replace query 3d file
        .spread(_taskBeforeInsert3DCExpsr)          // 15. before insert 3DC EXPSR
        .spread(_taskInsert3DCExpsr)                // 16. insert 3DC EXPSR
        .spread(_taskRemoveLocalFiles)              // 17. local file remove
        .spread(_taskAllSettled)                    // 18. 완료
        .catch(_taskDefaultError);                  // ** default error excute

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //task method START
    function _taskValidateParams(){
        return Q.all([
            ValidatorService.validatePromise(
                {
                    data:_.omit(UPLOAD_TASKS_MODEL.params,['fitting_zip_file', 'id']),
                    passingData:UPLOAD_TASKS_MODEL,
                    validator:[
                        {key:'account_uuid' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'secret' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'verify_code' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'item_id' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'make_category_name' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                        {key:'td_name' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }}
                    ]
                }
            )
        ]);
    }

    function _taskCheckVerifyCode(){
        console.log( '_taskCheckVerifyCode:::: '  );

        return Q.all([
            lib_vc
                .chkVerifyCodeOnPromise({
                    key:UPLOAD_TASKS_MODEL.params.account_uuid,
                    verify_code:UPLOAD_TASKS_MODEL.params.verify_code,
                    account_uuid:UPLOAD_TASKS_MODEL.params.account_uuid,
                    secret:UPLOAD_TASKS_MODEL.params.secret
                })
        ]);
    }

    function _taskZipFileUpload(){
        console.log( '_taskZipFileUpload:::: '  );
        var deferred = Q.defer();

        UPLOAD_TASKS_MODEL.fitting_zip_file = UPLOAD_TASKS_MODEL.req.file('fitting_zip_file');
        Q.all([
            Q.nbind(UPLOAD_TASKS_MODEL.fitting_zip_file.upload, UPLOAD_TASKS_MODEL.fitting_zip_file)({maxBytes:UPLOAD_TASKS_MODEL.upload_max_bytes})
        ]).spread(function(__results){
            console.log('_taskZipFileUpload: ',__results);

            if( __results.length === 0 ) {
                deferred.reject("ER_EMPTY_FILE");
            }else{
                UPLOAD_TASKS_MODEL.zip_src = __results[0].fd;
                UPLOAD_TASKS_MODEL.unzip_uuid = UUID.setUUID();
                UPLOAD_TASKS_MODEL.unzip_path = './.tmp/uploads/'+UPLOAD_TASKS_MODEL.unzip_uuid;
                UPLOAD_TASKS_MODEL.upload_req_list = [];
                UPLOAD_TASKS_MODEL.upload_req_file = [];
                deferred.resolve(UPLOAD_TASKS_MODEL);
            }
        }).catch(function(e){
            deferred.reject("ER_EMPTY_FILE");
        });

        return deferred.promise;
    }


    function _taskMakeDir(){
        console.log( '_taskMakeDir:::: '  );

        return Q.all([
            Q.nbind(fs.mkdir, fs)(UPLOAD_TASKS_MODEL.unzip_path)
        ]);
    }

    function _taskUploadFittingFilesInLocal(){
        console.log( '_taskUploadFittingFilesInLocal:::: '  );

        return _uploadFittingFilesInLocal(UPLOAD_TASKS_MODEL);
    }

    function _taskMakeFilesHash(){
        console.log( '_taskMakeFilesHash:::: '  );

        var deferred = Q.defer();
        var promises = UPLOAD_TASKS_MODEL.upload_req_list.map(function(__fileResource){
            return FileService.computeFileHashPromisify(__fileResource);
        });

        Q.all( promises )
            .spread(function(){
                UPLOAD_TASKS_MODEL.files_hash = arguments;
                deferred.resolve(UPLOAD_TASKS_MODEL);
            })
            .catch(function(e){
                deferred.reject('ER_NETWORK');
            });

        return deferred.promise;
    }

    function _taskCheckMemberAndGetStoreInfo(){
        console.log( '_taskCheckMember:::: '  );

        var deferred = Q.defer();
        SQLService
            .queryOnPromise(
                UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_STORE_INFO,
                [UPLOAD_TASKS_MODEL.params.account_uuid]
            ).then(function(results){
                if( parseInt(results[0].member_id) !== parseInt(UPLOAD_TASKS_MODEL.params.account_uuid) ){
                    deferred.reject('ER_NOT_MATCH_MEMBER_ID_AND_ACCOUNT_ID');
                }else{
                    console.log('_taskCheckMember results: ' , results);
                    UPLOAD_TASKS_MODEL.store_id = results[0].store_id;
                    UPLOAD_TASKS_MODEL.store_name = results[0].store_name;
                    deferred.resolve(UPLOAD_TASKS_MODEL);
                }
            }).catch(function(e){
                deferred.reject('ER_NETWORK_CHECK_MEMBER');
            });
            return deferred.promise;
    }

    function _taskChkCateName(){
        console.log( '_taskChkCateName:::: '  );

        var deferred = Q.defer();
        SQLService
            .queryOnPromise(
                UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_CAT_NO_FROM_3DC_CATE,
                [UPLOAD_TASKS_MODEL.params.make_category_name]
            ).then(function(results){
                // console.log('_taskChkCateName results: ' , results);
                if( results.length<1 ){
                    deferred.reject(ErrorHandler.NOT_FOUND_DATA.code + ": make_category_name ");
                }else{
                    UPLOAD_TASKS_MODEL.cate_no = results[0].cate_no;
                    deferred.resolve(UPLOAD_TASKS_MODEL);
                }

            }).catch(function(e){
                deferred.reject('ER_NETWORK_CHECK_CATE_NAME');
            });

        return deferred.promise;
    }


    function _taskInsert3DC(){
        console.log( '_taskInsert3DC:::: '  );

        var deferred = Q.defer();
        SQLService
            .queryOnPromise(
                UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.INSERT_INTO_3DC,
                [
                    UPLOAD_TASKS_MODEL.params.td_name, UPLOAD_TASKS_MODEL.cate_no, 'activated',
                    (UPLOAD_TASKS_MODEL.params.td_description || ''), UPLOAD_TASKS_MODEL.store_id, UPLOAD_TASKS_MODEL.store_name
                ]
            ).then(function(results){
                console.log('_taskInsert3DC results: ' , results);
                UPLOAD_TASKS_MODEL.td_no = results.insertId;
                UPLOAD_TASKS_MODEL.td_reg_result = results;
                deferred.resolve(UPLOAD_TASKS_MODEL);

            }).catch(function(e){
                deferred.reject('ER_NETWORK_INSERT_3DC');
            });

        return deferred.promise;
    }

    function _taskSelect3DC(){
        console.log( '_taskSelect3DC:::: '  );

        var deferred = Q.defer();
        SQLService
            .queryOnPromise(
                UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_TD_ID_TD_CD_FROM_3DC,
                [UPLOAD_TASKS_MODEL.td_no]
            ).then(function(results){
                console.log('_taskSelect3DC results: ' , results);
                if( results.length<1 ){
                    deferred.reject("ERR_NOT_FOUND_TD_DATA");
                }else{
                    UPLOAD_TASKS_MODEL.td_id = results[0].td_id;
                    UPLOAD_TASKS_MODEL.td_cd = results[0].td_cd;
                    deferred.resolve(UPLOAD_TASKS_MODEL);
                }
            }).catch(function(e){
                deferred.reject('ER_NETWORK_INSERT_3DC');
            });
        return deferred.promise;
    }


    function _taskSelectItemsInfo(){
        console.log( '_taskSelectItemsInfo:::: '  );

        var deferred = Q.defer();
        // UploadTasksModel.UPLOAD_TD_QRY.SELECT_ITEM , UploadTasksModel.params.item_id
        // goods_commonid , goods_name , store_id , store_name, brand_id , brand_name

        SQLService
            .queryOnPromise(
                UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_ITEM,
                [UPLOAD_TASKS_MODEL.params.item_id]
            ).then(function(results){
                console.log('_taskSelect3DC results: ' , results);
                if( results.length<1 ){
                    deferred.reject("ERR_NOT_FOUND_ITEMS_DATA");
                }else{
                    UPLOAD_TASKS_MODEL.goods_commonid = results[0].goods_commonid;
                    UPLOAD_TASKS_MODEL.goods_name = results[0].goods_name;
                    UPLOAD_TASKS_MODEL.brand_id = results[0].brand_id;
                    UPLOAD_TASKS_MODEL.brand_name = results[0].brand_name;
                    UPLOAD_TASKS_MODEL.goods_sex = results[0].goods_sex;
                    UPLOAD_TASKS_MODEL.goods_kids = results[0].goods_kids;

                    deferred.resolve(UPLOAD_TASKS_MODEL);
                }
            }).catch(function(e){
                deferred.reject('ER_NETWORK_SELECT_ITEMS_DATA');
            });

        return deferred.promise;
    }

    function _taskVadateSex(){
        console.log( '_taskSelectItemsInfo:::: '  );
        var deferred = Q.defer();
        var chk = true;
        var msg = '';
        /*
         UPLOAD_TASKS_MODEL.goods_sex   //  woman , man , unisex
         UPLOAD_TASKS_MODEL.goods_kids  // 0(어른) ,1(아이)
         UPLOAD_TASKS_MODEL.params.make_category_name   // WonClo , ManClo, Etc
        */

        if( _isKids(UPLOAD_TASKS_MODEL.params.make_category_name) ){
            //make_category_name가 '아동'
            if(parseInt(UPLOAD_TASKS_MODEL.goods_kids)===0){
               //goods_kids 가 어른일때
                chk = false;
            }

        }else{
            //make_category_name가 '성인'
            if(parseInt(UPLOAD_TASKS_MODEL.goods_kids)===1){
                //goods_kids 가 아동일때
                chk = false;
            }

            if(UPLOAD_TASKS_MODEL.goods_sex.toLowerCase()==='unisex'){
                //goods_sex 가 일때
                chk = false;
                msg = '';
            }
        }

        if(chk){
            deferred.resolve(UPLOAD_TASKS_MODEL);
        }else{
            deferred.reject(msg);
        }


        function _isKids(cate_str){
            return ( (cate_str) && (cate_str.toLowerCase().indexOf('kid')!==-1) )?true:false;
        }
        return deferred.promise;
    }


    function _taskUploadFiles(){
        console.log( '_taskUploadFiles:::: '  );
        return Q.all([ FileService.uploadThreedFiles( UPLOAD_TASKS_MODEL ) ]);
    }

    function _taskReplace3DDataQuery(){
        console.log( '_taskReplace3DDataQuery:::: '  );

        var promises = [];
        for(var i in UPLOAD_TASKS_MODEL.files_hash){
            promises.push(
                SQLService.queryOnPromise(
                    UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.REPLACE_3DC_FILE,
                    [
                        UPLOAD_TASKS_MODEL.td_id ,
                        String(UPLOAD_TASKS_MODEL.files_hash[i].source).substr( String(UPLOAD_TASKS_MODEL.files_hash[i].source).lastIndexOf('/')+1 ) ,
                        UPLOAD_TASKS_MODEL.files_hash[i].hash
                    ]
                )
            );
        }
        return Q.all(promises);
    }

    function _taskBeforeInsert3DCExpsr(){
        console.log( '_taskBeforeInsert3DCExpsr:::: '  );
        var deferred = Q.defer();

        SQLService
            .queryOnPromise(
                UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.BEFORE_INSERT_INTO_3DC_EXPSR,
                [UPLOAD_TASKS_MODEL.params.item_id]
            ).then(function(results){
                console.log( '_taskBeforeInsert3DCFile arguments:::: ', arguments  );
                deferred.resolve(UPLOAD_TASKS_MODEL);
            }).catch(function(e){
                console.log('_taskBeforeInsert3DCExpsr Error');
                deferred.resolve(UPLOAD_TASKS_MODEL);
            });
        return deferred.promise;
    }

    function _taskInsert3DCExpsr(){
        console.log( '_taskInsert3DCExpsr:::: '  );
        var deferred = Q.defer();

        SQLService.queryOnPromise(
            UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.INSERT_INTO_3DC_EXPSR,
            [
                UPLOAD_TASKS_MODEL.td_id ,
                UPLOAD_TASKS_MODEL.params.item_id,
                UPLOAD_TASKS_MODEL.goods_commonid,
                UPLOAD_TASKS_MODEL.goods_name,

                UPLOAD_TASKS_MODEL.store_id,
                UPLOAD_TASKS_MODEL.store_name,
                UPLOAD_TASKS_MODEL.brand_id,
                UPLOAD_TASKS_MODEL.brand_name
            ]
        ).then(function(results){
            console.log( '_taskInsert3DCExpsr arguments:::: ', arguments  );
            deferred.resolve(UPLOAD_TASKS_MODEL);
        }).catch(function(e){
            console.log('_taskInsert3DCExpsr Error', e);
            deferred.reject('ER_NETWORK_3DC_EXPSR');
        });

        return deferred.promise;
    }

    function _taskRemoveLocalFiles(){
        sails.log.debug('_taskRemoveLocalFiles:: ' );

        for(var i in UPLOAD_TASKS_MODEL.upload_req_list){
            fs.unlinkSync(UPLOAD_TASKS_MODEL.upload_req_list[i]);
        }
        fs.unlinkSync(UPLOAD_TASKS_MODEL.zip_src);
        fs.rmdirSync(UPLOAD_TASKS_MODEL.unzip_path);

        return Q.all([ UPLOAD_TASKS_MODEL ]);
    }

    function _taskAllSettled(){
        console.log( '_taskAllSettled final results: ' );

        return res.send(
            200,
            debug.wrap(
                UPLOAD_TASKS_MODEL.req,
                UPLOAD_TASKS_MODEL.res,
                200,
                {
                    fieldCount: 0,
                    affectedRows: 1,
                    insertId: UPLOAD_TASKS_MODEL.td_no,
                    serverStatus: 2,
                    warningCount: 0,
                    message: "",
                    protocol41: true,
                    results:[{
                        upload_req_file:UPLOAD_TASKS_MODEL.upload_req_file,
                        files_hash:UPLOAD_TASKS_MODEL.files_hash,
                        td_id:UPLOAD_TASKS_MODEL.td_id,
                        td_cd:UPLOAD_TASKS_MODEL.td_cd
                    }]
                }
            )
        );

    }

    function _taskDefaultError(__err){
        sails.log.debug('upload_td ERROR: ' , __err);

        var err_code = 'ERR_FAIL';
        var tmpStatus = 500;

        //  TODO : ERROR format 정규화 및 response 처리
        // ER_SECRET_ERROR , ER_VERIFY_ERROR , ER_BAD_BLANK_ERROR|ER_UNDEFINED_PARAM_ERROR ,
        // ER_NOT_MATCH : account_uuid , ER_NOT_MATCH : make_category_name , ER_UNDEFINED Error: package.bin is invalid format

        try{
            if( __err.stack.indexOf('ER_EMPTY_FILE')!==-1 ){
                tmpStatus = 400;
                err_code = 'ER_EMPTY_FILE';
            }else if(__err.stack.indexOf('ER_NOT_MATCH_MEMBER_ID_AND_ACCOUNT_ID')!==-1){
                tmpStatus = 400;
                err_code = 'ER_NOT_MATCH_MEMBER_ID_AND_ACCOUNT_ID';
            }else if( __err.stack.indexOf(ErrorHandler.NOT_FOUND_DATA.code + ": make_category_name ")!==-1 ){
                tmpStatus = 400;
                err_code = ErrorHandler.NOT_FOUND_DATA.code + ": make_category_name ";
            }
        }catch(e){
        }

        res.status(tmpStatus);
        return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), {code:err_code} ));
    }

    //util method
    function _uploadFittingFilesInLocal( __opt ){
        var deferred = Q.defer();
        var readStream;

        fs.createReadStream(__opt.zip_src)
            .pipe(unzip.Parse())
            .on('entry', function(entry){
                switch( true ){
                    case ( entry.path == "package.bin" ):
                    case ( entry.path == "thumbnail.png" ):
                    case ( entry.path.toLowerCase().indexOf('package.json') ):
                    case ( /preview[0-3]?[0-9]?[0-9]\.jpg/.test(entry.path) ):
                        entry.pipe( fs.createWriteStream( __opt.unzip_path+'/'+entry.path ) );
                        __opt.upload_req_list.push(__opt.unzip_path +'/'+ entry.path);
                        __opt.upload_req_file.push(entry.path);
                        break;
                    default:
                        entry.autodrain();
                }
            })
            .on('close', function(){
                readStream = fs.createReadStream(__opt.unzip_path+'/' + "package.bin");
                readStream.on('data', function(data){
                    readStream.close();
                    if(data.toString('utf8', 0, 2).indexOf('FX') === -1){
                        for(var i in __opt.upload_req_list){
                            fs.unlinkSync(__opt.upload_req_list[i]);
                        }
                        fs.rmdirSync(__opt.unzip_path);
                        fs.unlinkSync(__opt.zip_src);
                        deferred.reject(err);
                    }else{
                        deferred.resolve(__opt);
                    }

                });
            })
            .on('error', function(){
                console.log('fs.createReadStream error: ' , arguments);
            });

        return deferred.promise;
    }
    //task method END
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


}

function _test3dList(req, res){
    return res.json({
        "info": {
            "start": null,
            "count": 7
        },
        "results": [
            {
                "id": 45,
                "cat_id": 0,
                "org_id": 5,
                "store_name": "test003",
                "code": "WomCloTop001000002",
                "name": "Top Cloth 001",
                "sex": "Woman",
                "make_category_db_id": 32,
                "make_category_name": "WomCloTop001",
                "make_category_route": "Top>Long Sleeve T-Shirt",
                "description": "td 샘플입니당",
                "status": "activated",
                "registerd_datetime": "2016-08-11T16:51:41.000Z",
                "updated_datetime": null,
                "td_fitting_files": {
                    "package": {
                        "hash": "14993da06db31209cb7c3f871c80d93bf473ba62d9e44226e6a71844a560cb7e",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/package.bin"
                    },
                    "thumbnail": {
                        "hash": "63b5ff53512330f163a31965c3b9a276724a9303c7b32e1b193cc8761f51e2fb",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/thumbnail.png"
                    },
                    "preview30": {
                        "hash": "7af10b6ec3e80ea49606a75721568009240d0889e59cb7aa055fc413c270f570",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview30.jpg"
                    },
                    "preview60": {
                        "hash": "c6c83e7bad5a0d1ece1c30a9008ba8ddef3d0e60087eecadb5f443ccd2823318",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview60.jpg"
                    },
                    "preview90": {
                        "hash": "bcbe2828e28c1bdc712ea24cfd5fde45d40fa4064615077916012c69ba88719d",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview90.jpg"
                    },
                    "preview120": {
                        "hash": "f21fdacdf08024f5167e5791267ef6f65b77348b3af3e044869431ff78977bf9",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview120.jpg"
                    },
                    "preview150": {
                        "hash": "f34950c745576f5b1241de3e6e7b1265540855940d05c43add638a481c014640",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview150.jpg"
                    },
                    "preview180": {
                        "hash": "ddd757ee7caf1ee4768df6b01a64e462eee3d7596a41f755ecb1d89203d94b74",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview180.jpg"
                    },
                    "preview210": {
                        "hash": "5cb3d1049e2892a970fcbc572709cf1b83281c59acb6bf28b49f0f7cd89b3dae",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview210.jpg"
                    },
                    "preview240": {
                        "hash": "a9e4ae4e3a06f4822271c2eea74d59aa488ebdfa12e22e58bcf4f32df1fec0f3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview240.jpg"
                    },
                    "preview270": {
                        "hash": "0c6a2aafa86b90a860ff964741c612e89fb19f2d68099b8f99cd67f927d1f495",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview270.jpg"
                    },
                    "preview300": {
                        "hash": "23243490192d0144f7be0956d96b37661f03fa5f53cabc97f02aa38e17db5cf3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview300.jpg"
                    },
                    "preview330": {
                        "hash": "a3379d5fee81b458ebf82e6aac506974661effa89278209fe443561fd4643e5b",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview330.jpg"
                    },
                    "preview0": {
                        "hash": "a1c0f47b7d2929b82d066bdbaee78c06e757fa6a71ca7782571aa13e9350778a",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000002/preview0.jpg"
                    }
                }
            },
            {
                "id": 47,
                "cat_id": 0,
                "org_id": 5,
                "store_name": "test003",
                "code": "WomCloTop002000001",
                "name": "Top Cloth 002",
                "sex": "Woman",
                "make_category_db_id": 33,
                "make_category_name": "WomCloTop002",
                "make_category_route": "Top>Turtleneck Shirt",
                "description": "td 샘플입니당2",
                "status": "activated",
                "registerd_datetime": "2016-08-15T21:24:33.000Z",
                "updated_datetime": null,
                "td_fitting_files": {
                    "package": {
                        "hash": "14993da06db31209cb7c3f871c80d93bf473ba62d9e44226e6a71844a560cb7e",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/package.bin"
                    },
                    "thumbnail": {
                        "hash": "63b5ff53512330f163a31965c3b9a276724a9303c7b32e1b193cc8761f51e2fb",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/thumbnail.png"
                    },
                    "preview30": {
                        "hash": "7af10b6ec3e80ea49606a75721568009240d0889e59cb7aa055fc413c270f570",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview30.jpg"
                    },
                    "preview60": {
                        "hash": "c6c83e7bad5a0d1ece1c30a9008ba8ddef3d0e60087eecadb5f443ccd2823318",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview60.jpg"
                    },
                    "preview90": {
                        "hash": "bcbe2828e28c1bdc712ea24cfd5fde45d40fa4064615077916012c69ba88719d",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview90.jpg"
                    },
                    "preview120": {
                        "hash": "f21fdacdf08024f5167e5791267ef6f65b77348b3af3e044869431ff78977bf9",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview120.jpg"
                    },
                    "preview150": {
                        "hash": "f34950c745576f5b1241de3e6e7b1265540855940d05c43add638a481c014640",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview150.jpg"
                    },
                    "preview180": {
                        "hash": "ddd757ee7caf1ee4768df6b01a64e462eee3d7596a41f755ecb1d89203d94b74",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview180.jpg"
                    },
                    "preview210": {
                        "hash": "5cb3d1049e2892a970fcbc572709cf1b83281c59acb6bf28b49f0f7cd89b3dae",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview210.jpg"
                    },
                    "preview240": {
                        "hash": "a9e4ae4e3a06f4822271c2eea74d59aa488ebdfa12e22e58bcf4f32df1fec0f3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview240.jpg"
                    },
                    "preview270": {
                        "hash": "0c6a2aafa86b90a860ff964741c612e89fb19f2d68099b8f99cd67f927d1f495",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview270.jpg"
                    },
                    "preview300": {
                        "hash": "23243490192d0144f7be0956d96b37661f03fa5f53cabc97f02aa38e17db5cf3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview300.jpg"
                    },
                    "preview330": {
                        "hash": "a3379d5fee81b458ebf82e6aac506974661effa89278209fe443561fd4643e5b",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview330.jpg"
                    },
                    "preview0": {
                        "hash": "a1c0f47b7d2929b82d066bdbaee78c06e757fa6a71ca7782571aa13e9350778a",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000001/preview0.jpg"
                    }
                }
            },
            {
                "id": 48,
                "cat_id": 0,
                "org_id": 5,
                "store_name": "test003",
                "code": "WomCloTop002000002",
                "name": "Top Cloth 003",
                "sex": "Woman",
                "make_category_db_id": 33,
                "make_category_name": "WomCloTop002",
                "make_category_route": "Top>Turtleneck Shirt",
                "description": "td 샘플입니당3",
                "status": "activated",
                "registerd_datetime": "2016-08-15T21:25:43.000Z",
                "updated_datetime": null,
                "td_fitting_files": {
                    "package": {
                        "hash": "14993da06db31209cb7c3f871c80d93bf473ba62d9e44226e6a71844a560cb7e",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/package.bin"
                    },
                    "thumbnail": {
                        "hash": "63b5ff53512330f163a31965c3b9a276724a9303c7b32e1b193cc8761f51e2fb",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/thumbnail.png"
                    },
                    "preview30": {
                        "hash": "7af10b6ec3e80ea49606a75721568009240d0889e59cb7aa055fc413c270f570",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview30.jpg"
                    },
                    "preview60": {
                        "hash": "c6c83e7bad5a0d1ece1c30a9008ba8ddef3d0e60087eecadb5f443ccd2823318",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview60.jpg"
                    },
                    "preview90": {
                        "hash": "bcbe2828e28c1bdc712ea24cfd5fde45d40fa4064615077916012c69ba88719d",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview90.jpg"
                    },
                    "preview120": {
                        "hash": "f21fdacdf08024f5167e5791267ef6f65b77348b3af3e044869431ff78977bf9",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview120.jpg"
                    },
                    "preview150": {
                        "hash": "f34950c745576f5b1241de3e6e7b1265540855940d05c43add638a481c014640",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview150.jpg"
                    },
                    "preview180": {
                        "hash": "ddd757ee7caf1ee4768df6b01a64e462eee3d7596a41f755ecb1d89203d94b74",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview180.jpg"
                    },
                    "preview210": {
                        "hash": "5cb3d1049e2892a970fcbc572709cf1b83281c59acb6bf28b49f0f7cd89b3dae",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview210.jpg"
                    },
                    "preview240": {
                        "hash": "a9e4ae4e3a06f4822271c2eea74d59aa488ebdfa12e22e58bcf4f32df1fec0f3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview240.jpg"
                    },
                    "preview270": {
                        "hash": "0c6a2aafa86b90a860ff964741c612e89fb19f2d68099b8f99cd67f927d1f495",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview270.jpg"
                    },
                    "preview300": {
                        "hash": "23243490192d0144f7be0956d96b37661f03fa5f53cabc97f02aa38e17db5cf3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview300.jpg"
                    },
                    "preview330": {
                        "hash": "a3379d5fee81b458ebf82e6aac506974661effa89278209fe443561fd4643e5b",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview330.jpg"
                    },
                    "preview0": {
                        "hash": "a1c0f47b7d2929b82d066bdbaee78c06e757fa6a71ca7782571aa13e9350778a",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop002000002/preview0.jpg"
                    }
                }
            },
            {
                "id": 49,
                "cat_id": 0,
                "org_id": 5,
                "store_name": "test003",
                "code": "WomCloTop001000003",
                "name": "Top Cloth 001",
                "sex": "Woman",
                "make_category_db_id": 32,
                "make_category_name": "WomCloTop001",
                "make_category_route": "Top>Long Sleeve T-Shirt",
                "description": "td 샘플입니당",
                "status": "activated",
                "registerd_datetime": "2016-08-15T23:51:40.000Z",
                "updated_datetime": null,
                "td_fitting_files": null
            },
            {
                "id": 50,
                "cat_id": 0,
                "org_id": 5,
                "store_name": "test003",
                "code": "WomCloTop001000004",
                "name": "Top Cloth 001",
                "sex": "Woman",
                "make_category_db_id": 32,
                "make_category_name": "WomCloTop001",
                "make_category_route": "Top>Long Sleeve T-Shirt",
                "description": "td 샘플입니당",
                "status": "activated",
                "registerd_datetime": "2016-08-15T23:52:38.000Z",
                "updated_datetime": null,
                "td_fitting_files": {
                    "package": {
                        "hash": "14993da06db31209cb7c3f871c80d93bf473ba62d9e44226e6a71844a560cb7e",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/package.bin"
                    },
                    "thumbnail": {
                        "hash": "63b5ff53512330f163a31965c3b9a276724a9303c7b32e1b193cc8761f51e2fb",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/thumbnail.png"
                    },
                    "preview30": {
                        "hash": "7af10b6ec3e80ea49606a75721568009240d0889e59cb7aa055fc413c270f570",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview30.jpg"
                    },
                    "preview60": {
                        "hash": "c6c83e7bad5a0d1ece1c30a9008ba8ddef3d0e60087eecadb5f443ccd2823318",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview60.jpg"
                    },
                    "preview90": {
                        "hash": "bcbe2828e28c1bdc712ea24cfd5fde45d40fa4064615077916012c69ba88719d",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview90.jpg"
                    },
                    "preview120": {
                        "hash": "f21fdacdf08024f5167e5791267ef6f65b77348b3af3e044869431ff78977bf9",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview120.jpg"
                    },
                    "preview150": {
                        "hash": "f34950c745576f5b1241de3e6e7b1265540855940d05c43add638a481c014640",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview150.jpg"
                    },
                    "preview180": {
                        "hash": "ddd757ee7caf1ee4768df6b01a64e462eee3d7596a41f755ecb1d89203d94b74",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview180.jpg"
                    },
                    "preview210": {
                        "hash": "5cb3d1049e2892a970fcbc572709cf1b83281c59acb6bf28b49f0f7cd89b3dae",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview210.jpg"
                    },
                    "preview240": {
                        "hash": "a9e4ae4e3a06f4822271c2eea74d59aa488ebdfa12e22e58bcf4f32df1fec0f3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview240.jpg"
                    },
                    "preview270": {
                        "hash": "0c6a2aafa86b90a860ff964741c612e89fb19f2d68099b8f99cd67f927d1f495",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview270.jpg"
                    },
                    "preview300": {
                        "hash": "23243490192d0144f7be0956d96b37661f03fa5f53cabc97f02aa38e17db5cf3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview300.jpg"
                    },
                    "preview330": {
                        "hash": "a3379d5fee81b458ebf82e6aac506974661effa89278209fe443561fd4643e5b",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview330.jpg"
                    },
                    "preview0": {
                        "hash": "a1c0f47b7d2929b82d066bdbaee78c06e757fa6a71ca7782571aa13e9350778a",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000004/preview0.jpg"
                    }
                }
            },
            {
                "id": 51,
                "cat_id": 0,
                "org_id": 5,
                "store_name": "test003",
                "code": "WomCloTop001000005",
                "name": "Top Cloth 001",
                "sex": "Woman",
                "make_category_db_id": 32,
                "make_category_name": "WomCloTop001",
                "make_category_route": "Top>Long Sleeve T-Shirt",
                "description": "td 샘플입니당",
                "status": "activated",
                "registerd_datetime": "2016-08-16T00:19:15.000Z",
                "updated_datetime": null,
                "td_fitting_files": {
                    "package": {
                        "hash": "14993da06db31209cb7c3f871c80d93bf473ba62d9e44226e6a71844a560cb7e",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/package.bin"
                    },
                    "thumbnail": {
                        "hash": "63b5ff53512330f163a31965c3b9a276724a9303c7b32e1b193cc8761f51e2fb",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/thumbnail.png"
                    },
                    "preview30": {
                        "hash": "7af10b6ec3e80ea49606a75721568009240d0889e59cb7aa055fc413c270f570",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview30.jpg"
                    },
                    "preview60": {
                        "hash": "c6c83e7bad5a0d1ece1c30a9008ba8ddef3d0e60087eecadb5f443ccd2823318",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview60.jpg"
                    },
                    "preview90": {
                        "hash": "bcbe2828e28c1bdc712ea24cfd5fde45d40fa4064615077916012c69ba88719d",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview90.jpg"
                    },
                    "preview120": {
                        "hash": "f21fdacdf08024f5167e5791267ef6f65b77348b3af3e044869431ff78977bf9",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview120.jpg"
                    },
                    "preview150": {
                        "hash": "f34950c745576f5b1241de3e6e7b1265540855940d05c43add638a481c014640",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview150.jpg"
                    },
                    "preview180": {
                        "hash": "ddd757ee7caf1ee4768df6b01a64e462eee3d7596a41f755ecb1d89203d94b74",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview180.jpg"
                    },
                    "preview210": {
                        "hash": "5cb3d1049e2892a970fcbc572709cf1b83281c59acb6bf28b49f0f7cd89b3dae",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview210.jpg"
                    },
                    "preview240": {
                        "hash": "a9e4ae4e3a06f4822271c2eea74d59aa488ebdfa12e22e58bcf4f32df1fec0f3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview240.jpg"
                    },
                    "preview270": {
                        "hash": "0c6a2aafa86b90a860ff964741c612e89fb19f2d68099b8f99cd67f927d1f495",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview270.jpg"
                    },
                    "preview300": {
                        "hash": "23243490192d0144f7be0956d96b37661f03fa5f53cabc97f02aa38e17db5cf3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview300.jpg"
                    },
                    "preview330": {
                        "hash": "a3379d5fee81b458ebf82e6aac506974661effa89278209fe443561fd4643e5b",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview330.jpg"
                    },
                    "preview0": {
                        "hash": "a1c0f47b7d2929b82d066bdbaee78c06e757fa6a71ca7782571aa13e9350778a",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000005/preview0.jpg"
                    }
                }
            },
            {
                "id": 52,
                "cat_id": 0,
                "org_id": 5,
                "store_name": "test003",
                "code": "WomCloTop001000006",
                "name": "Top Cloth 001",
                "sex": "Woman",
                "make_category_db_id": 32,
                "make_category_name": "WomCloTop001",
                "make_category_route": "Top>Long Sleeve T-Shirt",
                "description": "td 샘플입니당",
                "status": "activated",
                "registerd_datetime": "2016-08-16T00:21:58.000Z",
                "updated_datetime": null,
                "td_fitting_files": {
                    "package": {
                        "hash": "14993da06db31209cb7c3f871c80d93bf473ba62d9e44226e6a71844a560cb7e",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/package.bin"
                    },
                    "thumbnail": {
                        "hash": "63b5ff53512330f163a31965c3b9a276724a9303c7b32e1b193cc8761f51e2fb",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/thumbnail.png"
                    },
                    "preview30": {
                        "hash": "7af10b6ec3e80ea49606a75721568009240d0889e59cb7aa055fc413c270f570",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview30.jpg"
                    },
                    "preview60": {
                        "hash": "c6c83e7bad5a0d1ece1c30a9008ba8ddef3d0e60087eecadb5f443ccd2823318",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview60.jpg"
                    },
                    "preview90": {
                        "hash": "bcbe2828e28c1bdc712ea24cfd5fde45d40fa4064615077916012c69ba88719d",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview90.jpg"
                    },
                    "preview120": {
                        "hash": "f21fdacdf08024f5167e5791267ef6f65b77348b3af3e044869431ff78977bf9",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview120.jpg"
                    },
                    "preview150": {
                        "hash": "f34950c745576f5b1241de3e6e7b1265540855940d05c43add638a481c014640",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview150.jpg"
                    },
                    "preview180": {
                        "hash": "ddd757ee7caf1ee4768df6b01a64e462eee3d7596a41f755ecb1d89203d94b74",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview180.jpg"
                    },
                    "preview210": {
                        "hash": "5cb3d1049e2892a970fcbc572709cf1b83281c59acb6bf28b49f0f7cd89b3dae",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview210.jpg"
                    },
                    "preview240": {
                        "hash": "a9e4ae4e3a06f4822271c2eea74d59aa488ebdfa12e22e58bcf4f32df1fec0f3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview240.jpg"
                    },
                    "preview270": {
                        "hash": "0c6a2aafa86b90a860ff964741c612e89fb19f2d68099b8f99cd67f927d1f495",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview270.jpg"
                    },
                    "preview300": {
                        "hash": "23243490192d0144f7be0956d96b37661f03fa5f53cabc97f02aa38e17db5cf3",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview300.jpg"
                    },
                    "preview330": {
                        "hash": "a3379d5fee81b458ebf82e6aac506974661effa89278209fe443561fd4643e5b",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview330.jpg"
                    },
                    "preview0": {
                        "hash": "a1c0f47b7d2929b82d066bdbaee78c06e757fa6a71ca7782571aa13e9350778a",
                        "url": "http://dev-file.fitnshop.com/DAT/WomCloTop001000006/preview0.jpg"
                    }
                }
            }
        ]
    });
}


//util fnc
function _taskSideEffect(func){
  return function(){
    if(func) func(arguments);
    return arguments;
  };
}

function _taskMapData(mapData){
  return function(){
    if(mapData){
      if(typeof mapData==='function'){
        return mapData(arguments);
      }else{
        return mapData
      }
    }else{
      return arguments;
    }
  };
}


function _invokePromise(fnc, valFnc){
  return function(results){
    return Q.fapply(fnc, valFnc(results) );
  }
}


function _chkAvailability(req, res) {
  var debug = require("./libs/debug.js"),
  item_ids = req.param("item_ids") || "",
  rtn_val = {},
  qry = "";

  chkData.chkParam(req, res, [item_ids], function() {
    qry += "SELECT commonid AS item_id ";
    qry += "  FROM sv_item_detail ";
    qry += " WHERE item_status<>'activated' ";
    qry += "   AND commonid IN (" + SQLService.escape(item_ids) + ")";

    DB.query(qry, [item_ids], function(err, results) {
      if (err) {
        status = 500;
        return res.send(status, debug.wrap(req, res, status, err));
      } else {
        //sails.log(_.pluck(results, 'ITEM_NO').join(","));
        rtn_val.not_available_item_ids = results;
        status = 200;
        return res.send(status, debug.wrap(req, res, status, rtn_val));
      }
    });
  });
}

