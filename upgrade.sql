#update store join help type
// 입점시 가이드 부분을 중국어로 노출
UPDATE `sv_help_type` SET `type_name`='入驻流程' WHERE (`type_id`='99');
UPDATE `sv_help_type` SET `type_name`='结算' WHERE (`type_id`='95');
UPDATE `sv_help_type` SET `type_name`='订单及售后' WHERE (`type_id`='94');
UPDATE `sv_help_type` SET `type_name`='专题' WHERE (`type_id`='93');
UPDATE `sv_help_type` SET `type_name`='店铺管理' WHERE (`type_id`='92');
UPDATE `sv_help_type` SET `type_name`='添加商品' WHERE (`type_id`='91');


#clear help type
TRUNCATE `sv_help`



#2016 09 19 setting
// 몰인몰 기능 사용여부
INSERT INTO `sv_setting` (`name`, `value`) VALUES ('site_scm', '1');
