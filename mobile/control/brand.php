<?php
/**
 * 商品分类
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');
class brandControl extends mobileHomeControl{


	public function __construct() {
		parent::__construct();
	}

	public function recommend_listOp()
	{
		        //获得品牌列表
        $model = Model();
        $brand_r_list = $model->table('brand')->field('brand_id,brand_name,brand_pic')->where(array('brand_apply'=>'1','brand_recommend'=>'1'))->order('brand_sort asc')->select();
		foreach($brand_r_list as $key=>$val)
		{
			$val['brand_pic'] = brandImage($val['brand_pic']);
			$brand_r_list[$key] = $val;
		}
        output_data(array('brand_list'=>$brand_r_list));
	}
}