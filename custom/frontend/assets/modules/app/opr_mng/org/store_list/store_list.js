(function($, angular, _, APP){
  'use strict';
  var tableComponentDirName = 'orgStoreList';

  if( $(document.body).attr('data-page-id')=='org_store_list' ){
    bootStrapList();
  }

  function bootStrapList(){
    console.log('[[bootStrapList:]]');

    angular.module('App')
      .directive( tableComponentDirName, tableListDirc );

    angular.module('App')
      .directive( 'tableSearch', tableSearchDirc );

    angular.module('App')
      .directive( 'tableOption', tableOptionDirc );

    angular.module('App')
      .directive( 'tableBody', tableBodyDirc );

    angular.module('App')
      .directive( 'tableBtn', btnDirc );

    angular.module('App')
      .directive( 'pagination', paginationDirc );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( parseInt(APP.info.user.auth.minorOrganization,10)>=1 ){
            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }
        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }

  function tableListDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.org.list.ctrl.storeAdminListDircCtrl,
      controllerAs: 'tableListCtrl',
      bindToController: APP.modules.org.list.ctrl.storeAdminListDircCtrl.bindToController,
      link: APP.modules.org.list.ctrl.storeAdminListDircCtrl.link(),
      templateUrl: 'table_component_basic.html'
    };
  }
  tableListDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];


  function tableSearchDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.org.list.ctrl.storeAdminListSearchCtrl,
      controllerAs: 'tableSearchCtrl',
      bindToController: APP.modules.org.list.ctrl.storeAdminListSearchCtrl.bindToController,
      link: APP.modules.org.list.ctrl.storeAdminListSearchCtrl.link(PubSub),
      templateUrl: 'table_search_org_store_list.html'
    };
  }
  tableSearchDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];


  function tableOptionDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableOptionsBasicCtrl,
      controllerAs: 'tableOptionCtrl',
      bindToController: APP.modules.ctrl.tableOptionsBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link:  APP.modules.ctrl.tableOptionsBasicCtrl.link(PubSub),
      templateUrl: 'table_option_basic.html'
    };
  }
  tableOptionDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function tableBodyDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.org.list.ctrl.storeAdminListBodyCtrl,
      controllerAs: 'tableBodyCtrl',
      bindToController: APP.modules.org.list.ctrl.storeAdminListBodyCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.org.list.ctrl.storeAdminListBodyCtrl.link(PubSub),
      templateUrl: 'table_body_org_store_list.html'
    };
  }
  tableBodyDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];


  function btnDirc(XHR, PubSub, URL_INFO) {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableBtnBasicCtrl,
      controllerAs: 'tableBtnBasicCtrl',
      bindToController: APP.modules.ctrl.tableBtnBasicCtrl.bindToController,
      require: ('^' + tableComponentDirName),
      link: APP.modules.ctrl.tableBtnBasicCtrl.link(PubSub),
      templateUrl: 'table_btn_basic.html'
    }
  }
  btnDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function paginationDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tablePaginationBasicCtrl,
      controllerAs: 'tablePaginationBasicCtrl',
      bindToController:  APP.modules.ctrl.tablePaginationBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tablePaginationBasicCtrl.link(PubSub),
      templateUrl: 'table_pagination_basic.html'
    };
  }
  paginationDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

})(window.jQuery, window.angular, window._, window.APP);

