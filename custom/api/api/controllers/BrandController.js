module.exports = {
  list: list
};

function list(req, res) {
  var debug = require("./libs/debug.js");

  var start = parseInt(req.param("start"), 10) || 0;
  var count = parseInt(req.param("count"), 10) || 10;
  var brand_status = req.param('status') || 'all';
  var order = req.param('order') || 'date';
  var show = req.param('show') || '';

  /* 미대응 */
  var query_opt = req.param('query_opt') || 'name';
  var search_query = req.param('query') || '';
  var account_uuid = req.param('account_uuid') || '';


  var status = 501;
  var error = {};
  var argQuery;
  var strQuery;
  var query = {};
  var results_wrap = {};

  query.select = "SELECT a.brand_id AS id, ";
  query.select +=       "a.brand_name AS name, ";
  query.select +=       "'' AS reg_date, ";
  query.select +=       "'' AS uuid, ";
  query.select +=       "COUNT(b.goods_commonid) AS item_count, ";
  query.select +=       "0 AS account_id, ";
  query.select +=       "CASE ";
  query.select +=         "WHEN a.brand_apply = 1 THEN 'activated' ";
  query.select +=         "ELSE 'disabled' ";
  query.select +=       "END AS status ";
  query.from =   "FROM sv_brand AS a ";
  query.join =   "JOIN sv_goods_common AS b ON a.brand_id = b.brand_id ";
  query.where =  "WHERE b.goods_state = 1 ";
  query.where +=   "AND b.goods_verify = 1 ";
  query.where +=   "AND b.goods_kids = 0 ";
  if (brand_status !== "all") {
    query.where += (brand_status == "activated" ? "AND a.brand_apply = 1 " : "AND a.brand_apply = 0 " );
  }
  query.groupby ="GROUP BY b.brand_id ";
  query.having = "HAVING COUNT(b.goods_commonid) > 0 ";
  query.orderby ="ORDER BY a.brand_sort ASC, ";
  query.orderby += (order === "title" ? "IF(ASCII(SUBSTRING(a.brand_name, 1)) < 128, 9, 1) ASC, a.brand_name ASC " : "a.brand_id DESC " );
  query.limit =  "LIMIT ?, ? ;";
  strQuery = query.select + query.from + query.join + query.where + query.groupby + query.having + query.orderby + query.limit;
  argQuery = [start, count];
  //console.log(strQuery)
  DB.query(strQuery, argQuery, function (err, results) {
    sails.log.debug(results);
    results_wrap.results = results;
    if (err) {
      status = 500;
      return res.send(status, debug.wrap(req, res, status, err));
    }
    if (results.length === 0) {
      status = ErrorHandler.NOT_FOUND_DATA.status;
      error.code = ErrorHandler.NOT_FOUND_DATA.code;
      error.index = 0;
      return res.send(status, debug.wrap(req, res, status, error));
    }
    query.select = "SELECT COUNT(DISTINCT a.brand_id) AS total ";
    query.from =   "FROM sv_brand AS a ";
    query.join =   "LEFT JOIN sv_goods_common AS b ON a.brand_id = b.brand_id ";
    query.where =  "WHERE b.goods_state = 1 AND b.goods_verify = 1 AND b.goods_kids = 0 ";
    if (brand_status !== "all") {
      query.where += (brand_status == "activated" ? "AND a.brand_apply = 1 " : "AND a.brand_apply = 0 " );
    }
    strQuery = query.select + query.from + query.join + query.where;
    argQuery = [];
    //console.log(strQuery)
    DB.query(strQuery, argQuery, function (err, results) {
      if (err) {
        status = 500;
        return res.send(status, debug.wrap(req, res, status, err));
      }
      if (results.length === 0) {
        status = ErrorHandler.NOT_FOUND_DATA.status;
        error.code = ErrorHandler.NOT_FOUND_DATA.code;
        error.index = 0;
        return res.send(status, debug.wrap(req, res, status, error));
      }
      /* success */
      status = 200;
      results_wrap.info = {
        start: start,
        count: count,
        total: results[0].total
      };
      return res.send(status, debug.wrap(req, res, status, results_wrap));
    });
  });

}
