<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
  <a href="<?php echo urlShop('store_plate', 'plate_add');?>" class="ncsc-btn ncsc-btn-green" title="관련템플릿추가">관련템플릿추가</a>
</div>
<div class="alert mt15 mb5"><strong>도움말：</strong>
  <ul>
    <li>템플릿에 미리 설정해놓은 내용을 상품설명의 앞뒤에 첨가할 수 있습니다. <br>판매자가 상품 설명을 대량으로 추가 혹은 수정할 때에 편리하게 이용될 수 있습니다.</li>
  </ul>
</div>
<form method="get">
<input type="hidden" name="act" value="store_plate">
<table class="search-form">
    <tr>
      <td>&nbsp;</td>
      
      <th>템플릿<br>위치</th>
      <td class="w80">
        <select name="p_position">
          <option>선택</option>
          <?php foreach ($output['position'] as $key => $val) {?>
          <option value="<?php echo $key;?>" <?php if (is_numeric($_GET['p_position']) && $key==$_GET['p_position']) {?>selected="selected"<?php }?>><?php echo $val;?></option>
          <?php }?>
        </select>
      </td><th>템플릿명</th>
      <td class="w160"><input type="text" class="text w150" name="p_name" value="<?php echo $_GET['p_name']; ?>"/></td>
      <td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="검색" /></label></td>
    </tr>
</table>
</form>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w30"></th>
      <th class="tl">템플릿명</th>
      <th class="w200">템플릿위치</th>
      <th class="w110">편집</th>
    </tr>
    <?php if (!empty($output['plate_list'])) { ?>
    <tr>
      <td class="tc"><input type="checkbox" id="all" class="checkall"/></td>
      <td colspan="10"><label for="all" >전체선택</label>
        <a href="javascript:void(0);" nc_type="batchbutton" uri="<?php echo urlShop('store_plate', 'drop_plate');?>" name="p_id" confirm="정말 삭제 하시겠습니까?" class="ncsc-btn-mini"><i class="icon-trash"></i>삭제</a>
      </td>
    </tr>
    <?php } ?>
  </thead>
  <tbody>
    <?php if (!empty($output['plate_list'])) { ?>
    <?php foreach($output['plate_list'] as $val) { ?>
    <tr class="bd-line">
      <td class="tc"><input type="checkbox" class="checkitem tc" value="<?php echo $val['plate_id']; ?>"/></td>
      <td class="tl"><?php echo $val['plate_name']; ?></td>
      <td><?php echo $output['position'][$val['plate_position']];?></td>
      <td class="nscs-table-handle">
        <span><a href="<?php echo urlShop('store_plate', 'plate_edit', array('p_id' => $val['plate_id']));?>" class="btn-blue"><i class="icon-edit"></i><p>편집</p></a></span>
        <span><a href="javascript:void(0)" onclick="ajax_get_confirm('정말 삭제 하시겠습니까?', '<?php echo urlShop('store_plate', 'drop_plate', array('p_id' => $val['plate_id']));?>');" class="btn-red"><i class="icon-trash"></i><p>삭제</p></a></span>
      </td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php } ?>
  </tbody>
  <tfoot>
    <?php if (!empty($output['plate_list'])) { ?>
    <tr>
      <th class="tc"><input type="checkbox" id="all" class="checkall"/></th>
      <th colspan="10"><label for="all" >전체선택</label>
        <a href="javascript:void(0);" nc_type="batchbutton" uri="<?php echo urlShop('store_plate', 'drop_plate');?>" name="p_id" confirm="정말 삭제 하시겠습니까?" class="ncsc-btn-mini"><i class="icon-trash"></i>삭제</a>
       </th>
    </tr>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
    <?php } ?>
  </tfoot>
</table>
