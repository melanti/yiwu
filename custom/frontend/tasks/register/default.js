module.exports = function (grunt) {

  if(process.env['NODE_ENV']==='local'){
        // grunt.registerTask('default', ['compileAssets', 'linkAssets']);
      grunt.registerTask('default', [
          'compileAssets',
          'concat',
          'uglify',
          'cssmin',
          'sails-linker:prodJsBase',
          'sails-linker:prodJs',
          'sails-linker:prodJsAll',
          'sails-linker:prodStyles',
          'sails-linker:devTpl',
          'sails-linker:prodJsJade',
          'sails-linker:prodStylesJade',
          'sails-linker:devTplJade'
      ]);
  }else{
    // grunt.registerTask('default', ['mocha-test', 'compileAssets', 'linkAssets',  'watch']);
    grunt.registerTask('default', [
     'compileAssets',
     'concat',
     'uglify',
     'cssmin',
     'sails-linker:prodJsBase',
     'sails-linker:prodJs',
     'sails-linker:prodJsAll',
     'sails-linker:prodStyles',
     'sails-linker:devTpl',
     'sails-linker:prodJsJade',
     'sails-linker:prodStylesJade',
     'sails-linker:devTplJade'
     ]);
  }

};
