<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="tabmenu"><?php include template('layout/submenu');?></div>
<table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
    <thead>
        <tr>
            <th colspan="20">기본정보</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="w150">회사명：</th>
            <td colspan="20"><?php echo $output['joinin_detail']['company_name'];?></td>
    </tr>
    <tr>
        <th>회사소재지：</th>
        <td><?php echo $output['joinin_detail']['company_address'];?></td>
        <th>상세주소：</th>
        <td colspan="20"><?php echo $output['joinin_detail']['company_address_detail'];?></td>
    </tr>
    <tr>
        <th>회사주소：</th>
        <td><?php echo $output['joinin_detail']['company_phone'];?></td>
        <th>직원수：</th>
        <td><?php echo $output['joinin_detail']['company_employee_count'];?>&nbsp;명</td>
        <th>자본금：</th>
        <td><?php echo $output['joinin_detail']['company_registered_capital'];?>&nbsp;만원 </td>
    </tr>
    <tr>
        <th>담당자：</th>
        <td><?php echo $output['joinin_detail']['contacts_name'];?></td>
        <th>담당자번호：</th>
        <td><?php echo $output['joinin_detail']['contacts_phone'];?></td>
        <th>E-MAIL:：</th>
        <td><?php echo $output['joinin_detail']['contacts_email'];?></td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
    <thead>
        <tr>
            <th colspan="20">사업자정보</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="w150">사업자 등록번호：</th>
            <td><?php echo $output['joinin_detail']['business_licence_number'];?></td></tr><tr>
    </tr>
    <tr>
        <th>경영범위：</th>
        <td colspan="20"><?php echo $output['joinin_detail']['business_sphere'];?></td>
    </tr>
    <tr>
        <th>사업자 등록증：</th>
        <td colspan="20"><a cnbiztype="nyroModal"  href="<?php echo getStoreJoininImageUrl($output['joinin_detail']['business_licence_number_electronic']);?>"> <img src="<?php echo getStoreJoininImageUrl($output['joinin_detail']['business_licence_number_electronic']);?>" alt="" /> </a></td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
    <thead>
        <tr>
            <th colspan="20">정산정보：</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="w150">이름：</th>
            <td><?php echo $output['joinin_detail']['settlement_bank_account_name'];?></td>
    </tr>
    <tr>
        <th>계좌번호：</th>
        <td><?php echo $output['joinin_detail']['settlement_bank_account_number'];?></td>
    </tr>
    <tr>
        <th>은행명：</th>
        <td><?php echo $output['bank_name_code'][$output['joinin_detail']['settlement_bank_name']];?></td>
    </tr>
    </tbody>

</table>
<table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
    <thead>
        <tr>
            <th colspan="20">운영정보</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="w150">아이디：</th>
            <td><?php echo $output['joinin_detail']['seller_name'];?></td>
    </tr>
    <tr>
        <th class="w150">점포명：</th>
        <td><?php echo $output['joinin_detail']['store_name'];?></td>
    </tr>
    <tr>
        <th class="w150">경영항목：</th>
        <td><?php echo $output['store_class_name'];?></td>
    </tr>
    <tr>
        <th>카테고리：</th>
        <td colspan="2"><table border="0" cellpadding="0" cellspacing="0" id="table_category" class="type">
                <thead>
                    <tr>
                        <th>카테고리1</th>
                        <th>카테고리2</th>
                        <th>카테고리3</th>
                        <th>비율</th>
                    </tr>
                </thead>
                <?php if(!empty($output['store_bind_class_list']) && is_array($output['store_bind_class_list'])) {?>
                <?php foreach($output['store_bind_class_list'] as $key=>$value) {?>
                    <tr>
                        <td><?php echo $value['class_1_name'];?></td>
                        <td><?php echo $value['class_2_name'];?></td>
                        <td><?php echo $value['class_3_name'];?></td>
                        <td><?php echo $value['commis_rate'];?>%</td>
                    </tr>
                <?php } ?>
                <?php } ?>
                </tbody>
        </table></td>
    </tr>
    <tr>
        <th>부가설명：</th>
        <td colspan="2"><?php echo $output['joinin_detail']['joinin_message'];?></td>
        </tr>
    </tbody>
</table>

