
(function($, angular, _, APP){

  /*
  angular.module('App')
    .directive( 'pagination', ['XHR', 'PubSub', 'URL_INFO', paginationDirective] );

  APP.modules.directive.paginationDirective = paginationDirective;
  function paginationDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', paginationCtrl],
      controllerAs: 'paginationCtrl',
      bindToController: {
        getTotal:'&',
        getCount:'&',
        getStart:'&',
        updatePage:'&',
        countRange:'&'
      },
      require: '^tableComponent',
      link: function ($scope, elem, attrs, ctrl) {
        console.log('paginationComponent ctrl: ', ctrl);
        PubSub.trigger('tableComponent:bootstrap',{paginationCtrl:true});
        $scope.$watch( 'paginationCtrl.getStart()', $scope.paginationCtrl.watchCurPage);
        $scope.$watch( 'paginationCtrl.getCount()', $scope.paginationCtrl.watchCurPage);
        $scope.$watch( 'paginationCtrl.getTotal()', $scope.paginationCtrl.watchCurPage);
      },
      templateUrl: 'tablePagination.html'
    };
  }
  */


  APP.modules.ctrl.tablePaginationBasicCtrl = tablePaginationBasicCtrl;

  //paginationCtrl
  function tablePaginationBasicCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO){
    var ctrl = this;
    ctrl.prevPage=0;
    ctrl.nextPage=0;
    ctrl.lastPage=0;
    ctrl.pageListData=[];

    ctrl.curPage=0;
    ctrl.getTotal;
    ctrl.getCount;
    ctrl.getStart;
    ctrl.updatePage;
    ctrl.countRange;
    ctrl.clickPage = _clickPage;
    ctrl.watchCurPage = _watchCurPage;

    _init();

    function _init(){
      $scope.$on('$destroy', function () {
      });
      _updatePagination();
      //console.log('paginationCtrl _init');
    }

    function _watchCurPage(__newVal, __oldVal){
      //console.log('_watchCurPage: ',__newVal, __oldVal);

      if(__newVal===__oldVal){
      }else{
        _updatePagination();
      }
    }

    function _updatePagination(){
      var tmpList = [],
        i=0,
        ii,
        st = ctrl.getStart()/ctrl.getCount(),
        iTotal = Math.ceil((st+1)/ctrl.countRange())*ctrl.countRange();
      i = iTotal - ctrl.countRange()+1;
      ctrl.lastPage = (Math.ceil(ctrl.getTotal()/ctrl.getCount())-1) ;

      ctrl.curPage = st;

      //if(startPage<0) startPage = 0;
      if(ctrl.lastPage<0) ctrl.lastPage = 0;
      //if(startPage>lastPage) startPage = lastPage;


      if(i<0) i = 1;
      if(iTotal>ctrl.lastPage+1) iTotal = ctrl.lastPage+1;
      if(iTotal<1) iTotal = 1;

      ctrl.prevPage=i-2;
      ctrl.nextPage=iTotal;

      if(ctrl.prevPage<0) ctrl.prevPage = 0;
      if(ctrl.nextPage>ctrl.lastPage) ctrl.nextPage = ctrl.lastPage;

      for(ii=i; ii<=iTotal; ++ii){
        //console.log('ii: ', ii);
        //console.log('ctrl.countRange(): ', ((ii-1)*ctrl.getCount()) );

        tmpList.push({
          page:(ii),
          count:(ii-1)*ctrl.getCount()
        });
      }

      ctrl.prevPage = ctrl.prevPage*ctrl.getCount() ;
      ctrl.nextPage = ctrl.nextPage*ctrl.getCount() ;

      /*
      console.log('getTotal: ',ctrl.getTotal());
      console.log('getCount: ',ctrl.getCount());
      console.log('st: ', st);
      console.log('countRange: ',ctrl.countRange());
      console.log('prevPage: ',ctrl.prevPage);
      console.log('nextPage: ',ctrl.nextPage);
      console.log('lastPage: ',ctrl.lastPage);
      console.log('lastPage1: ',(Math.ceil(ctrl.getTotal()/ctrl.getCount())-1));
      //console.log('tmpList: ',tmpList);*/

      ctrl.pageListData = tmpList;
    }

    function _clickPage(__start){
      //console.log('__start: ' , __start);

      ctrl.updatePage({options:{start:__start}});
    }

  }

  tablePaginationBasicCtrl.beforeChangePage = function(){
    $('.pagination').css('opacity', '0'); //
  }

  tablePaginationBasicCtrl.afterChangePage = function(){
    $('.pagination').css('opacity', '1'); //
  }

  tablePaginationBasicCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO'];
  tablePaginationBasicCtrl.bindToController = {
    getTotal:'&',
    getCount:'&',
    getStart:'&',
    updatePage:'&',
    countRange:'&'
  };
  tablePaginationBasicCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{paginationCtrl:true});
      scope.$watch( 'tablePaginationBasicCtrl.getStart()', scope.tablePaginationBasicCtrl.watchCurPage);
      scope.$watch( 'tablePaginationBasicCtrl.getCount()', scope.tablePaginationBasicCtrl.watchCurPage);
      scope.$watch( 'tablePaginationBasicCtrl.getTotal()', scope.tablePaginationBasicCtrl.watchCurPage);

      PubSub.subscribe('table:change',function(){
        console.log( 'subscribe table:change' );

        $('.pagination').css('margin-left' , ( parseInt(($('.pagination_wrapper').width()-$('.pagination').width())/2,10) )+'px');
        tablePaginationBasicCtrl.afterChangePage();
      });

      console.log('tablePaginationBasicCtrl.link::');

    }
  }


})(window.jQuery, window.angular, window._, window.APP);
