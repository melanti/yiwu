<?php
/**
 * 我的代金券
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class member_fundControl extends mobileMemberControl {

	public function __construct() {
		parent::__construct();
	}

    /**
     * 地址列表
     */
    public function predepositlogOp() {
        $model_pd = Model('predeposit');
        $condition = array();
        $condition['lg_member_id'] = $this->member_info['member_id'];
        $list = $model_pd->getPdLogList($condition,20,'*','lg_id desc');
        $page_count = $model_pd->gettotalpage($condition);
        
        output_data(array('list' => $list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function pdrechargelistOp() {
        $condition = array();
        $condition['pdr_member_id'] = $this->member_info['member_id'];

        $model_pd = Model('predeposit');
        $list = $model_pd->getPdRechargeList($condition,20,'*','pdr_id desc');
        $page_count = $model_pd->gettotalpage();

        output_data(array('list' => $list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function pdcashlistOp() {
        $condition = array();
        $condition['pdc_member_id'] =  $this->member_info['member_id'];
        $model_pd = Model('predeposit');
        $cash_list = $model_pd->getPdCashList($condition,30,'*','pdc_id desc');

        $page_count = $model_pd->gettotalpage($condition);

        output_data(array('list' => $cash_list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function pdcashinfoOp() {
        $pdc_id = intval($_GET["pdc_id"]);
        if ($pdc_id <= 0){
            output_error("缺少参数");
        }
        $model_pd = Model('predeposit');
        $condition = array();
        $condition['pdc_member_id'] = $this->member_info['member_id'];
        $condition['pdc_id'] = $pdc_id;
        $info = $model_pd->getPdCashInfo($condition);
        if (empty($info)){
            output_error("无记录");
        }
        else{
            $info['pdc_add_time_text'] = date("Y-m-d H:i",$info['pdc_add_time']);
            $info['pdc_payment_state_text'] = str_replace(array('0','1','2'),array('审核中','已审核','已支付'),$info['pdc_payment_state']);
            $info['pdc_payment_time_text'] = $info['pdc_payment_time']>0 ? date("Y-m-d H:i",$info['pdc_payment_time']) : "";
        }

        output_data(array('info' => $info));
    }

    /**
     * 地址列表
     */
    public function rcblogOp() {
        $model_rcb = Model('rcb_log');
        $list = $model_rcb->where(array(
            'member_id' => $this->member_info['member_id'],
        ))->page(20)->order('id desc')->select();


        $page_count = $model_rcb->gettotalpage(array(
            'member_id' => $this->member_info['member_id'],
        ));

        output_data(array('log_list' => $list), mobile_page($page_count));
    }


    /**
     * 平台充值卡
     */
    public function rechargecard_addOp()
    {
        list($checkvalue, $checktime, $checkidhash) = explode("\t", decrypt(cookie('seccode'.$_POST['codekey']),MD5_KEY));
        $return = $checkvalue == strtoupper($_POST['captcha']) && $checkidhash == $_POST['codekey'];
        if (!$return){
                output_error('您输入的验证码有误，请重新输入！',array('code'=>100));
                exit;
        }
        $sn = (string) $_POST['rc_sn'];
        if (!$sn || strlen($sn) > 50) {
            output_error('平台充值卡卡号不能为空且长度不能大于50',array('code'=>100));
            exit;
        }

      try {
            model('predeposit')->addRechargeCard($sn, $this->member_info);
            output_data(array('ok'=>'平台充值卡使用成功'));
            exit;
        } catch (Exception $e) {
            output_error($e->getMessage(),array('code'=>99));
            exit;
        }
    }

}
