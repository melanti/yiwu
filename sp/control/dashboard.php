<?php
/**
 * 控制台
 *
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');

class dashboardControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('dashboard');
	}
	/**
	 * 欢迎페이지
	 */
	public function welcomeOp(){
		/**
		 * 관리자信息
		 */
		$model_admin = Model('admin');
		$tmp = $this->getAdminInfo();
		$condition['admin_id'] = $tmp['id'];
		$admin_info = $model_admin->infoAdmin($condition);
		$admin_info['admin_login_time'] = date('Y-m-d H:i:s',($admin_info['admin_login_time'] == '' ? time() : $admin_info['admin_login_time']));
		/**
		 * 시스텀信息
		 */
		$version = C('version');
		$setup_date = C('setup_date');
		$statistics['os'] = PHP_OS;
		$statistics['web_server'] = $_SERVER['SERVER_SOFTWARE'];
		$statistics['php_version'] = PHP_VERSION;
		$statistics['sql_version'] = Db::getServerInfo();
		$statistics['shop_version'] = $version;
		$statistics['setup_date'] = substr($setup_date,0,10);

        // Cb c extension
        try {
            $r = new ReflectionExtension('Cb');
            $statistics['php_version'] .= ' / ' . $r->getVersion();
        } catch (ReflectionException $ex) {
        }

		Tpl::output('statistics',$statistics);
		Tpl::output('admin_info',$admin_info);
		Tpl::showpage('welcome');
	}

	/**
	 * 关于我们
	 */
	public function aboutusOp(){

		Tpl::showpage('aboutus');
	}

	/**
	 * 통계
	 */
	public function statisticsOp(){
        $statistics = array();
        // 本周시작시간点
        $tmp_time = mktime(0,0,0,date('m'),date('d'),date('Y'))-(date('w')==0?7:date('w')-1)*24*60*60;
        /**
         * 会员
         */
        $model_member = Model('member');
        // 会员생성수
        $statistics['member'] = $model_member->getMemberCount(array());
        // 신규회원수
        $statistics['week_add_member'] = $model_member->getMemberCount(array('member_time' => array('egt', $tmp_time)));
        // 적립금인출
        $statistics['cashlist'] = Model('predeposit')->getPdCashCount(array('pdc_payment_state'=>0));

        /**
         * 업체
         */
        $model_store = Model('store');
        // 업체생성수
        $statistics['store'] = Model('store')->getStoreCount(array());
        // 업체申请数
        $statistics['store_joinin'] = Model('store_joinin')->getStoreJoininCount(array('joinin_state' => array('in', array(10, 11))));
        //경영항목신청
        $statistics['store_bind_class_applay'] = Model('store_bind_class')->getStoreBindClassCount(array('state'=>0));
        //업체연장계약
        $statistics['store_reopen_applay'] = Model('store_reopen')->getStoreReopenCount(array('re_state'=>1));
        // 곧 만기
        $statistics['store_expire'] = $model_store->getStoreCount(array('store_state'=>1, 'store_end_time'=>array('between', array(TIMESTAMP, TIMESTAMP + 864000))));
        // 已经到期
        $statistics['store_expired'] = $model_store->getStoreCount(array('store_state'=>1, 'store_end_time'=>array('between', array(1, TIMESTAMP))));

        /**
         * 상품
         */
        $model_goods = Model('goods');
        // 상품생성수
        $statistics['goods'] = $model_goods->getGoodsCommonCount(array());
        // 추가상품数
        $statistics['week_add_product'] = $model_goods->getGoodsCommonCount(array('goods_addtime' => array('egt', $tmp_time)));
        // 심사대기중
        $statistics['product_verify'] = $model_goods->getGoodsCommonWaitVerifyCount(array());
        // 举报
        $statistics['inform_list'] = Model('inform')->getInformCount(array('inform_state'=>1));
        // 브랜드申请
        $statistics['brand_apply'] = Model('brand')->getBrandCount(array('brand_apply' => '0'));

        /**
         * 交易
         */
        $model_order = Model('order');
        $model_refund_return = Model('refund_return');
        $model_vr_refund = Model('vr_refund');
        $model_complain = Model('complain');
        // 주문생성수
        $statistics['order'] = $model_order->getOrderCount(array());
        // 환불
        $statistics['refund'] = $model_refund_return->getRefundReturn(array('refund_type' => 1, 'refund_state' => 2));
        // 退货
        $statistics['return'] = $model_refund_return->getRefundReturn(array('refund_type' => 2, 'refund_state' => 2));
        // E-쿠폰 주문환불
        $statistics['vr_refund'] = $model_vr_refund->getRefundCount(array('admin_state' => 1));
        // 신고
        $statistics['complain_new_list'] = $model_complain->getComplainCount(array('complain_state'=>10));
        // 待仲裁
		$statistics['complain_handle_list'] = $model_complain->getComplainCount(array('complain_state'=>40));

		/**
         * 运营
		 */
		// 공동구매수량
		$statistics['groupbuy_verify_list'] = Model('groupbuy')->getGroupbuyCount(array('state'=>10));
		// 积分주문
		$statistics['points_order'] = Model()->cls()->table('points_order')->where(array('point_orderstate'=>array('in',array(11,20))))->count();
		//심사대기계산서
		$model_bill = Model('bill');
		$model_vr_bill = Model('vr_bill');
		$condition = array();
		$condition['ob_state'] = BILL_STATE_STORE_COFIRM;
		$statistics['check_billno'] = $model_bill->getOrderBillCount($condition);
		$statistics['check_billno'] += $model_vr_bill->getOrderBillCount($condition);
		//待支付계산서
		$condition = array();
		$condition['ob_state'] = BILL_STATE_SYSTEM_CHECK;
		$statistics['pay_billno'] = $model_bill->getOrderBillCount($condition);
		$statistics['pay_billno'] += $model_vr_bill->getOrderBillCount($condition);
		// 고객센터
		$statistics['mall_consult'] = Model('mall_consult')->getMallConsultCount(array('is_reply' => 0));
        // 服务站
        $statistics['delivery_point'] = Model('delivery_point')->getDeliveryPointWaitVerifyCount(array());
        /**
         * CMS
         */
        if (C('cms_isuse')) {
            // 文章심사
            $statistics['cms_article_verify'] = Model('cms_article')->getCmsArticleCount(array('article_state' => 2));
            // 画报심사
            $statistics['cms_picture_verify'] = Model('cms_picture')->getCmsPictureCount(array('picture_state' => 2));
        }
        /**
         * 圈子
         */
        if (C('circle_isuse')) {
            $statistics['circle_verify'] = Model('circle')->getCircleUnverifiedCount();
        }

        echo json_encode($statistics);
		exit;
	}
}
