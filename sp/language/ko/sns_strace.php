<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['admin_snsstrace_storename']	= '업체명';
$lang['store_sns_coupon_price']		= '쿠폰금액';
$lang['store_sns_start-stop_time']	= '시작마감시간';
$lang['store_sns_formerprice']		= '프로모션가격';
$lang['store_sns_bundling_price']	= '셋트가격';
$lang['store_sns_free_shipping']	= '무료배송';
$lang['store_sns_goodsprice']		= '상품원가';
$lang['store_sns_groupprice']		= '공동구매가격';
$lang['sns_sharegoods_price']		= '가격';
$lang['sns_sharegoods_freight']		= '배송료';
$lang['store_sns_trace_type']		= '동태유형';
$lang['store_sns_normal']			= '새소식';
$lang['store_sns_new']				= '신품';
$lang['store_sns_coupon']			= '쿠폰';
$lang['store_sns_xianshi']			= '한시할인';
$lang['store_sns_mansong']			= '특정가증정';
$lang['store_sns_bundling']			= '할인셋트';
$lang['store_sns_groupbuy']			= '공동구매';
$lang['store_sns_recommend']		= '추천';
$lang['store_sns_hotsell']			= 'HOT';
$lang['store_sns_new_selease']		= '신상품';
$lang['store_sns_store_recommend']	= '업체추천';
$lang['store_sns_hotsell']			= '인기상품';

