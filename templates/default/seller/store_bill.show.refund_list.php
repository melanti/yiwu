<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<form method="get" id="formSearch">
<table class="search-form">
  <input type="hidden" id='act' name='act' value='store_bill' />
  <input type="hidden" id='op' name='op' value='show_bill' />
  <input type="hidden" name='ob_no' value='<?php echo $_GET['ob_no'];?>' />
  <input type="hidden" name='type' value='<?php echo $_GET['type'];?>' />
  <tr>
    <td>&nbsp;</td>
    <th>환불번호</th>
    <td class="w180"><input type="text" class="text"  value="" name="query_order_no" /></td>
    <th>환불시간</th>
    <td class="w180">
    	<input type="text" class="text w70" name="query_start_date" id="query_start_date" value="<?php echo $_GET['query_start_date']; ?>"/>
      &#8211;
      <input type="text" class="text w70" name="query_end_date" id="query_end_date" value="<?php echo $_GET['query_end_date']; ?>"/>
    </td>
    <td class="tc w180">
    <label class="submit-border"><input type="submit" class="submit" value="검색" /></label>
    <label class="submit-border"><input type="button" id="ncexport" class="submit" value="csv파일" /></label>
    </td>
</table>
</form>
<table class="ncsc-default-table">
    <thead>
      <tr>
        <th class="w10"></th>
        <th>환불번호</th>
        <th>주문번호</th>
        <th>환불금액</th>
        <th>환불수수료</th>
        <th>유형</th>
        <th>환불시간</th>
      </tr>
    </thead>
    <tbody>
      <?php if (is_array($output['refund_list']) && !empty($output['refund_list'])) { ?>
      <?php foreach($output['refund_list'] as $refund_info) { ?>
      <tr class="bd-line">
        <td></td>
        <td><?php echo $refund_info['refund_sn'];?></td>
        <td><?php echo $refund_info['order_sn'];?></td>
        <td><?php echo $refund_info['refund_amount'];?></td>
        <td><?php echo ncPriceFormat($refund_info['commis_amount']);?></td>
        <td><?php echo str_replace(array(1,2), array('환불 ','반품'),$refund_info['refund_type']);?></td>
        <td><?php echo date("Y-m-d",$refund_info['admin_time']);?></td>
      </tr>
      <?php }?>
      <?php } else { ?>
      <tr>
        <td colspan="20" class="norecord"><i>&nbsp;</i><span>내용이 없습니다.</span></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php if (is_array($output['refund_list']) && !empty($output['refund_list'])) { ?>
      <tr>
        <td colspan="20"><div class="pagination">보기</div></td>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
<script type="text/javascript">
$(function(){
    $('#ncexport').click(function(){
    	$('input[name="op"]').val('export_refund_order');
    	$('#formSearch').submit();
    });
});
</script>