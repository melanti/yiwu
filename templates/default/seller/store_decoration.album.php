<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert alert-info mt10">
  <h4>TIP</h4>
  <ul>
    <li>1. 미니샵 디자인 설정시 이미지를 업로드 하는 공간이며 최대 100장까지 업로드 가능합니다. 정기적으로 사용하시지 않는 이미지를 정리해주시면 지속적인 정상사용이 가능합니다.</li>
    <li>2. 이미지 삭제시 미니샵 디자인 템플릿에서 사용 여부를 꼭 확인하여 주십시오. 사용중인 이미지 삭제시 템플릿에서도 삭제됩니다.</li>
  </ul>
</div>
<?php if(!empty($output['image_list']) && is_array($output['image_list'])) {?>
<div class="ncsc-gallery">
  <ul>
    <?php foreach($output['image_list'] as $key => $value) {?>
    <li>
      <div class="pic-thumb"><a cnbiztype="nyroModal" href="<?php echo $value['image_url'];?>"><img src="<?php echo $value['image_url'];?>" alt="<?php echo $value['image_name'];?>"></a></div>
      <div class="pic-name"><?php echo $value['image_origin_name'];?></div>
      <div class="pic-handle"><span><?php echo $value['upload_time_format'];?></span><a cnbiztype="btn_del_image" href="javascript:void(0);" data-image-id="<?php echo $value['image_id'];?>" class="ncsc-btn-mini"><i class="icon-trash"></i>삭제</a></div>
    </li>
    <?php } ?>
  </ul>
</div>
<div class="pagination"><?php echo $output['show_page']; ?></div>
<?php } else { ?>
<div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div>
<?php } ?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/custom.min.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/styles/nyroModal.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.raty/jquery.raty.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
    $('.raty').raty({
        path: "<?php echo RESOURCE_SITE_URL;?>/js/jquery.raty/img",
        readOnly: true,
        score: function() {
          return $(this).attr('data-score');
        }
    });

   $('a[cnbiztype="nyroModal"]').nyroModal();
});
</script> 
<script type="text/javascript">
    $(document).ready(function(){
        $('[cnbiztype="btn_del_image"]').on('click', function() {
            if(confirm('정말 삭제 하시겠습니까？')) {
                $this = $(this);
                var image_id = $(this).attr('data-image-id');
                $.ajax({
                    type: "POST",
                    url: '<?php echo urlShop('store_decoration', 'decoration_album_del');?>',
                    data: {image_id: image_id},
                    dataType: 'json'
                })
                .done(function(data) {
                    if(typeof data.error == 'undefined') {
                        $this.parents('li').hide();
                    } else {
                        showError(data.error); 
                    }
                })
                .fail(function() {
                   showError('삭제실패'); 
                });
            }
        });
    });
</script> 
