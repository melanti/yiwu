<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 公用
 */
$lang['admin_evaluate'] 				= '평가';
$lang['admin_evaluate_list'] 			= '소비자의 평가';
$lang['admin_evalseller_list'] 			= '판매자의 평가';
$lang['admin_evaluate_addtime'] 		= '평가시간';
$lang['admin_evaluate_storename'] 		= '미니샵이름';
$lang['admin_evaluate_ordersn'] 		= '주문번호';
$lang['admin_evaluate_frommembername'] 	= '평가인';
$lang['admin_evaluate_tomembername'] 	= '평가된 사람';
$lang['admin_evalstore_list'] 			= '미니샵 평가 동태';
$lang['admin_evaluate_recorderror'] 	= '평가 정보 오류';
/**
 * 상품평가
 */
$lang['admin_evaluate_state_common'] 		= '최상평';
$lang['admin_evaluate_state_hidden'] 		= '노출금지';
$lang['admin_evaluate_grade_good'] 			= '상평';
$lang['admin_evaluate_grade_normal'] 		= '중평';
$lang['admin_evaluate_grade_bad'] 			= '하평';
$lang['admin_evaluate_edit'] 				= '평가수정';
$lang['admin_evaluate_goodsname'] 			= '상품이름';
$lang['admin_evaluate_goods_specinfo'] 		= '상품규칙';
$lang['admin_evaluate_grade'] 				= '평가등급';
$lang['admin_evaluate_state'] 				= '평E-쿠폰 태';
$lang['admin_evaluate_buyerdesc'] 			= '평가설명';
$lang['admin_evaluate_adminremark']			= '관리자 메모';
$lang['admin_evaluate_help1']				= '소비자는 주문 완료후 평가를 할 수 있습니다.';
$lang['admin_evaluate_help2']				= '평가 정보는 해당 상품 페이지에 노출됩니다.';
$lang['admin_evaluate_notice']				= '노출금지, 평가 유효는 되지만 노출안함';
$lang['admin_evaluate_explain'] 			= '해석';
$lang['admin_evaluate_defaultcontent_good'] 	= '상평!';
$lang['admin_evaluate_defaultcontent_normal'] 	= '중평!';
$lang['admin_evaluate_defaultcontent_bad'] 		= '하평!';
$lang['admin_evaluate_delexplain'] 			= '해석삭제';
/**
 * 업체动态평가
 */
$lang['admin_evalstore_help1']				= '소비자가 주문 완료후 해당 미니샵에 대한 평가를 할 수 있습니다.';
$lang['admin_evalstore_help2']				= '평가 통계 정보는 해당 미니샵에 노출됩니다.';
$lang['admin_evalstore_type']				= '평가유형';
$lang['admin_evalstore_type_1']				= '상품 및 설명이 잃치합니다.';
$lang['admin_evalstore_type_2']				= '판매자 서비스 태도가 좋아요';
$lang['admin_evalstore_type_3']				= '판매자 발송 속도가 좋아요';
$lang['admin_evalstore_score']				= '평가수';
$lang['admin_evalstore_score_unit']			= '점';
?>