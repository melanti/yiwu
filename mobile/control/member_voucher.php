<?php
/**
 * 我的代金券
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class member_voucherControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
    }

    /**
    * 地址列表
    */
    public function voucher_listOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], 0, $this->page);
        foreach($voucher_list as $k=>$v):
            $v['voucher_end_date_text'] = date("Y-m-d H:i",$v['voucher_end_date']);
            $v['store_avatar_url'] = $v['voucher_t_customimg'];
        
            $new_voucher_list[] = $v;
        endforeach;
        
        $page_count = $model_voucher->gettotalpage();

        output_data(array('voucher_list' => $new_voucher_list), mobile_page($page_count));
    }
}
