'use strict';

var request = require('supertest'),
should = require('should'),
_ = require('underscore'),
Q = require('q');

var tmpVersions,
    lib_net,
    CategoryController,
    RequestService,
    CommonService,
    testListReqModel = {
        account_uuid: '',
        brand_id: '',
        sex: '',
        status: '',
        show: 'debug'
    },
    testListResModel = {};

if (process.env['MODE'] == 'ALL_DEBUG') {
    //Operation Test
    // startTest();
}
startTest();

function startTest() {
    describe('=== A. category 테스트 케이스 ===\n', function () {
        testA0();
        testA1();
    });
}

function testA0() {
    describe('=== A.0 모듈 설정 === \n', function () {
        it('A.0.1 assignModule', function (done) {
            assignBasicModule();
            assignCategoryCtrlModule();
            done();
        });
    });
}

function testA1() {
    describe('=== A.1 카테고리 리스트 테스트 === \n', function () {
        it('A.1.1 카테고리 리스트', function (done) {
            CommonService.reqTestAPI({
                r: request,
                url: RequestService.API_URL.GET_CATE_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
                method: 'get',
                expectStatus: 200,
                sailsSid: CommonService.getTestSid(),
                errLogTitle: 'A.1.1 카테고리 리스트 ERR',
                assertCallback: function (body) {
                    testListResModel = body.results.concat();
                    sails.log.debug('A.1.1  body: ' + JSON.stringify(body, null, 4));
                },
                done: done
            });
        });

        it('A.1.2 카테고리 리스트 - 브랜드 id ', function (done) {
            testListReqModel.brand_id = 1;
            CommonService.reqTestAPI({
                r: request,
                url: RequestService.API_URL.GET_CATE_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
                method: 'get',
                expectStatus: 200,
                sailsSid: CommonService.getTestSid(),
                errLogTitle: 'A.1.2 카테고리 리스트 ERR',
                assertCallback: function (body) {
                    testListResModel = body.results.concat();
                    sails.log.debug('A.1.2  body: ' + JSON.stringify(body, null, 4));
                },
                done: done
            });
        });

        it('A.1.3 카테고리 리스트 - woman ', function (done) {
            testListReqModel.brand_id = '';
            testListReqModel.sex = 'woman';
            CommonService.reqTestAPI({
                r: request,
                url: RequestService.API_URL.GET_CATE_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
                method: 'get',
                expectStatus: 200,
                sailsSid: CommonService.getTestSid(),
                errLogTitle: 'A.1.3 카테고리 리스트 ERR',
                assertCallback: function (body) {
                    testListResModel = body.results.concat();
                    sails.log.debug('A.1.3  body: ' + JSON.stringify(body, null, 4));
                },
                done: done
            });
        });

        it('A.1.4 카테고리 리스트 - man ', function (done) {
            testListReqModel.sex = 'man';
            CommonService.reqTestAPI({
                r: request,
                url: RequestService.API_URL.GET_CATE_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
                method: 'get',
                expectStatus: 200,
                sailsSid: CommonService.getTestSid(),
                errLogTitle: 'A.1.4 카테고리 리스트 ERR',
                assertCallback: function (body) {
                    testListResModel = body.results.concat();
                    sails.log.debug('A.1.4  body: ' + JSON.stringify(body, null, 4));
                },
                done: done
            });
        });

        it('A.1.5 카테고리 리스트 - girl ', function (done) {
            testListReqModel.sex = 'girl';
            CommonService.reqTestAPI({
                r: request,
                url: RequestService.API_URL.GET_CATE_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
                method: 'get',
                expectStatus: 200,
                sailsSid: CommonService.getTestSid(),
                errLogTitle: 'A.1.5 카테고리 리스트 ERR',
                assertCallback: function (body) {
                    testListResModel = body.results.concat();
                    sails.log.debug('A.1.5  body: ' + JSON.stringify(body, null, 4));
                },
                done: done
            });
        });


        it('A.1.6 카테고리 리스트 - boy ', function (done) {
            testListReqModel.sex = 'boy';
            CommonService.reqTestAPI({
                r: request,
                url: RequestService.API_URL.GET_CATE_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
                method: 'get',
                expectStatus: 200,
                sailsSid: CommonService.getTestSid(),
                errLogTitle: 'A.1.6 카테고리 리스트 ERR',
                assertCallback: function (body) {
                    testListResModel = body.results.concat();
                    sails.log.debug('A.1.6  body: ' + JSON.stringify(body, null, 4));
                },
                done: done
            });
        });


        it('A.1.7 카테고리 리스트 - not found error ', function (done) {
            testListReqModel.sex = 'boys';
            CommonService.reqTestAPI({
                r: request,
                url: RequestService.API_URL.GET_CATE_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
                method: 'get',
                expectStatus: 400,
                sailsSid: CommonService.getTestSid(),
                errLogTitle: 'A.1.7 카테고리 리스트 ERR',
                assertCallback: function (body) {
                    testListResModel = body.results.concat();
                    sails.log.debug('A.1.7  body: ' + JSON.stringify(body, null, 4));
                },
                done: done
            });
        });
    });
}

function assignBasicModule() {
    tmpVersions = require('../../../package.json');
    lib_net = require('../../../api/controllers/libs/network.js');
    RequestService = require('../../util/RequestService.js');
    CommonService = require('../../util/CommonService.js');
}

function assignCategoryCtrlModule() {
    CategoryController = require('../../../api/controllers/CategoryController.js');
}