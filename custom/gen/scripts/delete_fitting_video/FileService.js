var crypto = require('crypto');
var fs = require('fs');
var Q = require('q');
var SFTPClient = require('ssh2').Client;
var conf = require("../../libs/config.js").get();
var _ = require("underscore");
var SFTP_INFO = {
  host: conf.ftp.host,
  port: conf.ftp.port,
  username: conf.ftp.username,
  password: conf.ftp.password,
  cwd: conf.ftp.cwd,
  upload_dir:'/DAT',
  readyTimeout:10000000
};

module.exports = {
  computeFileHash : _computeFileHash,
  computeFileHashPromisify:_computeFileHashPromisify,
  connectSFTP:_connectSFTP,
  destroyConnectSFTP:_destroyConnectSFTP,
  opendir:_opendir,
  createDir:_createDir,
  createFileStream:_createFileStream,
  uploadThreedFiles:_uploadThreedFiles,
  deleteFittingFolders:_deleteFittingFolders
};


function _computeFileHash(source, callback) {
  var hash;
  var fd;

  hash = crypto.createHash('sha256');
  hash.setEncoding('hex');

  fd = fs.createReadStream(source);
  fd.on('error', function (err) {
    return callback(err, null);
  });
  fd.on('end', function () {
    hash.end();
    var checksum = hash.read();
    return callback(null, checksum);
  });
  fd.pipe(hash);
}

function _computeFileHashPromisify(source){
  var deferred = Q.defer();
  var hash;
  var fd;

  hash = crypto.createHash('sha256');
  hash.setEncoding('hex');

  fd = fs.createReadStream(source);
  fd.on('error', function (err) {
    deferred.reject(err);
  });
  fd.on('end', function () {
    hash.end();
    deferred.resolve({source:source ,hash:hash.read()});
  });
  fd.pipe(hash);

  return deferred.promise;
}

function _connectSFTP(conn_info, destroy_fnc){
  var deferred = Q.defer();

  var conn = new SFTPClient();
  var sftp;

  conn.on('ready', function(){
    conn.sftp(function(err, ftp) {
      if (err) {
        console.log('err SFTP_CONNECTION sftp: ' , err);
        destroy_fnc(conn, sftp);
        deferred.reject(err);
        return false;
      }
      sftp = ftp;
      deferred.resolve({connection:conn, sftp:ftp});
    });
  });

  conn.on('error', function(err){
    if (err) {
      console.log('err SFTP_CONNECTION : ' , err);
      destroy_fnc(conn, sftp);
      deferred.reject(err);
    }
  });

  conn.on('end', function(){
    console.log('end conn : ' );
    destroy_fnc(conn, sftp);
  });

  conn.connect(conn_info);

  return deferred.promise;
}

function _destroyConnectSFTP(c, f){
  try{
    if(f) f.end();
    if(c){
      c.end();
      c.destroy();
    }
    c = null;
    f = null;
  }catch(e){

  }
}

function _createDir(conn, sftp, dir){
  var d = Q.defer();
  try{
    sftp.mkdir(dir, function(err){
      if(err){
        //throw err;
        console.log('fail, _createDir: ' , err);
        d.reject(err);

      }else{
        d.resolve({connection:conn, sftp:sftp, dir:dir});
      }

    });

  }catch(e){
    d.reject(e);
  }

  return d.promise;
}


function _opendir(conn, sftp, dir, errCode, doErrReject){
    var d = Q.defer();

    //console.log('_opendir dir: ' , dir);

    try{

        sftp.opendir(dir, function(err, data){
            if(err){

                //throw err;
                if(doErrReject){
                    console.log('FileService _opendir ERROR doErrReject: ',err);
                    d.reject({code:(errCode || 'ERR_DIR_OPEN'), status:500});
                }else{
                    d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:[], hasDir:false, error:err});
                }


            }else{
                //console.log('success, opendir: ', data);

                sftp.readdir(data, function(err, list) {
                    if(err){
                        //throw err;

                        if(doErrReject){
                            console.log('FileService readdir ERROR doErrReject: ',err);
                            d.reject({code:(errCode || 'ERR_DIR_READ'), status:500});
                        }else{
                            d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:[], hasDir:true, error:err});
                        }

                    }else{
                        //console.log('success, readdir: ', list);
                        d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:list, hasDir:true});
                    }
                });


            }

        });

    }catch(e){
        console.log('FileService _opendir ERROR catch doErrReject: ',e);
        d.reject({code:(errCode || 'ERR_DIR_OPEN'), status:500});
    }

    return d.promise;
}

function _removedir(conn, sftp, dir, errCode, doErrReject){
    // console.log('_removedir: ');
    var d = Q.defer();
    try{
        sftp.rmdir(dir, function(err, list) {
            if(err){
                //throw err;

                if(doErrReject){
                    console.log('FileService _removedir ERROR doErrReject: ',err);
                    d.reject({code:(errCode || 'ERR_DIR_REMOVE'), status:500});
                }else{
                    d.resolve({connection:conn, sftp:sftp, dir:dir, error:err});
                }

            }else{
                console.log('success, rmdir: ', list);
                d.resolve({connection:conn, sftp:sftp, dir:dir});
            }
        });
    }catch(e){
        console.log('FileService _removedir catch ERROR: ',e);
        if(doErrReject){
            d.reject({code:(errCode || 'ERR_DIR_REMOVE'), status:500});
        }else{
            d.resolve({connection:conn, sftp:sftp, dir:dir, error:e});
        }

    }
    return d.promise;
}

function _unlinkFile(conn, sftp, dir, errCode, doErrReject){
    // console.log('_unlinkFile:::: ');
    // console.log('conn: ', (typeof conn));
    // console.log('sftp sftp: ', (typeof sftp));
    // console.log('dir: ', (dir));

    var d = Q.defer();
    try{
        sftp.unlink(dir, function(err, list) {
            if(err){
                //throw err;
                if(doErrReject){
                    console.log('FileService _unlinkFile ERROR doErrReject: ',err);
                    d.reject({code:(errCode || 'ERR_DIR_FILE'), status:500});
                }else{
                    d.resolve({connection:conn, sftp:sftp, dir:dir, error:err});
                }
            }else{
                d.resolve({connection:conn, sftp:sftp, dir:dir});
            }
        });
    }catch(e){
        console.log('FileService _unlinkFile ERROR: ',e);
        if(doErrReject){
            console.log('FileService _unlinkFile ERROR doErrReject: ',e);
            d.reject({code:(errCode || 'ERR_DIR_FILE'), status:500});
        }else{
            d.resolve({connection:conn, sftp:sftp, dir:dir, error:e});
        }
    }
    return d.promise;
}

function _unlinkFiles(conn, sftp, filesNameArr){
    // console.log('_unlinkFiles: ');
    // console.log('conn: ', (typeof conn));
    // console.log('sftp sftp: ', (typeof sftp));
    // console.log('filesNameArr: ', (filesNameArr));

    var tmp_promises;
    tmp_promises = _.map(
        filesNameArr,
        function(v, k){
            return _unlinkFile(conn, sftp, v);
        }
    );

    return Q.all(tmp_promises);
}

function _createFileStream(conn, sftp, readFilePath, uploadPath){
  var d = Q.defer();
  try{
    //TODO: readStream, writeStream ==>destroy
    var readStream = fs.createReadStream( readFilePath );
    var writeStream = sftp.createWriteStream( uploadPath );

    writeStream.once('close',function () {
      console.log( "- file transferred succesfully" );
      writeStream.end();
      readStream = null;
      writeStream = null;
      d.resolve({code:'SUCCESS', connection:conn, sftp:sftp, readFilePath:readFilePath ,uploadPath:uploadPath });
    });

    writeStream.once('end', function () {
      console.log( "sftp connection closed" );
    });
    readStream.once('error', function (err) {
      if(err){
        //throw err;
        try{
          readStream = null;
          writeStream = null;
        }catch(e){}
        d.reject(err);
      }
    });
    readStream.pipe( writeStream );

  }catch(e){
    d.reject(e);
  }

  return d.promise;
}

function _uploadThreedFiles(source){
  // console.log('uploadThreedFiles: ',source.td_cd);

  var tmp_mkdir_path = '/'+SFTP_INFO.cwd+SFTP_INFO.upload_dir+'/'+(source.td_cd);
  var deferred = Q.defer();
  var td_ctx = {
      mkdir_path:'/'+SFTP_INFO.cwd+SFTP_INFO.upload_dir+'/'+(source.td_cd),
      connection:null,
      sftp:null,
      dirData:null,
      doRemoveFiles:false,
      doRemoveDir:false
  };

  Q.fcall(_connectSFTPServer)
      .then(_assignConnAndSFTP)
      .then(_openDirectory)
      .then(_checkDirectory)
      .then(_checkRemoveFiles)
      .then(_checkRemoveDir)
      .then(_createDirectory)
      .then(_uploadTdFiles)
      .spread(_allSetttled)
      .catch(function(e){
        console.log('fail::: FileServices.uploadThreedFiles ER_FILE_UPLOAD: ' , e);
        _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );

        deferred.reject({code:'ER_FILE_UPLOAD',status:500});
      });


    function _connectSFTPServer(){
        return _connectSFTP(SFTP_INFO, _destroyConnectSFTP);
    }

    function _assignConnAndSFTP(results){
    //예외처리때 destroy하기위한  connect, sftp instance의 mutable state
        td_ctx.connection = results.connection;
        td_ctx.sftp = results.sftp;
        td_ctx.mkdir_path = tmp_mkdir_path;
        return Q.all([results]);
    }

    function _openDirectory(){
        console.log('_openDirectory: ');
        return _opendir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path);
    }

    function _checkDirectory(results){
        console.log('_checkDirectory: ');
        if(results.hasDir){
            if(results.dirData !==false && results.dirData.length>0){
                console.log('dir exist && file exist:: file remove, dir remove');

                try{
                    td_ctx.dirData = results.dirData;
                    td_ctx.doRemoveFiles = true;
                    td_ctx.doRemoveDir = true;

                }catch(e){
                    console.log('WHAT???: ' , e);
                    deferred.reject({code:'ER_FILE_UPLOAD',status:500});
                }

            }else{
                console.log('dir exist && file empty:: only dir remove');
                td_ctx.doRemoveFiles = false;
                td_ctx.doRemoveDir = true;
            }

        }else{
            console.log('dir empty && file empty:: ALL create');
            td_ctx.doRemoveFiles = false;
            td_ctx.doRemoveDir = false;
        }

        return td_ctx;
    }


    function _checkRemoveFiles(){
        // console.log('_checkRemoveFiles: ');
        if(td_ctx.doRemoveFiles){
            _unlinkFiles(
                td_ctx.connection,
                td_ctx.sftp,
                _.map(td_ctx.dirData, function(v,k){return td_ctx.mkdir_path+'/'+v.filename;}))
                .then(function(){
                    return Q.all([td_ctx]);
                })
                .catch(function(er){
                    console.log('unlinkFiles: catch error ', er);
                    throw new Error('unlinkFiles catch error');
                });

        }else{
            return Q.all([td_ctx]);
        }
    }

    function _checkRemoveDir(){
        // console.log('_checkRemoveDir: ');
        if(td_ctx.doRemoveDir){
            _removedir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path)
                .then(function(){
                    return Q.all([td_ctx]);
                })
                .catch(function(er){
                    console.log('unlinkFiles: catch error ', er);
                    throw new Error('unlinkFiles catch error');
                });

        }else{
            return Q.all([td_ctx]);
        }
    }

    function _createDirectory(){
        // console.log('_createDirectory: ');
        // console.log('td_ctx.connection: ' , (typeof td_ctx.connection));
        // console.log('td_ctx.sftp: ' , (typeof td_ctx.sftp));
        // console.log('td_ctx.mkdir_path: ' , (typeof td_ctx.mkdir_path));
        return _createDir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path);
    }

    function _uploadTdFiles(){
        var uploadFilePromises = [];
        for(var i=0, iTotal = source.upload_req_list.length; i<iTotal; ++i){
          uploadFilePromises.push(_createFileStream(
              td_ctx.connection,
              td_ctx.sftp,
              source.upload_req_list[i],
              td_ctx.mkdir_path+'/'+source.upload_req_file[i]
          ));
        }
        return Q.all(uploadFilePromises);
    }

    function _allSetttled(){
        console.log('uploadThreedFiles _allSetttled: ' );
        _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );
        td_ctx = null;
        deferred.resolve(source);
    }

    return deferred.promise;
}


function _deleteFittingFolders(data) {
    console.log('deleteFittingFolders ::: ');


    var deferred = Q.defer();
    var td_ctx = {
        default_path:'/'+SFTP_INFO.cwd+'/',
        connection:null,
        sftp:null,
        dirData:null,
        doRemoveFiles:false,
        doRemoveDir:false,
        target_path:null
    };


    Q.fcall(_connectSFTPServer)
    .then(_assignConnAndSFTP)
    .then(_deleteFolders)
    .then(_allSettled)
    .catch(function(e){
        console.log('fail::: FileServices.deleteFiles ER_FILE_DELETE: ' , e);
        _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );

        deferred.reject({code:'ER_FILE_DELETE',status:500});
    });


    function _connectSFTPServer(){
        return _connectSFTP(SFTP_INFO, _destroyConnectSFTP);
    }

    function _assignConnAndSFTP(results){
        //예외처리때 destroy하기위한  connect, sftp instance의 mutable state
        td_ctx.connection = results.connection;
        td_ctx.sftp = results.sftp;
        return Q.all([results]);
    }

    function _deleteFolders(){
        console.log('_deleteFolders: ');
        var deferred = Q.defer();

        var videos = data.videos;
        var promises = videos.reduce(function (promise, video) {
            return promise.then(function () {
                var target = td_ctx.default_path + "USER/" + video.fitv_sex + "/" + video.fitv_cd + "/";
                return _doDeleteFolders(video, target);
            });
        }, Q.resolve());

        promises
        .then(function() {
            console.log('\nsftp delete done...');
            deferred.resolve();
        })
        .catch(function(err) {
            console.log('\nsftp delete fail...' + err);
            deferred.resolve();
        });
        return deferred.promise;
    }

    function _doDeleteFolders(video, target) {
        var deferred = Q.defer();
        Q.fcall(_openDirectory)
        .then(_checkDirectory)
        .then(_checkRemoveFiles)
        .then(_checkRemoveDir)
        .then(function() {
            console.log('\n[s3 delete detail]');
            console.log('1.sex :: ' + video.fitv_sex);
            console.log('2.code :: ' + video.fitv_cd);
            console.log('3.target :: ' + target);
            deferred.resolve();
        })
        .catch(function(e){
            deferred.reject(e);
        });


        function _openDirectory(){
            console.log('_openDirectory: ' + target);
            return _opendir(td_ctx.connection, td_ctx.sftp, target);
        }

        function _checkDirectory(results){
            console.log('_checkDirectory: ');
            if(results.hasDir){
                if(results.dirData !==false && results.dirData.length>0){
                    console.log('dir exist && file exist:: file remove, dir remove');

                    try{
                        td_ctx.dirData = results.dirData;
                        td_ctx.doRemoveFiles = true;
                        td_ctx.doRemoveDir = true;

                    }catch(e){
                        console.log('WHAT???: ' , e);
                        deferred.reject({code:'ER_FILE_UPLOAD',status:500});
                    }

                }else{
                    console.log('dir exist && file empty:: only dir remove');
                    td_ctx.doRemoveFiles = false;
                    td_ctx.doRemoveDir = true;
                }

            }else{
                console.log('dir empty && file empty:: ALL create');
                td_ctx.doRemoveFiles = false;
                td_ctx.doRemoveDir = false;
            }

            return td_ctx;
        }


        function _checkRemoveFiles(){
            console.log('_checkRemoveFiles: ');
            var deferred = Q.defer();
            if(td_ctx.doRemoveFiles){
                _unlinkFiles(
                td_ctx.connection,
                td_ctx.sftp,
                _.map(td_ctx.dirData, function(v,k){return target+'/'+v.filename;}))
                .then(function(){
                    deferred.resolve();
                })
                .catch(function(er){
                    console.log('unlinkFiles: catch error ', er);
                    deferred.reject(new Error('unlinkFiles catch error'));
                });

            }else{
                deferred.resolve();
            }
            return deferred.promise;
        }

        function _checkRemoveDir(){
            console.log('_checkRemoveDir: ');
            var deferred = Q.defer();
            if(td_ctx.doRemoveDir){
                _removedir(td_ctx.connection, td_ctx.sftp, target)
                .then(function(){
                    deferred.resolve();
                })
                .catch(function(er){
                    console.log('unlinkFiles: catch error ', er);
                    deferred.reject(new Error('unlinkFiles catch error'));
                });

            }else{
                deferred.resolve();
            }
            return deferred.promise;
        }
        return deferred.promise;
    }



    function _allSettled(){
        console.log('deleteFiles _allSettled: ' );
        _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );
        td_ctx = null;
        deferred.resolve();
    }

    return deferred.promise;
}