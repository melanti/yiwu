<div class="eject_con">
  <div id="warning" class="alert alert-error"></div>
  <form method="post" action="<?php echo urlShop('store_goods_online', 'edit_plate');?>" id="plate_form">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="commonid" value="<?php echo $_GET['commonid']; ?>" />
    <dl>
      <dt>관련템플릿：</dt>
      <dd>
        <p>
          <label>상단템플릿</label>
          <select name="plate_top">
            <option>선택하십시오.</option>
            <?php if (!empty($output['plate_list'][1])) {?>
            <?php foreach ($output['plate_list'][1] as $val) {?>
            <option value="<?php echo $val['plate_id']?>" <?php if ($output['goods']['plateid_top'] == $val['plate_id']) {?>selected="selected"<?php }?>><?php echo $val['plate_name'];?></option>
            <?php }?>
            <?php }?>
          </select>
        </p>
        <p>
          <label>하단템플릿</label>
          <select name="plate_bottom">
            <option>선택하십시오</option>
            <?php if (!empty($output['plate_list'][0])) {?>
            <?php foreach ($output['plate_list'][0] as $val) {?>
            <option value="<?php echo $val['plate_id']?>" <?php if ($output['goods']['plateid_bottom'] == $val['plate_id']) {?>selected="selected"<?php }?>><?php echo $val['plate_name'];?></option>
            <?php }?>
            <?php }?>
          </select>
        </p>
        <p class="hint">입력하지 않으시면 기존에 선택된 템플릿으로 제공됩니다. 신중히 선택하여 주십시오.</p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input type="submit" class="submit" value="완료"/></label>
    </div>
  </form>
</div>
<script>
$(function(){
    $('#plate_form').submit(function(){
        ajaxpost('plate_form', '', '', 'onerror');
        return false;
    });
});
</script>