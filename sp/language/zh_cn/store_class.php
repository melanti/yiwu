<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 업체카테고리 语言包
 */

$lang['store_class_name_no_null']     = '카테고리명을 입력하세요';
$lang['store_class_sort_only_number'] = '카테고리 정렬은 반드시 숫자로 입력하세요!';
$lang['back_store_class_list']        = '돌아가기';
$lang['continue_add_store_class']     = '계속 카테고리 추가';
$lang['re_edit_store_class']          = '본 카테고리 재수정!';
$lang['import_ok']                    = '불러오기 성공';
$lang['import_csv_no_null']           = '불러오는 csv파일이 옳바르지 않습니다!';
$lang['illegal_parameter']            = '오류';

$lang['store_class']      = '업체카테고리';
$lang['manage']           = '관리';
$lang['store_class_name'] = '카테고리명';
$lang['store_class_bail'] = '보정금액';
$lang['can_edit']         = '수정가능';
$lang['del_store_class']  = '수정후 복구할 수 없습니다, 정말 삭제하시겠습니까?';
$lang['store_class_help1'] = '업체 입점시 업체 카테고리를 지정할 수 있습니다.';
$lang['store_class_help2'] = '<a>카테고리 수정후 설정 -> 캐쉬 비우기를 하셔야 적용됩니다.</a>';
$lang['can_edit']         = '수정가능';
$lang['back']             = '뒤로';
$lang['update_sort']      = '숫자 범위는 0~255사이로 작은 순위로 정렬됩니다.';
$lang['reset']            = '재설정';
$lang['store_class_name_no_null']     = '카테고리명을 입력하세요';
$lang['store_class_name_is_there']    = '본 카테고리명은 존재합니다.';
$lang['store_class_sort_only_number'] = '카테고리 정렬은 반드시 숫자로 입력하세요';
