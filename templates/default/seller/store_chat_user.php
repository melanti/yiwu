<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div id="chatUserList" class="ncsc-chat-user-list">
  <?php if (is_array($output['list']) && !empty($output['list'])) { ?>
  <ul>
    <?php foreach ($output['list'] as $key => $val) { ?>
    <li class="normal" user_id="<?php echo $val['u_id'];?>">
        <a href="javascript:void(0)" onclick="user_chat_log('<?php echo $val['u_id'];?>','<?php echo $val['u_name']; ?>');" title=" <?php echo $val['u_name']; ?>와 마지막으로 대화한 시간:<?php echo $val['time'];?>">
        <span class="avatar"><img src="<?php echo $val['avatar'];?>"/></span><?php echo $val['u_name']; ?> </a></li>
    <?php } ?>
  </ul>
  <?php } else { ?>
  <div class="warning-option"><i class="icon-warning-sign">&nbsp;</i><span>내용이 없습니다.</span></div>
  <?php } ?>
</div>
<script type="text/javascript">
	$('#store_chat_log .demo').ajaxContent({
		target:'#store_chat_log'
	});
	//自定义滚定条
	$('#chatUserList').perfectScrollbar();
</script>
