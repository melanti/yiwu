<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['consulting_index_manage']		= '문의관리';
$lang['consulting']				= '문의';
$lang['consulting_index_sender']		= '문의자';
$lang['consulting_index_content']		= '문의내용';
$lang['consulting_index_object']		= '문의대상';
$lang['consulting_index_store_name']		= '미니샵명';
$lang['consulting_index_time']			= '문의시간';
$lang['consulting_index_guest']			= '손님';
$lang['consulting_index_goods']		= '상품';
$lang['consulting_index_group']		= '공동구매';
$lang['consulting_index_reply']			= '답변';
$lang['consulting_index_help1']			= '상품 상세 페이지 상의 회원 문의 내용입니다. 해당 사항을 삭제 혹은 미노출 하실 수 있습니다.';
$lang['consulting_index_no_reply']		= '등록된 답변이 없습니다.';
$lang['consulting_index_unfold']		= ' 【보기】';
$lang['consulting_index_retract']		= ' 【닫기】';