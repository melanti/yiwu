
(function($, angular, _, APP){
  'use strict';

  APP.modules.ctrl.loginCtrl = loginCtrl;

  //loginCtrl
  function loginCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO){
    var vm = this;

    vm._state = {
      id: "",
      password: ""
    }
    vm.exports = {
      init: _init
    }

    _init();
    function _init() {
    };

    vm._submit = function () {

      XHR.POST(API_URL_INFO.SIGN_IN, {id: vm._state.id, password: vm._state.password})
        .then(function (__sucData) {
          console.log('__sucData: ' , __sucData.data.auth);

          //TODO: auth 따라 page분기
          //TODO : 업체관련 권한 없을때 default page 이동
          var redirectURL = '/opr_mng/org/admin/modification?maxage=0';
          if(__sucData.data.auth && (__sucData.data.auth.majorOrganization===2) ){
          }else{
            redirectURL = '/opr_mng/org/admin/confirmation?maxage=0';
          }
          if(__sucData.data.session && (__sucData.data.session.account_type==='shop') ){
            // redirectURL = '/opr_mng/org/store/list';
            if(__sucData.data.auth.minorOrganization===2){
              redirectURL = '/opr_mng/org/store/modification?maxage=0&account_uuid='+(__sucData.data.session.account_uuid)+'&org_id='+(__sucData.data.session.org_id);
            }else if(__sucData.data.auth.minorOrganization<2){
              redirectURL = '/opr_mng/org/store/confirmation?maxage=0&account_uuid='+(__sucData.data.session.account_uuid)+'&org_id='+(__sucData.data.session.org_id);
            }
          }
          window.location.href = window.location.protocol+'//'+window.location.host+'/' + APP.info.gLocale + redirectURL;

        }, function (__errData) {
          console.log('API_URL_INFO.LOGIN __errData: ',__errData);
          if(__errData.data && __errData.data.code ){
            if( __errData.data.code.indexOf('ER_NOT_MATCH') !==-1 ){
              alert('아이디와 패스워드가 일치하지 않습니다.');
            }
          }else{
            alert('로그인을 실패했습니다.');
          }
          // alert( APP.i18n["common.ui.alert.check_id_pw"]);  //'아이디와 비밀번호를 확인해 주세요'
        });
    }

  }
  loginCtrl.$inject = ['$scope', '_',  '$timeout', 'XHR', 'PubSub', 'API_URL_INFO'];
  loginCtrl.link = function(){
    return function (scope, elem, attrs, ctrl, transclude) {
    }
  }


})(window.jQuery, window.angular, window._, window.APP);
