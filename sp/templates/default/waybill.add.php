<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
    <!-- 페이지导航 -->
    <div class="fixed-bar">
        <div class="item-title">
            <h3>송장템플릿</h3>
            <ul class="tab-base">
                <?php   foreach($output['menu'] as $menu) {  if($menu['menu_key'] == $output['menu_key']) { ?>
                <li><a href="JavaScript:void(0);" class="current"><span><?php echo $menu['menu_name'];?></span></a></li>
                <?php }  else { ?>
                <li><a href="<?php echo $menu['menu_url'];?>" ><span><?php echo $menu['menu_name'];?></span></a></li>
                <?php  } }  ?>
            </ul>
        </div>
    </div>
    <div class="fixed-empty"></div>
    <form id="add_form" method="post" action="<?php echo urlAdmin('waybill', 'waybill_save');?>" enctype="multipart/form-data">
        <?php if($output['waybill_info']) { ?>
        <input type="hidden" name="waybill_id" value="<?php echo $output['waybill_info']['waybill_id'];?>">
        <input type="hidden" name="old_waybill_image" value="<?php echo $output['waybill_info']['waybill_image'];?>">
        <?php } ?>
        <table class="table tb-type2">
            <tbody>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_name">모듈이름<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform"><input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_name']:'';?>" name="waybill_name" id="waybill_name" class="txt"></td>
                    <td class="vatop tips">송장 템플릿 이름, 최대10개</td>
                </tr>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_name">택배사<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform">
                        <select name="waybill_express">
                            <?php if(!empty($output['express_list']) && is_array($output['express_list'])) {?>
                            <?php foreach($output['express_list'] as $value) {?>
                            <option value="<?php echo $value['id'];?>|<?php echo $value['e_name'];?>" <?php if($value['selected']) { echo 'selected'; }?> ><?php echo $value['e_name'];?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                    <td class="vatop tips">모듈 사용중인 택배사</td>
                </tr>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_width">넓이<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform"><input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_width']:'';?>" name="waybill_width" id="waybill_width" class="txt"></td>
                    <td class="vatop tips">송장넓이,단위(mm)</td>
                </tr>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_height">높이<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform"><input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_height']:'';?>" name="waybill_height" id="waybill_height" class="txt"></td>
                    <td class="vatop tips">송장높이,단위(mm)</td>
                </tr>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_top">상위이동<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform"><input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_top']:'0';?>" name="waybill_top" id="waybill_top" class="txt"></td>
                    <td class="vatop tips">송장템플릿 상위이동, 단위(mm)</td>
                </tr>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_left">좌측이동<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform"><input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_left']:'0';?>" name="waybill_left" id="waybill_left" class="txt"></td>
                    <td class="vatop tips">송장템플릿 좌측이동, 단위(mm)</td>
                </tr>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_image">템플릿이미지<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform">
                        <span class="type-file-show">
                            <?php if($output['waybill_info']) { ?>
                            <img class="show_image" src="<?php echo ADMIN_TEMPLATES_URL;?>/images/preview.png">
                            <div class="type-file-preview"><img width="500" src="<?php echo $output['waybill_info']['waybill_image_url'];?>"></div>
                            <?php } ?>
                        </span>
                        <span class="type-file-box">
                            <input type='text' name='waybill_image_name' id='waybill_image_name' class='type-file-text' />
                            <input type='button' name='button' id='button1' value='' class='type-file-button' />
                            <input name="waybill_image" type="file" class="type-file-file" id="waybill_image" size="30" hidefocus="true" nc_type="change_seller_center_logo">
                        </span>
                    </td>
                    <td class="vatop tips">스캔된 이미지를 올려주세요, 이미지 사이즈는 반드시 해당 택배사에 맞는 규격으로 올려주세요.</td>
                </tr>
                <tr class="noborder">
                    <td colspan="2" class="required"><label class="validation" for="waybill_image">시동<?php echo $lang['nc_colon'];?></label></td>
                </tr>
                <tr class="noborder">
                    <td class="vatop rowform">
                    <?php
                    if(!empty($output['waybill_info']) && $output['waybill_info']['waybill_usable'] == '1') { 
                        $usable = 1;
                    } else {
                        $usable = 0;
                    }
                    ?>
                    <input id="waybill_usable_1" type="radio" name="waybill_usable" value="1" <?php echo $usable ? 'checked' : '';?>>
                    <label for="waybill_usable_1">예</label>
                    <input id="waybill_usable_0" type="radio" name="waybill_usable" value="0" <?php echo $usable ? '' : 'checked';?>>
                    <label for="waybill_usable_0">아니오</label>
                    <td class="vatop tips">디자인후 프린트 검토후 시동해주세요, 시동후 업체에서 바로 사용할 수 있습니다.</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"><a id="submit" href="javascript:void(0)" class="btn"><span><?php echo $lang['nc_submit'];?></span></a></td>
                </tr>
            </tfoot>
        </table>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#waybill_image").change(function(){
		$("#waybill_image_name").val($(this).val());
	});

    $("#submit").click(function(){
        $("#add_form").submit();
    });
    $('#add_form').validate({
        errorPlacement: function(error, element){
            error.appendTo(element.parents('tr').prev().find('td:first'));
        },
        rules : {
            waybill_name: {
                required : true,
                maxlength : 10
            },
            waybill_width: {
                required : true,
                digits: true 
            },
            waybill_height: {
                required : true,
                digits: true 
            },
            waybill_top: {
                required : true,
                number: true 
            },
            waybill_left: {
                required : true,
                number: true 
            },
            waybill_image: {
                <?php if(!$output['waybill_info']) { ?>
                required : true,
                <?php } ?>
                accept: "jpg|jpeg|png"
            }
        },
        messages : {
            waybill_name: {
                required : "모듈이름을 입력하세요",
                maxlength : "모듈이름은 최대 10자입니다." 
            },
            waybill_width: {
                required : "넓이를 입력하세요",
                digits: "넓이는 반드시 숫자로 입력하세요"
            },
            waybill_height: {
                required : "높이를 입력하세요",
                digits: "높이는 반드시 숫자로 입력하세요"
            },
            waybill_top: {
                required : "상위 이동량을 입력하세요",
                number: "상위 이동량은 반드시 숫자로 입력하세요"
            },
            waybill_left: {
                required : "좌측 이동량을 입력하세요",
                number: "좌측 이동량은 반드시 숫자로 입력하세요"
            },
            waybill_image: {
                <?php if(!$output['waybill_info']) { ?>
                required : '이미지를 올려주세요',
                <?php } ?>
                accept: '이미지 유형이 아닙니다.' 
            }
        }
    });
});
</script>

