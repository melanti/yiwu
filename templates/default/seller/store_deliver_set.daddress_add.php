<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="eject_con">
  <div id="warning" class="alert alert-error"></div>
  <form method="post" action="index.php?act=store_deliver_set&op=daddress_add" id="address_form" target="_parent">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="address_id" value="<?php echo $output['address_info']['address_id'];?>" />
    <dl>
      <dt><i class="required">*</i><?php echo '이름'.$lang['nc_colon'];?></dt>
      <dd>
        <input type="text" class="text" name="seller_name" value="<?php echo $output['address_info']['seller_name'];?>"/>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i><?php echo '소재지'.$lang['nc_colon'];?></dt>
      <dd>
        <div id="region">
          <input type="hidden" value="45055" name="city_id" id="city_id">
          <input type="hidden" name="area_id" id="area_id" value="556" class="area_ids" />
          <input type="hidden" name="area_info" id="area_info" value="상세주소" class="area_names" />
          <p style="margin-bottom:8px;">
          <input type="text" class="text w60" id="postcode1" name="postcode1" value="<?php echo $output['address_info']['postcode1'];?>"> -
          <input type="text" class="text w60" id="postcode2" name="postcode2" value="<?php echo $output['address_info']['postcode2'];?>">
          <input type="button" class="button" onclick="execDaumPostcode()" value="우편번호 찾기">
          </p>
          <p style="line-height:38px;">
          <input type="text" class="text w300" id="roadAddress" name="roadAddress" value="<?php echo $output['address_info']['area_info_road'];?>" readonly="readonly" style="background:#F2F2F2 none;" placeholder="도로명주소">
          </p>

          <p style="line-height:38px;">
          <input type="text" class="text w300" id="jibunAddress" name="jibunAddress" value="<?php echo $output['address_info']['area_info_jibun'];?>" readonly="readonly" style="background:#F2F2F2 none;" placeholder="지번주소">
          </p>

          <p style="line-height:38px;">
          <input type="text" class="text w300" value="<?php echo $output['address_info']['area_info'];?>" id="addressEnglish" name="addressEnglish" readonly="readonly" style="background:#F2F2F2 none;" placeholder="영문주소">
          </p>

          <p style="line-height:38px;">
          <input type="text" class="text w200" id="address" value="<?php echo $output['address_info']['address'];?>" name="address" placeholder="상세주소(숫자만 입력하세요)">
          </p>
        </div>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i><?php echo '전화번호'.$lang['nc_colon'];?></dt>
      <dd>
        <input type="text" class="text" name="telphone" value="<?php echo $output['address_info']['telphone'];?>"/>
      </dd>
    </dl>
    <dl>
      <dt class="required"><?php echo '회사'.$lang['nc_colon'];?></dt>
      <dd>
        <input type="text" class="text" name="company" value="<?php echo $output['address_info']['company'];?>"/>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input type="submit" cnbiztype="address_add_submit" class="submit" value="저장" /></label>
    </div>
  </form>
</div>
<script>
var SITEURL = "<?php echo SHOP_SITE_URL; ?>";
$(document).ready(function(){
	regionInit("region");
	$('input[cnbiztype="address_add_submit" ]').click(function(){
		if ($('#address_form').valid()) {
			if ($('select[class="valid"]').eq(1).val()>0) $('#city_id').val($('select[class="valid"]').eq(1).val());
			ajaxpost('address_form', '', '', 'onerror');
		}
	});
    $('#address_form').validate({
        errorLabelContainer: $('#warning'),
        invalidHandler: function(form, validator) {
           var errors = validator.numberOfInvalids();
           if(errors)
           {
               $('#warning').show();
           }
           else
           {
               $('#warning').hide();
           }
        },
        rules : {
            seller_name : {
                required : true
            },
            area_id : {
                required : true,
                min   : 1,
                checkarea : true
            },
            address : {
                required : true
            },
            telphone : {
                required : true,
                minlength : 6
            }
        },
        messages : {
            seller_name : {
                required : '<i class="icon-exclamation-sign"></i><?php echo $lang['store_daddress_input_receiver'];?>'
            },
            area_id : {
                required : '<i class="icon-exclamation-sign"></i><?php echo $lang['store_daddress_choose_location'];?>',
                min  : '<i class="icon-exclamation-sign"></i><?php echo $lang['store_daddress_choose_location'];?>',
                checkarea  : '<i class="icon-exclamation-sign"></i><?php echo $lang['store_daddress_choose_location'];?>'
            },
            address : {
                required : '<i class="icon-exclamation-sign"></i><?php echo $lang['store_daddress_input_address'];?>'
            },
            telphone : {
                required : '<i class="icon-exclamation-sign"></i>전화번호',
                minlength: '<i class="icon-exclamation-sign"></i>전화번호'
            }
        }
    });

});
</script> 
