<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncsc-flow-layout">
  <div class="ncsc-flow-container">
    <div class="title">
      <h3>환불서비스</h3>
    </div>
    <div id="saleRefund">
      <div class="ncsc-flow-step">
        <dl class="step-first current">
          <dt>구매자환불요청</dt>
          <dd class="bg"></dd>
        </dl>
        <dl class="<?php echo $output['refund']['seller_time'] > 0 ? 'current':'';?>">
          <dt>판매자취소신청</dt>
          <dd class="bg"> </dd>
        </dl>
        <dl class="<?php echo $output['refund']['admin_time'] > 0 ? 'current':'';?>">
          <dt>신청성공,환불완료</dt>
          <dd class="bg"> </dd>
        </dl>
      </div>
    </div>
    <div class="ncsc-form-default">
      <h3>구매자환불신청</h3>
      <dl>
        <dt>환불번호：</dt>
        <dd><?php echo $output['refund']['refund_sn']; ?></dd>
      </dl>
      <dl>
        <dt>신청인（구매자）：</dt>
        <dd><?php echo $output['refund']['buyer_name']; ?></dd>
      </dl>
      <dl>
        <dt>환불원인<?php echo $lang['nc_colon'];?></dt>
        <dd> <?php echo $output['refund']['reason_info_ko']; ?> </dd>
      </dl>
      <dl>
        <dt>환불금액<?php echo $lang['nc_colon'];?></dt>
        <dd><strong class="red"><?php echo number_format($output['refund']['refund_amount_ko']); ?>원</strong></dd>
      </dl>
      <dl>
        <dt>환불TIP：</dt>
        <dd> <?php echo $output['refund']['buyer_message']; ?> </dd>
      </dl>
      <dl>
        <dt>허가증업로드：</dt>
        <dd>
          <?php if (is_array($output['pic_list']) && !empty($output['pic_list'])) { ?>
          <ul class="ncsc-evidence-pic">
            <?php foreach ($output['pic_list'] as $key => $val) { ?>
            <?php if(!empty($val)){ ?>
            <li><a href="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>" cnbiztype="nyroModal" rel="gal" target="_blank"> <img class="show_image" src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>"></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
        </dd>
      </dl>
      <h3>판매자 처리의견</h3>
        <?php
  $refund_s_state_text = array(
      1=>"심사대기",
      2=>"동의",
      3=>"거절"
    );
  $refund_r_state_text = array(
      1=>"처리중",
      2=>"관리자심사중",
      3=>"완료"
    );

          ?>
      <dl>
        <dt>처리상태<?php echo $lang['nc_colon'];?></dt>
        <dd> <?php echo $refund_s_state_text[$output['refund']['seller_state']]; ?> </dd>
      </dl>
      <?php if ($output['refund']['seller_time'] > 0) { ?>
      <dl>
        <dt>비고<?php echo $lang['nc_colon'];?></dt>
        <dd> <?php echo $output['refund']['seller_message']; ?> </dd>
      </dl>
      <?php } ?>
      <?php if ($output['refund']['seller_state'] == 2) { ?>
      <h3>쇼핑몰환불신청</h3>
      <dl>
        <dt>처리상태<?php echo $lang['nc_colon'];?></dt>

        <dd><?php echo $refund_r_state_text[$output['refund']['refund_state']]; ?></dd>
      </dl>
      <?php } ?>
      <?php if ($output['refund']['admin_time'] > 0) { ?>
      <dl>
        <dt>비고<?php echo $lang['nc_colon'];?></dt>
        <dd> <?php echo $output['refund']['admin_message']; ?> </dd>
      </dl>
      <?php } ?>
      <div class="bottom">
        <label class=""><a href="javascript:history.go(-1);" class="ncsc-btn"><i class="icon-reply"></i>리스트로돌아가기</a></label>
      </div>
    </div>
  </div>
  <?php require template('seller/store_refund_right');?>
</div>
