<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>충전카드</h3>
      <ul class="tab-base">
        <li><a href="JavaScript:void(0);" class="current"><span>리스트</span></a></li>
        <li><a href="<?php echo urlAdmin('rechargecard', 'add_card'); ?>"><span>추가</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="get" name="formSearch">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="act" value="rechargecard" />
    <input type="hidden" name="op" value="index" />
    <table class="tb-type1 noborder search">
      <tbody>
        <tr>
          <th><label for="search-sn">충전카드번호</label></th>
          <td><input class="txt" type="text" name="sn" id="search-sn" value="<?php echo $output['sn'];?>" /></td>
          <th><label for="search-batchflag">일괄표식</label></th>
          <td><input class="txt" type="text" name="batchflag" id="search-batchflag" value="<?php echo $output['batchflag'];?>" /></td>
          <th><label for="search-state">수령상태</label></th>
          <td>
            <select name="state" id="search-state">
              <option value="">전체</option>
              <option value="0">미수령</option>
              <option value="1">수령완료</option>
            </select>
            <script>$('#search-state').val('<?php echo $output['state']; ?>');</script>
          </td>
          <td>
            <a href="javascript:document.formSearch.submit();" class="btn-search " title="<?php echo $lang['nc_query'];?>">&nbsp;</a>
<?php if ($output['form_submit'] == 'ok'): ?>
            <a class="btns " href="<?php echo urlAdmin('rechargecard', 'index');?>" title="<?php echo $lang['nc_cancel_search'];?>"><span><?php echo $lang['nc_cancel_search'];?></span></a>
<?php endif; ?>
          </td>
        </tr>
      </tbody>
    </table>
  </form>

  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
          <ul>
            <li>충전카드를 발표하면 회원은 마이페이지에서 해당 카드번호를 입력하여 충전할 수 있습니다.</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>

<div style="text-align: right;"><a class="btns" href="index.php?<?php echo http_build_query($_GET); ?>&op=export_step1" target="_blank"><span>불러내기Excel</span></a></div>

  <form method="post" action="<?php echo urlAdmin('rechargecard', 'del_card_batch');?>" onsubmit="" name="form1">
    <table class="table tb-type2">
      <thead>
        <tr class="thead">
          <th class="w24"> </th>
          <th class=" ">충전카드번호</th>
          <th class=" ">일괄표식</th>
          <th class="w60 align-center">금액(원)</th>
          <th class="w96 align-center">발표관리자</th>
          <th class="w150 align-center">발표시간</th>
          <th class="w270 align-center">수령상태</th>
          <th class="w48 align-center"><?php echo $lang['nc_handle'];?> </th>
        </tr>
      </thead>
<?php if (empty($output['card_list'])): ?>
      <tbody>
        <tr class="no_data">
          <td colspan="20"><?php echo $lang['nc_no_record'];?></td>
        </tr>
      </tbody>
<?php else: ?>
      <tbody>
<?php foreach ($output['card_list'] as $val): ?>
        <tr class="space">
          <td class="w24">
<?php if ($val['state'] == 0): ?>
            <input type="checkbox" class="checkitem" name="ids[]" value="<?php echo $val['id']; ?>" />
<?php else: ?>
            <input type="checkbox" disabled="disabled" />
<?php endif; ?>
          </td>
          <td class=""><?php echo $val['sn']; ?></td>
          <td class=""><?php echo $val['batchflag']; ?></td>
          <td class="align-center"><?php echo number_format($val['denomination_ko']); ?>원</td>
          <td class="align-center"><?php echo $val['admin_name']; ?></td>
          <td class="align-center"><?php echo date('Y-m-d H:i:s', $val['tscreated']); ?></td>
          <td class="align-center">
<?php if ($val['state'] == 1 && $val['member_id'] > 0 && $val['tsused'] > 0): ?>
            회원 <?php echo $val['member_name']; ?>은 <?php echo date('Y-m-d H:i:s', $val['tsused']); ?>에 수령.
<?php else: ?>
            미수령
<?php endif; ?>
          </td>
          <td class="align-center">
<?php if ($val['state'] == 0): ?>
            <a onclick="return confirm('정말 삭제하시겠습니까?');" href="<?php echo urlAdmin('rechargecard', 'del_card', array('id' => $val['id'])); ?>" class="normal"><?php echo $lang['nc_del'];?></a>
<?php endif; ?>
          </td>
        </tr>
<?php endforeach; ?>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td><input type="checkbox" class="checkall" id="checkallBottom"></td>
          <td colspan="16"><label for="checkallBottom"><?php echo $lang['nc_select_all']; ?></label>
            &nbsp;&nbsp;<a href="javascript:void(0);" class="btn" onclick="if ($('.checkitem:checked ').length == 0) { alert('삭제할 항목을 선택하세요!');return false;}  if(confirm('<?php echo $lang['nc_ensure_del'];?>')){document.form1.submit();}"><span><?php echo $lang['nc_del'];?></span></a>
            <div class="pagination"><?php echo $output['show_page'];?></div></td>
        </tr>
      </tfoot>
<?php endif; ?>
    </table>
  </form>
</div>

<script>

</script>
