var wait=180;
function get_sms_captcha(type){
    if($("#phone").val().length == 11 && $("#image_captcha").val().length == 4){
        var ajaxurl = 'index.php?act=sms_login&op=get_captcha&nchash=1&type='+type;
        ajaxurl += '&captcha='+$('#image_captcha').val()+'&phone='+$('#phone').val();
        $.ajax({
            type: "GET",
            url: ajaxurl,
            async: false,
            success: function(rs){
                if(rs == 'true') {
                    wait=180;
                    time();
                } else {
                    showError(rs);
                }
            }
        });
    }
}
function time() {

            if(wait==0)
            {
                $("#sms_text").html("短信动态码已发出 <a href='javascript:void(0);' onclick='get_sms_captcha(2)'><i class='icon-mobile-phone'></i>重新发送</a>");

            }else
            {

                $("#sms_text").html("短信动态码已发出 <a href='###' style='background:gray;'><i class='icon-mobile-phone'></i>重新发送(" + wait + ")</a>");
                wait--;
                setTimeout(function() {
                    time()
                },
                1000)

            }
    }
function check_captcha(){
    if($("#phone").val().length == 11 && $("#sms_captcha").val().length == 6){
    var ajaxurl = 'index.php?act=sms_login&op=check_captcha';
    ajaxurl += '&sms_captcha='+$('#sms_captcha').val()+'&phone='+$('#phone').val();
    $.ajax({
        type: "GET",
        url: ajaxurl,
        async: false,
        success: function(rs){
            if(rs == 'true') {
                $.getScript('index.php?act=sms_login&op=register'+'&phone='+$('#phone').val());
                $("#register_sms_form").show();
                $("#post_form").hide();
            } else {
                showError(rs);
            }
        }
    });
    }
}