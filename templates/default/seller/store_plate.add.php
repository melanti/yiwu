<?php defined( 'InCNBIZ') or exit( 'Access Invalid!');?>
    <script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
    <style type="text/css">
        .ncsc-form-default dl dt { width: 16%;} .ncsc-form-default dl dd { width:82%;}
    </style>
    <div class="tabmenu">
        <?php include template( 'layout/submenu');?>
    </div>
    <div class="ncsc-form-default">
        <form method="post" action="<?php if (empty($output['plate_info'])) { echo urlShop('store_plate', 'plate_add');} else { echo urlShop('store_plate', 'plate_edit');}?>" id="plate_form">
            <input type="hidden" name="form_submit" value="ok" />
            <input type="hidden" name="p_id" value="<?php echo $output['plate_info']['plate_id'];?>"
            />
            <dl>
                <dt><i class="required">*</i>템플릿명<?php echo $lang[ 'nc_colon'];?></dt>
                <dd>
                    <input type="text" class="text w200" name="p_name" value="<?php echo $output['plate_info']['plate_name']?>" id="p_name" />
                    <p class="hint">최대 10자의 명칭을 입력하여 주십시오. 상품 등록/편집 선택시 사용됩니다.</p>
                </dd>
            </dl>
            <dl>
                <dt>
                    <i class="required">*</i>템플릿설정<?php echo $lang[ 'nc_colon'];?></dt>
                <dd id="gcategory">
                    <ul class="ncsc-form-radio-list">
                        <li>
                            <label><input type="radio" name="p_position" id="p_position" value="1" class="radio" <?php if (empty($output[ 'plate_info']) || $output[ 'plate_info']['plate_position']==1 ) {?> checked="checked"<?php }?> />상단</label>
                        </li>
                        <li>
                            <label><input type="radio" name="p_position" id="p_position" value="0" class="radio" <?php if (!empty($output[ 'plate_info']) && $output[ 'plate_info']['plate_position']==0 ) {?> checked="checked"<?php }?> />하단</label>
                        </li>
                    </ul>
                    <p class="hint">
                        관련 템플릿을 페이지 중간에 끼워넣게 됩니다. 상품상세 내용이 상단 혹은 하단에 나타나게 됩니다.
                    </p>
                </dd>
            </dl>
            <dl>
                <dt>
                    <i class="required">*</i>템플릿내용<?php echo $lang[ 'nc_colon'];?></dt>
                <dd>
                    <?php showEditor( 'p_content',$output[ 'plate_info'][ 'plate_content'],'100%', '480px', 'visibility:hidden;', "false",$output[ 'editor_multimedia']);?>
                        <p class="hr8">
                            <a class="ncsc-btn" cnbiztype="show_desc" href="index.php?act=store_album&op=pic_list&item=des"><i class="icon-picture"></i>앨범에서선택</a>
                            <a href="javascript:void(0);" cnbiztype="del_desc" class="ncsc-btn ml5" style="display: none;"><i class=" icon-circle-arrow-up"></i>앨범 닫기</a>
                        </p>
                        <p id="des_demo">
                        </p>
                </dd>
            </dl>
            <div class="bottom">
                <label class="submit-border">
                    <input type="submit" class="submit" value="완료" />
                </label>
            </div>
        </form>
    </div>
    <script>
        $(function() {
            $('#plate_form').validate({
                submitHandler: function(form) {
                    ajaxpost('plate_form', '', '', 'onerror');
                },
                rules: {
                    p_name: {
                        required: true,
                        maxlength: 10
                    },
                    p_content: {
                        required: true
                    }
                },
                messages: {
                    p_name: {
                        required: '<i class="icon-exclamation-sign"></i>템플릿명칭을 입력하여주십시오.',
                        maxlength: '<i class="icon-exclamation-sign"></i>10자를 넘을 수 없습니다.'
                    },
                    p_content: {
                        required: '<i class="icon-exclamation-sign"></i>请填写版式内容'
                    }
                }
            });

            // 版式内容使用
            $('a[cnbiztype="show_desc"]').ajaxContent({
                event: 'click',
                //mouseover
                loaderType: "img",
                loadingMsg: SHOP_TEMPLATES_URL + "/images/loading.gif",
                target: '#des_demo'
            }).click(function() {
                $(this).hide();
                $('a[cnbiztype="del_desc"]').show();
            });
            $('a[cnbiztype="del_desc"]').click(function() {
                $(this).hide();
                $('a[cnbiztype="show_desc"]').show();
                $('#des_demo').html('');
            });
        });
        /* 插入编辑器 */
        function insert_editor(file_path) {
            KE.appendHtml('p_content', '<img src="' + file_path + '">');
        }
    </script>