<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 团购상태 
 */
$lang['groupbuy_state_all'] = '전체 공동구매';
$lang['groupbuy_state_verify'] = '미심사';
$lang['groupbuy_state_cancel'] = '취소됨';
$lang['groupbuy_state_progress'] = '통과됨';
$lang['groupbuy_state_fail'] = '심사실패';
$lang['groupbuy_state_close'] = '마감됨';

/**
 * index
 */
$lang['groupbuy_index_manage']		= '공동구매 관리';
$lang['groupbuy_verify']    		= '심사대기';
$lang['groupbuy_cancel']    		= '취소됨';
$lang['groupbuy_progress']  		= '심사됨';
$lang['groupbuy_close']     		= '마감됨';
$lang['groupbuy_back']     		= '리스트 돌아가기';

$lang['groupbuy_recommend_goods']	= '추천상품';
$lang['groupbuy_template_list']		= '공동구매 프로모션';
$lang['groupbuy_template_add']		= '프로모션 추가';
$lang['groupbuy_template_name']		= '프로모션 이름';
$lang['groupbuy_class_list']		= '공동구매 카테고리';
$lang['groupbuy_class_add']		    = '카테고리 추가';
$lang['groupbuy_class_edit']	    = '카테고리 수정';
$lang['groupbuy_class_name']	    = '카테고리명';
$lang['groupbuy_parent_class']	    = '해당 카테고리';
$lang['groupbuy_root_class']	    = '상위 카테고리';
$lang['groupbuy_area_list'] 		= '공동구매 지역';
$lang['groupbuy_area_add']		    = '지역 추가';
$lang['groupbuy_area_edit'] 	    = '지역 수정';
$lang['groupbuy_area_name'] 	    = '지역 이름';
$lang['groupbuy_parent_area']	    = '해당 지역';
$lang['groupbuy_root_area'] 	    = '1급 지역';
$lang['groupbuy_price_list']		= '공동구매 가격 범위';
$lang['groupbuy_price_add']		    = '가격범위 추가';
$lang['groupbuy_price_edit']	    = '가격범위 수정';
$lang['groupbuy_price_name']	    = '가격범위 이름';
$lang['groupbuy_price_range_start']	    = '가격범위 최대 제한';
$lang['groupbuy_price_range_end']	    = '가격 범위 최소 제한';
$lang['groupbuy_detail'] = '공동구매 정보 상세';
$lang['range_name']	    = '가격범위 이름';
$lang['range_start']	    = '가격 범위 최소 제한';
$lang['range_end']	    = '가격범위 최대 제한';
$lang['groupbuy_index_name']		= '공동구매 이름';
$lang['groupbuy_index_goods_name']	= '상품 이름';
$lang['groupbuy_index_store_name']	= '미니샵 이름';
$lang['start_time']             	= '시작시간';
$lang['end_time']               	= '마감시간';
$lang['join_end_time']             	= '지원 마감시간';
$lang['groupbuy_index_start_time']	= '시작시간';
$lang['groupbuy_index_end_time']	= '마감시간';
$lang['day']						= '일';
$lang['hour']						= '시간';
$lang['groupbuy_index_state']		= '공동구매 상태';
$lang['groupbuy_index_pub_state']	= '발표상태';
$lang['groupbuy_index_click']		= '조회수';
$lang['groupbuy_index_long_group']	= '장기 프로모션';
$lang['groupbuy_index_un_pub']		= '미발표';
$lang['groupbuy_index_canceled']	= '취소됨';
$lang['groupbuy_index_going']		= '진행중';
$lang['groupbuy_index_finished']	= '완료됨';
$lang['groupbuy_index_ended']		= '마감됨';
$lang['groupbuy_index_published']	= '발표됨';
$lang['group_template'] = '공동구매 프로모션';
$lang['group_name'] = '공동구매 이름';
$lang['store_name'] = '미니샵이름';
$lang['goods_name'] = '상품이름';
$lang['group_help'] = '공동구매 설명';
$lang['start_time'] = '시작시간';
$lang['end_time'] = '마감시간';
$lang['goods_price'] = '상품 원가';
$lang['store_price'] = '가격';
$lang['groupbuy_price'] = '공동구매 가격';
$lang['limit_type'] = '제한 유형';
$lang['virtual_quantity'] = 'E-쿠폰 수량';
$lang['min_quantity'] = '그룹수량';
$lang['sale_quantity'] = '제한수량';
$lang['max_num'] = '총 상품수';
$lang['group_intro'] = '본 공동구매 소개';
$lang['group_pic'] = '공동구매 이미지';
$lang['buyer_count'] = '구매수';
$lang['def_quantity'] = '구매된 상품수';
$lang['base_info'] = '기본정보';


/**
 * 페이지说明
 **/
$lang['groupbuy_template_help1'] = '프로모션 관리 버튼을 클릭하여 상세정보를 확인할 수 있습니다, 공동구매에 대한 심사 관리를 진행하세요';
$lang['groupbuy_template_help2'] = '시작이 된지 않은 프로모션는 바로 삭제할 수 있습니다, 삭제된 프로모션내 모든 정보는 삭제됩니다.';
$lang['groupbuy_template_help3'] = '프로모션 시작후 닫기 버튼을 클릭하 본 프로모션를 닫을 수 있습니다.';
$lang['groupbuy_template_help4'] = '공동구매 상품을 메인으로 추천할 수 있습니다, 공동구매 프로모션 관리 페이지의 체크박스를 선택하여 추천할 수 있습니다.';
$lang['groupbuy_template_add_help1'] = '프로모션 시간은 중복이 될 수 없습니다, 새로운 프로모션 시간은 반드시 마감시간보다 작아야됩니다.';
$lang['groupbuy_template_add_help2'] = '지원 마감시간은 시작시간보다 작아야합니다.';
$lang['groupbuy_start_time_explain'] = '공동구매 프로모션 시간은 아래 시간보다 빠를 수 없습니다.';
$lang['groupbuy_class_help1'] = '공동구매 카테고리는 사용자단에 기본적으로 노출됩니다.';
$lang['groupbuy_area_help1'] = '공동구매 지역은 사용자단에 기본적으로 노출됩니다.';
$lang['groupbuy_price_range_help1'] = '프론트 공동구매 가격 필터 범위는 중복될 수 없습니다.';
$lang['groupbuy_index_help1']		= "메뉴에서 '리스트 돌아가기' 링크를 클릭하여 프로모션 리스트로 돌아갑니다.";
$lang['groupbuy_index_help2']		= '공동구매 정보는 심사후 프론트단에 노출됩니다.';
$lang['groupbuy_parent_class_add_tip'] = '등록하실 해당 카테고리를 선택하세요.  ';
$lang['groupbuy_parent_area_add_tip'] = '서브 지역을 선택하세요, 디폴트는 1급 지역입니다.';
$lang['sort_tip'] = '순위는 0~255내 숫자를 입력하세요, 0은 최상단에 노출됩니다.';
$lang['price_range_tip'] = "가격범위 이름을 정확히 입력하세요, 예제: '1000원이하'와 '2000원-3000원'";
$lang['price_range_price_tip'] = '가격은 반드시 정수로 입력하세요';

$lang['groupbuy_recommend_help1'] = '본 페이지에 노출된 공동구매 상품은 심사가 완료된 상품으로 추천 설정만 할 수 있습니다.';

$lang['state_text_notstarted'] = '미시작';
$lang['state_text_in_progress'] = '진행중';
$lang['state_text_closed'] = '닫힘';

/**
 * 团购삭제
 */
$lang['groupbuy_del_choose']		= '삭제할 내용을 선택하세요';
$lang['groupbuy_del_succ']			= '삭제성공';
$lang['groupbuy_del_fail']			= '삭제실패';
/**
 * 团购추천
 */
$lang['groupbuy_recommend_choose']	= '추천할 내용을 선택하세요';
$lang['groupbuy_recommend_succ']	= '추천성공';
$lang['groupbuy_recommend_fail']	= '추천실패';


/**
 * 提示信息
 */
$lang['class_name_error'] = '카테고리명을 입력하세요';
$lang['sort_error'] = '순위는 반드시 숫자로 입력하세요';
$lang['area_name_error'] = '지역 이름을 입력하세요';
$lang['verify_success'] = '심사통과';
$lang['verify_fail'] = '심사실패';
$lang['ensure_verify_success'] = '정말 본 공동구매 프로모션를 심사 통과하시겠습니까?';
$lang['ensure_verify_fail'] = '정말 본 공동구매 프로모션를 심사 실패로 하시겠습니까?';
$lang['op_close'] = '마감';
$lang['ensure_close'] = '정말 본 공동구매 프로모션를 마감하시겠습니까?';
$lang['template_name_error'] = '프로모션 이름을 입력하세요';
$lang['start_time_error'] = '시작시간을 입력하세요';
$lang['end_time_error'] = '마감시간을 입력하세요, 하지만 반드시 시작시간보다 커야됩니다.';
$lang['join_end_time_error'] = '지원 마감 시간을 입력하세요';
$lang['range_name_error'] = '가격범위 이름을 입력하세요';
$lang['range_start_error'] = '가격범위 최대 제한은 반드시 숫자로 입력하세요';
$lang['range_end_error'] = '가격 범위 최소 제한은 반드시 숫자로 입력하세요';
$lang['start_time_overlap'] = '그룹 활동 시간은 중복됩니다, 다른 시간을 선택하세요';
/**
 * 提示信息
 */

$lang['admin_groupbuy_unavailable'] = '공동구매 기능 미시동, 자동으로 시동하겠습니까?';
$lang['groupbuy_template_add_success'] = '공동구매 프로모션 추가 성공';
$lang['groupbuy_template_add_fail'] = '공동구매 프로모션 추가 실패';
$lang['groupbuy_tempalte_drop_success'] = '공동구매 프로모션 삭제 성공';
$lang['groupbuy_template_drop_fail'] = '공동구매 프로모션 삭제 실패';
$lang['groupbuy_tempalte_close_success'] = '공동구매 프로모션 닫기 성공';
$lang['groupbuy_template_close_fail'] = '공동구매 프로모션 닫기 실패';
$lang['groupbuy_verify_success'] = '공동구매 심사 설정 성공';
$lang['groupbuy_verify_fail'] = '공동구매 심사 설정 실패';
$lang['groupbuy_close_success'] = '공동구매 마감 성공';
$lang['groupbuy_close_fail'] = '공동구매 마감 실패';
$lang['groupbuy_class_add_success'] = '공동구매 카테고리 추가 성공';
$lang['groupbuy_class_add_fail'] = '공동구매 카테고리 추가 실패';
$lang['groupbuy_class_edit_success'] = '공동구매 카테고리 수정 성공';
$lang['groupbuy_class_edit_fail'] = '공동구매 카테고리 수정 실패';
$lang['groupbuy_class_drop_success'] = '공동구매 카테고리 삭제 성공';
$lang['groupbuy_class_drop_fail'] = '공동구매 카테고리 삭제 실패';
$lang['groupbuy_area_add_success'] = '공동구매 지역 추가 성공';
$lang['groupbuy_area_add_fail'] = '공동구매 지역 추가 실패';
$lang['groupbuy_area_edit_success'] = '공동구매 지역 수정성공';
$lang['groupbuy_area_edit_fail'] = '공동구매 지역 수정실패';
$lang['groupbuy_area_drop_success'] = '공동구매 지역 삭제성공';
$lang['groupbuy_area_drop_fail'] = '공동구매 지역 삭제실패';
$lang['groupbuy_price_range_add_success'] = '동동구매 가격범위 추가 성공';
$lang['groupbuy_price_range_add_fail'] = '동동구매 가격범위 추가 실패';
$lang['groupbuy_price_range_edit_success'] = '공동구매 가격범위 수정 성공';
$lang['groupbuy_price_range_edit_fail'] = '공동구매 가격범위 수정 실패';
$lang['groupbuy_price_range_drop_success'] = '공동구매 가격범위 삭제 성공';
$lang['groupbuy_price_range_drop_fail'] = '공동구매 가격범위 삭제 실패';

$lang['groupbuy_close_confirm'] = '정말 공동구매 프로모션를  닫으시겠습니까? 한번 닫으시면 다시 열 수 없습니다.';
