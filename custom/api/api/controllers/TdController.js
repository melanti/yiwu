// dependencies external library
var Q = require("q");
var lib_vc = require("./libs/verifyCode.js");
var fs = require('fs');
var unzip = require('unzip');
var is = require("is_js");

// dependencies internal module(and service)
var UUID = require("./libs/uuid.js");
var debug = require("./libs/debug.js");
var SQLService = require('../services/SQLService');
var FileService = require('../services/FileService');
var ErrorHandler = require('../services/ErrorHandler');
var ValidatorService = require('../services/ValidatorService');
var QueryService = require('../services/QueryService');

function _isKids(cate_str){
    return ( (cate_str) && (cate_str.toLowerCase().indexOf('kid')!==-1) )?true:false;
}

process.on('uncaughtException', function (err) {
    //예상치 못한 예외 처리
    sails.log.debug('uncaughtException 발생 : ' + err);
});

/**
 * TdController
 *
 * @description :: Server-side logic for managing 3D Cloth Data
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * list
   */
  set:_uplaod_td,
  list: _list,
  get: _get
};

function _uplaod_td(req, res){

  sails.log.debug( 'td/set allParams: ', req.allParams() );

  // Context Object about Upload TD Tasking ( * Promise(Q) 사용시 dependiency 되는 Context 객체 )
  var UPLOAD_TASKS_MODEL ={
    //Dependency Object
    UPLOAD_TD_QRY:QueryService.UPLOAD_TD_QRY,
    req:req,
    res:res,
    params:req.allParams(),

    //local file upload info
    zip_src:null,
    unzip_uuid:'',
    unzip_path:null,
    upload_req_list:null,
    upload_req_file:null,
    upload_max_bytes:100000000,
    fitting_zip_file:null,
    files_hash:null,

    //goods info
    goods_id:'',
    goods_commonid:'',
    goods_name:'',
    goods_sex:'',
    goods_kids:'',

    //store info
    store_id:'',
    store_name:'',

    //brand info
    brand_id:'',
    brand_name:'',

    //3D info
    cate_no:'',
    representative_yn:'',
    td_name:'',
    td_no:'',
    td_id:'',
    td_cd:'',

    td_reg_result:null //성공시 마지막에 보낼 값
  };

    //promise tasks TEST
   /* Q.fcall(_taskZipFileUpload)
        .spread(_taskMakeDir)
        .spread(_taskUploadFittingFilesInLocal)
        .spread(_taskMakeFilesHash)
        .spread(_taskAllSettled)
        .catch(_taskDefaultError);*/

    //promise tasks
  Q.fcall(_taskValidateParams)                    // 1. parameter 검증
      .spread(_taskCheckVerifyCode)               // 2. verify code 체킹
      .spread(_taskZipFileUpload)                 // 3. zip file upload
      .spread(_taskMakeDir)                       // 4. upzip위한 directory 생성
      .spread(_taskUploadFittingFilesInLocal)     // 5. 생성된 폴더에 unzip
      .spread(_taskMakeFilesHash)                 // 6. unzip한 파일들의 hash 코그 생성 (UPLOAD_TASKS_MODEL.files_hash)
      .spread(_taskCheckMemberAndGetStoreInfo)    // 7. 멤버 체킹 , store 정보 fetch (output: store_id, store_name)
      .spread(_taskChkCateName)                   // 8. cate_no fetch (output: cate_no)
      .spread(_taskInsert3DC)                     // 9. 3d정보 insert (output: TD_NO, td_reg_result)
      .spread(_taskSelect3DC)                     // 10. insert한 3d정보 확인 (output: td_id, td_cd)
      .spread(_taskSelectItemsInfo)               // 11. item, brand, 성별키즈정보 fetch (output: goods_commonid, goods_name, brand_id,brand_name,goods_sex,goods_kids)
      .spread(_taskVadateSex)                       // 12. parameter(make_category_name)의 성별과 item의 성별정보 체킹 ()

      .spread(_taskReplace3DDataQuery)            // 14. replace query 3d file (input: td_id, files_hash)
      .spread(_taskBeforeInsert3DCExpsr)          // 15. before insert 3DC EXPSR (input:item_id)
      .spread(_taskInsert3DCExpsr)                // 16. insert 3DC EXPSR (input: td_id, goods_commonid, goods_name, store_id, store_name, brand_id, brand_name, params.item_id)
      .spread(_taskUploadFiles)                   // 13. uplaod  SFTP
      .spread(_taskRemoveLocalFiles)              // 17. local file remove
      .spread(_taskAllSettled)                    // 18. 완료
      .catch(_taskDefaultError);                  //  default error excute

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //task method START


  function _taskValidateParams(){
    return Q.all([
      ValidatorService.validatePromise(
          {
            data:_.omit(UPLOAD_TASKS_MODEL.params,['fitting_zip_file', 'id']),
            passingData:UPLOAD_TASKS_MODEL,
            validator:[
              {key:'account_uuid' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
              {key:'secret' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
              {key:'verify_code' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
              {key:'item_id' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
              {key:'make_category_name' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
              {key:'td_name' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }}
            ]
          }
      )
    ]);
  }

  function _taskCheckVerifyCode(){
    sails.log.debug( '_taskCheckVerifyCode:::: '  );

    return Q.all([
      lib_vc
          .chkVerifyCodeOnPromise({
            key:UPLOAD_TASKS_MODEL.params.account_uuid,
            verify_code:UPLOAD_TASKS_MODEL.params.verify_code,
            account_uuid:UPLOAD_TASKS_MODEL.params.account_uuid,
            secret:UPLOAD_TASKS_MODEL.params.secret
          })
    ]);
  }

  function _taskZipFileUpload(){
    sails.log.debug( '_taskZipFileUpload:::: '  );
    var deferred = Q.defer();

    UPLOAD_TASKS_MODEL.fitting_zip_file = UPLOAD_TASKS_MODEL.req.file('fitting_zip_file');
    try{

        var tmp_promise = Q.all([
            Q.nbind(UPLOAD_TASKS_MODEL.fitting_zip_file.upload, UPLOAD_TASKS_MODEL.fitting_zip_file)({maxBytes:UPLOAD_TASKS_MODEL.upload_max_bytes})
        ]);


        tmp_promise.spread(function(__results){
            sails.log.debug('_taskZipFileUpload: ',__results);

            if( __results.length === 0 ) {
                deferred.reject({code:'ER_EMPTY_FILE', status:400});
            }else{
                UPLOAD_TASKS_MODEL.zip_src = __results[0].fd;
                UPLOAD_TASKS_MODEL.unzip_uuid = UUID.setUUID();
                UPLOAD_TASKS_MODEL.unzip_path = './.tmp/uploads/'+UPLOAD_TASKS_MODEL.unzip_uuid;
                UPLOAD_TASKS_MODEL.upload_req_list = [];
                UPLOAD_TASKS_MODEL.upload_req_file = [];
                deferred.resolve(UPLOAD_TASKS_MODEL);
            }
        }).catch(function(e){
            sails.log.debug('TdController.js _taskZipFileUpload ER_FILE_UPLOAD_ERR, local file uploading error', e);
            deferred.reject({code:'ER_FILE_UPLOAD_ERR', status:500, message:'local zip file uploading error'});
        });
    }catch(e){
        deferred.reject({code:'ER_FILE_UPLOAD_ERR', status:500});
    }


    return deferred.promise;
  }


  function _taskMakeDir(){
    sails.log.debug( '_taskMakeDir:::: ' ,UPLOAD_TASKS_MODEL.unzip_path );

    return Q.all([
      Q.nbind(fs.mkdir, fs)(UPLOAD_TASKS_MODEL.unzip_path)
    ]);
  }

  function _taskUploadFittingFilesInLocal(){
    sails.log.debug( '_taskUploadFittingFilesInLocal:::: ', arguments  );

    return _uploadFittingFilesInLocal(UPLOAD_TASKS_MODEL);
  }

  function _taskMakeFilesHash(){
    sails.log.debug( '_taskMakeFilesHash:::: '  );

    var deferred = Q.defer();
    var promises = UPLOAD_TASKS_MODEL.upload_req_list.map(function(__fileResource){
      return FileService.computeFileHashPromisify(__fileResource);
    });

    Q.all( promises )
        .spread(function(){
          UPLOAD_TASKS_MODEL.files_hash = arguments;
          deferred.resolve(UPLOAD_TASKS_MODEL);
        })
        .catch(function(e){
          deferred.reject('ER_NETWORK');
        });

    return deferred.promise;
  }

  function _taskCheckMemberAndGetStoreInfo(){
    sails.log.debug( '_taskCheckMember:::: '  );

    var deferred = Q.defer();
    SQLService
        .queryOnPromise(
            UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_STORE_INFO,
            [UPLOAD_TASKS_MODEL.params.account_uuid]
        ).then(function(results){
      if( parseInt(results[0].member_id) !== parseInt(UPLOAD_TASKS_MODEL.params.account_uuid) ){
        deferred.reject(ErrorHandler.NOT_FOUND_DATA.code + ": account_uuid ");
      }else{
        sails.log.debug('_taskCheckMember results: ' , results);
        UPLOAD_TASKS_MODEL.store_id = results[0].store_id;
        UPLOAD_TASKS_MODEL.store_name = results[0].store_name;
        deferred.resolve(UPLOAD_TASKS_MODEL);
      }
    }).catch(function(e){
      deferred.reject('ER_NETWORK_CHECK_MEMBER');
    });
    return deferred.promise;
  }

  function _taskChkCateName(){
    sails.log.debug( '_taskChkCateName:::: '  );

    var deferred = Q.defer();
    SQLService
        .queryOnPromise(
            UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_CAT_NO_FROM_3DC_CATE,
            [UPLOAD_TASKS_MODEL.params.make_category_name]
        ).then(function(results){
      // sails.log.debug('_taskChkCateName results: ' , results);
      if( results.length<1 ){
        deferred.reject(ErrorHandler.NOT_FOUND_DATA.code + ": make_category_name");
      }else{
        UPLOAD_TASKS_MODEL.cate_no = results[0].cate_no;
        deferred.resolve(UPLOAD_TASKS_MODEL);
      }

    }).catch(function(e){
      deferred.reject('ER_NETWORK_CHECK_CATE_NAME');
    });

    return deferred.promise;
  }


  function _taskInsert3DC(){
    sails.log.debug( '_taskInsert3DC:::: '  );

    var deferred = Q.defer();
    SQLService
        .queryOnPromise(
            UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.INSERT_INTO_3DC,
            [
              UPLOAD_TASKS_MODEL.params.td_name, UPLOAD_TASKS_MODEL.cate_no, 'activated',
              (UPLOAD_TASKS_MODEL.params.td_description || ''), UPLOAD_TASKS_MODEL.store_id, UPLOAD_TASKS_MODEL.store_name
            ]
        ).then(function(results){
      sails.log.debug('_taskInsert3DC results: ' , results);
      UPLOAD_TASKS_MODEL.td_no = results.insertId;
      UPLOAD_TASKS_MODEL.td_reg_result = results;
      deferred.resolve(UPLOAD_TASKS_MODEL);

    }).catch(function(e){
      deferred.reject('ER_NETWORK_INSERT_3DC');
    });

    return deferred.promise;
  }

  function _taskSelect3DC(){
    sails.log.debug( '_taskSelect3DC:::: '  );

    var deferred = Q.defer();
    SQLService
        .queryOnPromise(
            UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_TD_ID_TD_CD_FROM_3DC,
            [UPLOAD_TASKS_MODEL.td_no]
        ).then(function(results){
      sails.log.debug('_taskSelect3DC results: ' , results);
      if( results.length<1 ){
        deferred.reject("ERR_NOT_FOUND_TD_DATA");
      }else{
        UPLOAD_TASKS_MODEL.td_id = results[0].td_id;
        UPLOAD_TASKS_MODEL.td_cd = results[0].td_cd;
        deferred.resolve(UPLOAD_TASKS_MODEL);
      }
    }).catch(function(e){
      deferred.reject('ER_NETWORK_INSERT_3DC');
    });
    return deferred.promise;
  }


  function _taskSelectItemsInfo(){
    sails.log.debug( '_taskSelectItemsInfo:::: '  );

    var deferred = Q.defer();
    // UploadTasksModel.UPLOAD_TD_QRY.SELECT_ITEM , UploadTasksModel.params.item_id
    // goods_commonid , goods_name , store_id , store_name, brand_id , brand_name



    SQLService
        .queryOnPromise(
            UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.SELECT_ITEM,
            [UPLOAD_TASKS_MODEL.params.item_id]
        ).then(function(results){
      sails.log.debug('_taskSelect3DC results: ' , results);
      if( results.length<1 ){
        deferred.reject("ERR_NOT_FOUND_ITEMS_DATA");
      }else{
        UPLOAD_TASKS_MODEL.goods_commonid = results[0].goods_commonid;
        UPLOAD_TASKS_MODEL.goods_id = results[0].goods_id;
        UPLOAD_TASKS_MODEL.goods_name = results[0].goods_name;
        UPLOAD_TASKS_MODEL.brand_id = results[0].brand_id;
        UPLOAD_TASKS_MODEL.brand_name = results[0].brand_name;
        UPLOAD_TASKS_MODEL.goods_sex = results[0].goods_sex;
        UPLOAD_TASKS_MODEL.goods_kids = results[0].goods_kids;

        deferred.resolve(UPLOAD_TASKS_MODEL);
      }
    }).catch(function(e){
      deferred.reject('ER_NETWORK_SELECT_ITEMS_DATA');
    });

    return deferred.promise;
  }

  function _taskVadateSex(){
    sails.log.debug( '_taskSelectItemsInfo:::: '  );
    var deferred = Q.defer();
    var chk = true;
    var msg = '';
    /*
     UPLOAD_TASKS_MODEL.goods_sex   //  woman , man , unisex
     UPLOAD_TASKS_MODEL.goods_kids  // 0(어른) ,1(아이)
     UPLOAD_TASKS_MODEL.params.make_category_name   // WonClo , ManClo, Etc
     */

    if( _isKids(UPLOAD_TASKS_MODEL.params.make_category_name) ){
      //make_category_name가 '아동'
      if(parseInt(UPLOAD_TASKS_MODEL.goods_kids)===0){
        //goods_kids 가 어른일때
        chk = false;
        msg = '3D data category is kids, but goods category is adult';
      }

    }else{
      //make_category_name가 '성인'
      if(parseInt(UPLOAD_TASKS_MODEL.goods_kids)===1){
        //goods_kids 가 아동일때
        chk = false;
        msg = '3D data category is adult, but goods category is kids';
      }

      if( String(UPLOAD_TASKS_MODEL.goods_sex).toLowerCase()==='unisex'){
        //goods_sex 가 일때
        chk = false;
        msg = '3D data category is adult, but goods category is unisex';
      }
    }

    if(chk){
      deferred.resolve(UPLOAD_TASKS_MODEL);
    }else{
      deferred.reject({code:'NOT_MATCH_SEX', message:msg});
    }

    return deferred.promise;
  }


  function _taskUploadFiles(){
    sails.log.debug( '_taskUploadFiles:::: '  );
    return Q.all([ FileService.uploadThreedFiles( UPLOAD_TASKS_MODEL ) ]);
  }

  function _taskReplace3DDataQuery(){
    sails.log.debug( '_taskReplace3DDataQuery:::: '  );

    var promises = [];
    for(var i in UPLOAD_TASKS_MODEL.files_hash){
      promises.push(
          SQLService.queryOnPromise(
              UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.REPLACE_3DC_FILE,
              [
                UPLOAD_TASKS_MODEL.td_id ,
                String(UPLOAD_TASKS_MODEL.files_hash[i].source).substr( String(UPLOAD_TASKS_MODEL.files_hash[i].source).lastIndexOf('/')+1 ) ,
                UPLOAD_TASKS_MODEL.files_hash[i].hash
              ]
          )
      );
    }
    return Q.all(promises);
  }

  function _taskBeforeInsert3DCExpsr(){
    sails.log.debug( '_taskBeforeInsert3DCExpsr:::: '  );
    var deferred = Q.defer();

    SQLService
        .queryOnPromise(
            UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.BEFORE_INSERT_INTO_3DC_EXPSR,
            [UPLOAD_TASKS_MODEL.goods_commonid, UPLOAD_TASKS_MODEL.store_id]
        ).then(function(results){
      sails.log.debug( '_taskBeforeInsert3DCFile arguments:::: ', arguments  );
      deferred.resolve(UPLOAD_TASKS_MODEL);
    }).catch(function(e){
      sails.log.debug('_taskBeforeInsert3DCExpsr Error');
      deferred.resolve(UPLOAD_TASKS_MODEL);
    });
    return deferred.promise;
  }

  function _taskInsert3DCExpsr(){
    sails.log.debug( '_taskInsert3DCExpsr:::: '  );
    var deferred = Q.defer();

    SQLService.queryOnPromise(
        UPLOAD_TASKS_MODEL.UPLOAD_TD_QRY.INSERT_INTO_3DC_EXPSR,
        [
          UPLOAD_TASKS_MODEL.td_id ,
          UPLOAD_TASKS_MODEL.goods_id,
          UPLOAD_TASKS_MODEL.goods_commonid,
          UPLOAD_TASKS_MODEL.goods_name,

          UPLOAD_TASKS_MODEL.store_id,
          UPLOAD_TASKS_MODEL.store_name,
          UPLOAD_TASKS_MODEL.brand_id,
          UPLOAD_TASKS_MODEL.brand_name
        ]
    ).then(function(results){
      sails.log.debug( '_taskInsert3DCExpsr arguments:::: ', arguments  );
      deferred.resolve(UPLOAD_TASKS_MODEL);
    }).catch(function(e){
      sails.log.debug('_taskInsert3DCExpsr Error', e);
      deferred.reject('ER_NETWORK_3DC_EXPSR');
    });

    return deferred.promise;
  }

  function _taskRemoveLocalFiles(){
    sails.log.debug('_taskRemoveLocalFiles:: ' );

    for(var i in UPLOAD_TASKS_MODEL.upload_req_list){
      fs.unlinkSync(UPLOAD_TASKS_MODEL.upload_req_list[i]);
    }
    fs.unlinkSync(UPLOAD_TASKS_MODEL.zip_src);
    fs.rmdirSync(UPLOAD_TASKS_MODEL.unzip_path);

    return Q.all([ UPLOAD_TASKS_MODEL ]);
  }

  function _taskAllSettled(){
    sails.log.debug( '_taskAllSettled final results: ' );

      return res.send(
          200,
          debug.wrap(
              UPLOAD_TASKS_MODEL.req,
              UPLOAD_TASKS_MODEL.res,
              200,
              {
                  results:[{
                      // upload_req_file:UPLOAD_TASKS_MODEL.upload_req_file,
                      td_no:UPLOAD_TASKS_MODEL.td_no,
                      td_cd:UPLOAD_TASKS_MODEL.td_cd
                  }]
              }
          )
      );

  }

  function _taskDefaultError(__err){
    sails.log.debug('_taskDefaultError ERROR: ' , __err);
    var err_code;
    var tmpStatus;
    var idx;
    var message;
    var key;
    var resultObj;
    try{
        err_code = __err.code || 'ERR_FAIL';
        tmpStatus = __err.status || 500;
        idx = __err.index || null;
        message = __err.message || null;
        key = (err_code==='ER_BAD_BLANK_ERROR' || err_code==='ER_UNDEFINED_PARAM_ERROR')?__err.key:null;
        resultObj = {code:err_code};

        if(idx) resultObj.index = idx;
        if(key) resultObj.key = key;
        if(message) resultObj.message = message;
        res.status(tmpStatus);
    }catch(e){
        sails.log.debug('_taskDefaultError catch first ERROR: ' , e);
        res.status(500);
    }

    if(UPLOAD_TASKS_MODEL.td_no===''){
      return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), resultObj ));
    }else{
      // insert 한 db data가 있다면 삭제
      try{
          Q.all([
              SQLService.queryOnPromise('delete from sv_3dc where 3dc_no=?', [UPLOAD_TASKS_MODEL.td_no]),
              SQLService.queryOnPromise('delete from sv_3dc_file where 3dc_no=?', [UPLOAD_TASKS_MODEL.td_no]),
              SQLService.queryOnPromise('delete from sv_3dc_exposure where 3dc_no=?', [UPLOAD_TASKS_MODEL.td_no])
          ]).then(function(){
              sails.log.debug('UPLOAD_TASKS_MODEL.td_no(3dc_no): DELETE: ' , UPLOAD_TASKS_MODEL.td_no);
              return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), resultObj ));
          });

      }catch(e){
          sails.log.debug('_taskDefaultError UPLOAD_TASKS_MODEL.td_no DELETE ERROR: ' , e);
          return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), resultObj ));
      }
    }

  }

  //util method
  function _uploadFittingFilesInLocal( __opt ){
    var deferred = Q.defer();
    var readStream;

    try{

        fs.createReadStream(__opt.zip_src)
            .pipe(unzip.Parse())
            .on('entry', function(entry){
                try{

                    switch( true ){
                        case ( entry.path == "package.bin" ):
                        case ( entry.path == "thumbnail.png" ):
                        case ( entry.path == 'ProductInfo.json'):
                        case ( /preview[0-3]?[0-9]?[0-9]\.jpg/.test(entry.path) ):
                            entry.pipe( fs.createWriteStream( __opt.unzip_path+'/'+entry.path ) );
                            __opt.upload_req_list.push(__opt.unzip_path +'/'+ entry.path);
                            __opt.upload_req_file.push(entry.path);
                            break;
                        default:
                            sails.log.debug('default:::');
                            deferred.reject({code:'ER_FILE_UPLOAD_ERR'});
                    }

                }catch(e){
                    sails.log.debug('_uploadFittingFilesInLocal entry catch ERROR: ' , e);
                    deferred.reject({code:'ER_UNDEFINED Error: package.bin is invalid format'});
                }


            })
            .on('close', function(){
                try{
                    readStream = fs.createReadStream(__opt.unzip_path+'/' + "package.bin");
                    readStream.on('data', function(data){

                        readStream.close();
                        if(data.toString('utf8', 0, 2).indexOf('FX') === -1){
                            for(var i in __opt.upload_req_list){
                                fs.unlinkSync(__opt.upload_req_list[i]);
                            }
                            fs.rmdirSync(__opt.unzip_path);
                            fs.unlinkSync(__opt.zip_src);
                            deferred.reject({code:'ER_UNDEFINED Error: package.bin is invalid format'});
                        }else{
                            deferred.resolve(__opt);
                        }

                    });
                }catch(e){
                    sails.log.debug('_uploadFittingFilesInLocal close catch ERROR: ' , e);
                    deferred.reject({code:'ER_UNDEFINED Error: package.bin is invalid format'});
                }
            })
            .on('error', function(){
                sails.log.debug('fs.createReadStream error: ' , arguments);
            });
    }catch(e){
        sails.log.debug('localfileUpload ERror entry catch ERROR: ' , e);
        deferred.reject({code:'ER_FILE_UPLOAD_ERR'});
    }



    return deferred.promise;
  }
  //task method END
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

function _get(req, res){
  var debug, chk_data, account_uuid, td_id, qry, bind, status, key;
  var rtn_val = {};

  debug     = require("./libs/debug.js");
  chk_data  = require("./libs/chkData.js");

  // 요청값 정리
  ParamHandler.paramChecker(req);

  account_uuid  = req.param('account_uuid');
  td_id         = req.param('td_id');
  show          = req.param('show');

  status = 501;

  if( show ){
    show = show.split(',');
    show.forEach( function( obj ){
      obj = obj.trim();
    });
  }else{
    show = [];
  }

  // 필수값 체크
  chk_data.chkParam(req, res, [ account_uuid ], function() {
    bind = [];
    qry = "";
    qry += " select 3dc_no as id ";
    qry += "      , cat_no as cat_id ";
    qry += "      , store_id as org_id ";
    qry += "      , store_name as org_name ";
    qry += "      , 3dc_code as code ";
    qry += "      , 3dc_name as name ";
    qry += "      , 3dc_sex as sex ";
    qry += "      , 3dc_category_no as make_category_db_id ";
    qry += "      , 3dc_category_name as make_category_name ";
    qry += "      , 3dc_category_route as make_category_route ";
    qry += "      , 3dc_description as description ";
    qry += "      , 3dc_status as status ";
    qry += "      , from_unixtime(regist_date) as registerd_datetime ";
    qry += "      , from_unixtime(update_date) as updated_datetime ";
    if( show.indexOf('td_fitting') +1 ){
      qry += "    , file_list as td_fitting_files ";
    }
    qry += "   from sv_3dc_detail ";
    qry += "  where store_id = ( select store_id ";
    qry += "                       from sv_seller ";
    qry += "                      where member_id = ? ) ";
    qry += "    and 3dc_no = ?;";
    bind.push(account_uuid);
    bind.push(td_id);

    DB.query ( qry, bind, function( err, result ){
      if (err) {
        status = 500;
        return res.send( debug.wrap( req, res, status, err ) );
      }

      result.forEach( function(obj){
        if( show.indexOf('td_fitting') +1 ){
          if( obj.td_fitting_files ){
            obj.td_fitting_files = JSON.parse( obj.td_fitting_files );
            for( key in obj.td_fitting_files ){
              obj.td_fitting_files[key].url = sails.config.ftp.domain+"/DAT/"+obj.code+"/"+obj.td_fitting_files[key].url;
            }
          }
        }
      });

      rtn_val.results = result;

      status = 200;
      return res.send( debug.wrap( req, res, status, rtn_val ) );
    });
  });
}

function _list(req, res) {
  var debug, chk_data,
  account_uuid,  start, count,
  td_name, td_code, org_id,
  cat_id, td_sex, td_status,
  make_cate_id, make_cate_name,
  period_reg_start, period_reg_end,
  reqeust_from, order, order_type, show,
  qry, sqry, fqry, wqry, oqry, lqry,
  bind, err, status, key, item_uuid, td_ids;

  debug    = require("./libs/debug.js");
  chk_data = require("./libs/chkData.js");

  // 요청값 정리
  ParamHandler.paramChecker(req);

  account_uuid     = req.param('account_uuid');
  start            = req.param('start');
  count            = req.param('count');
  item_uuid        = req.param('item_uuid');
  td_ids           = req.param('td_ids');
  td_name          = req.param('td_name');
  td_code          = req.param('td_code');
  org_id           = req.param('org_id');
  cat_id           = req.param('cat_id');
  td_sex           = req.param('td_sex');
  td_status        = req.param('td_status');
  make_cate_id     = req.param('make_category_db_id');
  make_cate_name   = req.param('make_category_name');
  period_reg_start = ParamHandler.checkTimeFormatISO( req.param('period_reg_start') );
  period_reg_end   = ParamHandler.checkTimeFormatISO( req.param('period_reg_end') );
  reqeust_from     = req.param('from');
  order            = req.param('order');
  order_type       = req.param('order_type');
  show             = req.param('show');

  err = {
    "code": "ER_WRONG_VALUE",
    "index": 0
  };

  if( period_reg_start === false || period_reg_end === false ){
    status = 400;
    err.code = "ER_WRONG_DATETIME_FORMAT";
    return res.send(  debug.wrap( req, res, status, err ) );
  }

  status = 501;

  if( show ){
    show = show.split(',');
    show.forEach( function( obj ){
      obj = obj.trim();
    });
  }else{
    show = [];
  }

  if( td_status ){
    td_status = td_status.split(',');
    td_status.forEach( function( obj, idx, arr ){
      arr[idx] = "'"+obj.trim()+"'";
    });
    td_status = td_status.toString();
  }else{
    td_status = null;
  }

  // 필수값 체크
  chk_data.chkParam(req, res, [ account_uuid ], function() {
    bind = [];
    sqry = "";
    fqry = "";
    wqry = "";
    oqry = "";
    lqry = "";

    sqry += " select 3dc_no as id ";
    sqry += "      , cat_no as cat_id ";
    sqry += "      , store_id as org_id ";
    sqry += "      , store_name as store_name ";
    sqry += "      , 3dc_code as code ";
    sqry += "      , 3dc_name as name ";
    sqry += "      , 3dc_sex as sex ";
    sqry += "      , 3dc_category_no as make_category_db_id ";
    sqry += "      , 3dc_category_name as make_category_name ";
    sqry += "      , 3dc_category_route as make_category_route ";
    sqry += "      , 3dc_description as description ";
    sqry += "      , 3dc_status as status ";
    sqry += "      , from_unixtime(regist_date) as registerd_datetime ";
    sqry += "      , from_unixtime(update_date) as updated_datetime ";
    if( show.indexOf('td_fitting') +1 ){
      sqry += "    , file_list as td_fitting_files ";
    }
    fqry += "   from  sv_3dc_detail ";
    wqry += "  where  store_id = ( select store_id  ";
    wqry += "                        from sv_seller  ";
    wqry += "                       where member_id = ? ) ";


    bind.push(account_uuid);

    var iqry = "";
    if( item_uuid ){
      iqry += " find_in_set (?, item_code_arr) ";
      bind.push( item_uuid );
    }

    if( td_ids ){
      if( iqry ) iqry += " or ";
      iqry += " find_in_set (3dc_no, ?) ";
      bind.push( td_ids );
    }

    if( td_name ){
      if( iqry ) iqry += " or ";
      iqry += " 3dc_name like ? ";
      bind.push( "%"+td_name+"%" );
    }

    if( td_code ){
      if( iqry ) iqry += " or ";
      iqry += " 3dc_code like ? ";
      bind.push( "%"+td_code+"%" );
    }

    if( iqry ){
      wqry += " and ("+iqry+") ";
    }

    if( org_id ){
      wqry += "  and  store_id = ? ";
      bind.push( org_id );
    }

    if( cat_id ){
      wqry += "  and  cat_no = ? ";
      bind.push( cat_id );
    }

    if( td_sex ){
      var td_sex_arr = td_sex.split(',');

      wqry += "  and  3dc_sex in ( ";
      for (var i = 0, length = td_sex_arr.length; i < length; i++) {
        if(i === length - 1) {
          wqry += '?';
        } else {
          wqry += '?,';
        }

        bind.push(td_sex_arr[i]);
      }
      wqry += " )";
      //bind.push( td_sex );
    }

    if( td_status ){
      wqry += "  and  3dc_status in ( "+td_status+" )";
    }

    if( make_cate_id ){
      wqry += "  and ( find_in_set( ?, 3dc_category_no_arr) or find_in_set( ?, parent_3dc_category_arr ) ) ";
      bind.push( make_cate_id );
      bind.push( make_cate_id );
    }

    if( make_cate_name ){
      wqry += "  and 3dc_category_name like ? ";
      bind.push( "%"+make_cate_name+"%" );
    }

    // 상품 등록 기간으로 검색
    if( period_reg_start && period_reg_end ){
      wqry += " and regist_date between ? and ?";
      bind.push( period_reg_start );
      bind.push( period_reg_end );
    }else{
      if( period_reg_start ){
        wqry += " and regist_date >= ?";
        bind.push( period_reg_start );
      }
      if( period_reg_end ){
        wqry += " and regist_date <= ?";
        bind.push( period_reg_end );
      }
    }

    // 검색 결과 정렬 조건
    if(order){
      switch( order ){
        case "registerd_date":
          oqry = " order by regist_date ";
          break;
        case "name":
          oqry = " order by 3dc_name ";
          break;
        default:
          status = 400;
          var aerr = {
            "code": "ER_WRONG_ORDER_VALUE",
            "index": 0
          };
          return res.send(  debug.wrap( req, res, status, aerr ) );
      }
    }

    if( order && order_type ){
      if( order_type.toLowerCase() == "desc" ){
        oqry += " desc ";
      }
    }

    // 검색 결과 출력 조건
    if( start && count ){
      lqry = " limit "+ start;
      lqry += ", "+count+";";
    }else{
      if( count ){
        lqry = " limit "+count+";";
      }
    }


    // 출력 형식 정의
    var rtn_val = {
      info: {
        start: start || 0,
        count: count || 0,
        total: 0
         /*sex: {
         man: 0,
         woman: 0,
         undefined: 0
         }*/
      },
      results: []
    };


    // 상품 검색
    qry = sqry + fqry + wqry + oqry + lqry;

    DB.query ( qry, bind, function( err, result ){
      if (err) {
        status = 500;
        return res.send( debug.wrap( req, res, status, err ) );
      }

      rtn_val.info.total = result.length;

      result.forEach( function(obj){
        if( show.indexOf('td_fitting') +1 ){
          if( obj.td_fitting_files ){
            obj.td_fitting_files = JSON.parse( obj.td_fitting_files );
            for( key in obj.td_fitting_files ){
              obj.td_fitting_files[key].url = sails.config.ftp.domain+"/DAT/"+obj.code+"/"+obj.td_fitting_files[key].url;
            }
          }
        }
      });

      rtn_val.results = result;
      status = 200;
      return res.send( debug.wrap( req, res, status, rtn_val ) );
    });
  });
}