<?php
defined('InCNBIZ') or exit('Access Invalid!');
$lang['microshop_not_install'] = '您没有安装微商城模块';

$lang['microshop_member'] = '회원';
$lang['microshop_channel'] = '채널';
$lang['microshop_commend'] = '추천';
$lang['microshop_text_id'] = '번호';

$lang['microshop_class_name'] = '카테고리명';
$lang['microshop_parent_class'] = '해당 카테고리';
$lang['microshop_class_image'] = '카테고리 이미지';
$lang['microshop_class_keyword'] = '카테고리 키워드';

$lang['microshop_goods_class_binding'] = '연동';
$lang['microshop_goods_class_binding_select'] = '카테고리 선택';
$lang['microshop_goods_class_binded'] = '연동';
$lang['goods_relation_save_success'] = '카테고리 연동 성공';
$lang['goods_relation_save_fail'] = '카테고리 연동 실패';
$lang['microshop_goods_class_default'] = '기본으로';

//카테고리表单
$lang['class_parent_id_error'] = '상위번호분류실패';
$lang['class_name_error'] = '카테고리명은 비워두실 수 없으며 10자이하로 작성하여 주시기 바랍니다';
$lang['class_name_required'] = '카테고리명은 비워두실 수 없습니다.';
$lang['class_name_maxlength'] = '카테고리명은 최대{0}자이내';
$lang['class_keyword_maxlength'] = '카테고리 키워드는 최대{0}자이내';
$lang['class_keyword_explain'] = 'TIP: "키워드1","키워드2","키워드3" / 쉼표(,)로 키워드 간격 띄움 / "키워드명"';
$lang['class_sort_explain'] = '0~255 사이의 정수 입력, 올림차순';
$lang['class_sort_error'] = '0~255 사이의 정수 입력';
$lang['class_sort_required'] = '순서는 비워두실 수 없습니다.';
$lang['class_sort_digits'] = '숫자로 정렬 하십시오';
$lang['class_sort_max'] = '순서 최대{0}';
$lang['class_sort_min'] = '순서 최저0}';
$lang['class_add_success'] = '분류 저장';
$lang['class_add_fail'] = '카테고리 저장 실패';
$lang['class_drop_success'] = '카테고리 삭제 성공';
$lang['class_drop_fail'] = '카테고리 삭제 실패';
$lang['microshop_sort_error'] = '순서는 0~144까지의숫자 이내';

//微商城관리
$lang['microshop_isuse'] = '위몰 사용여부 ';
$lang['microshop_isuse_explain'] = 'CLOSE 선택시 위몰 방문이 제한됩니다.';
$lang['microshop_url'] = '위몰주소';
$lang['microshop_url_explain'] = '만약 위몰이 두번째도메인 이름으로 구성되었으면 여기에 입력후 위몰은 두번째 도메인으로 연결되며 비어있을시 기본 주소로 함';
$lang['microshop_style'] = '위몰주제';
$lang['microshop_style_explain'] = '위몰주제，기본default';
$lang['microshop_header_image'] = '위몰 헤드 이미지';
$lang['microshop_personal_limit'] = '개인쇼 수량제한';
$lang['microshop_personal_limit_explain'] = '회원 개인쇼 등록 횟수(게시 수량) 제한, "0"일 경우 제한없음을 뜻합니다.';
$lang['taobao_api_isuse'] = '타오바오 인터페이스 사용여부';
$lang['taobao_api_isuse_explain'] = 'OPEN 후 정확한 인터페이스를 입력한 해주세요. 개인쇼 등록 시 타오바오와 티엔마오로 구매 연결 됩니다.';
$lang['taobao_app_key'] = '타오바오 응용 프로그램 표지';
$lang['taobao_app_key_explain'] = '온라인 신청하기';
$lang['taobao_secret_key'] = '타오바오 응용 프로그램 키';
$lang['microshop_seo_keywords'] = '위몰SEO키워드';
$lang['microshop_seo_description'] = '위몰SEO설명';

//随心看
$lang['microshop_goods_name'] = '상품이름';
$lang['microshop_goods_image'] = '상품이미지';
$lang['microshop_commend_time'] = '추천시간';
$lang['microshop_commend_message'] = '추천설명';
$lang['microshop_goods_tip1'] = '리스트에 나타나는 게시글 순서를 변경하실 수 있습니다. (올림차순 형식의 순서배열)';
$lang['microshop_goods_tip2'] = '해당 상품에 "추천"을 지정하면 위몰 메인화면에 추천상품으로 보여집니다.';
$lang['microshop_goods_class_tip1'] = '리스트에 나타나는 게시글 순서를 변경하실 수 있습니다. (올림차순 형식의 순서배열)';
$lang['microshop_goods_class_tip2'] = '해당 상품에 "추천"을 지정하면 위몰 메인화면에 추천상품으로 보여집니다.';
$lang['microshop_goods_class_tip3'] = '좌측의 "+"기호를 선택하시면 하위 카테고리를 펼쳐보실 수 있습니다.';
$lang['microshop_goods_class_tip4'] = '하위 카테고리 우측 메뉴의 "연동"을 클릭하시면 위몰&쇼핑몰의 카테고리가 연동되며, 이 후 랜덤으로 추천된 상품은 자동으로 분류됩니다.';
$lang['microshop_goods_class_tip5'] = '하위 카테고리 우측 메뉴의 "기본으로 "를 클릭하시면 위몰 기본 분류로 설정 되며, "랜덤보기" 상품 등록시 연동되지 않은 카테고리는 모두 해당 기본 카테고리로 설정됩니다.';
$lang['microshop_goods_class_binding_tip1'] = '아래부분몰분류선택후 잠금완성 클릭시 잠금후추천된상품은 자동으로 분류됨';
$lang['microshop_goods_class_binding_tip2'] = '마우스를 잠금된 분류로 이동후 우쪽위 의 "x"클릭시 자금삭제가능';

$lang['microshop_personal_tip1'] = '리스트에 나타나는 게시글 순서를 변경하실 수 있습니다. (올림차순 형식의 순서배열)';
$lang['microshop_personal_tip2'] = '해당 상품에 "추천"을 지정하면 위몰 메인화면에 추천상품으로 보여집니다.';

//업체
$lang['microshop_store_add_confirm'] = '미니샵를 미니샵스트리트에 추가하시겠습니까?';
$lang['microshop_store_goods_count'] = '상품수';
$lang['microshop_store_credit'] = '판매신용';
$lang['microshop_store_praise_rate'] = '호평률';
$lang['microshop_store_add'] = '추가됨';
$lang['microshop_store_tip1'] = '리스트에 나타나는 미니샵 순서를 변경하실 수 있습니다. (올림차순 형식의 순서배열)';
$lang['microshop_store_tip2'] = '해당 미니샵에 "추천"을 지정하면 위몰 메인화면에 추천미니샵로 보여집니다. (최대 15개)';
$lang['microshop_store_add_tip1'] = '추가버튼 클릭시 미니샵스트리트에 추가됨';

//댓글
$lang['microshop_comment_id'] = '댓글번호';
$lang['microshop_comment_object_id'] = '상대번호';
$lang['microshop_comment_message'] = '댓글내용';
$lang['microshop_comment_tip1'] = '"삭제" 버튼을 클릭하여 해당 댓글을 삭제하세요';

//广告
$lang['microshop_adv_type'] = '광고유형';
$lang['microshop_adv_name'] = '광고이름';
$lang['microshop_adv_image'] = '광고이미지';
$lang['microshop_adv_url'] = '광고LINK';
$lang['microshop_adv_type_index'] = '메인슬라이드';
$lang['microshop_adv_type_store_list'] = '미니샵 리스트 페이지 슬라이드';
$lang['microshop_adv_image_error'] = '광고이미지를 비워두실수 없습니다.';
$lang['microshop_adv_tip1'] = '리스트에 나타나는 광고 순서를 변경하실 수 있습니다. (올림차순 형식의 순서배열)';
$lang['microshop_adv_type_explain'] = '해당광고위치 설정';
$lang['microshop_adv_image_explain'] = '메인광고이미지 추천사이즈700px*280px，미니샵리스트페이지 광고이미지 추천사이즈1000px*250px';
$lang['microshop_adv_url_explain'] = '해당광고링크주소';


