<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<style>
.bill-alert-block {
    padding-bottom: 14px;
    padding-top: 14px;
}
.bill_alert {
    background-color: #F9FAFC;
    border: 1px solid #F1F1F1;
    margin-bottom: 20px;
    padding: 8px 35px 8px 14px;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	line-height:30px;
}
</style>
  <div class="bill_alert bill-alert-block mt10">
    <div style="width:800px"><h3 style="float:left">정산관리</h3><div style="float:right;">
    <?php if ($output['bill_info']['ob_state'] == BILL_STATE_CREATE){?>
    <a class="ncsc-btn mt5" onclick="ajax_get_confirm('승인하시게 되면 다시 되돌릴 수 없으며 시스템에서 자동으로 입금처리가 진행됩니다. 명세서가 정확히 입력되어 있으시면 확인을 클릭해 주세요.', 'index.php?act=store_bill&op=confirm_bill&ob_no=<?php echo $_GET['ob_no'];?>');" href="javascript:void(0)">해당 월 정산 "확인"</a>
    <?php } elseif ($output['bill_info']['ob_state'] == BILL_STATE_SUCCESS) {?>
    <a class="ncsc-btn mt5" target="_blank" href="index.php?act=store_bill&op=bill_print&ob_no=<?php echo $_GET['ob_no'];?>">정산정보 프린트</a>
    <?php } ?>
    </div>
    <div style="clear:both"></div>
    </div>
    <ul>
      <li>정산번호：<?php echo $output['bill_info']['ob_no'];?>&emsp;
      <?php echo date('Y-m-d',$output['bill_info']['ob_start_date']);?> &nbsp;부터&nbsp; <?php echo date('Y-m-d',$output['bill_info']['ob_end_date']);?></li>
      <li>생성날자：<?php echo date('Y-m-d',$output['bill_info']['ob_create_date']);?></li>
      <li><?php echo number_format($output['bill_info']['ob_order_totals']);?> (주문금액) - <?php echo number_format($output['bill_info']['ob_commis_totals']);?> (수수료) - <?php echo number_format($output['bill_info']['ob_order_return_totals']);?> (환불금액) + <?php echo number_format($output['bill_info']['ob_commis_return_totals']);?> (환불수수료) - <?php echo number_format($output['bill_info']['ob_store_cost_totals']);?> (프로모션비용)</li>
      <li>정산상태：<?php echo billStateKo($output['bill_info']['ob_state']);?>
      <?php if ($output['bill_info']['ob_state'] == BILL_STATE_SUCCESS){?>
      	，정산날자：<?php echo date('Y-m-d',$output['bill_info']['ob_pay_date']);?>
      <?php }?> 
      </li>
    </ul>
  </div>
  <div class="tabmenu">
  	<?php include template('layout/submenu');?>
  </div>
<?php include template('seller/'.$output['sub_tpl_name']);?>
<link type="text/css" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL."/js/jquery-ui/themes/ui-lightness/jquery.ui.css";?>"/>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8" ></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('#query_start_date').datepicker();
	$('#query_end_date').datepicker();
});
</script>