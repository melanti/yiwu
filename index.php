<?php
define('APP_ID','shop');
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));
if (!@include(dirname(__FILE__).'/global.php')) exit('global.php isn\'t exists!');
if (!@include(dirname(__FILE__).'/api/sms/api.class.php')) exit('sms.php isn\'t exists!');
if (!@include(BASE_PATH.'/control/control.php')) exit('control.php isn\'t exists!');
if (!@include(BASE_CORE_PATH.'/cnbiz.php')) exit('cnbiz.php isn\'t exists!');
define('APP_SITE_URL',SHOP_SITE_URL);
define('TPL_NAME',TPL_SHOP_NAME);
define('SHOP_RESOURCE_SITE_URL',SHOP_SITE_URL.DS.'resource');
define('SHOP_TEMPLATES_URL',SHOP_SITE_URL.'/templates/'.TPL_NAME);
define('BASE_TPL_PATH',BASE_PATH.'/templates/'.TPL_NAME);

$bankCode = array(
				'011'=>'농협은행',
				'004'=>'국민은행',
				'088'=>'신한은행',
				'020'=>'우리은행',
				'003'=>'기업은행',
				'081'=>'하나은행',
				'031'=>'대구은행',
				'032'=>'부산은행',
				'071'=>'우체국',
				'023'=>'스탠다드차타드은행',
				'005'=>'외환은행',
				'034'=>'광주은행'
				);

Base::run();
?>
