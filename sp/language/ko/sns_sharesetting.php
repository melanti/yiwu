<?php
defined('InCNBIZ') or exit('Access Invalid!');
$lang['shareset_list_tip'] 		= '시동후 외부 sns,weibo등에 상품/업체를 공유할 수 있습니다.';
$lang['shareset_list_appname'] 		= '응용이름';
$lang['shareset_list_appurl'] 		= '사이트';
$lang['shareset_list_appstate'] 	= '응용상태';
$lang['shareset_list_closeprompt'] 	= '정말 본 api를 닫으시겠습니까?';


$lang['shareset_edit_title'] 		= '포트설정';
$lang['shareset_edit_appisuse'] 	= '시동여부';
$lang['shareset_edit_appcode'] 		= '도메인 인증정보';
$lang['shareset_edit_appid'] 		= '응용표시';
$lang['shareset_edit_applylike'] 		= '온라인 신청';
$lang['shareset_edit_appkey'] 		= '응용키';