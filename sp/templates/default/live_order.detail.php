<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <table class="table tb-type2 order">
    <tbody>
      <tr class="space">
        <th colspan="15">주문상태</th>
      </tr>
      <tr>
        <td colspan="2"><ul>
            <li>
				<strong>주문번호:</strong><?php echo $output['live_order']['order_sn'];?>
            </li>
            <li>
				<strong>주문상태:</strong>
				<?php 
					if($output['live_order']['state'] == 1){
						echo '결제대기';
					}elseif($output['live_order']['state'] == 2){
						echo '교환사용';
					}elseif($output['live_order']['state'] == 3){
						echo '사용완료';
					}elseif($output['live_order']['state'] == 4){
						echo '주문취소';
					}
				?>
			</li>
            <li>
				<strong>주문 총금액:</strong>
				<span class="red_common"><?php echo $lang['currency'].$output['live_order']['price'];?> </span>
			</li>
          </ul></td>
      </tr>
      <tr class="space">
        <th colspan="2">주문상세</th>
      </tr>
      <tr>
        <th>주문정보</th>
      </tr>
      <tr>
        <td>
			<ul>
				<li>
					<strong>구매자<?php echo $lang['nc_colon'];?></strong>
					<?php echo $output['live_order']['member_name'];?>
				</li>
				<li>
					<strong>업체<?php echo $lang['nc_colon'];?></strong>
					<?php echo $output['live_order']['store_name'];?>
				</li>
				<li>
					<strong>지불방식<?php echo $lang['nc_colon'];?></strong>
					<?php echo orderPaymentName($output['live_order']['payment_code']);?>
				</li>
				<li>
					<strong>주문시간<?php echo $lang['nc_colon'];?></strong>
					<?php echo date('Y-m-d H:i:s',$output['live_order']['add_time']);?>
				</li>
				<?php if(intval($output['live_order']['payment_time'])){?>
				<li>
					<strong>결제시간<?php echo $lang['nc_colon'];?></strong>
					<?php echo date('Y-m-d H:i:s',$output['live_order']['payment_time']);?>
				</li>
				<?php }?>
				<?php if(intval($output['live_order']['finnshed_time'])){?>
				<li>
					<strong>완료시간<?php echo $lang['nc_colon'];?></strong>
					<?php echo date('Y-m-d H:i:s',$output['live_order']['finnshed_time']);?>
				</li>
				<?php }?>
			  </ul>
		  </td>
      </tr>
      <tr>
        <th>공동구매정보</th>
      </tr>
      <tr>
        <td>
		  <table class="table tb-type2 goods ">
            <tbody>
              <tr>
                <th></th>
                <th>공동구매정보</th>
                <th class="align-center">단가</th>
                <th class="align-center">수량</th>
                <th class="align-center">총금액</th>
              </tr>
              <tr>
                <td class="w60 picture">
					<div class="size-56x56"><span class="thumb size-56x56"><i></i><a href="<?php echo SHOP_SITE_URL;?>/index.php?act=show_live_groupbuy&op=groupbuy_detail&groupbuy_id=<?php echo $output['live_order']['groupbuy_id'];?>" target="_blank"><img src="<?php echo lgthumb($output['live_order']['groupbuy_pic'], 'small');?>" style=" max-width: 56px; max-height: 56px;" /> </a></span></div>
				</td>
                <td class="w50pre">
					<p><a href="<?php echo SHOP_SITE_URL;?>/index.php?act=show_live_groupbuy&op=groupbuy_detail&groupbuy_id=<?php echo $output['live_order']['groupbuy_id'];?>" target="_blank"><?php echo $output['live_order']['item_name'];?></a></p>
				</td>
				<td class="w96 align-center"><span class="red_common"><?php echo $lang['currency'].$output['live_order']['groupbuy_price'];?></span></td>
                <td class="w96 align-center"><span class="red_common"><?php echo $lang['currency'].$output['live_order']['number'];?></span></td>
                <td class="w96 align-center"><span class="red_common"><?php echo $output['live_order']['price'];?></span></td>
              </tr>
            </tbody>
          </table>
		 </td>
      </tr>
	  <?php if(is_array($output['live_order_pwd']) and !empty($output['live_order_pwd'])) { ?>
	  <tr>
		<th>공동구매码</th>
	  </tr>
	  <?php foreach($output['live_order_pwd'] as $val) { ?>
	  <tr>
	  	<td>
			<?php
				if($val['state']==1){
					echo $val['order_pwd'].',미사용';
				}elseif($val['state']==2){
					echo $val['order_pwd'].',사용완료,사용시간: '.date("Y-m-d H:i",$val['use_time']);
				}
			?>
	  	</td>
	  </tr>
	  <?php } ?>
	  <?php } ?>

    </tbody>
    <tfoot>
      <tr class="tfoot">
        <td><a href="JavaScript:void(0);" class="btn" onclick="history.go(-1)"><span><?php echo $lang['nc_back'];?></span></a></td>
      </tr>
    </tfoot>
  </table>
</div>
