<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>업체통계</h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  
  <form method="get" action="index.php" name="formSearch" id="formSearch">
    <input type="hidden" name="act" value="stat_store" />
    <input type="hidden" name="op" value="storearea" />
    <div class="w100pre" style="width: 100%;">
        <table class="tb-type1 noborder search left">
          <tbody>
            <tr>
            	<td>
                  	<select name="search_sclass" id="search_sclass" class="querySelect">
                  		<option value="" selected >업체카테고리</option>
                  	    <?php foreach ($output['store_class'] as $k=>$v){ ?>
                  		<option value="<?php echo $v['sc_id'];?>" <?php echo $_REQUEST['search_sclass'] == $v['sc_id']?'selected':''; ?>><?php echo $v['sc_name'];?></option>
                  		<?php } ?>
                    </select>
                </td>
              <td>
              	<input class="txt date" type="text" value="<?php echo ($t = $_GET['search_time'])?@date('Y-m-d',$t):'';?>" id="search_time" name="search_time">
              </td>
              <td><a href="javascript:void(0);" id="ncsubmit" class="btn-search tooltip" title="<?php echo $lang['nc_query'];?>">&nbsp;</a></td>
            </tr>
          </tbody>
        </table>
        <span class="right" style="margin:12px 0px 6px 4px;">
        	
        </span>
    </div>
  </form>
  
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th class="nobg" colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
    </tbody>
  </table>
  
	<table class="table tb-type2">
		<thead class="thead">
			<tr class="space">
				<th colspan="15">업체거주지분석</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<!-- 地图容器 -->
                    <div id="container_storenum" class="close_float" style="height:600px; width:90%; margin: 0 auto;">
                    	<div class="stat-map-color">상&nbsp;&nbsp;<span style="background-color: #fd0b07;">&nbsp;</span><span style="background-color: #ff9191;">&nbsp;</span><span style="background-color: #f7ba17;">&nbsp;</span><span style="background-color: #fef406;">&nbsp;</span><span style="background-color: #25aae2;">&nbsp;</span>&nbsp;&nbsp;하
                    	<p>알림: 높은순으로 3명씩 한단계, 4등까지 1~12,나머지 부분은 전체 5등으로 부여됩니다.</p></div>
                    </div>
				
				</td>
			</tr>
		</tbody>
	</table>
	
	<!-- 통계리스트 -->
	<table class="table tb-type2">
    	<thead class="thead">
    		<tr>
    			<th class="align-center">번호</th>
    			<th class="align-center">성</th>
    			<th class="align-center">수량</th>
    			<th class="align-center">조작</th>
    		</tr>
    	</thead>
    	<tbody>
    	<?php if(!empty($output['statlist'])){ ?>
    		<?php foreach($output['statlist'] as $k=>$v){?>
    		<tr>
    			<td class="align-center"><?php echo $v['sort'];?></td>
    			<td class="align-center"><?php echo $v['provincename'];?></td>
    			<td class="align-center"><?php echo $v['storenum'];?></td>
    			<td class="align-center"><a href="<?php echo $output['actionurl']."&provid={$v['province_id']}";?>">보기</a></td>
    		</tr>
    		<?php } ?>
    	<?php } else { ?>
        	<tr class="no_data">
            	<td colspan="3"><?php echo $lang['no_record']; ?></td>
            </tr>
    	<?php }?>
    	</tbody>
    </table>
  </div>
</div>
  <link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
  <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
  <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script>
  <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/highcharts/highcharts.js"></script>
  <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/statistics.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/map/jquery.vector-map.css"/>
  <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/map/jquery.vector-map.js"></script>
  <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/map/china-zh.js"></script>
  
<script>
$(function () {
	$('#search_time').datepicker({dateFormat: 'yy-mm-dd'});
	$('#ncsubmit').click(function(){
    	$('#formSearch').submit();
    });
    //地图
	getMap(<?php echo $output['stat_json']; ?>,'container_storenum'); 
});
</script>