(function($, angular, _, APP){
  'use strict';

  angular.module('App')
    .directive( 'lnbComp', lnbDirc );


  function lnbDirc(PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: false,
      scope: {
      },
      controller: lnbCtrls,
      controllerAs: 'LnbCtrls',
      bindToController: {
        accountType:'@'
      },
      link: function ($scope, elem, attrs, ctrl, transclude) {
        console.log( 'accountType: ',$scope.LnbCtrls.accountType );
      },
      template:[
        '<div ng-repeat="li in LnbCtrls.lnbData.children track by $index" ng-if="!li.isHide" >',
          '<h5 ng-bind="li.name"></h5>',
          '<a ng-repeat="chli in li.children track by $index" ng-if="!chli.isHide" active-nav="info" href="{{chli.link}}" class="list-group-item" ng-class="(chli.id===LnbCtrls.lnbCurId)?\'active\':\'\'" ng-bind="chli.name" ></a>',
        '</div>'
      ].join('')
    };
  }
  lnbDirc.$inject = ['PubSub', 'URL_INFO'];

  function lnbCtrls($scope){
    var vm = this;
    vm.lnbDepth;
    vm.lnbCurDepth;
    vm.lnbCurId;
    vm.lnbData;
    vm.accountType;

    _init();

    function _init(){

      vm.lnbCurDepth = parseInt(APP.MENU_DATA.cur_lnb_idx,10);
      vm.lnbData = APP.MENU_DATA.menu[vm.lnbCurDepth];
      vm.lnbCurId = APP.MENU_DATA.cur_lnb_id;

      console.log( 'LNB init:  ', vm.lnbCurDepth );
      console.log( 'LNB init:  ', vm.lnbData );

    }

  }
  lnbCtrls.$inject = ['$scope'];


})(window.jQuery, window.angular, window._, window.APP);

