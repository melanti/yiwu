<?php
/**
 * cms首页
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');
class indexControl extends mobileHomeControl{

    public function __construct() {
        parent::__construct();
    }

    /**
     * 首页
     */
    public function indexOp() {
        $model_mb_special = Model('mb_special'); 
        $data = $model_mb_special->getMbSpecialIndex();
        $this->_output_special($data, $_GET['type']);
    }

    /**
     * 모바일 광고정보 리턴
     */
    public function adv_listOp() {
    	
    	$model_mb_special = Model('mb_special');
    	$data = $model_mb_special->getMbSpecialAdvData();
    	$this->_output_special($data, $_GET['type']);
    	
    	//output_data(array("hot_info"=>array("name"=>"韩国直购门户平台")));
    }   
    
    
    /**
     * search
     */
    public function search_hot_infoOp() {

            output_data(array("hot_info"=>array("name"=>"韩国直购门户平台")));
    }

    /**
     * hot search
     */
    public function search_key_listOp() {
			$hotsearch = @explode(',',C('hot_search'));
            output_data(array("list"=>$hotsearch,'his_list'=>array()));
    }

				
    /**
     * search adv
     */
    public function search_advOp() {
		$data['area_list'] = array(
				array("area_id"=>"0",'area_name'=>"全国"),
		);

		$data['contract_list'] = array(
				array("id"=>"1",'name'=>"7天退货"),
				array("id"=>"2",'name'=>"服务保障")
		);

            output_data($data);
    }


    /**
     * 专题
     */
    public function specialOp() {
        $model_mb_special = Model('mb_special'); 
        $data['list'] = $model_mb_special->getMbSpecialItemUsableListByID($_GET['special_id']);
        $data['special_id'] = $_GET['special_id'];
        $data['special_desc'] = "special";

        $this->_output_special($data, $_GET['type'], $_GET['special_id']);
    }

    /**
     * 输出专题
     */
    private function _output_special($data, $type = 'json', $special_id = 0) {
        $model_special = Model('mb_special');
        if($_GET['type'] == 'html') {
            $html_path = $model_special->getMbSpecialHtmlPath($special_id);
            if(!is_file($html_path)) {
                ob_start();
                Tpl::output('list', $data);
                Tpl::showpage('mb_special');
                file_put_contents($html_path, ob_get_clean());
            }
            header('Location: ' . $model_special->getMbSpecialHtmlUrl($special_id));
            die;
        } else {
            output_data($data);
        }
    }

    /**
     * android客户端版本号
     */
    public function apk_versionOp() {
		$version = C('mobile_apk_version');
		$url = C('mobile_apk');
        if(empty($version)) {
           $version = '';
        }
        if(empty($url)) {
            $url = '';
        }

        output_data(array('version' => $version, 'url' => $url));
    }
}
