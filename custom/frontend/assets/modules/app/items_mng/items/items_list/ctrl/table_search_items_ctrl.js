
(function($, angular, _, APP){

  APP.modules.ctrl.tableSearchItemsCtrl = tableSearchItemsCtrl;

  //tableComponentCtrl
  function tableSearchItemsCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO , BrandTableManager, OperationTableManager, CategoryManager , COMMON_UTIL){
    var vm = this,
        _dateList = COMMON_UTIL.getCompareDateList(),
        _watchTimer,
        _watchTimerDateOpt,
        _isLoadBrand = false,
        _isLoadShop = false,
        _stDat = COMMON_UTIL.getTodayDateInTimezone(),
        _endDat = COMMON_UTIL.getTodayDateInTimezone();

    vm.parseInt = window.parseInt;
    vm.userData = window.APP.info.user;
    vm.options;


    vm.query='';
    vm.query_opt='item_name';
    vm.brand_id_arr=[];
    vm.org_id_arr=[];
      vm.item_name='';
      vm.item_uuid='';
      vm.origin_item_id='';
      vm.td_name='';
      vm.td_code='';
      vm.org_id='';
      vm.brand_id='';
      vm.sale_type=2;


    vm.item_cate_arr_01=[];
    vm.item_cate_arr_02=[];
    vm.item_cate_id_01='';
    vm.item_cate_id_02='';
      vm.item_cate_id;

      vm.item_status=''  // ''(all), ready, activated, disabled, deleted
      vm.td_reg_flag='all';  // 'all' , registerd, false

      vm.include_cate_family_flag = true;  // true, false
      vm.include_cate_unassigned_flag = true;  //  true, false


    vm.stDate = _stDat;
    vm.endDate = _endDat;
    vm.stDateOptions = {
      changeYear:false,
      changeMonth:false,
      yearRange:'1900:-0',
      dateFormat:'yy-mm-dd',
      onClose:_onCloseStDateOpt
    };
    vm.endDateOptions = {
      changeYear:false,
      changeMonth:false,
      yearRange:'1900:-0',
      dateFormat:'yy-mm-dd',
      onClose:_onCloseEndDateOpt
    };
    vm.period ='period_reg';  // period_reg , period_upd
    vm.period_unit ='all';  // today, 3days, week, 1month, 3month, 1year, all, custom
      // ( YYYY-MM-DD HH:II:SS )

      vm.period_reg_start='';
      vm.period_reg_end='';
      vm.period_upd_start='';
      vm.period_upd_end='';

    vm.getTotal;
    vm.getStart;
    vm.getCount;
    vm.getResults;
    vm.setSearchOpt;
    vm.updatePage;

    vm.changePeriod = _changePeriod;
    vm.updateList = _updateList;
    vm.onChangeCateDepth01 = _onChangeCateDepth01;
    vm.onChangeQueryOpt = _onChangeQueryOpt;
    vm.onChangePeriodType = _onChangePeriodType;




    _init();

    function _init(){
      if(vm.userData.account_type==='admin'){}else{
        _isLoadShop = true;
        vm.org_id = vm.userData.org_id;
        vm.org_id_arr = [{org_id:vm.userData.org_id, org_name:vm.userData.org_name}];
      }

      _setupOptData(_getSearchOptByURL());
      vm.setSearchOpt({options:_getCurSearchOptData()});

      PubSub.subscribe('ItemsSearchOpt:change',_onChangeTableData);
      $scope.$watch( 'tableSearchCtrl.period_unit', _watchPeriodUnit);
      $scope.$watch( 'tableSearchCtrl.query_opt', _watchQueryOpt);

      _watchTimer = $timeout(function(){
        _updatePeriodStEndValue( vm.period, $('#st_date').val(), $('#end_date').val() );
      }, 300);

      _getCateList();
      //console.log('tableSearchCtrl init');
    }


    ////////////////////////////////////////////////////////////////////////////////
    // watch/evt callback
    function _getSearchOptByURL(){
      //console.log('_changeLocationState:');
      var qIdx = window.location.href.indexOf('?'), tmpObj;
      if(qIdx===-1){
      }else{
        tmpObj = COMMON_UTIL.convertHashToObject(window.location.href.substr( qIdx+1 ));
      }
      return tmpObj;
    }


    function _setupOptData(__opt){
      if(__opt){
        if(__opt.brand_id){
          vm.query_opt = 'brand_id';
          vm.brand_id = String(__opt.brand_id);
          _getBrandList();
        }
        if(__opt.item_cate_id_01){
          vm.item_cate_id_01 = __opt.item_cate_id_01;
          if(__opt.item_cate_id_02){
            vm.item_cate_id_02 = __opt.item_cate_id_02;
          }
          vm.include_cate_unassigned_flag = false;
        }

      }
    }


    function _onChangeTableData(topic, data){
      var tmpPick;

      _resetSearthCate(data);

      vm.item_cate_status = data.options.item_cate_status;
      vm.include_cate_family_flag = data.options.include_cate_family_flag;
      vm.include_cate_unassigned_flag = data.options.include_cate_unassigned_flag;
      vm.item_status = data.options.item_status;
      vm.td_reg_flag = data.options.td_reg_flag;
      vm.sale_type = data.options.sale_type;

      vm.item_cate_id_01 = data.options.item_cate_id_01;
      vm.item_cate_id_02 = data.options.item_cate_id_02;

      if(data.options.item_cate_id_02 !==''){
        tmpPick = _.find(vm.item_cate_arr_01, function(val){
          return String(val.id)===String(data.options.item_cate_id_01);
        });
        if(tmpPick && tmpPick.children &&tmpPick.children.length>0){
          vm.item_cate_arr_02 = tmpPick.children;
        }
      }

      vm.period = data.options.period;
      vm.period_unit = data.options.period_unit;
      vm.period_reg_start = data.options.period_reg_start;
      vm.period_reg_end = data.options.period_reg_end;
      vm.period_upd_start = data.options.period_upd_start;
      vm.period_upd_end = data.options.period_upd_end;

      if(vm.period_unit=='all'){
      }else{
        if(vm.period==='period_reg'){
          if(vm.period_reg_start !='') vm.stDate = data.options.stDate;
          if(vm.period_reg_end !='') vm.endDate = data.options.endDate;

        }else if(vm.period==='period_upd'){
          if(vm.period_upd_start !='') vm.stDate = data.options.stDate;
          if(vm.period_upd_end !='') vm.endDate = data.options.endDate;
        }
      }

    }


    function _watchPeriodUnit(__newVal , __oldVal){

      vm.period_reg_start='';
      vm.period_reg_end='';
      vm.period_upd_start='';
      vm.period_upd_end='';

      if(__newVal==='custom'){
        _watchTimer = $timeout(function(){
          _updatePeriodStEndValue( vm.period, $('#st_date').val(), $('#end_date').val() );
          $scope.$apply();
        }, 300);
      }else{
        COMMON_UTIL.convertToDateByPeriodUnit(__newVal , vm.stDateOptions , vm.endDateOptions);
        _watchTimer = $timeout(function(){
          $('#end_date').val(vm.endDateOptions.setDate);
          _updatePeriodStEndValue( vm.period, $('#st_date').val(), vm.endDateOptions.setDate );
          $scope.$apply();
        }, 300);
      }

    }


    function _watchQueryOpt(__newVal , __oldVal){
      if(__newVal===__oldVal){
      }else{
        _resetQueryData(__newVal);
      }
    }



    ////////////////////////////////////////////////////////////////////////////////
    // external method
    function _changePeriod(__period_unit){
      //console.log('_changePeriod: ' , __period_unit);
      vm.period_unit =__period_unit;
    }


    function _updateList(){
      //console.log('update List');
      //TODO
      //setup된 data를 모아서 'tableComponentItemsCtrl'(table_component_items_ctrl.js)의 vm을 setup한다.
      vm.setSearchOpt({options:_getCurSearchOptData()});
      vm.updatePage({options:{start:0, count:vm.getCount()}});
    }


    function _onChangeCateDepth01(){
      if(vm.item_cate_id_01==null){
        vm.item_cate_arr_02=[];
        vm.item_cate_id_02 = '';
      }else{
        var tmpCateDtp2 = _.filter( vm.item_cate_arr_01, function(val){
          return parseInt(val.id,10) === parseInt(vm.item_cate_id_01,10);
        } )[0];

        if(tmpCateDtp2 && tmpCateDtp2.children && tmpCateDtp2.children.length>0){
          vm.item_cate_arr_02 = tmpCateDtp2.children;
        }else{
          vm.item_cate_arr_02=[];
        }
        vm.item_cate_id_02 = '';
      }
    }


    function _onChangeQueryOpt(){
      if(vm.query_opt=='brand_id'){
        vm.query='';
        if(vm.brand_id_arr.length<1){
          _getBrandList();
        }
      }else if(vm.query_opt=='org_id'){
        vm.query='';
        if(vm.org_id_arr.length<1){
          _getShopList();
        }
      }else{

      }
    }


    function _onChangePeriodType(){
      COMMON_UTIL.cancelTimeout(_watchTimer);
      _watchTimer = $timeout(function(){
        _updatePeriodStEndValue( vm.period, $('#st_date').val(), $('#end_date').val() );
      }, 300);
    }



    ////////////////////////////////////////////////////////////////////////////////
    // internal method
    function _updatePeriodStEndValue(__type, __stDate , __endDate){
      // console.log('_updatePeriodStEndValue __stDate: ' , __stDate , __endDate);

      if(__type==='period_reg'){
        vm.period_reg_start = __stDate;
        vm.period_reg_end = __endDate;
        vm.period_upd_start='';
        vm.period_upd_end='';
      }else{
        vm.period_reg_start='';
        vm.period_reg_end='';
        vm.period_upd_start = __stDate;
        vm.period_upd_end = __endDate;
      }
    }


    function _getBrandList(){
      //TODO
      //API change
      if(_isLoadBrand) return false;
      BrandTableManager.exports
        .updateTableData({start:0, count:1000, query_opt:'name', query:'', status:'all'})
        .then(
          function(__sucData){
            _isLoadBrand = true;
            if(__sucData.results && __sucData.results.length>0){
              //vm.brand_id_arr = __sucData.results;
              vm.brand_id_arr = _.map(__sucData.results, function(v, k){
                return {
                  id: String(v.id),
                  uuid: v.uuid,
                  name: v.name,
                  account_id: v.account_id,
                  status: v.status,
                  reg_date: v.reg_date,
                  item_count:v.item_count
                };
              });

            }

          },
          function(__errData){
            _isLoadBrand = true;
            alert( '브랜드 정보를 가져오지 못했습니다' ); //'브랜드 정보를 가져오지 못했습니다'
            console.log('tableComponentCtrl _getBrandList ERROR: ', __errData);
          }
        );
    }


    function _getShopList(){
      //TODO
      //API change
      if(_isLoadShop) return false;
      if(vm.userData.account_type==='admin'){

        OperationTableManager.exports
          .getOrgList()
          .then(
            function(__sucData){
              _isLoadShop = true;
              if(__sucData.results && __sucData.results.length>0) vm.org_id_arr = __sucData.results;

            },
            function(__errData){
              _isLoadShop = true;
              alert( '입점업체 정보를 가져오지 못했습니다' );  //'입점업체 정보를 가져오지 못했습니다'
              console.log('tableComponentCtrl _getShopList ERROR: ', __errData);
            }
          );
      }else{
      }
    }


    function _getCateList(){
      CategoryManager.exports
        .getCateList()
        .then(
          function(__sucData){
            if(__sucData.results && __sucData.results.length>0) {

              vm.item_cate_arr_01 = _.map(__sucData.results, function(v, k){
                var o = {
                  id: String(v.id),
                  item_count: v.item_count,
                  item_sort: v.item_sort,
                  name: v.name,
                  priority: v.priority,
                  status:v.status
                };

                if(v.children && v.children.length>0){
                  o.children = _.map(v.children, function(childValue, childKey){
                    return {
                      id: String(childValue.id),
                      item_count: childValue.item_count,
                      item_sort: childValue.item_sort,
                      name: childValue.name,
                      priority: childValue.priority,
                      status:childValue.status
                    }
                  });
                }
                return o;
              });

            }
            vm.item_cate_id_01='';

            var hasCateArr= _getSearchOptByURL();
            if(hasCateArr && hasCateArr.item_cate_id_01){
              vm.item_cate_id_01=hasCateArr.item_cate_id_01;
              var tmpCateDtp2 = _.filter( vm.item_cate_arr_01, function(val){
                return parseInt(val.id,10) === parseInt(vm.item_cate_id_01,10);
              } )[0];

              if(tmpCateDtp2 && tmpCateDtp2.children && tmpCateDtp2.children.length>0){
                vm.item_cate_arr_02 = tmpCateDtp2.children;
              }
            }
            if(hasCateArr && hasCateArr.item_cate_id_02){
              vm.item_cate_id_02=hasCateArr.item_cate_id_02;
            }


          },
          function(__errData){
            alert(APP.i18n["cate.alert.cant_get_cate_info"]);
            console.log('tableComponentCtrl _getCateList ERROR: ', __errData);
          }
        );

    }


    function _resetQueryData(__queryOpt){
      vm.query='';
      vm.item_name='';
      vm.item_uuid='';
      vm.origin_item_id='';
      vm.td_name='';
      vm.td_code='';
      vm.org_id='';
      vm.brand_id='';
      vm.sale_type=2;

    }


    function _getCurSearchOptData(){
      if(vm.query_opt=='brand_id' || vm.query_opt=='org_id'){}else{
        vm[vm.query_opt] = (vm.query);
      }

      return {
        from:'admin',
        query_opt:vm.query_opt,
        period:vm.period,
        period_unit:vm.period_unit,

        item_name:vm.item_name,
        item_uuid:vm.item_uuid,
        origin_item_id:vm.origin_item_id,
        td_name:vm.td_name,
        td_code:vm.td_code,
        org_id:vm.org_id,
        org_name:'',
        brand_id:vm.brand_id,
        sale_type:vm.sale_type,
        item_cate_id:(vm.item_cate_id_02=='')?vm.item_cate_id_01:vm.item_cate_id_02,
        item_cate_id_01:vm.item_cate_id_01,
        item_cate_id_02:vm.item_cate_id_02,
        include_cate_family_flag:vm.include_cate_family_flag,
        include_cate_unassigned_flag:vm.include_cate_unassigned_flag,
        td_reg_flag:vm.td_reg_flag,
        item_status:vm.item_status,
        stDate:vm.stDate,
        endDate:vm.endDate,
        period_reg_start:(vm.period_unit=='all')?'':vm.period_reg_start,
        period_reg_end:(vm.period_unit=='all')?'':vm.period_reg_end,
        period_upd_start:(vm.period_unit=='all')?'':vm.period_upd_start,
        period_upd_end:(vm.period_unit=='all')?'':vm.period_upd_end,
        order:'registerd_date',
        order_type:'desc'
      };
    }


    function _resetSearthCate(data){
      //상단 검색결과 setup
      vm.query='';
      vm.query_opt=data.options.query_opt;

      $timeout(function(){
        switch(vm.query_opt){
          case 'item_name':
            vm.query=data.options.item_name;
            break;
          case 'item_uuid':
            vm.query=data.options.item_uuid;
            break;
          case 'origin_item_id':
            vm.query=data.options.origin_item_id;
            break;
          case 'td_name':
            vm.query=data.options.td_name;
            break;
          case 'td_code':
            vm.query=data.options.td_code;
            break;
          case 'org_id':
            vm.org_id=data.options.org_id;
            _getShopList();
            break;
          case 'brand_id':
            vm.brand_id=String(data.options.brand_id);
            _getBrandList();
            break;
        }
      }, 200);

    }



    ////////////////////////////////////////////////////////////////////////////////
    // internal method (datepicker method)
    function _onCloseStDateOpt(__d){
      //console.log('stDateOptions: ' , __d);
      COMMON_UTIL.cancelTimeout(_watchTimerDateOpt);
      _watchTimerDateOpt = $timeout(function(){
        _changeStDateUpdate(__d, $('#end_date').val());
        $scope.$apply();
      }, 300);
    }


    function _onCloseEndDateOpt(__d){
      //console.log('endDateOptions: ' , __d);
      COMMON_UTIL.cancelTimeout(_watchTimerDateOpt);
      _watchTimerDateOpt = $timeout(function(){
        _changeStDateUpdate($('#st_date').val(), __d);
        $scope.$apply();
      }, 300);
    }


    function _changeStDateUpdate(__std, __end){
      var chkObjSt = COMMON_UTIL.compareList(_dateList,'dateStr',__std);
      var chkObjEnd = (_dateList[0].dateStr==__end)?true:false;
      if(chkObjSt && chkObjEnd){
        vm.period_unit = chkObjSt.period_unit;
      }else{
        vm.period_unit='custom';
        if(vm.period =='period_reg'){
          vm.period_reg_start=$('#st_date').val();
          vm.period_reg_end=$('#end_date').val();
        }else if(vm.period =='period_upd'){
          vm.period_upd_start=$('#st_date').val();
          vm.period_upd_end=$('#end_date').val();
        }
      }
    }



  }

  tableSearchItemsCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'BrandTableManager', 'OperationTableManager', 'CategoryManager', 'COMMON_UTIL'];
  tableSearchItemsCtrl.bindToController = {
    getTotal:'&',
    getCount:'&',
    getStart:'&',
    setSearchOpt:'&',
    updatePage:'&'
  };
  tableSearchItemsCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableSearchCtrl:true});
    }
  }


})(window.jQuery, window.angular, window._, window.APP);
