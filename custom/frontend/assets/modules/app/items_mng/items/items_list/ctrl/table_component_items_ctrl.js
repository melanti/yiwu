
(function($, angular, _, APP){

  APP.modules.ctrl.tableComponentItemsCtrl = tableComponentItemsCtrl;

  //tableComponentItemsCtrl
  function tableComponentItemsCtrl($scope, $location, _, $timeout, XHR, PubSub, ItemsTableManager, API_URL_INFO, COMMON_UTIL){
    var vm = this,
        itemsTableManager = ItemsTableManager.exports,
        pubsubBootstrap,
        btComplete;

    vm.bootstrapCtrl;
    vm._state={
      tableData:itemsTableManager.getTableData(),
      bootstrapTableCtrl:{
        complete:{
          //tableOptionCtrl:false,
          //tableBodyCtrl:false,
          //paginationCtrl:false,
        }
      }
    };

    vm.exports={
      COUNT_RANGE:itemsTableManager.COUNT_RANGE,
      editItems:_editItems,
      onCloseEditPopupWindow:_onCloseEditPopupWindow,
      setSearchOpt:_setSearchOpt,
      deleteRow:_deleteRow,
      updatePage:_updatePage
    };

    _init();

    function _init(){
      window.closeEditPopupWindow = _onCloseEditPopupWindow;

      (vm.bootstrapCtrl).split(',').forEach(function(element, index, array){
        vm._state.bootstrapTableCtrl.complete[element] =false;
      });

      btComplete = vm._state.bootstrapTableCtrl.complete;
      itemsTableManager.init();

      $scope.$on('$destroy', function () {
        PubSub.unsubscribe(pubsubBootstrap);
      });
      pubsubBootstrap = PubSub.subscribe('tableComponent:bootstrap', _listenBootstrapState);

    }


    function _listenBootstrapState(topic, data){
      var chk = true;
      btComplete = _.extend({}, btComplete, data);

      _.values(btComplete).forEach(function(element, index, array){
        if(element===false) chk =false;
      });

      if( chk ){
        //console.log('All bootstrap');
        PubSub.unsubscribe(pubsubBootstrap);
        PubSub.trigger('tableComponent:bootstrap:complete', {success:true});
        _updatePage();
      }
    }

    function _deleteRow(){
      var opt = {items_ids:''},
        strArr = _getCheckedListRow('id'),
        strShopNameArr;

      if(strArr.length<1){
        alert( '상품을 선택해주세요.' ); //'상품을 선택해주세요.'
        return false;
      }

      opt.items_ids = strArr.join(',');
      strShopNameArr = '선택한\n'+_getCheckedListRowName().join(',')+"\n을(를) 삭제하시겠습니까?";
      // strShopNameArr = (APP.i18n["oper.partner.alert.is_delete_shop"]).replace('%s' , '\n'+_getCheckedListRowName().join(',')+'\n')

      if (confirm(strShopNameArr) == true){
        //console.log('items_ids: ' , opt.items_ids);

        itemsTableManager
          .deleteRow(opt)
          .then(
            function(__sucData){
              //console.log('_deleteRow: ', __sucData);
              alert( '삭제하였습니다.' );  //삭제하였습니다.
              _updatePage({start:0, count:vm._state.tableData.count});
            },
            function(__errData){
              console.log('tableComponentBasicCtrl _deleteRow: ', __errData);
              alert( '삭제가 정상적으로 되지 않았습니다.' );  //"삭제가 정상적으로 되지 않았습니다."
            }
          );
      }else{
        return false;
      }
    }

    function _editItems(__itemsData){

      var tmpURL = APP.info.gURL+'/items_mng/items/modification?maxage=0';
      var $tmpFrm = $('<form id="tmpForm" name="tmpForm" action="'+tmpURL+'" method="post" target="popup_window"></form>');
      $tmpFrm.append('<input type="hidden" id="id" name="id" value="'+(__itemsData.id)+'">');
      $tmpFrm.append('<input type="hidden" id="code" name="code" value="'+(__itemsData.code)+'">');
      $tmpFrm.append('<input type="hidden" id="origin_item_id" name="origin_item_id" value="'+(__itemsData.origin_item_id)+'">');
      $tmpFrm.append('<input type="hidden" id="brand_id" name="brand_id" value="'+(__itemsData.brand_id)+'">');
      $tmpFrm.append('<input type="hidden" id="display_yn" name="display_yn" value="'+(__itemsData.display_yn)+'">');
      $tmpFrm.append('<input type="hidden" id="updated_datetime" name="updated_datetime" value="'+(__itemsData.updated_datetime)+'">');

      var tmpWid = $(window).width();
      var tmpHeight = $(window).height();
      $(document.body).append($tmpFrm);
      window.open("", "popup_window", "width="+(tmpWid)+", height="+(tmpHeight)+", scrollbars=yes");
      $tmpFrm.submit();
      $tmpFrm.remove();
    }

    function _onCloseEditPopupWindow(){
      console.log('//////_onCloseEditPopupWindow');
      _updatePage();
    }

    function _setSearchOpt(__options){
      //console.log('tableComponentItemsCtrl _setSearchOpt: ' , __options);
      _.each(__options , function(value, key, list){
        vm._state.tableData.options[key] = value;
      });
    }

    function _updatePage(__options){
      //console.log('_updatePage: ');

      var tmpPage = (__options&&__options.start>=0)?__options.start:itemsTableManager.getTableStateInfo('start');
      var tmpViewRange = (__options&&__options.count)?__options.count:itemsTableManager.getTableStateInfo('count');
      var opts = itemsTableManager.getTableData().options;
      var reqData = _.extend({}, {start:tmpPage, count:tmpViewRange}, opts);
      var stD, endD;


      if(reqData.period_unit=='custom'){
        if(reqData.period=='period_reg'){
          endD = new Date(reqData.period_reg_end.replace(/-/g, "/")).getTime();
          stD = new Date(reqData.period_reg_start.replace(/-/g, "/")).getTime();
        }else if(reqData.period=='period_upd'){
          endD = new Date(reqData.period_upd_end.replace(/-/g, "/")).getTime();
          stD = new Date(reqData.period_upd_start.replace(/-/g, "/")).getTime();
        }

        if( stD>endD ){
          alert( '종료기간이 시작기간보다 이전입니다.' ); //'종료기간이 시작기간보다 이전입니다.'
          return false;
        }
      }

      itemsTableManager
        .updateTableData(reqData)
        .then(
          function(__sucData){
            APP.modules.ctrl.tablePaginationBasicCtrl.beforeChangePage();
            vm._state.tableData = __sucData;
            PubSub.trigger('table:change', itemsTableManager.getTableData() );
          },
          function(__errData){
            alert( '상품 리스트 페이지 호출을 하지 못했습니다.' );  //'상품 리스트 페이지 호출을 하지 못했습니다.'
            console.log('tableComponentItemsCtrl _updatePage __errData: ',__errData);
          }
        );

    }


    function _getCheckedListRow(__attr){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push($(this).attr(__attr));
        }
      });
      return strArr;
    }

    function _getCheckedListRowName(){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push( $(this).attr('shop-name')+'('+$(this).attr('uuid')+')' );
        }
      });
      return strArr;
    }



  }

  tableComponentItemsCtrl.$inject = ['$scope', '$location', '_',  '$timeout', 'XHR', 'PubSub', 'ItemsTableManager', 'API_URL_INFO', 'COMMON_UTIL'];
  tableComponentItemsCtrl.bindToController = {
    bootstrapCtrl:'@'
  };
  tableComponentItemsCtrl.link = function () {
    return function (scope, elem, attrs, ctrl, transclude) {
      transclude(scope, function(clone, scope) {
        elem.find('.list_table').empty().append(clone);
      });
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
