<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<style type="text/css">
.dialog_content {
	overflow: hidden;
	padding: 0 15px 15px !important;
}
</style>

<div class="page"> 
  <!-- 페이지导航 -->
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $output['item_title'];?></h3>
      <ul class="tab-base">
        <?php   foreach($output['menu'] as $menu) {  if($menu['menu_key'] == $output['menu_key']) { ?>
        <li><a href="JavaScript:void(0);" class="current"><span><?php echo $menu['menu_name'];?></span></a></li>
        <?php }  else { ?>
        <li><a href="<?php echo $menu['menu_url'];?>" ><span><?php echo $menu['menu_name'];?></span></a></li>
        <?php  } }  ?>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="form_item" action="<?php echo urlAdmin('mb_special', 'special_item_save');?>" method="post">
    <input type="hidden" name="special_id" value="<?php echo $output['item_info']['special_id'];?>">
    <input type="hidden" name="item_id" value="<?php echo $output['item_info']['item_id'];?>">
    <table class="table tb-type2 nohover">
      <tbody>
        <?php $item_data = $output['item_info']['item_data'];?>
        <?php $item_edit_flag = true;?>
        <tr class="noborder">
          <td style="height: auto; padding: 0;"><div id="item_edit_content" class="mb-item-edit-content">
              <?php require('mb_special_item.module_' . $output['item_info']['item_type'] . '.php');?>
            </div></td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="2"><a id="btn_save" class="btn" href="javascript:;"><span>저장</span></a>
            <?php if($output['item_info']['special_id'] > 0) { ?>
            <a id="btn_back" href="<?php echo urlAdmin('mb_special', 'special_edit', array('special_id' => $output['item_info']['special_id']));?>" class="btn"><span>뒤로가기</span></a>
            <?php } else { ?>
            <a id="btn_back" href="<?php echo urlAdmin('mb_special', 'index_edit');?>" class="btn"><span>뒤로가기</span></a>
            <?php } ?></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<div id="dialog_item_edit_image" style="display:none;">
  <div class="s-tips margintop"><i></i>추천한 사이즈로 이미지를 올려주세요, 모바일 버전에서 최적화된 이미지를 노출하기 위해서입니다.</div>
  <div class="upload-thumb"> <img id="dialog_item_image" src="" alt=""></div>
  <input id="dialog_item_image_name" type="hidden">
  <input id="dialog_type" type="hidden">
  <form id="form_image" action="">
    <div class="dialog-handle-box clearfix">
      <h4 class="dialog-handle-title">업로드 할 이미지 선택하세요：</h4>
      <span>
      <input id="btn_upload_image" type="file" name="special_image">
      </span> <span id="dialog_image_desc" class="dialog-image-desc"></span>
      <h4 class="dialog-handle-title">조작유형：</h4>
      <div>
        <select id="dialog_item_image_type" name="" class="vatop">
        <option value="">-선택하세요-</option>
          <option value="keyword">키워드</option>
          <option value="special">스페셜번호</option>
          <option value="goods">상품번호</option>
          <option value="url">링크</option>
        </select>
        <input id="dialog_item_image_data" type="text" class="txt w200 marginright marginbot vatop"><br />
        <span id="dialog_item_image_desc" class="dialog-image-desc"></span>
        </div>
    </div>
    <a id="btn_save_item" class="btn" href="javascript:;"><span>저장</span></a>
  </form>
</div>
<script id="item_image_template" type="text/html">
    <div cbtype="item_image" class="item">
        <img cbtype="image" src="<%=image%>" alt="">
        <input cbtype="image_name" name="item_data[item][<%=image_name%>][image]" type="hidden" value="<%=image_name%>">
        <input cbtype="image_type" name="item_data[item][<%=image_name%>][type]" type="hidden" value="<%=image_type%>">
        <input cbtype="image_data" name="item_data[item][<%=image_name%>][data]" type="hidden" value="<%=image_data%>">
        <a cbtype="btn_del_item_image" href="javascript:;">삭제</a>
    </div>
</script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/template.min.js" charset="utf-8"></script> 
<script type="text/javascript">
    var url_upload_image = '<?php echo urlAdmin('mb_special', 'special_image_upload');?>';

    $(document).ready(function(){
        var $current_content = null;
        var $current_image = null;
        var $current_image_name = null;
        var $current_image_type = null;
        var $current_image_data = null;
        var old_image = '';
        var $dialog_item_image = $('#dialog_item_image');
        var $dialog_item_image_name = $('#dialog_item_image_name');
        var special_id = <?php echo $output['item_info']['special_id'];?>;

        //저장
        $('#btn_save').on('click', function() {
            $('#form_item').submit();
        });

        //수정이미지
        $('[cbtype="btn_edit_item_image"]').on('click', function() {
            //初始化当前이미지상대
            $item_image = $(this).parents('[cbtype="item_image"]');
            $current_image = $item_image.find('[cbtype="image"]');
            $current_image_name = $item_image.find('[cbtype="image_name"]');
            $current_image_type = $item_image.find('[cbtype="image_type"]');
            $current_image_data = $item_image.find('[cbtype="image_data"]');

            $('#dialog_item_image').attr('src', $current_image.attr('src'));
            $('#dialog_item_image_name').val($current_image_name.val());
            $('#dialog_item_image_type').val($current_image_type.val());
            $('#dialog_item_image_data').val($current_image_data.val());
            $('#dialog_image_desc').text('추천 이미지 사이즈' + $(this).attr('data-desc'));
            $('#dialog_type').val('edit');
            change_image_type_desc($('#dialog_item_image_type').val());
            $('#dialog_item_edit_image').nc_show_dialog({
                width: 600,
                title: '수정'
            });
        });

        //추가이미지
        $('[cbtype="btn_add_item_image"]').on('click', function() {
            $dialog_item_image.hide();
            $dialog_item_image_name.val('');
            $current_content = $(this).parent().find('[cbtype="item_content"]');
            $('#dialog_image_desc').text('추천 이미지 사이즈' + $(this).attr('data-desc'));
            $('#dialog_type').val('add');
            change_image_type_desc($('#dialog_item_image_type').val());
            $('#dialog_item_edit_image').nc_show_dialog({
                width: 600,
                title: '추가'
            });
        });

        //삭제이미지
        $('#item_edit_content').on('click', '[cbtype="btn_del_item_image"]', function() {
            $(this).parents('[cbtype="item_image"]').remove();
        });

        //이미지 업로드
        $("#btn_upload_image").fileupload({
            dataType: 'json',
            url: url_upload_image,
            formData: {special_id: special_id},
            add: function(e, data) {
                old_image = $dialog_item_image.attr('src');
                $dialog_item_image.attr('src', LOADING_IMAGE);
                data.submit();
            },
            done: function (e, data) {
                var result = data.result;
                if(typeof result.error === 'undefined') {
                    $dialog_item_image.attr('src', result.image_url);
                    $dialog_item_image.show();
                    $dialog_item_image_name.val(result.image_name);
                } else {
                    $dialog_item_image.attr('src') = old_image;
                    showError(result.error);
                }
            }
        });

        $('#btn_save_item').on('click', function() {
            var type = $('#dialog_type').val();
            if(type == 'edit') {
                edit_item_image_save();
            } else {
                if($dialog_item_image_name.val() == '') {
                    showError('이미지를 업로드하세요');
                    return false;
                }
                add_item_image_save();
            }
            $('#dialog_item_edit_image').hide();
        });

        function edit_item_image_save() {
            $current_image.attr('src', $('#dialog_item_image').attr('src'));
            $current_image_name.val($('#dialog_item_image_name').val());
            $current_image_type.val($('#dialog_item_image_type').val());
            $current_image_data.val($('#dialog_item_image_data').val());
        }

        function add_item_image_save() {
            var $html_item_image = $('#html_item_image');
            var item = {};
            item.image = $('#dialog_item_image').attr('src');
            item.image_name = $('#dialog_item_image_name').val();
            item.image_type = $('#dialog_item_image_type').val();
            item.image_data = $('#dialog_item_image_data').val();
            $current_content.append(template.render('item_image_template', item));
        }


        $('#dialog_item_image_type').on('change', function() {
            change_image_type_desc($(this).val());
        });

        function change_image_type_desc(type) {
            var desc_array = {};
            var desc = '전체 4가지 유형으로 검색이 가능합니다.';
            if(type != '') {
                desc_array['keyword'] = '키워드는 상품 검색창에 입력하신 내용으로 검색되는 부분입니다.';
                desc_array['special'] = '스페셜 번호는 해당 이벤트 페이지로 이동됩니다.';
                desc_array['goods'] = '상품번호는 해당 상품의 상세페이지로 이동됩니다.';
                desc_array['url'] = '링크는 지정하신 해당 링크로 이동됩니다.';
                desc = desc_array[type];
            }
            $('#dialog_item_image_desc').text(desc);
        }
    });
    </script> 
