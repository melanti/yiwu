<?php
  $payment_name_ko = array(
      "在线支付"=>"온라인결제",
      "支付宝"=>"Alipay",
    );

?>
<div class="ncsc-flow-item">
  <div class="title">관련상품 거리내역 </div>
  <div class="item-goods">
        <?php if (is_array($output['goods_list']) && !empty($output['goods_list'])) { ?>
        <?php foreach ($output['goods_list'] as $key => $val) { ?>
      <dl>
        <dt>
          <div class="ncsc-goods-thumb-mini"><a target="_blank" href="<?php echo urlShop('goods','index',array('goods_id'=> $val['goods_id'])); ?>">
            <img src="<?php echo thumb($val,60); ?>" onMouseOver="toolTip('<img src=<?php echo thumb($val,240); ?>>')" onMouseOut="toolTip()" /></a></div>
        </dt>
        <dd><a target="_blank" href="<?php echo urlShop('goods','index',array('goods_id'=> $val['goods_id'])); ?>"><?php echo $val['goods_name']; ?></a>
            <?php echo $val['goods_price']; ?>원 * <?php echo $val['goods_num']; ?> <font color="#AAA">(수량)</font>
            <span><?php echo orderGoodsTypeKo($val['goods_type']);?></span>
        </dd>
      </dl>
        <?php } ?>
        <?php } ?>
  </div>
  <div class="item-order">
    <dl>
      <dt>배&nbsp;&nbsp;송&nbsp;&nbsp;료：</dt>
      <dd><?php echo $output['order']['shipping_fee']>0 ? number_format($output['order']['shipping_fee_ko']):"(무료배송)"; ?></dd>
    </dl>
    <dl>
      <dt>주문총액：</dt>
      <dd><strong><?php echo number_format($output['order']['order_amount_ko']); ?>원
        <?php if ($output['order']['refund_amount'] > 0) { ?>
        (환불<?php echo $lang['nc_colon'].number_format($output['order']['refund_amount_ko'])."원";?>)
        <?php } ?>
        </strong> </dd>
    </dl>
    <dl class="line">
      <dt>주문번호：</dt>
      <dd><a target="_blank" href="index.php?act=store_order&op=show_order&order_id=<?php echo $output['order']['order_id']; ?>"><?php echo $output['order']['order_sn'];?></a> <a href="javascript:void(0);" class="a">더보기<i class="icon-angle-down"></i>
        <div class="more"> <span class="arrow"></span>
          <ul>
            <?php if($output['order']['payment_code'] != 'offline' && !in_array($output['order']['order_state'],array(ORDER_STATE_CANCEL,ORDER_STATE_NEW))) { ?>
            <li><?php echo '결제번호'.$lang['nc_colon'];?><span><?php echo $output['order']['pay_sn']; ?></span></li>
            <?php } ?>
            <li><?php echo '결제방식'.$lang['nc_colon'];?><span><?php echo $payment_name_ko[$output['order']['payment_name']]; ?></span></li>
            <li><?php echo $lang['store_order_add_time'].$lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order']['add_time']); ?></span></li>
            <?php if ($output['order']['payment_time'] > 0) { ?>
            <li>결제시간<?php echo $lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order']['payment_time']); ?></span></li>
            <?php } ?>
            <?php if ($output['order_common']['shipping_time'] > 0) { ?>
            <li>배송시간<?php echo $lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order_common']['shipping_time']); ?></span></li>
            <?php } ?>
            <?php if ($output['order']['finnshed_time'] > 0) { ?>
            <li>완료시간<?php echo $lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order']['finnshed_time']); ?></span></li>
            <?php } ?>
          </ul>
        </div>
        </a> </dd>
    </dl>
    <?php if (!empty($output['order']['shipping_code'])) { ?>
    <dl>
      <dt>상품번호：</dt>
      <dd><?php echo $output['order']['shipping_code'];?> <a href="javascript:void(0);" class="a"><?php echo $output['e_name'];?></a></dd>
    </dl>
    <?php } ?>
    <dl class="line">
      <dt>수&nbsp;&nbsp;취&nbsp;&nbsp;인：</dt>
      <dd><?php echo $output['order_common']['reciver_name'];?><a href="javascript:void(0);" class="a">더보기<i class="icon-angle-down"></i>
        <div class="more"><span class="arrow"></span>
          <ul>
            <li><?php echo $lang['store_order_address'].$lang['nc_colon'];?><span><?php echo $output['order_common']['reciver_info']['address'];?></span></li>
            <li>연락처：<span><?php echo $output['order_common']['reciver_info']['phone'];?></span></li>
          </ul>
        </div>
        </a>
        <div><span member_id="<?php echo $output['order']['buyer_id'];?>"></span>
          <?php if (!empty($output['member']['member_qq'])) { ?>
          <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $output['member']['member_qq'];?>&site=qq&menu=yes" title="QQ: <?php echo $output['member']['member_qq'];?>"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo $output['member']['member_qq'];?>:52" style=" vertical-align: middle;"/></a>
          <?php } ?>
          <?php if (!empty($output['member']['member_ww'])) { ?>
          <a target="_blank" href="http://amos.im.alisoft.com/msg.aw?v=2&uid=<?php echo $output['member']['member_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>"  class="vm" ><img border="0" src="http://amos.im.alisoft.com/online.aw?v=2&uid=<?php echo $output['member']['member_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>" alt="Wang Wang"  style=" vertical-align: middle;"/></a>
          <?php } ?>
        </div>
      </dd>
    </dl>
  </div>
</div>