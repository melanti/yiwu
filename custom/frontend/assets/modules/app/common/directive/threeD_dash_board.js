//APP.modules.ctrl.threeDDashBoardCtrl


(function($, angular, _, APP){

  APP.modules.ctrl.threeDDashBoardCtrl = threeDDashBoardCtrl;

  //threeDDashBoardCtrl
  function threeDDashBoardCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO, ItemsTableManager, ThreeDTableManager){
    var vm = this;
    vm.type;//='threed'; //item , threed
    vm.items={
      total:0,
      register:0,
      not_register:0
    };
    vm.threed={
      total:0,
      woman:0,
      man:0
    };


    _init();

    function _init(){

      if( vm.type=='item' ){
        _setItemsTotal();
      }else if( vm.type=='threed' ){
        _set3DTotal();
      }

      PubSub.subscribe('TOTAL_ITEMS_COUNT',function(__topic , __data){
        //vm._state.tableData.info
        if( vm.type=='item' ){
          _setItemsTotal();
        }else if( vm.type=='threed' ){
          _set3DTotal();
        }

      });
    }

    function _setItemsTotal(){

      var tmpParam=[
        '?start=0',
        '&count=1000',
        '&from=admin',
        '&item_name=',
        '&item_uuid=',
        '&sale_type=2',

        '&origin_item_id=',
        '&td_name=',
        '&td_code=',
        '&org_id=',
        '&org_name=',

        '&brand_id=',
        '&item_cate_id=',
        '&item_cate_status=ready,activated,disabled',
        '&include_cate_family_flag=true',
        '&include_cate_unassigned_flag=true',


        '&item_status=ready,activated,disabled',
        '&td_reg_flag=all',
        '&period_reg_start=',
        '&period_reg_end=',
        '&period_upd_start=',
        '&period_upd_end=',

        '&order=registerd_date',
        '&order_type=desc',

        '&show=td_id,td_list,debug'
      ].join('');

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_ITEMS_LIST+tmpParam
        })
        .then(
          function(__sucData){
            if(__sucData.data.info){
              vm.items={
                total:__sucData.data.info.total,
                register:__sucData.data.info.td.register,
                not_register:__sucData.data.info.td.not_register
              };
            }

          },
          function(__errData){
            alert( APP.i18n["tdlist.alert.cant_get_3d_regist_list"]); //'3D 의상 등록건을 가져오지 못했습니다.'
          }
        );


    }

    function _set3DTotal(){
      /*ThreeDTableManager.exports
        .updateTableData({start:0,count:20,show:'debug'})
        .then(
          function(__sucData){
            //console.log('TOTAL_ITEMS_COUNT _set3DTotal: ' , __sucData.info);
            vm.threed={
              total:__sucData.info.total,
              woman:__sucData.info.sex.woman,
              man:__sucData.info.sex.man
            };

          },
          function(__errData){
            alert('상품 리스트 페이지 호출을 하지 못했습니다.');
            console.log('threeDDashBoardCtrl  _setItemsTotal __errData: ',__errData);
          }
        );
      */

      var tmpParam=[
        '?start=0',
        '&count=1000',
        '&from=admin',
        '&td_name=',
        '&td_code=',
        '&item_uuid=',
        '&org_id=',
        '&org_name=',
        '&cat_id=',
        '&td_sex=',
        '&td_status=ready,activated,disabled',
        '&make_category_db_id=',
        '&make_category_name=',
        '&period_reg_start=',
        '&period_reg_end=',
        '&show=thumbnail,td,categories,debug,td_fitting'
      ].join('');

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_3DITEMS_LIST+tmpParam
        })
        .then(
          function(__sucData){
            vm.threed={
              total:__sucData.data.info.total,
              woman:__sucData.data.info.sex.woman,
              man:__sucData.data.info.sex.man
            };

          },
          function(__errData){
            alert( APP.i18n["items.list.alert.no_call_items_list"]);  //'상품 리스트 페이지 호출을 하지 못했습니다.'
            console.log('threeDDashBoardCtrl  _setItemsTotal __errData: ',__errData);
          }
        );

    }

  }

})(window.jQuery, window.angular, window._, window.APP);
