(function ($, angular, _, APP) {

  APP.modules.ctrl.brandRegistrationCtrl = brandRegistrationCtrl;

  //brandRegistrationCtrl
  function brandRegistrationCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO, BrandTableManager) {
    var vm = this;
    vm._state = {
      id: "",
      name: "",
      status: ""
    };
    vm.brand_code_arr = [];
    vm.exports = {
      init: _init
    };
    function _init() {
      console.log('init');
      console.log( 'vm.brandName: ' , vm.brandName );
      console.log( 'vm.brandName: ' , decodeURIComponent(vm.brandName) );

      _getBrandList();
      vm._state.id = vm.brandId === 'undefined' ? '' : vm.brandId;
      vm._state.name = vm.brandName === 'undefined' ? '' : decodeURIComponent(decodeURIComponent(vm.brandName));
      vm._state.status = vm.brandStatus === 'undefined' ? 'activated' : vm.brandStatus;
    }

    _init();
    function _getBrandList (){
      BrandTableManager.exports
        .updateTableData({start: 0, count: 1000, query_opt: 'name', query: '', status: 'all'})
        .then(
          function (__sucData) {
            //console.log(__sucData);
            if (__sucData.results && __sucData.results.length > 0) vm.brand_code_arr = __sucData.results;
          },
          function (__errData) {
            //console.log(__errData);
          }
        );
    }
    vm._submit = function () {
      console.log(vm._state);

      if (vm._state.name === "") {
        alert( '브랜드명을 입력해 주세요' );  //'브랜드명을 입력해 주세요'
        return;
      }

      var _arr = vm.brand_code_arr;
      if (vm._state.id === "") {
        for (var i in _arr) {
          if (_arr[i].name == vm._state.name) {
            alert( '이미 사용 중인 브랜드명입니다. 다시 입력해 주세요.' ); //'이미 사용 중인 브랜드명입니다. 다시 입력해 주세요.'
            vm._state.name = "";
            return;
          }
        }
      }

      var _URL = vm._state.id === "" ? API_URL_INFO.SET_BRAND : API_URL_INFO.UPDATE_BRAND;
      XHR.POST(_URL, {
          id: vm._state.id,
          name: vm._state.name,
          status: vm._state.status
        })
        .then(function (ret) {
          console.log('success');
          console.log(ret);
          alert(vm._state.id === "" ? "등록되었습니다." : "수정되었습니다."); //"등록되었습니다."  :"수정되었습니다."
          window.location.href = window.location.href = window.location.protocol+'//'+window.location.host+'/' + APP.info.gLocale + APP.URL_INFO.menu.ITEMS_MNG_CATEGORY_BRAND.link;

        }, function (ret) {
          console.log('error');
          console.log(ret);
          alert('error');
        })
    }

  }
})(window.jQuery, window.angular, window._, window.APP);
