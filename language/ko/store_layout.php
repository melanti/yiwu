<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * index
 */

$lang['show_store_index_store_due']			= '您要查看的店铺已到期';
/**
 * header中的文字
 */
$lang['nc_store_nonentity']     = '店铺信息不正确，请重新登录';
$lang['nc_hello']				= "您好";
$lang['nc_logout']				= "退出";
$lang['nc_guest']				= "游客";
$lang['nc_login']				= "登录";
$lang['nc_register']			= "注册";
$lang['nc_seller'] 				= '商家中心';
$lang['nc_user_center']			= "我的商城";
$lang['nc_message']				= "站内消息";
$lang['nc_help_center']			= "帮助中心";
$lang['nc_more_links']			= "更多链接";
$lang['nc_index']				= "首页";
$lang['nc_search_in_website']	= '全站搜';
$lang['nc_search_in_store']		= '店内搜';
$lang['nc_searchdefault']		= "搜索其实很容易！";
$lang['nc_search']				= "搜索";
$lang['nc_message_center']		= "客服中心";
$lang['nc_message_presales']	= "售前客服：";
$lang['nc_message_service']		= "售后客服：";
$lang['nc_message_working']		= "工作时间：";
$lang['nc_goods_class']			= "商品分类";
$lang['nc_store_class']			= "店铺分类";
$lang['nc_cart']				= "购物车";
$lang['nc_kindof_goods']		= "种商品";
$lang['nc_favorites']			= "我的收藏";
$lang['nc_more']				= '更多';
$lang['nc_description_of']		= '描述相符：';
$lang['nc_service_attitude']	= '服务态度：';
$lang['nc_delivery_speed']		= '发货速度：';
$lang['nc_grade']				= '分';
$lang['nc_around_shop']			= '逛逛店铺';
$lang['nc_message_me']			= '点击这里给我发消息';
$lang['nc_good_rate']			= '好评率：';
$lang['nc_dynamic_evaluation']	= '店铺动态评分';
$lang['nc_store_information']	= '店铺信息';
$lang['nc_store_owner']			= '店主：';
$lang['nc_store_addtime']		= '创店时间：';
$lang['nc_srore_location']		= '所&nbsp;&nbsp;在&nbsp;&nbsp;地：';
$lang['nc_goods_amount']		= '商品数量：';
$lang['nc_store_collect']		= '店铺收藏：';
$lang['nc_contact']				= '联系方式：';
$lang['nc_contact_way']			= '联系方式';
$lang['nc_me_collect']			= '我要收藏';
$lang['nc_person']				= '人';
$lang['nc_collect']				= '收藏';
$lang['nc_jian']				= '件';
$lang['nc_goods']				= '商品';
$lang['nc_name_identify']		= '实名认证';
$lang['nc_store_identify']		= '实体店铺认证';
$lang['nc_store_info']			= '店铺详情';
$lang['nc_store_map']			= '店铺地图';
$lang['nc_store_qrcode']		= '店铺二维码';
$lang['nc_send_message']		= '发站内信';
$lang['nc_identify']			= '认证信息：';
$lang['nc_store_grade']			= '店铺等级';
$lang['nc_credit_degree']		= '卖家信用：';
$lang['nc_map_loading']			= '地图加载中...';
$lang['nc_whole_goods']			= '全部商品';
$lang['nc_blogroll']			= '友情链接';
$lang['nc_store_index']			= '店铺首页';
$lang['nc_goods_info']			= '商品详情';
$lang['nc_coupon']				= "优惠券";
$lang['nc_bundling']			= "优惠套装";
$lang['nc_credit']				= '信用评价';
$lang['nc_whole_address']		= '详细地址：';
$lang['nc_phone']				= '联系电话：';
$lang['nc_keyword']				= '关键字：';
$lang['nc_price']				= '价格：';
$lang['nc_goods_rankings']		= '商品排行';
$lang['nc_hot_goods_rankings']	= '热销商品排行';
$lang['nc_hot_collect_rankings']= '热门收藏排行';
$lang['nc_sell_out']			= '售出：';
$lang['nc_bi']					= '笔';
$lang['nc_by_new']				= '按新品';
$lang['nc_by_price']			= '按价格';
$lang['nc_by_sale']				= '按销量';
$lang['nc_by_click']			= '按人气';
$lang['nc_look_more_store_goods']		= '查看本店其他商品';
$lang['nc_qrcode_desc']			= '手机扫描二维码<br/>快速收藏店铺';
$lang['nc_store_address']		= '店铺地址:';
$lang['nc_store_files']			= '店铺档案';
$lang['nc_basic_information']	= '基本信息';
$lang['nc_dynamic_scoring']		= '动态评分';
$lang['nc_service']				= '客服';
$lang['nc_what_goods']			= '想找什么商品？';
$lang['nc_shop_space']			= '店铺空间';
$lang['nc_store_the_dynamic']	= '店铺动态';
$lang['nc_share']				= '分享';
$lang['nc_collection_popularity']	= '收藏人气';

/**
 * 评价
 */
$lang['nc_credit_evalstore_type_1']	= '宝贝与描述相符';
$lang['nc_credit_evalstore_type_2']	= '卖家的服务态度';
$lang['nc_credit_evalstore_type_3']	= '卖家的发货速度';
$lang['nc_credit_good']				= '好评';
$lang['nc_credit_normal']			= '中评';
$lang['nc_credit_bad']				= '差评';
$lang['nc_credit_all']				= '全部评价';
$lang['nc_credit_defaultcontent_good']	= '好评!';
$lang['nc_credit_defaultcontent_normal']= '中评!';
$lang['nc_credit_defaultcontent_bad']	= '差评!';
$lang['nc_credit_explain']	= '商家解释';
$lang['nc_credit_buyer_credit']	= '买家信用：';

/**
 * footer中的文字
 */
$lang['nc_index']		= '首页';
$lang['nc_page_execute']	= '页面执行';
$lang['nc_second']		= '秒';
$lang['nc_query']		= '查询';
$lang['nc_times']		= '次';
$lang['nc_online']		= '在线';
$lang['nc_person']		= '人';
$lang['nc_enabled']		= '已启用';
$lang['nc_disabled']	= '已禁用';
$lang['nc_memory_cost']	= '占用内存';
$lang['nc_store_close']	= '该店铺已关闭';




/** add to lang **/
$lang['cb_day']	= '일';

$lang['store_daddress_isdefault'] = '기본여부';
$lang['nc_store_login_title']	= '로그인';
$lang['nc_tel']	= '전화';
$lang['seller_title']	= '판매관리자';
$lang['seller_home']	= '메인';
$lang['seller_guide']	= '도움가이드';
$lang['seller_layout_quick'] = '퀵링크';
$lang['seller_layout_nav'] = '네비게이션';
$lang['seller_layout_empty_search'] = '키워드를 입력하세요.';
$lang['store_deliver_daddress_list']		= '배송지';
$lang['store_deliver_default_express']		= '택배사';
$lang['store_deliver_freeship']		= '무료배송금액';
$lang['store_deliver_timeday']		= '도착시간';
$lang['store_deliver_print']		= '송장프린트';

$lang['free_freight_help'] = '默认为 0，表示不设置免运费额度，大于0表示购买金额超出该值后将免运费';
$lang['freetime_freight_help'] = '默认为2天，填写0，表示默认不显示，建议填写数值为3天或7天';
$lang["input_price"] = '请填写金额';
$lang["input_error"] = '请正确填写';
$lang["input_error_number"] = '请正确填写，只能整数';
$lang["input_error_day"] = '请填写天数';


 $lang['store_closed'] = '점포는 이미 패점상태입니다.';
 $lang['store_closed_issu'] = '패점이유';
 $lang['store_unopen'] = '잠시 방문할 수 없습니다.';
 $lang['store_call_system'] = '고객센터와 연락하세요.';
 $lang['store_home'] = '미니샵 바로가기';
 $lang['store_mypage'] = '마이페이지';
 $lang['store_logout'] = '로그아웃';
 $lang['maxed'] = '최대';
 $lang['maxed_text'] = '개 까지만 가능합니다.';
 $lang['store_notice'] = '공지뉴스';
 $lang['store_qa'] = '상담센터';
 $lang['store_gotop'] = '위로가기';



/** menu **/
$lang['store_menu_goods'] = '상품';
	$lang['store_menu_goods_add'] = '상품추가';
	$lang['store_menu_goods_on'] = '진열상품';
	$lang['store_menu_goods_off'] = '미진열상품';
	$lang['store_menu_goods_templates'] = '템플릿';
	$lang['store_menu_goods_option'] = '옵션설정';
	$lang['store_menu_goods_images'] = '이미지관리';
$lang['store_menu_order'] = '구매내역';
	$lang['store_menu_order_list'] = '주문정보';
	$lang['store_menu_order_e'] = 'E-쿠폰 상품주문';
	$lang['store_menu_order_trancity'] = '배송내역';
	$lang['store_menu_order_transet'] = '배송설정';
	$lang['store_menu_order_trantemplate'] = '송장템플릿';
	$lang['store_menu_order_comment'] = '상품평관리';
	$lang['store_menu_order_transport'] = '배송료설정';
$lang['store_menu_marketing'] = '마케팅';
	$lang['store_menu_marketing_groupbuy'] = '공동구매';
	$lang['store_menu_marketing_timesale'] = '기간한정세일';
	$lang['store_menu_marketing_amountsale'] = '금액별 사은품 증정';
	$lang['store_menu_marketing_setsale'] = '할인세트';
	$lang['store_menu_marketing_recom'] = '추천코너';
	$lang['store_menu_marketing_coupon'] = '쿠폰관리';
	$lang['store_menu_marketing_event'] = '이벤트관리';
$lang['store_menu_store'] = '미니샵';
	$lang['store_menu_store_setting'] = '설정';
	$lang['store_menu_store_layout'] = '레이아웃';
	$lang['store_menu_store_nav'] = '내비게이션';
	$lang['store_menu_store_notice'] = '미니샵소식';
	$lang['store_menu_store_info'] = '미니샵정보';
	$lang['store_menu_store_category'] = '상품카테고리';
	$lang['store_menu_store_offline'] = '오프라인매장';
	$lang['store_menu_store_brand'] = '브랜드신청';
$lang['store_menu_as'] = 'A/S';
	$lang['store_menu_as_ask'] = '문의관리';
	$lang['store_menu_as_repot'] = '신고관리';
	$lang['store_menu_as_retrun'] = '환불기록';
	$lang['store_menu_as_refund'] = '반품기록';
$lang['store_menu_count'] = '통계정산';
	$lang['store_menu_count_total'] = '판매실적';
	$lang['store_menu_count_goods'] = '상품별분석';
	$lang['store_menu_count_sale'] = '판매별통계';
	$lang['store_menu_count_category'] = '카테고리분석';
	$lang['store_menu_count_user'] = '방문자통계';
	$lang['store_menu_count_bill'] = '실주문정산';
	$lang['store_menu_count_ebill'] = 'E-쿠폰 상품정산';
$lang['store_menu_cs'] = 'CS메세지';
	$lang['store_menu_cs_setting'] = 'CS설정';
	$lang['store_menu_cs_msg'] = '시스템메세지';
	$lang['store_menu_cs_chat'] = '채팅기록';
$lang['store_menu_member'] = '계정';
	$lang['store_menu_member_list'] = '계정리스트';
	$lang['store_menu_member_group'] = '계정그룹';
	$lang['store_menu_member_log'] = '계정로그';
	$lang['store_menu_member_pay'] = '결제사항';
$lang['store_menu_mypage'] = '마이페이지';
	$lang['store_menu_mypage_index'] = '마이페이지';
	$lang['store_menu_mypage_edit'] = '정보수정';
	$lang['store_menu_mypage_ask'] = '1:1문의';
	$lang['store_menu_mypage_down'] = '자료실';


/**
 * 商家首页
 */
$lang['seller_id'] = "用户名";
$lang['seller_index_auth'] = "管理权限";
$lang['seller_index_lastlogin'] = "最后登录";
$lang['seller_index_upyear'] = "马上续签";
$lang['seller_index_contrast'] = "与行业相比";
$lang['seller_index_setting'] = "编辑店铺设置";
$lang['seller_index_protype'] = "店铺商品发布情况";
$lang['seller_index_published'] = "已发布";
$lang['seller_index_nolimit'] = "不限";
$lang['seller_index_wait_verfy'] = "发布待平台审核";
$lang['seller_index_wait_noverfy'] = "平台审核失败";

$lang['seller_index_score'] = '分';
$lang['seller_index_notice'] = '公告';
$lang['seller_index_platcon'] = '联系平台';
$lang['seller_index_berefund'] = '售前退款';
$lang['seller_index_afrefund'] = '售后退款';
$lang['seller_index_bereturn'] = '售前退货';
$lang['seller_index_afreturn'] = '售后退货';
$lang['seller_index_wait_bill'] = '待确认账单';
$lang['seller_index_salecount'] = '销售情况统计';
$lang['seller_index_orandpay'] = '按周期统计商家店铺的订单量和订单金额';
$lang['seller_index_project'] = '项目';
$lang['seller_index_orders'] = '订单量';
$lang['seller_index_orderamount'] = '订单金额';
$lang['seller_index_yestodaysale'] = '昨日销量';
$lang['seller_index_monthsale'] = '月销量';
$lang['seller_index_productrank'] = '单品销售排名';
$lang['seller_index_master'] = '掌握';
$lang['seller_index_todaysale'] = '日内最热销的商品及时补充货源';
$lang['seller_index_ranking'] = '排名';
$lang['seller_index_goodinfo'] = '商品信息';
$lang['seller_index_sales'] = '销量';
$lang['seller_index_eventsales'] = '合理参加促销活动可以有效提升商品销量';
$lang['seller_index_open'] = '已开通';
$lang['seller_index_close'] = '未开通';
$lang['seller_index_uppay'] = '续费至';
$lang['seller_index_recommend'] = '推荐展位';


/** seller step1 **/
 $lang['seller_guide_category'] = '카테고리 선택';
 $lang['seller_guide_goodinfo'] = '상세내용 입력';
 $lang['seller_guide_image'] = '이미지 업로드';
 $lang['seller_guide_finish'] = '등록 완료';
 $lang['seller_step1_categorys'] = '분류선택구역';
 $lang['seller_step1_mycate'] = '경영카테고리 추가';
 $lang['seller_step1_addcate'] = '카테고리 추가';
 $lang['seller_step1_quickcate'] = '자주 사용하는 카테고리';
 $lang['seller_step1_selelct_text'] = '선택하세요';
 $lang['seller_step1_quickcate_no'] = '자주 사용하는 카테고리가 없습니다.';
 $lang['seller_step1_category_text'] = '카테고리를 선택하세요. ';
 $lang['seller_step1_selected_text'] = '선택한카테고리';
 $lang['seller_step1_next'] = '다음';
 $lang['seller_step1_goods_detail'] = '상품설명입력';




 /* goods */

  $lang['seller_goods_quick'] = '퀵메뉴';
 $lang['seller_goods_info'] = '기본정보';
 $lang['seller_goods_detail'] = '상세내용 입력';
 $lang['seller_goods_befor'] = '출시예정 상품';
 $lang['seller_goods_trans'] = '배송정보';
 $lang['seller_goods_other'] = '기타정보';
 $lang['seller_goods_other_help'] = '미니샵 카테고리는 소비자가 고객님의 미니샵에 방문시 노출되는 카테고리입니다. 
            <br>카테고리 카테고리는 "셀러센터-미니샵-미니샵카테고리"에서 설정가능합니다.';
 $lang['seller_goods_selectcate'] = '카테고리 선택';
 $lang['seller_goods_indetail'] = '상세내용 입력';
 $lang['seller_goods_imgup'] = '이미지 업로드';
 $lang['seller_goods_upfinished'] = '등록 완료';
 $lang['seller_goods_ginfo'] = '상품기본정보';
 $lang['seller_goods_category'] = '카테고리';
 $lang['seller_goods_edit'] = '수정';
 $lang['seller_goods_input'] = '입력';
 $lang['seller_goods_brand'] = '브랜드';
 $lang['seller_goods_brand_help'] = '브랜드로 검색하는 중국소비자 비중으로 반드시 선택해주시길 바랍니다.<br />위 리스트에 브랜드가 없으시면 옆의 추가버튼으로 추가후 상품등록을 진행해주시길 바랍니다.';
 $lang['seller_goods_select'] = '선택하세요';
 $lang['seller_goods_brandadd'] = '브랜드신청';
 $lang['seller_goods_add'] = '추가';
 $lang['seller_goods_name_ko'] = '상품명(한국어)';
 $lang['seller_goods_name_ko_help'] = '한국어 상품명은 소비자가 주문시 판매 관리자에서 한글로 보여주기 위해서입니다.';
 $lang['seller_goods_name_cn'] = '상품명(중국어)';
 $lang['seller_goods_name_cn_help'] = '중국어 상품명은 소비자 페이지에서 노출됩니다.';
 $lang['seller_goods_memo_ko'] = '간략설명(한국어)';
 $lang['seller_goods_memo_ko_help'] = '한국어 간략설명은 판매관리자 페이지에서만 노출됩니다.';
 $lang['seller_goods_memo_cn'] = '간략설명(중국어)';
 $lang['seller_goods_memo_cn_help'] = '중국어 간략설명은 상품 상세 페이지 상품명 아래에 노출됩니다.';
 $lang['seller_goods_weight'] = '무게';
 $lang['seller_goods_weight_help'] = '국제 배송으로 인해 무게 설정은 아주 중요한 단계이므로 기본 무게에 뽁뽁이 무게까지 포함해서 조금 추가하시길 권장합니다.';

 $lang['seller_goods_cur'] = '환율';
 $lang['seller_goods_won'] = '원';
 $lang['seller_goods_cny'] = '위안';
 $lang['seller_goods_sell_price'] = '판매가';
 $lang['seller_goods_sell_price_help'] = '판매가는 실제 쇼핑몰에서 판매되는 가격입니다.';
 $lang['seller_goods_market_price'] = '소비자가';
 $lang['seller_goods_market_price_help'] = '소비자 가격은 실제 매장에서 판매하는 가격을 입력하여 싸게 구매한다는 느낌을 유도하기 위해서입니다.';
 $lang['seller_goods_cost_price'] = '공급가';
 $lang['seller_goods_cost_price_help'] = '공급 가격은 사업자가 구매하는 모든 상품의 공급가를 뜻하며 실제 공급가를 입력하여 주기시 바랍니다.  입력은 선택 사항이며';
 $lang['seller_goods_option_add'] = '옵션추가';
 $lang['seller_goods_option_help'] = '기본카테고리 명칭을 등록하여 주십시오. (최대 4자)';
 $lang['seller_goods_ok'] = '확인';
 $lang['seller_goods_option_setting'] = '옵션설정';
 $lang['seller_goods_allsetting'] = '일괄설정';
 $lang['seller_goods_allsetting_price'] = '일괄가격설정';
 $lang['seller_goods_option_name'] = '옵션명';
 $lang['seller_goods_cancel'] = '취소';
 $lang['seller_goods_setting'] = '설정';
 $lang['seller_goods_price'] = '가격';
 $lang['seller_goods_storage'] = '재고';
 $lang['seller_goods_all_storageset'] = '일괄재고설정';
 $lang['seller_goods_sku'] = '상품코드';

 $lang['seller_goods_storage_help'] = '실제 재고를 사용하셔서 소비자가 수취확인시 재고량 감소 혹은 반품에 대한 재고관리를 할 수 있습니다.<br />재고가 부족시 고객님고객님의 페이지 상단에 추천 상품으로 됩니다. 이메일 및 문로 재고 부족이라는 메세지가 발송됩니다.';
 $lang['seller_goods_sku_help'] = '판매자가 상품에 대한 유일한 상품 ';

 $lang['seller_goods_no_front'] = ' 소비자 페이지에는 노출되지 않습니다.';
 $lang['seller_goods_image'] = '이미지';
 $lang['seller_goods_image_help'] = '업로드한 이미지는 상품의 메인이미지가 되며, JPG/GIF/PNG형식 파일 업로드 혹은 앨범에서 선택 가능합니다.<br>사이즈는 <font color="red"> 800 X 800px</font>이상이 적당하며, 크기는 <font color="red"> 1M</font>를 초과하지 않는 정사각의 이미지가 가능합니다. <br>업로드 후 사진은 자동으로 앨범의 기본카테고리에 저장됩니다.';	//'.printf($lang['store_goods_step2_description_two'],intval(C('image_max_filesize'))/1024).'

 $lang['seller_goods_storage_allsetting'] = '무게 일괄설정';


 $lang['seller_goods_upload'] = '업로드';
 $lang['seller_goods_album_select'] = '앨범선택';
 $lang['seller_goods_close'] = '닫기';
 $lang['seller_goods_album_close'] = '앨범 닫기';
 $lang['seller_goods_hscode'] = '코드';
 $lang['seller_goods_search_code'] = '코드 검색';
 $lang['seller_goods_scategory'] = '카테고리를 선택하세요';
 $lang['seller_goods_search'] = '검색';
 $lang['seller_goods_1696'] = '상세설명';
 $lang['seller_goods_1709'] = '상품속성';
 $lang['seller_goods_1756'] = '무제한';
 $lang['seller_goods_1805'] = '상품내용';
 $lang['seller_goods_1805_help'] = '모바일에서 노출되는 내용입니다.';

 $lang['seller_goods_web'] = ' 웹';
 $lang['seller_goods_1825'] = '모바일';
 $lang['seller_goods_1875'] = '앨범에서선택';
 $lang['seller_goods_1885'] = '앨범 닫기';
 $lang['seller_goods_1938'] = '위로가기';
 $lang['seller_goods_1941'] = '아래로가기';
 $lang['seller_goods_1945'] = '편집';
 $lang['seller_goods_1948'] = '삭제';
 $lang['seller_goods_1985'] = '교체';
 $lang['seller_goods_2025'] = '텍스트';
 $lang['seller_goods_2025_help'] = '1) 최대 500자, 구두점, 특수문자는 한 글자에 해당；<br />너무 많은 글자를 입력하시면 뚜렷해 보이지 않을 수도 있습니다.';
 $lang['seller_goods_2036'] = '. 기본사항';
 $lang['seller_goods_2036_help'] = '1) 이미지+텍스트, 이미지 20장, 글수 5000；<br />모든 사진은 상품과 관련된 사진만 업로드 하여 주십시오.';
 $lang['seller_goods_image_guide'] = ' 이미지';
 $lang['seller_goods_image_guide_help'] = ' 1) 폭 480~620 px, 높이 최대 960px；<br />2) 가능한 파일：JPG\JEPG\GIF\PNG；<br />ex) 폭이 480,높이 960의  JPG 사진 업로드';
 $lang['seller_goods_2042'] = 'F-코드 확장';
 $lang['seller_goods_2042_help'] = '해당 상품에 대한 고유번호를 분리하기 위해 3-5이내의 영문을 입력하여 식별하시길 바랍니다.';
 $lang['seller_goods_2043'] = ' 글자수 최대 ';
 $lang['seller_goods_2046'] = '모든 사진은 상품과 관련된 사진만 업로드 하여 주십시오.';
 $lang['seller_goods_2051'] = '. 이미지';
 $lang['seller_goods_2054'] = ' 폭';
 $lang['seller_goods_2056'] = ' 높이 최대';
 $lang['seller_goods_2058'] = ' 가능한 파일';
 $lang['seller_goods_2060'] = ' 폭이';
 $lang['seller_goods_2061'] = '높이';
 $lang['seller_goods_2062'] = '고객님의 페이지 상단에 추천 상품으로 됩니다.';
 $lang['seller_goods_2063'] = ' 사진 업로드';
 $lang['seller_goods_2068'] = '. 텍스트';
 $lang['seller_goods_2072'] = ' 구두점';
 $lang['seller_goods_2073'] = ' 특수문자는 한 글자에 해당';
 $lang['seller_goods_2075'] = '너무 많은 글자를 입력하시면 뚜렷해 보이지 않을 수도 있습니다.';
 $lang['seller_goods_2136'] = '模板';	//템플릿
 $lang['seller_goods_2141'] = '상단양식';
 $lang['seller_goods_2192'] = '하단양식';
 $lang['seller_goods_2264'] = 'E-쿠폰 상품';
 $lang['seller_goods_2264_max'] = '구매제한';
 $lang['seller_goods_2264_help'] = '쿠폰 상품은 기타 프로모션으로 설정할 수 없습니다.';
 $lang['seller_goods_2264_help1'] = 'E-쿠폰 상품 교환 유효기간, 만기후 상품은 구매할 수 없으며 전자코드도 사용할 수 없습니다.';
 $lang['seller_goods_2264_help2'] = '1-10이내 숫자를 입력하세요, E-쿠폰 상품은 최대 10개까지 구매제한됩니다.';

 $lang['seller_goods_2291'] = '예';
 $lang['seller_goods_2312'] = '아니오';
 $lang['seller_goods_2337'] = '유효기간';
 $lang['seller_goods_2449'] = '만기후 환불여부';
 $lang['seller_goods_2449_help'] = '교환코드가 만기후 환불 신청 여부입니다.';
 $lang['seller_goods_2748'] = '사용';
 $lang['seller_goods_2753'] = '미사용';
 $lang['seller_goods_2924'] = '배송비';
 $lang['seller_goods_2954'] = '고정금액';
 $lang['seller_goods_3316'] = '미니샵';
 $lang['seller_goods_3316_hot'] = '미니샵 추천';
 
 $lang['seller_goods_3323'] = '상품등록';
 $lang['seller_goods_3359'] = '진열';
 $lang['seller_goods_3369'] = '예약진열';
 $lang['seller_goods_3420'] = '시';
 $lang['seller_goods_3456'] = '분';
 $lang['seller_goods_3479'] = '미진열';
 $lang['seller_goods_3568'] = '완료';
 $lang['seller_goods_3571'] = '다음';
 $lang['seller_goods_add_ok'] = '등록 완료';
 $lang['seller_goods_add_next'] = '다음, 이미지 등록';
 $lang['seller_goods_fcode'] = '코드';
 $lang['seller_goods_fcode_down'] = '다운로드</a>&nbsp;&nbsp;F-코드';
 $lang['seller_goods_fcode_max'] = '생성수';
 $lang['seller_goods_fcode_help'] = '*F-코드 상품은 프로모션에 참석할 수 없습니다.<br />F-코드는 소비자한테 유일한 16자리 코드를 판매하여 문자로 발급한뒤 해당 매장에서 실제 상품을 우선으로 교환할 수 있는 기능입니다.';
 $lang['seller_goods_fcode_help1'] = '100이내의 유일한 고유번호를 생성할 수량을 입력하세요. 수정시 F-코드 새롭게 생성됩니다.';

 $lang['seller_goods_presell'] = '예약';
 $lang['seller_goods_presell_help'] = '*예약상품은 프로모션에 참석할 수 없습니다.<br />날자를 지정하여 미리 예약을 받은 후 해당 날자에 배송하는 기능입니다.';
 $lang['seller_goods_trans_date'] = '배송날자';

 $lang['seller_goods_adsetting'] = '고급설정';
 $lang['seller_goods_adsetting_help'] = '배송비를 0으로 설정시 소비자 페이지에서 배송비 무료로 노출됩니다.<br />고정금액: 고정금액 설정시 무게에 상관없이 지정된 금액으로 배송료가 노출됩니다.<br />고급설정: 템플릿 설정 페이지에서 확인해주세요.';



/* seller goods online list */
 $lang['seller_gonline_1'] = '您确定要执行批量生成二维码吗';
 $lang['seller_gonline_3'] = '平台货号';
 $lang['seller_gonline_4'] = '设置广告词';
 $lang['seller_gonline_5'] = '设置关联版式';
 $lang['seller_gonline_7'] = '点击展开查看此商品全部规格; 规格值过多时请横向拖动区域内的滚动条进行浏览';
 $lang['seller_gonline_9'] = '虚拟兑换商品';
 $lang['seller_gonline_10'] = '虚拟';
 $lang['seller_gonline_11'] = 'F码优先购买商品';
 $lang['seller_gonline_fcode'] = 'F码';
 $lang['seller_gonline_12'] = 'F商品页面二维码';
 $lang['seller_gonline_13'] = '预先发售商品';
 $lang['seller_gonline_14'] = '预售';
 $lang['seller_gonline_15'] = '预约销售提示商品';
 $lang['seller_gonline_16'] = '预约';
 $lang['seller_gonline_17'] = '店铺推荐商品';
 $lang['seller_gonline_18'] = '荐';
 $lang['seller_gonline_19'] = '手机端商品详情';
 $lang['seller_gonline_20'] = '商品页面二维码';
 $lang['seller_gonline_21'] = '下载标签';
 $lang['seller_gonline_22'] = '下载F码';
 $lang['seller_gonline_24'] = '该商品参加抢购活动期间不能进行编辑及删除等操作,可以编辑赠品和推荐组合';
 $lang['seller_gonline_25'] = '可以编辑赠品和推荐组合';
 $lang['seller_gonline_26'] = '锁定';
 $lang['seller_gonline_29'] = '提示';
 $lang['seller_gonline_31'] = '请选择需要操作的记录';
 $lang['seller_gonline_list'] = '판매중 상품';
 $lang['seller_gonline_edit'] = '편집';
 $lang['seller_gonline_image'] = '상품사진';
 $lang['seller_gonline_combo'] = '추천패키지';
 $lang['seller_gonline_selecate'] = '카테고리선택';
 $lang['seller_gonline_apply_type'] = '심사상태';
 $lang['seller_gonline_apply'] = '심사';


/* seller plate */
$lang['seller_plate_0'] = '添加关联版式';
$lang['seller_plate_2'] = '操作提示';
$lang['seller_plate_3'] = '1、关联版式可以把预设内容插入到商品描述的顶部或者底部，方便商家对商品描述批量添加或修改。';
$lang['seller_plate_5'] = '版式位置';
$lang['seller_plate_6'] = '请选择';
$lang['seller_plate_7'] = '版式名称';
$lang['seller_plate_list'] = '版式列表';
$lang['seller_plate_add'] = '추가';
$lang['seller_plate_edit'] = '편집';


/* seller spec */
 $lang['seller_spec_0'] = '操作提示';
 $lang['seller_spec_help1'] = '1、选择店铺经营的商品分类，以读取平台绑定的商品分类-规格类型，如分类：“服装”；规格：“颜色”、“尺码”等；当商品分类具有“颜色”规格时，可选择色块加以标识。';
 $lang['seller_spec_help2'] = '2、添加所属规格下的规格值，已有规格值可以删除，新增未保存的规格值可以移除；<font color="red">新增的规格值必须填写</font>，否则该行数据不会被更新或者保存。';
 $lang['seller_spec_help3'] = '3、可通过排序0-255改变规格值显示顺序；在发布商品时勾选已绑定的商品规格，还可对规格值进行“别名”修改操作，但不会影响规格值默认名称的设定。';
 $lang['seller_spec_26'] = '选择经营的商品分类';
 $lang['seller_spec_34'] = '编辑';
 $lang['seller_spec_34'] = '规格';
 $lang['seller_spec_36'] = '该分类不能添加规格';



 /* seller album */
$lang['seller_album_0'] = '选择文件：';
$lang['seller_album_watermark'] = '选择文件：';


/* plate add */
$lang['seller_plate_add_0'] = '版式名称';
$lang['seller_plate_add_01'] = '请填写版式名称';
$lang['seller_plate_add_1'] = '请输入10个字符内的名称，方便商品发布 / 编辑时选择使用。';
$lang['seller_plate_add_5'] = '版式位置';
$lang['seller_plate_add_6'] = '顶部';
$lang['seller_plate_add_7'] = '底部';
$lang['seller_plate_add_8'] = '选择关联版式插入到页面中的位置，选择“顶部”为商品详情上方内容，“底部”为商品详情下方内容。';
$lang['seller_plate_add_11'] = '为商品详情上方内容';
$lang['seller_plate_add_14'] = '版式内容';
$lang['seller_plate_add_114'] = '请填写版式内容';
$lang['seller_plate_add_15'] = '插入相册图片';
$lang['seller_plate_add_16'] = '关闭相册';
$lang['seller_plate_add_19'] = '版式名称不能超过10个字符';


/* order index */
$lang['seller_order_nodisplay'] = '不显示已关闭的订单';
$lang['seller_order_type'] = '交易状态';
$lang['seller_order_apply'] = '交易操作';
$lang['seller_order_print'] = '打印发货单';
$lang['seller_order_address'] = '地址';
$lang['seller_order_transport'] = '运费';
$lang['seller_order_edit_transport'] = '修改运费';
$lang['seller_order_edit_price'] = '修改价格';
$lang['seller_order_return_refund'] = '退款退货中';
$lang['seller_order_gift'] = '赠品';

/* vr order index */
$lang['seller_vrorder_changecode'] = '兑换兑换码';
$lang['seller_vrorder_nodisplay'] = '不显示已关闭的订单';
$lang['seller_vrorder_type'] = '交易状态';
$lang['seller_vrorder_apply'] = '交易操作';
$lang['seller_vrorder_groupbuy'] = '抢购';

$lang['seller_pay_finish'] = '已支付';


/* vr roder exchange */
$lang['seller_vrorder_exchange_0'] = '电子兑换码';
$lang['seller_vrorder_exchange_1'] = '请输入买家提供的电子兑换码';
$lang['seller_vrorder_exchange_2'] = '清除';
$lang['seller_vrorder_exchange_3'] = '后退';
$lang['seller_vrorder_exchange_4'] = '提交验证';
$lang['seller_vrorder_exchange_5'] = '请输入买家提供的兑换码,核对无误后提交,每个兑换码抵消单笔消费。';
$lang['seller_vrorder_exchange_8'] = '兑换码';
$lang['seller_vrorder_exchange_9'] = '商品';
$lang['seller_vrorder_exchange_10'] = '订单号';
$lang['seller_vrorder_exchange_11'] = '下单留言';
$lang['seller_vrorder_exchange_12'] = '兑换成功';

/* order deliver */

$lang['seller_odeliver_help1'] = '1、可以对待发货的订单进行发货操作，发货时可以设置收货人和发货人信息，填写一些备忘信息，选择相应的物流服务，打印发货单。';
$lang['seller_odeliver_help2'] = '2、已经设置为发货中的订单，您还可以继续编辑上次的发货信息。';
$lang['seller_odeliver_help3'] = '3、如果因物流等原因造成买家不能及时收货，您可使用点击延迟收货按钮来延迟系统的自动收货时间。';
$lang['seller_odeliver_gift'] = '赠品：';
$lang['seller_odeliver_delay'] = '延迟收货';

