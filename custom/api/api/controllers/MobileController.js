// dependencies external library
var Q = require("q");

// dependencies internal module(and service)
var debug = require("./libs/debug.js");
var SQLService = require('../services/SQLService');
var ErrorHandler = require('../services/ErrorHandler');
var QueryService = require('../services/QueryService');

module.exports = {
  adv_list: adv_list
};

// 모바일 광고 데이터 출력 컨트롤러
function adv_list(req, res) {
    // 컨트롤러 내부에서 사용할 전역변수 생성
    var MOBILE_TASK_MODEL = {
      MOBILE_ADV_QRY:QueryService.MOBILE_ADV_QRY
    };

    // debug 파라미터값을 잘못입력했을때 오류처리
    if (req.param('show')) {
        if (req.param('show') != 'debug') {
            var err_result = _createResultData(400);
            status = err_result.code;
            return res.send(status, debug.wrap(req, res, status, err_result));
        }
    }

   Q.fcall(_get_adv_data)   // DB에서 배너광고 데이터 생성
   .then ( // php에서 serialize함수로 처리된 데이터를 unserialize시키기
        function (adv_data) {
            // _getUnserializeData() 에는 serialize 형식의 값만 입력해야한다.
            var unserialize_adv_data_array = _getUnserializeData(adv_data[0].item_data);
            return unserialize_adv_data_array.item;
        }
    )
    .then ( // unserialize된 데이터를 가공하여 광고데이터만 리스트 배열로 생성
        function (adv_item_list) {
            var res = [];
            res = _addHostnameAtImage(adv_item_list);
            return res;
        }
    )
    .then ( // status 값과 함께 결과값을  json타입으로 레핑한후 리턴
        function (array_adv_data) {
          var result = _createResultData('success', array_adv_data);
          status = result.code;
          return res.send(status, debug.wrap(req, res, status, result));
        }
    )
    .catch(_taskDefaultError); // 오류발생시 예외처리


    /*
     * DB에서 배너광고 데이터 생성
     *
     * @return array 모바일 광고 데이터 리턴
     */
    function _get_adv_data() {
        return SQLService.queryOnPromise(
                    MOBILE_TASK_MODEL.MOBILE_ADV_QRY.BANNER_ADV,
                    []
        );
    }

    /* php에서 serialize처리된 데이터를 unserialize처리하기
     *
     * @param  data string  php에서 serialize처리된 데이터
     * @return array  배열로 변환된 데이터 리턴
     */
    function _getUnserializeData(data) {
        var PHPUnserialize = require('php-unserialize');
        var unserialize_data_array = PHPUnserialize.unserialize(data);
        return unserialize_data_array;
    }

    /* json으로 요청상태에따른 결과 데이터 생성
     *
     * @param  status string 출력할 결과 타입, err 400오류결과, err아니면 광고 데이터 생성
     * @param  data array  출력할 리스트배열 데이터
     * @return json  브라우저에 출력할 데이터 리턴
     */
    function _createResultData(status, data) {
      var result = {};

      if (status == 400) {
          result.code = 400;
      } else {
          result.code = 200;
          result.datas = {};
          result.datas.adv_list = {};
          result.datas.adv_list.item = data;
      }
      /*
      var result = {
                      code: 200,
                      datas: {
                          adv_list: {
                              item: array_adv_data
                          }
                      }
                   };
      */
      return result;
    }

    /*
     * 모바일 광고 데이터의 image 속성에 server hostname 추가하기
     *
     * @param adv_item_list array 루프처리할 광고데이터
     * @return array
     */
    function _addHostnameAtImage(adv_item_list) {
        var i, image_name, image_path, file_url;
        var res = [];
        var n = 0;
        var file_path = sails.config.url.site + "/data/upload/mobile/special"; // 광고 이미지 경로

        // 광고 데이터를 루프돌려서 image 속성에 hostname 추가
        for (i in adv_item_list) {
            image_name = adv_item_list[i].image.split("_");

            // image_name에  저장폴더명이 prefix로 붙어있는경우 잘라낸다.
            if (image_name.length == 2) {
                image_path = image_name[0];
            } else {
                image_path = "";
            }

            file_url = file_path + "/" + image_path + "/" + adv_item_list[i].image;
            adv_item_list[i].image = file_url;
            res[n] = adv_item_list[i];
            n++;
        }
        return res;
    }

    function _taskDefaultError(__err){
        sails.log.debug('_taskDefaultError ERROR: ' , __err);

        var err_code = __err.code || 'ERR_FAIL';
        var tmpStatus = __err.status || 500;
        var idx = __err.index || null;
        var message = __err.message || null;
        var key = (err_code==='ER_BAD_BLANK_ERROR' || err_code==='ER_UNDEFINED_PARAM_ERROR')?__err.key:null;

        var resultObj = {code:err_code};
        if(idx) resultObj.index = idx;
        if(key) resultObj.key = key;
        if(message) resultObj.message = message;

        res.status(tmpStatus);
        return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), resultObj ));
	}
}