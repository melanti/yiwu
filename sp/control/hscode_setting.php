<?php
/**
 * 번역관리
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class hscode_settingControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('hscode');
	}
	public function indexOp() {
        $this->hscode_article_class_listOp();
	}

    /**
     * hscode카테고리리스트
     **/
    public function hscode_article_class_listOp() {
        $model = Model('hscode');
        $list = $model->getList(TRUE);
        $this->show_menu('list');
        Tpl::output('list',$list);
        Tpl::showpage("hscode_setting.index");
    }

    /**
     * hscode카테고리추가
     **/
    public function addOp() {
        $this->show_menu('add');
        Tpl::showpage('hscode_setting.add');
    }

    /**
     * hscode카테고리저장
     **/
    public function hscode_class_saveOp() {
        $obj_validate = new Validate();
        $validate_array = array(
            array('input'=>$_POST['class_name'],'require'=>'true',"validator"=>"Length","min"=>"1","max"=>"255",'message'=>Language::get('class_name_error')),
            array('input'=>$_POST['class_sort'],'require'=>'true','validator'=>'Range','min'=>0,'max'=>255,'message'=>Language::get('class_sort_error')),
        );
        $obj_validate->validateparam = $validate_array;
        $error = $obj_validate->validate();
        if ($error != ''){
            showMessage(Language::get('error').$error,'','','error');
        }

        $param = array();
        $param['class_name'] = trim($_POST['class_name']);
        $param['class_sort'] = intval($_POST['class_sort']);
        $model_class = Model('hscode');
        $result = $model_class->save($param);
        if($result) {
            $this->log(Language::get('hscode_log_article_class_save').$result, 1);
            showMessage("성공",'index.php?act=hscode_setting&op=index');
        } else {
            $this->log(Language::get('hscode_log_article_class_save').$result, 0);
            showMessage(Language::get('class_add_fail'),'index.php?act=hscode_setting&op=index','','error');
        }


    }

    /**
     * hscode카테고리정렬修改
     */
    public function update_class_sortOp() {
        if(intval($_GET['id']) <= 0) {
            echo json_encode(array('result'=>FALSE,'message'=>Language::get('param_error')));
            die;
        }
        $new_sort = intval($_GET['value']);
        if ($new_sort > 255){
            echo json_encode(array('result'=>FALSE,'message'=>Language::get('class_sort_error')));
            die;
        } else {
            $model_class = Model("hscode");
            $result = $model_class->modify(array('class_sort'=>$new_sort),array('class_id'=>$_GET['id']));
            if($result) {
                echo json_encode(array('result'=>TRUE,'message'=>'class_add_success'));
                die;
            } else {
                echo json_encode(array('result'=>FALSE,'message'=>Language::get('class_add_fail')));
                die;
            }
        }
    }

    public function update_class_nameOp() {
        $class_id = intval($_GET['id']);
        if($class_id <= 0) {
            echo json_encode(array('result'=>FALSE,'message'=>Language::get('param_error')));
            die;
        }

        $new_name = trim($_GET['value']);
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array('input'=>$new_name,'require'=>'true',"validator"=>"Length","min"=>"1","max"=>"255",'message'=>Language::get('class_name_error')),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            echo json_encode(array('result'=>FALSE,'message'=>Language::get('class_name_error')));
            die;
	} else {
            $model_class = Model("hscode");
            $result = $model_class->modify(array('class_name'=>$new_name),array('class_id'=>$class_id));
            if($result) {
                echo json_encode(array('result'=>TRUE,'message'=>'class_add_success'));
                die;
            } else {
                echo json_encode(array('result'=>FALSE,'message'=>Language::get('class_add_fail')));
                die;
            }
        }

    }

    /**
     * hscode카테고리삭제
     **/
     public function hscode_article_class_dropOp() {
        $class_id = trim($_POST['class_id']);
        $model_class = Model('hscode');
        $condition = array();
        $condition['class_id'] = array('in',$class_id);
        $result = $model_class->drop($condition);
        if($result) {
            $this->log(Language::get('hscode_log_article_class_drop').$_POST['class_id'], 1);
            showMessage(Language::get('class_drop_success'),'');
        } else {
            $this->log(Language::get('hscode_log_article_class_drop').$_POST['class_id'], 0);
            showMessage(Language::get('class_drop_fail'),'','','error');
        }

     }

    private function show_menu($menu_key) {
        $menu_array = array(
            'list'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_list'),'menu_url'=>'index.php?act=hscode_setting&op=index'),
            'add'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_new'),'menu_url'=>'index.php?act=hscode_setting&op=add'),
        );
        $menu_array[$menu_key]['menu_type'] = 'text';
        Tpl::output('menu',$menu_array);
    }

}
