<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){
	gcategoryInitKo("gcategory");

    jQuery.validator.addMethod("seller_name_exist", function(value, element, params) { 
        var result = true;
        $.ajax({  
            type:"GET",  
            url:'<?php echo urlShop('store_joinin', 'check_seller_name_exist');?>',  
            async:false,  
            data:{seller_name: $('#seller_name').val()},  
            success: function(data){  
                if(data == 'true') {
                    $.validator.messages.seller_name_exist = "아이디가 이미 존재합니다.";
                    result = false;
                }
            }  
        });  
        return result;
    }, '');

    $('#form_store_info').validate({
        errorPlacement: function(error, element){
            element.nextAll('span').first().after(error);
        },
        rules : {
            seller_name: {
                required: true,
                maxlength: 50,
                seller_name_exist: true
            },
            seller_passwd: {
                required: true,
                maxlength: 20
            },
            store_name: {
                required: true,
                maxlength: 50 
            },
            sg_name: {
                required: true
            },
            sc_name: {
                required: true
            },
            store_class: {
                required: true,
                min: 1
            }
        },
        messages : {
            seller_name: {
                required: '아이디를 입력하세요',
                maxlength: jQuery.validator.format("최대 {0}자")
            },
            seller_passwd: {
                required: '비밀번호를 입력하세요',
                maxlength: jQuery.validator.format("최대 {0}자")
            },
            store_name: {
                required: '점포명을 입력하세요',
                maxlength: jQuery.validator.format("최대 {0}자")
            },
            sg_name: {
                required: '점포 등급을 선택하세요'
            },
            sc_name: {
                required: '점포 유형을 선택하세요'
            },
            store_class: {
                required: '경영항목을 선택하세요',
                min: '경영항목을 선택하세요'
            }
        }
    });

    $('#btn_select_category').on('click', function() {
        $('#gcategory').show();
        $('#btn_select_category').hide();
        $('#gcategory_class1').val(0).nextAll("select").remove();
    });

    $('#btn_add_category').on('click', function() {
        var tr_category = '<tr class="store-class-item">';
        var category_id = '';
        var category_name = '';
        var class_count = 0;
        var validation = true;
        $('#gcategory').find('select').each(function() {
            if(parseInt($(this).val(), 10) > 0) {
                var name = $(this).find('option:selected').text();
                tr_category += '<td>';
                tr_category += name;
                tr_category += '</td>';
                category_id += $(this).val() + ',';
                category_name += name + ',';
                class_count++;
            } else {
                validation = false;
            }
        });
        if(validation) {
            for(; class_count < 3; class_count++) {
                tr_category += '<td></td>';
            }
            tr_category += '<td><a cbtype="btn_drop_category" href="javascript:;">삭제</a></td>';
            tr_category += '<input name="store_class_ids[]" type="hidden" value="' + category_id + '" />';
            tr_category += '<input name="store_class_names[]" type="hidden" value="' + category_name + '" />';
            tr_category += '</tr>';
            $('#table_category').append(tr_category);
            $('#gcategory').hide();
            $('#btn_select_category').show();
            select_store_class_count();
        } else {
            showError('카테고리를 선택하세요');
        }
    });

    $('#table_category').on('click', '[cbtype="btn_drop_category"]', function() {
        $(this).parent('td').parent('tr').remove();
        select_store_class_count();
    });

    // 统计已经选择的经营类目
    function select_store_class_count() {
        var store_class_count = $('#table_category').find('.store-class-item').length;
        $('#store_class').val(store_class_count);
    }

    $('#btn_cancel_category').on('click', function() {
        $('#gcategory').hide();
        $('#btn_select_category').show();
    });

    $('#sg_id').on('change', function() {
        if($(this).val() > 0) {
            $('#grade_explain').text($(this).find('option:selected').attr('data-explain'));
            $('#sg_name').val($(this).find('option:selected').text());
        } else {
            $('#sg_name').val('');
        }
    });

    $('#sc_id').on('change', function() {
        if($(this).val() > 0) {
            $('#sc_name').val($(this).find('option:selected').text());
        } else {
            $('#sc_name').val('');
        }
    });


    $('#btn_apply_store_next').on('click', function() {
        if($('#form_store_info').valid()) {
            $('#form_store_info').submit();
        }
    });
});
</script>
<!-- 店铺信息 -->

<div id="apply_store_info" class="apply-store-info">
  <div class="alert">
    <h4>주의사항: </h4>
    경영항목은 상품 카테고리입니다, 실제 운영하시는 항목을 추가하세요.</div>
    <form id="form_store_info" action="index.php?act=store_joinin_c2c&op=step4" method="post" >
      <table border="0" cellpadding="0" cellspacing="0" class="all">
        <thead>
          <tr>
            <th colspan="20">운영정보</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th class="w150"><i>*</i>점포명: </th>
            <td><input name="store_name" type="text" class="w200"/>
              <span></span>
              <p class="emphasis">점포명은 입점후 수정불가합니다.</p></td>
          </tr>
          <tr>
            <th><i>*</i>점포등급: </th>
            <td><select name="sg_id" id="sg_id">
                <option value="0">선택하세요</option>
                <?php if(!empty($output['grade_list']) && is_array($output['grade_list'])){ ?>
                <?php foreach($output['grade_list'] as $k => $v){ ?>
                <?php $goods_limit = empty($v['sg_goods_limit'])?'무제한':$v['sg_goods_limit'];?>
                <?php $explain = '상품수: '.$goods_limit.', 스킨수: '.$v['sg_template_number'].', 가격: '.number_format($v['sg_price_ko']).'원, 부가기능: '.$v['function_str'];?>
                <option value="<?php echo $v['sg_id'];?>" data-explain="<?php echo $explain;?>"><?php echo $v['sg_name'];?> (<?php echo $explain;?>)</option>
                <?php } ?>
                <?php } ?>
              </select>
              <input id="sg_name" name="sg_name" type="hidden" />
              <span></span>
              <div id="grade_explain" class="grade_explain"></div></td>
          </tr>
          <tr>
            <th><i>*</i>점포유형: </th>
            <td><select name="sc_id" id="sc_id">
                <option value="0">선택하세요</option>
                <?php if(!empty($output['store_class']) && is_array($output['store_class'])){ ?>
                <?php foreach($output['store_class'] as $k => $v){ ?>
                <option value="<?php echo $v['sc_id'];?>"><?php echo $v['sc_name'];?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <input id="sc_name" name="sc_name" type="hidden" />
              <span></span>
              <p class="emphasis">실제 경영하고 있는 항목으로 선택해주세요, 입점후 수정불가입니다.</p></td>
          </tr>
          <tr>
            <th><i>*</i>경영항목: </th>
            <td><a href="###" id="btn_select_category" class="btn">+항목추가</a>
              <div id="gcategory" style="display:none;">
                <select id="gcategory_class1">
                  <option value="0">선택하세요</option>
                  <?php if(!empty($output['gc_list']) && is_array($output['gc_list']) ) {?>
                  <?php foreach ($output['gc_list'] as $gc) {?>
                  <option value="<?php echo $gc['gc_id'];?>"><?php echo $gc['gc_name_ko'];?></option>
                  <?php }?>
                  <?php }?>
                </select>
                <input id="btn_add_category" type="button" value="확인" />
                <input id="btn_cancel_category" type="button" value="취소" />
            </div>
              <input id="store_class" name="store_class" type="hidden" />
              <span></span>
          </td>
          </tr>
          <tr>
            <td colspan="2"><table border="0" cellpadding="0" cellspacing="0" id="table_category" class="type">
                <thead>
                  <tr>
                    <th>1차항목</th>
                    <th>2차항목</th>
                    <th>3차항목</th>
                    <th>설정</th>
                  </tr>
                </thead>
              </table></td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="20">&nbsp;</td>
          </tr>
        </tfoot>
      </table>
    </form>
    <div class="bottom"><a id="btn_apply_store_next" href="javascript:;" class="btn">등록</a></div>
</div>
