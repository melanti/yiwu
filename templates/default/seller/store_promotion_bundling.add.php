<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<style>
.pic_list .small_pic ul li {
	height: 100px;
}
.ui-sortable-helper {
	border: dashed 1px #F93;
	box-shadow: 2px 2px 2px rgba(153,153,153, 0.25);
	filter: alpha(opacity=75);
	-moz-opacity: 0.75;
	opacity: .75;
	cursor: ns-resize;
}
.ui-sortable-helper td {
	background-color: #FFC !important;
}
.ajaxload {
	display: block;
	width: 16px;
	height: 16px;
	margin: 100px 300px;
}
</style>
<input id="level2_flag" type="hidden" value="false" />
<input id="level3_flag" type="hidden" value="false" />
<div class="wrap" style="width:auto !important;">
  <div class="tabmenu">
    <?php include template('layout/submenu');?>
  </div>
  <div class="alert"> <strong><?php echo 'TIP'.$lang['nc_colon'];?></strong>
    <ul>
      <?php if(intval(C('promotion_bundling_sum')) != 0){?>
      <li>50가지의 할인세트 프로모션을 등록하실 수 있습니다. 
        <br>각 프로모션 당 최대 5개의 상품을 추가하실 수 있습니다.</br></li>
      <?php }else{?>
      <li><?php printf($lang['bundling_add_explain2'], C('promotion_bundling_goods_sum'));?></li>
      <?php }?>
      <li>지금 선택하시는 모든 할인 상품은, 해당 상품의 상세페이지에 할인세트로 등록이 됩니다.</li>
      <li>특수 상품은 프로모션에 등록하실 수 없습니다.</li>
    </ul>
  </div>
  <div class="ncsc-form-default"> 
    <!-- 说明 -->
    
    <form id="add_form" method="post" action="index.php?act=store_promotion_bundling&op=bundling_add">
      <input type="hidden" name="form_submit" value="ok" />
      <?php if (!empty($output['bundling_info'])){?>
      <input type="hidden" name="bundling_id" value="<?php echo $output['bundling_info']['bl_id'];?>" />
      <?php }?>
      <dl>
        <dt><i class="required">*</i><?php echo '프로모션명(한국어)'.$lang['nc_colon'];?></dt>
        <dd>
          <p>
            <input id="bundling_name_ko" name="bundling_name_ko" type="text" maxlength="25" class="w400 text" value="<?php echo $output['bundling_info']['bl_name_ko'];?>" />
            <span></span> </p>
          <p class="hint"><?php echo $lang['bundling_name_explain'];?></p>
        </dd>
      </dl>
      <dl>
        <dt><?php echo '프로모션명(중국어)'.$lang['nc_colon'];?></dt>
        <dd>
          <p>
            <input id="bundling_name" name="bundling_name" type="text" maxlength="25" class="w400 text" value="<?php echo $output['bundling_info']['bl_name'];?>" />
            <span></span> </p>
          <p class="hint"><?php echo $lang['bundling_name_explain'];?></p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i><?php echo '할인세트가격'.$lang['nc_colon'];?></dt>
        <dd>
          <input id="discount_price" name="discount_price" type="text" readonly style="background:#E7E7E7 none;" class="text w60 mr5" value="<?php echo $output['bundling_info']['bl_discount_price_ko'];?>"/>
          원 <span></span>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i><?php echo '상품'.$lang['nc_colon'];?></dt>
        <dd>
          <p>
            <input id="bundling_goods" type="hidden" value="" name="bundling_goods">
            <span></span></p>
          <table class="ncsc-default-table mb15">
            <thead>
              <tr>
                <th class="w70">할인선택</th>
                <th class="tl" colspan="2">상품명</th>
                <th class="w90">원가</th>
                <th class="w90">할인가격</th>
                <th class="w90">편집</th>
              </tr>
            </thead>
            <tbody cnbiztype="bundling_data"  class="bd-line tip" title="진열상품을 상하로 움직여 순서를 지정하실 수 있습니다. 편집, 삭제, 순서 등의 편집은 등록완료 후 적용됩니다.">
              <tr style="display:none;">
                <td colspan="20" class="norecord"><div class="no-promotion"><i class="zh"></i><span>할인세트 상품이 선택되지 않았습니다.</span></div></td>
              </tr>
              <?php if(!empty($output['b_goods_list'])){?>
              <?php foreach($output['b_goods_list'] as $val){?>
              <?php if (isset($output['goods_list'][$val['goods_id']])) {?>
              <tr id="bundling_tr_<?php echo $val['goods_id']?>" class="off-shelf">
                <input type="hidden" value="<?php echo $val['bl_goods_id'];?>" name="goods[<?php echo $val['goods_id'];?>][bundling_goods_id]" />
                <input type="hidden" value="<?php echo $val['goods_id'];?>" name="goods[<?php echo $val['goods_id'];?>][gid]" cnbiztype="goods_id">
                <td class="w70"><input type="checkbox" name="goods[<?php echo $val['goods_id'];?>][appoint]" value="1" <?php if ($val['bl_appoint'] == 1) {?>checked="checked"<?php }?>></td>
                <td class="w50"><div class="shelf-state"><div class="pic-thumb"><img src="<?php echo cthumb($output['goods_list'][$val['goods_id']]['goods_image'], 60, $_SESSION['store_id']);?>" ncname="<?php echo $output['goods_list'][$val['goods_id']]['goods_image'];?>" cnbiztype="bundling_data_img">
                    </div></div>
                </td>
                <td class="tl"><dl class="goods-name">
                    <dt style="width: 300px;"><?php echo $output['goods_list'][$val['goods_id']]['goods_name'];?></dt>
                  </dl></td>
                <td class="goods-price w90" cnbiztype="bundling_data_price"><?php echo number_format($output['goods_list'][$val['goods_id']]['goods_price_ko']);?>원</td>
                <td class="w90"><?php echo $val['goods_store_price'];?>
                  <input cnbiztype="price" type="text" value="<?php echo $val['bl_goods_price_ko'];?>" name="goods[<?php echo $val['goods_id'];?>][price]" onchange="count_price_sum();" class="text w70"></td>
                <td class="nscs-table-handle w90"><span><a onclick="bundling_operate_delete($('#bundling_tr_<?php echo $val['goods_id']?>'), <?php echo $val['goods_id']?>)" href="JavaScript:void(0);" class="btn-orange"><i class="icon-ban-circle"></i>
                  <p>삭제</p>
                  </a></span></td>
              </tr>
              <?php }?>
              <?php }?>
              <?php }?>
            </tbody>
          </table>
          <a id="bundling_add_goods" href="index.php?act=store_promotion_bundling&op=bundling_add_goods" class="ncsc-btn ncsc-btn-acidblue">상품추가</a>
          <div class="div-goods-select-box">
            <div id="bundling_add_goods_ajaxContent"></div>
            <a id="bundling_add_goods_delete" class="close" href="javascript:void(0);" style="display: none; right: -10px;">X</a></div>
        </dd>
      </dl>
      <dl>
        <dt><?php echo '배송료'.$lang['nc_colon'];?></dt>
        <dd>
        <ul class="ncsc-form-radio-list">
          <li><label for="whops_seller"><input id="whops_seller" type="radio" name="bundling_freight_choose" <?php if(!isset($output['bundling_info']) || $output['bundling_info']['bl_freight_choose'] == '1'){ ?>checked="checked"<?php }?> value="1" />판매자부담</label></li>
          <li><label for="whops_buyer"><input id="whops_buyer" type="radio" name="bundling_freight_choose" <?php if(isset($output['bundling_info']) && $output['bundling_info']['bl_freight_choose'] == '0'){ ?>checked="checked"<?php }?> value="0" />구매자부담(특급우편)</label>
          <div id="whops_buyer_box" class="transport_tpl" style="<?php if(!isset($output['bundling_info']) || $output['bundling_info']['bl_freight_choose'] == '1'){ ?>display:none;<?php }?>"><input class="w50 text" type="text" name="bundling_freight" value="<?php echo $output['bundling_info']['bl_freight'];?>" /><em class="add-on">원</em>
          </div>
          </li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt><?php echo '상태'.$lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li><label for="bundling_status_1">
              <input type="radio" name="state" value="1" id="bundling_status_1" <?php if(!isset($output['bundling_info']) || $output['bundling_info']['bl_state'] == 1) echo 'checked="checked"'; ?> />
              진행</label></li>
            <li><label for="bundling_status_0">
              <input type="radio" name="state" value="0" id="bundling_status_0" <?php if(isset($output['bundling_info']) && $output['bundling_info']['bl_state'] == 0) echo 'checked="checked"'; ?> />
              진행안함</label></li>
          </ul>
        </dd>
      </dl>
      <div class="bottom">
          <label class="submit-border"><input id="submit_button" type="submit" value="완료"  class="submit"></label>        
      </div>
    </form>
  </div>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script> 
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_bundling.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script> 
<script type="text/javascript">
var DEFAULT_GOODS_IMAGE = '<?php echo defaultGoodsImage(60);?>';
$(function(){
    jQuery.validator.addMethod('bundling_goods', function(value, element){
    	return $('tbody[cnbiztype="bundling_data"] > tr').length >2?true:false;
    });
	//Ajax提示
    $('.tip').poshytip({
    	className: 'tip-yellowsimple',
    	showTimeout: 1,
    	alignTo: 'target',
    	alignX: 'left',
    	alignY: 'top',
    	offsetX: 5,
    	offsetY: -78,
    	allowTipHover: false
    });
    $('.tip2').poshytip({
    	className: 'tip-yellowsimple',
    	showTimeout: 1,
    	alignTo: 'target',
    	alignX: 'right',
    	alignY: 'center',
    	offsetX: 5,
    	offsetY: 0,
    	allowTipHover: false
    });
    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.nextAll('span:first');
            error_td.append(error);
        },
     	submitHandler:function(form){
    		ajaxpost('add_form', '', '', 'onerror')
    	},
        rules : {
            bundling_name_ko : {
                required : true
            },
            bundling_goods : {
				bundling_goods : true
	        },
            discount_price : {
				required : true,
				number : true
            }
        },
        messages : {
            bundling_name_ko : {
                required : '<i class="icon-exclamation-sign"></i>프로모션명을 입력해주세요.'
            },
            bundling_goods : {
            	bundling_goods : '<i class="icon-exclamation-sign"></i>두가지 이상의 상품을 선택해주세요.'
            },
            discount_price : {
				required : '<i class="icon-exclamation-sign"></i>가격을 입력해주세요.',
				number : '<i class="icon-exclamation-sign"></i>입력이 잘못되었습니다.'
            }
        
        }
    });

	$('input[name="bundling_freight_choose"]').click(function(){
		if($(this).val() == '0'){
			$('#whops_buyer_box').show();
		}else{
			$('#whops_buyer_box').hide();
		}
	});

    check_bundling_data_length();
    <?php if(!empty($output['bundling_info'])){?>
    count_cost_price_sum(); // 计算商品原价
    count_price_sum();
    <?php }?>

    $('tbody[cnbiztype="bundling_data"]').on('change', 'input[cnbiztype="price"]', function(){
        count_price_sum();
    });
});


/* 삭제商品 */
function bundling_operate_delete(o, id){
	o.remove();
	check_bundling_data_length();
	$('li[cnbiztype="'+id+'"]').children(':last').html('<a href="JavaScript:void(0);" onclick="bundling_goods_add($(this))" class="ncsc-btn-mini ncsc-btn-green"><i class="icon-plus"></i><?php echo $lang['bundling_goods_add_bundling'];?></a>');
	count_cost_price_sum();
}

function check_bundling_data_length(){
	if ($('tbody[cnbiztype="bundling_data"] tr').length == 1) {
	    $('tbody[cnbiztype="bundling_data"]').children(':first').show();
	}
}
</script>