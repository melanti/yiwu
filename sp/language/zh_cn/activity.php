<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 公用
 */
$lang['activity_openstate']		= '상태';
$lang['activity_openstate_open']		= '열기';
$lang['activity_openstate_close']		= '닫기';
/**
 * 活动리스트
 */
$lang['activity_index']				= '프로모션';
$lang['activity_index_content']		= '프로모션내용';
$lang['activity_index_manage']		= '프로모션관리';
$lang['activity_index_title']		= '프로모션제목';
$lang['activity_index_type']		= '프로모션유형';
$lang['activity_index_banner']		= '배너';
$lang['activity_index_style']		= '스타일';
$lang['activity_index_start']		= '시작시간';
$lang['activity_index_end']		= '마감시간';
$lang['activity_index_goods']		= '상품';
$lang['activity_index_group']		= '공동구매';
$lang['activity_index_default']		= '기본스타일';
$lang['activity_index_long_time']	= '장기프로모션';
$lang['activity_index_deal_apply']	= '신청처리';
$lang['activity_index_help1']		= '프로모션 발표시 미니샵에서 프로모션 참석 가능합니다.';
$lang['activity_index_help2']		= '"메뉴" 모듈에서 프로모션 메뉴를 추가할 수 있습니다.';
$lang['activity_index_help3']		= '닫힌 프로모션 혹은 만기가된 프로모션만 삭제가능합니다.';
$lang['activity_index_help4']		= '프로모션 리스트 순위가 작을 수록 상단에 노출됩니다.';
$lang['activity_index_periodofvalidity']= '유효기간';
/**
 * 추가活动
 */
$lang['activity_new_title_null']	= '프로모션 제목을 입력하세요';
$lang['activity_new_style_null']	= '페이지 스타일을 선택하세요';
$lang['activity_new_type_null']		= '프로모션 유형을 선택하세요';
$lang['activity_new_sort_tip']		= '순위는 0~255이내의 숫자만 입력하세요';
$lang['activity_new_end_date_too_early']	= '만기시간은 반드시 시작시간보다 커야됩니다.';
$lang['activity_new_title_tip']		= '프로모션에 정확한 제목을 입력하세요';
$lang['activity_new_type_tip']		= '프로모션 유형을 선택하세요';
$lang['activity_new_start_tip']		= '공백시 프로모션는 바로 진행됩니다.';
$lang['activity_new_end_tip']		= '공백시 영구적으로 진행합니다.';
$lang['activity_new_banner_tip']	= '확장명: jpg、jpeg、gif、png';
$lang['activity_new_style']			= '페이지 스타일';
$lang['activity_new_style_tip']		= '본 이펜트 스타일을 선택하세요';
$lang['activity_new_desc']			= '프로모션 설명';
$lang['activity_new_sort_tip1']		= '0~255숫자를 입력하세요, 숫자는 작을 수록 상단에 노출됩니다.';
$lang['activity_new_sort_null']		= '정렬 순위를 입력하세요';
$lang['activity_new_sort_minerror']	= '0~255숫자';
$lang['activity_new_sort_maxerror']	= '0~255숫자';
$lang['activity_new_sort_error']	= '정렬 순위는 0~255숫자입니다.';
$lang['activity_new_banner_null']   = '배너 이미지를 선택하세요';
$lang['activity_new_ing_wrong']     = '확장명: png,gif,jpeg,jpg';
$lang['activity_new_startdate_null']   = '시작시간을 입력하세요';
$lang['activity_new_enddate_null']     = '마감시간을 입력하세요';

/**
 * 삭제活动
 */
$lang['activity_del_choose_activity']	= '프로모션를 선택하세요';
/**
 * 活动내용
 */
$lang['activity_detail_index_goods_name']	= '상품이름';
$lang['activity_detail_index_store']		= '미니샵';
$lang['activity_detail_index_auditstate']	= '심사상태';
$lang['activity_detail_index_to_audit']		= '심사중';
$lang['activity_detail_index_passed']		= '통과됨';
$lang['activity_detail_index_unpassed']		= '거절됨';
$lang['activity_detail_index_apply_again']	= '재신청';
$lang['activity_detail_index_pass']			= '통과';
$lang['activity_detail_index_refuse']		= '거절';
$lang['activity_detail_index_pass_all']		= '정말 선택된 정보를 심사하시겠습니까?';
$lang['activity_detail_index_refuse_all']	= '정말 선택된 정보를 거절하시겠습니까?';
$lang['activity_detail_index_tip1']	= '신청된 상품은 심사가 되지 않았거나 실패된 경우만 삭제 가능합니다.';
$lang['activity_detail_index_tip2']	= '본 페이지 신청된 상품은 비심사된 상품을 기준으로 노출됩니다, 정렬순위가 작을 수록 위측에 노출됩니다.';
$lang['activity_detail_index_tip3']	= '미진열, 위반 및 미니샵가 닫힌 프로모션는 노출되지 않습니다.';

/**
 * 活动내용삭제
 */
$lang['activity_detail_del_choose_detail']	= '프로모션 내용을 선택하세요(EX.상품 혹은 공동구매등)';