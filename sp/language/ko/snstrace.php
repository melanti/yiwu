<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * SNS功能公用
 */
$lang['admin_snstrace_collectgoods']		= '본상품 즐겨찾기추가';
$lang['admin_snstrace_collectstore']		= '본업체 즐겨찾기추가';
$lang['admin_snstrace_pleasechoose_del']		= '삭제항목 선택';
$lang['admin_snstrace_pleasechoose_edit']		= '수정항목 선택';
$lang['admin_snstrace_manage']					= '동태관리';
$lang['admin_snstrace_tracelist']				= '동태리스트';
$lang['admin_snstrace_commentlist']				= '댓글리스트';
$lang['admin_snstrace_comment']					= 'SNS동태';
$lang['admin_snstrace_pl']						= 'SNS댓글';

$lang['admin_snstrace_membername']				= '회원명';
$lang['admin_snstrace_content']					= '내용';
$lang['admin_snstrace_state']					= '상태';
$lang['admin_snstrace_stateshow']				= '노출';
$lang['admin_snstrace_statehide']				= '금지';
$lang['admin_snstrace_addtime']					= '작성시간';
$lang['admin_snstrace_commentlisttip']			= '댓글 정보가 불법적인 내용이 있으시면 금지 혹은 바로 삭제할 수 있습니다.';

$lang['admin_snstrace_tracelisttip1']			= '동태 정보에 불법적인 내용이 있으시면 금지 혹은 바로 삭제할 수 있습니다.';
$lang['admin_snstrace_tracelisttip2']			= '동태 삭제후 해당 댓글도 같이 삭제됩니다.';
$lang['admin_snstrace_originaldeleted']			= '원글이 삭제됨';
$lang['admin_snstrace_forward']	= '전달';
$lang['admin_snstrace_comment']	= '댓글';
$lang['admin_snstrace_privacytitle']			= '노출도';
$lang['admin_snstrace_privacy_all']				= '전체보기';
$lang['admin_snstrace_privacy_friend']			= '친구만';
$lang['admin_snstrace_privacy_self']			= '자신만';
