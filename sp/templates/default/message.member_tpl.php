<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['nc_message_set'];?></h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li>플랫폼에서 한개 혹은 다수의 통지 방법을 시동할 수 있습니다.</li>
            <li>문자,메일은 회원이 휴대폰 연동을 시행후 정상 받을 수 있습니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form name='form1' method='post'>
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="submit_type" id="submit_type" value="" />
    <table class="table tb-type2">
      <thead>
        <tr class="space">
          <th colspan="15" class="nobg"><?php echo $lang['nc_list'];?></th>
        </tr>
        <tr class="thead">
          <th>&nbsp;</th>
          <th><?php echo $lang['mailtemplates_index_desc'];?></th>
          <th class="align-center">쪽지</th>
          <th class="align-center">문자</th>
          <th class="align-center">메일</th>
          <th class="align-center"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!empty($output['mmtpl_list'])){?>
        <?php foreach($output['mmtpl_list'] as $val){?>
        <tr class="hover">
          <td class="w24">&nbsp;</td>
          <td class="w25pre"><?php echo $val['mmt_name']; ?></td>
          <td class="align-center"><?php echo ($val['mmt_message_switch']) ? '시동' : '닫기';?></td>
          <td class="align-center"><?php echo ($val['mmt_short_switch']) ? '시동' : '닫기';?></td>
          <td class="align-center"><?php echo ($val['mmt_mail_switch']) ? '시동' : '닫기';?></td>
          <td class="w60 align-center"><a href="<?php echo urlAdmin('message', 'member_tpl_edit', array('code' => $val['mmt_code']));?>"><?php echo $lang['nc_edit'];?></a></td>
        </tr>
        <?php } ?>
        <?php } ?>
      </tbody>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.edit.js" charset="utf-8"></script> 
<script type="text/javascript">
function go(){
	var url="index.php?act=message&op=email_tpl_ajax";
	document.form1.action = url;
	document.form1.submit();
}
</script>