(function ($, angular, _, APP) {

  APP.modules.ctrl.tableBtnBasicCtrl = tableBtnBasicCtrl;

  //btnCtrl
  function tableBtnBasicCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO) {
    var ctrl = this;

    ctrl.viewOption;
    ctrl.apiOption;
    ctrl.notSelectedStr;
    ctrl.deleteRow;
    ctrl.locationUrl;

    ctrl.show = _show;
    ctrl.changeDelete = _changeDelete;

    _init();

    function _init() {
      $scope.$on('$destroy', function () {
      });
    }

    function _show(str){
      return ctrl.viewOption.indexOf(str) != -1;
    }

    function _changeDelete() {
      var _options = JSON.parse(ctrl.apiOption);
      ctrl.deleteRow({
        options: {
          kind: _options.kind,
          status: _options.status,
          show: _options.show,
          notSelectedStr: ctrl.notSelectedStr
        }
      });
    }
  }

  tableBtnBasicCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO'];
  tableBtnBasicCtrl.bindToController = {
    btnAddName:'@',
    viewOption:'@',
    apiOption: '@',
    notSelectedStr:'@',
    deleteRow: '&',
    locationUrl: '@'
  };
  tableBtnBasicCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap', {paginationCtrl: true});
    }
  }


})(window.jQuery, window.angular, window._, window.APP);
