<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form id="add_form" action="index.php?act=store_promotion_mansong&op=mansong_quota_add_save" method="post">
    <dl>
      <dt><i class="required">*</i><?php echo '패키지구매수량'.$lang['nc_colon'];?></dt>
      <dd>
          <input id="mansong_quota_quantity" name="mansong_quota_quantity" type="text" class="text w50"/><em class="add-on">월</em>
          <span></span>
        <p class="hint">구매단위 : 월(30일), 한번 구매시 최대 12개월. 구매 후, 금액별 사은품 증정 이벤트를 등록하실 수 있습니다. </p>
        <p class="hint"><?php echo $output['setting_config']['promotion_mansong_price'].'원/월';?></p>
        <p class="hint"><strong style="color: red">관련 비용은 정산시 차감됩니다. 
</strong></p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input id="submit_button" type="submit" value="완료" class="submit"></label>
    </div>
  </form>
</div>
<script>
$(document).ready(function(){
    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span');
            error_td.append(error);
        },
     	submitHandler:function(form){
            var unit_price = <?php echo $output['setting_config']['promotion_mansong_price'];?>;
            var quantity = $("#mansong_quota_quantity").val();
            var price = unit_price * quantity;
             showDialog('<?php echo $lang['mansong_quota_add_confirm'];?>'+price+'원', 'confirm', '', function(){ajaxpost('add_form', '', '', 'onerror');});
    	},
        rules : {
            mansong_quota_quantity : {
                required : true,
                digits : true,
                min : 1,
                max : 12
            }
        },
        messages : {
            mansong_quota_quantity : {
                required : '<i class="icon-exclamation-sign"></i>1~12사이의 정수를 입력해 주세요.',
                digits : '<i class="icon-exclamation-sign"></i>1~12사이의 정수를 입력해 주세요.',
                min : '<i class="icon-exclamation-sign"></i>1~12사이의 정수를 입력해 주세요.',
                max : '<i class="icon-exclamation-sign"></i>1~12사이의 정수를 입력해 주세요.'
            }
        }
    });
});
</script>
