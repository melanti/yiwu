<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['admin_log_man']		= '설정인';
$lang['admin_log_do']			= '행위';
$lang['admin_log_dotime']		= '시간';
$lang['admin_log_ago_1zhou']		= '1주전';
$lang['admin_log_ago_1month']	= '1개월전';
$lang['admin_log_ago_2month']	= '2개월전';
$lang['admin_log_ago_3month']	= '3개월전';
$lang['admin_log_ago_6month']	= '6개월전';
$lang['admin_log_tips1']		= '설정일지를 닫은 상태입니다. admin/config/config.ini.php: $config[\'sys_log\'] = true;에서 설정해주세요.';
$lang['admin_log_tips2']		= '설정일지를 시동시 관리자의 설정을 기록합니다.';

