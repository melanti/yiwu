<?php
/**
 * E-쿠폰 공동구매관리
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');

class vr_groupbuyControl extends SystemControl
{
    public function __construct()
    {
        parent::__construct();

        // 检查공동구매功能예否시동
        if (C('groupbuy_allow') != 1) {
            showMessage('E-쿠폰 공동구매 기능 미시동', 'index.php?act=dashboard&op=welcome', 'html', 'error');
        }
    }

    public function indexOp()
    {
        $this->class_listOp();
    }

    /*
     * 리스트카테고리
     */
    public function class_listOp()
    {
        $model_vr_groupbuy_class = Model('vr_groupbuy_class');
        $list = $model_vr_groupbuy_class->getVrGroupbuyClassList();
        Tpl::output('list', $list);
        Tpl::showpage('vr_groupbuy.class_list');
    }

    /*
     * 카테고리추가
     */
    public function class_addOp()
    {
        if (chksubmit()) { //추가E-쿠폰 공동구매카테고리
            // 数据验证
            $obj_validate = new Validate();
            $validate_array = array(
                array('input'=>$_POST['class_name'],'require'=>'true',"validator"=>"Length","min"=>"1","max"=>"10",'message'=>Language::get('groupbuy_class_name_is_not_null')),
                array('input'=>$_POST['class_name'],'require'=>'true','validator'=>'Range','min'=>0,'max'=>255,'message'=>Language::get('groupbuy_class_sort_is_not_null')),
            );
            $obj_validate->validateparam = $validate_array;
            $error = $obj_validate->validate();
            if ($error != '') {
                showMessage(Language::get('error').$error, '', '', 'error');
            }

            $params = array();
            $params['class_name'] = trim($_POST['class_name']);
            $params['class_sort'] = intval($_POST['class_sort']);
            if (isset($_POST['parent_class_id']) && intval($_POST['parent_class_id']) > 0) {
                $params['parent_class_id'] = $_POST['parent_class_id'];
            } else {
                $params['parent_class_id'] = 0;
            }

            $model_vr_groupbuy_class = Model('vr_groupbuy_class');
            $res = $model_vr_groupbuy_class->addVrGroupbuyClass($params); //카테고리추가
            if ($res) {
                // 삭제E-쿠폰 공동구매카테고리缓存
                Model('groupbuy')->dropCachedData('groupbuy_vr_classes');

                $this->log('추가E-쿠폰 공동구매카테고리[ID:'.$res.']', 1);

                $url = array(
                    array(
                        'url'=>'index.php?act=vr_groupbuy&op=class_add&parent_class_id='.$params['parent_class_id'],
                        'msg'=>'계속 추가',
                    ),
                    array(
                        'url'=>'index.php?act=vr_groupbuy&op=class_list',
                        'msg'=>'돌아가기',
                    )
                );
                showMessage('추가성공', $url);
            } else {
                showMessage('추가실패', 'index.php?act=vr_groupbuy&op=class_list', '', 'error');
            }
        }

        $model_vr_groupbuy_class = Model('vr_groupbuy_class'); //1급카테고리
        $list = $model_vr_groupbuy_class->getVrGroupbuyClassList(array('parent_class_id'=>0));
        Tpl::output('list', $list);

        Tpl::output('parent_class_id', isset($_GET['parent_class_id']) ? intval($_GET['parent_class_id']) : 0);
        Tpl::showpage('vr_groupbuy.class_add');
    }

    /*
     * 수정카테고리
     */
    public function class_editOp()
    {
        if (chksubmit()) {
            // 数据验证
            $obj_validate = new Validate();
            $validate_array = array(
                array('input'=>$_POST['class_name'],'require'=>'true',"validator"=>"Length","min"=>"1","max"=>"10",'message'=>Language::get('groupbuy_class_name_is_not_null')),
                array('input'=>$_POST['class_sort'],'require'=>'true','validator'=>'Range','min'=>0,'max'=>255,'message'=>Language::get('groupbuy_class_sort_is_not_null')),
            );
            $obj_validate->validateparam = $validate_array;
            $error = $obj_validate->validate();
            if ($error != '') {
                showMessage(Language::get('error').$error, '', '', 'error');
            }

            $params = array();
            $params['class_name'] = trim($_POST['class_name']);
            $params['class_sort'] = intval($_POST['class_sort']);
            if (isset($_POST['parent_class_id']) && intval($_POST['parent_class_id']) > 0) {
                $params['parent_class_id'] = $_POST['parent_class_id'];
            } else {
                $params['parent_class_id'] = 0;
            }

            $condition  = array(); //条件
            $condition['class_id'] = intval($_POST['class_id']);

            $model_vr_groupbuy_class = Model('vr_groupbuy_class');
            $res = $model_vr_groupbuy_class->editVrGroupbuyClass($condition,$params);

            if ($res) {
                // 삭제E-쿠폰 공동구매카테고리缓存
                Model('groupbuy')->dropCachedData('groupbuy_vr_classes');

                $this->log('수정E-쿠폰 공동구매카테고리[ID:'.intval($_POST['class_id']).']', 1);
                showMessage('수정성공', 'index.php?act=vr_groupbuy&op=class_list', '', 'succ');
            } else {
                showMessage('수정실패', 'index.php?act=vr_groupbuy&op=class_list', '', 'error');
            }
        }

        $model_vr_groupbuy_class = Model('vr_groupbuy_class'); //카테고리信息
        $class = $model_vr_groupbuy_class->getVrGroupbuyClassInfo(array('class_id'=>intval($_GET['class_id'])));
        if (empty($class)) {
            showMessage('카테고리가 존재하지 않습니다.', '', '', 'error');
        }
        Tpl::output('class', $class);


        $list = $model_vr_groupbuy_class->getVrGroupbuyClassList(array('parent_class_id'=>0));
        Tpl::output('list', $list);

        Tpl::showpage('vr_groupbuy.class_edit');
    }

    /*
     * 삭제카테고리
     */
    public function class_delOp()
    {
        if (chksubmit()) {
            $classidArr = explode(",", $_POST['class_id']);
            if (!empty($classidArr)) {
                $model = Model();
                foreach ($classidArr as $val) {
                    $class = $model->table('vr_groupbuy_class')->where(array('class_id'=>$val))->find();
                    if ($class['parent_class_id'] == 0) {
                        $model->table('vr_groupbuy_class')->where(array('parent_class_id'=>$class['class_id']))->delete();
                    }
                    $model->table('vr_groupbuy_class')->where(array('class_id'=>$val))->delete();
                }
            }
        }

        // 삭제E-쿠폰 공동구매카테고리缓存
        Model('groupbuy')->dropCachedData('groupbuy_vr_classes');

        $this->log('E-쿠폰 공동구매 카테고리 삭제[ID:'.$_POST['class_id'].']', 1);
        showMessage('삭제성공', 'index.php?act=vr_groupbuy&op=class_list', '', 'succ');
    }

    public function ajaxOp()
    {
        $field = $_GET['column'];
        $id = $_GET['id'];
        $value = $_GET['value'];

        switch ($_GET['column']) {
            case 'class_name':
                if (mb_strlen((string) $value, 'utf-8') > 10)
                    return;
                break;
            case 'class_sort':
                if ($value < 0 || $value > 255)
                    return;
                break;

            default:
                return;
        }

        switch ($_GET['branch']) {
            case 'class':
                $model_vr_groupbuy_class = Model('vr_groupbuy_class');
                $res = $model_vr_groupbuy_class->editVrGroupbuyClass(array('class_id'=>$id),array($field=>$value));
                if ($res) {
                    // 삭제E-쿠폰 공동구매카테고리缓存
                    Model('groupbuy')->dropCachedData('groupbuy_vr_classes');

                    $this->log('E-쿠폰 공동구매 카테고리 수정[ID:'.$id.']', 1);
                    echo 'true';
                } else {
                    echo 'false';
                }
                exit;

            default:
                return;
        }
    }

    /*
     * 지역리스트
     */
    public function area_listOp()
    {
        $condition = array(); // 搜索条件
        $condition['parent_area_id'] = 0;
        if (strlen($area_name = trim($_GET['area_name']))) {
            $condition['area_name'] = array('like', "%{$area_name}%");
            Tpl::output('area_name', $area_name);
        }

        if (isset($_GET['first_letter']) && !empty($_GET['first_letter'])) {
            $condition['first_letter'] = $_GET['first_letter'];
            Tpl::output('first_letter', $_GET['first_letter']);
        }

        $model_vr_groupbuy_area = Model('vr_groupbuy_area');
        $area = $model_vr_groupbuy_area->getVrGroupbuyAreaList($condition);
        Tpl::output('list', $area); //지역리스트
        Tpl::output('show_page', $model_vr_groupbuy_area->showpage());

        // 城市첫글자
        Tpl::output('letter', $this->letterArr);

        Tpl::showpage("vr_groupbuy.area_list");
    }

    /*
     * 지역추가
     */
    public function area_addOp()
    {
        if (isset($_POST) && !empty($_POST)) {
            // 数据验证
            $obj_validate = new Validate();
            $validate_array = array(
                array('input'=>$_POST['area_name'],'require'=>'true','message'=>'지역이름을 입력하세요'),
                array('input'=>$_POST['first_letter'],'require'=>'true','message'=>'첫글자를 입력하세요'),
            );
            $obj_validate->validateparam = $validate_array;
            $error = $obj_validate->validate();
            if ($error != '') {
                showMessage(Language::get('error').$error, '', '', 'error');
            }

            $params = array(
                'area_name' => trim($_POST['area_name']),
                'parent_area_id'=> isset($_POST['parent_area_id']) && !empty($_POST['parent_area_id']) ? $_POST['parent_area_id'] : 0,
                'add_time' => time(),
                'first_letter' => $_POST['first_letter'],
                'area_number' => trim($_POST['area_number']),
                'post' => trim($_POST['post']),
                'hot_city' => intval($_POST['is_hot'])
            );

            $model_vr_groupbuy_area = Model('vr_groupbuy_area');
            $res = $model_vr_groupbuy_area->addVrGroupbuyArea($params);

            if ($res) {
                // E-쿠폰 공동구매 지역 삭제缓存
                Model('groupbuy')->dropCachedData('groupbuy_vr_cities');

                $this->log('추가E-쿠폰 공동구매지역[ID:'.$res.']',1);
                showMessage('추가성공','index.php?act=vr_groupbuy&op=area_list','','succ');
            } else {
                showMessage('추가실패','index.php?act=vr_groupbuy&op=area_list','','error');
            }
        }

        // 城市첫글자
        Tpl::output('letter', $this->letterArr);

        if (isset($_GET['area_id'])) {
            $model_vr_groupbuy_area = Model('vr_groupbuy_area');
            $area = $model_vr_groupbuy_area->getVrGroupbuyAreaInfo(array('area_id'=>intval($_GET['area_id'])));

            Tpl::output('area_name', $area['area_name']);
            Tpl::output('area_id', $area['area_id']);
        } else {
            Tpl::output('area_name', Language::get('area_first_area'));
            Tpl::output('area_id', 0);
        }
        Tpl::showpage("vr_groupbuy.area_add");
    }

    /*
     * 수정지역
     */
    public function area_editOp()
    {
        if (isset($_POST) && !empty($_POST)) {
            //数据验证
            $obj_validate = new Validate();
            $validate_array = array(
                array('input'=>$_POST['area_name'],'require'=>'true','message'=>'지역이름을 입력하세요'),
                array('input'=>$_POST['first_letter'],'require'=>'true','message'=>'첫글자를 입력하세요'),
            );
            $obj_validate->validateparam = $validate_array;
            $error = $obj_validate->validate();
            if ($error != '') {
                showMessage(Language::get('error').$error,'','','error');
            }

            $params = array(
                'area_name' => trim($_POST['area_name']),
                'add_time' => time(),
                'first_letter' => $_POST['first_letter'],
                'area_number' => trim($_POST['area_number']),
                'post' => trim($_POST['post']),
                'hot_city' => intval($_POST['is_hot'])
            );

            $condition = array();
            $condition['area_id'] = intval($_POST['area_id']);

            $model_vr_groupbuy_area = Model('vr_groupbuy_area');
            $res = $model_vr_groupbuy_area->editVrGroupbuyArea($condition,$params);
            if ($res) {
                // E-쿠폰 공동구매 지역 삭제缓存
                Model('groupbuy')->dropCachedData('groupbuy_vr_cities');

                $this->log('수정E-쿠폰 공동구매지역[ID:'.intval($_POST['area_id']).']', 1);
                showMessage('수정성공', 'index.php?act=vr_groupbuy&op=area_list', '', 'succ');
            } else {
                showMessage('수정실패', 'index.php?act=vr_groupbuy&op=area_list', '', 'error');
            }
        }

        //城市첫글자
        Tpl::output('letter', $this->letterArr);

        $model_vr_groupbuy_area = Model('vr_groupbuy_area');

        $model = Model();
        $area = $model->table('vr_groupbuy_area')->where(array('area_id'=>intval($_GET['area_id'])))->find();
        Tpl::output('area',$area);

        $parent_area = $model->table('vr_groupbuy_area')->where(array('area_id'=>$area['parent_area_id']))->find();
        if(!empty($parent_area)){
            Tpl::output('parent_area_name',$parent_area['area_name']);
        }else{
            Tpl::output('parent_area_name',Language::get('area_first_area'));
        }

        Tpl::showpage("vr_groupbuy.area_edit");
    }

    /*
     * 지역보기
     */
    public function area_viewOp()
    {
        //获取지역信息
        $model = Model();
        $area_list = $model->table('vr_groupbuy_area')->where(array('parent_area_id'=>intval($_GET['parent_area_id'])))->select();
        Tpl::output('show_page', $model->showpage());
        Tpl::output('list', $area_list);

        $area = $model->table('vr_groupbuy_area')->where(array('area_id'=>intval($_GET['parent_area_id'])))->find();
        Tpl::output('parent_area', $area);
        Tpl::showpage("vr_groupbuy.area_view");
    }

    /*
     * 상품구역 보기
     */
    public function area_streetOp()
    {
        //获取지역信息
        $model = Model();
        $mall_list = $model->table('vr_groupbuy_area')->where(array('parent_area_id'=>intval($_GET['parent_area_id'])))->select();
        Tpl::output('show_page', $model->showpage());
        Tpl::output('list', $mall_list);

        $mall = $model->table('vr_groupbuy_area')->where(array('area_id'=>intval($_GET['parent_area_id'])))->find();
        Tpl::output('parent_area', $mall);

        Tpl::showpage("vr_groupbuy.area_street");
    }

    /*
     * 삭제지역
     */
    public function area_dropOp()
    {
        $model = Model();
        $res = $model->table('vr_groupbuy_area')->where(array('area_id'=>array('in',intval($_POST['area_id']))))->delete();

        if ($res) {
            // E-쿠폰 공동구매 지역 삭제缓存
            Model('groupbuy')->dropCachedData('groupbuy_vr_cities');

            $this->log('E-쿠폰 공동구매 지역 삭제[ID:'.intval($_POST['area_id']).']',1);
            showMessage('삭제성공','index.php?act=vr_groupbuy&op=area_list','','succ');
        } else {
            showMessage('삭제실패','index.php?act=vr_groupbuy&op=area_list','','error');
        }
    }

    protected $letterArr = array(
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
    );
}
