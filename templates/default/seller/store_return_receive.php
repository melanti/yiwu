<div class="eject_con">
  <div id="warning" class="alert alert-error"></div>
  <form action="index.php?act=store_return&op=receive&return_id=<?php echo $output['return']['refund_id']; ?>" method="post" id="post_form">
    <input type="hidden" name="form_submit" value="ok" />
	  <dl>
	    <dt><?php echo '등록시간'.$lang['nc_colon'];?></dt>
	    <dd> <?php echo date("Y-m-d H:i:s",$output['return']['delay_time']); ?> </dd>
	  </dl>
	  <dl>
	    <dt><?php echo '배송정보'.$lang['nc_colon'];?></dt>
	    <dd> <?php echo $output['e_name'].' , '.$output['return']['invoice_no']; ?> </dd>
	  </dl>
    <dl>
      <dt><i class="required">*</i><?php echo '수취상태'.$lang['nc_colon'];?></dt>
      <dd><select name="return_type">
          <option value="">선택</option>
          <option value="4"><?php echo '수령'; ?></option>
          <?php if ($output['delay_time'] > 0) { ?>
          <option value="3"><?php echo '미수령'; ?></option>
          <?php } ?>
        </select>
        <p class="hint">발송 <?php echo $output['return_delay'];?>후부터 미수령을 선택하실 수 있으며 구매자가 시간을 연장할 수 있습니다. 또한, <?php echo $output['return_confirm'];?>일을 초과한 경우 취소처리됩니다.
      </dd>
    </dl>
    <div class="bottom">
        <label class="submit-border"><input type="submit" class="submit" id="confirm_yes" value="확인" /></label>
    </div>
  </form>
</div>
<script type="text/javascript">
$(function(){
    $('#post_form').validate({
        errorLabelContainer: $('#warning'),
        invalidHandler: function(form, validator) {
               $('#warning').show();
        },
         submitHandler: function(form) {
			    	ajaxpost('post_form', '', '', 'onerror');
				 },
        rules : {
            return_type : {
                required   : true
            }
        },
        messages : {
            return_type  : {
                required  : '<i class="icon-exclamation-sign"></i><?php echo '수취상태선택';?>'
            }
        }
	    });
});
</script>