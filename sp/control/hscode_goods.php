<?php
/**
 * 번역관리
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class translate_goodsControl extends SystemControl{
    public function __construct(){
        parent::__construct();
        Language::read('translate');
    }

    public function indexOp(){
        $model_goods = Model ( 'goods' );
        /**
         * 상품 카테고리
         */
        $choose_gcid = ($t = intval($_REQUEST['choose_gcid']))>0?$t:0;
        $gccache_arr = Model('goods_class')->getGoodsclassCache($choose_gcid,3);
	    Tpl::output('gc_json',json_encode($gccache_arr['showclass']));
		Tpl::output('gc_choose_json',json_encode($gccache_arr['choose_gcid']));
        /**
         * 검색조건
         */
        $where = array();
        if ($_GET['search_goods_name'] != '') {
            $where['goods_name_ko'] = array('like', '%' . trim($_GET['search_goods_name']) . '%');
        }
        if (intval($_GET['search_commonid']) > 0) {
            $where['goods_commonid'] = intval($_GET['search_commonid']);
        }
        if ($_GET['search_store_name'] != '') {
            $where['store_name'] = array('like', '%' . trim($_GET['search_store_name']) . '%');
        }
        if (intval($_GET['b_id']) > 0) {
            $where['brand_id'] = intval($_GET['b_id']);
        }
        if ($choose_gcid > 0){
            $where['gc_id_'.($gccache_arr['showclass'][$choose_gcid]['depth'])] = $choose_gcid;
        }

        if (in_array($_GET['search_verify'], array(1,2,3,4,5,6,7,8,9,10,11,12,13))) {
            $where['t_status'] = $_GET['search_verify'];
        }

        $goods_list = $model_goods->getGoodsCommonListForTranslate($where);

        Tpl::output('goods_list', $goods_list);
        Tpl::output('page', $model_goods->showpage(10));

        $storage_array = $model_goods->calculateStorage($goods_list);
        Tpl::output('storage_array', $storage_array);

        // 브랜드
        $brand_list = Model('brand')->getBrandPassedList(array());
        Tpl::output('search', $_GET);
        Tpl::output('brand_list', $brand_list);


        Tpl::output('verify', array(
            '1' => '번역중',
            '2' => '디자인중',
            '3' => '번역디자인중',
            '4' => '번역완료',
            '5' => '디자인완료',
            '6' => '번+디완료',
            '7' => '번역승인완료',
            '8' => '디자인승인완료',
            '9' => '번역디자인승인완료',
            '10' => '최종승인완료',
            '11' => '위반',
            '12'=>'미분배'
        ));
        Tpl::output('ownShopIds', array_fill_keys(Model('store')->getOwnShopIds(), true));

        Tpl::showpage('translate_goods.index');
    }

    /**
     * 번역심사
     */
    public function translate_verifyOp() {
        if (chksubmit()) {
            $model_goods = Model('goods');
            $aid = intval($_POST['aid']);

            $rs = $model_goods->getTranslateRow(array('aid'=>$aid));
            if(!$rs):
                showDialog("오류", 'reload');
                exit;
            endif;

            switch ($rs['t_type']) {
                // 禁售
                case 1:
                    $rStatus = 7;
                    break;
                // 심사대기중
                case 2:
                    $rStatus = 8;
                    break;
                // 심사대기중
                case 3:
                    $rStatus = 9;
                    break;
                // 전체상품
                default:
                    $rStatus = 9;
                    break;
            }


            $update = array();


            if (intval($_POST['verify_state']) == 0) {
                $update['t_status'] = 11;
                $update['t_memo'] = trim($_POST['verify_reason']);
            } else {
                $update['t_status'] = $rStatus;
            }            
            $update['t_verifytime'] = time();

            $where = array();
            $where['aid'] = $aid;


            $model_goods->editTranslateById($update,$where);
            showDialog("심사완료", 'reload', 'succ');
            exit;
        }
        Tpl::output('aid', $_GET['aid']);
        Tpl::output('commonids', $_GET['id']);
        Tpl::showpage('translate.verify', 'null_layout');
    }


    /**
     * 번역심사
     */
    public function translate_assignOp() {
        $model = Model();
        if (chksubmit()) {
            $pstUserId = intval($_POST['user_ass']);
            $pstCommonids = intval($_POST['commonids']);
            $admin_info = $model->table('admin')->where(array('admin_id'=>$pstUserId))->find();
            $model_goods = Model('goods');

            $insert = array();

            $insert['t_commonid'] = $pstCommonids;
            $insert['t_user'] = $admin_info['admin_name'];
            $insert['t_uid'] = $admin_info['admin_id'];
            $insert['t_price'] = $admin_info['admin_trans_price'];
            $insert['t_type'] = $admin_info['admin_trans_type'];
            $insert['t_addtime'] = time();

            switch($admin_info['admin_trans_type'])
            {
                case 1:
                    $strTtype = 1;
                    break;
                case 2:
                    $strTtype = 2;
                    break;
                case 3:
                    $strTtype = 3;
                    break;
                default:
                    $strTtype = 1;
                    break;
            }

            $insert['t_status'] = $strTtype;


            $condition['t_commonid'] = $pstCommonids;
            $comrs = $model_goods->getTranslateRow($condition);
			if($comrs):
				$w['aid'] = $comrs['aid'];
				$model_goods->editTranslateById($insert,$w);
			else:
				$model_goods->addTranslate($insert);
			endif;


            showDialog("분배완료", 'reload', 'succ');
            exit;
        }
        $admin_list = $model->table('admin')->where(array('admin_gid'=>4))->select();

        $trans_list = array();
        foreach($admin_list as $k=>$v)
        {
            $condition = array();
            $condition['t_uid'] = $v['admin_id'];

            switch($v['admin_trans_type'])
            {
                case 1:
                    $strTtype1 = 1;
                    break;
                case 2:
                    $strTtype1 = 2;
                    break;
                case 3:
                    $strTtype1 = 3;
                    break;
                default:
                    $strTtype1 = 1;
                    break;
            }

            $condition['t_status'] = $strTtype1;
            $v['cnt'] = $model->table('translate')->where($condition)->count();

            $trans_list[$k] = $v;
        }


        Tpl::output('trans_users', $trans_list);
        Tpl::output('commonids', $_GET['id']);
        Tpl::showpage('translate.assign', 'null_layout');
    }

    public function editOp()
    {
        if(chksubmit()){

                $goods_commonid = intval($_POST['goods_commonid']);

                $model_goods = Model('goods');
                $goods_info = $model_goods->getGoodeCommonInfoByID($goods_commonid);

                $param  = array();
                $trans_type = 0;

                $trans_status = 10;

                $translate_id    = intval($_POST['translate_id']);
                $goods_commonid    = intval($_POST['goods_commonid']);
                $param['t_commonid'] = trim($_POST['goods_commonid']);
                $param['t_goods_name'] = trim($_POST['goods_name']);
                $param['t_goods_jingle'] = trim($_POST['goods_jingle']);
                $param['t_goods_body']= trim($_POST['goods_body']);
                $param['t_status']  = $trans_status;
                $param['t_transtime']  = time();
                $model_goods  = Model('goods');

                $result1 = $model_goods->editTranslate($param,array('aid'=>$translate_id));

                $param1['goods_name'] = trim($_POST['goods_name']);
                $param1['goods_jingle'] = trim($_POST['goods_jingle']);
                $param1['goods_body']= trim($_POST['goods_body']);
                $param1['goods_body_ko']= !empty($goods_info['goods_body_ko']) ? $goods_info['goods_body_ko'] : $goods_info['goods_body'];
                $param1['aid'] = $translate_id;

                $result = $model_goods->editGoodsCommon($param1,array('goods_commonid'=>$goods_commonid));

                if ($result){
                    $where = array('goods_commonid' => $goods_commonid);
                    $update['goods_name'] = trim($_POST['goods_name']);
                    $update['goods_jingle'] = trim($_POST['goods_jingle']);
                    $return = Model('goods')->editProducesNoLock($where, $update);

                    /**
                     * 업데이트이미지信息ID
                     */
                    $model_upload = Model('upload');
                    if (is_array($_POST['file_id'])){
                        foreach ($_POST['file_id'] as $k => $v){
                            $v = intval($v);
                            $update_array = array();
                            $update_array['upload_id'] = $v;
                            $update_array['item_id'] = intval($_POST['translate_id']);
                            $model_upload->update($update_array);
                            unset($update_array);
                        }
                    }


                    $this->log($getAdminInfo['name'].'님이 상품 수정[ID:'.$_POST['goods_commonid'].']',1);
                    if(empty($_GET['tp'])):
                        showMessage("성공적으로 등록되었습니다.","index.php?act=translate_goods");
                    else:
                        showMessage("성공적으로 등록되었습니다.","index.php?act=translate_goods&type=waitverify");
                    endif;
                    exit;
             }
         }


        $common_id = intval($_GET['goods_id']);

        $model_goods = Model('goods');
        $goods_info = $model_goods->getTranslateInfoByID($common_id);
        $model_upload = Model('upload');
        $condition['upload_type'] = '7';
        $condition['item_id'] = !empty($goods_info['aid']) ? $goods_info['aid'] : 2;
        $file_upload = $model_upload->getUploadList($condition);
        if (is_array($file_upload)){
            foreach ($file_upload as $k => $v){
                $tmps_ext = explode(".", $file_upload[$k]['file_name']);
                $tmps_ext = $tmps_ext[count($tmps_ext) - 1];
                $t_ext = strtolower($tmps_ext);
                $allow_noimg=array('hwp','xls','xlsx','zip','rar','doc','docx');

                if(!in_array($t_ext,$allow_noimg))
                {
                    $file_upload[$k]['cfile'] = true;
                }
                else
                {
                    $file_upload[$k]['cfile'] = false;
                }
                $file_upload[$k]['upload_path'] = UPLOAD_SITE_URL.'/translate/'.$file_upload[$k]['file_name'];
            }
        }
        Tpl::output('file_upload',$file_upload);
        Tpl::output('goods', $goods_info);
        if(!empty($_GET['type']) && $_GET['type']=="yes")
        {
            Tpl::showpage('translate_my.view');
        }
        else
        {
            Tpl::showpage('translate_goods.edit');
        }

    }

    public function translateOp()
    {
        if(chksubmit()){

                $goods_commonid = intval($_POST['goods_commonid']);

                $model_goods = Model('goods');
                $goods_info = $model_goods->getGoodeCommonInfoByID($goods_commonid);
                $getAdminInfo = $this->getAdminInfo();

                $param  = array();
                $trans_type = $getAdminInfo['t_type'];
                $trans_user = $getAdminInfo['name'];
                $trans_uid = $getAdminInfo['id'];
                if($trans_type==1)
                {
                    $trans_status = 4;
                }
                elseif($trans_type==2)
                {
                    $trans_status = 5;
                }
                elseif($trans_type==3)
                {
                    $trans_status = 6;
                }
                else
                {
                    $trans_status = 6;
                }
                $translate_id    = intval($_POST['translate_id']);
                $goods_commonid    = intval($_POST['goods_commonid']);
                $param['t_commonid'] = trim($_POST['goods_commonid']);
                $param['t_goods_name'] = trim($_POST['goods_name']);
                $param['t_goods_jingle'] = trim($_POST['goods_jingle']);
                $param['t_goods_body']= trim($_POST['goods_body']);
                $param['t_status']  = $trans_status;
                $param['t_type']  = $trans_type;
                $param['t_uid']  = $trans_uid;
                $param['t_user']  = $trans_user;
                $param['t_transtime']  = time();
                $model_goods  = Model('goods');

                $result1 = $model_goods->editTranslate($param,array('aid'=>$translate_id));

                $param1['goods_name'] = trim($_POST['goods_name']);
                $param1['goods_jingle'] = trim($_POST['goods_jingle']);
                $param1['goods_body']= trim($_POST['goods_body']);
                $param1['goods_body_ko']= !empty($goods_info['goods_body_ko']) ? $goods_info['goods_body_ko'] : $goods_info['goods_body'];
                $param1['aid'] = $translate_id;

                $result = $model_goods->editGoodsCommon($param1,array('goods_commonid'=>$goods_commonid));

                if ($result){
                    /**
                     * 업데이트이미지信息ID
                     */
                    $model_upload = Model('upload');
                    if (is_array($_POST['file_id'])){
                        foreach ($_POST['file_id'] as $k => $v){
                            $v = intval($v);
                            $update_array = array();
                            $update_array['upload_id'] = $v;
                            $update_array['item_id'] = intval($_POST['translate_id']);
                            $model_upload->update($update_array);
                            unset($update_array);
                        }
                    }


                    $this->log($getAdminInfo['name'].'님이 상품 번역 등록[ID:'.$_POST['goods_commonid'].']',1);
                    if(empty($_GET['tp'])):
                        showMessage("성공적으로 등록되었습니다.","index.php?act=translate_my");
                    else:
                        showMessage("성공적으로 등록되었습니다.","index.php?act=translate_my&type=waitverify");
                    endif;
                    exit;
             }
         }
     }


}
?>