<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert alert-block mt10">
  <ul>
    <li>1. 미니샵 유효기간이 30일 이내일때 미니샵갱신을 신청하실 수 있습니다.</li>
    <?php if (empty($output['reopen_list'])) { ?>
    <li>2. 해당 미니샵 유효기간은  <?php echo date('Y-m-d',$output['store_info']['store_end_time']);?> 까지입니다.， <?php echo date('Y-m-d',$output['store_info']['allow_applay_date']);?>부터 30일간 갱신신청 가능합니다.</li>
    <?php } ?>
  </ul>
</div>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w10"></th>
      <th>신청시간</th>
      <th>표준비용(원/년)</th>
      <th>갱신기간(년)</th>
      <th>지불금액(원)</th>
      <th>갱신가능기간</th>
      <th>지불증빙</th>
      <th>상태</th>
      <th>운영</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($output['reopen_list'])) { ?>
    <?php foreach($output['reopen_list'] as $val) { ?>
    <tr class="bd-line">
      <td></td>
      <td><?php echo date('Y-m-d',$val['re_create_time']); ?></td>
      <td><?php echo $val['re_grade_price']; ?> ( <?php echo $val['re_grade_name'];?> )</td>
      <td><?php echo $val['re_year']?></td>
      <td><?php echo $val['re_pay_amount'] == 0 ? '무료' : $val['re_pay_amount'];?></td>
      <td>
      <?php if ($val['re_start_time'] != '') {?>
      <?php echo date('Y-m-d',$val['re_start_time']).' ~ '.date('Y-m-d',$val['re_end_time']);?>
      <?php  } ?>
      </td>
      <td>
      <?php if ($val['re_pay_cert'] != '') {?>
      <a href="<?php echo getStoreJoininImageUrl($val['re_pay_cert']);?>" target="_blank">보기</a>
      <?php }?>
      </td>
      <td><?php echo str_replace(array('0','1','2'),array('입금대기','심사대기','심사완료'),$val['re_state']);?></td>
      <td class="nscs-table-handle">
      <?php if ($val['re_state'] == '0') {?>
      <span><a href="javascript:void(0)" class="btn-red" onclick="ajax_get_confirm('정말 삭제 하시겠습니까?', 'index.php?act=store_info&op=reopen_del&re_id=<?php echo $val['re_id']; ?>');"><i class="icon-trash"></i><p>삭제</p></a></span>
     <?php } ?>
      </td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<div class="ncsc-form-default">
<?php if ($output['upload_cert']) {?>
  <form method="post" action="index.php?act=store_info&op=reopen_upload" name="upload_form" id="upload_form" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="re_id" value="<?php echo $output['reopen_info']['re_id'];?>">
    <dl>
      <dt>납부금액<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <?php echo $output['reopen_info']['re_pay_amount'];?> 원
      </dd>
    </dl>
    <dl>
      <dt>지불증빙서류업로드<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <input name="re_pay_cert" type="file">
      </dd>
    </dl>
    <dl>
      <dt>비고<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <textarea name="re_pay_cert_explain" rows="10" cols="30"></textarea>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input type="button" id="btn_upload_reopen"  class="submit" value="완료" /></label>
    </div>
  </form>
<?php } ?>

<?php if ($output['applay_reopen']) {?>
  <form method="post" action="index.php?act=store_info&op=reopen_add" name="add_form" id="add_form">
    <input type="hidden" name="form_submit" value="ok" />
    <dl>
      <dt>미니샵등급신청<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <select name="re_grade_id" style="width: auto;">
            <?php if(!empty($output['grade_list']) && is_array($output['grade_list']) ) {?>
            <?php foreach ($output['grade_list'] as $val) {?>
            <option <?php if ($val['sg_id'] == $output['current_grade_id']) echo 'selected'; ?> value="<?php echo $val['sg_id'];?>"><?php echo $val['sg_name'];?> <?php echo floatval($val['sg_price'])>0 ? $val['sg_price'].' 원/년' : '무료';?></option>
            <?php }?>
            <?php }?>
          </select>
      </dd>
    </dl>
    <dl>
      <dt>갱신기간신청<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <select name="re_year">
            <option value="1">1 년</option>
            <option value="2">2 년</option>
          </select>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input type="button" id="btn_add_reopen"  class="submit" value="완료" /></label>
    </div>
  </form>
<?php } ?>
</div>
<script type="text/javascript">
$(document).ready(function(){
	//页面输入内容验证
    $('#btn_add_reopen').on('click', function() {
        ajaxpost('add_form', '', '', 'onerror')
    });

    $('#upload_form').validate({
		submitHandler:function(form){
			ajaxpost('upload_form', '', '', 'onerror');
		},
        rules : {
        	re_pay_cert: {
                required: true
            },
            re_pay_cert_explain: {
                maxlength: 100 
            }
        },
        messages : {
        	re_pay_cert: {
                required: '지불증빙서류를 선택하여주십시오.'
            },
            re_pay_cert_explain: {
                maxlength: jQuery.validator.format("최대{0}자")
            }
        }
    });

    $('#btn_upload_reopen').on('click', function() {
    	$('#upload_form').submit();
    });
});
</script>