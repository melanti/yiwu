(function ($, angular, _, APP) {

  APP.modules.ctrl.itemRegistrationCtrl = itemRegistrationCtrl;

  //itemRegistrationCtrl
  function itemRegistrationCtrl($scope, _, $q, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL, BrandTableManager, CategoryManager, AccOrgListTableManager, ThreeDTableManager) {
    //function itemRegistrationCtrl($scope, _, $q, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL, BrandTableManager, CategoryManager, OperationTableManager, ThreeDTableManager) {
    var vm = this;
    vm._state = {
      item_name: "",
      item_price: "",
      origin_item_id: "",
      brand_id: "",
      item_sex: "",
      status: "",
      display_start_date: "",
      display_end_date: "",
      sale_type: 0,
      thumbnail: ""
    };
    vm.display_start_date_str='';
    vm.display_end_date_str='';
    vm.stDateOptions = {
      changeYear: false,
      changeMonth: false,
      yearRange: '1900:-0',
      dateFormat: 'yy-mm-dd',
      onClose: function(__d){
        if (!__d) return;
        vm.display_start_date_str = __d;
        $scope.$apply();
      }
    };
    vm.endDateOptions = {
      changeYear: false,
      changeMonth: false,
      yearRange: '1900:-0',
      dateFormat: 'yy-mm-dd',
      onClose: function (__d) {
        __d && (vm.display_end_date_str = __d);
        $scope.$apply();
      }
    };
    vm.item_name_str = ""; // 수정 시 view를 위한 item_name
    vm.registerd_datetime = "";
    vm.updated_datetime = "";
    vm.date = "false";
    vm.brand_code_arr = [];
    vm.code = "";
    vm.rep_code = "";
    vm.itemId;
    vm.currency
    vm.category_list
    vm.td_list = []; //  실제 보내는 3D의상 리스트
    vm.pageType; // 상품 수정페이지라면 modification
    vm.drop_thumbnail_flag = false;
    vm.rep_yn = ""; // 선택된 대표 의상
    vm.catId_list = [];
    vm.cat3d_view_list = [];
    vm.cat3d_list = [];
    vm.cat3d_selected = null;
    vm.itemListImage = '';
    vm.threedThumbnailImage = '';
    vm.exports = {
      init: _init,
      showTdPrevModal: _showTdPrevModal
    };

    function _init() {
      //console.log('item_registration init');
      //console.log(APP.info.user);

      var deferred = $q.defer();
      function _getBrandList() {
        BrandTableManager.exports
          .updateTableData({start: 0, count: 1000, query_opt: 'name', query: '', status: 'all'})
          .then(
            function (__sucData) {
              //console.log(__sucData);
              if (__sucData.results && __sucData.results.length > 0){
                vm.brand_code_arr = __sucData.results;
                vm._state.brand_id = vm.brand_code_arr[0].id;
              }
              return deferred.resolve();
            },
            function (__errData) {
              console.log(__errData);
              return deferred.reject();
            }
          );
        return deferred.promise;
      }

      function _getCategoryList() {
        CategoryManager.exports.getCateList()
          .then(
            function (__sucData) {
              //console.log(__sucData);
              vm.category_list = CategoryManager.exports.getFilteredResults();
              return deferred.resolve();
            }, function (__errData) {
              console.log(__errData);
              return deferred.reject();
            })
        return deferred.promise;
      }

      $q.when(_getBrandList(), _getCategoryList()).then(function () {
        $(document).ready(function () {
          (vm.pageType === "modification") && _getItem();
          $('#threeDModal').on('show.bs.modal', function (e) {
            vm.cat3d_selected = null;
            vm.threedThumbnailImage = '';
          })
        })
      }, function(__errData){
        console.log(__errData)
      });

      _getOperationList();
      (vm.catId_list.length != 0) && _getcatList();
    }

    _init();

    /**
     * 상품 수정 시 상품 itemId로 상품 데이터를 불러옴
     */
    function _getItem() {
      console.log("item_id : " + vm.itemId);
      var _url = API_URL_INFO.GET_ITEM + "/" + vm.itemId;
      XHR.GET(_url)
        .then(function (__sucData) {
          //console.log(__sucData);
          if (__sucData.data.results.length == 0) {
            alert('error');
            return;
          }
          var _data = __sucData.data.results[0];

          // 기본 정보 셋팅
          for (var i in _data) {
            if (Object.keys(vm._state).indexOf(i) !== -1) {
              vm._state[i] = _data[i];
            }
          }
          // 기본 정보 셋팅 2
          vm._state.item_name = _data.name;
          vm.item_name_str = angular.copy(_data.name);
          vm._state.status = _data.display_yn ? 'activated' : 'disabled';


          // 날짜 셋팅
          vm.registerd_datetime = _data.registerd_datetime && COMMON_UTIL.getLocalTimezoneStr(_data.registerd_datetime, '-');
          vm.updated_datetime = _data.updated_datetime && COMMON_UTIL.getLocalTimezoneStr(_data.updated_datetime, '-');

          vm.display_start_date_str = _data.display_start_time && COMMON_UTIL.getLocalTimezoneStr(_data.display_start_time, '-');
          vm.display_end_date_str = _data.display_end_time && COMMON_UTIL.getLocalTimezoneStr(_data.display_end_time, '-');

          // 상품 분류 셋팅
          var _cate = _data.category_list;
          for (var i in _cate) {
            vm.selected.push(_cate[i].id);
          }

          // code & currency 셋팅
          vm.code = _data.code;
          vm.currency = _data.org_currency;

          // 3d 의상 정보 셋팅
          vm.td_list = _data.td_list || [];
          for (var i in _data.td_list) {
            if(_data.td_list[i].rep_yn){
              vm.rep_yn = _data.td_list[i].id;
              vm.rep_code = _data.td_list[i].code;
            }
          }

          // 상품노출 여부에 따른 셋팅
          if (_data.display_start_time !== null || _data.display_end_time !== null) {
            vm.date = 'true';
          } else {
            vm.date = 'false';
          }

          // 상품목록 이미지 셋팅
          _data.item_thumbnail_url && (vm.itemListImage = _data.item_thumbnail_url);

        }, function (__errData) {
          console.log(__errData);
        })
    }


    /**
     * 3D 의상 제작업체(CAT 아이디) 리스트 요청
     * - 입점업체 중 cat 서비스 사용
     * - 로그인한 id 의 org_id 로 자신에게 속한 입점업체만 get
     */
    function _getOperationList() {
      var _data = {
        service_type: 1
      };
      _data[(APP.info.user.org_id === APP.info.user.parent_org_id) ? 'parent_org_id' : 'org_id'] = APP.info.user.org_id;
      AccOrgListTableManager.exports.getTableList(_data).then(
        function (__sucData) {
          //console.log(__sucData);
          if (__sucData.results.length == 0) {
            return;
          }
          vm.catId_list = __sucData.results;

        }, function (__errData) {
          console.log(__errData);
        });
    }

    function _getcatList(id) {
      XHR.GET(API_URL_INFO.GET_3DITEMS_LIST + '?cat_id=' + (id || '') + '&show=debug,td_fitting').then(function (__sucData) {
        id || (vm.cat3d_list = __sucData.data.results);
        vm.cat3d_view_list = __sucData.data.results;
      }, function (__errData) {
        console.log(__errData);
      });
      return;
    }

    function dataUriToBlob(dataURI) {
      // serialize the base64/URLEncoded data
      var byteString;
      if (dataURI.split(',')[0].indexOf('base64') >= 0) {
        byteString = atob(dataURI.split(',')[1]);
      }
      else {
        byteString = unescape(dataURI.split(',')[1]);
      }

      // parse the mime type
      var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

      // construct a Blob of the image data
      var array = [];
      for (var i = 0; i < byteString.length; i++) {
        array.push(byteString.charCodeAt(i));
      }
      return new Blob(
        [new Uint8Array(array)],
        {type: mimeString}
      );
    }

    function isModification() {
      return vm.pageType === 'modification';
    }


    /**
     * 실제 상품등록 & 상품수정
     */
    vm._submit = function (e) {
      if (!vm._state.item_name) {
        alert('상품명 항목은 필수 입력 항목입니다.\n상품명을 입력해 주십시오.');
        return;
      }
      if (vm._state.sale_type != "1" && !vm._state.origin_item_id) {
        alert('쇼핑몰 상품번호 항목은 필수 입력 항목입니다.\n쇼핑몰 상품번호를 입력해 주십시오.');
        return;
      }
      if(vm._state.item_price === ''){
        alert('판매가격 항목은 필수 입력 항목입니다.\n판매가격을 입력해 주십시오.');
        return;
      }
      if (!vm._state.brand_id) {
        alert('브랜드 항목은 필수 입력 항목입니다.\n브랜드를 선택해 주십시오.');
        return;
      }
      if (vm.selected.length == 0) {
        alert('카테고리 항목은 필수 입력 항목입니다.\n카테고리를 선택해 주십시오.');
        return;
      }

      if (vm._state.status === 'disabled' || vm.date === 'false') {
        vm._state.display_start_date = '';
        vm._state.display_end_date = '';
        vm.display_start_date_str = '';
        vm.display_end_date_str = '';
      }

      var endD = vm.display_end_date_str && new Date(vm.display_end_date_str.replace(/-/g, "/")).getTime();
      var stD = vm.display_start_date_str && new Date(vm.display_start_date_str.replace(/-/g, "/")).getTime();

      if (stD && endD && (stD > endD)) {
        alert('검색 시작일자가 검색 종료 일자 이후 입니다.\n검색 시작일이 검색 종료일자 이전으로 수정해 주십시오.');
        return false;
      }

      vm.display_start_date_str && (vm._state.display_start_date = COMMON_UTIL.getcomputedUTCDate(vm.display_start_date_str + " 00:00:00"));
      vm.display_end_date_str && (vm._state.display_end_date = COMMON_UTIL.getcomputedUTCDate(vm.display_end_date_str + " 23:59:59"));

      if (vm._state.sale_type == "1") {
        vm._state.origin_item_id = 1;
        vm._state.item_price = 0;
      }

      //console.log(vm._state);
      var fd = new FormData();

      var _url = API_URL_INFO.SET_ITEM;
      if (isModification()) { // 상품수정 일 경우
        _url = API_URL_INFO.UPDATE_ITEM;
        fd.append('item_uuid', vm.code);
        fd.append('drop_thumbnail_flag', vm.drop_thumbnail_flag);
      }

      for(var i in vm._state){
        fd.append(i, vm._state[i]);
      }

      XHR.FILE(_url, fd)
        .then(function (__sucData) {
          //console.log('success');
          //console.log(__sucData);
          var deferred = $q.defer();

          $q.when(_category(), _tdList()).then(function(__sucData){
            if(isModification()){
              alert("저장되었습니다");
              vm.closeModification();
            }else{
              alert("등록되었습니다");
              if (e.currentTarget.name == "go") {
                window.location.href = window.location.href = window.location.protocol+'//'+window.location.host+'/' + APP.info.gLocale + APP.URL_INFO.menu.ITEMS_MNG_ITEMS_LIST.link;
              } else {
                document.getElementsByTagName("body")[0].scrollTop = 0;
                window.location.reload(true);
              }
            }
          }, function(__errData){
            //alert('error');
            if(isModification()){
              alert("상품이 정상적으로 수정되지 않았습니다.");
            }else{
              alert("상품이 정상적으로 등록되지 않았습니다.");
            }

            console.log('__errData: ' , __errData);
          });

          function _category(){
            if ( isModification() || (vm.selected && vm.selected.length > 0) ) {
            //if ( (vm.selected && vm.selected.length > 0) ) {
              XHR.POST(API_URL_INFO.ATTACH_CATEGORY, {
                item_id: isModification() ? vm.itemId : __sucData.data.insertId,
                category_ids: vm.selected.join(',')
              }).then(function (__suc) {
                //console.log(__suc);
                deferred.resolve();
              }, function (__err) {
                console.log(__err);
                deferred.reject();
              })
            }else{
              deferred.resolve();
            }
            return deferred.promise;
          }
          function _tdList(){
            if (isModification() || (vm.td_list && vm.td_list.length > 0) ) {
            //if ( (vm.td_list && vm.td_list.length > 0) ) {
              var _td_ids = [];
              for (var i in vm.td_list) {
                _td_ids.push(vm.td_list[i].id);
              }
              XHR.POST(API_URL_INFO.ATTACH_THREED_ITEM, {
                item_id: isModification() ? vm.itemId : __sucData.data.insertId,
                td_ids: _td_ids.join(','),
                td_rep_ids: vm.rep_yn
              }).then(function (__suc) {
                //console.log(__suc);
                deferred.resolve();
              }, function (__err) {
                console.log(__err);
                deferred.reject();
              })
            }else{
              deferred.resolve();
            }
            return deferred.promise;
          }
        }, function (__errData) {
          console.log('error');
          console.log(__errData);
          if (__errData.data.code === "ERROR_FILE_FORMAT") {
            alert('JPG, PNG, GIF, JPGE 형식의 이미지 1개만 등록가능합니다.');
          }
        })
    };
    vm._clickImageUpload = function () {
      $("#_image_upload").click();
    };
    vm._clickImageRemove = function () {
      vm.itemListImage = '';
      vm.code && (vm.drop_thumbnail_flag = true);
      vm._state.thumbnail = "";
    };
    vm._webImageUpload = function () {
      $("#imgUrlModal").modal('hide');
      vm.drop_thumbnail_flag = false;
      var _url = $("#webImgUrl").val();
      XHR.GET(API_URL_INFO.GET_IMG_URL + '?url=' + _url)
        .then(function (__s) {
          var tmpSrc = 'data:image/jpeg;base64,' + __s.data.image;
          vm.itemListImage = tmpSrc;
          var blob = dataUriToBlob(tmpSrc);
          vm._state.thumbnail = new File([blob], _url .replace(/^.*\//, ''));
          $("#webImgUrl").val('');
        }, function (ret) {
          alert('JPG, PNG, GIF, JPGE 형식의 이미지 1개만 등록가능합니다.');
          console.log(ret);
        })
    };
    vm.selected = [];
    vm.updateSelection = function (e, id) {
      var _index = vm.selected.indexOf(id);
      var _checked = e.target.checked;
      if (_checked && _index === -1) {
        vm.selected.push(id);
      } else if (!_checked && _index !== -1) {
        vm.selected.splice(_index, 1);
      }
      //console.log(vm.selected);
    };
    vm.isSelected = function (id) {
      return vm.selected.indexOf(id) >= 0;
    };
    var _id = '';
    vm.get3dList = function (id) {
      _getcatList(id);
      _id = id || '';
    };
    vm._submitSearch = function () {
      //console.log(_id);
      ThreeDTableManager.exports
        .updateTableData({
          start: 0,
          count: '',
          cat_id: _id || '',
          td_code: vm.search,
          td_name: vm.search,
          item_uuid: vm.search
        })
        .then(function (__sucData) {
          //console.log(__sucData);
          if (__sucData.results.length == 0) {
            vm.cat3d_view_list = [];
            return;
          }
          vm.cat3d_view_list = __sucData.results;
        }, function (__errData) {
          console.log(__errData);
          alert('error');
        })
    };
    vm.addTdList = function () {
      if(vm.cat3d_selected == null){
        alert('3D 의상을 선택해주세요');
        return;
      }

      $("#threeDModal").modal('hide');

      for (var i in vm.td_list) {
        if (vm.cat3d_selected.id == vm.td_list[i].id) {
          alert('이미 있습니다');
          return;
        } else if (_index !== '' && _index == i) {
          _index = '';
          (vm.rep_yn == vm.td_list[i].id) && (vm.rep_yn = vm.cat3d_selected.id);
          vm.td_list[i] = vm.cat3d_selected;
          return;
        }
      }
      vm.cat3d_selected && vm.td_list.push(vm.cat3d_selected);
    };
    vm.viewThumbnail = function (td) {
      vm.threedThumbnailImage = td.td_fitting_files.thumbnail.url;
      vm.cat3d_selected = td;
    };
    vm.tdRemove = function (id, index) {
      vm.td_list.splice(index, 1);
      (vm.rep_yn == id) && (vm.rep_yn = '');
    };
    var _index = "";
    vm.tdModify = function (index) {
      _index = index;
      $("#threeDModal").modal('show');
    };
    vm.closeModification = function(){
      window.opener.closeEditPopupWindow();
      window.close();
    };
    function _showTdPrevModal(__li){
      // console.log('_showTdPrevModal: ', __li, getUrlArr(__li.td_fitting_files));
      PubSub.publish('tdPrev:show', getUrlArr(__li.td_fitting_files));

      function getUrlArr(__arr){
        return {imgArr:[
          (__arr.preview0.url || ''),
          (__arr.preview30.url || ''),
          (__arr.preview60.url || ''),
          (__arr.preview90.url || ''),
          (__arr.preview120.url || ''),
          (__arr.preview150.url || ''),
          (__arr.preview180.url || ''),
          (__arr.preview210.url || ''),
          (__arr.preview240.url || ''),
          (__arr.preview270.url || ''),
          (__arr.preview300.url || ''),
          (__arr.preview330.url || '')
        ]};
      }
    }


  }
})(window.jQuery, window.angular, window._, window.APP);
