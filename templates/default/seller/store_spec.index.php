<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert mt15 mb5"><strong>도움말：</strong> 
  <ul>
    <li>1.  카테고리를 선택하시면 해당 카테고리 내 상품 옵션을 설정하실 수 있습니다.  </li>
    <li>2. 상품에 해당하는 검색기준을 추가할 수 있고, 이미 저장된 내용 또는 저장되지 않은 변경된 내용을 삭제할 수 있습니다. <br>
<font color="red">옵션저장</font>을 클릭하여 완료하여 주십시오. 
    <li>3. 0~255의 정수로 옵션 순서를 설정하실 수 있습니다. 상품 등록시 상품에 해당하는 옵션 타입을 선택하고 옵션명을 직접 지정하실 수 있습니다.
</li>
  </ul>
</div>
<table class="search-form">
  <tr>
    <td class="w20">&nbsp;</td>
    <td class="w120"><strong>카테고리</strong></td>
    <td>
        <span nctype="gc1">
        <?php if (!empty($output['gc_list'])) {?>
          <select nctype="gc" data-param="{deep:1}">
            <option>선택</option>
            <?php foreach ($output['gc_list'] as $val) {?>
            <option value="<?php echo $val['gc_id']?>"><?php echo $val['gc_name_ko'];?></option>
            <?php }?>
          </select>
        <?php }?></span>
        <span nctype="gc2"></span>
        <span nctype="gc3"></span>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
<div nctype="class_spec" class="ncsc-goods-spec">
  <div nctype="spec_ul" class="spec-tabmenu"></div>
  <div nctype="spec_iframe" class="spec-iframe">
    <div class="norecord tc">
      <div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div>
    </div>
  </div>
</div>
<script>
$(function() {
    // 查询下级分类，分类不存在显示当前分类绑定的规格
    $('select[nctype="gc"]').change(function(){
        $(this).parents('td:first').nextAll().html('');
        $('div[nctype="spec_ul"]').html('');
        $('div[nctype="spec_iframe"]').html('');
        getClassSpec($(this));
    });
});

// 选择상품분류
function getClassSpec($this) {
    var id = parseInt($this.val());
    var data_str = ''; eval('data_str =' + $this.attr('data-param'));
    var deep = data_str.deep;
    if (isNaN(id)) {
        // 清理分类
        clearClassHtml(parseInt(deep)+1);
    }
    $.getJSON('index.php?act=store_spec&op=ajax_class&id=' + id + '&deep=' + deep, function(data){
    	$('div[nctype="spec_iframe"]').empty();
        $('div[nctype="spec_ul"]').empty();
        if (data) {
            if (data.type == 'class') {
                nextClass(data.data, data.deep);
            } else if (data.type == 'spec') {
                specList(data.data, data.deep, data.gcid);
            }
        }
    });
}

// 下一级상품분류
function nextClass(data, deep) {
    $('span[nctype="gc' + deep + '"]').html('').append('<select data-param="{deep:' + deep + '}"></select>')
        .find('select').change(function(){
            getClassSpec($(this));
        }).append('<option>선택</option>');
    $.each(data, function(i, n){
        if (n != null) {
            $('span[nctype="gc' + deep + '"] > select').append('<option value="' + n.gc_id + '">' + n.gc_name_ko + '</option>');
        }
    });
    // 清理分类
    clearClassHtml(parseInt(deep)+1);
}

// 列出规格信息
function specList(data, deep, gcid) {
    if (typeof(data) != 'undefined' && data != '') {
        var $_ul = $('<ul></ul>');
        $.each(data, function(i, n){
            $_ul.append('<li><a href="javascript:void(0);" nctype="editSpec" data-param="{spid:'+ n.sp_id +',gcid:' + gcid + '}">' + n.sp_name + '옵션</a></li>');
        });
        $_ul.find('a').click(function(){
            $_ul.find('li').removeClass('selected');
            $(this).parents('li:first').addClass('selected');
            editSpecValue($(this));
        });
        $_ul.find('a:first').click();
        $('div[nctype="spec_ul"]').append($_ul);
    } else {
        $('div[nctype="spec_ul"]').append('<div class="warning-option"><i class="icon-warning-sign"></i><span>해당 카테고리는 규격을 추가할 수 없습니다. </span></div>');
    }
    // 清理分类
    clearClassHtml(deep);
}

// 清理二级分类信息
function clearClassHtml(deep) {
    switch (deep) {
        case 2:
            $('span[nctype="gc2"]').empty();
        case 3:
            $('span[nctype="gc3"]').empty();
            break;
    }
}

// ajax编辑规格值
function editSpecValue(o) {
    $('div[nctype="spec_iframe"]').html('');
    var data_str = '';
    eval('data_str =' + o.attr('data-param'));
    $_iframe = $('<iframe id="iframepage" name="iframepage" frameBorder=0 scrolling=no width="100%" height="630px" src="<?php echo SHOP_SITE_URL;?>/index.php?act=store_spec&op=add_spec&spid=' + data_str.spid + '&gcid=' + data_str.gcid + '" ></iframe>');
    $('div[nctype="spec_iframe"]').append($_iframe);
}

</script> 
