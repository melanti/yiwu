(function($, angular, _, APP){

  var tableComponentDirName = 'itemsTable';

  if( $(document.body).attr('data-page-id')=='items-list' ){
    bootstrapItemsList();
  }

  function bootstrapItemsList(){
    //console.log('[[bootstrapItemsList:]]');

    angular.module('App')
      .directive( 'threedDashBoard', ThreeDDashBoardDirective );

    angular.module('App')
      .directive( tableComponentDirName, tableComponentDirective );

    angular.module('App')
      .directive( 'tableSearch', tableSearchDirective );

    angular.module('App')
      .directive( 'tableOption', tableOptionDirc );

    angular.module('App')
      .directive( 'tableBody', tableBodyDirective );

    angular.module('App')
      .directive( 'tableBtn', btnDirc );

    angular.module('App')
      .directive( 'pagination', paginationDirective );

    angular.module('App')
      .directive( 'tdPrevModal', tdPrevModal );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( parseInt(APP.info.user.auth.item,10)>=1 ){
            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }
        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }


  function ThreeDDashBoardDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.threeDDashBoardCtrl,
      controllerAs: 'threeDDashBoardCtrl',
      bindToController: APP.modules.ctrl.threeDDashBoardCtrl.bindToController,
      link: APP.modules.ctrl.threeDDashBoardCtrl.link(),
      templateUrl: 'threeD_dash_board.html'
    };
  }
  ThreeDDashBoardDirective.$inject =['XHR', 'PubSub', 'URL_INFO'];

  function tableComponentDirective(XHR, ItemsTableManager, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.ctrl.tableComponentItemsCtrl,
      controllerAs: 'tableComponentItemsCtrl',
      bindToController: APP.modules.ctrl.tableComponentItemsCtrl.bindToController,
      link: APP.modules.ctrl.tableComponentItemsCtrl.link(),
      templateUrl: 'table_component_basic.html'
    };
  }
  tableComponentDirective.$inject =['XHR', 'ItemsTableManager', 'PubSub', 'URL_INFO'];

  function tableSearchDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableSearchItemsCtrl,
      controllerAs: 'tableSearchCtrl',
      bindToController: APP.modules.ctrl.tableSearchItemsCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tableSearchItemsCtrl.link(PubSub),
      templateUrl: 'table_search_items.html'
    };
  }
  tableSearchDirective.$inject =['XHR', 'PubSub', 'URL_INFO'];

  function tableOptionDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableOptionsBasicCtrl,
      controllerAs: 'tableOptionCtrl',
      bindToController: APP.modules.ctrl.tableOptionsBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link:  APP.modules.ctrl.tableOptionsBasicCtrl.link(PubSub),
      templateUrl: 'table_option_basic.html'
    };
  }
  tableOptionDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function tableBodyDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableBodyItemsCtrl,
      controllerAs: 'tableBodyCtrl',
      bindToController: APP.modules.ctrl.tableBodyItemsCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tableBodyItemsCtrl.link(PubSub),
      templateUrl: 'table_body_items.html'
    };
  }
  tableBodyDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function btnDirc(XHR, PubSub, URL_INFO) {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableBtnBasicCtrl,
      controllerAs: 'tableBtnBasicCtrl',
      bindToController: APP.modules.ctrl.tableBtnBasicCtrl.bindToController,
      require: ('^' + tableComponentDirName),
      link: APP.modules.ctrl.tableBtnBasicCtrl.link(PubSub),
      templateUrl: 'table_btn_basic.html'
    }
  }
  btnDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function paginationDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tablePaginationBasicCtrl,
      controllerAs: 'tablePaginationBasicCtrl',
      bindToController:  APP.modules.ctrl.tablePaginationBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tablePaginationBasicCtrl.link(PubSub),
      templateUrl: 'table_pagination_basic.html'
    };
  }
  paginationDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function tdPrevModal(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tdPrevModalCtrl,
      controllerAs: 'tdPrevModalCtrl',
      bindToController: APP.modules.ctrl.tdPrevModalCtrl.bindToController,
      link: APP.modules.ctrl.tdPrevModalCtrl.link(),
      templateUrl: 'td_prev_modal.html'
    };

  }
  tdPrevModal.$inject = ['XHR', 'PubSub', 'URL_INFO'];



})(window.jQuery, window.angular, window._, window.APP);
