<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form id="add_form" action="<?php echo urlShop('store_waybill', 'waybill_setting_save');?>" method="post">
    <input type="hidden" name="store_waybill_id" value="<?php echo $output['store_waybill_id'];?>">
    <dl>
      <dt><i class="required">*</i>좌측여백<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input class="w60 text" name="store_waybill_left" type="text" value="<?php echo $output['store_waybill_left'];?>" /><em class="add-on">mm</em> <span></span>
        <p class="hint">프린트템플릿 좌측여백</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>여백설정<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input class="w60 text" name="store_waybill_top" type="text" value="<?php echo $output['store_waybill_top'];?>" /><em class="add-on">mm</em> <span></span>
        <p class="hint">프린트템플릿 여백성정</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>항목보이기<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <?php if(!empty($output['store_waybill_data']) && is_array($output['store_waybill_data'])) {?>
        <ul class="ncsc-form-checkbox-list">
          <?php foreach($output['store_waybill_data'] as $key => $value) {?>
          <li>
            <input id="<?php echo $key;?>" type="checkbox" class="checkbox" name="data[<?php echo $key;?>]" <?php echo $value['show']?'checked':'';?>>
            <label for="<?php echo $key;?>"><?php echo $value['item_text'];?></label>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
        <p class="hint">프린트하실 항목을 선택하여 주십시오. 체크선택하지 않은 것은 프린트되지 않습니다.</p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border">
        <input type="submit" class="submit" value="완료">
      </label>
    </div>
  </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add_form').validate({
        onkeyup: false,
        errorPlacement: function(error, element){
            element.nextAll('span').first().after(error);
        },
        submitHandler:function(form){
            ajaxpost('add_form', '', '', 'onerror');
        },
        rules: {
            store_waybill_left: {
                required: true,
                number: true
            },
            store_waybill_top: {
                required: true,
                number: true
            }
        },
        messages: {
            store_waybill_left: {
                required: '<i class="icon-exclamation-sign"></i>여백의 수치는 비어있을 수 없습니다.',
                number: '<i class="icon-exclamation-sign"></i>여백의 수치는 숫자로만 입력 가능합니다. ' 
            },
            store_waybill_top: {
                required: '<i class="icon-exclamation-sign"></i>여백의 수치는 비어있을 수 없습니다.',
                number: '<i class="icon-exclamation-sign"></i>여백의 수치는 숫자로만 입력 가능합니다. ' 
            }
        }
    });
});
</script> 
