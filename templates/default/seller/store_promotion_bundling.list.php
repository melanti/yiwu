<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>

<?php if ($output['isOwnShop']) { ?>
  <a class="ncsc-btn ncsc-btn-green"  href="index.php?act=store_promotion_bundling&op=bundling_add" ><i class="icon-plus-sign"></i><?php echo $lang['bundling_add'];?></a>

<?php } else { ?>

  <?php if (empty($output['bundling_quota'])) { ?>
  <a href="javascript:void(0)" class="ncsc-btn ncsc-btn-acidblue" onclick="go('index.php?act=store_promotion_bundling&op=bundling_quota_add');" title="<?php echo $lang['bundling_quota_add'];?>"><i class="icon-money"></i>패키지구매</a>
  <?php } else if((isset($output['bundling_surplus']) && intval($output['bundling_surplus']) != 0 ) || C('promotion_bundling_sum') == 0) { ?>
  <?php if ($output['bundling_quota']['bl_state'] == 1) {?>
  <a class="ncsc-btn ncsc-btn-green"  href="index.php?act=store_promotion_bundling&op=bundling_add" style="right:100px"><i class="icon-plus-sign"></i>프로모션 추가</a>
  <?php }?>
  <a class="ncsc-btn ncsc-btn ncsc-btn-acidblue" href="index.php?act=store_promotion_bundling&op=bundling_renew"><i class="icon-money"></i>패키지갱신</a>
  <?php } ?>
<?php } ?>

</div>

<?php if ($output['isOwnShop']) { ?>
    <?php if (C('promotion_bundling_sum') != 0) {?>
<div class="alert alert-block mt10">
  <ul>
    <li>1. 최대 <?php echo C('promotion_bundling_sum');?>가지 패키지 등록이 가능합니다.</li>
  </ul>
</div>
    <?php }?>
<?php } else { ?>
<!-- 有可用套餐，发布活动 -->
<div class="alert alert-block mt10">
<?php if(empty($output['bundling_quota']) || $output['bundling_quota']['bl_state'] == 0) { ?>
  <strong>아직 패키지를 구매하지 않으셨거나 유효기간이 지나셨습니다. 패키지를 구매/갱신 하여 주십시오. </strong>
<?php } else {?>
  <strong>패키지 유효기간<?php echo $lang['nc_colon'];?></strong> <strong style=" color:#F00;"><?php echo date('Y-m-d H:i:s',$output['bundling_quota']['bl_quota_endtime']);?></strong>
<?php }?>
  <ul>
    <li>1. 패키지 구매와 패키지 갱신을 클릭하시면 패키지의 구매 혹은 갱신이 가능합니다.</li>
    <li>2. <strong style="color: red">관련 비용은 정산시 차감됩니다.</strong></li>
    <?php if (C('promotion_bundling_sum') != 0) {?>
    <li>3. 최대<?php echo C('promotion_bundling_sum');?>가지 우대 패키지 등록이 가능합니다.</li>
    <?php }?>
  </ul>
</div>
<?php } ?>
<form method="get">
  <input type="hidden" name="act" value="store_promotion_bundling" />
  <input type="hidden" name="op" value="bundling_list" />
  <table class="search-form">
    <tr>
      <td>&nbsp;</td>
      <th>상태</th>
      <td class="w100"><select name="state">
          <option value='all'>전체</option>
          <option value='0' <?php if(isset($_GET['state']) && $_GET['state'] == 0){?>selected="selected"<?php }?>>사용안함</option>
          <option value='1' <?php if(isset($_GET['state']) && $_GET['state'] == 1){?>selected="selected"<?php }?>>사용</option>
        </select></td>
      <th class="w110">이벤트명</th>
      <td class="w160"><input type="text" class="text w150" name="bundling_name" value="<?php echo $_GET['bundling_name_ko'];?>"/></td>
      <td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="검색" /></label></td>
    </tr>
  </table>
</form>

<?php if(empty($output['list'])) { ?>
<!-- 没有프로모션 추가 -->
<table class="ncsc-default-table ncsc-promotion-buy">
  <tbody>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
  </tbody>
</table>
<?php } else { ?>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w10"></th>
      <th class="w50"></th>
      <th class="tl">프로모션명</th>
      <th class="w180">할인세트가격</th>
      <th class="w180">상품수량</th>
      <th class="w90">상태</th>
      <th class="w110">편집</th>
    </tr>
    <tr>
      <td class="w30 tc"><input type="checkbox" id="all" class="checkall"/></td>
      <td colspan="20"><label for="all" >전체선택</label>
        <a href="javascript:void(0);" class="ncsc-btn-mini" nc_type="batchbutton" uri="index.php?act=store_promotion_bundling&op=drop_bundling" name="bundling_id" confirm="정말 삭제 하시겠습니까?"><i class="icon-trash"></i>삭제</a></td>
    </tr>
  </thead>
  <?php foreach($output['list'] as $key=>$val){?>
  <tbody>
    <tr class="bd-line">
      <td><input type="checkbox" class="checkitem tc" value="<?php echo $val['bl_id'];?>"/></td>
      <td><div class="pic-thumb"><a <?php if ($val['goods_id'] != '') {echo 'href="' . urlShop('goods', 'index', array('goods_id' => $val['goods_id'])) . '" target="_blank"';} else { echo 'href="javascript:void(0);"';}?> target="black"><img src="<?php echo $val['img'];?>"/></a></div></td>
      <td class="tl"><dl class="goods-name">
          <dt><a <?php if ($val['goods_id'] != '') {echo 'href="' . urlShop('goods', 'index', array('goods_id' => $val['goods_id'])) . '" target="_blank"';} else { echo 'href="javascript:void(0);"';}?>><?php echo $val['bl_name_ko'];?></a></dt>
        </dl></td>
      <td class="goods-price"><?php echo number_format($val['bl_discount_price_ko']);?>원</td>
      <td class=""><?php echo $val['count'];?></td>
      <td><?php echo $output['state_array'][$val['bl_state']];?></td>
      <td class="nscs-table-handle"><span><a href="index.php?act=store_promotion_bundling&op=bundling_add&bundling_id=<?php echo $val['bl_id'];?>" class="btn-blue"><i class="icon-cog"></i>
        <p>수정</p>
        </a></span> <span><a class="btn-red" href='javascript:void(0);' onclick="ajax_get_confirm('정말 삭제 하시겠습니까?', '<?php echo urlShop('store_promotion_bundling', 'drop_bundling', array('bundling_id' => $val['bl_id']));?>');"><i class="icon-trash"></i>
        <p>삭제</p>
        </a></span></td>
    </tr>
  </tbody>
  <?php }?>
  <tfoot>
    <tr>
      <th class="tc"><input type="checkbox" id="all" class="checkall"/></th>
      <th colspan="20"><label for="all" >전체선택</label>
        <a href="javascript:void(0);" class="ncsc-btn-mini" nc_type="batchbutton" uri="index.php?act=store_promotion_bundling&op=drop_bundling" name="bundling_id" confirm="정말 삭제 하시겠습니까?"><i class="icon-trash"></i>삭제</a></th>
    </tr>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
  </tfoot>
</table>
<?php } ?>
