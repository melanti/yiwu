/**
 * Local environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

var lib_env = require("../libs/env.js"),
  env = lib_env.get(),
  tmpPort = (process.env.PORT === undefined) ? (env.DOMAIN_PORT) ? env.DOMAIN_PORT : 8088 : parseInt(process.env.PORT, 10),
  tmpImgURL = env.WEBIMG_URL;

var tpmVersions = require('../../package.json');

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the local             *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  urlinfo: {
    domain: env.DOMAIN,
    port: tmpPort,
    img: tmpImgURL,
    api: {
      host: env.APIHOST,
      port: env.APIPORT
    }
  },
  port: tmpPort,
  maxage: 60,
  ver: tpmVersions.version

};
