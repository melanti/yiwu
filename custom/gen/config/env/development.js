var lib_env = require("../../libs/env.js"),
env = lib_env.get();

module.exports = {
  db: {
    name: "mysql",
    host: env.DB_HOST,
    port: env.DB_PORT,
    user: env.DB_USER,
    password: env.DB_PWD,
    database: env.DB_NAME
  },

  aws: {
    s3: {
      data: {
        config: {
          accessKeyId: env.AWS_DATA_S3_KEY,
          secretAccessKey: env.AWS_DATA_S3_SECRET,
          region: env.AWS_DATA_S3_REGION
        },
        bucket: env.AWS_DATA_S3_BUCKET,
        url: env.AWS_DATA_S3_DOMAIN
      }
    }
  },

  ftp: {
    host: env.FTP_HOST,
    username: env.FTP_USERNAME,
    password: env.FTP_PASSWORD,
    protocol: env.FTP_PROTOCOL,
    port: env.FTP_PORT,
    escape: env.FTP_ESCAPE,
    retries: env.FTP_RETRIES,
    timeout: env.FTP_TIMEOUT,
    requiresPassword: env.FTP_REQUIRES_PASSWORD,
    autoConfirm: env.FTP_AUTO_CONFIRM,
    cwd: env.FTP_CWD,
    domain: env.FTP_DOMAIN
  }
};
