
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.org.list.ctrl.accOrgListDircCtrl');
  APP.modules.org.list.ctrl.accOrgListDircCtrl = accOrgListDircCtrl;

  //accOrgListDircCtrl
  function accOrgListDircCtrl($scope, $location, _, $timeout, XHR, PubSub, AccOrgListTableManager, API_URL_INFO, COMMON_UTIL){
    var vm = this,
      tableManager = AccOrgListTableManager.exports,
      pubsubBootstrap,
      btComplete;

    vm.bootstrapCtrl;
    vm._state={
      tableData:tableManager.getTableData(),
      bootstrapTableCtrl:{
        complete:{
          //tableOptionCtrl:false,
          //tableBodyCtrl:false,
          //paginationCtrl:false,
        }
      }
    };

    vm.exports={
      imgUrl:APP.info.imgURL,
      gVersion:APP.info.gVersion,
      COUNT_RANGE:tableManager.COUNT_RANGE,
      getReqModel:_getReqModel,
      setReqModel:_setReqModel,
      updatePage:_updatePage,
      deleteRow:_deleteRow
    };

    _init();

    function _init(){
      console.log('APP.modules.org.list.accOrgListDircCtrl: init' );

      (vm.bootstrapCtrl).split(',').forEach(function(element, index, array){
        vm._state.bootstrapTableCtrl.complete[element] =false;
      });

      btComplete = vm._state.bootstrapTableCtrl.complete;
      tableManager.init();

      $scope.$on('$destroy', function () {
        PubSub.unsubscribe(pubsubBootstrap);
      });

      pubsubBootstrap = PubSub.subscribe('tableComponent:bootstrap', _listenBootstrapState);
    }

    function _listenBootstrapState(topic, data){
      var chk = true;
      btComplete = _.extend({}, btComplete, data);
      _.values(btComplete).forEach(function(element, index, array){
        if(element===false) chk =false;
      });

      if( chk ){
        console.log('All bootstrap');

        PubSub.unsubscribe(pubsubBootstrap);
        PubSub.trigger('tableComponent:bootstrap:complete', {success:true});
        _updatePage();
      }
    }

    function _getReqModel(){
      return tableManager.getTableData().reqModel;
    }

    function _setReqModel(__options){
      var reqMdl = tableManager.getTableData().reqModel;
      _.each(__options , function(value, key, list){
        reqMdl[key] = value;
      });
    }

    function _updatePage(__options){
      console.log(__options)
      var tmpPage = (__options && __options.start >= 0) ? __options.start : tableManager.getTableData().info.start,
        tmpViewRange = (__options && __options.count) ? __options.count : tableManager.getTableData().info.count,
        parentOrgId = (__options && __options.parent_org_id) ? __options.parent_org_id : APP.info.user.parent_org_id,
        orgId = (__options && __options.org_id) ? __options.org_id : APP.info.user.org_id,
        opts = tableManager.getTableData().reqModel,
        reqData = _.extend({}, {start: tmpPage, count: tmpViewRange}, opts, {parent_org_id:parentOrgId});
      (parentOrgId !== orgId) && (reqData.org_id = orgId);

      tableManager
        .getTableList(reqData)
        .then(
          function(__sucData){
            if(__sucData.info){
              if( __sucData.info.start==null || __sucData.info.start=='null' )  {
                __sucData.info.start = parseInt(reqData.start);
              }
              __sucData.info.count = parseInt(reqData.count);
            }
            APP.modules.ctrl.tablePaginationBasicCtrl.beforeChangePage();
            tableManager.setTableData(__sucData);
            PubSub.trigger('table:change', tableManager.getTableData() );
          },
          function(__errData){
            alert( '업체 리스트 페이지 호출을 하지 못했습니다.' );  //
            console.log('accOrgListDircCtrl _updatePage __errData: ',__errData);
          }
        );

    }

    function _deleteRow(__options) {
      if (!__options) return;
      var opt = {
          account_uuid: '',
          kind: __options.kind || '',
          status: __options.status || '',
          show: __options.show || ''
        },
        strArr = _getCheckedListRow('account_no'),
        accTypeArr = _getCheckedListRow('account_type'),
        uuidArr = _.map(_getCheckedListRow('uuid') , function(v,k,l){return Sha256.hash(v);}),
        strShopNameArr;

      if (strArr.length < 1) {
        alert(__options.notSelectedStr);
        return false;
      }

      console.log( 'accTypeArr: ' ,accTypeArr );
      console.log( 'uuidArr: ' ,uuidArr );
      console.log( 'APP.info.user.account_uuid_hash ' , APP.info.user.account_uuid_hash );

      for( var i=0,iTotal=uuidArr.length; i<iTotal; ++i ){
        if( uuidArr[i]=== APP.info.user.account_uuid_hash){
          alert('본인은 삭제가 불가능합니다.');
          return false;
        }
      }

      if(APP.info.user.account_type==='shop'){
        for( var i=0,iTotal=accTypeArr.length; i<iTotal; ++i ){
          if( accTypeArr[i]=== 0 || accTypeArr[i]=== '0' ){
            alert('대표운영자계정의 삭제는 불가능합니다.');
            return false;
          }
        }
      }

      opt.account_uuid = strArr.join(',');
      strShopNameArr = "선택한 %s를 삭제하시겠습니까?".replace('%s', '\n' + _getCheckedListRowName().join(',') + '\n');

      if (confirm(strShopNameArr) == true) {
        //console.log('account_uuid: ' , opt.account_uuid);
        tableManager
          .deleteRow(opt)
          .then(
            function (__sucData) {
              //console.log('_deleteRow: ', __sucData);
              alert("삭제하였습니다.");
              _updatePage({options: {start: 0}});
            },
            function (__errData) {
              console.log('tableComponentBasicCtrl _deleteRow: ', __errData);
              alert("삭제가 정상적으로 되지 않았습니다.");
            }
          );
      } else {
        return false;
      }
    }

    function _getCheckedListRow(__attr){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push($(this).attr(__attr));
        }
      });
      return strArr;
    }

    function _getCheckedListRowName(){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push( $(this).attr('shop-name')+'('+$(this).attr('uuid')+')' );
        }
      });
      return strArr;
    }

  }
  accOrgListDircCtrl.$inject = ['$scope', '$location', '_', '$timeout', 'XHR', 'PubSub', 'AccOrgListTableManager', 'API_URL_INFO', 'COMMON_UTIL'];
  accOrgListDircCtrl.bindToController = {
    bootstrapCtrl:'@'
  };
  accOrgListDircCtrl.link = function () {
    return function (scope, elem, attrs, ctrl, transclude) {
      transclude(scope, function(clone, scope) {
        elem.find('.list_table').empty().append(clone);
      });
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
