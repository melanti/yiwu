<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>경험치관리</h3>
      <ul class="tab-base">
      	<li><a href="index.php?act=exppoints&op=index" ><span>경험치명세</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>규칙설정</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="post" name="settingForm" id="settingForm">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
        <tr>
          <td class="" colspan="2"><table class="table tb-type2 nomargin">
              <thead>
                <tr class="space">
                  <th colspan="16">경험치 획은 규칙:</th>
                </tr>
                <tr class="thead">
                  <th>항목</th>
                  <th>획득 경험치</th>
                </tr>
              </thead>
              <tbody>
                <tr class="hover">
                  <td class="w200">회원이 매일 로그인시</td>
                  <td><input id="exp_login" name="exp_login" value="<?php echo $output['list_setting']['exppoints_rule']['exp_login'];?>" class="txt" type="text" style="width:60px;"></td>
                </tr>
                <tr class="hover">
                  <td class="w200">주문 상품 댓글</td>
                  <td><input id="exp_comments" name="exp_comments" value="<?php echo $output['list_setting']['exppoints_rule']['exp_comments'];?>" class="txt" type="text" style="width:60px;"></td>
                </tr>
              </tbody>
            </table>
            <table class="table tb-type2 nomargin">
              <thead>
                <tr class="thead">
                  <th colspan="2">구매</th>
                </tr>
              </thead>
              <tbody>
                <tr class="hover">
                  <td class="w200">소비 금액 및 경험치 비율</td>
                  <td><input id="exp_orderrate" name="exp_orderrate" value="<?php echo $output['list_setting']['exppoints_rule']['exp_orderrate'];?>" class="txt" type="text" style="width:60px;">본 값은 0보다 큰 숫자로 입력하세요, 예: 10으로 설정시 10원 소비시 경험치 1획득</td>
                </tr>
                <tr class="hover">
                  <td>매 주문 최대 획득 경험치</td>
                  <td><input id="exp_ordermax" name="exp_ordermax" value="<?php echo $output['list_setting']['exppoints_rule']['exp_ordermax'];?>" class="txt" type="text" style="width:60px;">본 값은 0과 같거나 큰 숫자를 입력하세요, 0으로 입력시 제한이 없습니다, 예:100으로 설정시 매 주문에 최대로 획득하는 경험치는 100입니다.</td>
                </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="2" ><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script>
$(function(){
	$("#submitBtn").click(function(){
		$("#settingForm").submit();
	});
});
</script> 
