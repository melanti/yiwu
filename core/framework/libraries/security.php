<?php
/**
 * 安全防范
 * 进行sql注入过滤，xss过滤和csrf过滤
 * 
 * 令牌调用方式
 * 输出：直接在模板上调用getToken
 * 验证：在验证位置调用checkToken
 *
 * @package    library* www.cnbiz.co.kr씨엔비즈为你提供售后服务 以便你更好的了解
 */
defined('InCNBIZ') or exit('Access Invalid!');

class Security{
	/**
	 * 取得令牌内容
	 * 自动输出html 隐藏域
	 *
	 * @param 
	 * @return void 字符串形式的返回结果
	 */
	public static function getToken(){
		$token = encrypt(TIMESTAMP,md5(MD5_KEY));
		echo "<input type='hidden' name='formhash' value='". $token ."' />";
	}
	public static function getTokenValue(){
        return encrypt(TIMESTAMP,md5(MD5_KEY));
	}

	/**
	 * 判断令牌是否正确
	 * 
	 * @param 
	 * @return bool 布尔类型的返回结果
	 */
	public static function checkToken(){
		$data = decrypt($_POST['formhash'],md5(MD5_KEY));
		return $data && (TIMESTAMP - $data < 5400);
	}

	/**
	 * 将字符 & " < > 替换
	 *
	 * @param unknown_type $string
	 * @return unknown
	 */
	public static function fliterHtmlSpecialChars($string) {
		$string = preg_replace('/&amp;((#(\d{3,5}|x[a-fA-F0-9]{4})|[a-zA-Z][a-z0-9]{2,5});)/', '&\\1',
				str_replace(array('&', '"', '<', '>'), array('&amp;', '&quot;', '&lt;', '&gt;'), $string));
		return $string;
	}

	/**
	 * 参数过滤
	 *
	 * @param array 参数内容
	 * @param array $ignore 被忽略过滤的元素
	 * @return array 数组形式的返回结果
	 * 
	 */
	public static function getAddslashesForInput($array,$ignore=array()){
        if (!function_exists('htmlawed')) require(BASE_CORE_PATH.'/framework/function/htmlawed.php');
		if (!empty($array)){
			while (list($k,$v) = each($array)) {
				if (is_string($v)) {
                    if (get_magic_quotes_gpc()) {
                        $v = stripslashes($v);
                    }		
					if($k != 'statistics_code') {
						if (!in_array($k,$ignore)){
							//如果不是编辑器，则转义< > & "
                            $v = self::fliterHtmlSpecialChars($v);
                        } else {
                            $v = htmlawed($v,array('safe'=>1));
                        }
                        if($k == 'ref_url') {
                            $v = str_replace('&amp;','&',$v);
                        }
					}
					$array[$k] = addslashes(trim($v));
				} else if (is_array($v))  {
					if($k == 'statistics_code') {
						$array[$k] = $v;
					} else {
						$array[$k] = self::getAddslashesForInput($v);
					}
				}
			}
			return $array;
		}else {
			return false;
		}
	}
	public static function getAddSlashes($array){
		if (!empty($array)){
			while (list($k,$v) = each($array)) {
				if (is_string($v)) {
					if (!get_magic_quotes_gpc()) {
						$v = addslashes($v);
					}
				} else if (is_array($v))  {
					$array[$k] = self::getAddSlashes($v);
				}
			}
			return $array;
		}else {
			return false;
		}
	}
	
	/**
	 * 로그인시 저장시킬 시크릿키 생성
	 *
	 * @return string 해쉬값
	 *
	 */	
	public static function createSecretKey(){
	
		$uuid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				// 32 bits for "time_low"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff),
	
				// 16 bits for "time_mid"
				mt_rand(0, 0xffff),
	
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand(0, 0x0fff) | 0x4000,
	
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand(0, 0x3fff) | 0x8000,
	
				// 48 bits for "node"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
				);
	
		$secret_key = preg_replace('/\W+/', "", base64_encode(hash('sha256', $uuid, true)));
		return $secret_key;
	}	
	

	/**
	 * member_id 암호화
	 *
	 * @return aes-128  해쉬값
	 *
	 */
	public function getCryptMemberId ($data) {
		global $config;
		$encryptKey = $config['fxgear_salt_key'];
		//$data = "imcore.net";
		//$encryptKey = 'fxgear2445!!1234';
		
		if (!function_exists('mcrypt_encrypt')) {
			return "not exists function";
		}
		
		$iv = '1234567890123456';
		$blocksize = 16;
	
		$pad = $blocksize - (strlen($data) % $blocksize);
		$data = $data . str_repeat(chr($pad), $pad);
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128,
				$encryptKey,
				$data, MCRYPT_MODE_CBC, $iv));
	}
	
	public function getDecryptMemberId ($data) {
		global $config;
		$encryptKey = $config['fxgear_salt_key'];
		$iv = '1234567890123456';
		$blocksize = 16;

		//return $this->unpad($data, $blocksize);
		
		$len = mb_strlen($data);
		$pad = ord( $data[$len - 1] );
		if ($pad && $pad < $blocksize) {
			$pm = preg_match('/' . chr($pad) . '{' . $pad . '}$/', $data);
			if( $pm ) {
				$str = mb_substr($data, 0, $len - $pad);
			}
		}
		
		return $str;
		
		/*
		return $this->unpad(base64_decode(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,
				$encryptKey,
				$data,
				MCRYPT_MODE_CBC, $iv), $blocksize));
		*/
		
	}
	
	
	private function unpad($str, $blocksize)
	{
		$len = mb_strlen($str);
		$pad = ord( $str[$len - 1] );
		if ($pad && $pad < $blocksize) {
			$pm = preg_match('/' . chr($pad) . '{' . $pad . '}$/', $str);
			if( $pm ) {
				return mb_substr($str, 0, $len - $pad);
			}
		}
		return $str;
	}		
	
}
