<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 运费模板列表
 */
$lang['transport_tpl_add']				= '배송료 템플릿 추가';
$lang['transport_tpl_help']				= '도움말';
$lang['transport_tpl_edit_time']		= '마지막 수정시간';
$lang['transport_tpl_copy']				= '복사';
$lang['transport_tpl_edit']				= '수정';
$lang['transport_tpl_del']				= '삭제';
$lang['transport_type']					= '운송방식';
$lang['transport_to']					= '운송지역';
$lang['transport_snum']					= '처음(개)';
$lang['transport_price']				= '배송료(원)';
$lang['transport_xnum']					= '추가시(개)';
$lang['transport_applay']				= '사용하기';
$lang['transport_del_confirm']			= '정말 본 템플릿을 삭제하시겠습니까?';
/**
 * 新运费模板
 */
$lang['transport_note_1']				= '기본배송료';
$lang['transport_note_2']				= 'Kg이내，';
$lang['transport_note_3']				= '，매 ';
$lang['transport_note_4']				= 'Kg, 추가 배송료';

$lang['transport_tpl_name']				= '템플릿명';
$lang['transport_tpl_name_note']		= '템플릿명을 입력하세요';
$lang['transport_type_description']		= '지정 지역외 기본 배송료로 설정됩니다.';
$lang['transport_type_note']			= '최소 하나의 운송방식을 선택하세요';
$lang['transport_tpl_save']				= '저장';
$lang['transport_tpl_select_area']		= '지역선택';
$lang['transport_tpl_ok']				= '확인';
$lang['transport_tpl_cancel']			= '취소';
$lang['transport_tpl_pl_op']			= '일괄조작';
$lang['transport_clone_name']			= '의 복사본';
$lang['transport_op_fail']				= '조작실패';
$lang['transport_op_using']				= '본 템플릿은 사용중이므로 삭제할 수 없습니다.';
$lang['transport_country']				= '전국';