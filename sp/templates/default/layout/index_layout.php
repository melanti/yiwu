<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;" charset="<?php echo CHARSET;?>">
<title><?php echo $output['html_title'];?></title>
<link href="<?php echo ADMIN_TEMPLATES_URL;?>/css/skin_0.css" rel="stylesheet" type="text/css" id="cssfile"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="<?php echo RESOURCE_SITE_URL;?>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->


		<!-- ace styles -->

		<link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/html5shiv.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/respond.min.js"></script>
		<![endif]-->
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.cookie.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?php echo RESOURCE_SITE_URL;?>/js/html5shiv.js"></script>
      <script src="<?php echo RESOURCE_SITE_URL;?>/js/respond.min.js"></script>
<![endif]-->

<script>
//
$(document).ready(function () {
    $('span.bar-btn').click(function () {
	$('ul.bar-list').toggle('fast');
    });
});

$(document).ready(function(){
	var pagestyle = function() {
		var iframe = $("#workspace");
		var h = $(window).height() - iframe.offset().top;
		var w = $(window).width() - iframe.offset().left;
		if(h < 300) h = 300;
		if(w < 953) w = 953;
		iframe.height(h);
		iframe.width(w);
	}
	pagestyle();
	$(window).resize(pagestyle);
	//turn location
	if($.cookie('now_location_act') != null){
		openItem($.cookie('now_location_op')+','+$.cookie('now_location_act')+','+$.cookie('now_location_nav'));
	}else{
		$('#mainMenu>ul').first().css('display','block');
		//第一次进入后台时，默认定到欢迎界面
		$('#item_welcome').addClass('selected');
		$('#workspace').attr('src','index.php?act=dashboard&op=welcome');
	}
	$('#iframe_refresh').click(function(){
		var fr = document.frames ? document.frames("workspace") : document.getElementById("workspace").contentWindow;;
		fr.location.reload();
	});

});
//收藏夹
function addBookmark(url, label) {
    if (document.all)
    {
        window.external.addFavorite(url, label);
    }
    else if (window.sidebar)
    {
        window.sidebar.addPanel(label, url, '');
    }
}


function openItem(args){
    closeBg();
	//cookie

	if($.cookie('<?php echo COOKIE_PRE?>sys_key') === null){
		location.href = 'index.php?act=login&op=login';
		return false;
	}

	spl = args.split(',');
	op  = spl[0];
	try {
		act = spl[1];
		nav = spl[2];
	}
	catch(ex){}
	if (typeof(act)=='undefined'){var nav = args;}
	$('.actived').removeClass('actived');
	$('#nav_'+nav).addClass('actived');

	$('.selected').removeClass('selected');
	//show
	$('#mainMenu ul').css('display','none');
	if(nav=='dashboard'){
		$('.submenu').css('display','none');
		$('.nav-list > li').removeClass('open');
	}
	$('#sort_'+nav).css('display','block');

	if (typeof(act)=='undefined'){
		//顶部菜单事件
		html = $('#sort_'+nav+'>li>dl>dd>ol>li').first().html();
		str = html.match(/openItem\('(.*)'\)/ig);
		arg = str[0].split("'");
		spl = arg[1].split(',');
		op  = spl[0];
		act = spl[1];
		nav = spl[2];
		first_obj = $('#sort_'+nav+'>li>dl>dd>ol>li').first().children('a');
		$(first_obj).addClass('selected');
		//crumbs
		$('#crumbs').html('<li><i class="icon-home home-icon"></i><a href="index.php"><?php echo $lang['nc_welcome_page'];?></a></li>&nbsp;<li class="active">'+$('#nav_'+nav+' > span').text()+'</li><li class="active">'+$(first_obj).text()+'</li>');
	}else{
		//左侧菜单事件
		//location
		$.cookie('now_location_nav',nav);
		$.cookie('now_location_act',act);
		$.cookie('now_location_op',op);
		$("a[name='item_"+op+act+"']").addClass('selected');
		//crumbs
		$('#crumbs').html('<li><i class="icon-home home-icon"></i><a href="javascript:void(0);" onclick="openItem(\'welcome,dashboard,dashboard\');"><?php echo $lang['nc_welcome_page'];?></a></li><li class="active">'+$('#nav_'+nav+' > span').text()+'</li>&nbsp;<li class="active">'+$('#item_'+op+act+' > span').text()+'</li>');
	}
	src = 'index.php?act='+act+'&op='+op;
	$('#workspace').attr('src',src);

}

$(function(){
		bindAdminMenu();
		})
		function bindAdminMenu(){

		$("[nc_type='parentli']").click(function(){
			var key = $(this).attr('dataparam');
			if($(this).find("dd").css("display")=="none"){
				$("[nc_type='"+key+"']").slideDown("fast");
				$(this).find('dt').css("background-position","-322px -170px");
				$(this).find("dd").show();
			}else{
				$("[nc_type='"+key+"']").slideUp("fast");
				$(this).find('dt').css("background-position","-483px -170px");
				$(this).find("dd").hide();
			}
		});
	}
</script>
<script type="text/javascript">
//노출灰色JS遮罩层
function showBg(ct,content){
var bH=$("body").height();
var bW=$("body").width();
var objWH=getObjWh(ct);
$("#pagemask").css({width:bW,height:bH,display:"none"});
var tbT=objWH.split("|")[0]+"px";
var tbL=objWH.split("|")[1]+"px";
$("#"+ct).css({top:tbT,left:tbL,display:"block"});
$(window).scroll(function(){resetBg()});
$(window).resize(function(){resetBg()});
}
function getObjWh(obj){
var st=document.documentElement.scrollTop;//滚动条距顶部的距离
var sl=document.documentElement.scrollLeft;//滚动条距左边的距离
var ch=document.documentElement.clientHeight;//屏幕的高度
var cw=document.documentElement.clientWidth;//屏幕的宽度
var objH=$("#"+obj).height();//浮动상대的高度
var objW=$("#"+obj).width();//浮动상대的宽度
var objT=Number(st)+(Number(ch)-Number(objH))/2;
var objL=Number(sl)+(Number(cw)-Number(objW))/2;
return objT+"|"+objL;
}
function resetBg(){
var fullbg=$("#pagemask").css("display");
if(fullbg=="block"){
var bH2=$("body").height();
var bW2=$("body").width();
$("#pagemask").css({width:bW2,height:bH2});
var objV=getObjWh("dialog");
var tbT=objV.split("|")[0]+"px";
var tbL=objV.split("|")[1]+"px";
$("#dialog").css({top:tbT,left:tbL});
}
}

//닫힘灰色JS遮罩层和조작窗口
function closeBg(){
$("#pagemask").css("display","none");
$("#dialog").css("display","none");
}
</script>
<script type="text/javascript">
$(function(){
    var $li =$("#skin li");
		$li.click(function(){
		$("#"+this.id).addClass("selected").siblings().removeClass("selected");
		$("#cssfile").attr("href","<?php echo ADMIN_TEMPLATES_URL;?>/css/"+ (this.id) +".css");
        $.cookie( "MyCssSkin" ,  this.id , { path: '/', expires: 10 });

        //$('iframe').contents().find('#cssfile2').attr("href","<?php echo ADMIN_TEMPLATES_URL;?>/css/"+ (this.id) +".css");
    });

    var cookie_skin = $.cookie( "MyCssSkin");
    if (cookie_skin) {
		$("#"+cookie_skin).addClass("selected").siblings().removeClass("selected");
		//$("#cssfile").attr("href","<?php echo ADMIN_TEMPLATES_URL;?>/css/"+ cookie_skin +".css");
		$.cookie( "MyCssSkin" ,  cookie_skin  , { path: '/', expires: 10 });
    }
});
function addFavorite(url, title) {
	try {
		window.external.addFavorite(url, title);
	} catch (e){
		try {
			window.sidebar.addPanel(title, url, '');
        	} catch (e) {
			showDialog("<?php echo $lang['nc_to_favorite'];?>", 'notice');
		}
	}
}
</script>

</head>

<body style="min-width: 1200px; margin: 0px; overflow-x:hidden;overflow-y:hidden">

<?php 
log::record("system admin account_uuid ". $output['admin_info']['account_uuid'], LOG::ERR);
log::record("system admin account_type ". $output['admin_info']['account_type'], LOG::ERR);
?>



<div id="pagemask"></div>
<div id="dialog" style="display:none">
  <div class="title">
    <h3><?php echo $lang['nc_admin_navigation'];?></h3>
    <span><a href="JavaScript:void(0);" onclick="closeBg();"><?php echo $lang['nc_close'];?></a></span> </div>
  <div class="content">
  <?php foreach ($output['map_nav'] as $k=>$v) {?>
  <dl>
  <dt><?php echo $v['text'];?></dt>
  	<?php foreach ($v['list'] as $key=>$value) {?>
  	<dd><a href="javascript:void(0)" onclick="openItem('<?php echo $value['args']?>')"><?php echo $value['text'];?></a></dd>
  	<?php }?>
  	 </dl>
  <?php }?>
  </div>
</div>

<table style="width: 100%;" id="frametable" height="100%" width="100%" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td colspan="2" class="mainhd"><!-- Title/Logo - can use text instead of image -->
          <!-- Top navigation -->
		<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="index.php" class="navbar-brand">
						<small>
							<i class="icon-leaf"></i>
							시스템관리자
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo RESOURCE_SITE_URL;?>/assets/avatars/user.jpg" alt="Jason's Photo" />
								<span class="user-info">
									<small><?php echo $lang['nc_hello'];?></small>
									<?php echo $output['admin_info']['name'];?>
								</span>

								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="index.php?act=index&op=modifypw" target="workspace">
										<i class="icon-cog"></i>
										<?php echo $lang['nc_modifypw']; ?>
									</a>
								</li>

								<li>
									<a href="<?php echo SHOP_SITE_URL;?>" target="_blank">
										<i class="icon-user"></i>
										<?php echo $lang['nc_homepage'];?>
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="index.php?act=index&op=logout">
										<i class="icon-off"></i>
										<?php echo $lang['nc_logout'];?>
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
          <!-- End of Top navigation -->
          <!-- Main navigation -->

        </div></td>
    </tr>
    <tr>
      <td class="mainleft" valign="top" width="161">
			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list open">
						<li>
							<a href="javascript:void(0);" onclick="openItem('welcome,dashboard,dashboard');" id="nav_dashboard">
								<i class="icon-dashboard"></i>
								<span class="menu-text"> <?php echo $lang['nc_console'];?> </span>
							</a>
						</li>
           				<?php foreach($output['map_nav'] as $k=>$v):?>
						<li>
							<a href="javascript:void(0);" class="dropdown-toggle" id="nav_<?php echo $v['nav']?>">
								<i class="icon-<?php echo $v['icon'];?>"></i>
								<span class="menu-text"> <?php echo $v['text'];?> </span>

								<b class="arrow icon-angle-down"></b>
							</a>
							<ul class="submenu">
           					<?php foreach($v['list'] as $sk=>$sv):?>
						<?php $tmp_args = explode(',',$sv['args']);?>
								<li>
									<a href="javascript:void(0)" onclick="openItem('<?php echo $sv['args']?>')" id="item_<?php echo $tmp_args['0'].$tmp_args['1'];?>" name="item_<?php echo $tmp_args['0'].$tmp_args['1'];?>">
										<i class="icon-double-angle-right"></i>
										<span><?php echo $sv['text'];?></span>
									</a>
								</li>
							<?php endforeach;?>
							</ul>
						</li>
					<?php endforeach;?>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>

					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>

				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="icon-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#e44b50">#e44b50</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; 选择皮肤</span>
						</div>

						<div>
					            <a id="siteMapBtn" href="#rhis" onclick="showBg('dialog','dialog_content');" class="btn btn-xs btn-danger"><span><?php echo $lang['nc_sitemap'];?></span></a>
						</div>

						<div>
						<a onclick="openItem('clear,cache,setting');" href="javascript:void(0)" class="btn btn-xs btn-danger"><?php echo $lang['nc_update_cache'];?></a>
						</div>
						<div>
						<a href="<?php echo ADMIN_SITE_URL;?>" id="iframe_refresh" class="btn btn-xs btn-danger"><?php echo $lang['nc_refresh'];?></a>
						</div>
						<div>
						<a href="<?php echo ADMIN_SITE_URL;?>" class="btn btn-xs btn-danger" title="<?php echo $lang['nc_admincp']; ?>-<?php echo $output['html_title'];?>" rel="sidebar" onclick="addFavorite('<?php echo ADMIN_SITE_URL;?>', '<?php echo $output['html_title'];?>-<?php echo $lang['nc_admincp']; ?>');return false;"><?php echo $lang['nc_favorite']; ?></a>
						</div>

					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

      <div class="copyright">&copy; 2014-<?php echo date("Y",time());?> <a href="http://www.cnbiz.co.kr" target="_blank">씨엔비즈</a></div></td>
      <td valign="top" width="100%">
				<div id="breadcrumbs" class="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb" id="crumbs">
						</ul><!-- .breadcrumb -->
					</div>
      <iframe src="" id="workspace" name="workspace" style="overflow: visible;" frameborder="0" width="100%" height="100%" scrolling="yes" onload="window.parent"></iframe></td>
    </tr>
  </tbody>
</table>

		<!-- basic scripts -->

		<!--[if !IE]> -->


		<!-- <![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/bootstrap.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.slimscroll.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.sparkline.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/flot/jquery.flot.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/ace-elements.min.js"></script>
		<script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/ace.min.js"></script>

</body>
</html>
