<?php
/**
 * 店铺卖家注销
 *
 *
 *
 **by J.K*/


defined('InCNBIZ') or exit('Access Invalid!');

class seller_logoutControl extends BaseSellerControl {

	public function __construct() {
		parent::__construct();
	}

    public function indexOp() {
        $this->logoutOp();
    }

    public function logoutOp() {
        $this->recordSellerLog('로그아웃 성공');

        /****************************************************************
         *  판매자 로그아웃시 시크릿키 삭제
         *
         *  판매관리자로 로그인했을때 일반회원, 판매관리자의 2가지 세션값과 시크릿키가 생성되기때문에
         *  판매자로 로그인할경우에는 일반회원, 판매관리자의 2가지의 시크릿키를 동시에 삭제시킨다
         ***************************************************************/        
		if (method_exists('Security', 'createSecretKey')) {
        	$model_secret = Model("secret");

        	// 판매관리자로 로그인했을때  로그아웃하면 일반회원 시크릿키까지 같이 삭제
        	if (!empty($_SESSION['store_id'])) {
        		
        		// 시크릿키 테이블에 일반회원용 시크릿키 존재여부 확인후 시크릿키 삭제처리
        		$is_secretkey = $model_secret->isSecretKeyByType($_SESSION['member_id'], $_SESSION['secret_key']);
        		if ($is_secretkey) {
        			$array_input = array();
        			$array_input['member_id'] = $_SESSION['member_id'];
        			$array_input['secret_key'] = $_SESSION['secret_key'];
        	
        			// 일반회원용 시크릿키 삭제
        			$del_secretkey = $model_secret->delSecretKey($array_input);
        			if (!$del_secretkey) {
        				$message = "secret_key delete fail at seller logout";
        				log::record($message, LOG::ERR);
        			}
        		}
        	
        		// 시크릿키 테이블에 판매관리자용 시크릿키 존재여부 확인후 시크릿키 삭제 처리
        		$is_seller_secretkey = $model_secret->isSecretKeyByType($_SESSION['store_id'],  $_SESSION['store_secret_key']);
        		if ($is_seller_secretkey) {
        			$array_input = array();
        			$array_input['member_id'] = $_SESSION['store_id'];
        			$array_input['secret_key'] = $_SESSION['store_secret_key'];
        	
        			// 판매관리자용 시크릿키 삭제
        			$del_secretkey = $model_secret->delSecretKey($array_input);
        			if (!$del_secretkey) {
        				$message = "secret_key delete fail at seller logout";
        				log::record($message, LOG::ERR);
        			}
        		}
        	} 
        }
        
        // 清除店铺消息数量缓存
        setNcCookie('storemsgnewnum'.$_SESSION['seller_id'],0,-3600);
        session_destroy();
        redirect('index.php?act=seller_login');
    }

}
