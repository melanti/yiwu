(function($, angular, _, APP){

  var tableComponentDirName = 'tableBrand';

  if( $(document.body).attr('data-page-id')=='brand-list' ){
    bootstrapBrandList();
  }

  function bootstrapBrandList(){
    console.log('[[bootstrapBrandList:]]');

    angular.module('App')
      .directive( tableComponentDirName, tableComponentDirective );

    angular.module('App')
      .directive( 'tableSearch', tableSearchDirective );

    angular.module('App')
      .directive( 'tableOption', tableOptionDirc );

    angular.module('App')
      .directive( 'tableBody', tableBodyDirective );

    angular.module('App')
      .directive( 'tableBtn', btnDirc );

    angular.module('App')
      .directive( 'pagination', paginationDirc );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
        .then(
          function(__sucData){
            APP.info.user.auth = JSON.parse(__sucData.data);
            for(var i in APP.info.user.auth){
              if(i==='account_uuid'){
              }else{
                APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
              }
            }

            APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

            if( parseInt(APP.info.user.auth.brand,10)>=1 ){
              angular.bootstrap( $('html'), ['App']);
            }else{
              APP.setup403Page();
              angular.bootstrap( $('html'), ['App']);
            }

          },
          function(__failData){
            console.log('getRedis fail: ' , __failData);
          }
        );
    //angular.bootstrap( $('html'), ['App']);


  }



  function tableComponentDirective(XHR, BrandTableManager, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.ctrl.tableComponentBrandCtrl,
      controllerAs: 'tableComponentBrandCtrl',
      bindToController: APP.modules.ctrl.tableComponentBrandCtrl.bindToController,
      link: APP.modules.ctrl.tableComponentBrandCtrl.link(PubSub),
      templateUrl: 'table_component_basic.html'
    };
  }
  tableComponentDirective.$inject = ['XHR', 'BrandTableManager', 'PubSub', 'URL_INFO'];



  function tableSearchDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableSearchBrandCtrl,
      controllerAs: 'tableSearchCtrl',
      bindToController: APP.modules.ctrl.tableSearchBrandCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tableSearchBrandCtrl.link(PubSub),
      templateUrl: 'table_search_brand.html'
    };
  }
  tableSearchDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];


  function tableOptionDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableOptionsBasicCtrl,
      controllerAs: 'tableOptionCtrl',
      bindToController: APP.modules.ctrl.tableOptionsBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link:  APP.modules.ctrl.tableOptionsBasicCtrl.link(PubSub),
      templateUrl: 'table_option_basic.html'
    };
  }
  tableOptionDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function tableBodyDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableBodyBrandCtrl,
      controllerAs: 'tableBodyCtrl',
      bindToController: APP.modules.ctrl.tableBodyBrandCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tableBodyBrandCtrl.link(PubSub),
      templateUrl: 'table_body_brand.html'
    };
  }
  tableBodyDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function btnDirc(XHR, PubSub, URL_INFO) {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableBtnBasicCtrl,
      controllerAs: 'tableBtnBasicCtrl',
      bindToController: APP.modules.ctrl.tableBtnBasicCtrl.bindToController,
      require: ('^' + tableComponentDirName),
      link: APP.modules.ctrl.tableBtnBasicCtrl.link(PubSub),
      templateUrl: 'table_btn_basic.html'
    }
  }
  btnDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];


  function paginationDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tablePaginationBasicCtrl,
      controllerAs: 'tablePaginationBasicCtrl',
      bindToController:  APP.modules.ctrl.tablePaginationBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tablePaginationBasicCtrl.link(PubSub),
      templateUrl: 'table_pagination_basic.html'
    };
  }
  paginationDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];


})(window.jQuery, window.angular, window._, window.APP);
