
//set/td Query
var UPLOAD_TD_QRY = {
    SELECT_STORE_INFO:[
        "SELECT a.member_id, a.store_id, c.store_name ",
        "FROM sv_seller a ",
        "LEFT JOIN sv_seller_group b ON a.seller_group_id = b.group_id ",
        "LEFT JOIN sv_store c ON a.store_id = c.store_id ",
        "WHERE (is_admin = 1 OR FIND_IN_SET('cat_login', limits))",
        "AND a.member_id = ?"
    ].join(''),
    SELECT_CAT_NO_FROM_3DC_CATE:"SELECT 3dc_category_no cate_no FROM sv_3dc_category WHERE folder_name = ?",
    INSERT_INTO_3DC:[
        "INSERT INTO sv_3dc ",
        "(3dc_name, 3dc_category_no, 3dc_status, 3dc_jingle, store_id, store_name, regist_date) ",
        "VALUES ",
        "(?, ?, ?, ?, ?, ?, unix_timestamp(now()))"
    ].join(''),
    SELECT_TD_ID_TD_CD_FROM_3DC:"select 3dc_no td_id, 3dc_code td_cd from sv_3dc where 3dc_no = ?",
    REPLACE_3DC_FILE:[
        "REPLACE sv_3dc_file ",
        "( 3dc_no, 3dc_file_category_no, file_hash, regist_date ) ",
        "VALUES ",
        "(?, (SELECT 3dc_file_category_no FROM sv_3dc_file_category WHERE 3dc_file_category_file_name = ?), ?, unix_timestamp(now()) )"
    ].join(''),



    SELECT_ITEM:[
        "SELECT a.goods_commonid, a.goods_id, a.goods_name, a.store_id, a.store_name, a.brand_id, b.brand_name, a.goods_sex, a.goods_kids ",
        "FROM sv_goods a LEFT JOIN sv_brand b ON a.brand_id = b.brand_id WHERE a.goods_commonid=? group by a.goods_commonid"
    ].join(''),
    BEFORE_INSERT_INTO_3DC_EXPSR:'UPDATE sv_3dc_exposure SET 3dc_representative_yn = 0 WHERE goods_commonid = ? AND store_id=?',
    INSERT_INTO_3DC_EXPSR:[
        "INSERT INTO sv_3dc_exposure ",
        "(",
        "3dc_no, goods_id, goods_commonid, goods_name, store_id, store_name, brand_id, brand_name, 3dc_representative_yn, regist_date",
        ") ",
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp(now()))"
    ].join('')
};

var ATTACH_TD_QRY = {
    SELECT_FROM_SV_SELLER:[
        "select a.store_id ",
        "from sv_seller a ",
        "left join sv_seller_group b on a.seller_group_id = b.group_id ",
        "where (a.is_admin = 1 or find_in_set('cat_login', b.limits)) ",
        "and a.member_id = ?"
    ].join(''),

    SELECT_FROM_SV_GOODS:[
        "select b.goods_id, a.goods_commonid, a.goods_name, a.store_id, c.store_name, a.brand_id, d.brand_name ",
        "FROM sv_goods a ",
        'left join sv_goods b on a.goods_commonid = b.goods_commonid ',
        'left join sv_store c on a.store_id = c.store_id ',
        'left join sv_brand d on a.brand_id = d.brand_id ',
        "where a.store_id=? ",
        "and a.goods_commonid = ? ",
        "group by a.goods_commonid"
    ].join(''),

    SELECT_FROM_SV_3DC:[
        "SELECT IF(count(*) > 0, true, false) having_3dc ",
        "FROM sv_3dc ",
        "WHERE store_id = ? ",
        "AND find_in_set(3dc_no, ?)"
    ].join(''),

    REPLACE_SV_3DV_EXPOSURE:[
        "REPLACE sv_3dc_exposure ",
        "( goods_id, goods_commonid, goods_name, store_id, store_name, brand_id, brand_name, ",
        " 3dc_no,  3dc_representative_yn, regist_date ) ",
        "VALUES "
        // "( ?,?,?,?,?,?,?,",
        // " ( SELECT 3dc_no, if(3dc_no in (?), true, false), unix_timestamp(now()) FROM sv_3dc WHERE store_id=? AND find_in_set(3dc_no, ?) ) )"
    ].join(''),

    DELETE_SV_3DV_EXPOSURE:[
        "DELETE FROM sv_3dc_exposure ",
        "WHERE store_id = ? ",
        "AND goods_id = ? ",
        "AND not find_in_set(3dc_no, ?) "
    ].join('')
};

var MAKE_CATE_QRY = {
    SELECT_SW_VERSION: [
        "SELECT software_version as ver",
        "  FROM sv_software_deploy ",
        " WHERE software_name='cat_csv'"
    ].join('')
};

var MOBILE_ADV_QRY = {
    BANNER_ADV: [
        "select item_data ",
        "from sv_mb_special_item ",
        " where item_type = 'adv_list'"
    ].join('')	
};

module.exports = {
    ATTACH_TD_QRY:ATTACH_TD_QRY,
    UPLOAD_TD_QRY:UPLOAD_TD_QRY,
    MAKE_CATE_QRY:MAKE_CATE_QRY,
    MOBILE_ADV_QRY:MOBILE_ADV_QRY
};
