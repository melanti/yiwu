<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page"> 
  <!-- 페이지导航 -->
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $output['item_title'];?></h3>
      <ul class="tab-base">
        <?php   foreach($output['menu'] as $menu) {  if($menu['menu_key'] == $output['menu_key']) { ?>
        <li><a href="JavaScript:void(0);" class="current"><span><?php echo $menu['menu_name'];?></span></a></li>
        <?php }  else { ?>
        <li><a href="<?php echo $menu['menu_url'];?>" ><span><?php echo $menu['menu_name'];?></span></a></li>
        <?php  } }  ?>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <!-- 帮助 -->
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12" class="nobg"> <div class="title nomargin">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span> </div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>오른쪽 모듈의 <strong>"추가"</strong>버튼을 클릭하여 모바일 페이지추가할 수 있습니다, 다만 <strong>"광고모듈"</strong>은 하나만 추가할 수 있습니다.</li>
            <li>모듈 추가후 오른쪽 부분에서 해동 모듈에 대해 <strong>"이동", "시동/금지", "수정", "삭제"</strong>를 진행하여 페이지를 구성합니다.</li>
            <li>추가된 모듈은 기본 <strong>"금지"</strong>상태입니다, <strong>"시동"</strong>상태만 모바일페이지에서 노출됩니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <!-- 리스트 -->
  <div class="mb-special-layout">
    <div class="mb-item-box">
      <div id="item_list" class="item-list">
        <?php if(!empty($output['list']) && is_array($output['list'])) {?>
        <?php foreach($output['list'] as $key => $value) {?>
        <div cbtype="special_item" class="special-item <?php echo $value['item_type'];?> <?php echo $value['usable_class'];?>" data-item-id="<?php echo $value['item_id'];?>">
          <div class="item_type"><?php echo $output['module_list'][$value['item_type']]['desc'];?></div>
          <?php $item_data = $value['item_data'];?>
          <?php $item_edit_flag = false;?>
          <div id="item_edit_content">
            <?php require('mb_special_item.module_' . $value['item_type'] . '.php');?>
          </div>
          <div class="handle"><a cbtype="btn_move_up" href="javascript:;"><i class="icon-arrow-up"></i>위로</a> <a cbtype="btn_move_down" href="javascript:;"><i class="icon-arrow-down"></i>아래로</a> <a cbtype="btn_usable" data-item-id="<?php echo $value['item_id'];?>" href="javascript:;"><i class="icon-off"></i>시동</a> <a cbtype="btn_edit_item" data-item-id="<?php echo $value['item_id'];?>" href="javascript:;"><i class="icon-edit"></i>수정</a> <a cbtype="btn_del_item" data-item-id="<?php echo $value['item_id'];?>" href="javascript:;"><i class="icon-trash"></i>삭제</a></div>
          </td>
        </div>
        <?php } ?>
        <?php } ?>
      </div>
    </div>
    <div class="module-list">
      <?php if(!empty($output['module_list']) && is_array($output['module_list'])){ ?>
      <?php foreach($output['module_list'] as $key => $value){ ?>
      <div class="module_<?php echo $key;?>"> <span><?php echo $value['desc'];?></span> <a cbtype="btn_add_item" class="add" href="javascript:;" data-module-type="<?php echo $value['name'];?>">추가</a> </div>
      <?php } ?>
      <?php } ?>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/template.min.js" charset="utf-8"></script> 
<!-- 페이지模块模板 --> 
<script id="item_template" type="text/html">
</script> 
<script type="text/javascript">
    var special_id = <?php echo $output['special_id'];?>;
    var url_item_add = "<?php echo urlAdmin('mb_special', 'special_item_add');?>";
    var url_item_del = "<?php echo urlAdmin('mb_special', 'special_item_del');?>";
    var url_item_edit = "<?php echo urlAdmin('mb_special', 'special_item_edit');?>";
    $(document).ready(function(){
        //추가模块
        $('[cbtype="btn_add_item"]').on('click', function() {
            var data = {
                special_id: special_id,
                item_type: $(this).attr('data-module-type')
            };
            $.post(url_item_add, data, function(data) {
                if(typeof data.error === 'undefined') {
                    location.reload();
                } else {
                    showError(data.error);
                }
            }, "json");
        });

        //삭제模块
        $('#item_list').on('click', '[cbtype="btn_del_item"]', function() {
            if(!confirm('정말 삭제하시겠습니까?')) {
                return false;
            }
            var $this = $(this);
            var item_id = $this.attr('data-item-id');
            $.post(url_item_del, {item_id: item_id, special_id: special_id} , function(data) {
                if(typeof data.error === 'undefined') {
                    $this.parents('.special-item').remove();
                } else {
                    showError(data.error);
                }
            }, "json");
        });

        //수정模块
        $('#item_list').on('click', '[cbtype="btn_edit_item"]', function() {
            var item_id = $(this).attr('data-item-id');
            go(url_item_edit + '&item_id=' + item_id);
        });

        //上移
        $('#item_list').on('click', '[cbtype="btn_move_up"]', function() {
            var $current = $(this).parents('[cbtype="special_item"]');
            $prev = $current.prev('[cbtype="special_item"]');
            if($prev.length > 0) {
                $prev.before($current);
                update_item_sort();
            } else {
                showError('제일 첫번째입니다.');
            }
        });

        //下移
        $('#item_list').on('click', '[cbtype="btn_move_down"]', function() {
            var $current = $(this).parents('[cbtype="special_item"]');
            $next = $current.next('[cbtype="special_item"]');
            if($next.length > 0) {
                $next.after($current);
                update_item_sort();
            } else {
                showError('제일 마지막입니다.');
            }
        });

        var update_item_sort = function() {
            var item_id_string = '';
            $item_list = $('#item_list').find('[cbtype="special_item"]');
            $item_list.each(function(index, item) {
                item_id_string += $(item).attr('data-item-id') + ',';
            });
            $.post("index.php?act=mb_special&op=update_item_sort", {special_id: special_id, item_id_string: item_id_string}, function(data) {
                if(typeof data.error != 'undefined') {
                    showError(data.message);
                }
            }, 'json');
        };

        //시동/금지控制
        $('#item_list').on('click', '[cbtype="btn_usable"]', function() {
            var $current = $(this).parents('[cbtype="special_item"]');
            var item_id = $current.attr('data-item-id');
            var usable = '';
            if($current.hasClass('usable')) {
                $current.removeClass('usable');
                $current.addClass('unusable');
                usable = 'unusable';
                $(this).html('<i class="icon-off"></i>시동');
            } else {
                $current.removeClass('unusable');
                $current.addClass('usable');
                usable = 'usable';
                $(this).html('<i class="icon-off"></i>금지');
            }

            $.post("index.php?act=mb_special&op=update_item_usable", {item_id: item_id, usable: usable, special_id: special_id}, function(data) {
                if(typeof data.error != 'undefined') {
                    showError(data.message);
                }
            }, 'json');
        });

    });
</script> 
