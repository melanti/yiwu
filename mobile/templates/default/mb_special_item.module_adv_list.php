<?php defined('InCNBIZ') or exit('Access Invalid!'); ?>

<div class="adv_list">
    <div class="swipe-wrap">
    <?php foreach ((array) $vv['item'] as $item) { ?>
        <div class="item" cbtype="item_image">
            <a cbtype="btn_item" href="javascript:;" data-type="<?php echo $item['type']; ?>" data-data="<?php echo $item['data']; ?>">
                <img cbtype="image" src="<?php echo $item['image']; ?>" alt="">
            </a>
        </div>
    <?php } ?>
    </div>
</div>
