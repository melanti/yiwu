<?php
/**
 * 我的代金券
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class member_refundControl extends mobileMemberControl {

	public function __construct() {
		parent::__construct();
	}

    /**
     * 地址列表
     */
    public function get_refund_listOp() {
        $model_refund = Model('refund_return');
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['refund_type'] = 1;

        $refund_list = $model_refund->getRefundList($condition,10);

        $page_count = $model_refund->gettotalpage($condition);

        output_data(array('refund_list' => $refund_list), mobile_page($page_count));
    }
}
