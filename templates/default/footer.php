<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php echo getChat($layout);?>
<div style="border-bottom:3px solid #ff4d3f; width:1200px; margin:0 auto;border-top:3px solid #ff4d3f; margin-top:25px; ">
<div class="mod_agree"> 
      <ul class="mod_agree_con">  

      <li class="mod_agree_item mod_agree_item1">
        <a href="###"><img src="/templates/default/images/icons/1.png"></a><strong>真品货源</strong><span>正品，优质，放心</span>
        </li>
            
            <li class="mod_agree_item mod_agree_item2">
          <a href="###"><img src="/templates/default/images/icons/2.png"></a>         <strong>数据互通</strong><span>网站，商家，消费者</span>
        </li>
              <li class="mod_agree_item mod_agree_item3">
          <a href="###"><img src="/templates/default/images/icons/3.png"></a>         <strong>统一价格</strong><span>拒绝被宰，安心购物</span>
        </li>
              <li class="mod_agree_item mod_agree_item4">
          <a href="###"><img src="/templates/default/images/icons/4.png"></a>         <strong>担保交易</strong><span>支付宝担保，交易更安全</span>
        </li>
              <li class="mod_agree_item mod_agree_item5">
          <a href="###"><img src="/templates/default/images/icons/5.png"></a>         <strong>闪电发货</strong><span>商家审核严格，当天内出库</span>
        </li>
              <li class="mod_agree_item mod_agree_item6">
          <a href="###"><img src="/templates/default/images/icons/6.png"></a>         <strong>惊喜购</strong><span>帮你发现身边的好货</span>
        </li>
            </ul>
    </div>
</div>

<div id="footer">
  <div class="footer-container">
&copy; 2016 <?php echo C('site_name');?>. All Rights Reserved.
 </div>
<?php if (C('debug') == 1){?>
<div id="think_page_trace" class="trace">
  <fieldset id="querybox">
    <legend><?php echo $lang['nc_debug_trace_title'];?></legend>
    <div> <?php print_r(Tpl::showTrace());?> </div>
  </fieldset>
</div>
<?php }?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.cookie.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/qtip/jquery.qtip.min.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/compare.js"></script>
<script type="text/javascript">
$(function(){
	// Membership card
	$('[cbtype="mcard"]').membershipCard({type:'shop'});
});
function fade() {
  $("img[rel='lazy']").each(function () {
    var $scroTop = $(this).offset();
    if ($scroTop.top <= $(window).scrollTop() + $(window).height()) {
      $(this).hide();
      $(this).attr("src", $(this).attr("data-url"));
      $(this).removeAttr("rel");
      $(this).removeAttr("name");
      $(this).fadeIn(500);
    }
  });
}
if($("img[rel='lazy']").length > 0) {
  $(window).scroll(function () {
    fade();
  });
};
fade();
</script>