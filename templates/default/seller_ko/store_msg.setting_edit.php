<div class="eject_con">
	<div id="warning" class="alert alert-error"></div>
	<form method="post" target="_parent"
		action="<?php echo urlShop('store_msg', 'save_msg_setting');?>"
		id="store_msg_setting">
		<input type="hidden" name="form_submit" value="ok" /> <input
			type="hidden" name="code"
			value="<?php echo $output['smt_info']['smt_code']; ?>" />
    <?php if ($output['smt_info']['smt_message_switch']) {?>
    <dl>
			<dt>
				<input type="checkbox" class="checkbox mr5" name="message_forced"
					value="1" <?php if ($output['smt_info']['smt_message_forced']) {?>
					disabled="disabled" checked="checked" <?php }?>
					<?php if ($output['smsetting_info']['sms_message_switch']) {?>
					checked="checked" <?php }?> /> <strong>판매자 메시지 알림<?php echo $lang['nc_colon'];?></strong>
			</dt>
			<dd>시스템 내에서 자동으로 판매자에게 메시지를 발송 합니다.</dd>
		</dl>
    <?php }?>
    <?php if ($output['smt_info']['smt_short_switch']) {?>
    <dl>
			<dt>
				<input type="checkbox" class="checkbox mr5" name="short_forced" id="short_forced"
					value="1" <?php if ($output['smt_info']['smt_short_forced']) {?>
					disabled="disabled" checked="checked" <?php }?>
					<?php if ($output['smsetting_info']['sms_short_switch']) {?>
					checked="checked" <?php }?> /> <strong>핸드폰 메시지 알림<?php echo $lang['nc_colon'];?></strong>
			</dt>
			<dd>
				<input type="text" class="text w250" name="short_number" id="short_number"
					value="<?php echo $output['smsetting_info']['sms_short_number']?>" />
				<p class="hint">메시지 알림 선택시 수신할 핸드폰 번호를 정확하게 입력해 주세요.</p>
			</dd>
		</dl>
    <?php }?>
    <?php if ($output['smt_info']['smt_mail_switch']) {?>
    <dl>
			<dt>
				<input type="checkbox" class="checkbox mr5" name="mail_forced" id="mail_forced"
					value="1" <?php if ($output['smt_info']['smt_mail_forced']) {?>
					disabled="disabled" checked="checked" <?php }?>
					<?php if ($output['smsetting_info']['sms_mail_switch']) {?>
					checked="checked" <?php }?> /> <strong>메일알림<?php echo $lang['nc_colon'];?></strong>
			</dt>
			<dd>
				<input type="text" class="text w250" name="mail_number" id="mail_number"
					value="<?php echo $output['smsetting_info']['sms_mail_number']?>" />
				<p class="hint">메일 알림 선택시 수신할 메일주소를 정확하게 입력해 주세요.</p>
			</dd>
		</dl>
    <?php }?>
    <div class="bottom">
			<label class="submit-border"> <input type="submit" class="submit"
				value="완료" />
			</label>
		</div>
	</form>
</div>
<script>
$(function(){
    $('#store_msg_setting').validate({
        errorLabelContainer: $('#warning'),
        submitHandler:function(form){
            ajaxpost('store_msg_setting', '', '', 'onerror');
        },
        rules : {
            short_number : {
                required : '#short_forced:checked',
                digits : true,
                rangelength : [11,11]
            },
            mail_number : {
                required : '#mail_forced:checked',
                email : true
            }
        },
        messages : {
            short_number : {
                required : '<i class="icon-exclamation-sign"></i>정확한 핸드폰 번호를 입력해 주세요',
                digits : '<i class="icon-exclamation-sign"></i>정확한 핸드폰 번호를 입력해 주세요',
                rangelength : '<i class="icon-exclamation-sign"></i>정확한 핸드폰 번호를 입력해 주세요'
            },
            mail_number : {
                required : '<i class="icon-exclamation-sign"></i>정확한 메일주소를 입력해 주세요',
                email : '<i class="icon-exclamation-sign"></i>정확한 메일주소를 입력해 주세요'
            }
        }
    });
});
</script>