/**
 * Autoinsert script tags (or other filebased tags) in an html file.
 *
 * ---------------------------------------------------------------
 *
 * Automatically inject <script> tags for javascript files and <link> tags
 * for css files.  Also automatically links an output file containing precompiled
 * templates using a <script> tag.
 *
 * For usage docs see:
 * 		https://github.com/Zolmeister/grunt-sails-linker
 *
 */

var tpmVersions = require('../../package.json');

module.exports = function(grunt) {

  grunt.config.set('sails-linker', {
    cleanCss: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    cleanJsBase: {
      options: {
        startTag: '<!--SCRIPTS_BASE-->',
        endTag: '<!--SCRIPTS_BASE END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    cleanJs: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    cleanJsAll: {
      options: {
        startTag: '<!--SCRIPTS_ALL-->',
        endTag: '<!--SCRIPTS_ALL END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    devJsBase: {
      options: {
        startTag: '<!--SCRIPTS_BASE-->',
        endTag: '<!--SCRIPTS_BASE END-->',
        fileTmpl: '<script src="%s?_='+(tpmVersions.version)+'"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsFilesToInjectBase,
        'views/**/*.html': require('../pipeline').jsFilesToInjectBase,
        'views/**/*.ejs': require('../pipeline').jsFilesToInjectBase
      }
    },

    devJsRelativeBase: {
      options: {
        startTag: '<!--SCRIPTS_BASE-->',
        endTag: '<!--SCRIPTS_BASE END-->',
        fileTmpl: '<script src="%s?_='+(tpmVersions.version)+'"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsFilesToInjectBase,
        'views/**/*.html': require('../pipeline').jsFilesToInjectBase,
        'views/**/*.ejs': require('../pipeline').jsFilesToInjectBase
      }
    },

    devJs: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '<script src="%s?_='+(tpmVersions.version)+'"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsFilesToInject
      }
    },

    devJsRelative: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '<script src="%s?_='+(tpmVersions.version)+'"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsFilesToInject
      }
    },

    devJsAll: {
      options: {
        startTag: '<!--SCRIPTS_ALL-->',
        endTag: '<!--SCRIPTS_ALL END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    devJsRelativeAll: {
      options: {
        startTag: '<!--SCRIPTS_ALL-->',
        endTag: '<!--SCRIPTS_ALL END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    prodJsBase: {
      options: {
        startTag: '<!--SCRIPTS_BASE-->',
        endTag: '<!--SCRIPTS_BASE END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    prodJsRelativeBase: {
      options: {
        startTag: '<!--SCRIPTS_BASE-->',
        endTag: '<!--SCRIPTS_BASE END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    prodJs: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    prodJsRelative: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '%s',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': ['<!--hello-->'],
        'views/**/*.html': ['<!--hello-->'],
        'views/**/*.ejs': ['<!--hello-->']
      }
    },

    prodJsAll: {
      options: {
        startTag: '<!--SCRIPTS_ALL-->',
        endTag: '<!--SCRIPTS_ALL END-->',
        fileTmpl: '<script src="%s?_='+(tpmVersions.version)+'"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.js']
      }
    },

    prodJsRelativeAll: {
      options: {
        startTag: '<!--SCRIPTS_ALL-->',
        endTag: '<!--SCRIPTS_ALL END-->',
        fileTmpl: '<script src="%s?_='+(tpmVersions.version)+'"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.js']
      }
    },

    devStyles: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s?_='+(tpmVersions.version)+'">',
        appRoot: '.tmp/public'
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssFilesToInject
      }
    },

    devStylesRelative: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s?_='+(tpmVersions.version)+'">',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssFilesToInject
      }
    },

    prodStyles: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s?_='+(tpmVersions.version)+'">',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.css']
      }
    },

    prodStylesRelative: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s?_='+(tpmVersions.version)+'">',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.css']
      }
    },

    // Bring in JST template object
    devTpl: {
      options: {
        startTag: '<!--TEMPLATES-->',
        endTag: '<!--TEMPLATES END-->',
        fileTmpl: '<script type="text/javascript" src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/jst.js'],
        'views/**/*.html': ['.tmp/public/jst.js'],
        'views/**/*.ejs': ['.tmp/public/jst.js']
      }
    },

    devJsJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsFilesToInject
      }
    },

    devJsRelativeJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsFilesToInject
      }
    },

    prodJsJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.js']
      }
    },

    prodJsRelativeJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.js']
      }
    },

    devStylesJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssFilesToInject
      }
    },

    devStylesRelativeJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssFilesToInject
      }
    },

    prodStylesJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.css']
      }
    },

    prodStylesRelativeJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.css']
      }
    },

    // Bring in JST template object
    devTplJade: {
      options: {
        startTag: '// TEMPLATES',
        endTag: '// TEMPLATES END',
        fileTmpl: 'script(type="text/javascript", src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/jst.js']
      }
    }
  });

  grunt.loadNpmTasks('grunt-sails-linker');
};
