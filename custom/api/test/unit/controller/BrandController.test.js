var request = require('supertest');
var should = require('should');
var _ = require('underscore');

var tmpVersions,
  lib_net,
  BrandController,
  RequestService,
  CommonService;

var testListReqModel = {
  account_uuid: '',
  start: '',
  count: '',
  query_opt: '',
  status: '',
  order: '',
  show: 'debug'
};

if (process.env['MODE'] == 'ALL_DEBUG') {
  // Operation Test
  startTest();
}
// terminal -> NODE_ENV=local MODE=debug LOG_LEVEL=$1 mocha test/bootstrap.test.js
startTest();

function startTest() {
  describe('=== D. Brand list 테스트 케이스 ===\n', function () {
    testD0();
    testD1();
  });
}

function testD0() {
  describe('=== D.0 모듈 설정 === \n', function () {
    it('D.0.1 assignModule', function (done) {
      assignBasicModule();
      assignBrandCtrlModule();
      done();
    });
  });
}

function testD1() {
  describe('BrandController', function () {
    describe('list', function () {
      it('1. should return success - default', function (done) {
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_BRAND_LIST + (CommonService.getQueryPath(testListReqModel, '?')),
          method: 'get',
          expectStatus: 200,
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "D.1.1 brand list success",
          assertCallback: function (res) {
            //sails.log.debug(res)
            res.should.property("debug");
            res.should.property("info");
            res.should.property("results");
              res.results.should.be.instanceOf(Array);
          },
          done: done
        });
      });
      it('2. should return success - count 2', function (done) {
        var _temp = _.extend({}, testListReqModel);
        _temp.count = 2;
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_BRAND_LIST + (CommonService.getQueryPath(_temp, '?')),
          method: 'get',
          expectStatus: 200,
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "D.1.2 brand list success",
          assertCallback: function (res) {
            //sails.log.debug(res)
            res.should.property("debug");
            res.should.property("info");
            res.should.property("results");
            res.results.should.be.instanceOf(Array).and.have.lengthOf(_temp.count);
          },
          done: done
        });
      });
      it('3. should return success - status : activated ', function (done) {
        var _temp = _.extend({}, testListReqModel);
        _temp.status = 'activated';
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_BRAND_LIST + (CommonService.getQueryPath(_temp, '?')),
          method: 'get',
          expectStatus: 200,
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "D.1.3 brand list status : activated",
          assertCallback: function (res) {
            //sails.log.debug(res)
            res.should.property("debug");
            res.should.property("info");
            res.should.property("results");
            for(var i in res.results){
              res.results[i].should.property("status", _temp.status);
            }
          },
          done: done
        });
      });
      it('4. should return success - status : disabled ', function (done) {
        var _temp = _.extend({}, testListReqModel);
        _temp.status = 'disabled';
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_BRAND_LIST + (CommonService.getQueryPath(_temp, '?')),
          method: 'get',
          expectStatus: 200,
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "D.1.4 brand list status : disabled",
          assertCallback: function (res) {
            //sails.log.debug(res)
            res.should.property("debug");
            res.should.property("info");
            res.should.property("results");
            for(var i in res.results){
              res.results[i].should.property("status", _temp.status);
            }
          },
          done: done
        });
      });
      it('5. should return success - order : title ', function (done) {
        var _temp = _.extend({}, testListReqModel);
        _temp.order = 'title';
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_BRAND_LIST + (CommonService.getQueryPath(_temp, '?')),
          method: 'get',
          expectStatus: 200,
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "D.1.5 brand list order : title",
          assertCallback: function (res) {
            //sails.log.debug(res)
            res.should.property("debug");
            res.should.property("info");
            res.should.property("results");
          },
          done: done
        });
      });
      it('6. should return success - call by App ', function (done) {
        var _temp = _.extend({}, testListReqModel);
        _temp.start = 0;
        _temp.count = 20;
        _temp.status = 'activated';
        _temp.order = 'title';
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_BRAND_LIST + (CommonService.getQueryPath(_temp, '?')),
          method: 'get',
          expectStatus: 200,
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "D.1.5 brand list call by App",
          assertCallback: function (res) {
            //sails.log.debug(res)
            res.should.property("debug");
            res.should.property("info");
            res.should.property("results");
          },
          done: done
        });
      });

    });
  });
}


function assignBasicModule() {
  tmpVersions = require('../../../package.json');
  lib_net = require('../../../api/controllers/libs/network.js');
  RequestService = require('../../util/RequestService.js');
  CommonService = require('../../util/CommonService.js');
}

function assignBrandCtrlModule() {
  BrandController = require('../../../api/controllers/BrandController.js');
}
