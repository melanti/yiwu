<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert alert-info alert-block">
  <div class="faq-img"></div>
  <h4>TIP：</h4>
  <ul>
    <li>1. 같은 상품을 중복 선택하지 마십시오.</li>
    <li>2. 특수상품(ex E-쿠폰 상품, F번호 상품, 예약판매상품)은 증점품이 될 수 없습니다.</li>
    <li>3. 사은품은 정상 판매중의 상품이 가능하며, 판매량에 기록됩니다.</li>
    <li>4. 재고에 사은품이 부족할 때에는 주문 과정에 사은품으로서 들어갈 수 없습니다.</li>
  </ul>
</div>
<form method="post" id="goods_gift" action="<?php echo urlShop('store_goods_online', 'save_gift');?>">
  <input type="hidden" name="form_submit" value="ok">
  <input type="hidden" name="ref_url" value="<?php echo $_GET['ref_url'];?>" />
  <input type="hidden" name="commonid" value="<?php echo intval($_GET['commonid']);?>" />
  <?php if (!empty($output['goods_array'])) {?>
  <?php foreach ($output['goods_array'] as $value) {?>
  <div class="ncsc-form-goods-gift" data-gid="<?php echo $value['goods_id'];?>">
    <div class="goods-pic"> <span><img src="<?php echo thumb($value, 240);?>"/></span></div>
    <div class="goods-summary">
      <h2><?php echo $value['goods_name_ko'];?><em>SKU：<?php echo $value['goods_id'];?></em></h2>
      <dl>
        <dt>상품가격：</dt>
        <dd><?php echo number_format($value['goods_price_ko']);?>원</dd>
      </dl>
      <dl>
        <dt>재&nbsp;&nbsp;고&nbsp;&nbsp;량：</dt>
        <dd><?php echo $value['goods_storage'];?></dd>
      </dl>      
      <dl>
        <dt>사은품<br>리스트：</dt>
        <dd>
          <ul class="goods-gift-list" cnbiztype="choose_goods_list">
            <?php if (!empty($output['gift_array'][$value['goods_id']])) {?>
            <?php foreach ($output['gift_array'][$value['goods_id']] as $gift) {?>
            <li>
              <div class="pic-thumb"><span><img src="<?php echo cthumb($gift['gift_goodsimage'], '60', $_SESSION['store_id']);?>"></span></div>
              <dl class="goods_name">
                <dt><?php echo $gift['gift_goodsname_ko'];?></dt>
                <dd>사은품수량：
                  <input class="text" type="text" value="<?php echo $gift['gift_amount'];?>" name="gift[<?php echo $value['goods_id'];?>][<?php echo $gift['gift_goodsid'];?>]">
                </dd>
              </dl>
              <a class="gift-del" cnbiztype="del_choosed" href="javascript:void(0);" title="사은품삭제">X</a></li>
            <?php }?>
            <?php }?>
          </ul>
          <a class="ncsc-btn-mini" cnbiztype="select_goods" href="javascript:void(0);"><i class="icon-gift"></i>사은품선택</a></dd>
      </dl>
    </div>
    <div class="div-goods-select" style="display: none;">
      <table class="search-form">
        <thead>
          <tr>
            <td></td>
            <th>상품명</th>
            <td class="w160"><input class="text" type="text" name="search_gift"></td>
            <td class="tc w70"><a class="ncsc-btn" href="javascript:void(0);" cnbiztype="search_gift"><i class="icon-search"></i>검색</a></td>
            <td class="w10"></td>
          </tr>
        </thead>
      </table>
      <div class="search-result" cnbiztype="gift_goods_list"></div>
      <a class="close" href="javascript:void(0);" cnbiztype="btn_hide_goods_select">X</a> </div>
  </div>
  <?php }?>
  </div>
  <?php }?>
  <div class="bottom tc">
    <label class="submit-border">
      <input type="submit" class="submit" value="완료" />
    </label>
  </div>
</form>
<script type="text/javascript">
$(function(){
	//凸显鼠标触及区域、其余区域半透明显示
	$("#goods_gift > div").jfade({
        start_opacity:"1",
        high_opacity:"1",
        low_opacity:".25",
        timing:"200"
    });
    // 选择赠品按钮
    $('a[cnbiztype="select_goods"]').click(function(){
        $(this).parents('.goods-summary:first').nextAll('.div-goods-select').show()
            .find('input[name="search_gift"]').val('').end()
            .find('a[cnbiztype="search_gift"]').click();
    });

    // 关闭按钮
    $('a[cnbiztype="btn_hide_goods_select"]').click(function(){
        $(this).parent().hide();
    });

    // 所搜商品
    $('a[cnbiztype="search_gift"]').click(function(){
        _url = "<?php echo urlShop('store_goods_online', 'search_goods');?>";
        _name = $(this).parents('tr').find('input[name="search_gift"]').val();
        $(this).parents('table:first').next().load(_url, {name: _name});
    });

    // 分页
    $('div[cnbiztype="gift_goods_list"]').on('click', 'a[class="demo"]', function(){
        $(this).parents('div[cnbiztype="gift_goods_list"]').load($(this).attr('href'));
        return false;
    });

    // 삭제
    $('ul[cnbiztype="choose_goods_list"]').on('click', 'a[cnbiztype="del_choosed"]', function(){
        $(this).parents('li:first').remove();
    });

    // 选择商品
    $('div[cnbiztype="gift_goods_list"]').on('click', 'a[cnbiztype="a_choose_goods"]', function(){
        _owner_gid = $(this).parents('.ncsc-form-goods-gift:first').attr('data-gid');
        eval('var data_str = ' + $(this).attr('data-param'));
        _li = $('<li></li>')
            .append('<div class="pic-thumb"><span><img src="' + data_str.gimage + '"></span></div>')
            .append('<dl class="goods_name"><dt>' + data_str.gname + '</dt><dd>사은품수량：<input class="text" type="text" value="1" name="gift[' + _owner_gid + '][' + data_str.gid + ']"></dd></dl>')
            .append('<a class="gift-del" cnbiztype="del_choosed" href="javascript:void(0);" title="사은품삭제">X</a>');
        $(this).parents('.div-goods-select:first').prev().find('ul[cnbiztype="choose_goods_list"]').append(_li);
    });

    $('#goods_gift').submit(function(){
        ajaxpost('goods_gift', '', '', 'onerror');
    });
});
</script> 
