<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['payment_index_name']				= '결제방식';
$lang['payment_index_enable_ing']	 	= '시동중';
$lang['payment_index_disable_ing']		= '닫기';
$lang['payment_index_enable']	 		= '시동';
$lang['payment_index_disable']			= '금지';
$lang['payment_index_ensure_disable']	= '정말 금지하시겠습니까?';
$lang['payment_help1']					= '시스텀 결제 방식입니다, 수정을 눌러 해당 결제 방식을 설정할 수 있습니다.';
/**
 * 설정
 */
$lang['payment_info']	 		= '계정정보';
$lang['payment_notice']	 		= '은행 계좌 입력 주의사항';
$lang['payment_chinabank_account']	 		= '은련 아이디';
$lang['payment_chinabank_key']	 		= '은련 키';
$lang['payment_tenpay_account']	 		= '텐페이 아이디';
$lang['payment_tenpay_key']	 		= '텐페이 키';
$lang['payment_alipay_account']	 		= '아리페이 아이디';
$lang['payment_alipay_key']	 		= '아리페이 키';
$lang['payment_alipay_partner']	 		= '파트너 아이디';
$lang['payment_edit_not_null']		= '반드시 입력하셔야됩니다.';