(function($, angular, _, APP){
  'use strict';

  if( $(document.body).attr('data-page-id')=='login' ){
    bootstrapLogin();
  }

  function bootstrapLogin(){
    console.log('[[bootstrapLogin:]]');

    angular.module('App')
      .directive( 'login', loginDirective );

    angular.bootstrap( $('html'), ['App']);
  }

  function loginDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.ctrl.loginCtrl,
      controllerAs: 'loginCtrl',
      bindToController: {
      },
      link: APP.modules.ctrl.loginCtrl.link(),
      templateUrl: window.location.protocol+'//'+window.location.host+'/modules/app/login/templates/login.html?_='+APP.info.gVersion
    };
  }
  loginDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];


})(window.jQuery, window.angular, window._, window.APP);
