<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 업체관리 语言包
 */

$lang['sel_del_store']     = '삭제할 내용을 선택하세요';
$lang['open']              = 'OPEN';
$lang['close']             = 'CLOSE';
$lang['no_limit']          = '무제한';
$lang['user_name_no_null'] = '아이디를 입력하세요';
$lang['pwd_no_null']       = '비밀번호를 입력하세요';
$lang['user_open_store']   = '본 회원은 이미 미니샵를 개통하였습니다.';
$lang['user_no_exist']     = '아이디가 존재하지 않습니다.';
$lang['pwd_fail']          = '비밀번호가 정확하지 않습니다';
$lang['please_input_store_user_name_p'] = '점장 이름을 입력하세요';
$lang['please_input_store_name_p']      = '미니샵 이름을 입력하세요';
$lang['please_input_store_level']       = '미니샵 등급을 입력하세요';
$lang['back_store_list']       = '미니샵 리스트 돌아가기';
$lang['countinue_add_store']   = '계속 새 미니샵 추가';
$lang['store_name_exists']		= '본 미니샵이름은 이미 존재합니다, 다른 이름을 사용해주세요';
$lang['store_no_exist']        = '본 미니샵는 존재하지 않습니다.';
$lang['store_no_meet']         = '요구에 적합하지 않는 미니샵입니다.';
$lang['please_sel_store']  = '설정할 미니샵를 선택하세요!';
$lang['address_no_null']       = '지역을 입력하세요';
$lang['please_sel_edit_store'] = '수정할 미니샵를 선택하세요';
$lang['please_sel_edit_store'] = '수정할 내용을 입력하세요!';

$lang['store']           = '미니샵';
$lang['store_help1']     = '미니샵 유효기간이 만료되었거나 미니샵이 CLOSE 상태 일 때에는, 씨엔비즈 웹사이트에서 해당 미니샵을 열람할 수 없으나, 판매자의 미니샵 편집은 가능 합니다.';
$lang['store_help2']     = '추천 미니샵가 되면 프론트단 미니샵 추천 위치에 노출됩니다, 닫힌 미니샵는 추천할 수 없습니다.';
$lang['store_audit_help1']= '미니샵 신청은 일괄 심사할 수 있습니다.';
$lang['store_grade_help1']=	'관리자 신청 업데이트 삭제후 점장은 다시 미니샵 업데이트를 할 수 있습니다.';
$lang['store_grade_help2']=	'신청 심사는 수정을 눌러주세요';
$lang['manage']          = '관리';
$lang['pending']         = '미니샵신청';
$lang['grade_apply']	 	 = '업데이트 신청';
$lang['store_user_name'] = '관리자 아이디';
$lang['store_user_id']   = '신분증번호';
$lang['store_name']      = '미니샵';
$lang['store_index']     = '미니샵메인';
$lang['belongs_class']   = '소속 카테고리';
$lang['location']        = '소재지';
$lang['details_address'] = '상세주소';
$lang['zip']             = '우편번호';
$lang['tel_phone']       = '연락전화';
$lang['belongs_level']   = '소속등급';
$lang['period_to']       = '유효기간';
$lang['formart']         = '양식: 2009-4-30, 공백시 무제한';
$lang['state']           = '상태';
$lang['open']            = 'OPEN';
$lang['close']           = 'CLOSE';
$lang['close_reason']    = '닫은 원인';
$lang['certification']   = '인증';
$lang['owner_certification']           = '실명인증';
$lang['owner_certification_del']	   = '정말 실명인증 파일을 삭제하시겠습니까?';
$lang['entity_store_certification']    = '오프라인 매점 인증';
$lang['entity_store_certification_del']= '정말 미니샵 인증 파일을 삭제하시겠습니까?';
$lang['certification_del_success']	   = '인증 파일 삭제 성공';
$lang['certification_del_fail']	       = '인증 파일 삭제 실패';
$lang['recommended']                   = '추천';
$lang['recommended_tips']              = '미니샵 단힌 상태에서 추천할 수 없습니다.';
$lang['reset']                         = '초기화';
$lang['please_input_store_name']       = '미니샵이름을 입력하세요';
$lang['please_input_address']       = '지역을 선택하세';
$lang['view_owner_certification_file'] = '실명 인증 파일 보기';
$lang['view_entity_store_certification_file'] = '오프라인 매장 인증 파일 보기';
$lang['store_user']               = '점장';
$lang['operation']                = '설정';
$lang['editable']                 = '수정가능';
$lang['are_you_sure_del_store']   = '정말 미니샵의 모든 정보(상품,주문등)를 삭제하시겠습니까?';
$lang['no_edit_please_no_choose'] = '비수정시 선택하지마세요';
$lang['unchanged']                = '유지';
$lang['to']                       = '로 수정';
$lang['no_edit_please_null']      = '비수정시 공백으로 설정하세요';
$lang['authing']                  = '인증중';
$lang['enter_shop_owner_info']    = '오픈할 회원 정보를 입력하세요';
$lang['user_name']                = '아이디';
$lang['pwd']                      = '비밀번호';
$lang['need_verify_pwd']           = '비밀번호 인증 필요';

//2차도메인
$lang['if_open_domain']          = '2차 도메인 OPEN 여부';
$lang['open_domain_document']    = '2차 도메인 OPEN시 다중 도메인 해석이 필요합니다.';
$lang['suffix']                  = '2차 도메인 확장';
$lang['demo']                    = '만약 2차 도메인이 "test.jk.com"일때 "jk.com"은 입력안하셔도 됩니다.';
$lang['reservations_domain']     = '보류 도메인';
$lang['please_input_domain']     = '2차 도메인 보류, 많을시 ","로 구분해주세요';
$lang['length_limit']            = '길이제한';
$lang['domain_length']           = '예: "3-12"자로 설정하시면 됩니다.';
$lang['domain_edit']             = '수정 가능 여부';
$lang['domain_times']            = '수정횟수';
$lang['domain_edit_tips']        = '수정 불가시 점장은 수정후 다시 수정할 수 없습니다.';
$lang['domain_times_tips']       = '수정 가능시 일정한 수정횟수를 초과하시면 다시 수정할 수 없습니다.';
$lang['domain_times_null']       = '수정횟수를 입력하세요';
$lang['domain_times_digits']     = '수정횟수는 반드시 숫자로 입력하세요';
$lang['domain_times_min']        = '수정 횟수 최도는 1입니다.';
$lang['domain_length_tips']      = '길이 제한 형식이 옳바르지 않습니다, 오른쪽 설명을 참고하세요';
$lang['domain_suffix_null']      = '2차 도메인 확장을 입력하세요';
$lang['store_domain'] = '2차 도메인';
$lang['store_domain_times']      = '이미 수정한 횟수';
$lang['store_domain_valid']      = '문자열,숫자,언더마,중앙선만 가능합니다.';
$lang['store_domain_rangelength']   = '2차 도메인 길이는 {0}부터 {1}사이로 입력하세요';
$lang['store_domain_times_digits']  = '이미 수정 횟수는 반드시 숫자로 입력하세요';
$lang['store_domain_times_max']  = '이미 수정 최대 횟수는 {0}입니다.';
$lang['store_domain_length_error']	= '2차도메인 길이가 옳바르지 않습니다.';
$lang['store_domain_exists']	= '본 2차 도메인은 이미 존재합니다, 다른 도메인을 입력하세요';
$lang['store_domain_sys']	= '본 2차 도메인은 금지된 도메인입니다, 다른 도메인을 입력하세요';
$lang['store_domain_invalid']		= '본 2차 도메인은 요구에 적합하지 않거나 불합적인 문자열이 추가되었습니다.';
$lang['store_save_defaultalbumclass_name']	= '기본앨범';