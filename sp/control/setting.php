<?php
/**
 * 网站설정
 *
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class settingControl extends SystemControl{
	private $links = array(
		array('url'=>'act=setting&op=base','lang'=>'web_set'),
		array('url'=>'act=setting&op=dump','lang'=>'dis_dump'),
	);
	public function __construct(){
		parent::__construct();
		Language::read('setting');
	}

	/**
	 * 오늘의 환율 불러오기
	 */
	public function todayCurOp(){
		$url = 'http://www.webservicex.net/CurrencyConvertor.asmx/ConversionRate?FromCurrency=CNY&ToCurrency=KRW&sessionid=&tmp='.random(4);
		import('function.ftp');
		$getCurrency = dfsockopen($url);
		preg_match('|<double xmlns="http://www.webserviceX.NET/">(.*?)<\/double>|i',$getCurrency,$todyCurrency);
		$j_arr = array('cur'=>$todyCurrency[1]);
		echo json_encode($j_arr);
	}


	/**
	 * 기본정보
	 */
	public function baseOp(){
		// 오늘의 환율 불러오기
		// $url = 'http://www.webservicex.net/CurrencyConvertor.asmx/ConversionRate?FromCurrency=CNY&ToCurrency=KRW&sessionid=&tmp='.random(4);
		// import('function.ftp');
		// $getCurrency = dfsockopen($url);
		// preg_match('|<double xmlns="http://www.webserviceX.NET/">(.*?)<\/double>|i',$getCurrency,$todyCurrency);

		$model_setting = Model('setting');
		if (chksubmit()){
			//上传网站Logo
			if (!empty($_FILES['site_logo']['name'])){
				$upload = new UploadFile();
				$upload->set('default_dir',ATTACH_COMMON);
				$result = $upload->upfile('site_logo');
				if ($result){
					$_POST['site_logo'] = $upload->file_name;
				}else {
					showMessage($upload->error,'','','error');
				}
			}
			if (!empty($_FILES['member_logo']['name'])){
				$upload = new UploadFile();
				$upload->set('default_dir',ATTACH_COMMON);
				$result = $upload->upfile('member_logo');
				if ($result){
					$_POST['member_logo'] = $upload->file_name;
				}else {
					showMessage($upload->error,'','','error');
				}
			}
			if (!empty($_FILES['seller_center_logo']['name'])){
				$upload = new UploadFile();
				$upload->set('default_dir',ATTACH_COMMON);
				$result = $upload->upfile('seller_center_logo');
				if ($result){
					$_POST['seller_center_logo'] = $upload->file_name;
				}else {
					showMessage($upload->error,'','','error');
				}
			}
			$list_setting = $model_setting->getListSetting();
			$update_array = array();
			$update_array['time_zone'] = $this->setTimeZone($_POST['time_zone']);
			$update_array['site_name'] = $_POST['site_name'];
			$update_array['site_phone'] = $_POST['site_phone'];
			$update_array['site_bank_account'] = $_POST['site_bank_account'];
			$update_array['site_email'] = $_POST['site_email'];
			$update_array['site_cur'] = $_POST['site_cur'];
			$update_array['statistics_code'] = $_POST['statistics_code'];
			if (!empty($_POST['site_logo'])){
				$update_array['site_logo'] = $_POST['site_logo'];
			}
			if (!empty($_POST['member_logo'])){
				$update_array['member_logo'] = $_POST['member_logo'];
			}
			if (!empty($_POST['seller_center_logo'])){
				$update_array['seller_center_logo'] = $_POST['seller_center_logo'];
			}
			$update_array['icp_number'] = $_POST['icp_number'];
			$update_array['site_status'] = $_POST['site_status'];
			$update_array['closed_reason'] = $_POST['closed_reason'];
			$result = $model_setting->updateSetting($update_array);
			if ($result === true){
				//判断有没有之前的이미지，如果有则삭제
				if (!empty($list_setting['site_logo']) && !empty($_POST['site_logo'])){
					@unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$list_setting['site_logo']);
				}
				if (!empty($list_setting['member_logo']) && !empty($_POST['member_logo'])){
					@unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$list_setting['member_logo']);
				}
				if (!empty($list_setting['seller_center_logo']) && !empty($_POST['seller_center_logo'])){
					@unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$list_setting['seller_center_logo']);
                }
				$this->log(L('nc_edit,web_set'),1);
				showMessage(L('nc_common_save_succ'));
			}else {
				$this->log(L('nc_edit,web_set'),0);
				showMessage(L('nc_common_save_fail'));
			}
		}
		$list_setting = $model_setting->getListSetting();
		foreach ($this->getTimeZone() as $k=>$v) {
			if ($v == $list_setting['time_zone']){
				$list_setting['time_zone'] = $k;break;
			}
		}
		Tpl::output('list_setting',$list_setting);

		//输出子菜单
		Tpl::output('top_link',$this->sublink($this->links,'base'));

		Tpl::showpage('setting.base');
	}

	/**
	 * 도배설정
	 */
	public function dumpOp(){
		$model_setting = Model('setting');
		if (chksubmit()){
			$update_array = array();
			$update_array['guest_comment'] = $_POST['guest_comment'];
			$update_array['captcha_status_login'] = $_POST['captcha_status_login'];
			$update_array['captcha_status_register'] = $_POST['captcha_status_register'];
			$update_array['captcha_status_goodsqa'] = $_POST['captcha_status_goodsqa'];
			$result = $model_setting->updateSetting($update_array);
			if ($result === true){
				$this->log(L('nc_edit,dis_dump'),1);
				showMessage(L('nc_common_save_succ'));
			}else {
				$this->log(L('nc_edit,dis_dump'),0);
				showMessage(L('nc_common_save_fail'));
			}
		}
		$list_setting = $model_setting->getListSetting();
		Tpl::output('list_setting',$list_setting);
		Tpl::output('top_link',$this->sublink($this->links,'dump'));
		Tpl::showpage('setting.dump');
	}

	/**
	 * SEO与rewrite설정
	 */
	public function seoOp(){
		$model_setting = Model('setting');
		if (chksubmit()){
			$update_array = array();
			$update_array['rewrite_enabled'] = $_POST['rewrite_enabled'];
			$result = $model_setting->updateSetting($update_array);
			if ($result === true){
				$this->log(L('nc_edit,nc_seo_set'),1);
				showMessage(L('nc_common_save_succ'));
			}else {
				$this->log(L('nc_edit,nc_seo_set'),0);
				showMessage(L('nc_common_save_fail'));
			}
		}
		$list_setting = $model_setting->getListSetting();

		//读取SEO信息
		$list = Model('seo')->select();
		$seo = array();
		foreach ((array)$list as $value) {
			$seo[$value['type']] = $value;
		}

		Tpl::output('list_setting',$list_setting);
		Tpl::output('seo',$seo);

		$category = Model('goods_class')->getGoodsClassForCacheModel();
		Tpl::output('category',$category);

		Tpl::showpage('setting.seo_setting');
	}

	public function ajax_categoryOp(){
		$model = Model('goods_class');
		$list = $model->field('gc_title,gc_keywords,gc_description')->find(intval($_GET['id']));
		//转码
		if (strtoupper(CHARSET) == 'GBK'){
			$list = Language::getUTF8($list);//网站GBK使用코드时,转换为UTF-8,防止json输出汉字问题
		}
		echo json_encode($list);exit();
	}

	/**
	 * SEO설정저장
	 */
	public function seo_updateOp(){
		$model_seo = Model('seo');
		if (chksubmit()){
			$update = array();
			if (is_array($_POST['SEO'][0])){
				$seo = $_POST['SEO'][0];
			}else{
				$seo = $_POST['SEO'];
			}
			foreach ((array)$seo as $key=>$value) {
				$model_seo->where(array('type'=>$key))->update($value);
			}
			dkcache('seo');
			showMessage(L('nc_common_save_succ'));
		}else{
			showMessage(L('nc_common_save_fail'));
		}
	}

	/**
	 * 카테고리SEO저장
	 *
	 */
	public function seo_categoryOp(){
		if (chksubmit()){
		    $where = array('gc_id' => intval($_POST['category']));
			$input = array();
			$input['gc_title'] = $_POST['cate_title'];
			$input['gc_keywords'] = $_POST['cate_keywords'];
			$input['gc_description'] = $_POST['cate_description'];
			if (Model('goods_class')->editGoodsClass($input, $where)){
				dkcache('goods_class_seo');
				showMessage(L('nc_common_save_succ'));
			}
		}
		showMessage(L('nc_common_save_fail'));
	}

    /**
     * 网站功能模块시동或者닫힘
     *
     */
	public function website_settingOp(){
		$model_setting = Model('setting');
		//저장信息
		if (chksubmit()){
			//构造업데이트数据数组
			$update_array = array();
			//站外分享功能
			$update_array['share_isuse'] = trim($_POST['share_isuse']);
			$result = $model_setting->updateSetting($update_array);
			if ($result === true){
				showMessage(Language::get('nc_common_save_succ'));
			}else {
				showMessage(Language::get('nc_common_save_fail'));
			}
		}
		//读取설정내용 $list_setting
		$list_setting = $model_setting->getListSetting();
		//模板输出
		Tpl::output('list_setting',$list_setting);
		Tpl::showpage('setting.website_setting');
	}

	/**
	 * 설정时区
	 *
	 * @param int $time_zone 时区键值
	 */
	private function setTimeZone($time_zone){
		$zonelist = $this->getTimeZone();
		return empty($zonelist[$time_zone]) ? 'Asia/Shanghai' : $zonelist[$time_zone];
	}

	private function getTimeZone(){
		return array(
		'-12' => 'Pacific/Kwajalein',
		'-11' => 'Pacific/Samoa',
		'-10' => 'US/Hawaii',
		'-9' => 'US/Alaska',
		'-8' => 'America/Tijuana',
		'-7' => 'US/Arizona',
		'-6' => 'America/Mexico_City',
		'-5' => 'America/Bogota',
		'-4' => 'America/Caracas',
		'-3.5' => 'Canada/Newfoundland',
		'-3' => 'America/Buenos_Aires',
		'-2' => 'Atlantic/St_Helena',
		'-1' => 'Atlantic/Azores',
		'0' => 'Europe/Dublin',
		'1' => 'Europe/Amsterdam',
		'2' => 'Africa/Cairo',
		'3' => 'Asia/Baghdad',
		'3.5' => 'Asia/Tehran',
		'4' => 'Asia/Baku',
		'4.5' => 'Asia/Kabul',
		'5' => 'Asia/Karachi',
		'5.5' => 'Asia/Calcutta',
		'5.75' => 'Asia/Katmandu',
		'6' => 'Asia/Almaty',
		'6.5' => 'Asia/Rangoon',
		'7' => 'Asia/Bangkok',
		'8' => 'Asia/Shanghai',
		'9' => 'Asia/Tokyo',
		'9.5' => 'Australia/Adelaide',
		'10' => 'Australia/Canberra',
		'11' => 'Asia/Magadan',
		'12' => 'Pacific/Auckland'
		);
	}
}
