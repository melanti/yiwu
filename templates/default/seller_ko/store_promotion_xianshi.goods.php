<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){?>

<ul class="goods-list">
  <?php foreach($output['goods_list'] as $key=>$val){?>
  <li>
    <div class="goods-thumb"> <a href="<?php echo urlShop('goods', 'index', array('goods_id' => $val['goods_id']));?>" target="_blank"><img src="<?php echo thumb($val, 240);?>"/></a></div>
    <dl class="goods-info">
      <dt><a href="<?php echo urlShop('goods', 'index', array('goods_id' => $val['goods_id']));?>" target="_blank"><?php echo $val['goods_name'];?></a> </dt>
      <dd>판매가격：<?php echo number_format($val['goods_price']);?>원
    </dl>
    <a cnbiztype="btn_add_xianshi_goods" data-goods-id="<?php echo $val['goods_id'];?>" data-goods-name="<?php echo $val['goods_name'];?>" data-goods-img="<?php echo thumb($val, 240);?>" data-goods-price="<?php echo number_format($val['goods_price']);?>" href="javascript:void(0);" class="ncsc-btn-mini">상품선택/할인가수정</a> </li>
  <?php } ?>
</ul>
<div class="pagination"><?php echo $output['show_page']; ?></div>
<?php } else { ?>
<div>내용이 없습니다.</div>
<?php } ?>
<div id="dialog_add_xianshi_goods" style="display:none;">
  <input id="dialog_goods_id" type="hidden">
  <input id="dialog_input_goods_price" type="hidden">
  <div class="selected-goods-info">
    <div class="goods-thumb"><img id="dialog_goods_img" src="" alt=""></div>
    <dl class="goods-info">
      <dt id="dialog_goods_name"></dt>
      <dd>판매가격：<span id="dialog_goods_price"></span>원</dd>
      <dd>할인가격：<input id="dialog_xianshi_price" type="text" class="text w70"><em class="add-on">원</em>
      <p id="dialog_add_xianshi_goods_error">
          <label for="dialog_xianshi_price" class="error" ><i class='icon-exclamation-sign'></i>할인가격을 입력해 주세요. 판매가보다 높을 수 없습니다.</label></p></dd>
        
    </dl>
  </div>
  <div class="eject_con">
    <div class="bottom pt10 pb10"><a id="btn_submit" class="submit" href="javascript:void(0);">완료</a></div>
  </div>
</div>
