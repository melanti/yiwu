<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="tabmenu">
    <?php include template('layout/submenu');?>
</div>
<div class="alert alert-block mt10">
  <ul class="mt5">
    <li>1. “템플릿선택 -> 미리보기" 로가시면 템플릿을 프린트하실 수 있습니다.</li>
    <li>2.“설정”</strong>을 클릭하시면 직접 설정할 수 있는 여백과 나타낼 항목들을 포함한 내용들이 나옵니다.  </li>
    <li>3.“기본”</strong>을 클릭하시면 최근 설정하신 템플릿을 기본 프린트 템플릿으로 설정하실 수 있습니다.</li>
    <li>4.“연동해제”</strong>를 클릭하시면 연동된 것을 해제하실 수 있고, 다른 템플릿을  다시 선택하실 수 있습니다.</li>
  </ul>
</div>
<table class="ncsc-default-table">
    <thead>
        <tr>
            <th class="w30"></th>
            <th class="w180 tl">물류회사</th>            
            <th class="w180 tl">송장템플릿</th>
            <th class="tl">송장사진</th>
            <th class="w100 tl">기본</th>
            <th class="w150">선택</th>
        </tr>
    </thead>
    <tbody>
        <?php if(!empty($output['express_list']) && is_array($output['express_list'])){?>
        <?php foreach($output['express_list'] as $key => $value){?>
        <tr class="bd-line">
            <td></td>
            <td class="tl"><?php echo $value['e_name'];?></td>
            <td class="tl"><?php echo $value['waybill_name'];?></td>
            <td class="tl">
                <?php if($value['bind']) { ?>
                <div class="waybill-img-thumb">
                    <a class="nyroModal" rel="gal" href="<?php echo $value['waybill_image_url'];?>">
                        <img src="<?php echo $value['waybill_image_url'];?>">
                    </a>
                </div>
                <div class="waybill-img-size">
                    <p>너비：<?php echo $value['waybill_width'];?>(mm)</p>
                    <p>높이：<?php echo $value['waybill_height'];?>(mm)</p>
                </div>
                <?php } ?>
    </td>
    <td class="tl"><?php echo $value['is_default_text'];?></td>
    <td class="nscs-table-handle">
        <span>
            <?php if($value['bind']) { ?>
            <a href="<?php echo urlShop('store_waybill', 'waybill_setting', array('store_waybill_id' => $value['store_waybill_id']));?>" class="btn-blue"><i class="icon-wrench"></i><p>설정</p></a></span><span><a href="javascript:;" nctype="btn_set_default" data-store-waybill-id="<?php echo $value['store_waybill_id'];?>" class="btn-green"><i class="icon-ok-sign"></i><p>기본</p></a></span><span><a href="javascript:;" nctype="btn_unbind" data-store-waybill-id="<?php echo $value['store_waybill_id'];?>" class="btn-red"><i class="icon-unlink"></i><p>연동해제</p></a></span>
        <?php } else { ?>
                    <span><a href="<?php echo urlShop('store_waybill', 'waybill_bind', array('express_id' => $value['id']));?>" class="btn-blue"><i class="icon-ok-circle"></i><p>템플릿선택</p></a>
                    <?php } ?>
                </span>
            </td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
            <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i>
                    <span>기본 물류회사를 아직 선택하지 않으셨습니다.<a href="<?php echo urlShop('store_deliver_set', 'express');?>">설정하기</a></span>
            </div></td>
        </tr>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
        </tr>
    </tfoot>
</table>
<form id="edit_form" method="post">
    <input id="store_waybill_id" name="store_waybill_id" type="hidden" />
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('[nctype="btn_unbind"]').on('click', function() {
            if(confirm('연동를 해제하시겠습니까？')) {
                $('#store_waybill_id').val($(this).attr('data-store-waybill-id'));
                $('#edit_form').attr('action', "<?php echo urlShop('store_waybill', 'waybill_unbind');?>");
                $('#edit_form').submit();
            }
        });

        $('[nctype="btn_set_default"]').on('click', function() {
            $('#store_waybill_id').val($(this).attr('data-store-waybill-id'));
            $('#edit_form').attr('action', "<?php echo urlShop('store_waybill', 'waybill_set_default');?>");
            $('#edit_form').submit();
        });
    });
</script>

