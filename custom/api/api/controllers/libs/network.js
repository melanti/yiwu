module.exports = {
  get_ip : function(req, res) {
    var ip_addr = '127.0.0.1';
    try{
        ip_addr = req.ip;
        if(typeof req.headers['x-forwarded-for'] !== 'undefined') {
            ip_addr = req.headers['x-forwarded-for'].replace(/unknown, /g, '').replace(/,.+$/, '');
        }
        ip_addr = ip_addr.replace(/^::ffff:/, '');
    }catch(e){
        sails.log.debug('network.js catch Error: ', e);
        ip_addr = '127.0.0.1';
    }
    return ip_addr;
  }
};
