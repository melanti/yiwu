(function($, angular, _, is){
  'use strict';

  angular.module('underscore.service',[]);
  angular.module('sha256.service',[]);
  angular.module('App.common',['underscore.service', 'PubSub', 'sha256.service','ngSanitize']);

  angular.module('App.common').directive('convertToNumber', convertToNumber);
  angular.module('App.common').directive('convertToBoolean', convertToBoolean);
  angular.module('App.common').directive('krInput', ['$parse', krInput]);
  angular.module('App.common').directive('activeNav', activeNav);
  angular.module('App.common').directive('activeHeaderNav', activeHeaderNav);

  angular.module('App.common').directive('validationEmail', validationEmail);
  angular.module('App.common').directive('validationPurchaseUrl', validationPurchaseUrl);
  angular.module('App.common').directive('validationUrl', validationUrl);
  angular.module('App.common').directive('deleteRequireBootstrap', deleteRequireBootstrap);

  angular.module('App.operation',['App.common', 'underscore.service', 'PubSub', 'sha256.service','ngSanitize']);
  angular.module('App.items',['App.common', 'underscore.service', 'PubSub', 'sha256.service','ngSanitize']);
  angular.module('App.org',['App.common', 'underscore.service', 'PubSub', 'sha256.service','ngSanitize']);
  angular.module('App',['App.common', 'App.operation', 'App.items', 'App.org', 'underscore.service', 'PubSub', 'sha256.service', 'ui.date','ngSanitize','ngMessages']);


  function convertToNumber() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(val) {
          return parseInt(val, 10);
        });
        ngModel.$formatters.push(function(val) {
          return '' + val;
        });
      }
    };
  }

  function convertToBoolean() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(val) {
          return val === 'true';
        });
        //ngModel.$formatters.push(function(val) {
        //  return val ? '' + val : null;
        //});
      }
    };
  }

  function krInput($parse) {
    return {
      priority: 2,
      restrict: 'A',
      compile: function (element) {
        element.on('compositionstart', function (e) {
          e.stopImmediatePropagation();
        });
      },
    };
  }

  function activeNav() {
    return {
      link: function (scope, element, attrs, ngModel) {
        var pageId = $("body").data("page-id");
        if ((attrs.activeNav == pageId)
          || (pageId == "operation-partner-registration" && attrs.activeNav == "operation-partner-list")
          || (pageId == "brand-registration" && attrs.activeNav == "brand-list")) {
          element.css({'font-weight': 'bold'})
        }
      }
    };
  }

  function activeHeaderNav() {
    return {
      link: function (scope, element, attrs, ngModel) {
        var pageId = $("body").data("page-id");
        if ((attrs.activeHeaderNav == "operation" && ( pageId == "info"
            || pageId == "operation-partner-list"
            || pageId == "operation-partner-registration" )
          ) || ( attrs.activeHeaderNav == "items" && ( pageId == "items-list"
            || pageId == "threeD-list"
            || pageId == "item-registration"
            || pageId == "category-operation"
            || pageId == "brand-list"
            || pageId == "brand-registration")
          )) {
          element.css({
            'opacity': '1.0',
            'border-bottom': '3px solid #30a6b2'
          })
        }
      }
    };
  }

  function validationEmail(){
    return {
      restrict: "A",
      require: "ngModel",
      link: function(scope, element, attributes, ngModel) {
        ngModel.$validators.validationEmail = function(modelValue) {
          return ( (modelValue.length===0) || (modelValue.length>0 && is.email(modelValue)) );
        }
      }
    };
  }

  function validationUrl(){
    return {
      restrict: "A",
      require: "ngModel",
      link: function(scope, element, attributes, ngModel) {
        ngModel.$validators.validationUrl = function(modelValue) {
          return ( (modelValue.length===0) || (modelValue.indexOf('http') !==-1) );
        }
      }
    };
  }


  function validationPurchaseUrl(){
    return {
      restrict: "A",
      require: "ngModel",
      link: function(scope, element, attributes, ngModel) {
        ngModel.$validators.validationPurchaseUrl = function(modelValue) {
          // return ( (modelValue.indexOf('http') !==-1) && (modelValue.indexOf('${origin_item_id}') !==-1) );
          return ( (modelValue.indexOf('http') !==-1) );
        }
      }
    };
  }

  function deleteRequireBootstrap(){
    return {
      restrict: "A",
      require: "ngModel",
      link: function(scope, element, attributes, ngModel) {
        element.off('invalid');
      }
    };
  }

})(window.jQuery, window.angular, window._, window.is);
