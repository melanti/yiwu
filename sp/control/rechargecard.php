<?php
/**
 * 충전카드
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');

class rechargecardControl extends SystemControl
{
    const EXPORT_SIZE = 100;

    public function __construct()
    {
        parent::__construct();
    }

    public function indexOp()
    {
        $model = Model('rechargecard');
        $condition = array();

        if (isset($_GET['form_submit'])) {
            $sn = trim((string) $_GET['sn']);
            $batchflag = trim((string) $_GET['batchflag']);
            $state = trim((string) $_GET['state']);

            if (strlen($sn) > 0) {
                $condition['sn'] = array('like', "%{$sn}%");
                Tpl::output('sn', $sn);
            }

            if (strlen($batchflag) > 0) {
                $condition['batchflag'] = array('like', "%{$batchflag}%");
                Tpl::output('batchflag', $batchflag);
            }

            if ($state === '0' || $state === '1') {
                $condition['state'] = $state;
                Tpl::output('state', $state);
            }

            if ($condition) {
                Tpl::output('form_submit', 'ok');
            }
        }

        $cardList = $model->getRechargeCardList($condition, 20);

        Tpl::output('card_list', $cardList);
        Tpl::output('show_page', $model->showpage());

        Tpl::showpage('rechargecard.index');
    }

    public function add_cardOp()
    {
        if (!chksubmit()) {
            Tpl::showpage('rechargecard.add_card');
            return;
        }

        $denomination = (float) $_POST['denomination'];
        if ($denomination < 0.01) {
            showMessage('최소금액:0.01', '', 'html', 'error');
            return;
        }
        if ($denomination > 10000000) {
            showMessage('최대금액:10000000', '', 'html', 'error');
            return;
        }

        $snKeys = array();

        switch ($_POST['type']) {
        case '0':
            $total = (int) $_POST['total'];
            if ($total < 1 || $total > 9999) {
                showMessage('1~9999이내 숫자를 입력하세요', '', 'html', 'error');
                exit;
            }
            $prefix = (string) $_POST['prefix'];
            if (!preg_match('/^[0-9a-zA-Z]{0,16}$/', $prefix)) {
                showMessage('16자 이내의 영문+숫자입니다.', '', 'html', 'error');
                exit;
            }
            while (count($snKeys) < $total) {
                $snKeys[$prefix . md5(uniqid(mt_rand(), true))] = null;
            }
            break;

        case '1':
            $f = $_FILES['_textfile'];
            if (!$f || $f['error'] != 0) {
                showMessage('파일 업로드 실패', '', 'html', 'error');
                exit;
            }
            if (!is_uploaded_file($f['tmp_name'])) {
                showMessage('옵로드한 파일을 찾을 수 없습니다.', '', 'html', 'error');
                exit;
            }
            foreach (file($f['tmp_name']) as $sn) {
                $sn = trim($sn);
                if (preg_match('/^[0-9a-zA-Z]{1,50}$/', $sn))
                    $snKeys[$sn] = null;
            }
            break;

        case '2':
            foreach (explode("\n", (string) $_POST['manual']) as $sn) {
                $sn = trim($sn);
                if (preg_match('/^[0-9a-zA-Z]{1,50}$/', $sn))
                    $snKeys[$sn] = null;
            }
            break;

        default:
            showMessage('오류', '', 'html', 'error');
            exit;
        }

        $totalKeys = count($snKeys);
        if ($totalKeys < 1 || $totalKeys > 9999) {
            showMessage('1~9999개의 충전카드번호를 추가할 수 있습니다.', '', 'html', 'error');
            exit;
        }

        if (empty($snKeys)) {
            showMessage('정확한 카드번호를 입력하세요', '', 'html', 'error');
            exit;
        }

        $snOccupied = 0;
        $model = Model('rechargecard');

        // chunk size = 50
        foreach (array_chunk(array_keys($snKeys), 50) as $snValues) {
            foreach ($model->getOccupiedRechargeCardSNsBySNs($snValues) as $sn) {
                $snOccupied++;
                unset($snKeys[$sn]);
            }
        }

        if (empty($snKeys)) {
            showMessage('조작실패, 이미 존재하는 카드번호가 있습니다.', '', 'html', 'error');
            exit;
        }

        $batchflag = $_POST['batchflag'];
        $adminName = $this->admin_info['name'];
        $ts = time();

        $snToInsert = array();
        foreach (array_keys($snKeys) as $sn) {
            $snToInsert[] = array(
                'sn' => $sn,
                'denomination' => $denomination/C('site_cur'),
                'denomination_ko' => $denomination,
                'batchflag' => $batchflag,
                'admin_name' => $adminName,
                'tscreated' => $ts,
            );
        }

        if (!$model->insertAll($snToInsert)) {
            showMessage('조작실패', '', 'html', 'error');
            exit;
        }

        $countInsert = count($snToInsert);
        $this->log("추가{$countInsert}장 충전카드 (금액: {$denomination}, 일괄표식“{$batchflag}”）");

        $msg = '조작성공';
        if ($snOccupied > 0)
            $msg .= "{$snOccupied}개의 카드번호가 충돌이 생겼습니다.";

        showMessage($msg, urlAdmin('rechargecard', 'index'));
    }

    public function del_cardOp()
    {
        if (empty($_GET['id'])) {
            showMessage('오류', '', 'html', 'error');
        }

        Model('rechargecard')->delRechargeCardById($_GET['id']);

        $this->log("충전카드 삭제（#ID: {$_GET['id']}）");

        showMessage('조작성공', getReferer());
    }

    public function del_card_batchOp()
    {
        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
            showMessage('오류', '', 'html', 'error');
        }

        Model('rechargecard')->delRechargeCardById($_POST['ids']);

        $count = count($_POST['ids']);
        $this->log("{$count}개 충전카드 삭제");

        showMessage('조작성공', getReferer());
    }

    /**
     * 불러내기
     */
    public function export_step1Op()
    {
        $model = Model('rechargecard');
        $condition = array();

        if (isset($_GET['form_submit'])) {
            $sn = trim((string) $_GET['sn']);
            $batchflag = trim((string) $_GET['batchflag']);
            $state = trim((string) $_GET['state']);

            if (strlen($sn) > 0) {
                $condition['sn'] = array('like', "%{$sn}%");
                Tpl::output('sn', $sn);
            }

            if (strlen($batchflag) > 0) {
                $condition['batchflag'] = array('like', "%{$batchflag}%");
                Tpl::output('batchflag', $batchflag);
            }

            if ($state === '0' || $state === '1') {
                $condition['state'] = $state;
                Tpl::output('state', $state);
            }

            if ($condition) {
                Tpl::output('form_submit', 'ok');
            }
        }

        if (!is_numeric($_GET['curpage'])){
            $count = $model->getRechargeCardCount($condition);
            $array = array();
            if ($count > self::EXPORT_SIZE ){	//노출下载링크
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=rechargecard&op=index');
                Tpl::showpage('export.excel');
                return;

            }else{	//如果수량小，直接下载
                $data = $model->getRechargeCardList($condition, self::EXPORT_SIZE);

                $this->createExcel($data);
            }
        }else{	//下载
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;

            $data = $model->getRechargeCardList($condition, 20, "{$limit1},{$limit2}");

            $this->createExcel($data);
        }
    }

    /**
     * 生成excel
     *
     * @param array $data
     */
    private function createExcel($data = array()){
        Language::read('export');
        import('libraries.excel');
        $excel_obj = new Excel();
        $excel_data = array();
        //설정样式
        $excel_obj->setStyle(array('id'=>'s_title','Font'=>array('FontName'=>'宋体','Size'=>'12','Bold'=>'1')));
        //header
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'충전카드번호');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'일괄표식');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'금액(원)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'발표관리자');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'발표시간');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'수취인');

        //data
        foreach ((array)$data as $k=>$v){
            $tmp = array();
            $tmp[] = array('data'=>"\t".$v['sn']);
            $tmp[] = array('data'=>"\t".$v['batchflag']);
            $tmp[] = array('data'=>"\t".$v['denomination']);
            $tmp[] = array('data'=>"\t".$v['admin_name']);
            $tmp[] = array('data'=>"\t".date('Y-m-d H:i:s', $v['tscreated']));
            if ($v['state'] == 1 && $v['member_id'] > 0 && $v['tsused'] > 0) {
                $tmp[] = array('data'=>"\t".$v['member_name']);
            } else {
                $tmp[] = array('data'=>"\t-");
            }
            $excel_data[] = $tmp;
        }
        $excel_data = $excel_obj->charset($excel_data,CHARSET);
        $excel_obj->addArray($excel_data);
        $excel_obj->addWorksheet($excel_obj->charset('충전카드',CHARSET));
        $excel_obj->generateXML($excel_obj->charset('충전카드',CHARSET).$_GET['curpage'].'-'.date('Y-m-d-H',time()));
    }

}
