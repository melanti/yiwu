<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['nc_common_pselect']	=  '-선택-';


/**
 * 信息텍스트
 */
$lang['error']				= '요청 처리중 오류 발생:<br />';
$lang['homepage']			= '메인';
$lang['cur_location']		= '현재위치';
$lang['miss_argument']		= '파라미터 부족';
$lang['invalid_request']	= '불법 방문';
$lang['param_error']		= '파라미터 오류';
$lang['wrong_checkcode']	= '인증코드 오류';
$lang['no_record']			= '등록된 기록이 없습니다.';
$lang['nc_record']			= '기록';
$lang['currency']			= '￥';
$lang['nc_colon']      = '：';
$lang['currency_zh']			= '원';
$lang['ten_thousand']       = '万';
$lang['close']                	= 'CLOSE';
$lang['open']                 	= 'OPEN';
$lang['nc_yes']				= '예';
$lang['nc_no']				= '아니오';
$lang['nc_more']				= '더보기';
$lang['nc_default']			= '기본';
//调试模式语言包
$lang['nc_debug_current_page']			= "현재페이지";
$lang['nc_debug_request_time']			= "요청시간";
$lang['nc_debug_execution_time']		= "로딩시간";
$lang['nc_debug_memory_consumption']	= "메모리";
$lang['nc_debug_request_method']		= "요청방법";
$lang['nc_debug_communication_protocol']= "통신협의";
$lang['nc_debug_user_agent']			= "회원대리";
$lang['nc_debug_session_id']			= "세션ID";
$lang['nc_debug_logging']				= "로그기록";
$lang['nc_debug_logging_1']				= "개로그";
$lang['nc_debug_logging_2']				= "등록된 기록이 없습니다.";
$lang['nc_debug_load_files']			= "파일로딩";
$lang['nc_debug_trace_title']			= "페이지 Trace정보";

//조작 알림
$lang['nc_common_op_succ'] 	= '완료';
$lang['nc_common_op_fail'] 	= '실패';
$lang['nc_common_del_succ'] = '삭제성공';
$lang['nc_common_del_fail'] = '삭제실패';
$lang['nc_common_save_succ'] = '저장성공';
$lang['nc_common_save_fail'] = '저장실패';
$lang['nc_succ'] = '성공';
$lang['nc_fail'] = '실패';

//站外分享接口
$lang['nc_shareset_qqweibo'] 		= 'QQ WEIBO';
$lang['nc_shareset_sinaweibo'] 		= 'SINA WEIBO';

/**
 * header中的텍스트
 */
$lang['nc_hello']	= "Hello";
$lang['nc_logout']	= "로그아웃";
$lang['nc_guest']	= "손님";
$lang['nc_login']	= "로그인";
$lang['nc_register']	= "회원가입";
$lang['nc_seller'] = '업체중심';
$lang['nc_user_center']	= "마이페이지";
$lang['nc_message']		= "시스템 쪽지";
$lang['nc_help_center']	= "도움중심";
$lang['nc_more_links']	= "링크 더보기";
$lang['nc_index']		= "메인";
$lang['nc_cart']			= "장바구니";
$lang['nc_kindof_goods']	= "가지 상품";
$lang['nc_favorites']		= "나의 즐겨찾기";
$lang['nc_nothing']			= "무";
$lang['nc_filter'] = '필터';
$lang['nc_all_goods'] = '전체상품';
$lang['nc_search'] = '검색';
$lang['nc_publish'] = '발표';
$lang['nc_download'] = '다운로드';

$lang['nc_buying_goods']			= "이미 구매한 상품";
$lang['nc_mysns']					= "나의공간";
$lang['nc_myfriends']				= "나의친구";
$lang['nc_selled_goods']			= "이미 판매된 상품";
$lang['nc_selling_goods']			= "진열상품";
$lang['nc_mystroe']					= "나의미니샵";
$lang['nc_favorites_goods']			= "즐겨찾기한 상품";
$lang['nc_favorites_stroe']			= "즐겨찾기한 미니샵";
$lang['nc_cart_no_goods']			= "장바구니에 등록된 상품이 없습니다.";
$lang['nc_check_cart']				= "장바구니 보기";
$lang['nc_accounts_goods']			= "결제상품";
$lang['nc_goods_num_one']			= "총";
$lang['nc_goods_num_two']			= "가지 상품, 총금액";
$lang['nc_sign_multiply']			= "×";


/**
 * 翻页
 **/
$lang['first_page'] = '처음';
$lang['last_page'] = '끝';
$lang['pre_page'] = '이전';
$lang['next_page'] = '다음';

$lang['nc_none_input'] 	= '입력하세요';
$lang['nc_current_edit'] 	= '수정중';

$lang['nc_add']				= '추가';
$lang['nc_new']				= '추가';
$lang['nc_update']			= '업데이트';
$lang['nc_edit']			= '수정';
$lang['nc_del']				= '삭제';

$lang['nc_cut']				= '짜르기';
$lang['download_lang']      = '데이터 분리 다운로드';

$lang['order_log_cancel'] = '주문을 취소하였습니다.';
$lang['order_log_receive_paye'] = '입금을 확인하였습니다.';
