
(function($, angular, _, APP){

  APP.modules.ctrl.tableBodyThreeDModalCtrl = tableBodyThreeDModalCtrl;

  //tableBodyThreeDModalCtrl
  function tableBodyThreeDModalCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL){
    var ctrl = this;
    ctrl._filterResults;

    ctrl.getTotal;
    ctrl.getStart;
    ctrl.getCount;
    ctrl.getResults;
    ctrl.updatePage;

    ctrl.curModel;
    ctrl.curIdx;

    ctrl.renderRegistSection = _renderRegistSection;
    ctrl.showTdPrevModal = _showTdPrevModal;


    _init();

    function _init(){
      console.log('body init');
      $scope.$on('$destroy', function () {
      });
    }


    function _renderRegistSection(__time){
      // var str = ( !COMMON_UTIL.isExist(__time) )?'':(__time).substr(0,10);
      // return str;
      return COMMON_UTIL.getLocalTimezoneStr(__time, '-');
    }


    function _showTdPrevModal(__li, __idx){
      // console.log('_showTdPrevModal: ', __li, getUrlArr(__li.td_fitting_files));
      if(__li.td_fitting_files){
        PubSub.publish('tdPrev:show', getUrlArr(__li.td_fitting_files));
      }else{
        PubSub.publish('tdPrev:show', {imgArr:['','','','','','','','','','','','']});
      }

      console.log( '__idx: ' , __idx );

      ctrl.curModel =  __li;
      ctrl.curIdx =  __idx;

      ctrl.setCurData({options:__li});
      ctrl.setCurIdx({options:__idx});

      $('.clickable-row').removeClass('on_td');
      $('.clickable-row').eq(__idx).addClass('on_td');

      function getUrlArr(__arr){
        return {imgArr:[
          (__arr.preview0.url || ''),
          (__arr.preview30.url || ''),
          (__arr.preview60.url || ''),
          (__arr.preview90.url || ''),
          (__arr.preview120.url || ''),
          (__arr.preview150.url || ''),
          (__arr.preview180.url || ''),
          (__arr.preview210.url || ''),
          (__arr.preview240.url || ''),
          (__arr.preview270.url || ''),
          (__arr.preview300.url || ''),
          (__arr.preview330.url || ''),
        ]};
      }
    }


  }

  tableBodyThreeDModalCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL'];
  tableBodyThreeDModalCtrl.bindToController = {
    getCurIdx:'&',
    setCurIdx:'&',
    getCurData:'&',
    setCurData:'&',

    getTotal:'&',
    getCount:'&',
    getStart:'&',
    getResults:'&'
  };
  tableBodyThreeDModalCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableBodyCtrl:true});
      APP.modules.directive.util
        .checkBoxHelper(scope, elem, attrs, ctrl, 'tableBodyCtrl.getResults()');
    }
  }


})(window.jQuery, window.angular, window._, window.APP);
