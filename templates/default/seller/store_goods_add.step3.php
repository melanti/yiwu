<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if ($output['edit_goods_sign']) {?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<?php } else {?>
<ul class="add-goods-step">
  <li><i class="icon icon-list-alt"></i>
    <h6>STEP.1</h6>
    <h2>카테고리 선택</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-edit"></i>
    <h6>STEP.2</h6>
    <h2>상세내용 입력</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li class="current"><i class="icon icon-camera-retro "></i>
    <h6>STEP.3</h6>
    <h2>이미지 업로드</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-ok-circle"></i>
    <h6>STEP.4</h6>
    <h2>등록 완료</h2>
  </li>
</ul>
<?php }?>
<form method="post" id="goods_image" action="<?php if ($output['edit_goods_sign']) { echo urlShop('store_goods_online', 'edit_save_image'); } else { echo urlShop('store_goods_add', 'save_image');}?>">
  <input type="hidden" name="form_submit" value="ok">
  <input type="hidden" name="commonid" value="<?php echo $output['commonid'];?>">
  <input type="hidden" name="ref_url" value="<?php echo $_GET['ref_url'];?>" />
  <?php if (!empty($output['value_array'])) {?>
  <div class="ncsc-form-goods-pic">
    <div class="container">
      <?php foreach ($output['value_array'] as $value) {?>
      <div class="ncsc-goodspic-list">
        <div class="title">
          <h3>색상：<?php if (isset($output['value'][$value['sp_value_id']])) { echo $output['value'][$value['sp_value_id']];} else {echo $value['sp_value_name'];}?></h3></div>
        <ul cnbiztype="ul<?php echo $value['sp_value_id'];?>">
          <?php for ($i = 0; $i < 5; $i++) {?>
          <li class="ncsc-goodspic-upload">
            <div class="upload-thumb"><img src="<?php echo cthumb($output['img'][$value['sp_value_id']][$i]['goods_image'], 240);?>" cnbiztype="file_<?php echo $value['sp_value_id'] . $i;?>">
              <input type="hidden" name="img[<?php echo $value['sp_value_id'];?>][<?php echo $i;?>][name]" value="<?php echo $output['img'][$value['sp_value_id']][$i]['goods_image'];?>" cnbiztype="file_<?php echo $value['sp_value_id'] . $i;?>">
            </div>
            <div class="show-default<?php if ($output['img'][$value['sp_value_id']][$i]['is_default'] == 1) {echo ' selected';}?>" cnbiztype="file_<?php echo $value['sp_value_id'] . $i;?>">
              <p><i class="icon-ok-circle"></i>기본이미지
                <input type="hidden" name="img[<?php echo $value['sp_value_id'];?>][<?php echo $i;?>][default]" value="<?php if ( $output['img'][$value['sp_value_id']][$i]['is_default'] == 1) {echo '1';}else{echo '0';}?>">
              </p><a href="javascript:void(0)" cnbiztype="del" class="del" title="제거">X</a>
            </div>
            <div class="show-sort">순서：<input name="img[<?php echo $value['sp_value_id'];?>][<?php echo $i;?>][sort]" type="text" class="text" value="<?php echo intval($output['img'][$value['sp_value_id']][$i]['goods_image_sort']);?>" size="1" maxlength="1">
            </div>
            <div class="ncsc-upload-btn"><a href="javascript:void(0);"><span><input type="file" hidefocus="true" size="1" class="input-file" name="file_<?php echo $value['sp_value_id'] . $i;?>" id="file_<?php echo $value['sp_value_id'] . $i;?>"></span><p><i class="icon-upload-alt"></i>업로드</p>
              </a></div>
            
          </li>
          <?php }?>
        </ul>
        <div class="ncsc-select-album">
          <a class="ncsc-btn" href="index.php?act=store_album&op=pic_list&item=goods_image&color_id=<?php echo $value['sp_value_id'];?>" cnbiztype="select-<?php echo $value['sp_value_id'];?>"><i class="icon-picture"></i>앨범에서 선택</a>
          <a href="javascript:void(0);" cnbiztype="close_album" class="ncsc-btn ml5" style="display: none;"><i class=" icon-circle-arrow-up"></i>앨범 닫기</a>
        </div>
        <div cnbiztype="album-<?php echo $value['sp_value_id'];?>"></div>
      </div>
      <?php }?>
    </div>
    <div class="sidebar"><div class="alert alert-info alert-block" id="uploadHelp">
    <div class="faq-img"></div>
    <h4>업로드 주의사항：</h4><ul>
    <li>1. jpg\jpeg\png파일, 한 장 크기 최대<?php echo intval(C('image_max_filesize'))/1024;?>M의 정사각형 이미지</li>
    <li>2. 업로드 사진의 사장 큰 사이즈는 1280px로 유지됩니다.</li>
    <li>3. 최대 5장의 이미지 업로드가 가능하며 혹은 이미지공간에 업로드 하셨던 이미지를 사용하실 수 있습니다. 업로드 후의 이미지는 이미지공간에 저장되며 이후 편리하게 사용하실 수 있습니다.</li>
    <li>4. 숫자 순서의 변경으로 상품 사진의 순서를 변경할 수 있습니다.</li>
    <li>5. 이미지의 화질은 밝고 뚜렷해야 하며, 허위 이미지를 사용하실 수 없습니다.</li>
    <li>6. 완료 후 "다음 단계"를 클릭하여 주십시오. (다음 단계로 넘어가지 않으시면 저장되지 않습니다.)</li>
    </ul><h4>TIP:</h4><ul><li>1. 기본이미지는 백색 배경의 정면 이미지가 좋습니다.</li><li>2. 상품이미지 사진 순서 : 정면->측면->자세한 이미지</li></ul></div></div>
  </div>
  <?php }?>
  <div class="bottom tc hr32"><label class="submit-border"><input type="submit" class="submit" value="<?php if ($output['edit_goods_sign']) { echo '완료'; } else { ?>다음，상품등록확정<?php }?>" /></label></div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_goods_add.step3.js" charset="utf-8"></script>
<script>
var SITEURL = "<?php echo SHOP_SITE_URL; ?>";
var DEFAULT_GOODS_IMAGE = "<?php echo UPLOAD_SITE_URL.DS.defaultGoodsImage(240);?>";
var SHOP_RESOURCE_SITE_URL = "<?php echo SHOP_RESOURCE_SITE_URL;?>";
$(function(){
    <?php if ($output['edit_goods_sign']) {?>
    $('input[type="submit"]').click(function(){
        ajaxpost('goods_image', '', '', 'onerror');
    });
    <?php }?>
    /* ajax打开图片空间 */
    <?php foreach ($output['value_array'] as $value) {?>
    $('a[cnbiztype="select-<?php echo $value['sp_value_id'];?>"]').ajaxContent({
        event:'click', //mouseover
        loaderType:"img",
        loadingMsg:SHOP_TEMPLATES_URL+"/images/loading.gif",
        target:'div[cnbiztype="album-<?php echo $value['sp_value_id'];?>"]'
    }).click(function(){
        $(this).hide();
        $(this).next().show();
    });
    <?php }?>
});
</script> 
