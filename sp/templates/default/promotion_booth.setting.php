<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){
	$("#submitBtn").click(function(){ 
 		$("#add_form").submit();
	});
    //페이지输入내용验证
	$("#add_form").validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
		},
		rules : {
		    promotion_booth_price: {
				required : true,
				digits : true
			},
			promotion_booth_goods_sum: {
				required : true,
				digits : true,
				min : 1
			}
		},
		messages : {
		    promotion_booth_price: {
				required : '추천위치 가격을 입력하세요',
				digits : '추천위치 가격을 입력하세요'
			},
			promotion_booth_goods_sum: {
				required : '최소 1보다 큰 정수를 입력하셔야합니다.',
				digits : '최소 1보다 큰 정수를 입력하셔야합니다.',
				min : '최소 1보다 큰 정수를 입력하셔야합니다.'
			}
		}
	});
});
</script> 
<div class="page">
  <!-- 페이지导航 -->
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['nc_promotion_bundling'];?></h3>
      <ul class="tab-base">
        <li><a href="<?php echo urlAdmin('promotion_booth', 'goods_list');?>"><span>상품리스트</span></a></li>
        <li><a href="<?php echo urlAdmin('promotion_booth', 'booth_quota_list');?>"><span>패키지리스트</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>설정</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="add_form" method="post" action="<?php echo urlAdmin('promotion_booth', 'booth_setting');?>">
    <input type="hidden" id="form_submit" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
        <tr class="noborder">
          <td colspan="2" class="required"><label class="validation" for="promotion_booth_price">추천위치 가격<?php echo $lang['nc_colon'];?></label></td>
        </tr>
        <tr class="noborder">
            <td class="vatop rowform">
                <input type="text" id="promotion_booth_price" name="promotion_booth_price" value="<?php echo $output['setting']['promotion_booth_price'];?>" class="txt">
            </td>
            <td class="vatop tips">구매 단위는 월(30일), 구매후 판매자는 해당 날자이내에 추천위치에 상품을 노출할 수 있습니다.</td>
        </tr>
        <tr class="noborder">
          <td colspan="2" class="required"><label class="validation" for="promotion_booth_goods_sum">추천 상품 최대 수량<?php echo $lang['nc_colon'];?></label></td>
        </tr>
        <tr class="noborder">
            <td class="vatop rowform">
                <input type="text" id="promotion_booth_goods_sum" name="promotion_booth_goods_sum" value="<?php echo $output['setting']['promotion_booth_goods_sum'];?>" class="txt">
            </td>
            <td class="vatop tips">매개 업체 추천 상품의 최대수량.</td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15"><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>

