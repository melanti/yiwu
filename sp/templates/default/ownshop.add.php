<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>자영업 업체</h3>
      <ul class="tab-base">
        <li><a href="index.php?act=ownshop&op=list"><span>관리</span></a></li>
        <li><a href="javascript:;" class="current"><span>추가</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>자영업 업체를 자유롭게 추가할 수 있으며 기본은 시동상태로 됩니다.</li>
            <li>자영업 업체를 추가시 모든 경영항목이 선택되고 수수료는 0, 또한 경영항목을 수동으로 연동할 수 있습니다.</li>
            <li>자영업 업체를 생성하면 회원 사용자, 업체관리자 등 모든 계정은 통일됩니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form id="store_form" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="store_id" value="<?php echo $output['store_array']['store_id']; ?>" />
    <table class="table tb-type2">
      <tbody>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="store_name">업체명:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="" id="store_name" name="store_name" class="txt" /></td>
          <td class="vatop tips"></td>
        </tr>
        <tr class="noborder">
          <td colspan="2" class="required"><label class="validation" for="member_name">업체아이디:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="" id="member_name" name="member_name" class="txt" /></td>
          <td class="vatop tips">회원로그인 아이디</td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="seller_name">점장판매자계정:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="" id="seller_name" name="seller_name" class="txt" /></td>
          <td class="vatop tips">업체 관리자 로그인시 필요한 아이디</td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="member_passwd">비밀번호:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="password" value="" id="member_passwd" name="member_passwd" class="txt" /></td>
          <td class="vatop tips"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15"><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript">
$(function(){
    //按钮先执行验证再提交表单
    $("#submitBtn").click(function(){
        if($("#store_form").valid()){
            $("#store_form").submit();
        }
    });

    $('#store_form').validate({
        errorPlacement: function(error, element){
            error.appendTo(element.parentsUntil('tr').parent().prev().find('td:first'));
        },
        rules : {
            store_name: {
                required : true,
                remote : '<?php echo urlAdmin('ownshop', 'ckeck_store_name')?>'
            },
            member_name: {
                required : true,
                minlength : 3,
                maxlength : 15,
                remote   : {
                    url : 'index.php?act=ownshop&op=check_member_name',
                    type: 'get',
                    data:{
                        member_name : function(){
                            return $('#member_name').val();
                        }
                    }
                }
            },
            seller_name: {
                required : true,
                minlength : 3,
                maxlength : 15,
                remote   : {
                    url : 'index.php?act=ownshop&op=check_seller_name',
                    type: 'get',
                    data:{
                        seller_name : function(){
                            return $('#seller_name').val();
                        }
                    }
                }
            },
            member_passwd : {
                required : true,
                minlength: 6
            }
        },
        messages : {
            store_name: {
                required: '업체명을 입력하세요',
                remote : '업체명이 존재합니다.'
            },
            member_name: {
                required : '업체 아이디를 입력하세요',
                minlength : '업체 아이디는 최소 3자입니다.',
                maxlength : '업체 아이디는 최대 15자입니다.',
                remote   : '본 아이디는 이미 기타 업체회원이 사용하고 있습니다.'
            },
            seller_name: {
                required : '점장판매자 아이디를 입력하세요',
                minlength : '점장 판매자 아이디는 최소 3자입니다.',
                maxlength : '점장 판매자 아이디는 최대 15자입니다.',
                remote   : '이미 존재하는 아이디입니다.'
            },
            member_passwd : {
                required : '비밀번호를 입력하세요',
                minlength: '비밀번호는 최소 6자이상으로 입력하세요'
            }
        }
    });
});
</script>
