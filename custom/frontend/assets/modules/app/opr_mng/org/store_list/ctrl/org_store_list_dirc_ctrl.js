
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.org.list.ctrl.storeAdminListDircCtrl');
  APP.modules.org.list.ctrl.storeAdminListDircCtrl = storeAdminListDircCtrl;

  //orgAdminListDircCtrl
  function storeAdminListDircCtrl($scope, $location, _, $timeout, XHR, PubSub, StoreListTableManager, API_URL_INFO, COMMON_UTIL){
    var vm = this,
        tableManager = StoreListTableManager.exports,
        pubsubBootstrap,
        btComplete;

    vm.bootstrapCtrl;
    vm._state={
      tableData:tableManager.getTableData(),
      bootstrapTableCtrl:{
        complete:{
          //tableOptionCtrl:false,
          //tableBodyCtrl:false,
          //paginationCtrl:false,
        }
      }
    };

    vm.exports={
      imgUrl:APP.info.imgURL,
      gVersion:APP.info.gVersion,
      COUNT_RANGE:tableManager.COUNT_RANGE,
      getFilter:_getFilter,
      setFilter:_setFilter,
      getReqModel:_getReqModel,
      setReqModel:_setReqModel,
      updatePage:_updatePage
    };

    _init();

    function _init(){
      console.log('APP.modules.org.list.orgAdminListDircCtrl: init' );

      (vm.bootstrapCtrl).split(',').forEach(function(element, index, array){
        vm._state.bootstrapTableCtrl.complete[element] =false;
      });

      btComplete = vm._state.bootstrapTableCtrl.complete;
      tableManager.init();

      $scope.$on('$destroy', function () {
        PubSub.unsubscribe(pubsubBootstrap);
      });

      pubsubBootstrap = PubSub.subscribe('tableComponent:bootstrap', _listenBootstrapState);
    }

    function _listenBootstrapState(topic, data){
      var chk = true;
      btComplete = _.extend({}, btComplete, data);
      _.values(btComplete).forEach(function(element, index, array){
        if(element===false) chk =false;
      });

      if( chk ){
        console.log('All bootstrap');

        PubSub.unsubscribe(pubsubBootstrap);
        PubSub.trigger('tableComponent:bootstrap:complete', {success:true});
        _updatePage();
      }
    }

    function _getReqModel(){
      return tableManager.getTableData().reqModel;
    }

    function _setReqModel(__options){
      var reqMdl = tableManager.getTableData().reqModel;
      _.each(__options , function(value, key, list){
        reqMdl[key] = value;
      });
    }

    function _getFilter(){
      return tableManager.getTableData().filter;
    }

    function _setFilter(__options){
      var reqMdl = tableManager.getTableData().filter;
      _.each(__options , function(value, key, list){
        reqMdl[key] = value;
      });
    }

    function _updatePage(__options){
      var tmpPage = (__options&&__options.start>=0)?__options.start:tableManager.getTableData().info.start,
          tmpViewRange = (__options&&__options.count)?__options.count:tableManager.getTableData().info.count,
          opts = tableManager.getTableData().reqModel,
          reqData = _.extend({}, {start:tmpPage, count:tmpViewRange}, opts);

      reqData.search_keyword = encodeURIComponent(reqData.search_keyword);

      tableManager
        .getTableList(reqData)
        .then(
          function(__sucData){
            if(__sucData.info){
              if( __sucData.info.start==null || __sucData.info.start=='null' )  {
                __sucData.info.start = parseInt(reqData.start);
              }
              __sucData.info.count = parseInt(reqData.count);
            }
            APP.modules.ctrl.tablePaginationBasicCtrl.beforeChangePage();
            tableManager.setTableData(__sucData);
            PubSub.trigger('table:change', tableManager.getTableData() );
          },
          function(__errData){
            alert( '입점업체 리스트 페이지 호출을 하지 못했습니다.' );  //
            console.log('orgAdminListDircCtrl _updatePage __errData: ',__errData);
          }
        );

    }

  }
  storeAdminListDircCtrl.$inject = ['$scope', '$location', '_', '$timeout', 'XHR', 'PubSub', 'StoreListTableManager', 'API_URL_INFO', 'COMMON_UTIL'];
  storeAdminListDircCtrl.bindToController = {
    bootstrapCtrl:'@'
  };
  storeAdminListDircCtrl.link = function () {
    return function (scope, elem, attrs, ctrl, transclude) {
      transclude(scope, function(clone, scope) {
        elem.find('.list_table').empty().append(clone);
      });
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
