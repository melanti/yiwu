<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['nc_statgeneral'];?></h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th class="nobg" colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
        <ul>
        	<li>상품 가격 범위 설정, 해당 가격 범위로 상품의 통계를 원하실때 설정하시면 됩니다.</li>
        	<li>가격 범위 설정의 추천: 1) 첫 가격은 0부터 시작; 2) 첫가격 및 끝가격을 같이 설정; 3) 가격 범위는 0~100,101~200으로 설정;</li>
        </ul></td>
      </tr>
    </tbody>
  </table>
  <form method="post" action="index.php" name="pricerangeform" id="pricerangeform">
  	<input type="hidden" value="ok" name="form_submit">
  	<input type="hidden" name="act" value="stat_general" />
    <input type="hidden" name="op" value="setting" />
    <table id="pricerang_table" class="table tb-type2">
    	<thead class="thead">
    		<tr class="space">
    			<th colspan='4'><a id="addrow" href="javascript:void(0);" class="btns"><span>행증가</span></a></th>
    		</tr>
    		<tr>
    			<th>첫금액</th>
    			<th>끝금액</th>
    			<th>조작</th>
    		</tr>
    	</thead>
    	<tbody>
    	<?php if (!empty($output['list_setting']['stat_pricerange']) && is_array($output['list_setting']['stat_pricerange'])){?>
    		<?php foreach ((array)$output['list_setting']['stat_pricerange'] as $k=>$v){ ?>
    		<tr id="row_<?php echo $k; ?>">
    			<td><input type="text" class="txt" value="<?php echo $v['s'];?>" name="pricerange[<?php echo $k;?>][s]"></td>
    			<td><input type="text" class="txt" value="<?php echo $v['e'];?>" name="pricerange[<?php echo $k;?>][e]"></td>
    			<td><a href="JavaScript:void(0);" onclick="delrow(<?php echo $k;?>);"><?php echo $lang['nc_del']; ?></a></td>
    		</tr>
    		<?php } ?>
    	<?php } ?>
    	</tbody>
    	<tfoot>
    		<tr class="tfoot">
    			<td colspan="4" class="align-center"><a id="ncsubmit" class="btn" href="JavaScript:void(0);"><span>확인</span></a></td>
    		</tr>
    	</tfoot>
      </table>
      
  </form>
  
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script>
<script type="text/javascript">
function delrow(i){
	$("#row_"+i).remove();
}
$(function(){
	var i = <?php echo count($output['list_setting']['stat_pricerange']); ?>;
	i += 1;
	var html = '';
	/*추가一行*/
	$('#addrow').click(function(){
		html = '<tr id="row_'+i+'">';
		html += '<td><input type="text" class="txt" name="pricerange['+i+'][s]" value="0"/></td>';
		html += '<td><input type="text" class="txt" name="pricerange['+i+'][e]" value="0"/></td>';
		html += '<td><a href="JavaScript:void(0);" onclick="delrow('+i+');"><?php echo $lang['nc_del']; ?></a></td>';
		$('#pricerang_table').find('tbody').append(html);
		i += 1;
	});
	
	$('#ncsubmit').click(function(){
		var result = true;
		$("#pricerang_table").find("[name^='pricerange']").each(function(){
			if(!$(this).val()){
				result = false;
			}
		});
		if(result){
			$('#pricerangeform').submit();
		} else {
			showDialog('가격범위를 정확히 입력하세요.');
		}
    });
})
</script>