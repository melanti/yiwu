<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['limit_admin']			= '관리자';
$lang['limit_gadmin']			= '권한그룹';
$lang['admin_add_limit_admin']			= '관리자추가';
$lang['admin_add_limit_gadmin']			= '권한그룹추가';
/**
 * 관리자리스트
 */
$lang['admin_index_not_allow_del']	= '시스템 관리자로 삭제할 수 없습니다.';
$lang['admin_index_login_null']		= '본 관리자는 로그인 기록이 없습니다.';
$lang['admin_index_username']		= '이름';
$lang['admin_index_password']		= '비밀번호';
$lang['admin_rpassword']			= '비밀번호확인';
$lang['admin_index_truename']		= '이름';
$lang['admin_index_email']			= '이메일';
$lang['admin_index_im']				= '채팅';
$lang['admin_index_last_login']		= '마지막로그인';
$lang['admin_index_login_times']	= '로그인수';
$lang['admin_index_sys_admin']		= '시스템관리자';
$lang['admin_index_del_admin']		= '삭제';
$lang['admin_index_sys_admin_no']	= '수정불가';
/**
 * 관리자추가
 */
$lang['admin_add_admin_not_exists']		= '본 아이디는 이미 존재합니다.';
$lang['admin_add_username_tip']			= '아이디를 입력하세요';
$lang['admin_add_password_tip']			= '비밀번호를 입력하세요';
$lang['admin_add_gid_tip']				= '권한그룹을 선택하세요, 미설정시 먼저 설정하셔야됩니다.';
$lang['admin_add_username_null']		= '아이디를 입력하세요';
$lang['admin_add_username_max']			= '아이디는 3-20자로 입력하세요';
$lang['admin_add_password_null']		= '비밀번호를 입력하세요';
$lang['admin_add_gid_null']				= '권한그룹을 선택하세요';
$lang['admin_add_password_type']		= '비밀번호는 영문 혹은 숫자로 입력하세요';
$lang['admin_add_password_max']			= '비밀번호는 6-20자로 입력하세요';
$lang['admin_add_username_not_exists']	= '본 아이디가 존재하지 않습니다.';
/**
 * 관리权限설정
 */
$lang['admin_set_admin_not_exists']		= '본 관라자는 존재하지않습니다.';
$lang['admin_set_back_to_admin_list']	= '돌아가기';
$lang['admin_set_back_to_member_list']	= '돌아가기';
$lang['admin_set_limt']					= '권한설정';
$lang['admin_set_system_login']			= '로그인';
$lang['admin_set_website_manage']		= '사이트관리';
$lang['admin_set_clear_cache']			= '캐쉬비우기';
$lang['admin_set_operation']			= '운영관리';
$lang['admin_set_operation_ztc_class']	= '직통차관리';
$lang['admin_set_operation_gold_buy']	= '금액 구매관리';
$lang['admin_set_operation_pointprod']	= '포인트 교환관리';
/**
 * 관리자修改
 */
$lang['admin_edit_success']				= '업데이트 성공';
$lang['admin_edit_fail']				= '업데이트 실패';
$lang['admin_edit_repeat_error']		= '두번 입력하신 비밀번호가 잃치하지 않습니다, 다시 입력해 주세요';
$lang['admin_edit_admin_error']			= '관리자 정보 오류';
$lang['admin_edit_admin_pw']			= '비밀번호';
$lang['admin_edit_admin_pw2']			= '비밀번호 확인';
$lang['admin_edit_pwd_tip1']			= '비수정시 공백으로 하시면됩니다.';


$lang['gadmin_name']				= '권한그룹';
$lang['gadmin_del_confirm']				= '본 그룹 삭제시 포함되여 있는 회원의 모든 권한도 같이 삭제됩니다, 정말 삭제하시겠습니까?';

