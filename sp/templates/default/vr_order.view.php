<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <table class="table tb-type2 order">
    <tbody>
      <tr class="space">
        <th colspan="2"><?php echo $lang['order_detail'];?></th>
      </tr>
      <tr>
        <th><?php echo $lang['order_info'];?></th>
      </tr>
      <tr>
        <td colspan="2"><ul>
            <li>
            <strong><?php echo $lang['order_number'];?>:</strong><?php echo $output['order_info']['order_sn'];?>
            </li>
            <li><strong><?php echo $lang['order_state'];?>:</strong><?php echo  str_replace(array("已取消","已完成"),array('취소됨','거래완료'),$output['order_info']['state_desc']);?></li>
            <li><strong><?php echo $lang['order_total_price'];?>:</strong><span class="red_common"><?php echo number_format($output['order_info']['order_amount_ko']);?>원 </span>
            </li>
            <li><strong><?php echo $lang['order_time'];?><?php echo $lang['nc_colon'];?></strong><?php echo date('Y-m-d H:i:s',$output['order_info']['add_time']);?></li>
            <li><strong><?php echo $lang['buyer_name'];?><?php echo $lang['nc_colon'];?></strong><?php echo $output['order_info']['buyer_name'];?></li>
            <li><strong>받을휴대폰<?php echo $lang['nc_colon'];?></strong><?php echo $output['order_info']['buyer_phone'];?></li>
            <li><strong><?php echo $lang['payment'];?><?php echo $lang['nc_colon'];?></strong><?php echo orderPaymentNameKo($output['order_info']['payment_code']);?></li>
            <?php if(intval($output['order_info']['payment_time'])){?>
            <li><strong><?php echo $lang['payment_time'];?><?php echo $lang['nc_colon'];?></strong><?php echo date('Y-m-d H:i:s',$output['order_info']['payment_time']);?></li>
            <?php }?>
            <?php if(intval($output['order_info']['shipping_time'])){?>
            <li><strong><?php echo $lang['ship_time'];?><?php echo $lang['nc_colon'];?></strong><?php echo date('Y-m-d H:i:s',$output['order_info']['shipping_time']);?></li>
            <?php }?>
            <?php if(intval($output['order_info']['finnshed_time'])){?>
            <li><strong><?php echo $lang['complate_time'];?><?php echo $lang['nc_colon'];?></strong><?php echo date('Y-m-d H:i:s',$output['order_info']['finnshed_time']);?></li>
            <?php }?>
            <?php if($output['order_info']['extend_order_common']['order_message'] != ''){?>
            <li><strong><?php echo $lang['buyer_message'];?><?php echo $lang['nc_colon'];?></strong><?php echo $output['order_info']['extend_order_common']['order_message'];?></li>
            <?php }?>
            <li><strong><?php echo $lang['store_name'];?><?php echo $lang['nc_colon'];?></strong><?php echo $output['order_info']['store_name'];?></li>
            <li><strong>구매자메모<?php echo $lang['nc_colon'];?></strong><?php echo $output['order_info']['buyer_msg'];?></li>
          </ul></td>
      </tr>
      <tr>
        <th><?php echo $lang['product_info'];?></th>
      </tr>
      <tr>
        <td><table class="table tb-type2 goods ">
            <tbody>
              <tr>
                <th></th>
                <th>상품</th>
                <th class="align-center">단가</th>
                <th class="align-center">수량</th>
                <th class="align-center">수수료율</th>
                <th class="align-center">받은수수료</th>
              </tr>
              <tr>
                <td class="w60 picture"><div class="size-56x56"><span class="thumb size-56x56"><i></i><a href="<?php echo SHOP_SITE_URL;?>/index.php?act=goods&goods_id=<?php echo $output['order_info']['goods_id'];?>" target="_blank"><img alt="<?php echo $lang['product_pic'];?>" src="<?php echo thumb($output['order_info'], 60);?>" /></a></span></div></td>
                <td class="w50pre"><p><a href="<?php echo SHOP_SITE_URL;?>/index.php?act=goods&goods_id=<?php echo $output['order_info']['goods_id'];?>" target="_blank"><?php echo $output['order_info']['goods_name'];?></a></p><p><?php if ($output['order_info']['order_promotion_type'] == 1) {?>공동구매，<?php } ?>유효기간：구매일로 부터 <?php echo date("Y-m-d",$output['order_info']['vr_indate']);?>
              <?php if ($output['order_info']['vr_invalid_refund'] == '0') { ?>
              ，기한완료후 환불 불가
              <?php } ?></p></td>
                <td class="w96 align-center"><span class="red_common"><?php echo number_format($output['order_info']['goods_price_ko']);?>원</span></td>
                <td class="w96 align-center"><?php echo $output['order_info']['goods_num'];?></td>
                <td class="w96 align-center"><?php echo $output['order_info']['commis_rate'] == 200 ? '' : $output['order_info']['commis_rate'].'%';?></td>
                <td class="w96 align-center"><?php echo $output['order_info']['commis_rate'] == 200 ? '' : ncPriceFormatKo(($output['order_info']['goods_price']*C('site_cur'))*$output['order_info']['commis_rate']/100);?>원</td>
              </tr>
            </tbody>
          </table></td>
      </tr>

      <tr>
        <th><?php echo $lang['product_info'];?></th>
      </tr>
      <tr>
        <td><table class="table tb-type2 goods">
            <tbody>
              <tr>
          <th class="w10"></th>
          <th>교환번호</th>
          <th>가격 (원)</th>
          <th>수량</th>
          <th>교환번호상태</th>
              </tr>
         <?php if (!empty($output['order_info']['extend_vr_order_code'])) { ?>
         <?php foreach($output['order_info']['extend_vr_order_code'] as $code_info){?>
            <tr>
            <td></td>
            <td class="w50"><?php echo $code_info['vr_code'];?></td>
            <td class="bdl"><?php echo number_format($output['order_info']['goods_price_ko']);?>원</td>
            <td class="bdl">1</td>
            </td>
            <td class="bdl"><?php echo $code_info['vr_code_desc'];?></td>
            </tr>
       <?php } ?>
	   <?php } else { ?>
	   <tr><td colspan="20" class="align-center">전자 교환번호 미생성</td></tr>
	   <?php } ?>
            </tbody>
          </table></td>
      </tr>
    </tbody>
    <tfoot>
      <tr class="tfoot">
        <td><a href="JavaScript:void(0);" class="btn" onclick="history.go(-1)"><span><?php echo $lang['nc_back'];?></span></a></td>
      </tr>
    </tfoot>
  </table>
</div>
