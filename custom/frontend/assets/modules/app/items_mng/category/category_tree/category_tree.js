(function($, angular, _, APP){

  if( $(document.body).attr('data-page-id')=='category-operation' ){
    bootstrapCategoryPage();
  }

  function bootstrapCategoryPage(){
    angular.module('App')
      .directive( 'categoryMain', ['XHR', 'PubSub', 'URL_INFO', 'CategoryManager', cateMainDirective] );

    angular.module('App')
      .directive( 'categoryList', ['$timeout', 'XHR', 'PubSub', 'URL_INFO', 'CategoryManager', cateListDirective] );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( parseInt(APP.info.user.auth.category,10)>=1 ){
            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }
        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }

  function cateMainDirective(XHR, PubSub, URL_INFO, CategoryManager){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      controller: ['$scope', '_',  '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'CategoryManager', 'COMMON_UTIL', APP.modules.ctrl.categoryMainCtrl],
      controllerAs: 'CategoryCtrl',
      bindToController: {
        // authCategory:'@',
        // accountType:'@'
      },
      link: function (scope, elem, attrs, ctrl, transclude) {
        transclude(scope, function(clone, scope) {
          var timer = null;
          elem.find('.panel-body').empty().append(clone);

          scope.$watch('CategoryCtrl.isShowCateNew', function(__newVal, __oldVal){
            if(timer) clearTimeout(timer);
            if(__newVal===true){
              timer = setTimeout(function(){
                $('#cateNew input[name=newRowName]').focus();
              },300);

            }
          });

        });
      },
      templateUrl: 'category_main.html'
    };
  }


  function cateListDirective($timeout, XHR, PubSub, URL_INFO, CategoryManager){
    return {
      restrict: 'E',
      require: '^categoryMain',
      replace:true,
      transclude: true,
      controller: ['$scope', '_',  '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'CategoryManager', APP.modules.ctrl.categoryListCtrl],
      controllerAs: 'CategoryListCtrl',
      bindToController: {
        getAuthCategory:'&',
        getAccountType:'&',
        getResults:'&',
        getFilteredResults:'&',
        //filteredResults:'=',
        setFilteredResults:'&',
        getDpt01ItemsCount:'&',
        setShowCateInfo:'&',
        updateCategoryDepth:'&',
        stopUpdateCategoryDepth:'&',
        setCurRow:'&',
        moveCagegory:'&'
      },
      templateUrl: 'category_list.html',
      link: function _cateListDirectiveLink(scope, elem, attrs, ctrl, transclude) {
        var isFst = true,
          timer = null,
          cateDepthtimer = null,
          SWAP_CANCEL_ID = 'swap';

        scope.$watchCollection('CategoryListCtrl.getFilteredResults()', function(__newVal, __oldVal){
          if(__newVal==__oldVal){}else{
            if(isFst){
              isFst = false;
              setTimeout(_init,300);

            }else{
              //_clearTimeout();
              //timer = setTimeout(function(){scope.$apply();},300);
            }
          }
        });

        function _init(){
          _registListener();
          if(scope.CategoryListCtrl.getAccountType() ==='shop' || parseInt(scope.CategoryListCtrl.getAuthCategory(),10)<2 ){
            $('.dd-handle').on('click' , function(__e){
              __e.preventDefault();
              if( $(__e.target).hasClass('children-cnt') ){
                $(window).trigger('CLICK_ITEM_CNT' , {target:__e.target});
                return false;
              }
              _activeListRow($(__e.target).closest('.dd-item'));
              return false;
            });

          } else{

            _setNestable();
          }

        }

        function _setNestable(){

          $('#nestable').nestable({
              group: 1,
              maxDepth: 2
            })
            .off('change')
            .on('change', function(__e){
              if(cateDepthtimer) $timeout.cancel(cateDepthtimer);
              XHR.CANCEL(SWAP_CANCEL_ID);

              scope.CategoryListCtrl.setFilteredResults({options:$('#nestable').nestable('serialize')});
              $('.dd-list-container>.dd-item').remove();
              scope.CategoryListCtrl.moveCagegory();
              scope.$apply();

              cateDepthtimer = $timeout(function(){
                scope.CategoryListCtrl.updateCategoryDepth({
                  options:{
                    cate_data:{categories:scope.CategoryListCtrl.getFilteredResults()},
                    cancelID:SWAP_CANCEL_ID
                  }
                });
              },600);

            });
        }

        function _registListener(){
          //NESTABLE_START

          $(window).on('NESTABLE_START', function(__e, __data){
            // console.log('NESTABLE_START');
            scope.CategoryListCtrl.stopUpdateCategoryDepth();
            if(cateDepthtimer) $timeout.cancel(cateDepthtimer);
          });

          $(window).on('NESTABLE_END', function(__e, __data){
            _deactiveListRow();
            _activeListRow(__data);
          });

          $(window).on('CLICK_ITEM_CNT', function(__e, __data){
            var dpt01 = $(__data.target).attr('data-dpt01-id');
            var dpt02 = $(__data.target).attr('data-dpt02-id');

            var tmpURL = APP.info.gURL+APP.URL_INFO.menu.ITEMS_MNG_ITEMS_LIST.link+'&item_cate_id_01='+dpt01+'&maxage=0';
            if( typeof dpt02==='string' && dpt02.length>0) tmpURL+='&item_cate_id_02='+dpt02
            window.location.href = tmpURL;

          });


          $(window).on('mouseup' , function(__e){
            if( $(__e.target).hasClass('dd-placeholder') || $(__e.target).attr('id')=='editCateOpt' || $(__e.target).closest('.cateInfoCont').length>0 || $(__e.target).closest('.panel-heading').length>0){
            }else{
              _deactiveListRow();
            }
          });

        }

        function _activeListRow(__data){
          console.log('__data: ' , __data);
          $(__data).find('>.dd-handle').addClass('active');
          scope.CategoryListCtrl.setCurRow({options:$(__data).attr('data-id') });
          scope.CategoryListCtrl.setShowCateInfo({options:'info'});
          scope.$apply();
        }

        function _deactiveListRow(){
          $('.dd-handle').removeClass('active');
          scope.CategoryListCtrl.setShowCateInfo({options:''});
          scope.$apply();
        }

        function _clearTimeout(){
          if(timer){
            clearTimeout(timer);
          }
        }


      }

    };
  }

})(window.jQuery, window.angular, window._, window.APP);
