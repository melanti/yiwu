<?php
/**
 * 시크릿키 처리 클래스
 * 
 */
defined('InCNBIZ') or exit('Access Invalid!');
class secretModel{
	/**
	 * 시크릿키 생성여부체크
	 *
	 * @param string $input  member_id
	 * @return boolean 
	 */
	public function isSecretKey(){
		$member_id = $_SESSION['member_id'];
		$secret_key = $_SESSION['secret_key'];
		
		$input = array(
					   'member_id' => $member_id, 
					   'secret_hash' => $secret_key
					   );		
		$table = "secret";
		$secrectkey = Db::getCount($table, $input);
		return $secrectkey['count']; 
	}	
	/**
	 * 특정 시크릿키 생성여부체크
	 *
	 * @param string $member_id  member_id
	 * @param string $secret_key  secret_key
	 * @return boolean
	 */
	public function isSecretKeyByType($member_id, $secret_key){
		//$member_id = $_SESSION['member_id'];
		//$secret_key = $_SESSION['secret_key'];
	
		$input = array(
				'member_id' => $member_id,
				'secret_hash' => $secret_key
		);
		$table = "secret";
		$secrectkey = Db::getCount($table, $input);
		return $secrectkey['count'];
	}	
	
	
	/**
	 * 시크릿키 리스트 생성
	 *
	 * @param string $member_id  member_id
	 * @param string $secret_key  secret_key
	 * @return boolean
	 */
	public function getSecretKeyByMemberId($member_id){
		$param	= array();
		$param['table']	= 'secret';
		$param['field']	= 'secret.secret_hash, secret.regist_date';
		$param['where']	= 'member_id = ' . $member_id;
		//$param['order']	= $condition['order'];		
		
		return Db::select($param);
	}
	/**
	 * 시크릿키 추가
	 *
	 * @param array $input  시크릿키 테이블에 저장시킬 데이터
	 * @return bool
	 */
	public function saveSecretkey($input){
		return Db::insert('secret',$input);
	}	
	/**
	 * 시크릿키 삭제
	 *
	 * @param string $id  member_id
	 * @return bool
	 */
	public function delSecretKey($input){
		$member_id = $input['member_id'];
		$secret_key = $input['secret_key'];		
		$where  = "where member_id = '$member_id' and secret_hash = '$secret_key'";
		return Db::delete('secret', $where);
	}
}