<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 交易관리 语言包
 */

//주문관리
$lang['order_manage']              = '주문관리';
$lang['order_help1']			   = '보기 - 주문 상세 내역 (주문상품 포함)	';
$lang['order_help2']			   = '취소 - 주문 취소하기(온라인 입금대기 혹은 미발송의 주문에 한해서 가능)';
$lang['order_help3']			   = '입금확인 - 구매자가 입금을 완료하고 관리자 확인 후 "입금확인" 버튼을 눌러 주십시오. ';
$lang['manage']                    = '관리';
$lang['store_name']                = '미니샵';
$lang['buyer_name']                = '구매자';
$lang['payment']                   = '결제방식';
$lang['order_number']              = '주문번호';
$lang['order_state']               = '주문상태';
$lang['order_state_new']           = '지불대기중';
$lang['order_state_pay']           = '발송대기중';
$lang['order_state_send']          = '입금확인대기중';
$lang['order_state_success']       = '완성';
$lang['order_state_cancel']        = '취소';
$lang['type']					   = '유형';
$lang['pended_payment']            = '확인대기중';//증가
$lang['order_time_from']           = '주문시간';
$lang['order_price_from']          = '주문금액';
$lang['cancel_search']             = '검사취소';
$lang['order_time']                = '주문시간';
$lang['order_total_price']         = '총주문금액';
$lang['order_total_transport']     = '운비';
$lang['miss_order_number']         = '주문번호 누락';

$lang['order_state_paid'] = '지불완성';
$lang['order_admin_operator'] = '시스템 관리자';
$lang['order_state_null'] = '무';
$lang['order_handle_history']	= '운영기록';
$lang['order_admin_cancel'] = '미지불，시스템 관리자 주문 취소';
$lang['order_admin_pay'] = '시스템 관리자 수취완성 확인';
$lang['order_confirm_cancel']	= '주문을 취소하시겠습니까?';
$lang['order_confirm_received']	= '입금확인하시겠습니까?';
$lang['order_change_cancel']	= '취소';
$lang['order_change_received']	= '입금확인';
$lang['order_log_cancel']	= '주문취소';

//주문상세
$lang['order_detail']              = '주문정보';
$lang['offer']                     = '세일';
$lang['order_info']                = '주문정보';
$lang['seller_name']               = '판매자';
$lang['pay_message']               = '지불메시지';
$lang['payment_time']              = '지불시간';
$lang['ship_time']                 = '발송시간';
$lang['complate_time']             = '완성된시간';
$lang['buyer_message']             = '구매자 부언';
$lang['consignee_ship_order_info'] = '수취인 및 발송정보';
$lang['consignee_name']            = '수취인성명';
$lang['region']                    = '주소';
$lang['zip']                       = '우편번호';
$lang['tel_phone']                 = '전화번호';
$lang['mob_phone']                 = '핸드폰번호';
$lang['address']                   = '상세주소';
$lang['ship_method']               = '배송방식';
$lang['ship_code']                 = '발송번호';
$lang['product_info']              = '상품정보';
$lang['product_type']              = '판촉';
$lang['product_price']             = '단가';
$lang['product_num']               = '수량';
$lang['product_shipping_mfee']     = '운비무료';
$lang['nc_promotion']				= '판촉프로모션';
$lang['nc_groupbuy_flag']			= '단체';
$lang['nc_groupbuy']				= '단체구매 프로모션';
$lang['nc_groupbuy_view']			= '보기';
$lang['nc_mansong_flag']			= '차다';
$lang['nc_mansong']					= '금액제 프로모션 프로모션';
$lang['nc_xianshi_flag']			= '세일';
$lang['nc_xianshi']					= '기간한정세일';
$lang['nc_bundling_flag']			= '조직';
$lang['nc_bundling']				= '세일패키지';


$lang['pay_bank_user']			= '입금자명';
$lang['pay_bank_bank']			= '입금은행';
$lang['pay_bank_account']		= '입금계좌';
$lang['pay_bank_num']			= '입금금액';
$lang['pay_bank_date']			= '입금날자';
$lang['pay_bank_extend']		= '기타';
$lang['pay_bank_order']			= '입금번호';

$lang['order_refund']			= '환불';
$lang['order_return']			= '반품';

$lang['order_show_system']				= '시스텀';
$lang['order_show_at']				= '에';
$lang['order_show_cur_state']			= '현재 주문상태';
$lang['order_show_next_state']		= '다음상태';
$lang['order_show_reason']			= '원인';