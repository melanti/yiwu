module.exports = {
  listItem: function(data, show) {
    var i,
    len_data = data.length;

    for(i=0; i< len_data; i++) {
      item = data[i];
      if(typeof item.item_thumbnail_url !== "undefined" && item.item_thumbnail_url !== null) {
        item.item_thumbnail_url += "?ver=" + new Date(item.updated_datetime).valueOf();
        //sails.log(item.item_thumbnail_url);
      }
    }

    return data;
  }, //listCate

  listCate: function(data, show) {
      var conv_data = [],
        chk_parent = false,
        prev_gc_id_1,
        prev_gc_id_2,
        idx_lv1 = 0,
        idx_lv2,
        idx_lv3,
        cate_status = 'activated',
        /*obj_sort = {
          "LRT": "reg_date",
          "LRB": "reg_date_desc",
          "LUT": "update_date",
          "LUB": "update_date_desc",
          "NAT": "title",
          "NAB": "title_desc"
        },*/
        i,
        len_data = data.length;

      for (i = 0; i < len_data; i++) {
        cate = data[i];

        if (prev_gc_id_1 !== cate.gc_id_1) {
          conv_data[idx_lv1] = {
            id: cate.gc_id_1,
            name: cate.gc_name_1,
            priority: cate.gc_sort_1,
            status: cate_status,
            children: []
          };

          idx_lv2 = 0;

          idx_lv1++;
        }

        // 1 4 | 1 4    | 1 4
        // 1 5 | 1 256  | 2 5
        if ((prev_gc_id_1 === cate.gc_id_1 && prev_gc_id_2 !== cate.gc_id_2) ||
            (prev_gc_id_1 !== cate.gc_id_1 && prev_gc_id_2 === cate.gc_id_2) ||
            (prev_gc_id_1 !== cate.gc_id_1 && prev_gc_id_2 !== cate.gc_id_2)) {
          conv_data[idx_lv1 - 1].children[idx_lv2] = {
            id: cate.gc_id_2,
            name: cate.gc_name_2,
            priority: cate.gc_sort_2,
            status: cate_status,
            children: []
          };
          idx_lv3 = 0;
          idx_lv2++;
        }


        if (cate.gc_id_3 !== null) {
          conv_data[idx_lv1-1].children[idx_lv2-1].children[idx_lv3] = {
            id: cate.gc_id_3,
            name: cate.gc_name_3,
            priority: cate.gc_sort_3,
            status: cate_status
          };

          idx_lv3++;
        }

        prev_gc_id_1 = cate.gc_id_1;
        prev_gc_id_2 = cate.gc_id_2;
      }

      return conv_data;
    } //listCate
};
