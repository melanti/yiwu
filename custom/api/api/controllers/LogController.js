module.exports = {
  get: function(req, res) {
    var target = "." + req.path.replace(/^\/log/, '/logs') + '.log',
      fs = require('fs');

    // 상위 directory 접근 제한
    if (req.path.indexOf("..") !== -1) {
      return res.badRequest();
    }

    fs.exists(target, function(exists) {
      if (!exists) {
        return res.notFound('The requested file does not exist.');
      }

      res.setHeader('Content-Type', 'text/plain');
      fs.createReadStream(target).pipe(res);
    });
  }
};
