var Q = require('q');
var SQLService = require('../../services/SQLService');
var ValidatorService = require('../../services/ValidatorService');
var is = require('is_js');


module.exports = {
    chkVerifyCode: function (req, res, key, verify_code, next) {
        var debug = require("./debug.js"),
        chk_vc_qry = "select verify_hash from sv_verify where verify_code=? and verify_hash=? limit 1",
        status = 501;

        DB.query(chk_vc_qry, [key, verify_code], function (err, results) {
            if (err) {
                status = 500;
                //return res.send(status, err);
                return res.send(status, debug.wrap(req, res, status, err));
            } else {
                if (results.length == 1) {
                    // remove verify code
                    var qry_rm_vc = "delete from sv_verify where verify_code=? and verify_hash=?";
                    DB.query(qry_rm_vc, [key, verify_code], function (err, results) {
                        if (err) {
                            status = 500;
                            return res.send(status, debug.wrap(req, res, status, err));
                            //return res.send(status, err);
                        } else {
                            next();
                        }
                    });
                } else {
                    err = {
                        code: "ER_VERIFY_ERROR",
                        index: 0
                    };
                    status = 400;
                    //return res.send(status, err);
                    return res.send(status, debug.wrap(req, res, status, err));
                }
            }
        });
    },
    chkVerifyCodeV2: function (req, res, user_id, secret, key, verify_code, next) {
        // check verify_code;
        var debug = require("./debug.js"),
        qry_chk_vc = "select VERIFY_HASH from VERIFY where VERIFY_CD=? and VERIFY_HASH=? limit 1",
        qry_chk_secret = "select MNGR_SECRET_HASH from MNGR mg " +
        "LEFT JOIN MNGR_SECRET ms ON mg.MNGR_NO=ms.MNGR_NO " +
        "where mg.MNGR_CD=? and ms.MNGR_SECRET_HASH=?";

        DB.query(qry_chk_vc, [key, verify_code], function (err, results) {
            if (err) {
                status = 500;
                //return res.send(status, err);
                return res.send(status, debug.wrap(req, res, status, err));
            } else {
                if (results.length == 1) {
                    DB.query(qry_chk_secret, [user_id, secret], function (err, results) {
                        if (err) {
                            status = 500;
                            //return res.send(status, err);
                            return res.send(status, debug.wrap(req, res, status, err));
                        } else {
                            if (results.length === 1) {
                                // remove verify code
                                var qry_rm_vc = "delete from VERIFY where VERIFY_CD=? and VERIFY_HASH=?";
                                DB.query(qry_rm_vc, [key, verify_code], function (err, results) {
                                    if (err) {
                                        status = 500;
                                        //return res.send(status, err);
                                        return res.send(status, debug.wrap(req, res, status, err));
                                    } else {
                                        return next();
                                    }
                                });
                            } else {
                                err = {
                                    code: "ER_SECRET_ERROR",
                                    index: 0
                                };
                                status = 400;
                                //return res.send(status, err);
                                return res.send(status, debug.wrap(req, res, status, err));
                            }
                        }
                    });
                } else {
                    err = {
                        code: "ER_VERIFY_ERROR",
                        index: 0
                    };
                    status = 400;
                    //return res.send(status, err);
                    return res.send(status, debug.wrap(req, res, status, err));
                }
            }
        });
    },


    chkVerifyCodeOnPromise:function(opt){

        var qry_chk_vc = "select verify_hash from sv_verify where verify_code=? and verify_hash=? limit 1";
        var qry_rm_vc = "delete from sv_verify where verify_code=? and verify_hash=?";
        var qry_chk_secret = [
            "select ms.secret_hash from sv_member mg " ,
            "JOIN sv_secret ms ON mg.member_id=ms.member_id " ,
            "where mg.member_id=? and ms.secret_hash=?"
        ].join('');
        var deferred = Q.defer();

        if( !opt ){
            deferred.reject({code:'ER_VERIFY_ERROR', message:'empty_parameter'});
        }else{
            Q.all([
                    ValidatorService.validatePromise(
                        {
                            data:opt,
                            validator:[
                                {key:'key' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
                                {key:'verify_code' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
                                {key:'account_uuid' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }},
                                {key:'secret' , validation:{ list:ValidatorService.VALIDATE_MANDATORY_VALUE }}
                            ]
                        }
                    ),
                    SQLService.queryOnPromise(qry_chk_vc, [opt.key , opt.verify_code]),
                    SQLService.queryOnPromise(qry_chk_secret, [opt.account_uuid , opt.secret])
                ]
            ).then(function(__results){

                return SQLService.queryOnPromise(qry_rm_vc, [opt.key , opt.verify_code]);
            }).then(function(){
                // sails.log.debug('chkVerifyCodeOnPromise opt: ' , opt);
                // sails.log.debug('chkVerifyCodeOnPromise SUCCESS');
                deferred.resolve({code:'SUCCESS', debug:{status:200}});
            }).catch(function(err){
                sails.log.debug('chkVerifyCodeOnPromise catch Error: ',err);
                deferred.reject({code:'ER_VERIFY_ERROR'});
            });

        }

        return deferred.promise;
    },

    chkFxmrAtvtnKey: function (req, res, activation_key, next) {
        var debug = require("./debug.js"),
        chk_ak_qry = "select FXMR_ACTVTN_NO as fxmr_actvtn_no, (DATE(FXMR_EXPIRE_DT) > DATE(NOW())) as expire_yn from FXMR_ACTVTN_KEY where FXMR_ACTVTN_KEY=?",
        status = 501;

        DB.query(chk_ak_qry, [activation_key], function (err, results) {
            if (err) {
                status = 500;
                //return res.send(status, err);
                return res.send(status, debug.wrap(req, res, status, err));
            } else {
                if (results.length == 1 && results[0].fxmr_actvtn_no) {
                    if(results[0].expire_yn === 1) {
                        next(results[0].fxmr_actvtn_no);
                    } else {
                        err = {
                            code: "ER_VERIFY_EXPIRE_ERROR",
                            index: 0
                        };
                        status = 400;
                        //return res.send(status, err);
                        return res.send(status, debug.wrap(req, res, status, err));
                    }
                } else {
                    err = {
                        code: "ER_VERIFY_ERROR",
                        index: 0
                    };
                    status = 400;
                    //return res.send(status, err);
                    return res.send(status, debug.wrap(req, res, status, err));
                }
            }
        });
    }
};
