<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>휴대폰결제</h3>
      <ul class="tab-base"><li><a class="current"><span>리스트</span></a></li></ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
        <ul>
            <li>모바일로 결제할 수 있는 방식입니다. 수정후 시동 및 기타 설정 하시면 됩니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <table class="table tb-type2">
    <thead>
      <tr class="thead">
        <th>지불방식</th>
        <th class="align-center">시동</th>
        <th class="align-center"><?php echo $lang['nc_handle'];?></th>
      </tr>
    </thead>
    <tbody>
        <?php if(!empty($output['mb_payment_list']) && is_array($output['mb_payment_list'])){ ?>
        <?php foreach($output['mb_payment_list'] as $k => $v) { ?>
      <tr class="hover">
        <td><?php echo $v['payment_name'];?></td>
        <td class="w25pre align-center"><?php echo $v['payment_state_text'];?></td>
        <td class="w156 align-center"><a href="<?php echo urlAdmin('mb_payment', 'payment_edit', array('payment_id' => $v['payment_id']));?>"><?php echo $lang['nc_edit']?></a></td>
      </tr>
      <?php } } ?>
    </tbody>
    <tfoot>
      <tr class="tfoot">
        <td colspan="15"></td>
      </tr>
    </tfoot>
  </table>
</div>
