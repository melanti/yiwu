<?php
/**
 * member info
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');
class member_accountControl extends mobileHomeControl{

    public function __construct() {
        parent::__construct();
    }

    public function get_mobile_infoOp() {
        output_data(array('state' => false,'mobile'=>null));
    }


    public function get_paypwd_infoOp() {
        output_data(array('state' => false));
    }


}
