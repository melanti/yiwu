(function($, angular, _, APP){

  if( $(document.body).attr('data-page-id')=='brand-registration' ){
    bootstrapInfo();
  }

  function bootstrapInfo(){
    console.log('[[bootstrapBrandRegistration:]]');

    angular.module('App')
      .directive( 'brandRegistration', ['XHR', 'PubSub', 'URL_INFO', brandRegistrationDirective] );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( (parseInt(APP.info.user.auth.brand,10)>=1 && window.location.href.indexOf('confirmation')!==-1) || (parseInt(APP.info.user.auth.brand,10)==2 && (window.location.href.indexOf('registration')!==-1) || (window.location.href.indexOf('modification')!==-1)) ){
          //if( parseInt(APP.info.user.auth.brand,10)>=2 ){

            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }



        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }

  function brandRegistrationDirective(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: ['$scope', '_',  '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'BrandTableManager', APP.modules.ctrl.brandRegistrationCtrl],
      controllerAs: 'brandRegistrationCtrl',
      bindToController: {
        brandId: '@',
        brandName: '@',
        brandStatus: '@',
        brandType: '@'
      },
      link: function (scope, elem, attrs, ctrl, transclude) {
      },
      templateUrl: 'brand-registration.html'
    };
  }


})(window.jQuery, window.angular, window._, window.APP);
