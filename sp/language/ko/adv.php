<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 导航及全局
 */
$lang['adv_index_manage']	= '광고관리';
$lang['adv_manage']	= '광고';
$lang['adv_add']	= '광고추가';
$lang['ap_manage']	= '광고위치';
$lang['ap_add']	    = '광고위치추가';
$lang['adv_change']	= '광고수정';
$lang['ap_change']	= '광고위치수정';
$lang['adv_pic']	= '이미지';
$lang['adv_word']	= '텍스트';
$lang['adv_slide']	= '슬라이드';
$lang['adv_edit']	= '편집';
$lang['adv_change']	= '수정';
$lang['adv_pix']	= '픽셀';
$lang['adv_edit_support'] = '허용되는 이미지 유형:';
$lang['adv_cache_refresh'] = '캐쉬비우기';
$lang['adv_cache_refresh_done'] = '광고 캐쉬 비우기 성공';
/**
 * 广告
 */
$lang['adv_name']	         = '광고이름';
$lang['adv_ap_id']	         = '소속광고위치';
$lang['adv_class']	         = '유형';
$lang['adv_start_time']	     = '시작시간';
$lang['adv_end_time']	     = '마감시간';
$lang['adv_all']	         = '전체';
$lang['adv_overtime']	     = '만기됨';
$lang['adv_not_overtime']	 = '미만기';
$lang['adv_img_upload']	     = '이미지 업로드';
$lang['adv_url']	         = '링크주소';
$lang['adv_url_donotadd']	 = '링크주소에 http://는 포함하지 않습니다.';
$lang['adv_word_content']	 = '텍스트내용';
$lang['adv_max']	         = '최대';
$lang['adv_byte']	         = '자';
$lang['adv_slide_upload']	 = '슬라이드 이미지 업로드';
$lang['adv_slide_sort']	     = '슬라이드 정렬';
$lang['adv_slide_sort_role'] = '작은순으로 정렬';
$lang['adv_ap_select']       = '광고위치 선택';
$lang['adv_search_from']     = '발표시간';
$lang['adv_search_to']	     = '부터';
$lang['adv_click_num']	     = '클릭수ˇ';
$lang['adv_admin_add']	     = '관리자 추가';
$lang['adv_owner']	         = '광고주';
$lang['adv_wait_check']	     = '광고 심사대기';
$lang['adv_flash_upload']	 = 'Flash파일 업로드';
$lang['adv_please_upload_swf_file']	 = 'swf유형의 파일을 올려주세요';
$lang['adv_help1']			 = '광고 추가시 소속 광고위치를 지정하세요';
$lang['adv_help2']			 = '광고위치 코드복사로 프론트 페이지에 붙여넣어주세요.';
$lang['adv_help3']			 = '점장은 현금으로 광고를 구매할 수 있습니다.';
$lang['adv_help4']			 = '점장이 구매한 광고 심사';
$lang['adv_help5']			 = '보기를 눌러 심사에 대한 조작을 할 수 있습니다.';

/**
 * 광고위치
 */
$lang['ap_name']	         = '이름';
$lang['ap_intro']	         = '소개';
$lang['ap_class']	         = '유형';
$lang['ap_show_style']	     = '노출방식';
$lang['ap_width']	         = '넓이/자수';
$lang['ap_height']	         = '높이';
$lang['ap_price']	         = '단가(금액/월)';
$lang['ap_show_num']	     = '노출중';
$lang['ap_publish_num']	     = '발표완료';
$lang['ap_is_use']	         = '시동여부';
$lang['ap_slide_show']	     = '슬라이드쇼';
$lang['ap_mul_adv']	         = '다수광고노출';
$lang['ap_one_adv']	         = '단일광고노출';
$lang['ap_use']	             = '시동';
$lang['ap_not_use']	         = '미시동';
$lang['ap_get_js']	         = '코드복사';
$lang['ap_use_s']	         = '시동';
$lang['ap_not_use_s']	     = '미시동';
$lang['ap_price_name']	     = '단가';
$lang['ap_price_unit']	     = '원/월';
$lang['ap_allow_mul_adv']	 = '다수의 광고를 발표하여 랜덤으로 노출합니다.';
$lang['ap_allow_one_adv']	 = '하나의 광고만 발표할 수 있습니다.';
$lang['ap_width_l']	         = '넓이';
$lang['ap_height_l']	     = '높이';
$lang['ap_word_num']	     = '자수';
$lang['ap_select_showstyle'] = '본 광고위치 광고의 형식을 선택하세요';
$lang['ap_click_num']	     = '클릭수';
$lang['ap_help1']			 = '광고위치 추가후 시동여부를 선택하세요';
/**
 * 提示信息
 */
$lang['adv_can_not_null']	    = '이름을 입력하세요';
$lang['must_select_ap']	        = '반드시 하나의 광고위치는 선택하셔야합니다.';
$lang['must_select_start_time'] = '시작시간은 반드시 선택하셔야합니다.';
$lang['must_select_end_time']	= '마감시간은 반드시 선택하셔야합니다.';
$lang['must_select_ap_id']		= '광고위치 선택하세요';
$lang['textadv_null_error']		= '텍스트 내용을 입력하세요';
$lang['slideadv_null_error']	= '슬라이드 이미지를 업로드하세요';
$lang['slideadv_sortnull_error']	= '슬라이드 정렬을 입력하세요';
$lang['flashadv_null_error']	= 'FLASH파일을 업로드하세요';
$lang['picadv_null_error']		= '이미지를 업로드하세요';
$lang['wordadv_toolong']	    = '광고의 텍스트 정보가 너무 깁니다.';
$lang['goback_adv_manage']	    = '돌아가기';
$lang['resume_adv_add']	        = '계속 광고 추가';
$lang['resume_ap_add']	        = '계속 광고위치 추가';
$lang['adv_add_succ']	        = '추가성공';
$lang['adv_add_fail']	        = '추가실패';
$lang['ap_add_succ']	        = '추가성공';
$lang['ap_add_fail']	        = '광고위치 추가 실패';
$lang['goback_ap_manage']	    = '돌아가기';
$lang['ap_stat_edit_fail']	    = '광고위치 상태 수정 실패';
$lang['ap_del_fail']	        = '광고위치 삭제 실패';
$lang['ap_del_succ']	        = '광고위치 삭제 성공, 코드복사를 사용한 부분을 삭제해주세요';
$lang['adv_del_fail']	        = '광고 삭제 실패';
$lang['adv_del_succ']	        = '광고 삭제 성공';
$lang['ap_can_not_null']	    = '광고위치이름을 입력하세요';
$lang['adv_url_can_not_null']	    = '광고 링크주소를 입력하세요';
$lang['ap_price_can_not_null']	= '광고위치 가격을 입력하세요';
$lang['ap_input_digits_pixel']		= '픽셀값을 입력하세요(정수)';
$lang['ap_input_digits_words']		= '텍스트수를 입력하세요(정수)';
$lang['ap_default_word_can_not_null'] = '기본 텍스트를 입력하세요';
$lang['adv_start_time_can_not_null']	= '광고 시작시간을 입력하세요';
$lang['adv_end_time_can_not_null']	= '광고 마감시간을 입력하세요';
$lang['ap_w&h_can_not_null']	= '광고위치 넓이와 높이를 입력하세요';
$lang['ap_display_can_not_null']	= '광고위치 노출방식을 선택하세요';
$lang['ap_wordnum_can_not_null']	= '광고위치 자수를 이력하세요';
$lang['ap_price_must_num']	    = '광고위치 가격은 숫자로 입력하세요';
$lang['ap_width_must_num']	    = '광고위치 넓이는 숫자로 입력하세요';
$lang['ap_wordwidth_must_num']	= '광고위치 자수는 숫자로 입력하세요';
$lang['ap_height_must_num']	    = '광고위치 높이는 숫자로 입력하세요';
$lang['ap_change_succ']	        = '광고위치 정보 수정 성공';
$lang['ap_change_fail']	        = '광고위치 정보 수정 실패';
$lang['adv_change_succ']	    = '광고정보 수정 성공';
$lang['adv_change_fail']	    = '광고정보 수정 실패';
$lang['adv_del_sure']	        = '정말 선택된 광고의 모든 정보를 삭제하시겠습니까?';
$lang['ap_del_sure']	        = '정말 선택된 광고위치의 모든 정보를 삭제하시겠습니까?';
$lang['default_word_can_not_null'] = '광고위치 기본 텍스트를 입력하세요';
$lang['default_pic_can_not_null']  = '광고위치 기본 이미지를 업로드하세요';
$lang['must_input_all']  = '(모든 내용을 입력후 확인을 눌러주세요!)';
$lang['adv_index_copy_to_clip']	= 'JavaScript 혹은 PHP코드를 해당 템플릿에 붙여넣으세요';

$lang['check_adv_submit']  = '광고 신청 심사';
$lang['check_adv_yes']     = '심사통과';
$lang['check_adv_no']      = '미통과';
$lang['check_adv_no2']     = '미통과';
$lang['check_adv_type']    = '유형';
$lang['check_adv_buy']     = '구매';
$lang['check_adv_order']   = '예약';
$lang['check_adv_change']  = '내용수정';
$lang['check_adv_view']    = '보기';
$lang['check_adv_nothing'] = '심사 대기인 광고가 없습니다.';
$lang['check_adv_chart']   = '광고 클릭수 통계표';
$lang['adv_chart_searchyear_input']  = ' 검색 년도 입력:';
$lang['adv_chart_year']    = '년';
$lang['adv_chart_years_chart']    = '년의 광고 클릭 통계표';
$lang['ap_default_pic']    = '광고위치 기본 이미지:';
$lang['ap_default_pic_upload']    = '광고위치 기본 이미지 업로드:';
$lang['ap_default_word']   = '광고위치 기본 텍스트';
$lang['ap_show_defaultpic_when_nothing']    = '노출할 광고가 없으면 기본 이미지를 노출합니다.';
$lang['ap_show_defaultword_when_nothing']    = '노출할 광고가 없으면 기본 텍스트를 노출합니다.';

$lang['goback_to_adv_check']    = '돌아가기';
$lang['adv_check_ok']      = '광고 심사 성공';
$lang['adv_check_failed']    = '광고 심사 실패';
$lang['return_goldpay']    = '광고를 구매한 금액 환불';
$lang['adv_chart_nothing_left']    = '본 광고는 ';
$lang['adv_chart_nothing_right']    = '년의 클릭율 정보가 없습니다.';
