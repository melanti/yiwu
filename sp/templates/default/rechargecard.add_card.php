<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<style>
.switch-tab-title {font-size:14px; margin-right:15px; }
</style>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>충전카드</h3>
      <ul class="tab-base">
        <li><a href="<?php echo urlAdmin('rechargecard', 'index'); ?>"><span>리스트</span></a></li>
        <li><a href="javascript:void(0);" class="current"><span>추가</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>

  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
          <ul>
            <li>플랫폼에서 발표하는 카드 종류는 3가지;</li>
            <li>1. 생성수, 앞단 고유번호를 입력하여 생성수만큼 자동으로 32자리 카드번호가 생성;</li>
            <li>2. 파일형식으로 업로드하여 추가;</li>
            <li>3. 수동입력, 매행에 한개의 카드번호;</li>
            <li>충전카드 번호는 50자의 영문과 숫자 조합으로 생성합니다.</li>
            <li>만약 입력하신 충전카드 번호와 기존에 존재하는 카드번호가 충돌이 되면 자동으로 무시합니다.</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>

  <form method="post" enctype="multipart/form-data" name="form_add" id="form_add">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
        <tr>
          <td colspan="2" class="required"><label class="validation">생성방식선택:</label></td>
        </tr>
      </tbody>

      <tbody>
        <tr class="noborder">
          <td colspan="2" class="required">
            <label class="switch-tab-title">
              <input type="radio" name="type" value="0" checked="checked" class="tabswitch" />
              자동생성
            </label>
            <label class="switch-tab-title">
              <input type="radio" name="type" value="1" class="tabswitch" />
              파일업로드
            </label>
            <label class="switch-tab-title">
              <input type="radio" name="type" value="2" class="tabswitch" />
              수동입력
            </label>
          </td>
        </tr>

        <tr class="noborder tabswitch-target">
          <td class="vatop rowform">
            생성수：
            <input type="text" class="txt" name="total" style="width:40px;" />
            앞단：
            <input type="text" class="txt" name="prefix" style="width:130px;" />
          </td>
          <td class="vatop tips">생성수는 1~9999이내로 입력; 앞단 고유번호는 16자 이내의 영문 및 숫자의 조합으로 설정할 수 있습니다.</td>
        </tr>

        <tr class="noborder tabswitch-target" style="display:none;">
          <td class="vatop rowform">
            <span class="type-file-box">
              <input type="text" name="textfile" id="textfile" class="type-file-text" />
              <input type="button" name="button" id="button" value="" class="type-file-button" />
              <input type="file" name="_textfile" class="type-file-file" size="30" hidefocus="true" onchange="$('#textfile').val(this.value);" />
            </span>
          </td>
          <td class="vatop tips">카드번호 파일 업롣, 파일은 txt형식, 매행 한개의 카드번호; 카드번호는 영문 및 숫자의 조합으로 50자 이내로 설정; 규격에 맞지 않는 카드번호는 자동으로 무시합니다.</td>
        </tr>

        <tr class="noborder tabswitch-target" style="display:none;">
          <td class="vatop rowform">
            <textarea name="manual" style="width:300px;height:150px;"></textarea>
          </td>
          <td class="vatop tips">카드번호 입력, 매행 한개 카드번호; 카드번호는 영문 및 숫자의 조합으로 50자 이내로 입력; 규격에 맞지 않는 카드번호는 자동으로 무시합니다.</td>
        </tr>

      </tbody>

      <tbody>
        <tr>
          <td colspan="2" class="required"><label class="validation">금액(원):</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input class="txt" type="text" name="denomination" style="width:150px;" /></td>
          <td class="vatop tips">금액을 입력하세요, 금액은 10000000을 초과할 수 없습니다.</td>
        </tr>

        <tr>
          <td colspan="2" class="required"><label>구분표식:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input class="txt" type="text" name="batchflag" /></td>
          <td class="vatop tips">20자 이내의 "구분표식"을 입력할 수 있습니다, "구분표식"은 제품에 따라 구분으로 표식할 수 있습니다.</td>
        </tr>

      </tbody>

      <tfoot>
        <tr class="tfoot">
          <td colspan="2" ><a href="javascript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript">
$(function(){

$('.tabswitch').click(function() {
    var i = parseInt(this.value);
    $('.tabswitch-target').hide().eq(i).show();
});

$("#submitBtn").click(function(){
    $("#form_add").submit();
});

jQuery.validator.addMethod("r0total", function(value, element) {
    var v = parseInt(value);
    return $(":radio[name='type']:checked").val() != '0' || (value == v && v >= 1 && v <= 9999);
}, "생성수는 반드시 1~9999이내의 숫자로 입력하세요");

jQuery.validator.addMethod("r0prefix", function(value, element) {
    return $(":radio[name='type']:checked").val() != '0' || this.optional(element) || /^[0-9a-zA-Z]{0,16}$/.test(value);
}, "앞단 고유번호는 16자이내로 영문 및 숫자의 조합으로 설정하세요");

jQuery.validator.addMethod("r1textfile", function(value, element) {
    return $(":radio[name='type']:checked").val() != '1' || value;
}, "TXT파일로 된 충전카드 번호를 업로드하세요");

jQuery.validator.addMethod("r2manual", function(value, element) {
    return $(":radio[name='type']:checked").val() != '2' || value;
}, "충전카드 번호를 입력하세요");

$("#form_add").validate({
    errorPlacement: function(error, element){
        error.appendTo(element.parents('tbody').find('tr:first td:first'));
    },
    rules : {
        denomination : {
            required : true,
            min: 0.01,
            max: 10000000
        },
        batchflag : {
            maxlength: 20
        },
        total : {
            r0total : true
        },
        prefix : {
            r0prefix : true
        },
        textfile : {
            r1textfile : true
        },
        manual : {
            r2manual : true
        }
    },
    messages : {
        denomination : {
            required : '금액을 입력하세요',
            min : '금액은 최소 0.01입니다.',
            max: '금액은 최대 10000000입니다.'
        },
        batchflag : {
            maxlength: '20자 이내의 일괄표식을 입력하세요'
        }
    }
});
});
</script>
