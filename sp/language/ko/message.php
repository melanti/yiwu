<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 邮件模板index
 */
$lang['mailtemplates_index_desc']		= '설명';


/**
 * 邮件模板수정
 */
$lang['nc_current_edit']			= '편집중';
$lang['mailtemplates_edit_no_null']		= '번호를 입력하세요';
$lang['mailtemplates_edit_title_null']	= '제목을 입력하세요';
$lang['mailtemplates_edit_content_null']	= '내용을 입력하세요';
$lang['mailtemplates_edit_succ']		= '통지 템플릿 업데이트 성공';
$lang['mailtemplates_edit_fail']		= '통지 템플릿 업데이트 실패';
$lang['mailtemplates_edit_code_null']	= '코드를 입력하세요';
$lang['mailtemplates_edit_title']		= '제목';
$lang['mailtemplates_edit_content']		= '내용';
/**
 * 消息模板수정
 */
$lang['mailtemplates_msg_edit_no_null']	= '쪽지 번호를 입력하세요';
$lang['mailtemplates_msg_edit_content_null']	= '쪽지 내용을 입력하세요';
$lang['mailtemplates_msg_edit_content']	= '쪽지내용';