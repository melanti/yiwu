<?php
/**
 * 前台登录 退出操作
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class seccodeControl extends mobileHomeControl {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * 登录
	 */
	public function makecodeOp(){
		$refererhost = parse_url($_SERVER['HTTP_REFERER']);
		$refererhost['host'] .= !empty($refererhost['port']) ? (':'.$refererhost['port']) : '';

		$seccode = makeSeccode($_GET['k']);
		$type="50,120";

		@header("Expires: -1");
		@header("Cache-Control: no-store, private, post-check=0, pre-check=0, max-age=0", FALSE);
		@header("Pragma: no-cache");
		$tp = explode(",",$type);
		$code = new seccode();
		$code->code = $seccode;
		$code->width = !empty($tp['1']) ? $tp['1'] : 90;
		$code->height = !empty($tp['0']) ? $tp['0'] : 26;
		$code->background = 1;
		$code->adulterate = 1;
		$code->scatter = '';
		$code->color = 1;
		$code->size = 0;
		$code->shadow = 1;
		$code->animator = 0;
		$code->datapath =  BASE_DATA_PATH.'/resource/seccode/';
		$code->display();
	}

	/**
	 * 登录
	 */
	public function makecodekeyOp(){
		    $act = $act ? $act : $_GET['act'];
		    $op = $op ? $op : $_GET['op'];
		    if (C('captcha_status_login')){
		        $cdata = substr(md5(SHOP_SITE_URL.$act.$op),0,8);
		    }
		output_data(array("codekey"=>$cdata));
	}

	public function getRandom($param){
		$str="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$key = "";
		for($i=0;$i<$param;$i++)
		{
			$key .= $str{mt_rand(0,32)};
		}
		return $key;
	}

	/**
	 * AJAX验证
	 *
	 */
	public function checkOp(){
		if (checkSeccode($_POST['codekey'],$_POST['captcha'])){
			exit('true');
		}else{
			exit('false');
		}
	}
}
