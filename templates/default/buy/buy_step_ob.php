<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncc-main">
  <div class="ncc-title">
    <h3>订单已生成<em style="margin-left:10px; font-size:16px; font-weight:bold; color:#5bb75b;">付款单号：<?php echo $_GET['pay_sn'];?></em></h3>
    <h5>您的订单已生成，可在线上、线下银行转账即可。（注：转账时请注明您的付款单号）</h5>
  </div>
  <div class="ncc-receipt-info mb30">
  <div class="ncc-finish-a" style="margin-left:35px;">您的订单已生成！订单金额为<em>￥<?php echo $_GET['pay_amount'];?></em>。</div>
  <div class="ncc-finish-a" style="margin-left:35px; margin-top:0px;">转账完成后需要点击下方“我已付款”按钮来完成支付。</div>
  <div class="ncc-finish-a" style="margin-left:35px; margin-top:10px; height:auto;">
  	<table border="1" bordercolor="#ccc" width="520">
  		<tr>
  			<th style="padding:5px; font-weight:bold;">开户名</th>
  			<th style="padding:5px;">大连韩中友谊电子商务有限公司</th>
  		</tr>
  		<tr>
  			<th style="padding:5px; font-weight:bold;">开户银行</th>
  			<td style="padding:5px;"><?php echo $output['ob_data']['offbank_name'];?></td>
  		</tr>
  		<tr>
  			<th style="padding:5px; font-weight:bold;">账号</th>
  			<td style="padding:5px;"><?php echo $output['ob_data']['offbank_num'];?></td>
  		</tr>
  	</table>
  </div>
  <div class="ncc-finish-b">可通过用户中心<a href="<?php echo SHOP_SITE_URL?>/index.php?act=member_order">已买到的商品</a>查看订单状态。</div>
  <div class="ncc-finish-c mb30">
  <?php if($output['pay_info']['api_pay_state']==2):?>
  <a href="javascript:void(0)" class="ncc-btn-mini ncc-btn-gray mr15"><i class="icon-check"></i>付款确认中</a>
<?php else:?>
  <a href="javascript:void(0)" class="ncc-btn-mini ncc-btn-red mr15" onclick="ajax_get_confirm('如果您已经银行转账的情况下，点击确定按钮来完成支付。', 'index.php?act=member_order&op=change_state&state_type=order_offbank&order_id=<?php echo $_GET['pay_sn']; ?>');"><i class="icon-check"></i>我已付款</a>

<?php endif;?>
  <a href="<?php echo SHOP_SITE_URL?>" class="ncc-btn-mini ncc-btn-green mr15"><i class="icon-shopping-cart"></i>继续购物</a>
  <a href="<?php echo SHOP_SITE_URL?>/index.php?act=member_order" class="ncc-btn-mini ncc-btn-acidblue"><i class="icon-file-text-alt"></i>查看订单</a>
  </div>
  </div>
</div>
