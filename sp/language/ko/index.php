<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * 修改密码
 */
$lang['index_modifypw_repeat_error']		= '두번 입력한 비밀번호가 잃치하지 않습니다.';
$lang['index_modifypw_admin_error']			= '관리자 정보 오류';
$lang['index_modifypw_oldpw_error']			= '기존 비밀번호 오류';
$lang['index_modifypw_success']				= '비밀번호 설정 성공';
$lang['index_modifypw_fail']				= '비밀번호 설정 실패';
$lang['index_modifypw_oldpw']				= '기존 비밀번호';
$lang['index_modifypw_newpw']				= '새로운 비밀번호';
$lang['index_modifypw_newpw2']				= '비밀번호 확인';