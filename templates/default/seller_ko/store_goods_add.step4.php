
<ul class="add-goods-step">
  <li><i class="icon icon-list-alt"></i>
    <h6>STEP.1</h6>
    <h2>카테고리 선택</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-edit"></i>
    <h6>STEP.2</h6>
    <h2>상세내용 입력</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-camera-retro "></i>
    <h6>STEP.3</h6>
    <h2>이미지 업로드</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li class="current"><i class="icon icon-ok-circle"></i>
    <h6>STEP.4</h6>
    <h2>등록 완료</h2>
  </li>
</ul>
<div class="alert alert-block hr32">
  <h2><i class="icon-ok-circle mr10"></i>축하합니다, 상품등록이 완료되었습니다！ &nbsp;&nbsp;<?php if (C('goods_verify')) {?><?php }?></h2>
  <div class="hr16"></div>
  <div>
    <?php if ($output['allow_gift']) {?>
    <a class="ncsc-btn ncsc-btn-green ml30" href="<?php echo urlShop('store_goods_online', 'add_gift', array('commonid' => $_GET['commonid'], 'ref_url' => urlShop('store_goods_online', 'index')));?>"><i class="icon-gift"></i>해당상품에 증점품 추가</a>
    <?php }?>
    <?php if ($output['allow_combo']) {?>
    <a class="ncsc-btn ncsc-btn-orange ml10" href="<?php echo urlShop('store_goods_online', 'add_combo', array('commonid' => $_GET['commonid'], 'ref_url' => urlShop('store_goods_online', 'index')));?>"><i class="icon-thumbs-up "></i>해당상품 추천상품에 추가</a></div>  
    <?php }?>
  <div class="hr16"></div>
  <strong>
    <a class="ml30" href="<?php echo urlShop('goods', 'index', array('goods_id'=>$output['goods_id']));?>">해당 상품 페이지 보러가기&gt;&gt;</a>
    <a class="ml30" href="<?php echo urlShop('store_goods_online', 'edit_goods', array('commonid' => $_GET['commonid'], 'ref_url' => urlShop('store_goods_online', 'index')));?>">해당상품 편집관리&gt;&gt;</a>    
  </strong>  
  <div class="hr16"></div>
  <h4 class="ml10">Continue : </h4>
  <ul class="ml30">
    <li>1. 계속해서 &quot; <a href="<?php echo urlShop('store_goods_add', 'index');?>"> 상품 등록하기</a>&quot;</li>
    <li>2. 관리자센터의  &quot;<a href="<?php echo urlShop('store_goods_online', 'index');?>">진열상품관리</a>&quot;</li>
    <li>3. 쇼핑몰상품 &quot; <a href="<?php echo urlShop('store_activity', 'store_activity');?>">이벤트관리</a> &quot;</li>
  </ul>
</div>
