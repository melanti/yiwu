<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
  <a href="javascript:void(0)" class="ncsc-btn ncsc-btn-green" nc_type="dialog" dialog_title="주소추가" dialog_id="my_address_add"  uri="index.php?act=store_deliver_set&op=daddress_add" dialog_width="550" title="주소추가">주소추가</a></div>
<div></div>
<table class="ncsc-default-table" >
  <thead>
    <tr>
      <th class="w70">기본주소지</th>
      <th class="w90">발송자</th>
      <th class="tl">발송지</th>
      <th class="w150">전화</th>
      <th class="w110">편집</th>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($output['address_list']) && is_array($output['address_list'])){?>
    <?php foreach($output['address_list'] as $key=>$address){?>
    <tr class="bd-line">
      <td>
        <label for="is_default_<?php echo $address['address_id'];?>"><input type="radio" id="is_default_<?php echo $address['address_id'];?>" name="is_default" <?php if ($address['is_default'] == 1) echo 'checked';?> value="<?php echo $address['address_id'];?>">
        기본설정</label>
      </td>
      <td><?php echo $address['seller_name'];?></td>
      <td class="tl"><?php echo $address['address'];?>&nbsp;<?php echo $address['area_info'];?></td>
      <td><span class="tel"><?php echo $address['telphone'];?></span> <br/>
      <td class="nscs-table-handle"><span><a href="javascript:void(0);" dialog_id="my_address_edit" dialog_width="640" dialog_title="주소편집" nc_type="dialog" uri="index.php?act=store_deliver_set&op=daddress_add&address_id=<?php echo $address['address_id'];?>" class="btn-blue"><i class="icon-edit"></i>
        <p>편집</p>
        </a></span><span> <a href="javascript:void(0)" onclick="ajax_get_confirm('정말 삭제 하시겠습니까?', 'index.php?act=store_deliver_set&op=daddress_del&address_id=<?php echo $address['address_id'];?>');" class="btn-red"><i class="icon-trash"></i>
        <p>삭제</p>
        </a></span></td>
    </tr>
    <?php }?>
    <?php }else{?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php }?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">&nbsp;</td>
    </tr>
  </tfoot>
</table>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script> 
<script>
$(function (){
	$('input[name="is_default"]').on('click',function(){
		$.get('index.php?act=store_deliver_set&op=daddress_default_set&address_id='+$(this).val(),function(result){})
	});
});
</script> 
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
    function execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
 
                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById("postcode1").value = data.postcode1;
                document.getElementById("postcode2").value = data.postcode2;
                document.getElementById("roadAddress").value = data.roadAddress;
                document.getElementById("jibunAddress").value = data.jibunAddress;
                document.getElementById("addressEnglish").value = data.addressEnglish;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById("address").focus();
            }
        }).open();
    }
</script>
