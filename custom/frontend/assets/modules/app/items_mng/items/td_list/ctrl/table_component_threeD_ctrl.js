
(function($, angular, _, APP){

  APP.modules.ctrl.tableComponentThreeDModalCtrl = tableComponentThreeDModalCtrl;

  //tableComponentThreeDModalCtrl
  function tableComponentThreeDModalCtrl($scope, _, $timeout, XHR, PubSub, ThreeDTableManager, API_URL_INFO, COMMON_UTIL){
    var vm = this,
      threeDTableManager = ThreeDTableManager.exports,
      pubsubBootstrap,
      btComplete,
      bootstrapCtrlStrArr=[];

    vm.bootstrapCtrl;

    vm.curModel;
    vm.curIdx;

    vm._state={
      tableData:threeDTableManager.getTableData(),
      bootstrapTableCtrl:{
        complete:{
          //tableOptionCtrl:false,
          //tableBodyCtrl:false,
          //paginationCtrl:false,
        }
      }
    };

    vm.exports={
      COUNT_RANGE:threeDTableManager.COUNT_RANGE,
      getCurData:function(){return vm.curModel},
      setCurData:function(__data){vm.curModel = __data;},
      getCurIdx:function(){return vm.curIdx},
      setCurIdx:function(__idx){vm.curIdx = __idx;},
      sendData:_sendData,
      shopPopup:_shopPopup,
      hidePopup:_hidePopup,
      setSearchOpt:_setSearchOpt,
      deleteRow:_deleteRow,
      updatePage:_updatePage
    };

    _init();

    function _shopPopup(){
      _resize();
      $('.td_popup_wrapper').css({display:'block'});
      _resize();
    }

    function _hidePopup(){
      _disposeCurData();
      $('.td_popup_wrapper').css({display:'none'});
    }

    function _disposeCurData(){
      vm.curModel=null;
      vm.curIdx = null;
      $('.clickable-row').removeClass('on_td');
      $('.table_cont').scrollTop(0);
    }

    function _sendData(){
      if(!vm.curModel){
        alert('3D 의상을 선택해주세요.');
        return false;
      }
      $(window).trigger('ATTACH_3D_DATA', {data:[vm.curModel]});
      _hidePopup();
    }

    function _resize(){
      var tmpTop = parseInt( ( $(window).height()-$('.td_popup_wrapper').outerHeight() )/2,10 );
      var tmpLeft = parseInt( ( $(window).width()-$('.td_popup_wrapper').outerWidth() )/2,10 );
      $('.td_popup_wrapper').css({top:tmpTop+'px', left:tmpLeft+'px'});
    }

    function _resize(){
      var tmpTop = parseInt( ( $(window).height()-$('.td_popup_wrapper').outerHeight() )/2,10 );
      var tmpLeft = parseInt( ( $(window).width()-$('.td_popup_wrapper').outerWidth() )/2,10 );
      $('.td_popup_wrapper').css({top:tmpTop+'px', left:tmpLeft+'px'});
    }

    function _init(){

      console.log('component init');
      if((vm.bootstrapCtrl)){
        bootstrapCtrlStrArr = (vm.bootstrapCtrl).split(',');
      }
      bootstrapCtrlStrArr.forEach(function(element, index, array){
        vm._state.bootstrapTableCtrl.complete[element] =false;
      });

      btComplete = vm._state.bootstrapTableCtrl.complete;

      threeDTableManager.init();

      $(window).on('OPEN_3D_POPUP', function(){
        console.log('OPEN_3D_POPUP::');
        _shopPopup();
      });

      $(window).resize(function(){
        _resize();
      });

      $scope.$on('$destroy', function () {
        PubSub.unsubscribe(pubsubBootstrap);
      });
      pubsubBootstrap = PubSub.subscribe('tableComponent:bootstrap', _listenBootstrapState);
    }

    function _listenBootstrapState(topic, data){
      var chk = true;
      btComplete = _.extend({}, btComplete, data);

      _.values(btComplete).forEach(function(element, index, array){
        if(element===false) chk =false;
      });

      if( chk ){
        //console.log('All bootstrap');
        PubSub.unsubscribe(pubsubBootstrap);
        PubSub.trigger('tableComponent:bootstrap:complete',{success:true});
        _updatePage();
      }
    }



    function _setSearchOpt(__options){
      //console.log('tableComponentThreeDCtrl _setSearchOpt: ' , __options);
      _.each(__options , function(value, key, list){
        vm._state.tableData.options[key] = value;
      });
    }

    function _updatePage(__options){
      var tmpPage = (__options&&__options.start>=0)?__options.start:threeDTableManager.getTableStateInfo('start');
      var tmpViewRange = (__options&&__options.count)?__options.count:threeDTableManager.getTableStateInfo('count');
      var stD;
      var endD;

      //period_reg_start ,
      if( vm._state.tableData.options.period_unit=='custom' ){
        endD = new Date(vm._state.tableData.options.period_reg_end.replace(/-/g, "/")).getTime();
        stD = new Date(vm._state.tableData.options.period_reg_start.replace(/-/g, "/")).getTime();
        if( stD>endD ){
          alert( '종료기간이 시작기간보다 이전입니다.' ); //'종료기간이 시작기간보다 이전입니다.'
          return false;
        }
      }

      threeDTableManager
        .updateTableData({start:tmpPage, count:tmpViewRange})
        .then(
          function(__sucData){
            APP.modules.ctrl.tablePaginationBasicCtrl.beforeChangePage();
            vm._state.tableData = __sucData;
            PubSub.trigger('table:change', threeDTableManager.getTableData() );
            _disposeCurData();
            // console.log( 'PubSub.trigger threeDTableManager.getTableData(): ' , threeDTableManager.getTableData() );
            // console.log( 'PubSub.trigger table:change' );
          },
          function(__errData){
            console.log('__errData: ' , __errData);
            alert( '3d 상품 리스트 페이지 호출을 하지 못했습니다.' );  //
          }
        );
    }


    function _deleteRow(){
      var opt = {items_ids:''},
        strArr = _getCheckedListRow('id'),
        strShopNameArr;

      if(strArr.length<1){
        alert('3d상품을 선택해주세요.'); //'3d상품을 선택해주세요.'
        return false;
      }

      opt.items_ids = strArr.join(',');
      strShopNameArr = '선택한\n'+_getCheckedListRowName().join(',')+"\n을(를) 삭제하시겠습니까?";
      // strShopNameArr = (APP.i18n["oper.partner.alert.is_delete_shop"]).replace('%s' , '\n'+_getCheckedListRowName().join(',')+'\n')

      if (confirm(strShopNameArr) == true){
        //console.log('items_ids: ' , opt.items_ids);

        threeDTableManager
          .deleteRow(opt)
          .then(
            function(__sucData){
              //console.log('_deleteRow: ', __sucData);
              alert( '삭제하였습니다.' );  //삭제하였습니다.
              _updatePage({start:0, count:vm._state.tableData.count});
            },
            function(__errData){
              console.log('tableComponentBasicCtrl _deleteRow: ', __errData);
              alert( '삭제가 정상적으로 되지 않았습니다.' );  //"삭제가 정상적으로 되지 않았습니다."
            }
          );
      }else{
        return false;
      }
    }

    function _getCheckedListRow(__attr){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push($(this).attr(__attr));
        }
      });
      return strArr;
    }

    function _getCheckedListRowName(){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push( $(this).attr('shop-name')+'('+$(this).attr('uuid')+')' );
        }
      });
      return strArr;
    }

  }

    tableComponentThreeDModalCtrl.$inject = ['$scope', '_',  '$timeout', 'XHR', 'PubSub', 'ThreeDTableManager', 'API_URL_INFO', 'COMMON_UTIL'];
    tableComponentThreeDModalCtrl.bindToController = {
    bootstrapCtrl:'@'
  };
    tableComponentThreeDModalCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      transclude(scope, function(clone, scope) {
        elem.find('.list_table').empty().append(clone);
      });
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
