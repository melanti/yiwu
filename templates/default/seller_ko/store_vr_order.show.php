<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncsc-oredr-show">
  <div class="ncsc-order-info">
    <div class="ncsc-order-details">
      <div class="title">E-쿠폰 주문정보</div>
      <div class="content">
        <dl>
          <dt>E-쿠폰 주문번호：</dt>
          <dd><?php echo $output['order_info']['order_sn'];?><a href="javascript:void(0);">더보기<i class="icon-angle-down"></i>
            <div class="more"><span class="arrow"></span>
              <ul>

                               <?php
                 if (strstr(orderPaymentName($output['order_info']['payment_code']),'支付宝')) {
                      $order_type = "Alipay";
                 } else if (strstr(orderPaymentName($output['order_info']['payment_code']),'在线付款')) {
                      $order_type = "온라인결제";
                 } else {
                      $order_type = "취소";
                 }

                  ?>
                <li>지불방식：<?php echo $order_type;?></li>
                <li>주문시간：<span><?php echo date("Y-m-d H:i:s",$output['order_info']['add_time']);?></span></li>
                <?php if(!empty($output['order_info']['payment_time'])){?>
                <li>결제시간：<span><?php echo date("Y-m-d H:i:s",$output['order_info']['payment_time']);?></span></li>
                <?php } ?>
              </ul>
            </div>
            </a></dd>
        </dl>
        <dl class="line">
          <dt>구매&#12288;&#12288;자：</dt>
          <dd><?php echo $output['order_info']['buyer_name'];?></dd>
        </dl>
        <dl class="line">
          <dt>접수전화：</dt>
          <dd><?php echo $output['order_info']['buyer_phone'];?></dd>
        </dl>
        <dl class="line">
          <dt>구매자 메세지：</dt>
          <dd><?php echo $output['order_info']['buyer_msg'];?></dd>
        </dl>
      </div>
    </div>
    <?php if ($output['order_info']['order_state'] == ORDER_STATE_CANCEL){ ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-off orange"></i>주문현황：</dt>
        <dd>취소완료</dd>
      </dl>
      <ul>
        <li><?php echo date("Y-m-d H:i:s",$output['order_info']['close_time']);?> 취소완료，이유：<?php echo $output['order_info']['close_reason'];?></li>
      </ul>
    </div>
    <?php } elseif ($output['order_info']['order_state'] == ORDER_STATE_NEW){ ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>주문이 완료되었습니다. 구매자 결제 대기 중입니다.</dd>
      </dl>
      <ul>
        <li>1. 구매자가 아직 결제를 진행하지 않았습니다.</li>
        <li>2. 해당주문이 무효한 주문이라면 클릭하여 주십시오. <a href="javascript:void(0)" class="ncsc-btn-mini" nc_type="dialog" dialog_width="480" dialog_title="주문취소" dialog_id="buyer_order_cancel_order" uri="index.php?act=store_vr_order&op=change_state&state_type=order_cancel&order_id=<?php echo $output['order_info']['order_id']?>&order_sn=<?php echo $output['order_info']['order_sn'];?>"  id="order_action_cancel">주문취소</a>。 </li>
        <li>2. 구매자가 해당 주문의 결제를 진행하지 않는다면, 시스템에서
          <time><?php echo date('Y-m-d H:i:s',$output['order_info']['order_cancel_day']);?></time>
          자동으로 주문이 취소됩니다.</li>
      </ul>
    </div>
    <?php } elseif ($output['order_info']['order_state'] == ORDER_STATE_PAY){ ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>구매자 결제완료, 전자교환번호 발송 완료</dd>
      </dl>
      <ul>
        <li>1. 해당 주문의 전자교환번호는 시스템에 의하여 자동적으로 구매자에게 보내집니다.</li>
        <li>2. 해당 거래는 당일부터 
          <time><?php echo date("Y-m-d",$output['order_info']['vr_indate']);?></time>
          기한이 지나 자동적으로 효력이 없어질 때 까지 입니다. </li>
      </ul>
    </div>
    <?php } elseif ($output['order_info']['order_state'] == ORDER_STATE_SUCCESS){ ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>주문완료</dd>
      </dl>
    </div>
    <?php }?>
  </div>
  <?php if ( $output['order_info']['order_state'] != ORDER_STATE_CANCEL){ ?>
  <div class="ncsc-order-step">
    <dl class="step-first current">
      <dt>주문생성</dt>
      <dd class="bg"></dd>
      <dd class="date" title="주문생성시간"><?php echo date("Y-m-d H:i:s",$output['order_info']['add_time']); ?></dd>
    </dl>
    <dl class="<?php echo $output['order_info']['step_list']['step2'] ? 'current' : null ; ?>">
      <dt>지불완료</dt>
      <dd class="bg"> </dd>
      <dd class="date" title="결제시간"><?php echo @date('Y-m-d H:i:s',$output['order_info']['payment_time']); ?></dd>
    </dl>
    <dl class="<?php echo $output['order_info']['step_list']['step3'] ? 'current' : null ; ?>">
      <dt>교환번호 발송</dt>
      <dd class="bg"> </dd>
    </dl>
    <dl class="long <?php echo $output['order_info']['step_list']['step4'] ? 'current' : null ; ?>">
      <dt>주문완료</dt>
      <dd class="bg"> </dd>
      <dd class="date" title="주문완료"><?php echo date("Y-m-d H:i:s",$output['order_info']['finnshed_time']); ?></dd>
    </dl>
    <div class="code-list tip" title="리스트가 너무 길게 보일 때에는 마우스를 굴려서 보기를 계속하십시오."><i class="arrow"></i>
      <h5>전자교환번호</h5>
      <div id="codeList">
        <ul>
          <?php foreach($output['order_info']['extend_vr_order_code'] as $code_info){ ?>
          <li class="<?php echo $code_info['vr_state'] == 1 ? 'used' : null;?>"><strong><?php echo $code_info['vr_state'] == '0' ? ($code_info['vr_indate'] < TIMESTAMP ? $code_info['vr_code'] : encryptShow($code_info['vr_code'],7,12)) : $code_info['vr_code'];?></strong> <?php echo $code_info['vr_code_desc'];?> </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
  <?php } else { ?>
  <br/>
  <?php }?>
  <div class="ncsc-order-contnet">
    <table class="ncsc-default-table order">
      <thead>
        <tr>
          <th class="w10"></th>
          <th colspan="2">상품</th>
          <th class="w100">단가(원)</th>
          <th class="w60">수량</th>
          <th class="w240"><strong>실결제  * 수수료율 = 기본수수료(원)</strong></th>
          <th class="w100">거래현황</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th colspan="20"><span class="ml10" title="E-쿠폰 주문번호">E-쿠폰 주문번호：<?php echo $output['order_info']['order_sn'];?></span><span>주문시간：<?php echo date("Y-m-d H:i",$output['order_info']['add_time']);?></span><span><a href="<?php echo urlShop('show_store','index',array('store_id'=>$output['order_info']['store_id']));?>" title="<?php echo $output['order_info']['store_name'];?>"><?php echo $output['store']['store_name'];?></a></span></th>
        </tr>
        <tr>
          <td class="bdl"></td>
          <td><div class="pic-thumb"><a href="<?php echo urlShop('goods','index',array('goods_id' => $output['order_info']['goods_id']));?>" target="_blank" onMouseOver="toolTip('<img src=<?php echo thumb($output['order_info'], 240);?>>')" onMouseOut="toolTip()"/><img src="<?php echo thumb($output['order_info'], 60);?>"/></a></div></td>
          <td class="tl"><dl class="goods-name">
              <dt><a href="<?php echo urlShop('goods','index',array('goods_id'=>$output['order_info']['goods_id']));?>" target="_blank" title="<?php echo $output['order_info']['goods_name'];?>"><?php echo $output['order_info']['goods_name'];?></a></dt>
              <dd><span class="sale-type"><?php if ($output['order_info']['order_promotion_type'] == 1) {?>공동구매，<?php } ?>유효기간：당일부터 <?php echo date("Y-m-d",$output['order_info']['vr_indate']);?>
              <?php if ($output['order_info']['vr_invalid_refund'] == '0') { ?>
              ，기간 이후 환불 불가
              <?php } ?>
              </span></dd>
            </dl></td>
          <td><?php echo number_format($output['order_info']['goods_price']);?>원</td>
          <td><?php echo $output['order_info']['goods_num'];?></td>
          <td class="commis bdl bdr">
          <?php if ($output['order_info']['commis_rate'] != 200) { ?>
          <?php echo ncPriceFormatKo($output['order_info']['order_amount_ko']);?> * <?php echo $output['order_info']['commis_rate'];?>% = <?php echo ncPriceFormatKo($output['order_info']['order_amount_ko'] * $output['order_info']['commis_rate']/100);?>
          <?php } ?>
          </td>
          <?php
    if(strstr($output['order_info']['state_desc'],"待付款")):
      $state_od = '<span style="color:#36C">결제대기</span>';
    elseif(strstr($output['order_info']['state_desc'],"待发货")):
      $state_od = '<span style="color:#ed008c">발송대기</span>';
    elseif(strstr($output['order_info']['state_desc'],"已支付")):
      $state_od = '<span style="color:#ed008c">결제완료</span>';
    elseif(strstr($output['order_info']['state_desc'],"待收货")):
      $state_od = '<span style="color:#99cc00">수취확인</span>';
    elseif(strstr($output['order_info']['state_desc'],"已取消")):
      $state_od = '<span style="color:#999">취소됨</span>';
    elseif(strstr($output['order_info']['state_desc'],"已完成")):
      $state_od = '<span style="color:#999">거래완료</span>';
    else:
      $state_od = $output['order_info']['state_desc'];
    endif;

?>
          <td class="bdl"><?php echo $state_od;?></td>
        </tr>
      </tbody>
      <tfoot>
        <?php if(!empty($output['order_info']['voucher_code'])){ ?>
        <tr>
          <th colspan="20"><dl class="ncsc-store-sales">
              <?php if(!empty($output['order_info']['voucher_code'])){ ?>
              <dd><span>&emsp;사용함 <strong><?php echo $output['order_info']['voucher_price'];?></strong> 원 쿠폰（번호：<?php echo $output['order_info']['voucher_code'];?>）</span></dd>
              <?php } ?>
            </dl>
          </th>
        </tr>
        <?php } ?>
        <tr>
          <td colspan="20" class="transportation tl"><dl class="sum">
              <dt>주문금액：</dt>
              <dd><em><?php echo number_format($output['order_info']['order_amount_ko']);?></em>원</dd>
            </dl></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>
<script type="text/javascript">
//兑换码列表过多时出现滚条
$(function(){
	$('#codeList').perfectScrollbar();
	//title提示
    	$('.tip').poshytip({
            className: 'tip-yellowsimple',
            showTimeout: 1,
            alignTo: 'target',
            alignX: 'left',
            alignY: 'top',
            offsetX: 5,
            offsetY: -60,
            allowTipHover: false
        });
});
</script>
