<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 업체등급 语言包
 */
$lang['store_grade_name_no_null']               = '등급 명칭은 비워두실 수 없습니다.';
$lang['charges_standard_no_null']               = '표준비용은 비워두실 수 없습니다.';
$lang['allow_pubilsh_product_num_only_lnteger'] = '발표상품수는정수로 허용합니다.';
$lang['upload_space_size_only_lnteger']         = '업로드 공간사이즈는 정수여야만 합니다.';
$lang['sort_only_lnteger']                      = '순서정렬은 정수로만 합니다';
$lang['back_store_grade_list']                  = '미니샵등급 목록으로 가기';
$lang['illegal_parameter']                      = '불법 변수';

$lang['store_grade']                = '미니샵등급';
$lang['store_grade_tpl']            = '미니샵등급 탬플릿';
$lang['manage']                     = '관리';
$lang['store_grade_name']           = '등급명칭';
$lang['cancel_search']              = '검색 취소';
$lang['allow_pubilsh_product_num']  = '허용발표상품수';
$lang['upload_space_size']          = '업로드공간 사이즈';
$lang['optional_template_num']      = '선택할수 있는 탬플릿 수';
$lang['charges_standard']           = '요금표준';
$lang['charges_standard_notice']           = '예:100원/년';
$lang['need_audit']                 = '검토필요 함';
$lang['default_store_grade_no_del'] = '삭제불가';
//$lang['problem_del']                = '您确定要삭제该업체등급吗？삭제该업체등급下的所有업체会自动改为默认等级';
$lang['problem_del']                = '미니샵등급을 삭제 하시겠습니까?';
$lang['set_template']               = '설정할수 있는 탬플릿 설정';
$lang['store_grade']                            = '미니샵등급';
$lang['manage'] = '관리';
$lang['store_grade_name']                       = '등급명칭';
$lang['allow_pubilsh_product_num']              = '발표할수 있는 상품 수 ';
$lang['zero_said_no_limit']                     = '0무제한을 표시합니다';
$lang['allow_upload_album_num']               	= '업로드할수 있는 이미지 수';
$lang['optional_template_num']                  = '선택할수 있는 탬플릿 수';
$lang['charges_standard']                       = '요금표준';
$lang['charges_standard_notice']                = '요금표준，회원이 미니샵를 오픈 및 업데이트 할때 앞단에 표시됩니다.';
$lang['need_audit']                             = '검토필요 함';
$lang['need_audit_notice']                     	= '회원은 미니샵 개통 및 업로드시 관리자의 심사를 필요로 하십니까?';
$lang['default_store_grade_no_del']             = '삭제불가';
//$lang['problem_del']                            = '您确定要삭제该업체등급吗？삭제该업체등급下的所有업체会自动改为默认等级';
$lang['problem_del']                            = '미니샵등급을 삭제하시 겠습니까?';
$lang['set_template']                           = '템플릿설정';
$lang['in_store_grade_list_set']                = '미니샵등급 목록중 설정';
$lang['additional_features']                    = '부가기능 사용';
$lang['editor_media_features']                  = '편집기 멀티미디어 기능';
$lang['application_note']                       = '설명신청';
$lang['application_note_notice']                = '설명신청，회원개통 및 업데이트 미니샵 시 앞단에 표시됨';
$lang['reset']                                  = '다시 설정';
$lang['now_store_grade_name_is_there']          = '등급명칭이 존재합니다. 다시 작성해 주세요';
$lang['only_lnteger']                           = '정수로만 가능합니다.';
$lang['only_number']                            = '숫자로만 가능합니다.';
$lang['del_remark'] = "주의: 미니샵등급아래 미니샵정보가 있을시 삭제할 수 없습니다.";
$lang['del_gradehavestore'] = "미니샵 등급아래 미니샵 정보를 삭제하신후 시도해 주세요";
$lang['add_gradesortexist'] = "등급이 존재합니다";
$lang['grade_parameter_error'] = "변수오류";
$lang['record_error'] = "기록오류";
$lang['grade_edit_gradeerror'] = "등급정보오류,미니샵등급정보 보기 ";
$lang['grade_edit_grade_same_error'] = "등급정보 수정됨,미니샵등급정보를 확인해 주세요";
$lang['grade_edit_grade_store_error'] = "미니샵정보 오류";
$lang['grade_edit_grade_sort_error'] = "업데이트 등급은 현미니샵 현재 등급보다 높아야 합니다.";
$lang['grade_del_please_choose_error'] = "운영 기록을 선택하세요";
$lang['grade_sortname'] = "등급";
$lang['grade_sort_tip'] = "숫자가 클수록 높은 등급을 나타냅니다.";
$lang['grade_add_sort_null_error'] = "등급정보는 비워 두실 수 없습니다.";
$lang['store_grade_help1']                = '셀프로 미니샵 등급을 추가 및 편집 하실 수 있습니다.';
$lang['store_grade_help2']                = '미니샵 오픈하실때 미니샵 등급을 설정하실 수 있습니다.';
$lang['store_grade_help3']                = '미니샵를 가지고 계신 회원님은 등급 업데이트를 하실 수 있습니다.';
/**
 * 日志리스트
 */
$lang['admin_gradelog_membername']			= '회원명칭';
$lang['admin_gradelog_storename']			= '미니샵명';
$lang['admin_gradelog_gradename']			= '등급명칭';
$lang['admin_gradelog_auditstate']			= '심사상태';
$lang['admin_gradelog_auditing']			= '심사대기중';
$lang['admin_gradelog_auditpass']			= '심사통과';
$lang['admin_gradelog_auditnopass']			= '심사실패';
$lang['admin_gradelog_list_number']			= '순번';
$lang['admin_gradelog_needcheck']			= '심사가 필요합니다';
$lang['admin_gradelog_addtime']				= '시간 추가';
$lang['admin_gradelog_audittip']			= '주의：심사는 한번만 가능 합니다.';
$lang['admin_gradelog_remark']				= '비고';
$lang['admin_gradelog_auditadmin']				= '심사인원';