var Q = require('q'),
chkData = require("./libs/chkData.js"),
libVc = require("./libs/verifyCode.js");

module.exports = {
    defaultVideos: defaultVideos,
    setVideo: setVideo,
    setCapture: setCapture,
    updateVideo: updateVideo,
    getVideo: getVideo,
    getQRData: getQRData
};

function defaultVideos(req, res) {
    var debug = require("./libs/debug.js"),
    rtn_val = {
        "results": []
    },
    sex = req.param("sex") || "all",
    kids_yn,
    hidden = req.param("hidden") || "",
    arr_vod = {
        'man': [95, 100, 105, 110],
        'woman': [44, 55, 66],
        'boy': [],
        'girl': []
    },
    arr_version = {
        'man': [1, 1, 1, 1],
        'woman': [1, 1, 1],
        'boy': [],
        'girl': []
    },
    ver_info = "?ver={version}&date=" + (new Date()).valueOf(), // TODO: 차후 버전 값은 DB에 넣고 각 영상별 버젼을 달아야 함
    status = 501,
    sex_mapping = {
        all : {
            all : ['man','woman','boy','girl'],
            kids : ['boy','girl'],
            adults : ['man','woman']
        },
        man : {
            all : ['man','boy'],
            kids : ['boy'],
            adults : ['man']
        },
        woman : {
            all : ['woman','girl'],
            kids : ['girl'],
            adults : ['woman']
        }
    },
    type_mapping = {
        man : ['man', 0],
        woman : ['woman', 0],
        boy : ['man', 1],
        girl : ['girl', 1]
    },
    sex_mapping_arr = [];

    if(!req.param('kids_yn')) {
        kids_yn = 'all';
    } else if (req.param('kids_yn') == 0){
        kids_yn = 'adults';
    } else if (req.param('kids_yn') == 1){
        kids_yn = 'kids';
    }

    sex_mapping_arr = sex_mapping[sex][kids_yn];



    var setResults = function (sex) {

        var len_arr = arr_vod[sex].length;

        for (var i = 0; i < len_arr; i++) {
            var size = arr_vod[sex][i];
            var version = arr_version[sex][i];
            var version_val = ver_info.replace("{version}", version);

            var vod_obj = {
                "sex": type_mapping[sex][0],
                "kids_yn": type_mapping[sex][1],
                "video": sails.config.ftp.domain + "/fitting/vods/" + sex + "/" + size + "/Video.mp4" + version_val,
                "animation": sails.config.ftp.domain + "/fitting/vods/" + sex + "/" + size + "/Animation.bin" + version_val,
                "thumbnail": sails.config.ftp.domain + "/fitting/vods/" + sex + "/" + size + "/Thumbnail.jpg" + version_val,
                "capture": sails.config.ftp.domain + "/fitting/vods/" + sex + "/" + size + "/Capture.jpg" + version_val,
                "background": "",
                "version": version
            };


            for ( var elem in vod_obj ) {
                if ( hidden.indexOf(elem) > -1 ) {
                    delete vod_obj[elem];
                }
            }

            rtn_val.results.push(vod_obj);
        }

    };

    for (var i = 0, length = sex_mapping_arr.length; i < length; i++) {
        setResults(sex_mapping_arr[i]);
    }

    status = 200;
    return res.send(status, debug.wrap(req, res, status, rtn_val));
}

function setVideo(req, res) {

    var start_second, end_second;
    var start_second1, end_second1;
    var start_second2, end_second2;
    var start_second3, end_second3;
    var start_second4, end_second4;
    var start_second5, end_second5;
    var start_second6, end_second6;

    var debug = require("./libs/debug.js"),
    params = req.params.all(),
    guid = require("./libs/uuid.js").setUUID(),
    videoSex = params.video_sex,
    videoKids = params.video_kids || 0,
    disk = "USER/" + params.video_sex + "/" + guid + "/",
    diskUrl = sails.config.ftp.domain + '/' + disk,
    localDir = require("./libs/uuid.js").setUUID(),
    videoSerial = FittingService.genSerial(),
    status,
    rtnVal = {},
    filesArr = ['video', 'animation', 'capture', 'background'],
    type = 'video';

    var videoInfo = {
        activationKey: params.activation_key,
        guid: guid,
        videoSex: videoSex,
        videoKids: videoKids,
        fileInfo: {},
        disk: disk,
        diskUrl: diskUrl,
        localDir: localDir,
        videoSerial: videoSerial
    };

    start_second = new Date().getTime() / 1000;
    start_second1 = new Date().getTime() / 1000;

    chkData.chkParam(req, res, [videoInfo.activationKey], function () {
        /*libVc.chkFxmrAtvtnKey(req, res, videoInfo.activationKey, function (activationNo) {*/
            ///videoInfo.activationNo = activationNo;
            videoInfo.activationNo = 1;

            Q.fcall(function () {
                /* 1. Upload files to local */
                return uploadFilesAll(req, videoInfo, filesArr);
            })
            .then(function () {

                end_second1 = new Date().getTime() / 1000;
                start_second2 = new Date().getTime() / 1000;

                /* 2. Initialize video information */
                return initVideo(videoInfo, type);
            })
            .then(function () {
                end_second2 = new Date().getTime() / 1000;
                start_second3 = new Date().getTime() / 1000;

                /* 3. Calculate hashes : 'sha256' */
                return computeHash(videoInfo);
            })
            .then(function () {
                end_second3 = new Date().getTime() / 1000;
                start_second4 = new Date().getTime() / 1000;

                /* 4. Generate a QR code which includes 'serial' */
                return genQR(videoInfo);
            })
            .then(function () {
                end_second4 = new Date().getTime() / 1000;
                start_second5 = new Date().getTime() / 1000;

                /* 5. Upload files to FTP storage */
                /*return s3Upload(videoInfo);*/
                return sftpUpload(videoInfo);
            })
            .then(function () {
                end_second5 = new Date().getTime() / 1000;
                start_second6 = new Date().getTime() / 1000;

                /* 6. Insert file information */
                return insertFile(videoInfo);
            })
            .then(function () {
                /* 7. Remove temp files */
                return removeFile(videoInfo);
            })
            .then(function () {
                end_second6 = new Date().getTime() / 1000;

                /* 8. Get output results */
                return outputResults(videoInfo);
            })
            .then(function (result) {
                end_second = new Date().getTime() / 1000;
                sails.log.debug('::::::: SUMMARY ::::::');
                sails.log.debug("total : " + (end_second - start_second));
                sails.log.debug("1.uploadLocal : " + (end_second1 - start_second1));
                sails.log.debug("2.initVideo : " + (end_second2 - start_second2));
                sails.log.debug("3.computeHash : " + (end_second3 - start_second3));
                sails.log.debug("4.genQR : " + (end_second4 - start_second4));
                sails.log.debug("5.s3Upload : " + (end_second5 - start_second5));
                sails.log.debug("6.insertFile : " + (end_second6 - start_second6));

                status = 200;

                rtnVal.result = result;
                return res.send(status, debug.wrap(req, res, status, rtnVal));
            })
            .catch(function (err) {
                sails.log.debug('error ::::: ' + JSON.stringify(err, null, 4));

                return res.send(status, debug.wrap(req, res, status, err));
            })
            .done();
        /*});*/
    });
}

function setCapture(req, res) {

    var start_second, end_second;
    var start_second1, end_second1;
    var start_second2, end_second2;
    var start_second3, end_second3;
    var start_second4, end_second4;
    var start_second5, end_second5;
    var start_second6, end_second6;

    var debug = require("./libs/debug.js"),
    params = req.params.all(),
    guid = require("./libs/uuid.js").setUUID(),
    videoSex = params.video_sex,
    videoKids = params.video_kids || 0,
    itemCodes = params.item_codes,
    disk = "USER/" + params.video_sex + "/" + guid + "/",
    diskUrl = sails.config.ftp.domain + '/' + disk,
    localDir = require("./libs/uuid.js").setUUID(),
    videoSerial = FittingService.genSerial(),
    status,
    rtnVal = {},
    filesArr = ['capture'],
    type = 'capture';

    var videoInfo = {
        activationKey: params.activation_key,
        guid: guid,
        videoSex: videoSex,
        videoKids: videoKids,
        itemCodes: itemCodes,
        fileInfo: {},
        disk: disk,
        diskUrl: diskUrl,
        localDir: localDir,
        videoSerial: videoSerial
    };

    start_second = new Date().getTime() / 1000;
    start_second1 = new Date().getTime() / 1000;

    chkData.chkParam(req, res, [videoInfo.activationKey, videoInfo.videoSex], function () {
        /*libVc.chkFxmrAtvtnKey(req, res, videoInfo.activationKey, function (activationNo) {*/
            //videoInfo.activationNo = activationNo;
            videoInfo.activationNo = 1;

            Q.fcall(function () {
                /* 1. Upload files to local */
                return uploadFilesAll(req, videoInfo, filesArr);
            })
            .then(function () {

                end_second1 = new Date().getTime() / 1000;
                start_second2 = new Date().getTime() / 1000;

                if(videoInfo.files && videoInfo.files.length > 0) {
                    /* 2. Initialize video information */
                    return initVideo(videoInfo, type);
                } else {
                    var err_code = ErrorHandler.BAD_BLANK_ERROR;
                    var err = {
                        code: err_code,
                        index: 0
                    };
                    status = 400;

                    throw err;
                }
            })
            .then(function () {
                end_second2 = new Date().getTime() / 1000;
                start_second3 = new Date().getTime() / 1000;

                /* 3. Calculate hashes : 'sha256' */
                return computeHash(videoInfo);
            })
            .then(function () {
                end_second3 = new Date().getTime() / 1000;
                start_second4 = new Date().getTime() / 1000;

                /* 4. Generate a QR code which includes 'serial' */
                return genQR(videoInfo);
            })
            .then(function () {
                end_second4 = new Date().getTime() / 1000;
                start_second5 = new Date().getTime() / 1000;

                /* 5. Upload files to S3 storage */
                /*return s3Upload(videoInfo);*/
                return sftpUpload(videoInfo);
            })
            .then(function () {
                end_second5 = new Date().getTime() / 1000;
                start_second6 = new Date().getTime() / 1000;

                var index = videoInfo.files.indexOf('capture');

                if (index !== -1) {
                    videoInfo.files[index] = 'capture_photo';
                    videoInfo.fileInfo['capture_photo'] = videoInfo.fileInfo['capture'];
                }

                /* 6. Insert file information */
                return insertFile(videoInfo);
            })
            .then(function () {
                if(itemCodes) {
                    return insertFitvItemMap(videoInfo);
                }
            })
            .then(function () {
                /* 7. Remove temp files */
                return removeFile(videoInfo);
            })
            .then(function () {
                end_second6 = new Date().getTime() / 1000;

                /* 8. Get output results */
                return outputResults(videoInfo);
            })
            .then(function (result) {
                end_second = new Date().getTime() / 1000;
                sails.log.debug('::::::: SUMMARY ::::::');
                sails.log.debug("total : " + (end_second - start_second));
                sails.log.debug("1.uploadLocal : " + (end_second1 - start_second1));
                sails.log.debug("2.initVideo : " + (end_second2 - start_second2));
                sails.log.debug("3.computeHash : " + (end_second3 - start_second3));
                sails.log.debug("4.genQR : " + (end_second4 - start_second4));
                sails.log.debug("5.s3Upload : " + (end_second5 - start_second5));
                sails.log.debug("6.insertFile : " + (end_second6 - start_second6));

                status = 200;

                rtnVal.result = result;
                return res.send(status, debug.wrap(req, res, status, rtnVal));
            })
            .catch(function (err) {
                sails.log.debug('error ::::: ' + JSON.stringify(err, null, 4));

                return res.send(status, debug.wrap(req, res, status, err));
            })
            .done();
        /*});*/
    });
}

function updateVideo(req, res) {

    var start_second, end_second;
    var start_second1, end_second1;
    var start_second2, end_second2;
    var start_second3, end_second3;
    var start_second4, end_second4;
    var start_second5, end_second5;

    var debug = require("./libs/debug.js"),
    params = req.params.all(),
    localDir = require("./libs/uuid.js").setUUID(),
    status,
    rtnVal = {},
    filesArr = ['video', 'animation', 'capture', 'background'];

    var videoInfo = {
        activationKey: params.activation_key,
        videoSerial: params.serial,
        videoSex: params.video_sex,
        videoKids : params.video_kids || 0,
        localDir: localDir,
        fileInfo: {}
    };

    start_second = new Date().getTime() / 1000;
    start_second1 = new Date().getTime() / 1000;

    chkData.chkParam(req, res, [videoInfo.activationKey], function () {
        /*libVc.chkFxmrAtvtnKey(req, res, videoInfo.activationKey, function (activationNo) {*/
            //videoInfo.activationNo = activationNo;
            videoInfo.activationNo = 1;
            Q.fcall(function () {
                /* 1. Check serial duplication */
                return checkSerialDuplication(videoInfo);
            })
            .then(function () {

                /* 2. Upload files to local */
                return uploadFilesAll(req, videoInfo, filesArr);
            })
            .then(function () {
                end_second1 = new Date().getTime() / 1000;
                start_second2 = new Date().getTime() / 1000;

                /* 3. Update FITV */
                return fitvUpdate(videoInfo);
            })
            .then(function () {
                end_second2 = new Date().getTime() / 1000;
                start_second3 = new Date().getTime() / 1000;

                /* 4. Calculate hashes : 'sha256' */
                return computeHash(videoInfo);
            })
            .then(function () {
                end_second3 = new Date().getTime() / 1000;
                start_second4 = new Date().getTime() / 1000;

                /* 5. Replace files to S3 storage */
                return s3Replace(videoInfo);
            })
            .then(function () {
                end_second4 = new Date().getTime() / 1000;
                start_second5 = new Date().getTime() / 1000;

                /* 6. Update file information */
                return updateFile(videoInfo);
            })
            .then(function () {
                end_second5 = new Date().getTime() / 1000;

                /* 7. Remove temp files */
                return removeFile(videoInfo);
            })
            .then(function () {
                /* 8. Get output results */
                return outputResults(videoInfo);
            })
            .then(function (result) {
                end_second = new Date().getTime() / 1000;
                sails.log.debug('::::::: SUMMARY ::::::');
                sails.log.debug("total : " + (end_second - start_second));
                sails.log.debug("1.uploadLocal : " + (end_second1 - start_second1));
                sails.log.debug("2.updateFitv : " + (end_second2 - start_second2));
                sails.log.debug("3.computeHash : " + (end_second3 - start_second3));
                sails.log.debug("4.s3Replace : " + (end_second4 - start_second4));
                sails.log.debug("5.updateFile : " + (end_second5 - start_second5));

                status = 200;

                rtnVal.result = result;
                return res.send(status, debug.wrap(req, res, status, rtnVal));
            })
            .catch(function (err) {
                sails.log.debug('error ::::: ' + JSON.stringify(err, null, 4));

                status = 400;
                return res.send(status, debug.wrap(req, res, status, err));
            })
            .done();
        /*});*/
    });
}

function initVideo(videoInfo, type) {

    var deferred = Q.defer();

    videoInfo.videoSerial = FittingService.genSerial();

    initVideoLoop(videoInfo, fitvInsert, genSerial, type)
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        deferred.reject(err);
    });

    return deferred.promise;

}

function initVideoLoop(videoInfo, fitvInsert, genSerial, type) {
    var deferred = Q.defer();

    function loop() {
        //When the result of calling 'fitvInsert' is true, we are done.
        fitvInsert(videoInfo, type)
        .then(function (bool) {
            if (bool) {
                deferred.resolve();
            } else {
                // When it completes, loop again.
                // Otherwise, if it fails, reject the promise.
                return Q.when(genSerial(videoInfo), loop, deferred.reject);
            }
        })
        .catch(function (err) {
            deferred.reject(err);
        });
    }

    // Start running the loop in the next tick.
    Q.nextTick(loop);

    return deferred.promise;
}

function fitvInsert(videoInfo, type) {
    var deferred = Q.defer(),
    qry = "insert into sv_fitv (fxmr_activation_no, fitv_serial, fitv_code, fitv_sex, fitv_kids, fitv_duration) values(?, ?, ?, ?, ?, SEC_TO_TIME(?))";

    if ( type === 'video' ) {
        if (!videoInfo.duration || videoInfo.duration === 0) {
            videoInfo.duration = 20;
        }
    } else {
        videoInfo.duration = 0;
    }

    DB.query(qry,
    [videoInfo.activationNo,
        videoInfo.videoSerial,
        videoInfo.guid,
        videoInfo.videoSex,
        videoInfo.videoKids,
        videoInfo.duration
    ],
    function (err, result) {
        if (err) {
            if (err.code === "ER_DUP_ENTRY") {
                deferred.resolve(false);
            } else {
                deferred.reject(err);
            }
        } else {
            videoInfo.fitvNo = result.insertId;
            return deferred.resolve(true);
        }
    }
    );

    return deferred.promise;
}

function genSerial(videoInfo) {
    videoInfo.videoSerial = FittingService.genSerial();
}

function uploadFilesAll(req, videoInfo, filesArr) {
    var deferred = Q.defer();

    var files = filesArr;
    var promises = [];
    for (var i = 0; i < files.length; i++) {
        var promise = doUpload(req, videoInfo, files[i]);
        promises.push(promise);
    }

    Q.allSettled(promises).then(function (results) {
        sails.log.debug('local uploading done...' + ' ::: localDir---' + videoInfo.localDir);

        deferred.resolve();
    });
    return deferred.promise;
}

function doUpload(req, videoInfo, file) {
    var deferred = Q.defer();
    videoInfo.currentFile = file;

    FittingService.uploadTemp(req, videoInfo, file)
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        deferred.reject(err);
    });


    return deferred.promise;
}

function computeHash(videoInfo) {
    var deferred = Q.defer();

    var files = videoInfo.files;


    var promises = [];
    for (var i = 0; i < files.length; i++) {
        var promise = doComputeHash(videoInfo, files[i]);
        promises.push(promise);
    }

    Q.all(promises)
    .then(function () {
        sails.log.debug('hashing done...');
        deferred.resolve();
    }, function (err) {
        sails.log.error('hashing failure');
        deferred.reject(err);
    });

    return deferred.promise;
}

function doComputeHash(videoInfo, file) {
    var deferred = Q.defer();
    FittingService.computeHash(videoInfo, file)
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function genQR(videoInfo) {

    var deferred = Q.defer();
    var file = 'tag';

    FittingService.genQR(videoInfo, file)
    .then(function () {
        sails.log.debug('genQR done...');
        deferred.resolve();
    })
    .catch(function (err) {
        sails.log.error('genQR fail...');
        deferred.reject(err);
    });
    return deferred.promise;
}

function s3Upload(videoInfo) {
    var deferred = Q.defer();
    var S3 = new S3Handler('data');
    videoInfo.S3 = S3;

    var files = videoInfo.files;
    files.push('tag');

    var promises = [];
    for (var i = 0; i < files.length; i++) {
        var promise = doS3Upload(videoInfo, files[i]);
        promises.push(promise);
    }

    Q.all(promises)
    .then(function () {
        sails.log.debug('s3 uploading done...:: guid---' + videoInfo.guid + ' ::: localDir---' + videoInfo.localDir);
        deferred.resolve();
    }, function (err) {
        sails.log.error('s3 upload failure');
        deferred.reject(err);
    });

    return deferred.promise;
}

function doS3Upload(videoInfo, file) {
    var deferred = Q.defer();
    FittingService.s3Upload(videoInfo, file)
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function sftpUpload(videoInfo) {
    var deferred = Q.defer();

    FittingService.sftpUpload(videoInfo)
    .then(function () {
        sails.log.debug('sftp uploading done...:: guid---' + videoInfo.guid + ' ::: localDir---' + videoInfo.localDir);
        deferred.resolve();
    })
    .catch(function (err) {
        sails.log.error('sftp upload failure');
        deferred.reject(err);
    });

    return deferred.promise;
}

function outputResults(videoInfo) {

    var result = {
        serial: videoInfo.videoSerial
    };
    return result;

}

function insertFile(videoInfo) {
    var deferred = Q.defer();
    var type = 'insert';
    var qry = FittingService.getSQLForFitvFile(videoInfo, type);

    //sails.log.debug("insert file ::: " + qry);
    DB.query(qry,
    function (err, results) {
        if (err) {
            sails.log.error('insert fitv_file, fail...');
            deferred.reject(err);
        } else {
            sails.log.debug('insert fitv_file, done...');
            deferred.resolve();
        }
    }
    );

    return deferred.promise;
}

function insertFitvItemMap(videoInfo) {
    var deferred = Q.defer();
    var itemCodes = videoInfo.itemCodes;
    var itemCodesArr;
    var qry;
    var params = [];

    if(itemCodes) {
        itemCodesArr = itemCodes.split(',');
        qry = 'insert into sv_fitv_item_mapp (fitv_no,goods_commonid) values ';

        for (var i = 0, length = itemCodesArr.length; i < length; i++) {
            if( itemCodesArr[i] ) {
                qry += '(?, ?),';
                params.push(videoInfo.fitvNo, itemCodesArr[i]);
            }
        }

        var query = qry.substring(0, qry.length - 1);

        DB.query(query, params,
        function (err, results) {
            if (err) {
                sails.log.error('insert fitv_item_mapp, fail...');
                deferred.reject(err);
            } else {
                sails.log.debug('insert fitv_item_mapp, done...');
                deferred.resolve();
            }
        }
        );
    }

    return deferred.promise;
}

function removeFile(videoInfo) {
    var deferred = Q.defer();

    FittingService.removeTemp(videoInfo)
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        sails.log.error('remove local file failure...');
        deferred.reject(err);
    });

    return deferred.promise;
}

function checkSerialDuplication(videoInfo) {
    var deferred = Q.defer(),
    qry = "SELECT FITV_ST as fitv_status, FITV_NO as fitv_no, FITV_CD as fitv_cd, FITV_SEX as fitv_sex FROM FITV WHERE FITV_SERIAL = ?";


    DB.query(qry,
    [videoInfo.videoSerial],
    function (err, results) {
        if (err) {
            deferred.reject(err);
        } else {

            if (results.length === 0 || !results[0].fitv_status) {
                //wrong serial
                var error = {};

                error.code = ErrorHandler.NOT_FOUND_DATA.code;
                error.index = 0;

                deferred.reject(error);
            } else if (results[0].fitv_status === "deleted") {
                //deleted video
                deferred.reject(err);
            } else {
                videoInfo.guid = results[0].fitv_cd;
                videoInfo.videoSexOld = results[0].fitv_sex;
                videoInfo.fitvNo = results[0].fitv_no;
                return deferred.resolve();
            }
        }
    }
    );

    return deferred.promise;
}

function getVideo(req, res) {
    var debug = require("./libs/debug.js"),
    chk_data = require("./libs/chkData.js"),
    network = require("./libs/network.js"),
    serial = req.param("serial"),
    device_id = req.param("device_id"),
    show = req.param("show"),
    ip_addr = network.get_ip(req, res),
    qry,
    qry_from,
    qry_where,
    qry_group,
    qry_inst,
    arr_param = [],
    arr_param_inst = [],
    rtn_val = {},
    status = 501,
    error = {};

    chk_data.chkParam(req, res, [serial, device_id], function () {
        qry = "SELECT a.fitv_sex as sex ";
        qry += "    , a.fitv_kids as kids_yn ";
        qry += "    , a.fitv_status as video_st ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'video' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Video.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as video ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'animation' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Animation.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as animation ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'capture' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Capture.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as capture ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'background' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Background.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as background ";

        qry_from = " FROM sv_fitv a ";
        qry_from += "LEFT OUTER JOIN sv_fitv_file b ON a.fitv_no = b.fitv_no ";
        qry_from += "LEFT OUTER JOIN sv_fitv_file_category c ON b.fitv_file_category_no = c.fitv_file_category_no ";
        qry_from += ", (SELECT ? AS access_url FROM DUAL) d ";
        qry_where = "WHERE a.fitv_serial = ? ";
        qry_group = "GROUP BY a.fitv_no ";

        qry = qry + qry_from + qry_where + qry_group;

        arr_param.push('USER');
        arr_param.push(serial);


        qry_inst = 'INSERT INTO sv_fitv_access_log (fitv_no,device_id,regist_ip) VALUES ';
        qry_inst += '((SELECT fitv_no FROM sv_fitv WHERE fitv_serial = ?), ?, INET_ATON(?))';

        arr_param_inst.push(serial);
        arr_param_inst.push(device_id);
        if (ip_addr === "::1") {
            ip_addr = "127.0.0.1";
        }

        arr_param_inst.push(ip_addr);

        Q.fcall(function () {
            var deferred = Q.defer();
            DB.query(qry, arr_param, function (err, results) {
                if (err) {
                    deferred.reject(err);
                } else if (!results || results.length === 0) {
                    status = ErrorHandler.NOT_FOUND_DATA.status;
                    error.code = ErrorHandler.NOT_FOUND_DATA.code;
                    error.index = 0;
                    deferred.reject(error);
                } else {
                    if (results[0].video_st === "deleted") {
                        status = ErrorHandler.DELETED_DATA.status;
                        error.code = ErrorHandler.DELETED_DATA.code;
                        error.index = 0;
                        deferred.reject(error);
                    } else if (!results[0].video) {
                        status = ErrorHandler.NOT_FOUND_DATA.status;
                        error.code = ErrorHandler.NOT_FOUND_DATA.code;
                        error.index = 0;
                        deferred.reject(error);
                    } else {
                        rtn_val.result = results[0];
                        setResults(rtn_val.result);
                        deferred.resolve();
                    }
                }
            });
            return deferred.promise;
        })
        .then(function () {
            var deferred = Q.defer();
            DB.query(qry_inst, arr_param_inst, function (err, results) {
                if (err) {
                    deferred.reject(err);
                } else {
                    deferred.resolve();
                }
            });
            return deferred.promise;
        })
        .then(function () {
            status = 200;
            return res.send(status, debug.wrap(req, res, status, rtn_val));
        })
        .catch(function (err) {
            status = (status) ? status : 500;
            return res.send(status, debug.wrap(req, res, status, err));
        });
    });

    var setResults = function (result) {

        if(result.video) {
            result.video = result.video.replace('%', '?');
            result.video = sails.config.ftp.domain + '/' + result.video;
        }

        if(result.animation) {
            result.animation = result.animation.replace('%', '?');
            result.animation = sails.config.ftp.domain + '/' + result.animation;
        }

        if(result.capture) {
            result.capture = result.capture.replace('%', '?');
            result.capture = sails.config.ftp.domain + '/' + result.capture;
        }

        if(result.background) {
            result.background = result.background.replace('%', '?');
            result.background = sails.config.ftp.domain + '/' + result.background;
        }

        result.version = -1;

        delete result.video_st;
    }
}

function getQRData(req, res) {
    var debug = require("./libs/debug.js"),
    chk_data = require("./libs/chkData.js"),
    network = require("./libs/network.js"),
    serial = req.param("serial"),
    device_id = req.param("device_id"),
    show = req.param("show"),
    ip_addr = network.get_ip(req, res),
    qry,
    qry_from,
    qry_where,
    qry_group,
    qry_inst,
    arr_param = [],
    arr_param_inst = [],
    rtn_val = {},
    status = 501,
    error = {};

    chk_data.chkParam(req, res, [serial, device_id], function () {
        qry = "SELECT a.fitv_sex as sex ";
        qry += "    , a.fitv_kids as kids_yn ";
        qry += "    , a.fitv_status as video_st ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'video' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Video.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as video ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'animation' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Animation.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as animation ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'capture' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Capture.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as capture ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'capture_photo' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Capture.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as capture_photo ";
        qry += "    , GROUP_CONCAT( ";
        qry += "	  				CASE c.fitv_file_category_name ";
        qry += "                    WHEN 'background' ";
        qry += "                    THEN CONCAT( ";
        qry += "                                 d.access_url ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_sex ";
        qry += "                               , '/' ";
        qry += "                               , a.fitv_code ";
        qry += "                               , '/Background.' ";
        qry += "                               , b.fitv_file_extend ";
        qry += "                               , '%h=' ";
        qry += "                               , b.fitv_file_hash ";
        qry += "                               ) END ";
        qry += "                   ) as background ";

        qry_from = " FROM sv_fitv a ";
        qry_from += "LEFT OUTER JOIN sv_fitv_file b ON a.fitv_no = b.fitv_no ";
        qry_from += "LEFT OUTER JOIN sv_fitv_file_category c ON b.fitv_file_category_no = c.fitv_file_category_no ";
        qry_from += ", (SELECT ? AS access_url FROM DUAL) d ";
        qry_where = "WHERE a.fitv_serial = ? ";
        qry_group = "GROUP BY a.fitv_no ";

        qry = qry + qry_from + qry_where + qry_group;

        arr_param.push('USER');
        arr_param.push(serial);

        qry_inst = 'INSERT INTO sv_fitv_access_log (fitv_no,device_id,regist_ip) VALUES ';
        qry_inst += '((SELECT fitv_no FROM sv_fitv WHERE fitv_serial = ?), ?, INET_ATON(?))';

        arr_param_inst.push(serial);
        arr_param_inst.push(device_id);
        if (ip_addr === "::1") {
            ip_addr = "127.0.0.1";
        }

        arr_param_inst.push(ip_addr);

        Q.fcall(function () {
            var deferred = Q.defer();
            DB.query(qry, arr_param, function (err, results) {
                if (err) {
                    deferred.reject(err);
                } else if (!results || results.length === 0) {
                    status = ErrorHandler.NOT_FOUND_DATA.status;
                    error.code = ErrorHandler.NOT_FOUND_DATA.code;
                    error.index = 0;
                    deferred.reject(error);
                } else {
                    if (results[0].video_st === "deleted") {
                        status = ErrorHandler.DELETED_DATA.status;
                        error.code = ErrorHandler.DELETED_DATA.code;
                        error.index = 0;
                        deferred.reject(error);
                    } else {
                        rtn_val.result = results[0];
                        setResults(rtn_val.result);
                        deferred.resolve();
                    }
                }
            });
            return deferred.promise;
        })
        .then(function () {
            var deferred = Q.defer();
            DB.query(qry_inst, arr_param_inst, function (err, results) {
                if (err) {
                    deferred.reject(err);
                } else {
                    deferred.resolve();
                }
            });
            return deferred.promise;
        })
        .then(function () {
            status = 200;
            return res.send(status, debug.wrap(req, res, status, rtn_val));
        })
        .catch(function (err) {
            status = (status) ? status : 500;
            return res.send(status, debug.wrap(req, res, status, err));
        });
    });

    var setResults = function (result) {

        if(result.video) {
            result.video = result.video.replace('%', '?');
            result.video = sails.config.ftp.domain + '/' + result.video;
        }

        if(result.animation) {
            result.animation = result.animation.replace('%', '?');
            result.animation = sails.config.ftp.domain + '/' + result.animation;
        }

        if(result.capture) {
            result.capture = result.capture.replace('%', '?');
            result.capture = sails.config.ftp.domain + '/' + result.capture;
        }

        if(result.capture_photo) {
            result.capture = result.capture_photo.replace('%', '?');
            result.capture = sails.config.ftp.domain + '/' + result.capture;
        }

        if(result.background) {
            result.background = result.background.replace('%', '?');
            result.background = sails.config.ftp.domain + '/' + result.background;
        }

        if(result.video) {
            result.type = 'video';
        } else {
            result.type = 'capture';
        }

        result.version = -1;

        delete result.video_st;
        delete result.capture_photo;
    }
}

function fitvUpdate(videoInfo) {
    var deferred = Q.defer();
    var qry;
    var params = [];
    var files = videoInfo.files;
    var isVideo = false;
    for (var i = 0; i < files.length; i++) {
        if (files[i] === 'video') {
            isVideo = true;
            break;
        }
    }

    if (isVideo) {
        qry = "update sv_fitv set fitv_sex = ?, fitv_kids = ?, fitv_duration = sec_to_time(?) where fitv_serial = ?";
        params.push(videoInfo.videoSex, videoInfo.duration, videoInfo.videoSerial);
    } else {
        qry = "update sv_fitv set fitv_sex = ?, fitv_kids = ? where fitv_serial = ?";
        params.push(videoInfo.videoSex, videoInfo.videoSerial);
    }

    if (!videoInfo.duration || videoInfo.duration === 0) {
        videoInfo.duration = 20;
    }

    DB.query(qry, params, function (err, result) {
        if (err) {
            deferred.reject(err);
        } else {
            return deferred.resolve();
        }
    }
    );

    return deferred.promise;
}

function s3Replace(videoInfo) {
    var deferred = Q.defer();
    var S3 = new S3Handler('data');
    videoInfo.S3 = S3;
    videoInfo.disk = "USER/" + videoInfo.videoSex + "/" + videoInfo.guid + "/";
    videoInfo.oldDisk = "USER/" + videoInfo.videoSexOld + "/" + videoInfo.guid + "/";
    videoInfo.newDisk = "USER/" + videoInfo.videoSex + "/" + videoInfo.guid + "/";

    var files = videoInfo.files;

    if (videoInfo.videoSex === videoInfo.videoSexOld) {

        var promises = [];
        for (var i = 0; i < files.length; i++) {
            var promise = doS3Upload(videoInfo, files[i]);
            promises.push(promise);
        }

        Q.all(promises)
        .then(function () {
            sails.log.debug('s3 uploading done...:: guid---' + videoInfo.guid + ' ::: localDir---' + videoInfo.localDir);
            deferred.resolve();
        }, function (err) {
            sails.log.error('s3 upload failure');
            deferred.reject(err);
        });
    } else {
        Q.fcall(function () {
            return FittingService.s3CopyFolder(videoInfo.S3, videoInfo.oldDisk, videoInfo.newDisk);
        })
        .then(function () {
            return FittingService.s3Replace(videoInfo);
        })
        .then(function () {
            var target = videoInfo.oldDisk.substring(0, videoInfo.oldDisk.length - 1);
            return FittingService.s3DeleteFolder(videoInfo.S3, target);
        })
        .then(function () {
            deferred.resolve();
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    }

    return deferred.promise;
}

function updateFile(videoInfo) {
    var deferred = Q.defer();

    var type = 'update';
    var qry = FittingService.getSQLForFitvFile(videoInfo, type);

    DB.query(qry,
    function (err, results) {
        if (err) {
            sails.log.error('update fitv_file, fail...');
            deferred.reject(err);
        } else {
            sails.log.debug('update fitv_file, done...');
            deferred.resolve();
        }
    }
    );

    return deferred.promise;
}
