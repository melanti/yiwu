
(function($, angular, _, APP){

  APP.modules.ctrl.tableComponentBrandCtrl = tableComponentBrandCtrl;

  //tableComponentBrandCtrl
  function tableComponentBrandCtrl($scope, _, $timeout, XHR, PubSub, BrandTableManager, API_URL_INFO){
    var vm = this,
      brandTableManager = BrandTableManager.exports,
      pubsubBootstrap,
      btComplete,
      bootstrapCtrlStrArr=[];

    vm.bootstrapCtrl;

    vm._state={
      tableData:brandTableManager.getTableData(),
      bootstrapTableCtrl:{
        complete:{
          //tableOptionCtrl:false,
          //tableBodyCtrl:false,
          //paginationCtrl:false,
        }
      }
    };

    vm.exports={
      COUNT_RANGE:brandTableManager.COUNT_RANGE,
      updateQueryOpt:brandTableManager.updateQueryOpt,
      updateQuery:brandTableManager.updateQuery,
      updateStatus:brandTableManager.updateStatus,
      updatePage:_updatePage,
      deleteRow:_deleteRow
    };

    _init();

    function _init(){
      if((vm.bootstrapCtrl)){
        bootstrapCtrlStrArr = (vm.bootstrapCtrl).split(',');
      }
      bootstrapCtrlStrArr.forEach(function(element, index, array){
        vm._state.bootstrapTableCtrl.complete[element] =false;
      });

      btComplete = vm._state.bootstrapTableCtrl.complete;

      brandTableManager.init();
      $scope.$on('$destroy', function () {
        PubSub.unsubscribe(pubsubBootstrap);
      });
      pubsubBootstrap = PubSub.subscribe('tableComponent:bootstrap', _listenBootstrapState);
    }

    function _listenBootstrapState(topic, data){
      var chk = true;
      btComplete = _.extend({}, btComplete, data);

      _.values(btComplete).forEach(function(element, index, array){
        if(element===false) chk =false;
      });

      if( chk ){
        //console.log('All bootstrap');
        PubSub.unsubscribe(pubsubBootstrap);
        PubSub.trigger('tableComponent:bootstrap:complete',{success:true});
        _updatePage();
      }
    }

    function _updatePage(__options){
      var tmpPage = (__options&&__options.start>=0)?__options.start:brandTableManager.getTableStateInfo('start');
      var tmpViewRange = (__options&&__options.count)?__options.count:brandTableManager.getTableStateInfo('count');
      var opts = brandTableManager.getTableData().options;

      brandTableManager
        .updateTableData({start:tmpPage, count:tmpViewRange, query_opt:opts.query_opt, query:opts.query, status:opts.status})
        .then(
          function(__sucData){
            //__sucData.info.start = (typeof __sucData.info.start==='number')?(__sucData.info.start).toString():__sucData.info.start;
            //__sucData.info.count = (typeof __sucData.info.count==='number')?(__sucData.info.count).toString():__sucData.info.count;
            //__sucData.info.total = (typeof __sucData.info.total==='number')?(__sucData.info.total).toString():__sucData.info.total;
            APP.modules.ctrl.tablePaginationBasicCtrl.beforeChangePage();
            vm._state.tableData = __sucData;
            PubSub.trigger('table:change', brandTableManager.getTableData() );
          },
          function(__errData){
            console.log('__errData: ' , __errData);
            alert( '브랜드 리스트 페이지 호출을 하지 못했습니다.' );  //
          }
        );
    }


    function _deleteRow(){
      var opt = {brand_ids:''},
        strArr = _getCheckedListRow('id'),
        strShopNameArr;

      if(strArr.length<1){
        alert('브랜드를 선택해주세요.');  //'브랜드를 선택해주세요.'
        return false;
      }

      opt.brand_ids = strArr.join(',');
      strShopNameArr = '선택한\n'+_getCheckedListRowName().join(',')+"\n을(를) 삭제하시겠습니까?";
      // strShopNameArr = (APP.i18n["oper.partner.alert.is_delete_shop"]).replace('%s' , '\n'+_getCheckedListRowName().join(',')+'\n')

      if (confirm(strShopNameArr) == true){
        //console.log('brand_ids: ' , opt.brand_ids);

        brandTableManager
          .deleteRow(opt)
          .then(
            function(__sucData){
              //console.log('_deleteRow: ', __sucData);
              alert('삭제하였습니다.');  //삭제하였습니다.
              _updatePage({start:0, count:vm._state.tableData.count});
            },
            function(__errData){
              console.log('tableComponentBasicCtrl _deleteRow: ', __errData);
              alert('삭제가 정상적으로 되지 않았습니다.');  //"삭제가 정상적으로 되지 않았습니다."
            }
          );
      }else{
        return false;
      }
    }


    function _getCheckedListRow(__attr){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push($(this).attr(__attr));
        }
      });
      return strArr;
    }

    function _getCheckedListRowName(){
      var strArr = [];
      $('.table-row-check-box').each(function(){
        if($(this).prop('checked')==true){
          strArr.push( $(this).attr('shop-name')+'('+$(this).attr('uuid')+')' );
        }
      });
      return strArr;
    }

  }

  tableComponentBrandCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'BrandTableManager', 'API_URL_INFO'];
  tableComponentBrandCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      transclude(scope, function(clone, scope) {
        elem.find('.list_table').empty().append(clone);
        //console.log('tableComponent link ctrl: ', ctrl);
      });
    }
  };
  tableComponentBrandCtrl.bindToController= {
    bootstrapCtrl:'@'
  };

})(window.jQuery, window.angular, window._, window.APP);
