<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
<?php if ($output['isOwnShop']) { ?>
  <a href="<?php echo urlShop('store_groupbuy', 'groupbuy_add_vr'); ?>" style="right:100px" class="ncsc-btn ncsc-btn-green" title="E-쿠폰 상품 공동구매 추가"><i class="icon-plus-sign"></i>E-쿠폰 상품 공동구매 추가</a>
  <a href="<?php echo urlShop('store_groupbuy', 'groupbuy_add');?>" class="ncsc-btn ncsc-btn-green" title="<?php echo $lang['groupbuy_index_new_group'];?>"><i class="icon-plus-sign"></i>공동구매 추가</a>
<?php } else { ?>

  <?php if(!empty($output['current_groupbuy_quota'])) { ?>
  <a href="<?php echo urlShop('store_groupbuy', 'groupbuy_add_vr'); ?>" style="right:213px" class="ncsc-btn ncsc-btn-green" title="E-쿠폰 상품 공동구매 추가"><i class="icon-plus-sign"></i>E-쿠폰 상품 공동구매 추가</a>
  <a href="<?php echo urlShop('store_groupbuy', 'groupbuy_add');?>" style="right:100px" class="ncsc-btn ncsc-btn-green" title="<?php echo $lang['groupbuy_index_new_group'];?>"><i class="icon-plus-sign"></i>공동구매 추가</a>
  <a class="ncsc-btn ncsc-btn-acidblue" href="<?php echo urlShop('store_groupbuy', 'groupbuy_quota_add');?>" title="패키지갱신"><i class="icon-money"></i>패키지갱신</a>
  <?php } else { ?>
  <a class="ncsc-btn ncsc-btn-acidblue" href="<?php echo urlShop('store_groupbuy', 'groupbuy_quota_add');?>" title="패키지구매"><i class="icon-money"></i>패키지구매</a>
  <?php } ?>
<?php } ?>

</div>
<?php if ($output['isOwnShop']) { ?>
<div class="alert alert-block mt10">
  <ul class="mt5">
    <li>1. "공동구매 새로추가"를 클릭하시면 새로운 공동구매를 추가하실 수 있습니다.</li>
    <li>2. 공동구매에 E-쿠폰 상품 등록을 원하시면 "E-쿠폰  공동구매 새로추가"를 클릭하여 주십시오.</li>
  </ul>
</div>
<?php } else { ?>
<div class="alert alert-block mt10">
  <?php if(!empty($output['current_groupbuy_quota'])) { ?>
  <strong>패키지 유효기간<?php echo $lang['nc_colon'];?></strong><strong style="color: #F00;"><?php echo date('Y-m-d H:i:s', $output['current_groupbuy_quota']['end_time']);?></strong>
  <?php } else { ?>
  <strong>사용 가능한 패키지가 없습니다. 패키지를 먼저 구매하여 주십시오.</strong>
  <?php } ?>
  <ul class="mt5">
    <li>1. 패키지 구매와 패키지 갱신을 클릭하시면 패키지의 구매 혹은 갱신이 가능합니다.</li>
    <li>2. "공동구매 새로추가"를 클릭하시면 새로운 공동구매를 추가하실 수 있습니다.</li>
    <li>3. 공동구매에 E-쿠폰 상품 등록을 원하시면 "E-쿠폰  공동구매 새로추가"를 클릭하여 주십시오.</li>
    <li>4. <strong style="color: red">관련 비용은 정산시 차감됩니다.</strong></li>
  </ul>
</div>
<?php } ?>

<table class="search-form">
  <form method="get">
    <input type="hidden" name="act" value="store_groupbuy" />
    <tr>
      <td>&nbsp;</td>
      <th>공동구매 카테고리</th>
      <td class="w100">
        <select name="groupbuy_vr" class="w90">
          <option value="">전체</option>
          <option value="0"<?php if ($output['groupbuy_vr'] === '0') echo ' selected'; ?>>공동구매</option>
          <option value="1"<?php if ($output['groupbuy_vr'] === '1') echo ' selected'; ?>>E-쿠폰  공동구매</option>
        </select>
      </td>
      <th>현황</th>
      <td class="w100"><select name="groupbuy_state" class="w90">
          <?php if(is_array($output['groupbuy_state_array'])) { ?>
          <?php foreach($output['groupbuy_state_array'] as $key=>$val) { ?>
          <option value="<?php echo $key;?>" <?php if($key == $_GET['groupbuy_state']) { echo 'selected';}?>><?php echo $val;?></option>
          <?php } ?>
          <?php } ?>
        </select></td>
      <th>공동구매명</th>
      <td class="w160"><input class="text" type="text" name="groupbuy_name" value="<?php echo $_GET['groupbuy_name_ko'];?>"/></td>
      <td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="검색" /></label></td>
    </tr>
  </form>
</table>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w10"></th>
      <th class="w50"></th>
      <th class="tl">공동구매명</th>
      <th class="w130">시작시간</th>
      <th class="w130">종료시간</th>
      <th class="w90">열람수</th>
      <th class="w90">구매</th>
      <th class="w110">현황</th>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($output['group']) && is_array($output['group'])){?>
    <?php foreach($output['group'] as $key=>$group){?>
    <tr class="bd-line">
      <td></td>
      <td><div class="pic-thumb"><a href="<?php echo $group['groupbuy_url'];?>" target="_blank"><img src="<?php echo gthumb($group['groupbuy_image'], 'small');?>"/></a></div></td>
      <td class="tl">
        <dl class="goods-name">
          <dt>
<?php if ($group['is_vr']) { ?>
            <span title="E-쿠폰 교환제품" class="type-virtual">E-쿠폰 그룹</span>
<?php } ?>
            <a target="_blank" href="<?php echo $group['groupbuy_url'];?>"><?php echo $group['groupbuy_name_ko'];?></a>
          </dt>
        </dl>
      </td>
      <td><?php echo $group['start_time_text'];?></td>
      <td><?php echo $group['end_time_text'];?></td>
      <td><?php echo $group['views'];?></td>
      <td><?php echo $group['buy_quantity'];?></td>
      <td><?php echo $group['groupbuy_state_text'];?></td>
    </tr>
    <?php }?>
    <?php }else{?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php }?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
  </tfoot>
</table>
