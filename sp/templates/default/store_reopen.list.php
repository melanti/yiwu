<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['store'];?></h3>
      <ul class="tab-base">
        <li><a href="index.php?act=store&op=store" ><span><?php echo $lang['manage'];?></span></a></li>
        <li><a href="index.php?act=store&op=store_joinin" ><span><?php echo $lang['pending'];?></span></a></li>
        <li><a href="JavaScript:void(0);" class="current" ><span>연장계약</span></a></li>
        <li><a href="index.php?act=store&op=store_bind_class_applay_list" ><span>경영항목신청</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="get" name="formSearch">
    <input type="hidden" value="store" name="act">
    <input type="hidden" value="reopen_list" name="op">
    <table class="tb-type1 noborder search">
      <tbody>
        <tr>
          <th><label><?php echo $lang['store_name'];?></label></th>
          <td><input type="text" value="<?php echo $_GET['store_name'];?>" name="store_name" id="store_name" class="txt"></td>
          <th><label><?php echo $lang['store_name'];?>ID</label></th>
          <td><input type="text" value="<?php echo $_GET['store_id'];?>" name="store_id" id="store_id" class="txt"></td>
          <th>신청상태</th>
          <td>
          <select name="re_state">
          <option value=""><?php echo $lang['nc_please_choose'];?>...</option>
          <option <?php if ($_GET['re_state'] == '0') echo 'selected';?> value="0">심사대기</option>
          <option <?php if ($_GET['re_state'] == '1') echo 'selected';?> value="1">심사중</option>
          <option <?php if ($_GET['re_state'] == '2') echo 'selected';?> value="2">심사완료</option>
          </select>
          </td>
          <td><a href="javascript:document.formSearch.submit();" class="btn-search " title="<?php echo $lang['nc_query'];?>">&nbsp;</a>
        </tr>
        </tbody>
    </table>
</form>
<table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li>업체에 대한 계약을 연장할 수 있습니다며 보기/심사/삭제 조작도 가능합니다.</li>
            <li>심사 통과후 자동으로 만기날자가 추가됩니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form method="post" id="store_form" name="store_form">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <thead>
        <tr class="thead">
          <th>업체/ID</th>
          <th>신청시간</th>
          <th>표준가격(원/년)</th>
          <th>연장시간(년)</th>
          <th>결제금액(년)</th>
          <th>연장일로부터 유효</th>
          <th>상태</th>
          <th>결제번호</th>
          <th>결제메모</th>
          <th><?php echo $lang['operation'];?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!empty($output['reopen_list']) && is_array($output['reopen_list'])){ ?>
        <?php foreach($output['reopen_list'] as $k => $val){ ?>
        <tr class="hover edit">
          <td><?php echo $val['re_store_name'];?>/<?php echo $val['re_store_id'];?></td>
          <td><?php echo date('Y-m-d',$val['re_create_time']); ?></td>
          <td><?php echo number_format($val['re_grade_price'])."원"; ?> ( <?php echo $val['re_grade_name'];?> )</td>
          <td><?php echo $val['re_year']?></td>
          <td><?php echo $val['re_pay_amount'] == 0 ? '무료' : number_format($val['re_pay_amount'])."원";?></td>
          <td>
          <?php if ($val['re_start_time'] != '') {?>
          <?php echo date('Y-m-d',$val['re_start_time']).' ~ '.date('Y-m-d',$val['re_end_time']);?>
          <?php  } ?>
          </td>
          <td><?php echo str_replace(array('0','1','2'),array('결제대기','심사대기','통과심사'),$val['re_state']);?></td>
          <td>
          <?php if ($val['re_pay_cert'] != '') {?>
          <a cbtype="nyroModal" href="<?php echo getStoreJoininImageUrl($val['re_pay_cert']);?>">보기</a>
          <?php } ?>
          </td>
          <td><?php echo $val['re_pay_cert_explain'];?></td>
          <td>
          <?php if ($val['re_state'] == '1') {?>
          <a href="javascript:if(confirm('심사 통과후 자동으로 만기날자가 추가됩니다, 정말 심사하시겠습니까?'))window.location = 'index.php?act=store&op=reopen_check&re_id=<?php echo $val['re_id'];?>';">심사</a>
          <?php } ?>
          <?php echo $val['re_state'] == '1' ? ' | ' : null;?>
         <?php if ($val['re_state'] != '2') {?>
         <a href="javascript:if(confirm('정말 삭제하시겠습니까?'))window.location = 'index.php?act=store&op=reopen_del&re_id=<?php echo $val['re_id'];?>&re_store_id=<?php echo $val['re_store_id'];?>';"><?php echo $lang['nc_del'];?></a>
          <?php } ?>
          </td>
        </tr>
        <?php } ?>
        <?php }else { ?>
        <tr class="no_data">
          <td colspan="10"><?php echo $lang['nc_no_record'];?></td>
        </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td></td>
          <td colspan="15">
              <?php if(!empty($output['reopen_list']) && is_array($output['reopen_list'])){ ?>
              <div class="pagination"><?php echo $output['page'];?></div>
              <?php } ?>
          </td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/custom.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/styles/nyroModal.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">
    $(document).ready(function(){
        $('a[cbtype="nyroModal"]').nyroModal();
    });
</script>