
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.org.ctrl.storeAdminMainCtrl');
  APP.modules.org.ctrl.storeAdminMainCtrl = storeAdminMainCtrl;

  //storeAdminMainCtrl
  function storeAdminMainCtrl($scope, $location, _, $timeout, XHR, PubSub, StoreAdminManager, API_URL_INFO, COMMON_UTIL, Validator, PopupService, OrgListTableManager){
    var vm = this,
        orgAdminManager = StoreAdminManager.exports,
        listURL = APP.info.gURL+'/opr_mng/org/store/list';

    vm.pageType;  //registration(등록) , modification(수정) , confirmation(보기)
    vm.authType;  //admin, store
    vm.accountUuid;
    vm.contactUuid = '';
    vm.orgId;
    vm.accountType = APP.info.user.account_type;

    vm._state={
      parent_org_id:APP.info.user.parent_org_id,
      org_certification:'',
      org_name:'',
      status:0,
      contact_name:'',
      contact_phone:'',
      contact_mail:'',
      contact_homepage:'',
      purchase_url:'',
      show:'debug'
    };

    vm._options = {
      ORG_LIST:[],
    };

    vm.exports={
      imgUrl:APP.info.imgURL,
      gVersion:APP.info.gVersion,
      onChangeOrgList:_onChangeOrgList,
      setParentInfo:_setParentInfo,
      getMinorOrganization:_getMinorOrganization,
      setMinorOrganization:_setMinorOrganization,
      updateMinorOrganization:_updateMinorOrganization,
      goListPage:_goListPage
    };

    _init();

    function _init(){
      console.log('storeAdminMainCtrl: init' );
      console.log('vm.pageType: ' ,vm.pageType);
      console.log('vm.authType: ' ,vm.authType);
      console.log('vm.accountUuid: ' ,vm.accountUuid);
      console.log('vm.orgId: ' ,vm.orgId);

      switch(vm.pageType){
        case 'registration':
          _initPage();
          break;

        case 'modification':
          _initPage();
          _getMinorOrganization();
          break;

        case 'confirmation':
          _getMinorOrganization();
          break;
      }

      function _initPage(){
        orgAdminManager.init();
        vm._state = orgAdminManager.getState();
      }

    }

    function _setParentInfo(){
      return vm._options.parent_contact_name +'  (' + vm._options.parent_contact_phone+')';
    }

    function _getMinorOrganization(){
      orgAdminManager
        .getMinorOrganization({
          account_uuid:vm.accountUuid,
          org_id:vm.orgId
        })
        .then(
          function(__sucData){
            console.log('storeAdminMainCtrl _getMinorOrganization __sucData: ' , __sucData);
            if(  __sucData.results &&  __sucData.results.length>0 ){
              vm._state = _exceptionStateValue(_.extend(vm._state, __sucData.results[0]));
              if(__sucData.results[0].contact_uuid) vm.contactUuid = __sucData.results[0].contact_uuid;
            }
            $('#org_frm').removeClass('opacity_hide');
          },
          function (__failData) {
            PopupService.showPopup({type:'alert', msg:'정보를 가져오지 못했습니다.'});
            console.log('__failData: ' , __failData);
          }
        );
    }

    function _setMinorOrganization(){
      console.log('storeAdminMainCtrl _setMinorOrganization: ');

      orgAdminManager
        .setMinorOrganization( _.extend({},vm._state) )
        .then(
          function(__sucData){
            console.log('storeAdminMainCtrl _setMinorOrganization __sucData: ' , __sucData);

            PopupService
              .showPopup({type:'alert', msg:'등록을 완료헀습니다.'})
              .then(function(){
                window.location.reload(true);
              });

          },
          function (__failData) {
            PopupService.showPopup({type:'alert', msg:'등록을 하지 못했습니다.'});
            console.log('__failData: ' , __failData);
          }
        );

    }

    function _updateMinorOrganization(){
      orgAdminManager
        .updateMinorOrganization( _.extend({},vm._state,{account_uuid:vm.contactUuid,org_id:vm.orgId}) )
        .then(
          function(__sucData){
            console.log('storeAdminMainCtrl _updateMinorOrganization __sucData: ' , __sucData);

            PopupService
              .showPopup({type:'alert', msg:'수정을 완료헀습니다.'})
              .then(function(){
                window.location.reload(true);
              });

          },
          function (__failData) {
            if(__failData.data && __failData.data.code){

              if(__failData.data.code==="ER_DUP_ENTRY"){
                PopupService.showPopup({type:'alert', msg:'입점업체 사업자 코드나 입점업체명이 중복되어 등록을 하지 못했습니다.'});
              }else{
                PopupService.showPopup({type:'alert', msg:'등록을 하지 못했습니다.'});
              }

            }else{
              PopupService.showPopup({type:'alert', msg:'등록을 하지 못했습니다.'});
            }

            console.log('__failData: ' , __failData);
          }
        );

    }

    function _goListPage(){
      window.location.replace( listURL );
    }

    function _onChangeOrgList(){
      var tmpObj = _.find( vm._options.ORG_LIST , function(v){return parseInt(v.org_id,10)===parseInt(vm._state.parent_org_id,10);} )
      if(tmpObj){
        vm._options.parent_contact_name = (tmpObj.contact_name || '');
        vm._options.parent_contact_phone = (tmpObj.contact_phone || '');
      }
    }

    function _exceptionStateValue(__state){
      if(__state.org_certification===null) __state.org_certification = '';
      if(__state.org_name===null) __state.org_name = '';
      if(__state.contact_homepage===null) __state.contact_homepage = '';
      if(__state.contact_mail===null) __state.contact_mail = '';
      if(__state.contact_name===null) __state.contact_name = '';
      if(__state.contact_phone===null) __state.contact_phone = '';
      if(__state.purchase_url===null) __state.purchase_url = '';
      return __state;
    }


  }
  storeAdminMainCtrl.$inject = ['$scope', '$location', '_', '$timeout', 'XHR', 'PubSub', 'StoreAdminManager', 'API_URL_INFO', 'COMMON_UTIL', 'Validator', 'PopupService', 'OrgListTableManager'];
  storeAdminMainCtrl.bindToController = {
    pageType:'@',
    authType:'@',
    accountUuid:'@',
    orgId:'@'
  };
  storeAdminMainCtrl.link = function () {
    return function (scope, elem, attrs, ctrl, transclude) {

    }
  };




})(window.jQuery, window.angular, window._, window.APP);
