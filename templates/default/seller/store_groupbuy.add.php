<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form id="add_form" action="index.php?act=store_groupbuy&op=groupbuy_save" method="post" enctype="multipart/form-data">
    <dl>
      <dt><i class="required">*</i>공동구매명(한국어)： </dt>
      <dd>
        <input class="w400 text" name="groupbuy_name_ko" type="text" id="groupbuy_name_ko" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt>공동구매명(중국어)： </dt>
      <dd>
        <input class="w400 text" name="groupbuy_name" type="text" id="groupbuy_name" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt>공동구매 부주제<?php echo $lang['nc_colon'];?>(한국어)</dt>
      <dd>
        <input class="w400 text" name="remark_ko" type="text" id="remark_ko" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 부주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt>공동구매 부주제<?php echo $lang['nc_colon'];?>(중국어)</dt>
      <dd>
        <input class="w400 text" name="remark" type="text" id="remark" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 부주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>시작시간<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <input id="start_time" name="start_time" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em><span></span>
          <p class="hint"><?php echo '공동구매 시작시간 '.date('Y-m-d H:i', $output['groupbuy_start_time']);?> 이후</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>종료시간<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <input id="end_time" name="end_time" type="text" class="text w130"/><em class="add-on"><i class="icon-calendar"></i></em><span></span>
          <p class="hint">
<?php if (!$output['isOwnShop']) { ?>
          <?php echo '공동구매 종료시간 '.date('Y-m-d H:i', $output['current_groupbuy_quota']['end_time']);?> 이전
          </p>
<?php } ?>

      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>공동구매상품： </dt>
      <dd>
      <div cnbiztype="groupbuy_goods_info" class="selected-group-goods " style="display:none;">
      <div class="goods-thumb"><img id="groupbuy_goods_image" src=""/></div>
          <div class="goods-name">
          <a cnbiztype="groupbuy_goods_href" id="groupbuy_goods_name" href="" target="_blank"></a>
          </div>
          <div class="goods-price">쇼핑몰가：<span cnbiztype="groupbuy_goods_price"></span>원</div>
      </div>
      <a href="javascript:void(0);" id="btn_show_search_goods" class="ncsc-btn ncsc-btn-acidblue">상품선택</a>
      <input id="groupbuy_goods_id" name="groupbuy_goods_id" type="hidden" value=""/>
      <span></span>
      <div id="div_search_goods" class="div-goods-select mt10" style="display: none;">
          <table class="search-form">
              <tr>
                  <th class="w150">
                      <strong>1단계 : 미니샵 내 상품 선택</strong>
                  </th>
                  <td class="w160">
                      <input id="search_goods_name" type="text w150" class="text" name="goods_name" value=""/>
                  </td>
                  <td class="w70 tc">
                      <a href="javascript:void(0);" id="btn_search_goods" class="ncsc-btn"/><i class="icon-search"></i>검색</a></td>
                    <td class="w10"></td>
                    <td>
                        <p class="hint">입력하지 않으시고 검색하시면 미니샵 내 모든 상품이 보여지게 됩니다. 또한 특수상품은 선택이 불가능 합니다.</p>
                    </td>
                </tr>
            </table>
            <div id="div_goods_search_result" class="search-result" style="width:739px;"></div>
            <a id="btn_hide_search_goods" class="close" href="javascript:void(0);">X</a>
        </div>
        <p class="hint">원하시는 공동구매 상품을 선택해 주세요.</br><span class="red">공동구매가 시작되고 난 후 해당상품의 모든 SKU규격은 공동구매 가격과 동일하게 적용됩니다.</span></p>
        </dd>
    </dl>
    <dl cnbiztype="groupbuy_goods_info" style="display:none;">
      <dt><?php echo $lang['groupbuy_index_store_price'].$lang['nc_colon'];?></dt>
      <dd><span cnbiztype="groupbuy_goods_price"></span>원<input id="input_groupbuy_goods_price" type="hidden"></dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>공동구매가： </dt>
      <dd>
        <input class="w70 text" id="groupbuy_price_ko" name="groupbuy_price_ko" type="text" value=""/><em class="add-on">원</em> <span></span>
        <p class="hint">공동구매가 진행될 때 해당 상품의 판매가격을 입력하여 주십시오. <br>0.01~1000000사이의 숫자(단위:원）<br>공동구매가는 배송료를 포함한 가격이며, 공동구매 상품은 시스템에서 기본배송료가 포함되지 않습니다.</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>공동구매이미지<?php echo $lang['nc_colon'];?></dt>
      <dd>
      <div class="ncsc-upload-thumb groupbuy-pic">
          <p><i class="icon-picture"></i>
          <img cnbiztype="img_groupbuy_image" style="display:none;" src=""/></p>
      </div>
        <input cnbiztype="groupbuy_image" name="groupbuy_image" type="hidden" value="">
        <div class="ncsc-upload-btn">
            <a href="javascript:void(0);">
                <span>
                    <input type="file" hidefocus="true" size="1" class="input-file" name="groupbuy_image" cnbiztype="btn_upload_image"/>
                </span>
                <p><i class="icon-upload-alt"></i>업로드</p>
            </a>
        </div>
        <span></span>
        <p class="hint">공동구매 페이지의 이미지로 사용됩니다. <br>너비: 440px 높이:293px 크기 1M이내의 이미지<br>JPG,GIF,PNG 형식 파일 가능</p>
        </dd>
    </dl>
    <dl>
      <dt>공동구매추천이미지<?php echo $lang['nc_colon'];?></dt>
      <dd>
      <div class="ncsc-upload-thumb groupbuy-commend-pic">
          <p><i class="icon-picture"></i>
          <img cnbiztype="img_groupbuy_image" style="display:none;" src=""/></p>
      </div>
        <input cnbiztype="groupbuy_image" name="groupbuy_image1" type="hidden" value="">
        <span></span>
        <div class="ncsc-upload-btn">
            <a href="javascript:void(0);">
                <span>
                    <input type="file" hidefocus="true" size="1" class="input-file" name="groupbuy_image" cnbiztype="btn_upload_image"/>
                </span>
                <p><i class="icon-upload-alt"></i>업로드</p>
            </a>
        </div>
        <p class="hint">공동구매 페이지 측면 추천 위치에 사용되게 됩니다.<br>너비: 210px 높이: 180px 크기 1M이내의 이미지<br>JPG,GIF,PNG 형식 파일 가능</p>
        </dd>
    </dl>
    <dl>
      <dt>카테고리： </dt>
      <dd>
        <select id="class_id" name="class_id" class="w80">
        <option value="0">선택</option>
        </select>
        <select id="s_class_id" name="s_class_id" class="w80">
          <option value="0">선택</option>
        </select>
        <span></span>
        <p class="hint">공동구매 상품의 카테고리를 선택하여 주십시오.</p>
      </dd>
    </dl>
    <dl>
      <dt>E-쿠폰 수량： </dt>
      <dd>
        <input class="w70 text" id="virtual_quantity" name="virtual_quantity" type="text" value="0"/>
        <span></span>
        <p class="hint">E-쿠폰 구매 수량을 입력하여 주십시오. 실제 거래기록에는 영향을 주지 않습니다. </p>
      </dd>
    </dl>
    <dl>
      <dt>1인 구매 한정 수량 ： </dt>
      <dd>
        <input class="w70 text" id="upper_limit" name="upper_limit" type="text" value="0"/>
        <span></span>
        <p class="hint">각 구매자 ID 마다 구매할 수 있는 최대 수량을 입력하여 주십시오.<br>'0'을 입력하시면 수량 제한없음을 뜻합니다.</p>
      </dd>
    </dl>
    <dl>
      <dt>내용</dt>
      <dd>
        <?php showEditor('groupbuy_intro','','740px','360px','','false',false);?>
        <p class="hr8"><a class="des_demo ncsc-btn" href="index.php?act=store_album&op=pic_list&item=groupbuy"><i class="icon-picture"></i>앨범에서선택</a></p>
        <p id="des_demo" style="display:none;"></p>
      </dd>
    </dl>
    <div class="bottom"><label class="submit-border">
      <input type="submit" class="submit" value="완료"></label>
    </div>
  </form>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#start_time').datetimepicker({
        controlType: 'select'
    });

    $('#end_time').datetimepicker({
        controlType: 'select'
    });

    $('#btn_show_search_goods').on('click', function() {
        $('#div_search_goods').show();
    });

    $('#btn_hide_search_goods').on('click', function() {
        $('#div_search_goods').hide();
    });

    //搜索商品
    $('#btn_search_goods').on('click', function() {
        var url = "<?php echo urlShop('store_groupbuy', 'search_goods');?>";
        url += '&' + $.param({goods_name: $('#search_goods_name').val()});
        $('#div_goods_search_result').load(url);
    });

    $('#div_goods_search_result').on('click', 'a.demo', function() {
        $('#div_goods_search_result').load($(this).attr('href'));
        return false;
    });

    //选择商品
    $('#div_goods_search_result').on('click', '[cnbiztype="btn_add_groupbuy_goods"]', function() {
        var goods_commonid = $(this).attr('data-goods-commonid');
        $.get('<?php echo urlShop('store_groupbuy', 'groupbuy_goods_info');?>', {goods_commonid: goods_commonid}, function(data) {
            if(data.result) {
                $('#groupbuy_goods_id').val(data.goods_id);
                $('#groupbuy_goods_image').attr('src', data.goods_image);
                $('#groupbuy_goods_name').text(data.goods_name_ko);
                $('[cnbiztype="groupbuy_goods_price"]').text(data.goods_price_ko);
                $('#input_groupbuy_goods_price').val(data.goods_price_ko);
                $('[cnbiztype="groupbuy_goods_href"]').attr('href', data.goods_href);
                $('[cnbiztype="groupbuy_goods_info"]').show();
                $('#div_search_goods').hide();
            } else {
                showError(data.message);
            }
        }, 'json');
    });

    //업로드
    $('[cnbiztype="btn_upload_image"]').fileupload({
        dataType: 'json',
            url: "<?php echo urlShop('store_groupbuy', 'image_upload');?>",
            add: function(e, data) {
                $parent = $(this).parents('dd');
                $input = $parent.find('[cnbiztype="groupbuy_image"]');
                $img = $parent.find('[cnbiztype="img_groupbuy_image"]');
                data.formData = {old_groupbuy_image:$input.val()};
                $img.attr('src', "<?php echo SHOP_TEMPLATES_URL.'/images/loading.gif';?>");
                data.submit();
            },
            done: function (e,data) {
                var result = data.result;
                $parent = $(this).parents('dd');
                $input = $parent.find('[cnbiztype="groupbuy_image"]');
                $img = $parent.find('[cnbiztype="img_groupbuy_image"]');
                if(result.result) {
                    $img.prev('i').hide();
                    $img.attr('src', result.file_url);
                    $img.show();
                    $input.val(result.file_name);
                } else {
                    showError(data.message);
                }
            }
    });

    jQuery.validator.methods.greaterThanDate = function(value, element, param) {
        var date1 = new Date(Date.parse(param.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };

    jQuery.validator.methods.lessThanDate = function(value, element, param) {
        var date1 = new Date(Date.parse(param.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 > date2;
    };

    jQuery.validator.methods.greaterThanStartDate = function(value, element) {
        var start_date = $("#start_time").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };

    jQuery.validator.methods.lessThanGoodsPrice= function(value, element) {
        var goods_price = $("#input_groupbuy_goods_price").val();
        return Number(value) < Number(getNum(goods_price));
    };

    jQuery.validator.methods.checkGroupbuyGoods = function(value, element) {
        var start_time = $("#start_time").val();
        var result = true;
        $.ajax({
            type:"GET",
            url:'<?php echo urlShop('store_groupbuy', 'check_groupbuy_goods');?>',
            async:false,
            data:{start_time: start_time, goods_id: value},
            dataType: 'json',
            success: function(data){
                if(!data.result) {
                    result = false;
                }
            }
        });
        return result;
    };

    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span');
            error_td.append(error);
        },
        onfocusout: false,
    	submitHandler:function(form){
    		ajaxpost('add_form', '', '', 'onerror');
    	},
        rules : {
            groupbuy_name_ko: {
                required : true
            },
            start_time : {
                required : true,
                greaterThanDate : '<?php echo date('Y-m-d H:i',$output['groupbuy_start_time']);?>'
            },
            end_time : {
                required : true,
<?php if (!$output['isOwnShop']) { ?>
                lessThanDate : '<?php echo date('Y-m-d H:i',$output['current_groupbuy_quota']['end_time']);?>',
<?php } ?>
                greaterThanStartDate : true
            },
            groupbuy_goods_id: {
                required : true,
                checkGroupbuyGoods: true
            },
            groupbuy_price_ko: {
                required : true,
                number : true,
                lessThanGoodsPrice: true,
                min : 0.01,
                max : 1000000
            },
            virtual_quantity: {
                required : true,
                digits : true
            },
            upper_limit: {
                required : true,
                digits : true
            },
            groupbuy_image: {
                required : true
            }
        },
        messages : {
            groupbuy_name_ko: {
                required : '<i class="icon-exclamation-sign"></i>공동구매명을 입력하세요.'
            },
            start_time : {
                required : '<i class="icon-exclamation-sign"></i>공동구매 시작시간을 입력해주세요.',
                greaterThanDate : '<i class="icon-exclamation-sign"></i><?php echo sprintf('공동구매 최소 시작시간:{0}',date('Y-m-d H:i',$output['current_groupbuy_quota']['start_time']));?>'
            },
            end_time : {
                required : '<i class="icon-exclamation-sign"></i>공동구매 종료시간을 입력해주세요.',
<?php if (!$output['isOwnShop']) { ?>
                lessThanDate : '<i class="icon-exclamation-sign"></i><?php echo sprintf('공동구매 최대 종료시간{0}',date('Y-m-d H:i',$output['current_groupbuy_quota']['end_time']));?>',
<?php } ?>
                greaterThanStartDate : '<i class="icon-exclamation-sign"></i>종료시간은 시작시간보다 늦어야 합니다. '
            },
            groupbuy_goods_id: {
                required : '<i class="icon-exclamation-sign"></i><?php echo $lang['group_goods_error'];?>',
                checkGroupbuyGoods: '해당 상품은 이미 프로모션 진행중입니다. '
            },
            groupbuy_price_ko: {
                required : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.',
                number : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.',
                lessThanGoodsPrice: '<i class="icon-exclamation-sign"></i>공동구매 가격은 상품 가격보다 작아야 합니다. ',
                min : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.',
                max : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.'
            },
            virtual_quantity: {
                required : '<i class="icon-exclamation-sign"></i>E-쿠폰 상품 상품 공동구매 수량을 "정수"로 입력하세요.',
                digits : '<i class="icon-exclamation-sign"></iE-쿠폰 상품 상품 공동구매 수량을 "정수"로 입력하세요.'
            },
            upper_limit: {
                required : '<i class="icon-exclamation-sign"></i>수량을 "정수"로 입력하세요.',
                digits : '<i class="icon-exclamation-sign"></i>수량을 "정수"로 입력하세요.'
            },
            groupbuy_image: {
                required : '<i class="icon-exclamation-sign"></i>공동구매 이미지를 등록해주세요. '
            }
        }
    });

	$('#li_1').click(function(){
		$('#li_1').attr('class','active');
		$('#li_2').attr('class','');
		$('#demo').hide();
	});

	$('#goods_demo').click(function(){
		$('#li_1').attr('class','');
		$('#li_2').attr('class','active');
		$('#demo').show();
	});

	$('.des_demo').click(function(){
		if($('#des_demo').css('display') == 'none'){
            $('#des_demo').show();
        }else{
            $('#des_demo').hide();
        }
	});

    $('.des_demo').ajaxContent({
        event:'click', //mouseover
            loaderType:"img",
            loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/loading.gif",
            target:'#des_demo'
    });
});

function insert_editor(file_path){
	KE.appendHtml('goods_body', '<img src="'+ file_path + '">');
}

(function(data) {
    var s = '<option value="0">선택</option>';
    if (typeof data.children != 'undefined') {
        if (data.children[0]) {
            $.each(data.children[0], function(k, v) {
                s += '<option value="'+v+'">'+data['name'][v]+'</option>';
            });
        }
    }
    $('#class_id').html(s).change(function() {
        var ss = '<option value="0">선택</option>';
        var v = this.value;
        if (parseInt(v) && data.children[v]) {
            $.each(data.children[v], function(kk, vv) {
                ss += '<option value="'+vv+'">'+data['name'][vv]+'</option>';
            });
        }
        $('#s_class_id').html(ss);
    });
})($.parseJSON('<?php echo json_encode($output['groupbuy_classes']); ?>'));


</script>
