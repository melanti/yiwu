<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncsc-flow-layout">
  <div class="ncsc-flow-container">
    <div class="title">
      <h3>반품환불 서비스</h3>
    </div>
    <div id="saleRefundReturn">
      <div class="ncsc-flow-step">
        <dl class="step-first current">
          <dt>구매자 반품 신청</dt>
          <dd class="bg"></dd>
        </dl>
        <dl class="<?php echo $output['return']['seller_time'] > 0 ? 'current':'';?>">
          <dt>판매자 반품 신청 처리</dt>
          <dd class="bg"> </dd>
        </dl>
        <dl class="<?php echo ($output['return']['ship_time'] > 0 || $output['return']['return_type']==1) ? 'current':'';?>">
          <dt>구매자 상품 반품</dt>
          <dd class="bg"> </dd>
        </dl>
        <dl class="<?php echo $output['return']['admin_time'] > 0 ? 'current':'';?>">
          <dt>수취확인, 관리자심사</dt>
          <dd class="bg"> </dd>
        </dl>
      </div>
      <div class="ncsc-form-default">
        <h3>구매자 반품환불 신청</h3>
        <dl>
          <dt>반품환불 번호：</dt>
          <dd><?php echo $output['return']['refund_sn']; ?> </dd>
        </dl>
        <dl>
          <dt>신청인(구매자) : </dt>
          <dd><?php echo $output['return']['buyer_name']; ?></dd>
        </dl>
        <dl>
          <dt>반품원인<?php echo $lang['nc_colon'];?></dt>
          <dd> <?php echo $output['return']['reason_info']; ?> </dd>
        </dl>
        <dl>
          <dt>환불금액 : </dt>
          <dd><?php echo $lang['currency'];?><?php echo $output['return']['refund_amount']; ?> </dd>
        </dl>
        <dl>
          <dt>반품수량<?php echo $lang['nc_colon'];?></dt>
          <dd><?php echo $output['return']['return_type']==2 ? $output['return']['goods_num']:'无'; ?></dd>
        </dl>
        <dl>
          <dt>환불설명 : </dt>
          <dd> <?php echo $output['return']['buyer_message']; ?> </dd>
        </dl>
        <dl>
          <dt>증빙서류 업로드 : </dt>
          <dd>
            <?php if (is_array($output['pic_list']) && !empty($output['pic_list'])) { ?>
            <ul class="ncsc-evidence-pic">
              <?php foreach ($output['pic_list'] as $key => $val) { ?>
              <?php if(!empty($val)){ ?>
              <li><a href="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>" cnbiztype="nyroModal" rel="gal" target="_blank"> <img class="show_image" src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>"></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
            <?php } ?>
          </dd>
        </dl>
        <form id="post_form" method="post" action="index.php?act=store_return&op=edit&return_id=<?php echo $output['return']['refund_id']; ?>">
          <input type="hidden" name="form_submit" value="ok" />
          <h3>판매자 처리 의견 </h3>
          <dl>
            <dt><i class="required">*</i>확인여부<?php echo $lang['nc_colon'];?></dt>
            <dd>
              <div>
                <label class="mr20">
                  <input type="radio" class="vm" name="seller_state" value="2" />
                  동의</label>
                <label>
                  <input name="return_type" class="vm" type="checkbox" value="1" />
                  상품포기</label>
                <p class="hint">상품포기를 선택하실 경우, 구매자는 상품을 반품할 필요가 없고, 제출 후 바로 관리자가 환불을 승인합니다. </p>
              </div>
              <div class="mt10">
                <label>
                  <input type="radio" class="vm" name="seller_state" value="3" />
                  거절</label>
              </div>
              <span class="error"></span>
            </dd>
          </dl>
          <dl>
            <dt><i class="required">*</i>비고<?php echo $lang['nc_colon'];?></dt>
            <dd>
              <textarea name="seller_message" rows="2" class="textarea w300"></textarea>
              <span class="error"></span>
              <p class="hint">반품에 동의 하시면 구매자가 상품을 반품하는 상태를 확인하고 상품을 인수 받으세요. (발송 3일 후 미인수 상태를 선택하실 수 있으며, 7일 경과 후 미처리 시 상품포기 상태로 처리됩니다.)<br>
              </p>
            </dd>
          </dl>
          <div class="bottom">
              <label class="submit-border">
                <a class="submit" id="confirm_button">확인</a>
              </label>
              <label class="submit-border">
                <a href="javascript:history.go(-1);" class="submit"><i class="icon-reply"></i>뒤로가기</a>
              </label>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php require template('seller/store_refund_right');?>
</div>
<script type="text/javascript">
$(function(){
    $("#confirm_button").click(function(){
        $("#post_form").submit();
    });
    $('#post_form').validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parentsUntil('dl').find('span.error'));
        },
         submitHandler: function(form) {
			    	ajaxpost('post_form', '', '', 'onerror');
				 },
        rules : {
            seller_state : {
                required   : true
            },
            seller_message : {
                required   : true
            }
        },
        messages : {
            seller_state  : {
                required  : '<i class="icon-exclamation-sign"></i><?php echo $lang['return_seller_confirm_null'];?>'
            },
            seller_message  : {
                required   : '<i class="icon-exclamation-sign"></i><?php echo $lang['return_message_null'];?>'
            }
        }
	    });
});
</script>
