<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 
 */
$lang['cache_cls_operate']		= '캐쉬 비우기';
$lang['cache_cls_all']			= '전체';
$lang['cache_cls_seting']		= '쇼핑몰설정';
$lang['cache_cls_category']		= '상품카테고리';
$lang['cache_cls_adv']			= '광고캐쉬';
$lang['cache_cls_group']		= '공동고매캐쉬(지역|카테고리|가격범위)';
$lang['cache_cls_link']			= '추천사이트';
$lang['cache_cls_nav']			= '하단메뉴';
$lang['cache_cls_index']		= '메인';
$lang['cache_cls_table']		= '테이블구조';
$lang['cache_cls_express']		= '택배회사';
$lang['cache_cls_store_class']	= '미니샵 카테고리';
$lang['cache_cls_store_grade']	= '미니샵 등급';
$lang['cache_cls_circle_level']	= 'SNS맴버등급';
$lang['cache_cls_ok']			= '업데이트 성공';