<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form id="add_form" action="<?php echo urlShop('store_waybill', 'waybill_save');?>" method="post" enctype="multipart/form-data">
    <?php if($output['waybill_info']) { ?>
    <input type="hidden" name="waybill_id" value="<?php echo $output['waybill_info']['waybill_id'];?>">
    <input type="hidden" name="old_waybill_image" value="<?php echo $output['waybill_info']['waybill_image'];?>">
    <?php } ?>
    <dl>
      <dt><i class="required">*</i>템플릿명<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_name']:'';?>" name="waybill_name" id="waybill_name" class="w120 text">
        <span></span>
        <p class="hint">송장 템플릿 명칭, 최대 10자</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>물류회사<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <select name="waybill_express">
          <?php if(!empty($output['express_list']) && is_array($output['express_list'])) {?>
          <?php foreach($output['express_list'] as $value) {?>
          <option value="<?php echo $value['id'];?>|<?php echo $value['e_name'];?>" <?php if($value['selected']) { echo 'selected'; }?> ><?php echo $value['e_name'];?></option>
          <?php } ?>
          <?php } ?>
        </select>
        <span></span>
        <p class="hint">해당 템플릿 물류회사</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>너비<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_width']:'';?>" name="waybill_width" id="waybill_width" class="w60 text"><em class="add-on">mm</em>
        <span></span>
        <p class="hint">송장 너비, 단위 mm</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>높이<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_height']:'';?>" name="waybill_height" id="waybill_height" class="w60 text"><em class="add-on">mm</em>
        <span></span>
        <p class="hint">송장 높이, 단위 mm</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>상단여백<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_top']:'0';?>" name="waybill_top" id="waybill_top" class="w60 text"><em class="add-on">mm</em>
        <span></span>
        <p class="hint">단위 mm</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>좌측여백<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input type="text" value="<?php echo $output['waybill_info']?$output['waybill_info']['waybill_left']:'0';?>" name="waybill_left" id="waybill_left" class="w60 text"><em class="add-on">mm</em>
        <span></span>
        <p class="hint">단위 mm</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>템플릿사진<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <?php if($output['waybill_info']) { ?>
        <img width="500" src="<?php echo $output['waybill_info']['waybill_image_url'];?>">
        <?php } ?>
        <input name="waybill_image" type="file" class="type-file-file" >
        <span></span>
        <p class="hint">송장 이미지를 스캔하여 업로드 하여주시고, 이미지는 반드시 익스프레스 우편 실제 크기와 동일하게 업로드 하여주시기 바랍니다.</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>사용<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <?php
        if(!empty($output['waybill_info']) && $output['waybill_info']['waybill_usable'] == '1') { 
            $usable = 1;
        } else {
            $usable = 0;
        }
        ?>
        <ul class="ncsc-form-radio-list"><li><label for="waybill_usable_1"><input id="waybill_usable_1" type="radio" name="waybill_usable" value="1" <?php echo $usable ? 'checked' : '';?>>
        예</label></li>
        <li><label for="waybill_usable_0"><input id="waybill_usable_0" type="radio" name="waybill_usable" value="0" <?php echo $usable ? '' : 'checked';?>>
        아니오</label></li></ul>
        <span></span>
        <p class="hint">설정을 완료 후 저장하시고 미리보기로 확인하신 후 사용을 하여 주시기 바랍니다. 사용 설정 후에는 바로 사용하실 수 있습니다.</p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border">
        <input type="submit" class="submit" value="완료">
      </label>
    </div>
  </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#waybill_image").change(function(){
		$("#waybill_image_name").val($(this).val());
	});

    $("#submit").click(function(){
        $("#add_form").submit();
    });
    $('#add_form').validate({
        onkeyup: false,
        errorPlacement: function(error, element){
            element.nextAll('span').first().after(error);
        },
        submitHandler:function(form){
            ajaxpost('add_form', '', '', 'onerror');
        },
        rules : {
            waybill_name: {
                required : true,
                maxlength : 10
            },
            waybill_width: {
                required : true,
                digits: true 
            },
            waybill_height: {
                required : true,
                digits: true 
            },
            waybill_top: {
                required : true,
                number: true 
            },
            waybill_left: {
                required : true,
                number: true 
            },
            waybill_image: {
                <?php if(!$output['waybill_info']) { ?>
                required : true,
                <?php } ?>
                accept: "jpg|jpeg|png"
            }
        },
        messages : {
            waybill_name: {
                required : "<i class="icon-exclamation-sign"></i>탬플릿명을 입력하세요. ",
                maxlength : "<i class="icon-exclamation-sign"></i>최대 10자까지 입력가능 합니다. " 
            },
            waybill_width: {
                required : "<i class="icon-exclamation-sign"></i>너비를 입력하세요. ",
                digits: "<i class="icon-exclamation-sign"></i>너비는 숫자로만 입력 가능 합니다."
            },
            waybill_height: {
                required : "<i class="icon-exclamation-sign"></i>높이를 입력하세요.",
                digits: "<i class="icon-exclamation-sign"></i>높이는 숫자로만 입력 가능 합니다. "
            },
            waybill_top: {
                required : "<i class="icon-exclamation-sign"></i>상단여백 값을 입력하세요. ",
                number: "<i class="icon-exclamation-sign"></i>상단여백 값은 숫자로만 입력 가능 합니다. "
            },
            waybill_left: {
                required : "<i class="icon-exclamation-sign"></i>좌측여백 값을 입력하세요. ",
                number: "<i class="icon-exclamation-sign"></i>좌측여백 값은 숫자로만 입력 가능 합니다. "
            },
            waybill_image: {
                <?php if(!$output['waybill_info']) { ?>
                required : '<i class="icon-exclamation-sign"></i>이미지를 등록해 주세요. ',
                <?php } ?>
                accept: '<i class="icon-exclamation-sign"></i>이미지 카테고리가 옳바르지 않습니다. ' 
            }
        }
    });
});
</script> 
