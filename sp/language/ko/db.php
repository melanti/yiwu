<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['db_index_min_size']		= '분리 크기는 최소 10K입니다.';
$lang['db_index_name_exists']	= '백업명이 존재합니다, 다른 이름으로 입력하세요';
$lang['db_index_choose']		= '백업할 테이블을 선택하세요';
$lang['db_index_backup_to_wait']	= '잠깜! 백업중...';
$lang['db_index_back_to_db']	= '돌아가기';
$lang['db_index_backup_succ']	= '백업성공';
$lang['db_index_backuping']		= '백업중...';
$lang['db_index_backup_succ1']	= '분리표기#';
$lang['db_index_backup_succ2']	= '만들기 성공, 자동으로 다음단계가 진행됩니다.';
$lang['db_index_db']			= '디비';
$lang['db_index_backup']		= '백업';
$lang['db_index_restore']		= '복구';
$lang['db_index_backup_method']	= '백업방식';
$lang['db_index_all_data']		= '전체 테이블 백업';
$lang['db_index_spec_table']	= '선택된 테이블 백업';
$lang['db_index_table']			= '테이블';
$lang['db_index_size']			= '분리크기';
$lang['db_index_name']			= '백업명';
$lang['db_index_name_tip']		= '백업명은 1~20의 숫자,영문,언더바로 설정해주세요.';
$lang['db_index_backup_tip']	= '백업전 최대한 사이트 방문을 닫고 진행해주시길 바랍니다, 정말 진행하시겠습니까?';
$lang['db_index_help1']			= '분리 기능은 선택된 테이블만 백업되며 "복구"로 해당 데이터를 복구할 수 있습니다.';
$lang['db_index_help2']			= '데이터는 적어도 하루 한번씩은 백업하시길 권장합니다.';
/**
 * 恢復
 */
$lang['db_restore_file_not_exists']		= '삭제할 파일이 존재하지 않습니다.';
$lang['db_restore_del_succ']			= '백업 삭제 성공';
$lang['db_restore_choose_file_to_del']	= '삭제할 내용을 선택하세요';
$lang['db_restore_backup_time']			= '백업시간';
$lang['db_restore_backup_size']			= '백업크기';
$lang['db_restore_volumn']				= '분리수';
$lang['db_restore_import']				= '불러오기';
/**
 * 導入
 */
$lang['db_import_back_to_list']			= '돌아가기';
$lang['db_import_succ']					= '불러오기 성공';
$lang['db_import_going']				= '불러오는중...';
$lang['db_import_succ2']				= '성공적으로 불러왔습니다, 자동으로 다음단계가 실행됩니다.';
$lang['db_import_fail']					= '불러오기 실패';
$lang['db_import_file_not_exists']		= '불러올 파일이 존재하지 않습니다.';
$lang['db_import_help1']				= '불러오기 옵션으로 데이터를 복구할 수 있습니다.';
/**
 * 刪除
 */
$lang['db_del_succ']	= '백업삭제 성공';