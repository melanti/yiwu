<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){?>
<ul class="goods-list" style="width:760px;">
<?php foreach($output['goods_list'] as $key=>$val){?>
<li><div class="goods-thumb">
    <?php $goods_url = urlShop('goods', 'index', array('goods_id' => $val['goods_id']));?>
    <?php $goods_image_url = thumb($val, 240);?><img src="<?php echo $goods_image_url;?>"/></div>
<dl class="goods-info"><dt><a href="<?php echo $goods_url;?>" target="_blank"><?php echo $val['goods_name_ko'];?></a>
</dt><dd>판매가：<?php echo number_format($val['goods_price_ko']);?>원</dl>
<a cnbiztype="btn_add_mansong_goods" data-goods-id="<?php echo $val['goods_id'];?>" data-goods-name="<?php echo $val['goods_name_ko'];?>" data-goods-image-url="<?php echo $goods_image_url;?>" data-goods-url="<?php echo $goods_url;?>" href="javascript:void(0);" class="ncsc-btn-mini ncsc-btn-green"><i class="icon-ok-circle "></i>사은품으로 선택</a>
</li>
<?php } ?>
</ul>
<div class="pagination"><?php echo $output['show_page']; ?></div>
<?php } else { ?>
<div>내용이 없습니다.</div>
<?php } ?>
