(function($, angular, _, APP){

  angular.module('App.org')
    .service('StoreListTableManager', StoreListTableManager);

  function StoreListTableManager(_, $q , $timeout, $http, XHR, API_URL_INFO, COMMON_UTIL){
    var vm = this;

    var COUNT_RANGE = 5,
        VIEW_RANGE_LIST = APP.info.table.TABLE_VIEW_RANGE_LIST;

    vm._state = {
      tableData: {
        reqModel:{
          search_type:0, // { 0 | 2 | 3 } ( 전체 | 입점업체 명칭 | 담당자명 )
          search_keyword:'',
          parent_org_id:APP.info.user.parent_org_id,
          status:2, //0 : activated, 1 :disabled, 2 : all
          show:'debug'
        },
        filter:{
          orgListArr:[]
        },
        info: {
          start: 0,
          count: 20,
          total: 0
        },
        results: []
      }
    };

    vm.exports = {
      COUNT_RANGE:COUNT_RANGE,
      VIEW_RANGE_LIST:VIEW_RANGE_LIST,
      init:_init,
      getTableData:_getTableData,
      setTableData:_setTableData,
      getTableList:_getTableList,
      updateQueryOpt:_updateQueryOpt,
      updateQuery:_updateQuery

    };

    function _init(){
    }

    function _getTableData(){
      return vm._state.tableData;
    }

    function _setTableData(__data){
      if(typeof __data==='object'){
        var passingData = {};
        if(__data.reqModel) passingData.reqModel = __data.reqModel;
        if(__data.info) passingData.info = __data.info;
        if(__data.results) passingData.results = __data.results;
        if(__data.filter) passingData.filter = __data.filter;
        vm._state.tableData = _.extend(vm._state.tableData, passingData );
      }
    }

    function _getTableList(__data){
      var deferred = $q.defer(),
          params = COMMON_UTIL.getQueryPath( __data, '?' ); //obj, prefix, postfix)

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_MINOR_ORGANIZATION_LIST+params
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _updateQueryOpt(__query_opt){
      vm._state.tableData.options.query_opt =  __query_opt;
    }

    function _updateQuery(__query){
      vm._state.tableData.options.query =  __query;
    }


  }
  StoreListTableManager.$inject= ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', 'COMMON_UTIL'];


})(window.jQuery, window.angular, window._, window.APP);
