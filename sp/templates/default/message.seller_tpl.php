<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['nc_message_set'];?></h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li>플랫폼에 업체에 대해,쪽지,문자,메일 세가지 방식을 통지할 수 있습니다.</li>
            <li>강제 받기가 되면 업체에서는 본 통지를 무시할 수 없습니다.</li>
            <li>쪽지,메일은 업체에서 정확한 번호를 제공후 받을 수 있습니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form name='form1' method='post'>
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="submit_type" id="submit_type" value="" />
    <table class="table tb-type2">
      <thead>
        <tr class="space">
          <th colspan="15" class="nobg"><?php echo $lang['nc_list'];?></th>
        </tr>
        <tr class="thead">
          <th>&nbsp;</th>
          <th><?php echo $lang['mailtemplates_index_desc'];?></th>
          <th class="align-center">쪽지</th>
          <th class="align-center">문자</th>
          <th class="align-center">메일</th>
          <th class="align-center"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!empty($output['mstpl_list'])){?>
        <?php foreach($output['mstpl_list'] as $val){?>
        <tr class="hover">
          <td class="w24">&nbsp;</td>
          <td class="w25pre"><?php echo $val['smt_name']; ?></td>
          <td class="align-center"><?php echo ($val['smt_message_switch']) ? '시동' : '닫기';?></td>
          <td class="align-center"><?php echo ($val['smt_short_switch']) ? '시동' : '닫기';?></td>
          <td class="align-center"><?php echo ($val['smt_mail_switch']) ? '시동' : '닫기';?></td>
          <td class="w60 align-center"><a href="<?php echo urlAdmin('message', 'seller_tpl_edit', array('code' => $val['smt_code']));?>"><?php echo $lang['nc_edit'];?></a></td>
        </tr>
        <?php } ?>
        <?php } ?>
      </tbody>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.edit.js" charset="utf-8"></script> 
<script type="text/javascript">
function go(){
	var url="index.php?act=message&op=email_tpl_ajax";
	document.form1.action = url;
	document.form1.submit();
}
</script>