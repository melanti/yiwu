<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['goods_is_verify']                = '상품 심사 여부';
$lang['goods_index_choose_recommend']	= '추천 내용을 선택하세요';
$lang['goods_index_choose_edit']		= '수정 내용을 선택하세요';
$lang['goods_index_goods_vialotion']	= '상품진열불가';
$lang['nc_common_del_succ']				= '성공 발표 삭제';
$lang['nc_common_del_fail']				= '삭제할 내용을 선택하세요';
$lang['goods_index_argument_invalid']	= '상태 파라미터 오류';
$lang['goods_index_goods']				= '상품';
$lang['goods_index_all_goods']			= '전체상품';
$lang['goods_index_lock_goods']			= '진열불가';
$lang['goods_index_name']				= '상품이름';
$lang['goods_index_store_name']			= '미니샵';
$lang['goods_index_brand']				= '브랜드';
$lang['goods_index_class_name']			= '카테고리';
$lang['goods_index_show']				= '진열';
$lang['goods_index_lock']				= '진열불가';
$lang['goods_index_unchanged']			= '유지';
$lang['goods_index_click']				= '보기';
$lang['goods_index_ensure_handle']		= '정말 본 설정을 진행하시겠습니까?';
$lang['goods_index_help1']				= '상품 미진열시 구매자는 해당 페이지를 열람할 수 없습니다. 판매자 혹은 관리자만이 진열 상태를 설정할 수 있습니다.';
$lang['goods_index_help2']				= '진열 불가로 미진열 상태가 된 상품은 홈페이지에 노출되지 않으며 판매자 진열 위반, 진열불가 상태시 구매자 페이지에는 나타나지 않으며, 관리자만 해당 상품의 상태를 변경할 수 있습니다. ';
/**
 * 상품추천
 */
$lang['goods_recommend_choose_type']	= '추천 유형을 선택하세요';
$lang['goods_recommend_goods_null']		= '추천할 상품을 선택하세요';
$lang['goods_recommend_type_null']		= '추천 유형을 입력하세요';
$lang['goods_recommend_batch_handle']	= '상품 일괄 설정';
$lang['goods_recommend_to']				= '추천함';
/**
 * 상품수정
 */
$lang['goods_edit_goods_null']			= '수정할 상품 정보를 선택하세요';
$lang['goods_edit_batch_succ']			= '일괄 수정 성공';
$lang['goods_edit_not_choose']			= '미 수정시 선택하지마세요';
$lang['goods_edit_keep_blank']			= '미 수정시 공백으로 해주세요';
$lang['goods_edit_lock_state']			= '진열불가 상태';
$lang['goods_edit_keep']				= '유지';
$lang['goods_edit_allow_sell']			= '판매가능';
$lang['goods_edit_lock_reason']			= '진열불가 이유';