<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['spec_index_spec_list']		= '사양 목록';
$lang['spec_index_spec_name']		= '사양';
$lang['spec_index_spec_value']		= '사양 값';
$lang['spec_index_no_checked']		= '설정할 데이터 항목을 선택하세요';
$lang['spec_index_prompts_one']		= '규격은 상품발표규격에 적용되며, 규격 값은 미니샵차체가 추가합니다.';
$lang['spec_index_prompts_two']		= '기본설치중 기본컬러가 자동 추가되며,이 컬러규격만이 제품상세 페이지에서 이미지로 표시되니 삭제하지 마십시오.';
/**
 * 추가규격
 */
$lang['spec_add_spec_add']			= '규격값 추가';
$lang['spec_add_name_no_null']		= '규격 이름을 입력하세요';
$lang['spec_add_name_max']			= '규격 이름은 1~10자 이내로 입력하세요';
$lang['spec_add_sort_no_null']		= '규격 정렬을 입력하세요';
$lang['spec_add_sort_no_digits']	= '정수로 입력하세요';
$lang['spec_index_continue_to_dd']	= '계속 규격 추가';
$lang['spec_index_return_type_list']= '돌아가기';
$lang['spec_index_spec_name_desc']	= '자주 사용하는 구격 이름을 입력하세요; 규격 이름은 반드시 (중국어|한국어)로 입력하세요; 예: 颜色|색상,尺码|사이즈등.';
$lang['spec_index_spec_sort_desc']	= '자연수를 입력하세요. 숫자가 작은 순위로 노출됩니다.';
$lang['spec_common_belong_class']	= '소속카테고리';
$lang['spec_common_belong_class_tips']	= '카테고리 선택, 관련 상위/하위 카테고리를 선택하세요';
/**
 * 수정규격
 */
$lang['spec_edit_spec_value_null']	= '등록된 규격값 정보가 없습니다.';
