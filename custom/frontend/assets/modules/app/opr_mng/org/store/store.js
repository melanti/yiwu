(function($, angular, _, APP){
  'use strict';

  if( $(document.body).attr('data-page-id')=='org_store' ){
    bootStrapStoreAdmin();
  }

  function bootStrapStoreAdmin(){
    console.log('[[bootStrapStoreAdmin:]]');

    angular.module('App')
      .directive( 'storeAdmin', storeAdminDirc );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( (parseInt(APP.info.user.auth.minorOrganization,10)>=1 && window.location.href.indexOf('confirmation')!==-1) || (parseInt(APP.info.user.auth.minorOrganization,10)==2 && (window.location.href.indexOf('registration')!==-1) || (window.location.href.indexOf('modification')!==-1)) ){
            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }
        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }

  function storeAdminDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {
      },
      controller: APP.modules.org.ctrl.storeAdminMainCtrl,
      controllerAs: 'StoreAdminMainCtrl',
      bindToController: APP.modules.org.ctrl.storeAdminMainCtrl.bindToController,
      link: APP.modules.org.ctrl.storeAdminMainCtrl.link(),
      templateUrl: 'store_admin_registration.html'
    };
  }
  storeAdminDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];



})(window.jQuery, window.angular, window._, window.APP);

