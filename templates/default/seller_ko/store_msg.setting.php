<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert alert-block mt10"> <strong>TIP：</strong>
  <ul class="mt5">
    <li>1. 메시지,메일은 정확한 수신 번호를 설정하셔야 정상수신 가능 합니다.</li>
    <li>2. 서브아이디 수신 권한은<a style="color: red" href="<?php echo urlShop('store_account_group', 'group_list');?>" target="_blank">계정그룹</a>에서 설정하세요.</li>
  </ul>
</div>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w10"></th>
      <th>템플릿명칭</th>
      <th class="w300">수신방식</th>
      <th class="w70">운영</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($output['smt_list'])) { ?>
    <?php foreach($output['smt_list'] as $val) { ?>
    <tr class="bd-line">
      <td></td>
      <td class="tl"><strong><?php echo $val['smt_name'];?></strong></td>
      <td><?php echo $val['is_opened'];?></td>
      <td class="nscs-table-handle"><span><a href="javascript:void(0);" class="btn-acidblue" nc_type="dialog" dialog_title="수신설정" dialog_id="msg_setting" dialog_width="480" uri="<?php echo urlShop('store_msg', 'edit_msg_setting', array('code'=>$val['smt_code']));?>"><i class="icon-cog"></i>
        <p>설정</p>
        </a></span></td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php } ?>
  </tbody>
  <tfoot>
    <?php if (!empty($output['brand_list'])) { ?>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
    <?php } ?>
  </tfoot>
</table>
