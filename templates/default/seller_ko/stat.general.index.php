<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="alert mt10" style="clear:both;">
	<ul class="mt5">
		<li>1. 다음 조건에 한 항목이라도 해당되는 주문은 유효한 주문입니다. (1) 온라인상에서 이미 지불이 완료된 경우 (2) 상품수령 후 결제완료한 주문</li>
        <li>2. 주문서와 주문상품에 대한 최근 30일 간 통계 수치 : 전일부터 최근 30일 간의 유효한 주문</li>
    </ul>
</div>
<div class="alert alert-info mt10" style="clear:both;">
    <ul class="mt5">
    <li>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="전일부터 최근 30일 간의 유효한 주문서의 총 금액" class="tip icon-question-sign"></i>
    		최근 30일 주문금액：<strong><?php echo $output['statnew_arr']['orderamount'].'원';?></strong>
    	</span>
		<span class="w210 fl h30" style="display:block;">
			<i title="전일부터 최근 30일 간의 유효한 주문서의 총 회원수" class="tip icon-question-sign"></i>
			최근 30일 주문 회원수：<strong><?php echo $output['statnew_arr']['ordermembernum'];?></strong>
		</span>
		<span class="w210 fl h30" style="display:block;">
			<i title="전일부터 최근 30일 간의 유효한 주문서의 총 주문량" class="tip icon-question-sign"></i>
			최근 30일 주문량：<strong><?php echo $output['statnew_arr']['ordernum'];?></strong>
		</span>
		<span class="w210 fl h30" style="display:block;">
			<i title="전일부터 최근 30일 간의 유효한 주문서의 총 상품 수량" class="tip icon-question-sign"></i>
			최근 30일 주문상품 수량：<strong><?php echo $output['statnew_arr']['ordergoodsnum'];?></strong>
		</span>
    </li>
    <li>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="전일부터 최근 30일 간의 유효한 각 주문서 거래금액의 평균값" class="tip icon-question-sign"></i>
    		1인평균구매가：<strong><?php echo $output['statnew_arr']['avgorderamount'].'원';?></strong>
    	</span>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="전일부터 최근 30일 간 성사된 거래의 상품 가격 평균값" class="tip icon-question-sign"></i>
    		평균가격：<strong><?php echo $output['statnew_arr']['avggoodsprice'].'원';?></strong>
    	</span>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="미니샵 내 모든 상품의 소장 수량" class="tip icon-question-sign"></i>
    		상품소장량：<strong><?php echo $output['statnew_arr']['gcollectnum'];?></strong>
    	</span>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="미니샵 내 보유중인 상품 수량（상품 종류만 계산, 재고는 통계에서 제외）" class="tip icon-question-sign"></i>
    		상품총수량：<strong><?php echo $output['statnew_arr']['goodsnum'];?></strong>
    	</span>
    </li>
    <li>
<?php if(C('site_scm')):?>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="미니샵 총 소장 수량" class="tip icon-question-sign"></i>
    		미니샵소장량：<strong><?php echo $output['statnew_arr']['store_collect'];?></strong>
    	</span>
<?php endif;?>
    	<span class="w400 fl h30" style="display:block;">
    		<i title="전일부터 최근 30일 간의 유효한 주문서의 빈번한 시간대" class="tip icon-question-sign"></i>
    		판매최고시간대：<strong><?php echo ($t = $output['statnew_arr']['hothour'])?$t:'없음';?></strong>
    	</span>
    </li>
  </ul>
  <div style="clear:both;"></div>
</div>

<div id="container"></div>

<div class="w450 fl mr50">
	<div class="alert alert-info" style="margin-bottom:0px;"><strong>보급제안상품</strong>
		&nbsp;<i title="전일부터 7일 이내 TOP30 상품, 이하 상품들의 확대를 통한 수익률 상승 기대" class="tip icon-question-sign"></i>
	</div>
    <table class="ncsc-default-table">
      <thead>
        <tr class="sortbar-array">
        	<th class="align-center">순서</th>
        	<th class="align-center">상품명</th>
        	<th class="align-center">판매량</th>
        </tr>
      </thead>
      <tbody id="datatable">
      	<?php if (!empty($output['goodstop30_arr']) && is_array($output['goodstop30_arr'])) { ?>
            <?php foreach($output['goodstop30_arr'] as $k=>$v) { ?>
            <tr class="bd-line">
            	<td class="w50"><?php echo $k+1;?></td>
            	<td class="tl">
            		<span class="over_hidden w340 h20">
            			<a href="<?php echo urlShop('goods', 'index', array('goods_id' => $v['goods_id']));?>" target="_blank"><?php echo $v['goods_name'];?></a>
            		</span>
            	</td>
            	<td class="w50"><?php echo $v['ordergoodsnum'];?></td>
            </tr>
            <?php } ?>
        <?php } else { ?>
        <tr>
        	<td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
<div class="w450 fl">
	<div class="alert alert-info" style="margin-bottom:0px;"><strong>동종업계 히트상품</strong>
		&nbsp;<i title="유사한 경영 카테고리를 가진 동종업계 미니샵의 히트상품에 대한 이해는 상품 구조 조정에 용이 " class="tip icon-question-sign"></i>
	</div>
    <table class="ncsc-default-table">
      <thead>
        <tr class="sortbar-array">
        	<th class="align-center">순서</th>
        	<th class="align-center">상품명</th>
        	<th class="align-center">판매량</th>
        </tr>
      </thead>
      <tbody id="datatable">
      	<?php if (!empty($output['othergoodstop30_arr']) && is_array($output['othergoodstop30_arr'])) { ?>
            <?php foreach($output['othergoodstop30_arr'] as $k=>$v) { ?>
            <tr class="bd-line">
            	<td class="w50"><?php echo $k+1;?></td>
            	<td class="tl">
            		<span class="over_hidden w340 h20">
            			<a href="<?php echo urlShop('goods', 'index', array('goods_id' => $v['goods_id']));?>" target="_blank"><?php echo $v['goods_name'];?></a>
            		</span>
            	</td>
            	<td class="w50"><?php echo $v['ordergoodsnum'];?></td>
            </tr>
            <?php } ?>
        <?php } else { ?>
        <tr>
        	<td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
<div class="h30 cb">&nbsp;</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>
 
<script>
$(function(){
	//Ajax提示
    $('.tip').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'top',
        offsetY: 5,
        allowTipHover: false
    });
    
	$('#container').highcharts(<?php echo $output['stattoday_json'];?>);
});
</script>
