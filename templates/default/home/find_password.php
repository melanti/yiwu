<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/taglibs.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/tabulous.js"></script>

<div class="nc-login-layout">
  <div class="left-pic" style="margin:6px 0 !important;"> <img src="<?php echo $output['lpic'];?>"  border="0"> </div>
  <div class="nc-login">
    <div class="arrow"></div>
    <div class="nc-password-mode">
      <ul class="tabs-nav">
        <li><a href="#default" class="tabulous_active"><?php echo $lang['login_index_find_password'];?><i></i></a></li>
                <li><a class="sms_find" title="手机找回密码" href="#mobile">手机找回密码<i></i></a></li><span class="tabulousclear"></span>
              </ul>
      <div class="tabs-container" id="tabs_container" style="height: 233px;">
        <div class="tabs-content" id="default" style="position: absolute; top: 0px;">
          <form id="find_password_form" method="POST" action="index.php?act=login&op=find_password" class="nc-login-form">
        <?php Security::getToken();?>
        <input type="hidden" name="form_submit" value="ok" />
        <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
            <dl>
              <dt><?php echo $lang['login_password_you_account'];?></dt>
              <dd>
                <input type="text" placeholder="输入您已注册的用户名" name="username" class="text" value="" style="color: rgb(187, 187, 187);">
              </dd>
            </dl>
            <dl>
              <dt><?php echo $lang['login_password_you_email'];?></dt>
              <dd>
                <input type="text" placeholder="输入您已注册的邮箱" name="email" class="text" value="" style="color: rgb(187, 187, 187);">
              </dd>
            </dl>
            <div class="code-div mt15">
              <dl>
                <dt><?php echo $lang['login_register_code'];?></dt>
                <dd>
                  <input type="text" placeholder="输入验证码" size="10" id="captcha" class="text w100" name="captcha" value="" style="color: rgb(187, 187, 187);">
                </dd>
              </dl>
              <span><img id="codeimage" name="codeimage" title="<?php echo $lang['login_index_change_checkcode'];?>" src="index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>"> <a onclick="javascript:document.getElementById('codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();" href="javascript:void(0);" class="makecode">看不清，换一张</a></span></div>
            <div class="submit-div">
              <input type="submit" id="Submit" name="Submit" value="重置密码" class="submit">
                <input type="hidden" value="<?php echo $output['ref_url']?>" name="ref_url">
            </div>
          </form>
        </div>
        <div class="tabs-content hideflip" id="mobile" style="position: absolute; top: 0px;">
        <form action="index.php?act=connect_sms&op=find_password" class="nc-login-form" method="post" id="post_form">
        <?php Security::getToken();?>
        <input type="hidden" name="form_submit" value="ok" />
        <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
            <dl>
              <dt>手机号：</dt>
              <dd>
                <input type="text" placeholder="输入您已注册的手机号" id="phone" name="phone" value="" autocomplete="off" class="text" style="color: rgb(187, 187, 187);">
              </dd>
            </dl>
            <div class="code-div">
              <dl>
                <dt>验证码：</dt>
                <dd>
                  <input type="text" placeholder="输入验证码" size="10" id="image_captcha" class="text w100" name="captcha" value="" style="color: rgb(187, 187, 187);">
              </dd></dl>
              <span><img id="sms_codeimage" name="codeimage" title="" src="index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>"> <a onclick="javascript:document.getElementById('sms_codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();" href="javascript:void(0);" class="makecode">看不清，换一张</a></span>
              
            </div>
            <div class="tiptext">正确输入上方验证码后，点击<span id="sms_text"> <a onclick="get_sms_captcha('3')" href="javascript:void(0);"><i class="icon-mobile-phone"></i>发送短信验证</a></span>，查收短信将系统发送的“6位手机动态码”输入到下方验证后登录。</div>
            <dl>
              <dt>短信验证：</dt>
              <dd>
                <input type="text" placeholder="输入动态码" size="15" id="sms_captcha" class="text w100" autocomplete="off" name="sms_captcha" value="" style="color: rgb(187, 187, 187);">
              </dd>
            </dl>
            <dl>
              <dt>新密码：</dt>
              <dd>
                <input type="text" placeholder="输入您修改的密码" class="text" id="password" name="password" value="" style="color: rgb(187, 187, 187);">
              </dd>
            </dl>
            <div class="submit-div">
              <input type="submit" value="确认重置" class="submit" id="submit">
            </div>
          </form>
        </div>
              </div>
    </div>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
$(function(){
            jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[^:%,'\*\"\s\<\>\&]+$/i.test(value);
    }, "Letters only please"); 
    jQuery.validator.addMethod("lettersmin", function(value, element) {
      return this.optional(element) || ($.trim(value.replace(/[^\u0000-\u00ff]/g,"aa")).length>=3);
    }, "Letters min please"); 
    jQuery.validator.addMethod("lettersmax", function(value, element) {
      return this.optional(element) || ($.trim(value.replace(/[^\u0000-\u00ff]/g,"aa")).length<=15);
    }, "Letters max please");

    //初始化Input的灰色提示信息  
    // $('input[tipMsg]').inputTipText({pwd:'password'});
    //找回密码方式切换
    $('.nc-password-mode').tabulous({
         effect: 'flip'//动画反转效果
    }); 
    var div_form = '#default';
    $(".nc-password-mode .tabs-nav li a").click(function(){
        if($(this).attr("href") !== div_form){
            div_form = $(this).attr('href');
            $(""+div_form).find(".makecode").trigger("click");
        }
    });
    
    $('#Submit').click(function(){
        if($("#find_password_form").valid()){
            ajaxpost('find_password_form', '', '', 'onerror');
        } else{
            document.getElementById('codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&type=50,120&op=makecode&nchash=<?php echo getNchash();?>&t=' + Math.random();
        }
    });
    $('#find_password_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
        rules : {
            username : {
                required : true,
                lettersmin : true,
                lettersmax : true,
                lettersonly : true,
            },
            email : {
                required : true,
                email    : true
            },
            captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : 'index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#captcha').val();
                        }
                    }
                }
            } 
        },
        messages : {
            username : {
                required : '<?php echo $lang['login_register_input_username'];?>',
                lettersmin : '<?php echo $lang['login_register_username_range'];?>',
                lettersmax : '<?php echo $lang['login_register_username_range'];?>'
            },
            email  : {
                required : '<?php echo $lang['login_register_input_email'];?>',
                email    : '<?php echo $lang['login_register_invalid_email'];?>',
            },
            captcha : {
                required : '<?php echo $lang['login_usersave_code_isnull']  ;?>',
                minlength : '<?php echo $lang['login_usersave_wrong_code'];?>',
                remote   : '<?php echo $lang['login_usersave_wrong_code'];?>'
            }
        }
    });
});
</script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/connect_sms.js" charset="utf-8"></script>
<script>
$(function(){
    $("#post_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
        submitHandler:function(form){
            ajaxpost('post_form', '', '', 'onerror');
        },
        onkeyup: false,
        rules: {
            phone: {
                required : true,
                mobile : true
            },
            captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : 'index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#image_captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                            document.getElementById('sms_codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&type=50,120&op=makecode&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            },
            sms_captcha: {
                required : function(element) {
                    return $("#captcha").val().length == 4;
                },
                minlength: 6
            },
            password : {
                required : function(element) {
                    return $("#sms_captcha").val().length == 6;
                },
                minlength: 6,
                maxlength: 20
            }
        },
        messages: {
      phone: {
                required : '请输入正确的手机号',
                mobile : '请输入正确的手机号'
            },
      captcha : {
                required : '<?php echo $lang['login_register_input_text_in_image'];?>',
                minlength: '<?php echo $lang['login_register_input_text_in_image'];?>',
        remote   : '<?php echo $lang['login_register_input_text_in_image'];?>'
            },
      sms_captcha: {
                required : '请输入六位短信动态码',
                minlength: '请输入六位短信动态码'
            },
            password  : {
                required : '<?php echo $lang['login_register_input_password'];?>',
                minlength: '<?php echo $lang['login_register_password_range'];?>',
                maxlength: '<?php echo $lang['login_register_password_range'];?>'
            }
        }
    });
});
</script>