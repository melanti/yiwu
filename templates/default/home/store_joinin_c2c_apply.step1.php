<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){

    $('#btn_apply_agreement_next').on('click', function() {
        if($('#input_apply_agreement').prop('checked')) {
            $('#apply_agreement').hide();
            $('#apply_company_info').show();
        } else {
            alert('약관에 동의해주세요');
        }
    });

    $('#form_company_info').validate({
        errorPlacement: function(error, element){
            element.nextAll('span').first().after(error);
        },
        rules : {
            seller_name: {
                required: true,
                maxlength: 20 
            },
            seller_passwd: {
                required: true,
                maxlength: 60 
            },
            seller_passwd2: {
                required: true,
                maxlength: 60,
                equalTo  : '#seller_passwd'
            },
            company_name: {
                required: true,
                maxlength: 50 
            },
            company_address: {
                required: true,
                maxlength: 50 
            },
            company_address_detail: {
                required: true,
                maxlength: 50 
            },
            contacts_name: {
                required: true,
                maxlength: 20 
            },
            contacts_phone: {
                required: true,
                maxlength: 20 
            },
            contacts_email: {
                required: true,
                email: true 
            },
            business_licence_number: {
                required: true,
                maxlength: 20
            },
            business_sphere: {
                required: true,
                maxlength: 500
            },
            business_licence_number_electronic: {
                required: true
            },
        },
        messages : {
            seller_name: {
                required: '아이디를 입력하세요',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            seller_passwd: {
                required: '비밀번호를 입력하세요',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            seller_passwd2: {
                required: '비밀번호 확인을 입력하세요',
                equalTo: '두번 입력한 비밀번호가 옳바르지 않습니다.',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            company_name: {
                required: '회사명을 입력하세요',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            company_address: {
                required: '지역을 선택하세요',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            company_address_detail: {
                required: '상세주소를 입력하세요',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            contacts_name: {
                required: '이름을 입력하세요',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            contacts_phone: {
                required: '전화번호를 입력하세요',
                maxlength: jQuery.validator.format("최대{0}자")
            },
            contacts_email: {
                required: '메일주소를 입력하세요',
                email: '정확한 메일주소를 입력하세요'
            },
        }
    });

    $('#btn_apply_company_next').on('click', function() {
        if($('#form_company_info').valid()) {
            $('#form_company_info').submit();
        }
    });
});
</script>

<!-- 公司信息 -->

<div id="apply_company_info" class="apply-company-info">
  <div class="alert">
    <h4>주의사항: </h4>
    로그인 정보는 입점후 관리자 페이지에 접속할 수 있는 계정입니다.</div>
  <form id="form_company_info" action="index.php?act=store_joinin_c2c&op=step2" method="post" enctype="multipart/form-data" >
    <table border="0" cellpadding="0" cellspacing="0" class="all">
    <thead>
      <th colspan="2">로그인 정보</th>
    </thead>
    <tbody>
              <tr>
            <th class="w150"><i>*</i>아이디: </th>
            <td><input id="seller_name" name="seller_name" type="text" class="w200"/>
            <span></span>
          </tr>
        <tr>
            <th class="w150"><i>*</i>비밀번호: </th>
            <td><input id="seller_passwd" name="seller_passwd" type="password" class="w200"/>
            <span></span>
          </tr>
        <tr>
            <th class="w150"><i>*</i>비밀번호 확인: </th>
            <td><input id="seller_passwd2" name="seller_passwd2" type="password" class="w200"/>
            <span></span>
          </tr>
        </tbody>
      <tfoot>
        <tr>
          <td colspan="20">&nbsp;</td>
        </tr>
      </tfoot>
    </table>

    <table border="0" cellpadding="0" cellspacing="0" class="all">
      <thead>
        <tr>
          <th colspan="2">점포 및 연락처정보</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th><i>*</i>회사명: </th>
          <td><input name="company_name" type="text" class="w200"/>
            <span></span></td>
        </tr>
        <tr>
          <th><i>*</i>거주지: </th>
          <td>
            <!-- Korea주소 검색 S -->
          <dl>
            <dd>
              <p style="margin-bottom:8px;">
              <input type="text" class="text w60" id="postcode1" name="postcode1"> -
              <input type="text" class="text w60" id="postcode2" name="postcode2">
              <input type="button" class="button" onclick="execDaumPostcode()" value="우편번호 찾기">
              </p>
              <p style="line-height:38px;">
              <input type="text" class="text w400" id="roadAddress" name="roadAddress" readonly="readonly" style="background:#F2F2F2 none;" placeholder="도로명주소">
              </p>

              <p style="line-height:38px;">
              <input type="text" class="text w400" id="jibunAddress" name="jibunAddress" readonly="readonly" style="background:#F2F2F2 none;" placeholder="지번주소">
              </p>

              <p style="line-height:38px;">
              <input type="text" class="text w400" id="company_address" name="company_address" readonly="readonly" style="background:#F2F2F2 none;" placeholder="영문주소">
              </p>
            </dd>
          </dl>
          <!-- Korea주소 검색 E -->
            <span></span></td>
        </tr>
        <tr>
          <th><i>*</i>상세주소: </th>
          <td><input name="company_address_detail" id="company_address_detail" type="text" class="w200">
            <span></span></td>
        </tr>
        <tr>
          <th><i>*</i>이름: </th>
          <td><input name="contacts_name" type="text" class="w100" />
            <span></span></td>
        </tr>
        <tr>
          <th><i>*</i>전화번호: </th>
          <td><input name="contacts_phone" type="text" class="w100" />
            <span></span></td>
        </tr>
        <tr>
          <th><i>*</i>메일: </th>
          <td><input name="contacts_email" type="text" class="w200" />
            <span></span></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="20">&nbsp;</td>
        </tr>
      </tfoot>
    </table>
  </form>
  <div class="bottom"><a id="btn_apply_company_next" href="javascript:;" class="btn">다음, 재무정보 등록</a></div>
</div>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
    function execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
 
                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById("postcode1").value = data.postcode1;
                document.getElementById("postcode2").value = data.postcode2;
                document.getElementById("roadAddress").value = data.roadAddress;
                document.getElementById("jibunAddress").value = data.jibunAddress;
                document.getElementById("company_address").value = data.addressEnglish;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById("company_address").focus();
            }
        }).open();
    }
</script>