'use strict';

var request = require('supertest'),
  should = require('should'),
  _ = require('underscore'),
  Q = require('q');

var tmpVersions,
  lib_net,
  FittingController,
  RequestService,
  CommonService,
  curQR = {},
  curVideo = {},
  curVideoList = [],
  testOrgList = [],
  testListReqModel = {
    start: 0,
    count: 20,
    serial: '',
    device_id: '',
    client_id: '',
    client_name: '',
    fxmr_id: '',
    fxmr_name: '',
    video_reg_start_dt: '',
    video_reg_end_dt: '',
    device_reg_start_dt: '',
    device_reg_end_dt: '',
    show: 'debug'
  },
  testGetReqModel = {
    serial: 'MkeYQx8Q',
    device_id: '11111',
    show: 'debug'
  },
  testSetReqModel = {
    activation_key: '',
    secret: '',
    video_sex: '',
    fileInfo: {},
    files: [],
    show: 'debug',
    fields: ['activation_key', 'video_sex', 'show', 'item_codes']
  },
  testSetResModel = {
    serial: ''
  },
  testPutReqModel = {
    activation_key: '',
    secret: '',
    video_sex: '',
    fileInfo: {},
    files: [],
    show: 'debug',
    fields: ['activation_key', 'serial', 'video_sex', 'show'],
    serial: ''
  },
  testPutResModel = {
    serial: ''
  },
  activation_key,
  activation_key_expired;

if (process.env['MODE'] == 'ALL_DEBUG') {
  //Operation Test
  // startTest();
}
startTest();

function startTest() {
  describe('=== B. fitting 테스트 케이스 ===\n', function () {
    testB0();
    /*testB1();*/
    testB2();
    testB21();
    /*testB3();
    testB4();
    testB5();
    testB6();
    testB7();*/
  });
}

function testB0() {
  describe('=== B.0 모듈 설정 === \n', function () {
    it('B.0.1 assignModule', function (done) {
      assignBasicModule();
      assignFittingCtrlModule();

      activation_key = process.env['ACTIVATION_KEY'];
      activation_key_expired = process.env['ACTIVATION_KEY_EXPIRED'];
      done();
    });
  });
}

function testB1() {
  describe('=== B.1 영상목록 테스트 === \n', function () {
    it('B.1.1 검색옵션관련 리스트 - 클라이언트 리스트 호출', function (done) {
      var paramsData = {
        account_uuid: testUserModel.session.account_uuid,
        parent_org_id: testUserModel.session.parent_org_id,
        service: 'admin',
        show: 'debug'
      };

      CommonService.reqTestAPI({
        r: request,
        url: RequestService.API_URL.GET_ORG_LIST + (CommonService.getQueryPath(paramsData, '?')),
        method: 'get',
        expectStatus: 200,
        sailsSid: CommonService.getTestSid(),
        errLogTitle: 'B.1.1 검색옵션관련 리스트 - 클라이언트 리스트 호출 ERR',
        assertCallback: function (body) {
          testOrgList = body.results.concat();
          sails.log.debug('B.1.1  body: ', body);
        },
        done: done
      });
    });

    it('B.1.2 검색옵션관련 리스트 - Mirror 리스트 호출', function (done) {
      var paramsData;
      if (testOrgList.length < 1) {
        done();
      } else {
        paramsData = {
          org_id: 5
          //org_id:testOrgList[0].org_id
        };

        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_SERVICE_ACCOUNT_LIST + (CommonService.getQueryPath(paramsData, '?')),
          method: 'get',
          expectStatus: 200,
          sailsSid: CommonService.getTestSid(),
          errLogTitle: 'B.1.2 검색옵션관련 리스트 - Mirror 리스트 호출 ERR',
          assertCallback: function (body) {
            sails.log.debug('B.1.2  body: ', body);
          },
          done: done
        });
      }

    });

    it('B.1.3 검색옵션관련 리스트 -  영상목록 상단 촬영 건수 (디바이스등록은 안씀)', function (done) {
      sails.log.debug("RequestService.API_URL.GET_FITTING_LIST_CNT: ", RequestService.API_URL.GET_FITTING_LIST_CNT);

      CommonService.reqTestAPI({
        r: request,
        url: RequestService.API_URL.GET_FITTING_LIST_CNT + '?show=debug',
        method: 'get',
        expectStatus: 200,
        sailsSid: CommonService.getTestSid(),
        errLogTitle: 'F.1.3 검색옵션관련 리스트 -  영상목록 상단 촬영/디바이스등록 건수 ERR',
        assertCallback: function (body) {
          // sails.log.debug('F.1.3  body: ' , body);
        },
        done: done
      });

    });

    it('B.1.4 영상목록 호출', function (done) {
      reqGetVideoList(
        CommonService.getQueryPath(testListReqModel, '?'),
        'B.1.4 영상목록 호출 ERROR: ',
        function (body) {
          assertGetVideoListCallback(body);
          curVideoList = body.results.concat();
        },
        done
      );

    });

    it('B.1.5 영상목록 호출 : 10개씩 보기', function (done) {
      reqGetVideoListByCnt(0, 10, 'B.1.5 영상목록 호출 : 10개씩 보기', done);
    });

    it('B.1.6 영상목록 호출 : 30개씩 보기', function (done) {
      reqGetVideoListByCnt(0, 30, 'B.1.6 영상목록 호출 : 30개씩 보기', done);
    });

    it('B.1.7 영상목록 호출 : 50개씩 보기', function (done) {
      reqGetVideoListByCnt(0, 50, 'B.1.7 영상목록 호출 : 50개씩 보기', done);
    });

    it('B.1.8 영상목록 호출 : 100개씩 보기', function (done) {
      reqGetVideoListByCnt(0, 100, 'B.1.8 영상목록 호출 : 100개씩 보기', done);
    });

    it('B.1.9 영상목록 호출 : 10개씩/2page 보기', function (done) {
      if (curVideoList.length > 10) {
        reqGetVideoListByCnt(1, 10, 'B.1.9 영상목록 호출 : 10개씩/2page 보기', done);
      } else {
        done();
      }
    });

    it('B.1.10 영상목록 호출 : 검색분류-QR코드(serial) 검색', function (done) {
      if (curVideoList && curVideoList.length > 0 && curVideoList[0].serial) {
        // sails.log.debug('curVideoList[0]::::==>' , curVideoList[0]);
        reqGetVideoList(
          CommonService.getQueryPath(_.extend({}, testListReqModel, {
            start: 0,
            count: 10,
            serial: curVideoList[0].serial
          }), '?'),
          'B.1.10 영상목록 호출 검색분류-QR코드 검색 ERROR: ',
          function (body) {
            // sails.log.debug('B.1.10 영상목록 호출:: ', body.results);
            assertGetVideoListCallback(body);
            var chkObj = _.find(body.results, function (ele) {
              return ele.serial === curVideoList[0].serial
            });
            should.exist(chkObj);
          },
          done
        );
      } else {
        done();
      }
    });

    it('B.1.11 영상목록 호출 : 검색분류-클라이언트 업체명(client_name) 검색', function (done) {
      if (curVideoList && curVideoList.length > 0 && curVideoList[0].client_name) {
        // sails.log.debug('curVideoList[0]::::==>' , curVideoList[0]);
        reqGetVideoList(
          CommonService.getQueryPath(_.extend({}, testListReqModel, {
            start: 0,
            count: 10,
            client_name: curVideoList[0].client_name
          }), '?'),
          'B.1.11 영상목록 호출 검색분류-클라이언트 업체명(client_name) 검색 ERROR: ',
          function (body) {
            assertGetVideoListCallback(body);
            var chkObj = _.find(body.results, function (ele) {
              return ele.client_name === curVideoList[0].client_name
            });
            should.exist(chkObj);
          },
          done
        );
      } else {
        done();
      }
    });

    it('B.1.12 영상목록 호출 : 검색분류-Fxmirror_id(fxmr_id) 검색', function (done) {
      if (curVideoList && curVideoList.length > 0 && curVideoList[0].fxmr_id) {
        sails.log.debug('curVideoList[0]::::==>', curVideoList[0]);
        reqGetVideoList(
          CommonService.getQueryPath(_.extend({}, testListReqModel, {
            start: 0,
            count: 10,
            fxmr_id: curVideoList[0].fxmr_id
          }), '?'),
          'B.1.12 영상목록 호출 검색분류-클라이언트 업체명(client_name) 검색 ERROR: ',
          function (body) {
            assertGetVideoListCallback(body);
            var chkObj = _.find(body.results, function (ele) {
              return ele.fxmr_id === curVideoList[0].fxmr_id
            });
            should.exist(chkObj);
          },
          done
        );
      } else {
        done();
      }
    });

    it('B.1.13 영상목록 호출(TODO) : 검색분류-(date) 검색 : 기간 2010년 2011년', function (done) {
      if (curVideoList && curVideoList.length > 0 && curVideoList[0].video_reg_dt) {
        // sails.log.debug('curVideoList[0]::::==>' , curVideoList[0]);
        reqGetVideoList(
          CommonService.getQueryPath(_.extend({}, testListReqModel, {
            start: 0,
            count: 10,
            video_reg_start_dt: '2010-01-01 00:00:00',
            video_reg_end_dt: '2011-01-01 00:00:00'
          }), '?'),
          'B.1.13 영상목록 호출(TODO) : 검색분류-(date) 검색 ERROR: ',
          function (body) {
            assertGetVideoListCallback(body);
            // sails.log.debug( 'F.7.1 영상목록 호출(TODO) : 검색분류-(date) 검색: ', body );
            should.equal(body.results.length, 0);
          },
          done
        );
      } else {
        done();
      }
    });

    it('B.1.14 영상목록 호출(TODO) : 검색분류-(date) 검색 :  curVideoList[0].video_reg_dt-4일 ~ curVideoList[0].video_reg_dt', function (done) {
      if (curVideoList && curVideoList.length > 0 && curVideoList[0].video_reg_dt) {
        // sails.log.debug( 'video_reg_dt 0: ',curVideoList[0].video_reg_dt );
        // sails.log.debug( 'video_reg_dt 1: ',( new Date(curVideoList[0].video_reg_dt) ) );
        // sails.log.debug( 'video_reg_dt 2: ', getUTCDateStr( new Date(curVideoList[0].video_reg_dt) ) );

        var tmp_video_reg_start_dt = getUTCDateStr(new Date(new Date(curVideoList[0].video_reg_dt).getTime() - (1000 * 60 * 60 * 24 * 4)));
        var tmp_video_reg_end_dt = getUTCDateStr(new Date(curVideoList[0].video_reg_dt));

        reqGetVideoList(
          CommonService.getQueryPath(_.extend({}, testListReqModel, {
            start: 0,
            count: 100,
            video_reg_start_dt: tmp_video_reg_start_dt,
            video_reg_end_dt: tmp_video_reg_end_dt
          }), '?'),
          'B.1.14 영상목록 호출(TODO) : 검색분류-(date) 검색 ERROR: ',
          function (body) {
            assertGetVideoListCallback(body);
            sails.log.debug('B.1.14  ::::::::::::::::::::::==>', body.results.length);
          },
          done
        );
      } else {
        done();
      }
    });

    it('B.1.15 영상목록 호출(TODO) : 검색분류-(date) 검색 : curVideoList[0].video_reg_dt-10년 ~ curVideoList[0].video_reg_dt-4일', function (done) {
      if (curVideoList && curVideoList.length > 0 && curVideoList[0].video_reg_dt) {

        var tmp_video_reg_start_dt = getUTCDateStr(new Date(new Date(curVideoList[0].video_reg_dt).getTime() - (1000 * 60 * 60 * 24 * 365 * 10)));
        var tmp_video_reg_end_dt = getUTCDateStr(new Date(new Date(curVideoList[0].video_reg_dt).getTime() - (1000 * 60 * 60 * 24 * 4)));

        reqGetVideoList(
          CommonService.getQueryPath(_.extend({}, testListReqModel, {
            start: 0,
            count: 100,
            video_reg_start_dt: tmp_video_reg_start_dt,
            video_reg_end_dt: tmp_video_reg_end_dt
          }), '?'),
          'B.1.15 영상목록 호출(TODO) : 검색분류-(date) 검색 ERROR: ',
          function (body) {
            assertGetVideoListCallback(body);
            sails.log.debug('B.1.15  ::::::::::::::::::::::==>', body.results.length);
          },
          done
        );
      } else {
        done();
      }
    });

  });
}

function testB2() {
  describe('=== B.2 영상조회 테스트 === \n', function () {
    it('B.2.1 영상 호출', function (done) {
      this.timeout(10000);
      reqGetVideo(
        CommonService.getQueryPath(testGetReqModel, '?'),
        200,
        'B.2.1 영상 호출 ERROR: ',
        function (body) {
          assertGetVideoCallback(body);
          curVideo = body;
          sails.log.debug('B.2.1 body:', curVideo);
        },
        done
      );

    });

    it('B.2.2 영상 호출 BLANK_FIELD_ERROR : serial', function (done) {
      this.timeout(10000);
      reqGetVideo(
        CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: ''}), '?'),
        400,
        'B.2.2 영상 호출: ',
        function (body) {
          //assertGetVideoCallback(body);
          curVideo = body;
          sails.log.debug('B.2.2 body:', curVideo);
        },
        done
      );

    });

    it('B.2.3 영상 호출 BLANK_FIELD_ERROR : device_id', function (done) {
      this.timeout(10000);
      reqGetVideo(
        CommonService.getQueryPath(_.extend({}, testGetReqModel, {device_id: ''}), '?'),
        400,
        'B.2.3 영상 호출: ',
        function (body) {
          //assertGetVideoCallback(body);
          curVideo = body;
          sails.log.debug('B.2.3 body:', curVideo);
        },
        done
      );

    });

    it('B.2.4 영상 호출 WRONG_SERIAL', function (done) {
      this.timeout(10000);
      reqGetVideo(
        CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: '11111'}), '?'),
        400,
        'B.2.4 영상 호출: ',
        function (body) {
          assertGetVideoCallback(body);
          curVideo = body;
          sails.log.debug('B.2.4 body:', curVideo);
        },
        done
      );

    });

    it('B.2.4 영상 호출 WRONG_SERIAL', function (done) {
      this.timeout(10000);
      reqGetVideo(
      CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: '2NpgFEqY'}), '?'),
      400,
      'B.2.4 영상 호출: ',
      function (body) {
        assertGetVideoCallback(body);
        curVideo = body;
        sails.log.debug('B.2.4 body:', curVideo);
      },
      done
      );

    });

    it('B.2.4 영상 호출 DELETED_DATA', function (done) {
      this.timeout(10000);
      reqGetVideo(
        CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: 'Px7BGTGX'}), '?'),
        400,
        'B.2.4 영상 호출: ',
        function (body) {
          assertGetVideoCallback(body);
          curVideo = body;
          sails.log.debug('B.2.4 body:', curVideo);
        },
        done
      );

    });
  });
}

function testB21() {
  describe('=== B.2 QR조회 테스트 === \n', function () {
    it('B.2.1 QR 영상 호출', function (done) {
      this.timeout(10000);
      reqGetQRData(
      CommonService.getQueryPath(testGetReqModel, '?'),
      200,
      'B.2.1 QR 영상 호출 ERROR: ',
      function (body) {
        assertGetQRCallback(body);
        curQR = body;
        sails.log.debug('B.2.1 body:', curQR);
      },
      done
      );

    });

    it('B.2.2 QR 캡쳐 호출', function (done) {
      this.timeout(10000);
      reqGetQRData(
      CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: '2NpgFEqY'}), '?'),
      200,
      'B.2.2 QR 캡쳐 호출 ERROR: ',
      function (body) {
        assertGetQRCallback(body);
        curQR = body;
        sails.log.debug('B.2.2 body:', curQR);
      },
      done
      );

    });

    it('B.2.3 QR 호출 BLANK_FIELD_ERROR : serial', function (done) {
      this.timeout(10000);
      reqGetQRData(
      CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: ''}), '?'),
      400,
      'B.2.3 QR 호출: ',
      function (body) {
        //assertGetVideoCallback(body);
        curQR = body;
        sails.log.debug('B.2.3 body:', curQR);
      },
      done
      );

    });

    it('B.2.4 QR 호출 BLANK_FIELD_ERROR : device_id', function (done) {
      this.timeout(10000);
      reqGetQRData(
      CommonService.getQueryPath(_.extend({}, testGetReqModel, {device_id: ''}), '?'),
      400,
      'B.2.4 QR 호출: ',
      function (body) {
        //assertGetVideoCallback(body);
        curQR = body;
        sails.log.debug('B.2.4 body:', curQR);
      },
      done
      );

    });

    it('B.2.5 QR 호출 WRONG_SERIAL', function (done) {
      this.timeout(10000);
      reqGetQRData(
      CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: '11111'}), '?'),
      400,
      'B.2.5 QR 호출: ',
      function (body) {
        //assertGetQRCallback(body);
        curQR = body;
        sails.log.debug('B.2.5 body:', curQR);
      },
      done
      );

    });

    it('B.2.6 QR 호출 DELETED_DATA', function (done) {
      this.timeout(10000);
      reqGetQRData(
      CommonService.getQueryPath(_.extend({}, testGetReqModel, {serial: 'Px7BGTGX'}), '?'),
      400,
      'B.2.6 QR 호출: ',
      function (body) {
        //assertGetQRCallback(body);
        curQR = body;
        sails.log.debug('B.2.6 body:', curQR);
      },
      done
      );

    });
  });
}

function testB3() {
  describe('=== B.3 영상등록 테스트 === \n', function () {
    it('B.3.1 영상 등록', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.video = './.tmp/Video.mp4';
      testSetReqModel.fileInfo.animation = './.tmp/Animation.bin';
      testSetReqModel.fileInfo.capture = './.tmp/Capture.png';
      testSetReqModel.fileInfo.background = './.tmp/Background.png';
      testSetReqModel.files = ['video', 'animation', 'capture', 'background'];

      reqSetVideo(
        testSetReqModel,
        200,
        'B.3.1 영상 등록 ERROR: ',
        function (body) {
          assertSetVideoCallback(body);
        }
      ).then(
        function (data) {
          testSetResModel = data;
          sails.log.debug('B.3.1 body:', testSetResModel);
          done();
        }
      );
    });

    it('B.3.2 영상 등록 - 파일 확장자 변환', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.video = './.tmp/Capture.png';
      testSetReqModel.fileInfo.animation = './.tmp/Capture.png';
      testSetReqModel.fileInfo.capture = './.tmp/Capture.png';
      testSetReqModel.fileInfo.background = './.tmp/Capture.png';
      testSetReqModel.files = ['video', 'animation', 'capture', 'background'];

      reqSetVideo(
        testSetReqModel,
        200,
        'B.3.2 영상 등록 ERROR: ',
        function (body) {
          assertSetVideoCallback(body);
        }
      ).then(
        function (data) {
          testSetResModel = data;
          sails.log.debug('B.3.2 body:', testSetResModel);
          done();
        }
      );
    });

    it('B.3.3 영상 등록 - 파일 개수 변환', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.video = './.tmp/test.mp4';
      testSetReqModel.files = ['video'];

      reqSetVideo(
        testSetReqModel,
        200,
        'B.3.3 영상 등록 ERROR: ',
        function (body) {
          assertSetVideoCallback(body);
        }
      ).then(
        function (data) {
          testSetResModel = data;
          sails.log.debug('B.3.3 body:', testSetResModel);
          done();
        }
      );
    });

  });
}

function testB4() {
  describe('=== B.4 영상수정 테스트 === \n', function () {
    it('B.4.1 영상 등록', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.video = './.tmp/Video.mp4';
      testSetReqModel.fileInfo.animation = './.tmp/Animation.bin';
      testSetReqModel.fileInfo.capture = './.tmp/Capture.png';
      testSetReqModel.fileInfo.background = './.tmp/Background.png';
      testSetReqModel.files = ['video', 'animation', 'capture', 'background'];

      reqSetVideo(
        testSetReqModel,
        200,
        'B.4.1 영상 등록 ERROR: ',
        function (body) {
          assertSetVideoCallback(body);
        }
      ).then(
        function (data) {
          testSetResModel = data.result;
          sails.log.debug('B.4.1 body:', testSetResModel);
          done();
        }
      );
    });

    it('B.4.2 영상 수정 - 성별, 파일수정(비디오,캡쳐)', function (done) {
      this.timeout(30000);
      testPutReqModel.activation_key = activation_key;
      testPutReqModel.serial = testSetResModel.serial;
      testPutReqModel.ip_addr = '127.0.0.1';
      testPutReqModel.video_sex = 'man';
      testPutReqModel.fileInfo.video = './.tmp/Video.mp4';
      testPutReqModel.fileInfo.capture = './.tmp/test.jpg';
      testPutReqModel.files = ['video', 'capture'];

      reqPutVideo(
        testPutReqModel,
        200,
        'B.4.2 영상 수정 ERROR: ',
        function (body) {
          assertPutVideoCallback(body);
        }
      ).then(
        function (data) {
          testPutResModel = data.result;
          sails.log.debug('B.4.2 body:', testPutResModel);
          done();
        }
      );
    });

    it('B.4.3 영상 수정 - 파일수정(비디오)', function (done) {
      this.timeout(30000);
      testPutReqModel.activation_key = activation_key;
      testPutReqModel.serial = testPutResModel.serial;
      testPutReqModel.ip_addr = '127.0.0.1';
      testPutReqModel.video_sex = 'man';
      testPutReqModel.fileInfo.video = './.tmp/test.mp4';
      testPutReqModel.files = ['video'];

      reqPutVideo(
        testPutReqModel,
        200,
        'B.4.3 영상 수정 ERROR: ',
        function (body) {
          assertPutVideoCallback(body);
        }
      ).then(
        function (data) {
          testPutResModel = data.result;
          sails.log.debug('B.4.3 body:', testPutResModel);
          done();
        }
      );
    });
  });
}

function testB5() {
  describe('=== B.5 영상등록 - 비디오 확장자 테스트 === \n', function () {

    it('B.5.1 영상 등록 - 비디오확장자(mp4)', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.video = './.tmp/test.mp4';
      testSetReqModel.files = ['video'];

      reqSetVideo(
        testSetReqModel,
        200,
        'B.5.1 영상 등록 ERROR: ',
        function (body) {
          assertSetVideoCallback(body);
        }
      ).then(
        function (data) {
          testSetResModel = data;
          sails.log.debug('B.5.1 body:', testSetResModel);
          done();
        }
      );
    });

  });
}

function testB6() {
  describe('=== B.6 영상등록 - 액티베이션 테스트 === \n', function () {

    it('B.6.1 영상 등록 - 만료된 키', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key_expired;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.video = './.tmp/test.mp4';
      testSetReqModel.files = ['video'];

      reqSetVideo(
      testSetReqModel,
      400,
      'B.6.1 영상 등록 ERROR: ',
      function (body) {
        assertSetVideoCallback(body);
      }
      ).then(
      function (data) {
        testSetResModel = data;
        sails.log.debug('B.6.1 body:', testSetResModel);
        done();
      }
      );
    });

    it('B.6.2 영상 등록 - 없는 키', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = '11111';
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.video = './.tmp/test.mp4';
      testSetReqModel.files = ['video'];

      reqSetVideo(
      testSetReqModel,
      400,
      'B.6.2 영상 등록 ERROR: ',
      function (body) {
        assertSetVideoCallback(body);
      }
      ).then(
      function (data) {
        testSetResModel = data;
        sails.log.debug('B.6.2 body:', testSetResModel);
        done();
      }
      );
    });

  });
}


function testB7() {
  describe('=== B.7 캡쳐등록 테스트 === \n', function () {
    it('B.7.1 캡쳐 등록', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.fileInfo.capture = './.tmp/Capture.png';
      testSetReqModel.files = ['capture'];

      reqSetCapture(
      testSetReqModel,
      200,
      'B.7.1 캡쳐 등록 ERROR: ',
      function (body) {
        assertSetCaptureCallback(body);
      }
      ).then(
      function (data) {
        testSetResModel = data;
        sails.log.debug('B.7.1 body:', testSetResModel);
        done();
      }
      );
    });

    it('B.7.2 캡쳐 등록 - null 에러', function (done) {
      this.timeout(30000);
      testSetReqModel.activation_key = activation_key;
      testSetReqModel.ip_addr = '127.0.0.1';
      testSetReqModel.video_sex = 'woman';
      testSetReqModel.item_codes = '9,10,11';
      testSetReqModel.fileInfo = {};
      testSetReqModel.files = [];


      reqSetCapture(
        testSetReqModel,
        400,
        'B.7.2 캡쳐 등록 ERROR:',
        function (body) {
          assertSetCaptureCallback(body);
        }
      ).then(
        function (data) {
          testSetResModel = data;
          sails.log.debug('B.7.2 body:', testSetResModel);
          done();
        }
      );
    });
  });
}


function assignBasicModule() {
  tmpVersions = require('../../../package.json');
  lib_net = require('../../../api/controllers/libs/network.js');
  RequestService = require('../../util/RequestService.js');
  CommonService = require('../../util/CommonService.js');
}

function assignFittingCtrlModule() {
  FittingController = require('../../../api/controllers/FittingController.js');
}

function reqGetVideoList(reqParams, errLogTitle, assertCallback, done) {
  return CommonService.reqTestAPI({
    r: request,
    url: RequestService.API_URL.GET_FITTING_LIST + reqParams,
    method: 'get',
    expectStatus: 200,
    sailsSid: CommonService.getTestSid(),
    errLogTitle: errLogTitle,
    assertCallback: assertCallback,
    done: done
  });
}

function reqGetVideoListByCnt(st, cnt, errLogStr, done) {
  return reqGetVideoList(
    CommonService.getQueryPath(_.extend({}, testListReqModel, {start: (st || 0), count: cnt}), '?'),
    errLogStr + ' ERROR: ',
    function (body) {
      // sails.log.debug(errLogStr, body.results.length);
      assertGetVideoListCallback(body);
      if (parseInt(body.info.total, 10) >= cnt) {
        should.equal(parseInt(body.results.length, 10), cnt);
      }
    },
    done
  );
}

function assertGetVideoListCallback(body) {
  body.should.have.property('results');
  body.should.have.property('info');
  body.info.should.have.property('start');
  body.info.should.have.property('count');
  body.info.should.have.property('total');
}

function reqGetVideo(reqParams, expectStatus, errLogTitle, assertCallback, done) {
  return CommonService.reqTestAPI({
    r: request,
    url: RequestService.API_URL.GET_FITTING + reqParams,
    method: 'get',
    expectStatus: expectStatus,
    sailsSid: CommonService.getTestSid(),
    errLogTitle: errLogTitle,
    assertCallback: assertCallback,
    done: done
  });
}

function assertGetVideoCallback(body) {
  body.should.have.property('result');
  body.result.should.have.property('sex');
  body.result.should.have.property('video');
  body.result.should.have.property('animation');
  body.result.should.have.property('capture');
  body.result.should.have.property('background');
  body.result.should.have.property('version');
}

function reqGetQRData(reqParams, expectStatus, errLogTitle, assertCallback, done) {
  return CommonService.reqTestAPI({
    r: request,
    url: RequestService.API_URL.GET_QR_DATA + reqParams,
    method: 'get',
    expectStatus: expectStatus,
    sailsSid: CommonService.getTestSid(),
    errLogTitle: errLogTitle,
    assertCallback: assertCallback,
    done: done
  });
}

function assertGetQRCallback(body) {
  body.should.have.property('result');
  body.result.should.have.property('sex');
  body.result.should.have.property('video');
  body.result.should.have.property('animation');
  body.result.should.have.property('capture');
  body.result.should.have.property('background');
  body.result.should.have.property('type');
  body.result.should.have.property('version');
}

function reqSetVideo(sendData, expectStatus, errLogTitle, assertCallback) {
  var deferred = Q.defer();

  Q.fcall(function () {

      return CommonService.reqTestAPI({
        r: request,
        url: RequestService.API_URL.SET_FITTING,
        method: 'post/multipart',
        sendData: sendData,
        expectStatus: expectStatus,
        sailsSid: CommonService.getTestSid(),
        errLogTitle: errLogTitle,
        assertCallback: assertCallback,
        done: null
      });
    })
    .then(function (data) {
      deferred.resolve(data);
    })
    .catch(function (err) {
      deferred.reject(err);
    });

  return deferred.promise;
}

function assertSetVideoCallback(body) {
  body.should.have.property('result');
  body.result.should.have.property('serial');
}

function reqSetCapture(sendData, expectStatus, errLogTitle, assertCallback) {
  var deferred = Q.defer();

  Q.fcall(function () {

    return CommonService.reqTestAPI({
      r: request,
      url: RequestService.API_URL.SET_CAPTURE,
      method: 'post/multipart',
      sendData: sendData,
      expectStatus: expectStatus,
      sailsSid: CommonService.getTestSid(),
      errLogTitle: errLogTitle,
      assertCallback: assertCallback,
      done: null
    });
  })
  .then(function (data) {
    deferred.resolve(data);
  })
  .catch(function (err) {
    deferred.reject(err);
  });

  return deferred.promise;
}

function assertSetCaptureCallback(body) {
  body.should.have.property('result');
  body.result.should.have.property('serial');
}

function reqPutVideo(sendData, expectStatus, errLogTitle, assertCallback) {
  var deferred = Q.defer();

  Q.fcall(function () {

      return CommonService.reqTestAPI({
        r: request,
        url: RequestService.API_URL.PUT_FITTING,
        method: 'post/multipart',
        sendData: sendData,
        expectStatus: expectStatus,
        sailsSid: CommonService.getTestSid(),
        errLogTitle: errLogTitle,
        assertCallback: assertCallback,
        done: null
      });
    })
    .then(function (data) {
      deferred.resolve(data);
    })
    .catch(function (err) {
      deferred.reject(err);
    });

  return deferred.promise;
}

function assertPutVideoCallback(body) {
  body.should.have.property('result');
  body.result.should.have.property('serial');


  var qry = "select FITV_SEX as fitv_sex, UPD_DT as upd_dt from FITV where FITV_SERIAL = ?";
  DB.query(qry, [body.result.serial], function (err, result) {
    sails.log.debug('::::err ::::' + err);
    sails.log.debug('::::result ::::' + JSON.stringify(result, null, 4));
    should.exist(result[0]);
    result[0].should.have.property('upd_dt');
    result[0].should.have.property('fitv_sex');
    result[0].fitv_sex.should.equal(testPutReqModel.video_sex);
  });

}

function getUTCDateStr(date, separateStr) {
  var separateStr = separateStr || '-';
  var y = date.getUTCFullYear();
  var m = convertNumToStr(date.getUTCMonth() + 1);
  var d = convertNumToStr(date.getUTCDate());
  var h = convertNumToStr(date.getUTCHours());
  var mi = convertNumToStr(date.getUTCMinutes());
  var s = convertNumToStr(date.getUTCSeconds());
  return y + separateStr + m + separateStr + d + ' ' + h + ':' + mi + ':' + s;

  function convertNumToStr(num) {
    return ( num < 10) ? '0' + num : num;
  }
}


