<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form method="post"  action="index.php?act=store_deliver_set&op=free_freight" id="my_store_form">
    <input type="hidden" name="form_submit" value="ok" />
    <dl>
      <dt>무료배송한도<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <input class="text w60" name="store_free_price" maxlength="10" type="text"  id="store_free_price" value="<?php echo $output['store_free_price'];?>" /><em class="add-on"><strong>원</strong></em>
        <p class="hint">금액을 '0'으로 입력하실 경우, '0'원 이상의 주문은 무료배송이 가능하게 됩니다.</p>
      </dd>
    </dl>
    <div class="bottom">
        <label class="submit-border"><input type="submit" class="submit" value="저장" /></label>
      </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script> 
<script type="text/javascript">
var SITEURL = "<?php echo SHOP_SITE_URL; ?>";
$(function(){
	$('#my_store_form').validate({
    	submitHandler:function(form){
    		ajaxpost('my_store_form', '', '', 'onerror')
    	},
		rules : {
			store_free_price: {
			required : true,
			number : true
			}
        },
        messages : {
        	store_free_price: {
				required : '금액입력',
				number : '정확한숫자입력'
			}
        }
    });    
    
});
</script> 
