<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['nc_message_set'];?></h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li>플랫폼에 업체에 대해,쪽지,문자,메일 세가지 방식을 통지할 수 있습니다.</li>
            <li>강제 받기가 되면 업체에서는 본 통지를 무시할 수 없습니다.</li>
            <li>쪽지,메일은 업체에서 정확한 번호를 제공후 받을 수 있습니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <div class="homepage-focus" cbtype="sellerTplContent">
    <h4>모듈이름：<?php echo $output['mmtpl_info']['mmt_name'];?></h4>
    <ul class="tab-menu">
      <li class="current">쪽지템플릿</li>
      <li>문자템플릿</li>
      <li>메일템플릿</li>
    </ul>
    <!-- 站内信 S -->
    <form class="tab-content" method="post" name="message_form" >
      <input type="hidden" name="form_submit" value="ok" />
      <input type="hidden" name="code" value="<?php echo $output['mmtpl_info']['mmt_code'];?>" />
      <input type="hidden" name="type" value="message" />
      <table class="table tb-type2">
        <tbody>
          <tr class="noborder">
            <td colspan="2" class="required"><label>站内信开关:</label></td>
          </tr>
          <tr class="noborder">
            <td class="vatop rowform onoff">
              <label for="message_switch1" class="cb-enable <?php if($output['mmtpl_info']['mmt_message_switch'] == 1){?>selected<?php }?>"><span><?php echo $lang['open'];?></span></label>
              <label for="message_switch0" class="cb-disable <?php if($output['mmtpl_info']['mmt_message_switch'] == 0){?>selected<?php }?>"><span><?php echo $lang['close'];?></span></label>
              <input id="message_switch1" name="message_switch" <?php if($output['mmtpl_info']['mmt_message_switch'] == 1){?>checked="checked"<?php }?> value="1" type="radio">
              <input id="message_switch0" name="message_switch" <?php if($output['mmtpl_info']['mmt_message_switch'] == 0){?>checked="checked"<?php }?> value="0" type="radio"></td>
            <td class="vatop tips"></td>
          </tr>
          <tr>
            <td colspan="2" class="required"><label>내용:</label></td>
          </tr>
          <tr class="noborder">
            <td class="vatop rowform">
              <textarea name="message_content" rows="6" class="tarea"><?php echo $output['mmtpl_info']['mmt_message_content'];?></textarea>
            </td>
            <td class="vatop tips"></td>
          </tr>
        </tbody>
      </table>
      <div class="margintop">
        <a href="JavaScript:void(0);" onclick="document.message_form.submit();" class="btn"><span><?php echo $lang['nc_submit'];?></span></a>
      </div>
    </form>
    <!-- 站内信 E -->
    <!-- 短消息 S -->
    <form class="tab-content" method="post" name="short_name" style="display:none;">
      <input type="hidden" name="form_submit" value="ok" />
      <input type="hidden" name="code" value="<?php echo $output['mmtpl_info']['mmt_code'];?>" />
      <input type="hidden" name="type" value="short" />
      <table class="table tb-type2">
        <tbody>
          <tr class="noborder">
            <td colspan="2" class="required"><label>문자오픈여부:</label></td>
          </tr>
          <tr class="noborder">
            <td class="vatop rowform onoff">
              <label for="short_switch1" class="cb-enable <?php if($output['mmtpl_info']['mmt_short_switch'] == 1){?>selected<?php }?>"><span><?php echo $lang['open'];?></span></label>
              <label for="short_switch0" class="cb-disable <?php if($output['mmtpl_info']['mmt_short_switch'] == 0){?>selected<?php }?>"><span><?php echo $lang['close'];?></span></label>
              <input id="short_switch1" name="short_switch" <?php if($output['mmtpl_info']['mmt_short_switch'] == 1){?>checked="checked"<?php }?> value="1" type="radio">
              <input id="short_switch0" name="short_switch" <?php if($output['mmtpl_info']['mmt_short_switch'] == 0){?>checked="checked"<?php }?> value="0" type="radio"></td>
            <td class="vatop tips"></td>
          </tr>
          <tr>
            <td colspan="2" class="required"><label>내용:</label></td>
          </tr>
          <tr class="noborder">
            <td class="vatop rowform">
              <textarea name="short_content" rows="6" class="tarea"><?php echo $output['mmtpl_info']['mmt_short_content'];?></textarea>
            </td>
            <td class="vatop tips"></td>
          </tr>
        </tbody>
      </table>
      <div class="margintop">
        <a href="JavaScript:void(0);" onclick="document.short_name.submit();" class="btn"><span><?php echo $lang['nc_submit'];?></span></a>
      </div>
    </form>
    <!-- 短消息 E -->
    <!-- 邮件 S -->
    <form class="tab-content" method="post" name="mail_form" style="display:none;">
      <input type="hidden" name="form_submit" value="ok" />
      <input type="hidden" name="code" value="<?php echo $output['mmtpl_info']['mmt_code'];?>" />
      <input type="hidden" name="type" value="mail" />
      <table class="table tb-type2">
        <tbody>
          <tr class="noborder">
            <td colspan="2" class="required"><label>메일오픈여부:</label></td>
          </tr>
          <tr class="noborder">
            <td class="vatop rowform onoff">
              <label for="mail_switch1" class="cb-enable <?php if($output['mmtpl_info']['mmt_mail_switch'] == 1){?>selected<?php }?>"><span><?php echo $lang['open'];?></span></label>
              <label for="mail_switch0" class="cb-disable <?php if($output['mmtpl_info']['mmt_mail_switch'] == 0){?>selected<?php }?>"><span><?php echo $lang['close'];?></span></label>
              <input id="mail_switch1" name="mail_switch" <?php if($output['mmtpl_info']['mmt_mail_switch'] == 1){?>checked="checked"<?php }?> value="1" type="radio">
              <input id="mail_switch0" name="mail_switch" <?php if($output['mmtpl_info']['mmt_mail_switch'] == 0){?>checked="checked"<?php }?> value="0" type="radio"></td>
            <td class="vatop tips"></td>
          </tr>
          <tr>
            <td colspan="2" class="required"><label>제목:</label></td>
          </tr>
          <tr class="noborder">
            <td class="vatop rowform">
              <textarea name="mail_subject" rows="6" class="tarea"><?php echo $output['mmtpl_info']['mmt_mail_subject'];?></textarea>
            </td>
            <td class="vatop tips"></td>
          </tr>
          <tr>
            <td colspan="2" class="required"><label>내용:</label></td>
          </tr>
          <tr class="noborder">
            <td colspan="2"><?php showEditor('mail_content', $output['mmtpl_info']['mmt_mail_content']);?></td>
          </tr>
        </tbody>
      </table>
      <div class="margintop">
        <a href="JavaScript:void(0);" onclick="document.mail_form.submit();" class="btn"><span><?php echo $lang['nc_submit'];?></span></a>
      </div>
    </form>
    <!-- 邮件 E -->
  </div>
</div>
<script>
$(function(){
    $('div[cbtype="sellerTplContent"] > ul').find('li').click(function(){
        $(this).addClass('current').siblings().removeClass('current');
        var _index = $(this).index();
        var _form = $('div[cbtype="sellerTplContent"]').find('form');
        _form.hide();
        _form.eq(_index).show();
    });
});
</script>