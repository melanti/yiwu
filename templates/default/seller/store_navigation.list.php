<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="wrap" style="width:auto !important;">
  <div class="tabmenu">
    <?php include template('layout/submenu');?>
    <a href="<?php echo urlShop('store_navigation', 'navigation_add');?>" class="ncsc-btn ncsc-btn-green" title="네비게이션추가">내비게이션추가</a> </div>
  
<div class="alert alert-block mt10">
  <ul class="mt5">
    <li>"내비게이션"이란 미니샵 페이지에 메뉴처럼 나타나며, 내비게이션을 클릭하시면</li>
    <li>설정하신 텍스트/이미지 혹은 페이지가 나타나게 됩니다.</li>
    <li>공지사항, 신상품 광고, 인기 품목, 인기 카테고리 등을 설정해 두시면 상품 업로드 등 게시하실 때 편리합니다.</li>
  </ul>
</div>


  <table class="ncsc-default-table">
    <thead>
      <tr>
        <th class="w60">순서</th>
        <th class="tl">메뉴명</th>
        <th class="w120">노출여부</th>
        <th class="w110">편집</th>
      </tr>
    </thead>
    <tbody>
      <?php if(!empty($output['navigation_list'])){?>
      <?php foreach($output['navigation_list'] as $key=> $value){?>
      <tr class="bd-line">
        <td><?php echo $value['sn_sort'];?></td>
        <?php $sn_href = empty($value['sn_url'])?urlShop('show_store', 'show_article', array('store_id' => $_SESSION['store_id'], 'sn_id' => $value['sn_id'])):$value['sn_url'];?>
        <td class="tl"><dl class="goods-name"><dt><a href="<?php echo $sn_href;?>" ><?php echo $value['sn_title'];?></a></dt></dl></td>
        <td><?php if($value['sn_if_show']){echo "예";}else{echo "아니오";}?></td>
        <td class="nscs-table-handle"><span><a href="<?php echo urlShop('store_navigation', 'navigation_edit', array('sn_id' => $value['sn_id']));?>" class="btn-blue"><i class="icon-edit"></i>
          <p> 편집</p>
          </a></span><span> <a href="javascript:;" cnbiztype="btn_del" data-sn-id="<?php echo $value['sn_id'];?>"class="btn-red"><i class="icon-trash"></i>
          <p>삭제</p>
          </a></span></td>
      </tr>
      <?php }?>
      <?php } else { ?>
      <tr>
        <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
      </tr>
      <?php }?>
    </tbody>
  </table>
</div>
<form id="del_form" method="post" action="<?php echo urlShop('store_navigation', 'navigation_del');?>">
  <input id="del_sn_id" name="sn_id" type="hidden" />
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('[cnbiztype="btn_del"]').on('click', function() {
            var sn_id = $(this).attr('data-sn-id');
            if(confirm('정말 삭제 하시겠습니까？')) {
                $('#del_sn_id').val(sn_id);
                ajaxpost('del_form', '', '', 'onerror')
            }
        });
    });
</script>
