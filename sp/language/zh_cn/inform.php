<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * 페이지需要的语言
 */
$lang['inform_page_title'] = '상품신고';
$lang['inform_manage_title'] = '신고관리';
$lang['inform'] 			= '신고';

$lang['inform_state_all'] = '전체신고';
$lang['inform_state_handled'] = '처리됨';
$lang['inform_state_unhandle'] = '미처리';
$lang['inform_goods_name'] = '상품명';
$lang['inform_member_name'] = '신고인';
$lang['inform_subject'] = '신고제목';
$lang['inform_type'] = '신고유형';
$lang['inform_type_desc'] = '신고유형설명';
$lang['inform_pic'] = '이미지';
$lang['inform_pic_view'] = '보기이미지';
$lang['inform_pic_none'] = '이미지 없음';
$lang['inform_datetime'] = '신고시간';
$lang['inform_state'] = '상태';
$lang['inform_content'] = '신고내용';
$lang['inform_handle_message'] = '처리정보';
$lang['inform_handle_type'] = '처리결과';
$lang['inform_handle_type_unuse'] = '무효신고';
$lang['inform_handle_type_venom'] = '장난신고';
$lang['inform_handle_type_valid'] = '유효신고';
$lang['inform_handle_type_unuse_message'] = '무효신고--상품은 정상 판매됩니다.';
$lang['inform_handle_type_venom_message'] = '장난신고--본 회원의 모든 신고가 취소상태로 변경되고 재신고가 불가합니다.';
$lang['inform_handle_type_valid_message'] = '유효신고--상품은 위반되어 내림상태로됩니다.';
$lang['inform_subject_add'] = '제목추가';
$lang['inform_type_add'] = '유형추가';

$lang['inform_text_none'] = '무';
$lang['inform_text_handle'] = '처리';
$lang['inform_text_select'] = '-선택-';

/**
 * 提示信息
 */
$lang['inform_content_null'] = '신고내용는 100자 이내로입력하세요';
$lang['inform_subject_add_null'] = '신고제목는 100자 이내로입력하세요';
$lang['inform_handle_message_null'] = '처리정보는 100자 이내로입력하세요';
$lang['inform_type_null'] = '신고유형는 50자 이내로입력하세요';
$lang['inform_type_desc_null'] = '신고유형설명는 100자 이내로입력하세요';
$lang['inform_handle_confirm'] = '정말 본 신고를 처리하시겠습니까?';
$lang['inform_type_delete_confirm'] = '신고 카테고리를 삭제시 해당항목이 같이 삭제됩니다, 계속 진행하시겠습니까?';
$lang['confirm_delete'] = '정말 삭제하시겠습니까?';
$lang['inform_pic_error'] = '이미지는 jpg만 가능합니다.';
$lang['inform_handling'] = '본 상품은 이미 신고상태이며 처리대기상태입니다.';
$lang['inform_type_error'] = '신고유형이 존재하지 않습니다.';
$lang['inform_subject_null'] = '신고제목이 존재하지 않습니다.';
$lang['inform_success'] = '신고성공, 처리가 될때까지 기다리세요';
$lang['inform_fail'] = '신고실패, 관리자에게 문의하세요';
$lang['goods_null'] = '상품이 존재하지 않습니다.';
$lang['deny_inform'] = '신고가 불가한 상품입니다, 관리자에게 문의하세요'; 
$lang['inform_help1']='신고유형과 신고제목은 관리자 페이지에서 설정가능합니다, 상품상세페이지에서 신고 제목으로 상품을 신고할 수 있습니다.';
$lang['inform_help2']='상세보기, 신고내용보기';
$lang['inform_help3']='처리된 신고내용 보기';
$lang['inform_help4']='동일 신고유형에 다수의 신고제목을 추가할 수 있습니다.';
$lang['inform_help5']='회원 신고제목으로 신고 위반 상품을 선택할 수 있습니다.';

?>
