<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['sns_member_tag_name_not_null']	= '회원태그명을 입력하세요';
$lang['sns_member_tag_sort_is_number']	= '회원태그 정렬은 반드시 숫자로 입력하세요';
$lang['sns_member_add_once_more']		= '더 추가';
$lang['sns_memner_return_list']			= '돌아가기';

$lang['sns_member_tag']					= '회원태그';
$lang['sns_member_tag_manage']			= '태그관리';
$lang['sns_member_index_tips_1']		= '다수의 태그를 선택할 수 있습니다.';
$lang['sns_member_index_tips_2']		= '태그를 회원 검색 페이지로 추천할 수 있습니다.';
$lang['sns_member_tag_name']			= '태그명';
$lang['sns_member_view_member']			= '회원보기';
$lang['sns_member_tag_name_tips']		= '20자 이내로 입력하세요';
$lang['sns_member_tag_recommend_tips']	= '소속 회원에 태그를 추가하여 친구 검색 페이지에 추천할 수 있습니다.';
$lang['sns_member_tag_sort_tips']		= '0~255이내의 숫자를 입력하세요, 작은 숫자로 정렬됩니다.';
$lang['sns_member_tag_desc']			= '태그설명';
$lang['sns_member_tag_desc_tips']		= '50자 이내로 입력하세요, 설명은 친구 검색 페이지에 노출됩니다.';
$lang['sns_member_tag_img']				= '태그이미지';
$lang['sns_member_tag_img_tips']		= '추천크기: 120px*120px';
$lang['sns_member_tag_name_null_error']	= '이름을 입력하세요';
$lang['sns_member_tag_name_max_error']	= '20자 이내로 입력하세요';
$lang['sns_member_tag_sort_error']		= '숫자로 입력하세요';
$lang['sns_member_tag_desc_max_error']	= '50자 이내로 입력하세요';	

$lang['sns_member_member_list']			= '회원리스트';
$lang['sns_member_member_list_tips']	= '회원 검색 페이지 추천된 회원 우선 노출됩니다.';
$lang['sns_member_member_name']			= '회원아이디';