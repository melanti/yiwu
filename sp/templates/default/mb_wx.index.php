<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>微信公众号</h3>
      <ul class="tab-base">
          <li>微信公众账号二维码图片上传</li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>

  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12" class="nobg"> <div class="title nomargin">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span> </div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>上传商城的微信公众账号二维码图片后，前台用户可使用微信扫一扫进行关注</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form id="link_form" enctype="multipart/form-data" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2 nobdb">
      <tbody>
        <tr>
          <td colspan="2" class="required"><label class="validation">微信二维码图片:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
              <div class="input-file-show"><span class="show"> <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/'.$output['mobile_wx'];?>"> <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/'.$output['mobile_wx'];?>>')" onMouseOut="toolTip()"></i></a> </span> <span class="type-file-box">
            <input name="mobile_wx" type="file" class="type-file-file" id="mobile_wx" size="30" hidefocus="true">
            </span></div>
          </td>
          <td class="vatop tips">建议大小90px*90px</td>
        </tr>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></td>
        </tr>
      </tfoot>
	</table>
  </form>
</div>
<script>
$(function(){
	//图片上传
 	var textButton="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />"
	$(textButton).insertBefore("#mobile_wx");
	$("#mobile_wx").change(function(){
	$("#textfield1").val($("#mobile_wx").val());
});	
//按钮先执行验证再提交表单	
	$("#submitBtn").click(function(){
    if($("#link_form").valid()){
        <?php if ($output['mobile_wx']) { ?>
        if ($('#mobile_wx').val() == '') {
        	if(!confirm('您未选择要上传的图片，继续保存会清除现有图片，确认继续提交吗')){
            	return false;
        	}
        }
        <?php } ?>
        $("#link_form").submit();
	}
	});

	$('#link_form').validate({
        errorPlacement: function(error, element){
        	var error_td = element.parents('dl').find('span.err');
            error_td.append(error);
        },
        rules : {
        	mobile_wx : {
                accept : 'png|jpe?g|gif'
            }
        },
        messages : {
        	mobile_wx : {
                accept   : '<i class="fa fa-exclamation-circle"></i>图片格式错误'
            }
        }
    });
});
</script> 
