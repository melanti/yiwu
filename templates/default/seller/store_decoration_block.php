<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php 
$control_flag = false;
if($_GET['op'] == 'decoration_edit' || $_GET['op'] == 'decoration_block_add') { 
    $control_flag = true;
} 
$block = empty($block) ? $output['block'] : $block;
$block_content = $block['block_content'];
if($control_flag) { 
    $block_title = '드래그 하셔서 레이아웃 블록의 순서를 수정하실 수 있으며, 삭제도 가능합니다. 레이아웃 블록 내용은 편집을 통하여 편집/수정해 주세요.';
} else {
    $block_title = '';
}
?>
<?php $extend_class = $block['block_full_width'] == '1' ? 'store-decoration-block-full-width' : '';?>
<div id="block_<?php echo $block['block_id'];?>" data-block-id="<?php echo $block['block_id'];?>" cnbiztype="store_decoration_block" class="ncsc-decration-block store-decoration-block-1 <?php echo $extend_class;?> tip" title="<?php echo $block_title;?>">
    <div cnbiztype="store_decoration_block_content" class="ncsc-decration-block-content store-decoration-block-1-content">
        <div cnbiztype="store_decoration_block_module" class="store-decoration-block-1-module">
            <?php if(!empty($block['block_module_type'])) { ?>
            <?php require('store_decoration_module.' . $block['block_module_type'] . '.php');?>
            <?php } ?>
        </div>
        <?php if($control_flag) { ?>
        <a class="edit" cnbiztype="btn_edit_module" data-module-type="<?php echo $block['block_module_type'];?>" href="javascript:;" data-block-id="<?php echo $block['block_id'];?>"><i class="icon-edit"></i>편집</a>
        <?php } ?>
    </div>
    <?php if($control_flag) { ?>
    <a class="delete" cnbiztype="btn_del_block" href="javascript:;" data-block-id=<?php echo $block['block_id'];?> title="삭제"><i class="icon-trash"></i>삭제</a>    
    <?php } ?>
</div>
