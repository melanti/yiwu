<?php
/**
 * 我的代金券
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class member_pointsControl extends mobileMemberControl {

	public function __construct() {
		parent::__construct();
	}

    /**
     * 地址列表
     */
    public function pointslogOp() {
        $condition_arr = array();
        $condition_arr['pl_memberid'] = $this->member_info['member_id'];
        //查询积分日志列表
        $points_model = Model('points');

        $list_log = $points_model->getPointsLogList($condition_arr,$this->page,'*');
        foreach($list_log as $val):
                    switch ($val['pl_stage']){
                        case 'regist':
                            $l = "注册";
                            break;
                        case 'login':
                            $l = "登录";
                            break;
                        case 'comments':
                            $l = "商品评论";
                            break;
                        case 'order':
                            $l = "订单消费";
                            break;
                        case 'system':
                            $l = "积分管理";
                            break;
                        case 'pointorder':
                            $l = "礼品兑换";
                            break;
                        case 'app':
                            $l = "积分兑换";
                            break;
                    }
            $val['stagetext'] = $l;
            $val['addtimetext'] = date("Y-m-d H:i",$val['pl_addtime']);
            $new_list_log[] = $val;
        endforeach;
        $page_count = $points_model->gettotalpage($condition_arr);
        output_data(array('log_list' => $new_list_log), mobile_page(count($page_count)));
    }

    /**
     * 地址列表
     */
    public function pdrechargelistOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], $_POST['voucher_state'], $this->page);
        $page_count = $model_voucher->gettotalpage();

        output_data(array('list' => $voucher_list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function pdcashlistOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], $_POST['voucher_state'], $this->page);
        $page_count = $model_voucher->gettotalpage();

        output_data(array('list' => $voucher_list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function rcblogOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], $_POST['voucher_state'], $this->page);
        $page_count = $model_voucher->gettotalpage();

        output_data(array('list' => $voucher_list), mobile_page($page_count));
    }


}
