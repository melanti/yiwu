module.exports = {
  login: catLogin
};

function catLogin(req, res) {
  var chk_data = require("./libs/chkData.js");
  var crypto = require('crypto');
  var debug = require("./libs/debug.js");
  var lib_vc = require("./libs/verifyCode.js");
  var random_uuid = require("./libs/uuid.js").setUUID();

  var id = req.param('id');
  var password = req.param('password');
  var ip_addr = req.param('ip_addr');
  var verify_code =req.param('verify_code');
  var service = req.param('service');
  var show = req.param('show');
  var secret = crypto.createHash('sha256').update(random_uuid).digest('base64').replace(/\W/g, '');

  var status = 501;
  var error = {};
  var argQuery;
  var strQuery;
  var query = {};
  var results_wrap = {};

  chk_data.chkParam(req, res, [verify_code, id, password, service], function () {
    password = crypto.createHash('md5').update(password).digest("hex");
    lib_vc.chkVerifyCode(req, res, id, verify_code, function () {

      query.select = "SELECT x.member_id AS account_uuid, x.store_id, x.store_name AS client_name ";
      query.from =   "FROM ( ";
      query.sub_select =   "SELECT a.member_id, ";
      query.sub_select +=         "b.seller_group_id, ";
      query.sub_select +=         "e.store_id, ";
      query.sub_select +=         "e.store_name, ";
      query.find_in_set =         "FIND_IN_SET( ";
      query.find_in_set +=          "'cat_login', ";
      query.find_in_set +=          "CASE ";
      query.find_in_set +=            "WHEN b.seller_group_id = 0 THEN 'cat_login' ";
      query.find_in_set +=            "ELSE c.limits ";
      query.find_in_set +=          "END ";
      query.find_in_set +=        ") AS limits ";
      query.sub_from =     "FROM sv_member AS a ";
      query.join =         "JOIN sv_seller AS b ON a.member_id = b.member_id ";
      query.join +=        "JOIN sv_store AS e ON b.store_id = e.store_id ";
      query.left_join =    "LEFT OUTER JOIN sv_seller_group AS c ON b.seller_group_id = c.group_id ";
      query.sub_where =    "WHERE b.seller_name = ? AND a.member_passwd = ? ";
      query.as =       ") AS x ";
      query.where =  "WHERE limits > 0";

      strQuery = query.select + query.from
        + query.sub_select + query.find_in_set + query.sub_from
        + query.join + query.left_join + query.sub_where
        + query.as + query.where;
      argQuery = [id, password];

      DB.query(strQuery, argQuery, function (err, results) {
        sails.log.debug(results);
        results_wrap.results = results;
        if (err) {
          status = 500;
          return res.send(status, debug.wrap(req, res, status, err));
        }
        if (results.length === 0) {
          status = ErrorHandler.NOT_FOUND_DATA.status;
          error.code = ErrorHandler.NOT_FOUND_DATA.code;
          error.index = 0;
          return res.send(status, debug.wrap(req, res, status, error));
        }

        /* secret */
        query.secret = 'INSERT INTO sv_secret (member_id, secret_hash, regist_ip, regist_date) VALUES (?, ?, INET_ATON(?), unix_timestamp(now())) ';
        argQuery = [results[0]['member_id'], secret, ip_addr];
        DB.query(query.secret, argQuery, function (err, results) {
          sails.log.debug(results);
          if (err) {
            status = 500;
            return res.send(status, debug.wrap(req, res, status, err));
          }

          /* success */
          status=200;
          results_wrap.results[0].secret = secret;
          sails.log.debug(debug.wrap(req, res, status, results_wrap));
          return res.send(status, debug.wrap(req, res, status, results_wrap));
        });
      })
    });
  });
}
