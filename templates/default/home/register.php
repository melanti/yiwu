<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/taglibs.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/tabulous.js"></script>

  <div class="nc-register-box">
    <div class="nc-register-layout">
      <div class="left">
        <div class="nc-register-mode">
          <ul class="tabs-nav">
            <li><a href="#default" class="tabulous_active">账号注册<i></i></a></li>
                        <li><a href="#mobile">手机注册<i></i></a></li><span class="tabulousclear"></span>
                      </ul>
          <div class="tabs-container" id="tabs_container" style="height: 383px;">
            <div class="tabs-content" id="default" style="position: absolute; top: 0px;">
              <form action="<?php echo SHOP_SITE_URL;?>/index.php?act=login&op=usersave" method="post" class="nc-login-form" id="register_form">
              <?php Security::getToken();?>
              <dl>
                  <dt><?php echo $lang['login_register_username'];?></dt>
                  <dd>
                    <input type="text" placeholder="请使用3-15个中、英文、数字及“-”符号" class="text" name="user_name" id="user_name" value="" style="color: rgb(187, 187, 187);">
                  </dd>
                </dl>
                <dl>
                  <dt><?php echo $lang['login_register_pwd'];?></dt>
                  <dd>
                    <input type="password" placeholder="6-20个大小写英文字母、符号或数字" class="text" name="password" id="password" value="" style="color: rgb(187, 187, 187);">
                  </dd>
                </dl>
                <dl>
                  <dt><?php echo $lang['login_register_ensure_password'];?></dt>
                  <dd>
                    <input type="password" placeholder="请再次输入密码" class="text" name="password_confirm" id="password_confirm" value="" style="color: rgb(187, 187, 187);">
                  </dd>
                </dl>
                <dl class="mt15">
                  <dt><?php echo $lang['login_register_email'];?></dt>
                  <dd>
                    <input type="text" placeholder="输入常用邮箱作为验证及找回密码使用" class="text" name="email" id="email" value="" style="color: rgb(187, 187, 187);">
                  </dd>
                </dl>
                                <div class="code-div mt15">
                  <dl>
                    <dt>验证码：</dt>
                    <dd>
                      <input type="text" placeholder="输入验证码" size="10" class="text w80" name="captcha" id="captcha" value="" style="color: rgb(187, 187, 187);">
                    </dd>
                  </dl>
                  <span><img id="codeimage" name="codeimage" src="index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>"> <a onclick="javascript:document.getElementById('codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();" href="javascript:void(0)" class="makecode">看不清，换一张</a></span></div>
                                <dl class="clause-div">
                  <dd>
                    <input type="checkbox" checked="checked" value="1" id="clause" class="checkbox" name="agree">
                    <?php echo $lang['login_register_agreed'];?><a title="阅读并同意" class="agreement" target="_blank" href="<?php echo urlShop('document', 'index',array('code'=>'agreement'));?>"><?php echo $lang['login_register_agreement'];?></a></dd>
                </dl>
                <div class="submit-div">
                  <input type="submit" class="submit" value="<?php echo $lang['login_register_regist_now'];?>" id="Submit">
                </div>
                <input type="hidden" value="<?php echo $_GET['ref_url']?>" name="ref_url">
                <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
                <input type="hidden" name="form_submit" value="ok" />
              </form>
            </div>
            <div class="tabs-content hideleft" id="mobile" style="position: absolute; top: 0px;">
              <form class="nc-login-form" method="post" id="post_form">
              <?php Security::getToken();?>
                <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
                <input type="hidden" name="form_submit" value="ok" />
                <dl>
                  <dt>手机号：</dt>
                  <dd>
                    <input type="text" id="phone" name="phone" value="" autocomplete="off" placeholder="请输入手机号码" class="text" style="color: rgb(187, 187, 187);">
                  </dd>
                </dl>
                <div class="code-div">
                  <dl>
                    <dt>验证码：</dt>
                    <dd>
                      <input type="text" placeholder="输入验证码" size="10" id="image_captcha" class="text w100" name="captcha" value="" style="color: rgb(187, 187, 187);">
                    </dd>
                  </dl>
                  <span><img id="sms_codeimage" name="codeimage" title="" src="index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>"><a onclick="javascript:document.getElementById('sms_codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();" href="javascript:void(0);" class="makecode">看不清，换一张</a></span> </div>
                <div id="sms_text" class="tiptext">确保上方验证码输入正确，点击<span><a onclick="get_sms_captcha('1')" href="javascript:void(0);"><i class="icon-mobile-phone"></i>发送短信验证</a></span>，并将您手机短信所接收到的“6位动态码”输入到下方短信验证，再提交下一步。</div>
                <dl>
                  <dt>短信验证：</dt>
                  <dd>
                    <input type="text" size="15" id="sms_captcha" class="text" placeholder="输入6位短信验证码" autocomplete="off" name="sms_captcha" value="" style="color: rgb(187, 187, 187);">
                  </dd>
                </dl>
                <div class="submit-div">
                  <input type="button" value="下一步" class="submit" id="submitBtn">
                </div>
              </form>
        <form action="<?php echo SHOP_SITE_URL;?>/index.php?act=connect_sms&op=register" method="post" class="nc-login-form" id="register_sms_form" style="display: none;">
                <input type="hidden" value="ok" name="form_submit">
                <input type="hidden" value="" id="register_sms_captcha" name="register_captcha">
                <input type="hidden" value="" id="register_phone" name="register_phone">
                <dl>
                  <dt>用户名：</dt>
                  <dd>
                    <input type="text" value="" class="text w150" name="member_name" id="member_name">
                  </dd>
                  <span class="note">系统生成随机用户名，可选择默认或自行修改一次。</span>
                </dl>
                <dl>
                  <dt>设置密码：</dt>
                  <dd>
                    <input type="password" value="" class="text w150" name="password" id="sms_password">
                  </dd>
                  <span class="note">系统生成随机密码，请牢记或修改为自设密码。</span>
                </dl>
                <dl class="mt15">
                  <dt>邮箱：</dt>
                  <dd>
                    <input type="text" placeholder="输入常用邮箱作为验证及找回密码使用" value="" class="text" name="email" id="sms_email" style="color: rgb(187, 187, 187);">
                  </dd>
                </dl>
                <dl class="clause-div">
                  <dd>
                    <input type="checkbox" checked="checked" value="1" id="sms_clause" class="checkbox" name="agree">
                    <?php echo $lang['login_register_agreed'];?><a title="阅读并同意" class="agreement" target="_blank" href="<?php echo urlShop('document', 'index',array('code'=>'agreement'));?>"><?php echo $lang['login_register_agreement'];?></a></dd>
                </dl>
                <div class="submit-div">
                  <input type="submit" title="提交注册" class="submit" value="提交注册">
                </div>
              </form>
            </div>
                      </div>
        </div>
      </div>
      <div class="right">
        <?php if ($output['setting_config']['qq_isuse'] == 1 || $output['setting_config']['sina_isuse'] == 1){?>
        <div class="api-login">
        <h4>使用合作网站账号直接登录</h4>
          <?php if ($output['setting_config']['qq_isuse'] == 1){?>
          <a href="<?php echo SHOP_SITE_URL;?>/api.php?act=toqq" title="QQ账号登录" class="qq"><i></i>QQ</a>
          <?php } ?>
          <?php if ($output['setting_config']['sina_isuse'] == 1){?>
          <a href="<?php echo SHOP_SITE_URL;?>/api.php?act=tosina" title="新浪微博账号登录" class="sina"><i></i>新浪微博</a>
          <?php } ?>
        </div>
        <?php } ?>

                <div class="reister-after">
          <h4><?php echo $lang['login_register_after_regist'];?></h4>
          <ol>
            <li class="ico01"><i></i>购买商品支付订单</li>
            <li class="ico02"><i></i>收藏商品关注店铺</li>
            <li class="ico03"><i></i>安全交易诚信无忧</li>
            <li class="ico04"><i></i>积分获取优惠购物</li>
            <li class="ico05"><i></i>会员等级享受特权</li>
            <li class="ico06"><i></i>评价晒单站外分享</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
<script>
$(function(){
    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[^:%,'\*\"\s\<\>\&]+$/i.test(value);
    }, "Letters only please"); 
    jQuery.validator.addMethod("lettersmin", function(value, element) {
      return this.optional(element) || ($.trim(value.replace(/[^\u0000-\u00ff]/g,"aa")).length>=3);
    }, "Letters min please"); 
    jQuery.validator.addMethod("lettersmax", function(value, element) {
      return this.optional(element) || ($.trim(value.replace(/[^\u0000-\u00ff]/g,"aa")).length<=15);
    }, "Letters max please");

  //初始化Input的灰色提示信息  
  // $('input[tipMsg]').inputTipText({pwd:'password,password_confirm'});
  //注册方式切换
  $('.nc-register-mode').tabulous({
     //动画缩放渐变效果effect: 'scale'
     effect: 'slideLeft'//动画左侧滑入效果
    //动画下方滑入效果 effect: 'scaleUp'
    //动画反转效果 effect: 'flip'
  });
  var div_form = '#default';
  $(".nc-register-mode .tabs-nav li a").click(function(){
        if($(this).attr("href") !== div_form){
            div_form = $(this).attr('href');
            $(""+div_form).find(".makecode").trigger("click");
      }
  });
  
//注册表单验证
    $("#register_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
      submitHandler:function(form){
          ajaxpost('register_form', '', '', 'onerror');
      },
        onkeyup: false,
        rules : {
            user_name : {
                required : true,
                lettersmin : true,
                lettersmax : true,
                lettersonly : true,
                remote   : {
                    url :'index.php?act=login&op=check_member&column=ok',
                    type:'get',
                    data:{
                        user_name : function(){
                            return $('#user_name').val();
                        }
                    }
                }
            },
            password : {
                required : true,
                minlength: 6,
        maxlength: 20
            },
            password_confirm : {
                required : true,
                equalTo  : '#password'
            },
            email : {
                required : true,
                email    : true,
                remote   : {
                    url : 'index.php?act=login&op=check_email',
                    type: 'get',
                    data:{
                        email : function(){
                            return $('#email').val();
                        }
                    }
                }
            },
                  captcha : {
                required : true,
                remote   : {
                    url : '<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                          document.getElementById('codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            },
                  agree : {
                required : true
            }
        },
        messages : {
            user_name : {
                required : '<?php echo $lang['login_register_input_username'];?>',
                lettersmin : '<?php echo $lang['login_register_username_range'];?>',
                lettersmax : '<?php echo $lang['login_register_username_range'];?>',
                remote   : '<?php echo $lang['login_register_username_exists'];?>'
            },
            password  : {
                required : '<?php echo $lang['login_register_input_password'];?>',
                minlength: '<?php echo $lang['login_register_password_range'];?>',
        maxlength: '<?php echo $lang['login_register_password_range'];?>'
            },
            password_confirm : {
                required : '<?php echo $lang['login_register_input_password_again'];?>',
                equalTo  : '<?php echo $lang['login_register_password_not_same'];?>'
            },
            email : {
                required : '<?php echo $lang['login_register_input_email'];?>',
                email    : '<?php echo $lang['login_register_invalid_email'];?>',
        remote   : '<?php echo $lang['login_register_email_exists'];?>'
            },
      <?php if(C('captcha_status_register') == '1') { ?>
            captcha : {
                required : '<?php echo $lang['login_register_input_text_in_image'];?>',
        remote   : '<?php echo $lang['login_register_code_wrong'];?>'
            },
      <?php } ?>
                  agree : {
                required : '<?php echo $lang['login_register_must_agree'];?>'
            }
        }
    });
});
</script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/connect_sms.js" charset="utf-8"></script>

<script>
$(function(){

  $("#submitBtn").click(function(){
        if($("#post_form").valid()){
            check_captcha();
      }
  });
  $("#post_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
        onkeyup: false,
    rules: {
      phone: {
                required : true,
                mobile : true
            },
      captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : '<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#image_captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                          document.getElementById('sms_codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            },
      sms_captcha: {
                required : function(element) {
                    return $("#image_captcha").val().length == 4;
                },
                minlength: 6
            }
    },
    messages: {
      phone: {
                required : '请输入正确的手机号',
                mobile : '请输入正确的手机号'
            },
      captcha : {
                required : '<?php echo $lang['login_register_input_text_in_image'];?>',
                minlength: '<?php echo $lang['login_register_input_text_in_image'];?>',
        remote   : '<?php echo $lang['login_register_input_text_in_image'];?>'
            },
      sms_captcha: {
                required : '请输入六位短信动态码',
                minlength: '请输入六位短信动态码'
            }
    }
  });
    $('#register_sms_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
      submitHandler:function(form){
          ajaxpost('register_sms_form', '', '', 'onerror');
      },
        rules : {
            member_name : {
                required : true,
                lettersmin : true,
                lettersmax : true,
                lettersonly : true,
                remote   : {
                    url :'index.php?act=login&op=check_member&column=ok',
                    type:'get',
                    data:{
                        user_name : function(){
                            return $('#member_name').val();
                        }
                    }
                }
            },
            password : {
                required   : true,
                minlength: 6,
        maxlength: 20
            },
            email : {
                email    : true,
                remote   : {
                    url : 'index.php?act=login&op=check_email',
                    type: 'get',
                    data:{
                        email : function(){
                            return $('#sms_email').val();
                        }
                    }
                }
            },
            agree : {
                required : true
            }
        },
        messages : {
            member_name : {
                required : '<?php echo $lang['login_register_input_username'];?>',
                lettersmin : '<?php echo $lang['login_register_username_range'];?>',
                lettersmax : '<?php echo $lang['login_register_username_range'];?>',
                lettersonly: '<?php echo $lang['login_register_username_lettersonly'];?>',
                remote   : '<?php echo $lang['login_register_username_exists'];?>'
            },
            password  : {
                required : '<?php echo $lang['login_register_input_password'];?>',
                minlength: '<?php echo $lang['login_register_password_range'];?>',
                maxlength: '<?php echo $lang['login_register_password_range'];?>'
            },
            email : {
                required : '<?php echo $lang['login_register_input_email'];?>',
                email    : '<?php echo $lang['login_register_invalid_email'];?>',
                remote   : '<?php echo $lang['login_register_email_exists'];?>'
            },
            agree : {
                required : '<?php echo $lang['login_register_must_agree'];?>'
            }
        }
    });
});
</script>
