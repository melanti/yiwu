<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 品牌管理
 */
$lang['store_goods_brand_apply']				= '브랜드신청';
$lang['store_goods_brand_name']					= '브랜드명';
$lang['store_goods_brand_my_applied']			= '내가 신청한';
$lang['store_goods_brand_icon']					= '브랜드 이미지';
$lang['store_goods_brand_belong_class']			= '소속유형';
$lang['store_goods_brand_no_record']			= '조건에 적합한 브랜드가 없습니다.';
$lang['store_goods_brand_input_name']			= '브랜드명을 입력하세요';
$lang['store_goods_brand_name_error']			= '브랜드명은 반드시 100자이내로 입력하세요';
$lang['store_goods_brand_icon_null']			= '브랜드 이미지를 등록해주세요';
$lang['store_goods_brand_edit']					= '브랜드수정';
$lang['store_goods_brand_class']				= '브랜드유형';
$lang['store_goods_brand_pic_upload']			= '이미지 업로드';
$lang['store_goods_brand_upload_tip']			= '建议上传大小为150x50的品牌图片。<br />申请品牌的目的是方便买家通过品牌索引页查找商品，申请时请填写品牌所属的类别，方便平台归类。在平台审核前，您可以编辑或撤销申请。';
$lang['store_goods_brand_name_null']			= '브랜드명을 입력하세요';
$lang['store_goods_brand_apply_success']		= '등록되었습니다. 심사를 기다려주세요.';
$lang['store_goods_brand_choose_del_brand']		= '삭제할 내용을 선택하세요!';
$lang['store_goods_brand_browse']				= '찾아보기...';
?>
