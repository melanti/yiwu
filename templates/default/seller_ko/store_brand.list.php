<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
  <a href="javascript:void(0)" class="ncsc-btn ncsc-btn-green" nc_type="dialog" dialog_title="브랜드신청" dialog_id="my_goods_brand_apply" dialog_width="480" uri="index.php?act=store_brand&op=brand_add">브랜드신청</a></div>
<table class="search-form">
  <form method="get">
    <input type="hidden" name="act" value="store_brand">
    <input type="hidden" name="op" value="brand_list">
    <tr> 
      <td>&nbsp;</td>
      <th>브랜드명</th>
      <td class="w160"><input type="text" class="text" name="brand_name" value="<?php echo $_GET['brand_name']; ?>"/></td>
      <td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="검색" /></label></td>
    </tr>
  </form>
</table>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w150">브랜드아이콘</th>
      <th>브랜드명</th>
      <th>카테고리</th>
      <th class="w100">상태</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($output['brand_list'])) { ?>
    <?php foreach($output['brand_list'] as $val) { ?>
    <tr class="bd-line">
      <td><img src="<?php echo brandImage($val['brand_pic']);?>" onload="javascript:DrawImage(this,88,44);" /></td>
      <td><?php echo $val['brand_name']; ?></td>
      <td><?php echo $val['brand_class']; ?></td>
      <td class="nscs-table-handle"><?php if ($val['brand_apply'] == 0) { ?>
     <span><a href="javascript:void(0)" class="btn-blue" nc_type="dialog" dialog_title="편집" dialog_id="my_goods_brand_edit" dialog_width="480" uri="index.php?act=store_brand&op=brand_add&brand_id=<?php echo $val['brand_id']; ?>"><i class="icon-edit"></i><p>편집</p></a></span>
        <span><a href="javascript:void(0)" class="btn-red" onclick="ajax_get_confirm('정말 삭제 하시겠습니까?', 'index.php?act=store_brand&op=drop_brand&brand_id=<?php echo $val['brand_id']; ?>');"><i class="icon-trash"></i><p>삭제</p></a></span><?php } ?></td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php } ?>
  </tbody>
  <tfoot>
    <?php if (!empty($output['brand_list'])) { ?>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
    <?php } ?>
  </tfoot>
</table>
