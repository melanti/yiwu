(function ($, angular, _, APP, is) {
  'use strict';

  var realURL = '/api',
      testMockURL = '/api/test';

  angular.module('App.common')
    .constant('API_URL_INFO', APP.info.API_FRONT_URL)
    .constant('REGEX', {
      email:/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    })
    .constant('URL_INFO', {
      template: {
        baseURL: '/templates/test/',
        nocache: '?_=' + window.APP.info.gVersion+'_'+parseInt(Math.random()*100000000,10)
      }
    })
    .config(['$httpProvider',function ($httpProvider) {
      //initialize get if not there
      if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
      }

      // Answer edited to include suggestions from comments
      // because previous version of code introduced browser-related errors

      //disable IE ajax request caching
      //$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
      // extra
      // $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
      // $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    }])
    .factory('XHR', ['_', '$q', '$timeout', '$http', XHRFactory])
    .factory('Validator', ['_', '$q', '$timeout', '$http', ValidatorFactory])
    .factory('PopupService', ['_', '$q', '$timeout', '$http', PopupFactory])
    .factory('APP_INFO_DATA', ['$window', function ($window) {
      return $window.APP.info;
    }])
    .factory('COMMON_UTIL', ['APP_INFO_DATA', '$timeout', COMMON_UTIL]);


    function ValidatorFactory(_, $q, $timeout, $http){

      return {
        validate:_validate,
        validatePromise:_validatePromise
      };


      /*

       Validator
        .validatePromise({
            data:{},
            validator:[
              {key:'account_uuid' , validation:{ list:[{fnc:is.not.empty, msg:'EMPTY_ACCOUNT_UUID', node:'#id'}] }},
            ]
          })
          .then(
            function(__successData){

            },
            function(__failData){

            }
          );

      */

      function _validate(__opt){

        if(is.not.existy(__opt.data)){
          return {isValid:false, msg:'NO_DATA'};
        }

        if(is.existy(__opt.validator) && is.array(__opt.validator) && __opt.validator.length>0){
          var chk = true, msg='', key='', node='', iLen = __opt.validator.length, jLen;
          for( var i=0; i<iLen; ++i ){
            chk = true;
            if( is.not.undefined(__opt.data[__opt.validator[i].key]) && is.existy(__opt.validator[i].validation) && is.existy(__opt.validator[i].validation.list) ){
              jLen = __opt.validator[i].validation.list.length;
              for( var j=0; j<jLen; ++j ){
                if(__opt.validator[i].validation.list[j].fnc(__opt.data[__opt.validator[i].key])===true){
                }else{
                  chk= false;
                  msg = __opt.validator[i].validation.list[j].msg;
                  node = __opt.validator[i].validation.list[j].node;
                  key = __opt.validator[i].key;
                  break;
                }
              }
            }
            if(!chk) {
              break;
            }
          }
          if(chk){
            return {isValid:true, data:__opt.data};

          }else{
            return {isValid:false, msg:msg, key:key, node:node};
          }

        }else{
          return {isValid:true, data:__opt.data}
        }

      }

      function _validatePromise(__opt){
        var deferred = $q.defer(),
          rel = _validate(__opt);

        if( rel.isValid ){
          deferred.resolve(rel);
        }else{
          deferred.reject(rel);
        }
        return deferred.promise;
      }
    }


  function PopupFactory(_, $q, $timeout, $http){

    return {
      showPopup:_showPopup
    };

    function _showPopup(__opt){
      var deferred = $q.defer();

      switch(__opt.type){
        case 'confirm':
          _showConfirm(__opt , deferred);
          break;

        default:
          _showAlert(__opt , deferred);
          break;
      }
      return deferred.promise;
    }

    function _showConfirm(__opt , __deferred){
      if(__opt.msg) {
        if( confirm(__opt.msg)===true ){
          if( __opt.afterCb ){
            if( typeof __opt.afterCb==='function' ){
              __opt.afterCb.call(null);
            }else{
              __opt.afterCb.fnc.apply( __opt.afterCb.scope, __opt.afterCb.args );
            }
          }
          __deferred.resolve();

        }else{
          __deferred.reject();
        }
      }else{
        __deferred.reject();
      }
    }

    function _showAlert(__opt , __deferred){
      if(__opt.msg) {
        alert(__opt.msg);
      }
      if( __opt.afterCb ){
        if( typeof __opt.afterCb==='function' ){
          __opt.afterCb();
        }else{
          __opt.afterCb.fnc.apply( __opt.afterCb.scope, __opt.afterCb.args );
        }
      }
      __deferred.resolve();
    }

  }

    function XHRFactory(_, $q, $timeout, $http) {
      var cancelerArr=[],
          cancelId= 0,
          canceler;

      var _req = function (__opt) {
        var deferred = $q.defer(),
            tmpOpt,
            tmpCancelerId,
            tmpCancelerObj;

        tmpCancelerId = __opt.cancelID || (++cancelId);
        tmpCancelerObj = {canceler:$q.defer(), id:tmpCancelerId, url:__opt.url};
        cancelerArr.push(tmpCancelerObj);

        tmpOpt = _.extend({}, {timeout: tmpCancelerObj.canceler.promise}, __opt);
        if(tmpOpt.method && tmpOpt.method.toLowerCase()==='get'){
          if( tmpOpt.url.indexOf('?')===-1 ){
            tmpOpt.url += '?_='+window.APP.info.gVersion+'_'+new Date().getTime();
          }else{
            tmpOpt.url += '&_='+window.APP.info.gVersion+'_'+new Date().getTime();
          }
        }

        $http.apply(null, [tmpOpt])
          .then(
            function(__res){
              _popCanceler(tmpCancelerId);

              try {
                if (__res.data && __res.data.debug && (__res.data.debug.status == '200' || __res.data.debug.status == 200)) {
                  deferred.resolve({data: __res.data});
                } else {
                  deferred.reject({data: __res.data});
                }
              } catch (e) {
                deferred.resolve({data: __res.data});
              }
            },
            function(__res){
              _popCanceler(tmpCancelerId);
              if(__res.status==-1){

              }else{
                deferred.reject({data: __res.data});
              }
            }
          );

        return deferred.promise;
      };

      var _popCanceler = function(__id){
        var chkIdx;
        _.each(cancelerArr, function(ele, idx, list){
          if(ele.id===__id){
            chkIdx = idx;
          }
        });
        if(chkIdx !=undefined){
          var ele = cancelerArr.splice(chkIdx, 1);
          return ele;
        }else{
          return null;
        }

      };

      var _cancelXHR = function(__id){
        var tmpId = __id,
          chkIdx = _popCanceler(tmpId);

        if(chkIdx != null && chkIdx[0] && chkIdx[0].canceler && typeof chkIdx[0].canceler.resolve=='function') {
          chkIdx[0].canceler.resolve({data:{debug:{status:500}, code:'CANCEL_REQUEST'}});
        }
      };


      var _cancelAllXHR = function(){
        _.each(cancelerArr, function(ele, idx, list){
          if(ele.canceler) ele.canceler.resolve( {debug:{status:500}, code:'CANCEL_REQUEST', results:[], statusText:'CANCEL_REQUEST'} );
        });
        cancelerArr=[];
      };


      var _get = function (__url) {
        return _req({method: 'GET', url: __url});
      };

      var _post = function (__url, __data) {
        /*
         var deferred = $q.defer(),
         canceler = $q.defer();

         $http.post.apply(null, [__url, __data, {timeout: canceler.promise}])
         .success(function(__res){
         deferred.resolve({data:__res});
         })
         .error(function(__res){
         deferred.reject({data:__res});
         });
         return deferred.promise;
         */

        return _req({method: 'POST', url: __url, data: __data});
      };

      var _put = function (__url, __data) {
        return _req({method: 'PUT', url: __url, data: __data});
      };

      var _delete = function (__url, __data) {
        return _req({method: 'DELETE', url: __url, data: __data});
      };
      var _file = function (__url, __data) {
        return _req({
          method: 'POST',
          url: __url,
          data: __data,
          withCredentials: true,
          headers: {
            'Content-Type': undefined
          },
          transformRequest: angular.identity
        });
      };

      window.onbeforeunload = function(e) {
        _cancelAllXHR();
      };

      return {
        CANCEL: _cancelXHR,
        GET: _get,
        POST: _post,
        PUT: _put,
        DELETE: _delete,
        FILE: _file,
        REQ: _req
      };
    }

    function COMMON_UTIL(APP_INFO_DATA , $timeout){

      return {
        reqForm: _reqForm,
        getQueryPath: _getQueryPath,
        isExist: _isExist,
        convertHashToObject: _convertHashToObject,
        cancelTimeout: _cancelTimeout,
        getDateStr: _getDateStr,
        convertToDateByPeriodUnit: _convertToDateByPeriodUnit,
        compareList: _compareList,
        getCurTimeZone: _getCurTimeZone,
        getcomputedUTCDate: _getcomputedUTCDate,
        getTodayDateInTimezone: _getTodayDateInTimezone,
        getLocalTimezoneStr: _getLocalTimezoneStr,
        getCompareDateList: _getCompareDateList
      }

      function _cancelTimeout(__timer){
        if(__timer){
          $timeout.cancel(__timer);
        }
      }

      function _getCurTimeZone(){
        return APP.info.user.timezonecnt || 0;
      }

      function _getLocalTimezoneStr(__str, __sep){
        var d = new Date(__str),
            curHours = d.getUTCHours();
        d.setUTCHours(curHours-1*APP.info.user.timezonecnt);
        return _getUTCDateStr(d, (__sep || ':') );
      }

      /**
       * _getDateStr
       * @desc  date Object to YYYY-MM-DD String
       *
       * @param __date {Date}
       * @param __separateStr {String}
       * @returns {string}
       * @private
       */
      function _getDateStr(__date, __separateStr) {
        var separateStr = __separateStr || '-';
        var y = __date.getFullYear();
        var m = __date.getMonth() + 1;
        var d = __date.getDate();
        m = (m < 10) ? '0' + m : m;
        d = (d < 10) ? '0' + d : d;
        return y + separateStr + m + separateStr + d;
      }

      function _getUTCDateStr(__date, __separateStr) {
        var separateStr = __separateStr || '-';
        var y = __date.getUTCFullYear();
        var m = __date.getUTCMonth() + 1;
        var d = __date.getUTCDate();
        m = (m < 10) ? '0' + m : m;
        d = (d < 10) ? '0' + d : d;
        return y + separateStr + m + separateStr + d;
      }

      function _getcomputedUTCDate(__dateStr, __sepStr){
        var d = new Date( __dateStr ),
            sep = __sepStr || '-';

        d.setHours(d.getHours()+APP.info.user.timezonecnt);

        return d.getFullYear()+sep+(_getStr(d.getMonth()+1))+sep+(_getStr(d.getDate()))+' '+(_getStr(d.getHours()))+':'+(_getStr(d.getMinutes()))+':'+(_getStr(d.getSeconds()));

        function _getStr(__num){
          return (parseInt(__num,10)<10)? '0'+__num : String(__num);
        }
      }

      /**
       * _getCompareDateList
       * @desc  datePicker에서 input을 변경시 날짜버튼옵션(오늘 3일~전체)을 sync위해 쓰이는 값
       *
       * @param __separateStr {String}
       * @returns {Array}
       * @private
       */
      function _getCompareDateList(__separateStr) {
        var _$ghostCompareIpt = $('<input type="text" id="ghostIpt">'),
          _separateStr = __separateStr || '-',
          _ghostDatepickerOpt = {dateFormat: 'yy-mm-dd'}, _arr=[];

        //today, 3days, week, 1month, 3month, 1year, all
        _$ghostCompareIpt.datepicker();
        var today = _$ghostCompareIpt.datepicker('refresh').datepicker(_ghostDatepickerOpt).datepicker('setDate', '+0d').datepicker('getDate');
        var days3 = _$ghostCompareIpt.datepicker('refresh').datepicker(_ghostDatepickerOpt).datepicker('setDate', '-3d').datepicker('getDate');
        var aweek = _$ghostCompareIpt.datepicker('refresh').datepicker(_ghostDatepickerOpt).datepicker('setDate', '-7d').datepicker('getDate');
        var m1 = _$ghostCompareIpt.datepicker('refresh').datepicker(_ghostDatepickerOpt).datepicker('setDate', '-1m').datepicker('getDate');
        var m3 = _$ghostCompareIpt.datepicker('refresh').datepicker(_ghostDatepickerOpt).datepicker('setDate', '-3m').datepicker('getDate');
        var y1 = _$ghostCompareIpt.datepicker('refresh').datepicker(_ghostDatepickerOpt).datepicker('setDate', '-1y').datepicker('getDate');

        _arr = [
          {period_unit: 'today', dateStr: _getDateStr(today, _separateStr)},
          {period_unit: '3days', dateStr: _getDateStr(days3, _separateStr)},
          {period_unit: 'week', dateStr: _getDateStr(aweek, _separateStr)},
          {period_unit: '1month', dateStr: _getDateStr(m1, _separateStr)},
          {period_unit: '3month', dateStr: _getDateStr(m3, _separateStr)},
          {period_unit: '1year', dateStr: _getDateStr(y1, _separateStr)}
        ];

        today =null;  days3 =null;  aweek =null;  m1 =null;  m3 =null;  y1 =null;
        _$ghostCompareIpt = null;
        _ghostDatepickerOpt = null;
        return _arr;
      }

      /**
       * _compareList
       * @desc  다차원배열에서 특정key값과 __val 비교
       *
       * @param __arr {Array}
       * @param __key {String}
       * @param __val {String}
       * @return  {*}
       */
      function _compareList(__arr, __key, __val){
        var chk = false;
        __arr.forEach(function(element, index, array){
          if(element[__key]===__val){
            //console.log('element: ' , element);
            chk = element;
          }
        });
        return chk;
      }


      /**
       * _convertToDateByPeriodUnit
       * @desc  table option에서 기간설정버튼누를때 datepicker 의 값을 변경하는 invoke method
       *
       * @param __period_unit {String}
       * @param __stDateOpt {Object}
       * @param __endDateOpt {Object}
       */
      function _convertToDateByPeriodUnit(__period_unit, __stDateOpt, __endDateOpt) {

        // today, 3days, week, 1month, 3month, 1year, all
        var stDt, endDt;

        switch (__period_unit) {
          case 'today':
            stDt = _getTodayDateInTimezone();
            __stDateOpt.setDate = _getDateStr(stDt);
            break;

          case '3days':
            // __stDateOpt.setDate = '-3d';
            __stDateOpt.setDate = _getDateStr(_getTodayDateInTimezone(-1*60*24*3));
            break;

          case 'week':
            // __stDateOpt.setDate = '-7d';
            __stDateOpt.setDate = _getDateStr(_getTodayDateInTimezone(-1*60*24*7));
            break;

          case '1month':
            // __stDateOpt.setDate = '-1m';
            __stDateOpt.setDate = _getDateStr(_getTodayDateInTimezone(-1*60*24*30));
            break;

          case '3month':
            // __stDateOpt.setDate = '-3m';
            __stDateOpt.setDate = _getDateStr(_getTodayDateInTimezone(-1*60*24*90));
            break;

          case '1year':
            var tmpDt = parseInt(new Date().getFullYear(),10),
                tmpDay = 365;
            if( (tmpDt%4==0 && tmpDt%100!=0) || tmpDt%400==0) {
              tmpDay = 366;
            }
            __stDateOpt.setDate = _getDateStr(_getTodayDateInTimezone(-1*60*24*tmpDay));
            break;

          case 'all':
          case 'custom':
            __stDateOpt.setDate = '';
            break;

          default:

            break;
        }

        endDt = _getTodayDateInTimezone();
        __endDateOpt.setDate = _getDateStr(endDt);

        // console.log('__stDateOpt: ' ,__stDateOpt);
        // console.log('__endDateOpt: ' ,__endDateOpt);

      }

      function _getTodayDateInTimezone(__min){
        var d = new Date(),
            m = (__min===undefined)?0:__min;

        d.setMinutes( d.getMinutes()+(d.getTimezoneOffset())-(APP.info.user.timezonecnt*60)+(m) );
        return d;
      }

      /**
       * _reqForm
       * @desc url이동시 사용하는 method
       *
       * @param __action {String}
       * @param __method {String}
       * @param __data {Object}
       */
      function _reqForm(__action, __method, __data) {
        $('#tpmFrm').remove();
        var $tmpForm = $('<form id="tpmFrm" name="tpmFrm" method="' + __method + '" action="' + (__action) + '"></form>'), iptStr = '';

        if (__data) {
          for (var i in __data) {
            iptStr += '<input type="hidden" name="' + i + '" value="' + __data[i] + '">'
          }
        }

        $tmpForm.append(iptStr);
        $(document.body).append($tmpForm);
        $tmpForm.submit();
      }


      function _getQueryPath(obj, prefix, postfix){
        var str='';
        if(obj){}else{
          return '';
        }

        for(var i in obj){
          str += i+'='+obj[i]+'&';
        }
        str = str.substr(0, str.length-1);
        if(prefix)str = prefix + str;
        if(postfix)str = str + postfix;
        return str;
      }

      /**
       * _isExist
       * @desc null or undefined 체킹
       *
       * @param __data {*}
       */
      function _isExist(__data) {
        if (__data === undefined || __data === null || __data === 'undefined' || __data === 'null' || __data === '') {
          return false;
        } else {
          return true;
        }
      }

      function _convertHashToObject(str){
        var vars = str.split("&");
        var dataArr = [], i, iTotal=vars.length, pair, obj = {};
        for(i =0; i<iTotal; ++i){
          pair = vars[i].split("=");
          if(pair[0]){
            obj[pair[0]] = pair[1];
          }
        }

        return obj;
      };

    }



})(window.jQuery, window.angular, window._, window.APP, window.is);
