module.exports = {
    checkTimeFormatISO: function( str ){
      if( typeof str == "string" ){
        if( str.trim() !== "" ){
          tmp = new Date( str );
          if( tmp == "Invalid Date" ){
            return false;
          }else{
            return tmp.toISOString();
          }
        }
      }
      return null;
    },

    paramChecker: function(req){
      var key;
      for( key in req.query ){
        if( !isNaN(req.query[key]*1) ) req.query[key] = req.query[key]*1;
        if( typeof req.query[key] == "string" ){
          req.query[key] = req.query[key].trim();
          if( req.query[key].toLowerCase() == "true"   ){
            req.query[key] = true;
          }else if( req.query[key].toLowerCase() == "false"  ) req.query[key] = false;
          if( req.query[key] === '' ) req.query[key] = null;
        }
      }

      for( key in req.body ){
        if( !isNaN(req.body[key]*1) ) req.body[key] = req.body[key]*1;
        if( typeof req.body[key] == "string" ){
          req.body[key] = req.body[key].trim();
          if( req.body[key].toLowerCase() == "true"   ){
            req.body[key] = true;
          }else if( req.body[key].toLowerCase() == "false"  ) req.body[key] = false;
          if( req.body[key] === '' ) req.body[key] = null;
        }
      }
    }
};
