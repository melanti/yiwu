<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){?>
<ul class="goods-list">
  <?php foreach($output['goods_list'] as $key=>$val){?>
  <li>
    <div class="goods-thumb"><img src="<?php echo thumb($val, 240);?>"/></div>
    <dl class="goods-info">
      <dt><a href="<?php echo urlShop('goods', 'index', array('goods_id' => $val['goods_id']));?>" target="_blank"><?php echo $val['goods_name'];?></a> </dt>
      <dd>판매가：<?php echo number_format($val['goods_price']);?>원
    </dl>
    <a cnbiztype="a_choose_goods" data-param="{gid:<?php echo $val['goods_id'];?>, gname:'<?php echo $val['goods_name'];?>', gimage:'<?php echo thumb($val, '60');?>', gimage240:'<?php echo thumb($val, '240');?>', gurl:'<?php echo urlShop('goods', 'index', array('goods_id'=>$val['goods_id']));?>', gprice:'<?php echo $val['goods_price'];?>'}" href="javascript:void(0);" class="ncsc-btn-mini ncsc-btn-green"><i class="icon-plus"></i>상품추가</a> </li>
  <?php } ?>
</ul>
<div class="pagination"><?php echo $output['show_page'];?></div>
<?php } else { ?>
<div class="norecord">내용이 없습니다.</div>
<?php } ?>
