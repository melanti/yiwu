(function($, angular, _, APP){
  'use strict';
  var tableComponentDirName = 'accOrgList';

  if( $(document.body).attr('data-page-id')=='acc_org_list' ){
    bootStrapList();
  }

  function bootStrapList(){
    console.log('[[bootStrapList:]]');

    angular.module('App')
      .directive( tableComponentDirName, tableListDirc );

    angular.module('App')
      .directive( 'tableSearch', tableSearchDirc );

    angular.module('App')
      .directive( 'tableOption', tableOptionDirc );

    angular.module('App')
      .directive( 'tableBody', tableBodyDirc );

    angular.module('App')
      .directive( 'tableBtn', btnDirc );

    angular.module('App')
      .directive( 'pagination', paginationDirc );

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( parseInt(APP.info.user.auth.organizationAccount,10)>=1 ){
            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }
        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }

  function tableListDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.org.list.ctrl.accOrgListDircCtrl,
      controllerAs: 'tableListCtrl',
      bindToController: APP.modules.org.list.ctrl.accOrgListDircCtrl.bindToController,
      link: APP.modules.org.list.ctrl.accOrgListDircCtrl.link(),
      templateUrl: 'table_component_basic.html'
    };
  }
  tableListDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];


  function tableSearchDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:false,
      transclude: true,
      scope: {},
      controller: APP.modules.org.list.ctrl.accOrgListSearchCtrl,
      controllerAs: 'tableSearchCtrl',
      bindToController: APP.modules.org.list.ctrl.accOrgListSearchCtrl.bindToController,
      link: APP.modules.org.list.ctrl.accOrgListSearchCtrl.link(PubSub),
      templateUrl: 'table_search_org_list.html'
    };
  }
  tableSearchDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function tableOptionDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableOptionsBasicCtrl,
      controllerAs: 'tableOptionCtrl',
      bindToController: APP.modules.ctrl.tableOptionsBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link:  APP.modules.ctrl.tableOptionsBasicCtrl.link(PubSub),
      templateUrl: 'table_option_basic.html'
    };
  }
  tableOptionDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function tableBodyDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.org.list.ctrl.accOrgListBodyCtrl,
      controllerAs: 'tableBodyCtrl',
      bindToController: APP.modules.org.list.ctrl.accOrgListBodyCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.org.list.ctrl.accOrgListBodyCtrl.link(PubSub),
      templateUrl: 'table_body_org_list.html'
    };
  }
  tableBodyDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function btnDirc(XHR, PubSub, URL_INFO) {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tableBtnBasicCtrl,
      controllerAs: 'tableBtnBasicCtrl',
      bindToController: APP.modules.ctrl.tableBtnBasicCtrl.bindToController,
      require: ('^' + tableComponentDirName),
      link: APP.modules.ctrl.tableBtnBasicCtrl.link(PubSub),
      templateUrl: 'table_btn_basic.html'
    }
  }
  btnDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

  function paginationDirc(XHR, PubSub, URL_INFO){
    return {
      restrict: 'E',
      replace:true,
      transclude: false,
      scope: {},
      controller: APP.modules.ctrl.tablePaginationBasicCtrl,
      controllerAs: 'tablePaginationBasicCtrl',
      bindToController:  APP.modules.ctrl.tablePaginationBasicCtrl.bindToController,
      require: ('^'+tableComponentDirName),
      link: APP.modules.ctrl.tablePaginationBasicCtrl.link(PubSub),
      templateUrl: 'table_pagination_basic.html'
    };
  }
  paginationDirc.$inject = ['XHR', 'PubSub', 'URL_INFO'];

})(window.jQuery, window.angular, window._, window.APP);


/*

 function bootstrapFittingList(){
   console.log('[[bootstrapFittingList:]]');

   angular.module('App')
   .directive( tableComponentDirName, fittingListDirective );

   angular.module('App')
   .directive( 'tableSearch', tableSearchDirective );

   angular.module('App')
   .directive( 'tableOption', tableOptionDirective );

   angular.module('App')
   .directive( 'tableBody', tableBodyDirective );

   angular.module('App')
   .directive( 'pagination', paginationDirective );

   angular.bootstrap( $('html'), ['App']);
 }

 function fittingListDirective(XHR, PubSub, URL_INFO){
   return {
   restrict: 'E',
   replace:false,
   transclude: true,
   scope: {},
   controller: APP.modules.ctrl.fittingListDirectiveCtrl,
   controllerAs: 'fittingListComponentCtrl',
   bindToController: APP.modules.ctrl.fittingListDirectiveCtrl.bindToController,
   link: APP.modules.ctrl.fittingListDirectiveCtrl.link(),
   templateUrl: window.location.protocol+'//'+window.location.host+'/modules/app/common/templates/table_component_basic.html?_='+APP.info.gVersion
   };
 }
 fittingListDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];

 function tableSearchDirective(XHR, PubSub, URL_INFO){
 return {
 restrict: 'E',
 replace:false,
 transclude: true,
 scope: {},
 controller: APP.modules.ctrl.fittingListSearchCtrl,
 controllerAs: 'tableSearchCtrl',
 bindToController: APP.modules.ctrl.fittingListSearchCtrl.bindToController,
 link: APP.modules.ctrl.fittingListSearchCtrl.link(PubSub),
 templateUrl: window.location.protocol+'//'+window.location.host+'/modules/app/fitting/list/templates/table_search_fitting.html?_='+APP.info.gVersion
 };
 }
 tableSearchDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];

 function tableBodyDirective(XHR, PubSub, URL_INFO){
 return {
 restrict: 'E',
 replace:true,
 transclude: false,
 scope: {},
 controller: APP.modules.ctrl.fittingListbodyCtrl,
 controllerAs: 'tableBodyCtrl',
 bindToController: APP.modules.ctrl.fittingListbodyCtrl.bindToController,
 require: ('^'+tableComponentDirName),
 link: APP.modules.ctrl.fittingListbodyCtrl.link(PubSub),
 templateUrl: window.location.protocol+'//'+window.location.host+'/modules/app/fitting/list/templates/table_body_fitting.html?_='+APP.info.gVersion
 };
 }
 tableBodyDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];

 function tableOptionDirective(XHR, PubSub, URL_INFO){
 return {
 restrict: 'E',
 replace:true,
 transclude: false,
 scope: {},
 controller: APP.modules.ctrl.fittingListOptionCtrl,
 controllerAs: 'tableOptionCtrl',
 bindToController: APP.modules.ctrl.fittingListOptionCtrl.bindToController,
 require: ('^'+tableComponentDirName),
 link:  APP.modules.ctrl.fittingListOptionCtrl.link(PubSub),
 templateUrl: window.location.protocol+'//'+window.location.host+'/modules/app/fitting/list/templates/table_option_fitting.html?_='+APP.info.gVersion
 };
 }
 tableOptionDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];

 function paginationDirective(XHR, PubSub, URL_INFO){
 return {
 restrict: 'E',
 replace:true,
 transclude: false,
 scope: {},
 controller: APP.modules.ctrl.tablePaginationBasicCtrl,
 controllerAs: 'tablePaginationBasicCtrl',
 bindToController:  APP.modules.ctrl.tablePaginationBasicCtrl.bindToController,
 require: ('^'+tableComponentDirName),
 link: APP.modules.ctrl.tablePaginationBasicCtrl.link(PubSub),
 templateUrl: window.location.protocol+'//'+window.location.host+'/modules/app/common/templates/table_pagination_basic.html?_='+APP.info.gVersion
 };
 }
 tableOptionDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];
*/
