(function ($, angular, _, APP) {
  'use strict';
  if ($(document.body).attr('data-page-id') == 'acc_org_registration' || $(document.body).attr('data-page-id') == 'acc_org_modification') {
    bootstrapAccount();
  }

  function bootstrapAccount() {
    console.log('[[bootstrapAccount:]]');

    angular.module('App')
      .directive('account', accountDirective);

    $.get('/api/getRedisAuth?_='+(new Date().getTime()))
      .then(
        function(__sucData){
          APP.info.user.auth = JSON.parse(__sucData.data);
          for(var i in APP.info.user.auth){
            if(i==='account_uuid'){
            }else{
              APP.info.user.auth[i] = parseInt(APP.info.user.auth[i],10);
            }
          }

          APP.MENU_DATA.menu = APP.createNavModel(APP.info.gLocale ,APP.info.user.auth , APP.URL_INFO ,APP.info.user );

          if( (parseInt(APP.info.user.auth.organizationAccount,10)>=1 && window.location.href.indexOf('confirmation')!==-1) || (parseInt(APP.info.user.auth.organizationAccount,10)==2 && (window.location.href.indexOf('registration')!==-1) || (window.location.href.indexOf('modification')!==-1)) ){
          //if( parseInt(APP.info.user.auth.organizationAccount,10)>=1 ){
            angular.bootstrap( $('html'), ['App']);
          }else{
            APP.setup403Page();
            angular.bootstrap( $('html'), ['App']);
          }
        },
        function(__failData){
          console.log('getRedis fail: ' , __failData);
        }
      );
    //angular.bootstrap( $('html'), ['App']);
  }

  function accountDirective(XHR, PubSub, URL_INFO) {
    return {
      restrict: 'E',
      replace: false,
      transclude: true,
      scope: {},
      controller: APP.modules.ctrl.accountCtrl,
      controllerAs: 'accountCtrl',
      bindToController: APP.modules.ctrl.accountCtrl.bindToController,
      link: APP.modules.ctrl.accountCtrl.link(),
      templateUrl: 'account.html'
    };
  }
  accountDirective.$inject = ['XHR', 'PubSub', 'URL_INFO'];


})(window.jQuery, window.angular, window._, window.APP);
