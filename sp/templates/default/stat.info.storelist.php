<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>업체통계</h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <div style="width:100%; text-align:right;padding-top:10px;">
  	<input type="hidden" id="export_type" name="export_type" data-param='{"url":"<?php echo $output['actionurl']; ?>&exporttype=excel"}' value="excel"/>
  	<a class="btns" href="javascript:void(0);" id="export_btn"><span>불러내기Excel</span></a>
  </div>
  <form method="post" id="form_member">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2 nobdb">
      <thead>
        <tr class="thead">
          <th class="align-center">업체이름</th>
          <th class="align-center">업체아이디</th>
          <th class="align-center">점장판매자계정</th>
          <th class="align-center">소속등급</th>
          <th class="align-center">유효기간</th>
          <th class="align-center">오픈시간</th>
        </tr>
      <tbody id="datatable">
        <?php if(!empty($output['store_list']) && is_array($output['store_list'])){ ?>
        <?php foreach($output['store_list'] as $k => $v){ ?>
        <tr class="hover member">
          <td class="align-center"><?php echo $v['store_name']; ?></td>
          <td class="align-center"><?php echo $v['member_name']; ?></td>
          <td class="align-center"><?php echo $v['seller_name']; ?></td>
          <td class="align-center"><?php echo ($t = $output['search_grade_list'][$v['grade_id']])?$t:'자영업'; ?></td>
          <td class="align-center"><?php echo $v['store_end_time']?date('Y-m-d', $v['store_end_time']):'무제한'; ?></td>
          <td class="align-center"><?php echo date('Y-m-d', $v['store_time']); ?></td>
        </tr>
        <?php } ?>
        <?php }else { ?>
        <tr class="no_data">
          <td colspan="11"><?php echo $lang['nc_no_record']?></td>
        </tr>
        <?php } ?>
      </tbody>
      <?php if(!empty($output['store_list']) && is_array($output['store_list'])){ ?>
      <tfoot class="tfoot">
        <tr>
          <td colspan="16">
            <div class="pagination"> <?php echo $output['show_page'];?> </div></td>
        </tr>
      </tfoot>
      <?php } ?>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/statistics.js"></script>
<script>
$(function(){
	//불러내기图表
    $("#export_btn").click(function(){
        var item = $("#export_type");
        var type = $(item).val();
        if(type == 'excel'){
        	download_excel(item);
        }
    });
});
</script>
