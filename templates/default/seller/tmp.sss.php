array (
  'seller_vrorder_exchange_0' => '电子兑换码',
  'seller_vrorder_exchange_1' => '请输入买家提供的电子兑换码',
  'seller_vrorder_exchange_2' => '清除',
  'seller_vrorder_exchange_3' => '后退',
  'seller_vrorder_exchange_4' => '提交验证',
  'seller_vrorder_exchange_5' => '请输入买家提供的兑换码,核对无误后提交,每个兑换码抵消单笔消费。',
  'seller_vrorder_exchange_8' => '兑换码',
  'seller_vrorder_exchange_9' => '商品',
  'seller_vrorder_exchange_10' => '订单号',
  'seller_vrorder_exchange_11' => '下单留言',
  'seller_vrorder_exchange_12' => '兑换成功',
)