<?php
error_reporting(E_ALL & ~E_NOTICE);
define('BASE_ROOT_PATH',str_replace('\\','/',dirname(__FILE__)));
define('BASE_CORE_PATH',BASE_ROOT_PATH.'/core');
define('BASE_DATA_PATH',BASE_ROOT_PATH.'/data');
define('BASE_SITE_URL','/');
define('SCM_SITE_URL', "/scm");
define('MEMBER_SITE_URL', "/");
define('LOGIN_SITE_URL', "/");
define('DS','/');
define('InCNBIZ',true);
define('StartTime',microtime(true));
define('TIMESTAMP',time());
define('DIR_SHOP','');
define('DIR_CMS','cms');
define('DIR_CIRCLE','circle');
define('DIR_MICROSHOP','microshop');
define('DIR_ADMIN','admin');
define('DIR_API','api');
define('DIR_MOBILE','mobile');
define('DIR_WAP','wap');
define('DIR_RESOURCE','data/resource');
define('DIR_UPLOAD','data/upload');
define('ATTACH_PATH','/');
define('ATTACH_COMMON','common');
define('ATTACH_AVATAR','/avatar');
define('ATTACH_EDITOR','/editor');
define('ATTACH_MEMBERTAG','/membertag');
define('ATTACH_STORE','store');
define('ATTACH_GOODS','/store/goods');
define('ATTACH_STORE_DECORATION','/store/decoration');
define('ATTACH_LOGIN','/login');
define('ATTACH_WAYBILL','/waybill');
define('ATTACH_ARTICLE','/article');
define('ATTACH_BRAND','/brand');
define('ATTACH_ADV','/adv');
define('ATTACH_ACTIVITY','/activity');
define('ATTACH_WATERMARK','/watermark');
define('ATTACH_POINTPROD','/pointprod');
define('ATTACH_GROUPBUY','/groupbuy');
define('ATTACH_LIVE_GROUPBUY','/livegroupbuy');
define('ATTACH_SLIDE','/store/slide');
define('ATTACH_VOUCHER','/voucher');
define('ATTACH_STORE_JOININ','/store_joinin');
define('ATTACH_REC_POSITION','/rec_position');
define('ATTACH_MOBILE','mobile');
define('ATTACH_CIRCLE','circle');
define('ATTACH_CMS','cms');
define('ATTACH_LIVE','live');
define('ATTACH_MALBUM','/member');
define('ATTACH_MICROSHOP','microshop');
define('TPL_SHOP_NAME','default');
define('TPL_CIRCLE_NAME', 'default');
define('TPL_MICROSHOP_NAME', 'default');
define('TPL_CMS_NAME', 'default');
define('TPL_ADMIN_NAME', 'default');
define('ADMIN_RESOURCE_URL', 'resource');
define('RESOURCE_PATH', '/data/resource');
define('STORE_JOIN_STATE_NEW', 10);
define('STORE_JOIN_STATE_PAY', 11);
define('STORE_JOIN_STATE_VERIFY_SUCCESS', 20);
define('STORE_JOIN_STATE_VERIFY_FAIL', 30);
define('STORE_JOIN_STATE_PAY_FAIL', 31);
define('STORE_JOIN_STATE_FINAL', 40);
define('DEFAULT_SPEC_COLOR_ID', 1);
define('GOODS_IMAGES_WIDTH', '60,240,360,1280');
define('GOODS_IMAGES_HEIGHT', '60,240,360,12800');
define('GOODS_IMAGES_EXT', '_60,_240,_360,_1280');
//已取消
define('ORDER_STATE_CANCEL', 0);
//已产生但未支付
define('ORDER_STATE_NEW', 10);
//已产生银行转账
define('ORDER_STATE_OFFBANK', 15);
//已支付
define('ORDER_STATE_PAY', 20);
//已发货
define('ORDER_STATE_SEND', 30);
//已收货，交易成功
define('ORDER_STATE_SUCCESS', 40);
//订单结束后可评论时间，15天，60*60*24*15
define('ORDER_EVALUATE_TIME', 1296000);
//무통장 입금 아이디
define('OFFBANK_ID', 6);

?>