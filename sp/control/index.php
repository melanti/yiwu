<?php
/**
 * 默认展示페이지
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');

class indexControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('index');
	}
	public function indexOp(){
		
		$admin_info = $this->getAdminInfo();
		
		//输出관리자信息
		Tpl::output('admin_info',$this->getAdminInfo());
		//输出菜单
		$this->getNav('',$top_nav,$left_nav,$map_nav);
		Tpl::output('top_nav',$top_nav);
		Tpl::output('left_nav',$left_nav);
		Tpl::output('map_nav',$map_nav);

		Tpl::showpage('index','index_layout');
	}

	/**
	 * 退出
	 */
	public function logoutOp(){
		
		// 시스템 관리자 로그아웃시 시크릿키 삭제
		if (method_exists('Security', 'createSecretKey')) {		
			$model_secret = Model("secret");
			
			// 로그아웃시 시크릿키 삭제
			$is_secretkey = $model_secret->isSecretKey();
			if ($is_secretkey) {
				$array_input = array();
				$array_input['member_id'] = $_SESSION['member_id'];
				$array_input['secret_key'] = $_SESSION['secret_key'];
				
				// 시크릿키 삭제
				$del_secretkey = $model_secret->delSecretKey($array_input);
				if (!$del_secretkey) {
					$message = "secret_key delete fail at system admin logout";
					log::record($message, LOG::ERR);					
				}
			}		
		}
			
		setNcCookie('sys_key','',-1,'',null);
		session_destroy();
		@header("Location: index.php");
		exit;
	}
	/**
	 * 修改密码
	 */
	public function modifypwOp(){
		if (chksubmit()){
			if (trim($_POST['new_pw']) !== trim($_POST['new_pw2'])){
				//showMessage('两次输入的密码不一致，请重新输入');
				showMessage(Language::get('index_modifypw_repeat_error'));
			}
			$admininfo = $this->getAdminInfo();
			//검색관리자信息
			$admin_model = Model('admin');
			$admininfo = $admin_model->getOneAdmin($admininfo['id']);
			if (!is_array($admininfo) || count($admininfo)<= 0){
				showMessage(Language::get('index_modifypw_admin_error'));
			}
			//旧密码예否正确
			if ($admininfo['admin_password'] != md5(trim($_POST['old_pw']))){
				showMessage(Language::get('index_modifypw_oldpw_error'));
			}
			$new_pw = md5(trim($_POST['new_pw']));
			$result = $admin_model->updateAdmin(array('admin_password'=>$new_pw,'admin_id'=>$admininfo['admin_id']));
			if ($result){
				showMessage(Language::get('index_modifypw_success'));
			}else{
				showMessage(Language::get('index_modifypw_fail'));
			}
		}else{
			Language::read('admin');
			Tpl::showpage('admin.modifypw');
		}
	}
}
