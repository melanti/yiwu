var crypto = require('crypto');
var fs = require('fs');
var Q = require('q');
var SFTPClient = require('ssh2').Client;

var SFTP_INFO = {
  host: sails.config.ftp.host,
  port: sails.config.ftp.port,
  username: sails.config.ftp.username,
  password: sails.config.ftp.password,
  cwd: sails.config.ftp.cwd,
  upload_dir:'/DAT',
  readyTimeout:10000000
};

module.exports = {
  computeFileHash : _computeFileHash,
  computeFileHashPromisify:_computeFileHashPromisify,
  connectSFTP:_connectSFTP,
  destroyConnectSFTP:_destroyConnectSFTP,
  opendir:_opendir,
  createDir:_createDir,
  createFileStream:_createFileStream,
  uploadThreedFiles:_uploadThreedFiles
};


function _computeFileHash(source, callback) {
  var hash;
  var fd;

  hash = crypto.createHash('sha256');
  hash.setEncoding('hex');

  fd = fs.createReadStream(source);
  fd.on('error', function (err) {
    return callback(err, null);
  });
  fd.on('end', function () {
    hash.end();
    var checksum = hash.read();
    return callback(null, checksum);
  });
  fd.pipe(hash);
}

function _computeFileHashPromisify(source){
  var deferred = Q.defer();
  var hash;
  var fd;

  hash = crypto.createHash('sha256');
  hash.setEncoding('hex');

  fd = fs.createReadStream(source);
  fd.on('error', function (err) {
    deferred.reject(err);
  });
  fd.on('end', function () {
    hash.end();
    deferred.resolve({source:source ,hash:hash.read()});
  });
  fd.pipe(hash);

  return deferred.promise;
}

function _connectSFTP(conn_info, destroy_fnc){
  var deferred = Q.defer();

  var conn = new SFTPClient();
  var sftp;

  conn.on('ready', function(){
    conn.sftp(function(err, ftp) {
      if (err) {
        sails.log.debug('err SFTP_CONNECTION sftp: ' , err);
        destroy_fnc(conn, sftp);
        deferred.reject(err);
        return false;
      }
      sftp = ftp;
      deferred.resolve({connection:conn, sftp:ftp});
    });
  });

  conn.on('error', function(err){
    if (err) {
      sails.log.debug('err SFTP_CONNECTION : ' , err);
      destroy_fnc(conn, sftp);
      deferred.reject(err);
    }
  });

  conn.on('end', function(){
    sails.log.debug('end conn : ' );
    destroy_fnc(conn, sftp);
  });

  conn.connect(conn_info);

  return deferred.promise;
}

function _destroyConnectSFTP(c, f){
  try{
    if(f) f.end();
    if(c){
      c.end();
      c.destroy();
    }
    c = null;
    f = null;
  }catch(e){

  }
}

function _createDir(conn, sftp, dir){
  var d = Q.defer();
  try{
    sftp.mkdir(dir, function(err){
      if(err){
        //throw err;
        sails.log.debug('fail, _createDir: ' , err);
        d.reject(err);

      }else{
        d.resolve({connection:conn, sftp:sftp, dir:dir});
      }

    });

  }catch(e){
    d.reject(e);
  }

  return d.promise;
}


function _opendir(conn, sftp, dir, errCode, doErrReject){
    var d = Q.defer();

    // sails.log.debug('_opendir dir: ' , dir);
    try{
        sftp.opendir(dir, function(err, data){
            if(err){
                //throw err;
                if(doErrReject){
                    sails.log.debug('FileService _opendir ERROR doErrReject: ',err);
                    d.reject({code:(errCode || 'ERR_DIR_OPEN'), status:500});
                }else{
                    d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:[], hasDir:false, error:err});
                }


            }else{
                sails.log.debug('success, opendir: ', data);

                sftp.readdir(data, function(err, list) {
                    if(err){
                        //throw err;

                        if(doErrReject){
                            sails.log.debug('FileService readdir ERROR doErrReject: ',err);
                            d.reject({code:(errCode || 'ERR_DIR_READ'), status:500});
                        }else{
                            d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:[], hasDir:true, error:err});
                        }

                    }else{
                        sails.log.debug('success, readdir: ', list);
                        d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:list, hasDir:true});
                    }
                });


            }

        });

    }catch(e){
        sails.log.debug('FileService _opendir ERROR catch doErrReject: ',e);
        d.reject({code:(errCode || 'ERR_DIR_OPEN'), status:500});
    }

    return d.promise;
}

function _removedir(conn, sftp, dir, errCode, doErrReject){
    // sails.log.debug('_removedir: ');
    var d = Q.defer();
    try{
        sftp.rmdir(dir, function(err, list) {
            if(err){
                //throw err;

                if(doErrReject){
                    sails.log.debug('FileService _removedir ERROR doErrReject: ',err);
                    d.reject({code:(errCode || 'ERR_DIR_REMOVE'), status:500});
                }else{
                    d.resolve({connection:conn, sftp:sftp, dir:dir, error:err});
                }

            }else{
                sails.log.debug('success, rmdir: ', list);
                d.resolve({connection:conn, sftp:sftp, dir:dir});
            }
        });
    }catch(e){
        sails.log.debug('FileService _removedir catch ERROR: ',e);
        if(doErrReject){
            d.reject({code:(errCode || 'ERR_DIR_REMOVE'), status:500});
        }else{
            d.resolve({connection:conn, sftp:sftp, dir:dir, error:e});
        }

    }
    return d.promise;
}

function _unlinkFile(conn, sftp, dir, errCode, doErrReject){
    // sails.log.debug('_unlinkFile:::: ');
    // sails.log.debug('conn: ', (typeof conn));
    // sails.log.debug('sftp sftp: ', (typeof sftp));
    // sails.log.debug('dir: ', (dir));

    var d = Q.defer();
    try{
        sftp.unlink(dir, function(err, list) {
            if(err){
                //throw err;
                if(doErrReject){
                    sails.log.debug('FileService _unlinkFile ERROR doErrReject: ',err);
                    d.reject({code:(errCode || 'ERR_DIR_FILE'), status:500});
                }else{
                    d.resolve({connection:conn, sftp:sftp, dir:dir, error:err});
                }
            }else{
                d.resolve({connection:conn, sftp:sftp, dir:dir});
            }
        });
    }catch(e){
        sails.log.debug('FileService _unlinkFile ERROR: ',e);
        if(doErrReject){
            sails.log.debug('FileService _unlinkFile ERROR doErrReject: ',e);
            d.reject({code:(errCode || 'ERR_DIR_FILE'), status:500});
        }else{
            d.resolve({connection:conn, sftp:sftp, dir:dir, error:e});
        }
    }
    return d.promise;
}

function _unlinkFiles(conn, sftp, filesNameArr){
    // sails.log.debug('_unlinkFiles: ');
    // sails.log.debug('conn: ', (typeof conn));
    // sails.log.debug('sftp sftp: ', (typeof sftp));
    // sails.log.debug('filesNameArr: ', (filesNameArr));

    var tmp_promises;
    tmp_promises = _.map(
        filesNameArr,
        function(v, k){
            return _unlinkFile(conn, sftp, v);
        }
    );

    return Q.all(tmp_promises);
}

function _createFileStream(conn, sftp, readFilePath, uploadPath){
  var d = Q.defer();
  try{
    //TODO: readStream, writeStream ==>destroy
    var readStream = fs.createReadStream( readFilePath );
    var writeStream = sftp.createWriteStream( uploadPath );

    writeStream.once('close',function () {
      sails.log.debug( "- file transferred succesfully" );
      writeStream.end();
      readStream = null;
      writeStream = null;
      d.resolve({code:'SUCCESS', connection:conn, sftp:sftp, readFilePath:readFilePath ,uploadPath:uploadPath });
    });

    writeStream.once('end', function () {
      sails.log.debug( "sftp connection closed" );
    });
    readStream.once('error', function (err) {
      if(err){
        //throw err;
        try{
          readStream = null;
          writeStream = null;
        }catch(e){}
        d.reject(err);
      }
    });
    readStream.pipe( writeStream );

  }catch(e){
    d.reject(e);
  }

  return d.promise;
}

function _uploadThreedFiles(source){
  // sails.log.debug('uploadThreedFiles: ',source.td_cd);

  var tmp_mkdir_path = '/'+SFTP_INFO.cwd+SFTP_INFO.upload_dir+'/'+(source.td_cd);
  var deferred = Q.defer();
  var td_ctx = {
      mkdir_path:'/'+SFTP_INFO.cwd+SFTP_INFO.upload_dir+'/'+(source.td_cd),
      connection:null,
      sftp:null,
      dirData:null,
      doRemoveFiles:false,
      doRemoveDir:false
  };

  Q.fcall(_connectSFTPServer)
      .then(_assignConnAndSFTP)
      .then(_openDirectory)
      .then(_checkDirectory)
      .then(_checkRemoveFiles)
      .then(_checkRemoveDir)
      .then(_createDirectory)
      .then(_uploadTdFiles)
      .spread(_allSetttled)
      .catch(function(e){
        sails.log.debug('fail::: FileServices.uploadThreedFiles ER_FILE_UPLOAD: ' , e);
        _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );

        deferred.reject({code:'ER_FILE_UPLOAD',status:500});
      });


    function _connectSFTPServer(){
        return _connectSFTP(SFTP_INFO, _destroyConnectSFTP);
    }

    function _assignConnAndSFTP(results){
    //예외처리때 destroy하기위한  connect, sftp instance의 mutable state
        td_ctx.connection = results.connection;
        td_ctx.sftp = results.sftp;
        td_ctx.mkdir_path = tmp_mkdir_path;
        return Q.all([results]);
    }

    function _openDirectory(){
        sails.log.debug('_openDirectory: ');
        return _opendir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path);
    }

    function _checkDirectory(results){
        sails.log.debug('_checkDirectory: ');
        if(results.hasDir){
            if(results.dirData !==false && results.dirData.length>0){
                sails.log.debug('dir exist && file exist:: file remove, dir remove');

                try{
                    td_ctx.dirData = results.dirData;
                    td_ctx.doRemoveFiles = true;
                    td_ctx.doRemoveDir = true;

                }catch(e){
                    sails.log.debug('WHAT???: ' , e);
                    deferred.reject({code:'ER_FILE_UPLOAD',status:500});
                }

            }else{
                sails.log.debug('dir exist && file empty:: only dir remove');
                td_ctx.doRemoveFiles = false;
                td_ctx.doRemoveDir = true;
            }

        }else{
            sails.log.debug('dir empty && file empty:: ALL create');
            td_ctx.doRemoveFiles = false;
            td_ctx.doRemoveDir = false;
        }

        return td_ctx;
    }


    function _checkRemoveFiles(){
        // sails.log.debug('_checkRemoveFiles: ');
        if(td_ctx.doRemoveFiles){
            _unlinkFiles(
                td_ctx.connection,
                td_ctx.sftp,
                _.map(td_ctx.dirData, function(v,k){return td_ctx.mkdir_path+'/'+v.filename;}))
                .then(function(){
                    return Q.all([td_ctx]);
                })
                .catch(function(er){
                    sails.log.debug('unlinkFiles: catch error ', er);
                    throw new Error('unlinkFiles catch error');
                });

        }else{
            return Q.all([td_ctx]);
        }
    }

    function _checkRemoveDir(){
        // sails.log.debug('_checkRemoveDir: ');
        if(td_ctx.doRemoveDir){
            _removedir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path)
                .then(function(){
                    return Q.all([td_ctx]);
                })
                .catch(function(er){
                    sails.log.debug('unlinkFiles: catch error ', er);
                    throw new Error('unlinkFiles catch error');
                });

        }else{
            return Q.all([td_ctx]);
        }
    }

    function _createDirectory(){
        // sails.log.debug('_createDirectory: ');
        // sails.log.debug('td_ctx.connection: ' , (typeof td_ctx.connection));
        // sails.log.debug('td_ctx.sftp: ' , (typeof td_ctx.sftp));
        // sails.log.debug('td_ctx.mkdir_path: ' , (typeof td_ctx.mkdir_path));
        return _createDir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path);
    }

    function _uploadTdFiles(){
        var uploadFilePromises = [];
        for(var i=0, iTotal = source.upload_req_list.length; i<iTotal; ++i){
          uploadFilePromises.push(_createFileStream(
              td_ctx.connection,
              td_ctx.sftp,
              source.upload_req_list[i],
              td_ctx.mkdir_path+'/'+source.upload_req_file[i]
          ));
        }
        return Q.all(uploadFilePromises);
    }

    function _allSetttled(){
        sails.log.debug('uploadThreedFiles _allSetttled: ' );
        _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );
        td_ctx = null;
        deferred.resolve(source);
    }

    return deferred.promise;
}