<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 페이지텍스트
 */
$lang['nc_admincp']				= '관리자'; 
$lang['nc_loca']				= '현재위치'; 
$lang['nc_skin_peeler']			= '스킨';
$lang['nc_default_style']		= '기본 스타일';
$lang['nc_mac_style']			= 'Mac 스타일';
$lang['nc_sitemap']				= '관리자 메뉴얼';
$lang['nc_admin_navigation']	= '관리자 메뉴';
$lang['nc_update_cache_succ']	= '전체 캐쉬 업데이트 성공';
$lang['nc_update_cache_again']	= '새로고침하여 변경된 내용을 확인할 수 있습니다.';
$lang['nc_hello']				= '안녕하세요';
$lang['nc_logout']				= '로그아웃';
$lang['nc_homepage']			= '메인';
$lang['nc_refresh']				= '새로고침';
$lang['nc_updatestat_qk']		= '신용통계 업데이트';
$lang['nc_modifypw']			= '비밀번호 수정';
$lang['nc_update_cache']		= '캐쉬 업데이트';
$lang['nc_favorite']			= '즐겨찾기';
$lang['nc_to_favorite']			= 'Ctrl+D 조합키로 즐겨찾기에 추가하세요';
$lang['nc_backup_db']			= '디비백업복구파일생성';
$lang['nc_CnBiz_message']		= '시스템 소식';
$lang['nc_auto_redirect']		= '선택 안할시 자동으로 이동';
$lang['nc_back_to_pre_page']	= '이전 페이지 돌아가기';
$lang['nc_assign_right']		= '<br/><br/>본 설정할 권한이 없습니다.';
$lang['nc_quickly_targeted']    = '(퀵 네비게이션)';
/**
 * 导航텍스트
 */
$lang['nc_console']				= '메인';
$lang['nc_normal_handle']		= '퀵메뉴';
$lang['nc_welcome_page']		= '관리자 메인';
$lang['nc_aboutus']				= '회사소개';
$lang['nc_config']				= '설정';
$lang['nc_web_set']				= '사이트설정';
$lang['nc_web_account_syn']		= '계정동기화';
$lang['nc_upload_set']			= '업로드설정';
$lang['nc_points_set']			= '포인트 제도';
$lang['nc_seo_set']				= 'SEO설정';
$lang['nc_goods_set']			= '상품설정';
$lang['nc_seo_url_rewrite']		= 'Rewrite';
$lang['nc_message_set']			= '알림쪽지';
$lang['nc_area_config']			= '지역보기';
$lang['nc_pay_method']			= '결제방식';
$lang['nc_binding_manage']		= '공유연동';
$lang['nc_inform_config']    = '신고관리';
$lang['nc_complain_config']    = '소송관리';
$lang['nc_voucher_price_manage']		= '쿠폰';
$lang['nc_goods']				= '상품';
$lang['nc_class_manage']		= '카테고리 관리';
$lang['nc_brand_manage']		= '브랜드 관리';
$lang['nc_goods_manage']		= '상품 관리';
$lang['nc_type_manage']			= '유형 관리';
$lang['nc_spec_manage']			= '규칙 관리';
$lang['nc_album_manage']		= '이미지 공간';
$lang['nc_groupbuy_manage']		= '공동구매 관리';
$lang['nc_brand_class']			= '브랜드 카테고리';
$lang['nc_store']				= '미니샵';
$lang['nc_store_grade']			= '미니샵등급';
$lang['nc_store_class']			= '미니샵 카테고리';
$lang['nc_store_credit']		= '미니샵신용';
$lang['nc_store_set']			= '기본설정';
$lang['nc_store_manage']		= '미니샵관리';
$lang['nc_domain_manage']		= '2차도메인';
$lang['nc_domain_shop']			= '도메인 리스트';
$lang['nc_store_customer_service']		= '고객센터';
$lang['nc_member']				= '회원';
$lang['nc_member_manage']		= '회원관리';
$lang['nc_exppoints_manage']		= '경험치관리';
$lang['nc_member_level']		= '회원등급';
$lang['nc_limit_manage']		= '권한설정';
$lang['nc_admin_express_set']	= '택배회사';
$lang['nc_admin_express_template']	= '배송탬플릿';
$lang['nc_admin_offpay_area_set'] = '배송지역';
$lang['nc_admin_perform_opt']	= '성능튜닝';
$lang['nc_admin_clear_cache']	= '캐쉬정리';
$lang['nc_admin_search_set']	= '검색설정';
$lang['nc_admin_log']			= '설정일지';
$lang['nc_admin_res_position']	= '추천위치';
$lang['nc_member_notice']		= '회원통지';
$lang['nc_member_pointsmanage']		= '포인트 관리';
$lang['nc_member_pointslog']		= '포인트 상세';
$lang['nc_member_predepositmanage']		= '적립금';
$lang['nc_member_predepositlog']		= '현금잔고 상세내역';
$lang['nc_chat_history']				= '채팅기록';
$lang['nc_trade']				= '거래';
$lang['nc_order_manage']		= '주문관리';
$lang['nc_bill_manage']			= '정산관리';
$lang['nc_website']				= '디자인&내용';
$lang['nc_hscode']				= 'HS코드관리';
$lang['nc_website_setting']		= '기능모듈';
$lang['nc_article_class']		= '문장 카테고리';
$lang['nc_article_manage']		= '게시글관리';
$lang['nc_document']			= '시스템문장';
$lang['nc_partner']				= '파트너';
$lang['nc_navigation']			= '네이비게이션';
$lang['nc_db']					= '디비관리';
$lang['nc_updatestat']			= '통계 업데이트';
$lang['nc_consult_manage']		= '문의관리';
$lang['nc_goods_evaluate']		= '평가관리';
$lang['nc_goods_evaluatestat']		= '평가통계';
$lang['nc_adv_manage']			= '광고관리';
$lang['nc_share_link']			= '공유링크';
$lang['nc_activity_manage']		= '프로모션관리';
$lang['nc_coupon_manage']		= '쿠폰';
$lang['nc_coupon_class_manage']	= '쿠폰 카테고리';
$lang['nc_stat'] = '통계';
$lang['nc_operation'] = '운영';
$lang['nc_tool'] = '도구';
$lang['nc_mobile'] = '모바일';
$lang['nc_apps'] = '응용';
$lang['nc_operation_set'] = '기본설정';
$lang['nc_operation_point'] = '포인트';
$lang['nc_operation_sell'] = '오퍼레이션';
$lang['nc_gold_manage'] = '금화설정';
$lang['nc_gold_buy'] = '금화관리';
$lang['nc_pointprod'] = '선물환전';
$lang['nc_promotion_xianshi'] = '기간한정세일';
$lang['nc_promotion_mansong'] = '금액별 사은품 증정';
$lang['nc_promotion_bundling']		= '할인세트';
$lang['nc_promotion_booth']         = '추천코너';
$lang['nc_mobile_adset'] 			= '광고설정';
$lang['nc_mobile_homeset'] 			= '메인설정';
$lang['nc_mobile_catepic'] 			= '카테고리 이미지';
$lang['nc_mobile_feedback'] 		= '의견트랙백';
$lang['nc_mobile_update_cache'] 	= '캐쉬 업데이트';

$lang['nc_statgeneral']				= '설명및설정';
$lang['nc_statindustry']	        = '업계분석';
$lang['nc_statgoods']				= '상품분석';
$lang['nc_stat']					= '통계';
$lang['nc_statmember']				= '회원통계';
$lang['nc_statstore']				= '미니샵통계';
$lang['nc_stattrade']				= '판매분석';
$lang['nc_statmarketing']			= '마케팅분석';
$lang['nc_stataftersale']			= '애프터 서비스 분석';

$lang['nc_web_index']				= '메인관리';
$lang['nc_product_recd']				= '상품 추천 유형';
$lang['nc_microshop']			= '위몰';
$lang['nc_microshop_goods']			= "랜덤";
$lang['nc_microshop_album']			= "앨범";
$lang['nc_microshop_personal']		= "개인쇼";
$lang['nc_microshop_store']			= "미니샵등급";
$lang['nc_microshop_manage']			= '위몰관리';
$lang['nc_microshop_goods_manage']			= '랜덤관리';
$lang['nc_microshop_goods_class']			= '랜덤 카테고리';
$lang['nc_microshop_personal_manage']			= '개인쇼 관리';
$lang['nc_microshop_personal_class']			= '개인쇼 카테고리';
$lang['nc_microshop_store_manage']			= '미니샵 등급관리';
$lang['nc_microshop_comment_manage']			= '댓글관리';
$lang['nc_microshop_adv_manage']			= '광고관리';
$lang['nc_cms']			= 'CMS';
$lang['nc_cms_manage']			= 'CMS관리';
$lang['nc_cms_navigation_manage']			= '메뉴관리';
$lang['nc_cms_index_manage']			= '메인관리';
$lang['nc_cms_tag_manage']			= '태그관리';
$lang['nc_cms_article_manage']			= '게시글관리';
$lang['nc_cms_article_class']			= '문장 카테고리';
$lang['nc_cms_picture_manage']			= '화보관리';
$lang['nc_cms_picture_class']			= '화보 카테고리';
$lang['nc_cms_special_manage']			= '스페셜관리';
$lang['nc_cms_comment_manage']			= '댓글관리';
$lang['nc_hscode']			= 'hscode';
$lang['nc_hscode_manage']			= 'hscode관리';
$lang['nc_hscode_navigation_manage']			= '메뉴관리';
$lang['nc_hscode_index_manage']			= '메인관리';
$lang['nc_hscode_tag_manage']			= '태그관리';
$lang['nc_hscode_article_manage']			= '게시글관리';
$lang['nc_hscode_article_class']			= '문장 카테고리';
$lang['nc_hscode_picture_manage']			= '화보관리';
$lang['nc_hscode_picture_class']			= '화보 카테고리';
$lang['nc_hscode_special_manage']			= '스페셜관리';
$lang['nc_hscode_comment_manage']			= '댓글관리';
//$lang['nc_sns']						= 'SNS';
$lang['nc_snstrace']				= '소비자동태';
$lang['nc_s_snstrace']				= '미니샵동태';
$lang['nc_s_help']				= '입점도움';
$lang['nc_s_main']				= '미니샵메인';
$lang['nc_s_self']				= '자영업';
$lang['nc_member_album_manage']		= '회원앨범';
$lang['nc_member_tag']				= '회원태그';
$lang['nc_circle']					= '카페';
$lang['nc_circle_setting']			= '카페설정';
$lang['nc_circle_manage']			= '카페관리';
$lang['nc_circle_classmanage']		= '카테고리 설정';
$lang['nc_circle_thememanage']		= '게시글 관리';
$lang['nc_circle_membermanage']		= '멤버 관리';
$lang['nc_circle_memberlevel']		= '맴버거리 설정';
$lang['nc_circle_advmanage']		= '카페메인광고';
$lang['nc_circle_cache']			= '캐쉬 업데이트';
$lang['nc_circle_informnamage']		= '카페 신고 관리';

/**
 * 페이지中的常用텍스트
 */
$lang['nc_select_all']		= '전체선택';
$lang['nc_ensure_del']		= '정말 삭제하시겠습니까?';
$lang['nc_ensure_cancel']		= '정말 취소하시겠습니까?';
$lang['nc_please_select_item'] = '설정하실 대상을 선택하세요';
$lang['nc_del_all']			= '일괄삭제';
$lang['nc_message']			= '쪽지쓰기';
$lang['nc_no_record']		= '등록된 기록이 없습니다.';
$lang['nc_handle']			= '설정';
$lang['nc_edit_all']		= '일괄수정';
$lang['nc_ok']				= '확인';
$lang['nc_cancel']			= '취소';
$lang['nc_verify']          = '심사';
$lang['nc_display']			= '진열';
$lang['nc_search']			= '검색';
$lang['nc_submit']			= '확인';
$lang['nc_please_choose']	= '선택하세요';
$lang['nc_class']			= '카테고리';
$lang['nc_list']			= '리스트';
$lang['nc_all']				= '전체';
$lang['nc_sort']			= '정렬';
$lang['nc_query']			= '검색';
$lang['nc_view']			= '보기';
$lang['nc_to']				= '부터';
$lang['nc_add_sub_class']	= '서브 추가';
$lang['nc_manage']			= '관리';
$lang['nc_editable']		= '수정가능';
$lang['nc_year']			= '년';
$lang['nc_month']			= '월';
$lang['nc_day']				= '일';
$lang['nc_reset']			= '초기화';
$lang['nc_set']				= '설정';
$lang['nc_status']			= '상태';
$lang['nc_cancel_search']	= '검색취소';
$lang['nc_recommend']		= '추천';
$lang['nc_refuse']			= '거절';
$lang['nc_pass']			= '통과';
$lang['nc_back']			= '돌아가기';
$lang['nc_recommendopen']	= '추천시동';
$lang['nc_recommendclose']	= '추천취소';
$lang['nc_applylog']		= '신청일지';
$lang['nc_backlist']		= '리스트 돌아가기';
$lang['nc_detail']		= '상세';
$lang['nc_open']		= 'OPEN';
$lang['nc_close']		= 'CLOSE';
$lang['nc_end']         = '마감';
$lang['nc_prompts']		= 'TIP';
$lang['nc_groupbuy'] = '그룹';
$lang['nc_groupbuy_view'] = '공동구매 정보 보기';
$lang['nc_state'] = '상태';
$lang['nc_end'] = '마감';
$lang['nc_normal'] = '정상';
$lang['nc_invalidation'] = '무효';
$lang['nc_export'] = '불러내기';

/**
 * footer中的텍스트
 */
$lang['nc_index']			= "메인";
$lang['nc_page_execute']	= "페이지 로딩";
$lang['nc_second']			= "초";
$lang['nc_query']			= "검색";
$lang['nc_times']			= "번";
$lang['nc_online']			= "온라인";
$lang['nc_person']			= "명";
$lang['nc_enabled']			= "시동됨";
$lang['nc_disabled']		= "미시동";
$lang['nc_memory_cost']		= "메모리";
?>
