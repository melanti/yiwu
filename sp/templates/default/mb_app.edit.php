<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>다운로드설정</h3>
      <ul class="tab-base">
        <li><a href="JavaScript:void(0);" class="current"><span>다운로드설정</span></a></li>
        <li><a href="index.php?act=mb_app&op=mb_qr"><span>QR코드생성</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
        <ul>
            <li>다운로드 주소는 "http://"포함된 전체적인 주소를 입력하세요</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form id="post_form" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
        <tr class="noborder">
          <td colspan="2" class="required"><label class="" for="mobile_apk">안드로이드 앱 주소:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
            <input type="text" name="mobile_apk" id="mobile_apk" value="<?php echo $output['mobile_apk']['value'];?>" class="txt"></td>
          <td class="vatop tips"></td>
        </tr>
        <tr class="noborder">
          <td colspan="2" class="required"><label class="" for="mobile_apk">안드로이드 버전:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
            <input type="text" name="mobile_apk_version" id="mobile_apk_version" value="<?php echo $output['mobile_version']['value'];?>" class="txt"></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="" for="mobile_ios">iOS앱 주소:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
            <input type="text" name="mobile_ios" id="mobile_ios" value="<?php echo $output['mobile_ios']['value'];?>" class="txt" >
          </td>
          <td class="vatop tips"></td>
        </tr>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15"><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){$("#submitBtn").click(function(){
    if($("#post_form").valid()){
     $("#post_form").submit();
	}
	});
});
//
$(document).ready(function(){
	$('#post_form').validate({
        errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        rules : {
            mobile_apk : {
                url      : true
            },
            mobile_ios  : {
                url      : true
            }
        },
        messages : {
            mobile_apk  : {
                url      : '정확한 링크주소를 입력하세요'
            },
            mobile_ios  : {
                url      : '정확한 링크주소를 입력하세요'
            }
        }
    });
});
</script>
