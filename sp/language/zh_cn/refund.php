<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 共有语言
 */

$lang['refund_manage']            = '환불관리';
$lang['refund_add']	= '환불';
$lang['refund_order_amount']	= '주문금액';
$lang['refund_refund_amount']	= '환불된금액';
$lang['refund_order_refund']	= '환불금액';
$lang['refund_pay_refund']	= '환불가능금액';
$lang['refund_message']	= '메모';
$lang['refund_message_null']	= '메모를 입력하세요';
$lang['refund_add_time']	= '가입시간';
$lang['refund_handle_desc']	= '조작설명';
$lang['refund_order_ordersn']		= '주문번호';
$lang['refund_order_refundsn']		= '환불번호';
$lang['refund_order_add_time']		= '신청시간';
$lang['refund_order_buyer']			= '구매자 회원 아이디';
$lang['refund_pay_min']	= '최소금액';

$lang['refund_seller_message']	= '업체메모';
$lang['refund_state']	= '심사상태';
$lang['refund_state_confirm']	= '심사대기';
$lang['refund_state_yes']	= '동의';
$lang['refund_state_no']	= '부동의';

$lang['refund_seller_confirm']	= '동의여부';
$lang['refund_seller_confirm_null']	= '동의여부를 선택하세요';

$lang['refund_buyer_message']	= '환불원인';
$lang['refund_buyer_add_time']	= '신청시간';
$lang['refund_store_name']			= '업체명';

$lang['return_manage']            = '환불관리';
$lang['return_order_return']	= '환불수량';
$lang['return_add_return']	= '환불가능수량';
$lang['return_number']	= '이번환불';
$lang['return_message']	= '메모';
$lang['return_message_null']	= '메모를 입력하세요';
$lang['return_add_time']	= '가입시간';
$lang['return_order_ordersn']		= '주문번호';
$lang['return_order_returnsn']		= '환불번호';
$lang['return_order_add_time']		= '신청시간';
$lang['return_order_buyer']			= '구매자 회원 아이디';
$lang['return_goods_name']	= '상품명';
$lang['return_number_min']	= '최소수량';
$lang['return_number_max']	= '최대수량';

$lang['return_seller_message']	= '업체내용';
$lang['return_state']	= '심사상태';
$lang['return_seller_confirm']	= '동의여부';
$lang['return_seller_confirm_null']	= '동의여부를 선택하세요';
$lang['return_state_confirm']	= '심사대기';
$lang['return_state_yes']	= '동의';
$lang['return_state_no']	= '부동의';
$lang['return_store_name']			= '업체명';
$lang['return_buyer_message']	= '환불원인';

$lang['order_sn']		= '주문';
$lang['order_max_day'] = '초과';
$lang['order_max_day_cancel'] = '일동안 미지불시 자동으로 취소됩니다.';
$lang['order_max_day_confirm'] = '일동안 미수취시 자동으로 거래완료가 됩니다.';
$lang['order_day_refund'] = '일동안 환불신청 처리안할 시 자동으로 동의처리됩니다.';

$lang['order_refund_freeze_predeposit'] = '환불시 적립금 동결 금액';
$lang['order_refund_add_predeposit'] = '환불시 사용 가능 적립금 금액';
$lang['order_completed_freeze_predeposit'] = '수취 확인시 적립금 동결 금액';

$lang['order_completed'] = '시스텀에서 자동으로 수취가 되어 주문완료가됩니다.';