module.exports = function(req, res, next) {
  var lib_net = require("../controllers/libs/network.js"),
    ip_addr = lib_net.get_ip(req, res);

  if (process.env.NODE_ENV === "local" || (process.env.NODE_ENV !== "production" && ip_addr === "219.251.8.108")) {
    next();
  } else {
    return res.forbidden('This action is only allowed in the DEV/STG office network.');
  }
};
