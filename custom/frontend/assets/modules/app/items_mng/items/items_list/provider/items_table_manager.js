(function($, angular, _){

  angular.module('App.items')
    .service('ItemsTableManager', ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', 'COMMON_UTIL', 'PubSub', ItemsTableManager]);

  function ItemsTableManager(_, $q , $timeout, $http, XHR, API_URL_INFO , COMMON_UTIL, PubSub){
    var vm = this,
        getItemsListReqID = 'itemList_',
        getItemsListReqCnt = 0;

    var COUNT_RANGE = 5,
        VIEW_RANGE_LIST = ['10','20','30','50','100'];

    vm._state = {
      tableData: {
        options:{
          from:'admin',
          query_opt:'item_name',
          period_unit:'all',
          item_name:'',
          item_uuid:'',

          origin_item_id:'',
          td_name:'',
          td_code:'',
          org_id:'',
          org_name:'',
          brand_id:'',
          sale_type:2,

          item_cate_status:'ready,activated,disabled',
          item_cate_id:'',
          item_cate_id_01:'',
          item_cate_id_02:'',
          include_cate_family_flag:true,
          include_cate_unassigned_flag:true,
          item_status:'',
          td_reg_flag:'all',

          order:'registerd_date',
          order_type:'desc',

          period:'period_reg',
          period_reg_start:'',
          period_reg_end:'',
          period_upd_start:'',
          period_upd_end:''
        },
        info: {
          start: 0,
          count: '20',
          total: 0,
          sex: {
            "man": 0,
            "woman": 0,
            "unisex": 0,
            "undefined": 0
          },
          td: {
            "register": 0,
            "not_register": 0
          }
        },
        results: []
      }
    };

    vm.exports = {
      COUNT_RANGE:COUNT_RANGE,
      VIEW_RANGE_LIST:VIEW_RANGE_LIST,
      init:_init,
      getCountRange:function(){return COUNT_RANGE;},
      getViewRangeList:function(){return VIEW_RANGE_LIST;},
      getTableData:_getTableData,
      getTableStateInfo:_getTableStateInfo,
      deleteRow:_deleteRow,
      updateTableData:_updateTableData
    };

    function _init(){
    }

    function _updateTableData(__data){

      console.log('__data.period_reg_start: ' , __data.period_reg_start);
      console.log('__data.period_reg_end: ' , __data.period_reg_end);
      console.log('__data.period_upd_start: ' , __data.period_upd_start);
      console.log('__data.period_upd_end: ' , __data.period_upd_end);

      var deferred = $q.defer();
      var tmp_period_reg_start = ( (COMMON_UTIL.isExist(__data.period_reg_start))?(__data.period_reg_start):vm._state.tableData.options.period_reg_start ),
          tmp_period_reg_end = ( (COMMON_UTIL.isExist(__data.period_reg_end))?(__data.period_reg_end):vm._state.tableData.options.period_reg_end ),
          tmp_period_upd_start = ( (COMMON_UTIL.isExist(__data.period_upd_start))?(__data.period_upd_start):vm._state.tableData.options.period_upd_start ),
          tmp_period_upd_end = ( (COMMON_UTIL.isExist(__data.period_upd_end))?(__data.period_upd_end):vm._state.tableData.options.period_upd_end );

      tmp_period_reg_start = tmp_period_reg_start.replace(/-/g, "/");
      tmp_period_reg_end = tmp_period_reg_end.replace(/-/g, "/");
      tmp_period_upd_start = tmp_period_upd_start.replace(/-/g, "/");
      tmp_period_upd_end = tmp_period_upd_end.replace(/-/g, "/");

      if(tmp_period_reg_start !=''){
        tmp_period_reg_start+=' 00:00:00';
        tmp_period_reg_start = COMMON_UTIL.getcomputedUTCDate(tmp_period_reg_start);
      };
      if(tmp_period_upd_start !=''){
        tmp_period_upd_start+=' 00:00:00';
        tmp_period_upd_start = COMMON_UTIL.getcomputedUTCDate(tmp_period_upd_start);
      };
      if(tmp_period_reg_end !=''){
        tmp_period_reg_end+=' 23:59:59';
        tmp_period_reg_end = COMMON_UTIL.getcomputedUTCDate(tmp_period_reg_end);
      };
      if(tmp_period_upd_end !=''){
        tmp_period_upd_end+=' 23:59:59';
        tmp_period_upd_end = COMMON_UTIL.getcomputedUTCDate(tmp_period_upd_end);
      };

      var tmpStart =(COMMON_UTIL.isExist(__data.start))?(__data.start):vm._state.tableData.info.start;
      var tmpCount =(COMMON_UTIL.isExist(__data.count))?(__data.count):vm._state.tableData.info.count;

      console.log('__data.tmp_period_reg_start: ' , tmp_period_reg_start);
      console.log('__data.tmp_period_reg_end: ' , tmp_period_reg_end);

      var tmpParam=[
        '?start='+( tmpStart ),
        '&count='+( tmpCount ),
          '&from='+( (COMMON_UTIL.isExist(__data.from))?(__data.from):vm._state.tableData.options.from ),
          '&sale_type='+( (COMMON_UTIL.isExist(__data.sale_type))?(__data.sale_type):vm._state.tableData.options.sale_type ),
          '&item_name='+( (COMMON_UTIL.isExist(__data.item_name))?encodeURIComponent(__data.item_name):encodeURIComponent(vm._state.tableData.options.item_name) ),
          '&item_uuid='+( (COMMON_UTIL.isExist(__data.item_uuid))?encodeURIComponent(__data.item_uuid):encodeURIComponent(vm._state.tableData.options.item_uuid) ),

          '&origin_item_id='+( (COMMON_UTIL.isExist(__data.origin_item_id))?encodeURIComponent(__data.origin_item_id):encodeURIComponent(vm._state.tableData.options.origin_item_id) ),
          '&td_name='+( (COMMON_UTIL.isExist(__data.td_name))?encodeURIComponent(__data.td_name):encodeURIComponent(vm._state.tableData.options.td_name) ),
          '&td_code='+( (COMMON_UTIL.isExist(__data.td_code))?encodeURIComponent(__data.td_code):encodeURIComponent(vm._state.tableData.options.td_code) ),
          '&org_id='+( (COMMON_UTIL.isExist(__data.org_id))?encodeURIComponent(__data.org_id):encodeURIComponent(vm._state.tableData.options.org_id) ),
          '&org_name='+( (COMMON_UTIL.isExist(__data.org_name))?encodeURIComponent(__data.org_name):encodeURIComponent(vm._state.tableData.options.org_name) ),

          '&brand_id='+( (COMMON_UTIL.isExist(__data.brand_id))?encodeURIComponent(__data.brand_id):encodeURIComponent(vm._state.tableData.options.brand_id) ),
          '&item_cate_id='+( (COMMON_UTIL.isExist(__data.item_cate_id))?(__data.item_cate_id):vm._state.tableData.options.item_cate_id ),
          '&item_cate_status='+( (COMMON_UTIL.isExist(__data.item_cate_status))?(__data.item_cate_status):vm._state.tableData.options.item_cate_status ),
          '&include_cate_family_flag='+( (COMMON_UTIL.isExist(__data.include_cate_family_flag))?(__data.include_cate_family_flag):vm._state.tableData.options.include_cate_family_flag ),
          '&include_cate_unassigned_flag='+( (COMMON_UTIL.isExist(__data.include_cate_unassigned_flag))?(__data.include_cate_unassigned_flag):vm._state.tableData.options.include_cate_unassigned_flag ),


          '&item_status='+( (COMMON_UTIL.isExist(__data.item_status))?(__data.item_status):'ready,activated,disabled' ),
          '&td_reg_flag='+( (COMMON_UTIL.isExist(__data.td_reg_flag))?(__data.td_reg_flag):vm._state.tableData.options.td_reg_flag ),
          '&period_reg_start='+encodeURIComponent(tmp_period_reg_start),
          '&period_reg_end='+encodeURIComponent(tmp_period_reg_end),
          '&period_upd_start='+encodeURIComponent(tmp_period_upd_start),
          '&period_upd_end='+encodeURIComponent(tmp_period_upd_end),

          '&order='+( (COMMON_UTIL.isExist(__data.order))?(__data.order):vm._state.tableData.options.order ),
          '&order_type='+( (COMMON_UTIL.isExist(__data.order_type))?(__data.order_type):vm._state.tableData.options.order_type ),

          '&show='+encodeURIComponent('thumbnail,td_id,td_list,td_fitting,debug,categories,preview')
        ].join('');

      XHR.CANCEL( getItemsListReqID+(getItemsListReqCnt) );

      XHR
        .REQ({
          cancelID: getItemsListReqID+(++getItemsListReqCnt),
          method: 'get',
          url: API_URL_INFO.GET_ITEMS_LIST+tmpParam
        })
        .then(
          function(__sucData){
            if(__sucData.data.info){
              if( __sucData.data.info.start==null || __sucData.data.info.start=='null' )  {
                __sucData.data.info.start = parseInt(tmpStart);
              }
              __sucData.data.info.count = parseInt(tmpCount);
            }

            _setTableData(__sucData.data);
            deferred.resolve(_getTableData());
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _getTableData(){
      return vm._state.tableData;
    }

    function _getTableStateInfo(__key){
      return vm._state.tableData.info[__key];
    }

    function _setTableData(__data){
      if(__data){
        vm._state.tableData = _.extend({}, vm._state.tableData, __data);
      }else{
        vm._state.tableData = _.extend({}, vm._state.tableData);
      }
      PubSub.trigger('ItemsSearchOpt:change',vm._state.tableData);
    }


    function _deleteRow(__data){
      var deferred = $q.defer();

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.CHANGE_STATUS,
          data: {ids: __data.items_ids, kind:'item', status:'deleted', show:'debug,contact'}
        })
        .then(
          function(__sucData){
            PubSub.trigger('TOTAL_ITEMS_COUNT');
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }


  }


})(window.jQuery, window.angular, window._);
