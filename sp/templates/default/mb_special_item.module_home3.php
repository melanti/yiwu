<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if($item_edit_flag) { ?>
<table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12" class="nobg"> <div class="title nomargin">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span> </div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>내용추가 버튼을 클릭하여 새로운 이미지를 업로드해주세요</li>
            <li>해당 내용에 이미지 롤 오버시 삭제 버튼으로 해당 내용을 조작할 수 있습니다.</li>
            <li>모든 내용 변경후 저장 버튼을 눌러 적용;</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <?php } ?>
<div class="index_block home3">
      <?php if($item_edit_flag) { ?>
  <h3>모형모듈C</h3>
  <?php } ?>
  <div class="title">
    <?php if($item_edit_flag) { ?>
    <h5>제목：</h5>
    <input id="home1_title" type="text" class="txt w200" name="item_data[title]" value="<?php echo $item_data['title'];?>">
    <?php } else { ?>
    <span><?php echo $item_data['title'];?></span>
    <?php } ?>
  </div>
  <div cbtype="item_content" class="content">
      <?php if($item_edit_flag) { ?>
    <h5>내용：</h5>
    <?php } ?>
    <?php if(!empty($item_data['item']) && is_array($item_data['item'])) {?>
    <?php foreach($item_data['item'] as $item_key => $item_value) {?>
    <div cbtype="item_image" class="item"> <img cbtype="image" src="<?php echo getMbSpecialImageUrl($item_value['image']);?>" alt="">
      <?php if($item_edit_flag) { ?>
      <input cbtype="image_name" name="item_data[item][<?php echo $item_key;?>][image]" type="hidden" value="<?php echo $item_value['image'];?>">
      <input cbtype="image_type" name="item_data[item][<?php echo $item_key;?>][type]" type="hidden" value="<?php echo $item_value['type'];?>">
      <input cbtype="image_data" name="item_data[item][<?php echo $item_key;?>][data]" type="hidden" value="<?php echo $item_value['data'];?>">
      <a cbtype="btn_del_item_image" href="javascript:;"><i class="icon-trash"></i>삭제</a>
      <?php } ?>
    </div>
    <?php } ?>
    <?php } ?>
  </div>
  <?php if($item_edit_flag) { ?>
  <a cbtype="btn_add_item_image" class="btn-add" data-desc="320*85" href="javascript:;">내용추가</a>
  <?php } ?>
</div>
