<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 추천位
 */
$lang['rec_position']		= '추천위치';
$lang['rec_ps_type']		= '유형';
$lang['rec_ps_pic']			= '이미지';
$lang['rec_ps_txt']			= '텍스트';
$lang['rec_ps_title']		= '타이틀';
$lang['rec_ps_content']		= '내용';
$lang['rec_ps_gourl']		= '링크 주소';
$lang['rec_ps_target']		= '띄울방식';
$lang['rec_ps_help1']		= '추천위치코드 복사후 샵에붙여서 쓰실수 있으며 텍스트와 이미지 형식을 지지합니다.';
$lang['rec_ps_picb']		= '이미지[태컴퓨터]';
$lang['rec_ps_picy']		= '이미[원격]';
$lang['rec_ps_picdan']		= '[이미지 한장]';
$lang['rec_ps_picduo']		= '[이미지 여러장]';
$lang['rec_ps_tg1']			= '현재 창';
$lang['rec_ps_tg2']			= '새로운 창';
$lang['rec_ps_code']		= '코드 호출';
$lang['rec_ps_view']		= '미리 보기';
$lang['rec_ps_title_tips']	= '추천위치 설명에는 간편한 유지 관리 및 사용;탬플릿에 표시되지 않습니다.';
$lang['rec_ps_type_tips']	= '드롭다운 추천위치 내용은, 이미지 및 텍스트 형식으로 표현가능.';
$lang['rec_ps_type_tips2']	= '컴퓨터 이미지 업로드 및 원격 이미지 주소로 추가 업로드 가능,확장명jpg/gif/png권장 ';
$lang['rec_ps_select_pic']	= '이미지 위치 선택하기';
$lang['rec_ps_local']		= '내컴퓨터 이미지';
$lang['rec_ps_remote']		= '원격이미지';
$lang['rec_ps_add_remote']	= '원격이미지 추가';
$lang['rec_ps_edit_remote']	= '원격이미지 파일 편집';
$lang['rec_ps_remote_url']	= '원격 이미지 주소';
$lang['rec_ps_ztxt']		= '텍스트 표시하기';
$lang['rec_ps_addjx']		= '계속해서 추가하기';
$lang['rec_ps_selfile_local']= '이미지 위치';
$lang['rec_ps_selfile']		= '내컴퓨터 이미지 파일 수정';
$lang['rec_ps_selfile_edit']= '내 컴퓨터 이미지 파일 수정';
$lang['rec_ps_support']		= '만 지지 합니다';
$lang['rec_ps_kcg']			= '이미지 크기설정';
$lang['rec_ps_kcg_tips']	= '이미지 사이즈 설정px;비워두실 시 이미지 실제사이즈가 적용됩니다.';
$lang['rec_ps_image_width']	= 'width';
$lang['rec_ps_image_height']= 'high';
$lang['rec_ps_error_ztxt']	= '표시될 텍스트를 입력하여 주십시오.';
$lang['rec_ps_error_pics']	= '이미지를 선택하여 업로드 해주십시오.';
$lang['rec_ps_error_picy']	= '원격이미지 주소를 넣어 주십시오.';
$lang['rec_ps_error_jz']	= '이미지추가가 초과 하였으므로 새이미지르 추가 하실 수 없습니다.';
$lang['rec_ps_copy_clip']	= '브라우저가 자동복사 기능을 지원하지 않으므로 수동으로 복사하여 주십시오.';
$lang['rec_ps_clip_succ']	= '클립 보드에 성공적으로 복사하였습니다.';