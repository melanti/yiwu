<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>E-쿠폰 주문환불</h3>
      <ul class="tab-base">
        <li><a href="index.php?act=vr_refund&op=refund_manage"><span><?php echo '심사대기';?></span></a></li>
        <li><a href="index.php?act=vr_refund&op=refund_all"><span><?php echo '전체기록';?></span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span><?php echo '심사';?></span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="post_form" method="post" action="index.php?act=vr_refund&op=edit&refund_id=<?php echo $output['refund']['refund_id']; ?>">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
        <tr>
          <td colspan="2" class="required"><?php echo $lang['refund_order_refund'].$lang['nc_colon'];?></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><?php echo ncPriceFormatKo($output['refund']['refund_amount_ko']); ?>원</td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required">상품명<?php echo $lang['nc_colon'];?></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><a href="<?php echo urlShop('goods','index',array('goods_id'=> $output['refund']['goods_id']));?>" target="_blank"><?php echo $output['refund']['goods_name_ko']; ?></a></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required">교환번호 환불<?php echo $lang['nc_colon'];?></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
        <?php if (is_array($output['code_array']) && !empty($output['code_array'])) { ?>
          <?php foreach ($output['code_array'] as $key => $val) { ?>
          <?php echo $val;?><br />
          <?php } ?>
        <?php } ?>
            </td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><?php echo '환불설명'.$lang['nc_colon'];?></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><?php echo $output['refund']['buyer_message']; ?></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation">동의여부<?php echo $lang['nc_colon'];?></label>
            </td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform onoff">
            <label for="state1" class="cb-enable" title="<?php echo $lang['nc_yes'];?>"><span><?php echo $lang['nc_yes'];?></span></label>
            <label for="state0" class="cb-disable" title="<?php echo $lang['nc_no'];?>"><span><?php echo $lang['nc_no'];?></span></label>
            <input id="state1" name="admin_state"  value="2" type="radio">
            <input id="state0" name="admin_state" value="3" type="radio"></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation"><?php echo $lang['refund_message'].$lang['nc_colon'];?></label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform">
        	<textarea id="admin_message" name="admin_message" class="tarea"></textarea></td>
          <td class="vatop tips"></td>
        </tr>

      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15" ><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/custom.min.js" charset="utf-8"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/styles/nyroModal.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">
$(function(){
    $('.nyroModal').nyroModal();
	$("#submitBtn").click(function(){
    if($("#post_form").valid()){
     $("#post_form").submit();
		}
	});
    $('#post_form').validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parentsUntil('tr').parent().prev().find('td:first'));
        },
        rules : {
            admin_state : {
                required   : true
            },
            admin_message : {
                required   : true
            }
        },
        messages : {
            admin_state : {
                required   : '환불 동의여부를 선택하세요.'
            },
            admin_message  : {
                required   : '<?php echo $lang['refund_message_null'];?>'
            }
        }
    });
});
</script>