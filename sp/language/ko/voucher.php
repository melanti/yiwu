<?php
defined('InCNBIZ') or exit('Access Invalid!');
$lang['admin_voucher_unavailable']    = '쿠폰,쿠폰을 설정하셔야 합니다. 설정페이지로 넘어갑니다....';
$lang['admin_voucher_jb_unavailable']    = '금화를 설정하셔야 합니다. 금화 설정 페이지로 넘어갑니다....';
$lang['admin_voucher_applystate_new']    = '심사대기중';
$lang['admin_voucher_applystate_verify']    = '심사됨';
$lang['admin_voucher_applystate_cancel']    = '취소됨';
$lang['admin_voucher_quotastate_activity']	= '정상';
$lang['admin_voucher_quotastate_cancel']    = '취소';
$lang['admin_voucher_quotastate_expire']    = '종료';
$lang['admin_voucher_templatestate_usable']	= '유효';
$lang['admin_voucher_templatestate_disabled']= '실패';
$lang['admin_voucher_storename']			= '미니샵명';
$lang['admin_voucher_cancel_confirm']    	= '취소하시겠습니까?';
$lang['admin_voucher_verify_confirm']    	= '심사하시겠습니까?';
//菜单
$lang['admin_voucher_apply_manage']= '패키지 신청 관리';
$lang['admin_voucher_quota_manage']= '패키지관리';
$lang['admin_voucher_template_manage']= '미니샵 쿠폰';
$lang['admin_voucher_template_edit']= '쿠폰 편집';
$lang['admin_voucher_setting']		= '쿠폰 설정';
$lang['admin_voucher_pricemanage']		= '금액 설정';
$lang['admin_voucher_priceadd']		= '금액추가';
$lang['admin_voucher_priceedit']		= '금액편집';
$lang['admin_voucher_styletemplate']	= '탬플릿 스타일';
/**
 * 설정
 */
$lang['admin_voucher_setting_price_error']		= '구매단위는 최소0보다 큰 정수여야 합니다.';
$lang['admin_voucher_setting_storetimes_error']	= '매월 프로모션수량은 최소0보다 큰 정수여야 합니다.';
$lang['admin_voucher_setting_buyertimes_error']	= '최대 받을수 있는 숫자는 최소 0보다 큰 정수여야 합니다.';
$lang['admin_voucher_setting_price']			= '구매단위(원/월）';
$lang['admin_voucher_setting_price_tip']		= '쿠폰프로모션 구미시 필요한 금액, 구매후 판매자는 구매주내에 쿠폰프로모션 발행 가능 ';
$lang['admin_voucher_setting_storetimes']		= '매월프로모션 수량';
$lang['admin_voucher_setting_storetimes_tip']	= '매월 최대 발표할수있는 쿠폰 프로모션 수량';
$lang['admin_voucher_setting_buyertimes']		= '구매자가 최대 받을수 있는 수량';
$lang['admin_voucher_setting_buyertimes_tip']	= '구매자는 최대 하나의 미니샵에서 소유할수 있는 소비되지 않은 쿠폰의 수량은? ';
//$lang['admin_voucher_setting_default_styleimg']	= '쿠폰默认样式模板';
/**
 * 쿠폰금액
 */
$lang['admin_voucher_price_error']   		= '쿠폰 금액은 최소0보다 큰 정수여야 합니다.';
$lang['admin_voucher_price_describe_error'] = '설명은 비워두실 수 없습니다.';
$lang['admin_voucher_price_describe_lengtherror'] = '쿠폰 설명은 비워두실 수 없으며 최대255자 입니다.';
$lang['admin_voucher_price_points_error']   = '기본 포인트 교환 가능 수는 최소 0보다큰 정수여야 합니다.';
$lang['admin_voucher_price_exist']    		= '쿠폰 금액이 이미 존재합니다';
$lang['admin_voucher_price_title']    		= '쿠폰 금액';
$lang['admin_voucher_price_describe']    	= '설명';
$lang['admin_voucher_price_points']    		= '기본 포인트 교환 가능 수 ';
$lang['admin_voucher_price_points_tip']    	= '쿠폰 판매가 프로모션 등록시 기본 소비 포인트 수 ';
$lang['admin_voucher_price_tip1']    	= '관리자는 쿠폰금액을 규정하며 미니샵에서 쿠폰 발행시 가격옵션에서 선택해야 합니다.';
/**
 * 쿠폰套餐申请
 */
$lang['admin_voucher_apply_goldnotenough']	= '회원잔액이 부족합니다. 작업실패 하였습니다.';
$lang['admin_voucher_apply_goldlog']    	= '쿠폰 프로모션 %s개월,단가%s금액,총지불%s금액 구매';
$lang['admin_voucher_apply_message']    	= '성공적으로 쿠폰 프로모션를 구매하였으며%s개월,단가%s금액,총지불%s금액,시간은 심사후부터 계산';
$lang['admin_voucher_apply_verifysucc']    	= '신청심사성공，프로모션 패키지 발행 완료';
$lang['admin_voucher_apply_verifyfail']    	= '신청심사실패';
$lang['admin_voucher_apply_cancelsucc']    	= '신청취소성공';
$lang['admin_voucher_apply_cancelfail']    	= '신청취소실패';
$lang['admin_voucher_apply_list_tip1']    	= '판매자의 패키지에 대하여 심사가 진행되며 심사후 시스템은 편지를 통해 판매자에게 알림.';
$lang['admin_voucher_apply_list_tip2']    	= '판매자가 금액부족으로 심사 실패 시 판매자가 발행한 쿠폰은 쿠폰 센터에 나타나며 구매자는 쿠폰으로 교환 가능 합니다. ';
$lang['admin_voucher_apply_num']    		= '신청 수량';
$lang['admin_voucher_apply_date']    		= '신청 날짜';
/**
 * 쿠폰套餐
 */
$lang['admin_voucher_quota_cancelsucc']    	= '패키지 취소 성공';
$lang['admin_voucher_quota_cancelfail']    	= '패키지 취소 실패';
$lang['admin_voucher_quota_tip1']    	= '취소후 복구 할수 없습니다. 신중을 가하시기 바랍니다.';

$lang['admin_voucher_quota_startdate']    	= '시작 시간';
$lang['admin_voucher_quota_enddate']    	= '종료 시간';
$lang['admin_voucher_quota_timeslimit']    	= '프로모션 차수 제한';
$lang['admin_voucher_quota_publishedtimes']    	= '이미 발표한 프로모션 수';
$lang['admin_voucher_quota_residuetimes']    	= '남은 프로모션 수 ';
/**
 * 쿠폰
 */
$lang['admin_voucher_template_points_error']	= '필요한 포인트 교환수량은 최소0보다 큰 정수여야 합니다.';
$lang['admin_voucher_template_title']			= '쿠폰 명칭';
$lang['admin_voucher_template_enddate']			= '유효기간';
$lang['admin_voucher_template_price']			= '금액';
$lang['admin_voucher_template_total']			= '총발행 수';
$lang['admin_voucher_template_eachlimit']		= '개개인의 받을수 있는 제한';
$lang['admin_voucher_template_orderpricelimit']	= '소비금액';
$lang['admin_voucher_template_describe']		= '쿠폰설명';
$lang['admin_voucher_template_styleimg']		= '쿠폰 스킨 선택';
$lang['admin_voucher_template_image']			= '쿠폰 이미지';
$lang['admin_voucher_template_points']			= '포인트';
$lang['admin_voucher_template_adddate']			= '시간 추가 ';
$lang['admin_voucher_template_list_tip']		= '직접 쿠폰 설정에 실패하게 되면, 사용자는 해당 쿠폰을 발급 받을 수 없습니다. (*이미 발급 된 쿠폰은 사용 가능 합니다.)';
$lang['admin_voucher_template_giveoutnum']		= '기 발급';
$lang['admin_voucher_template_usednum']			= '사용중';
?>
