<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['express_name']	= '택배사';
$lang['express_order']	= '사용';
$lang['express_state']	= '상태';
$lang['express_pickup']	= '픽업배송';
$lang['express_letter']	= '첫글자';
$lang['express_url']	= '사이트 (참고)';

$lang['express_index_help1']		= '시스텀에서 설정된 택배사는 삭제할 수 없으며 수정만 가능합니다, 첫글자의 순위로 정렬이 됩니다.';
$lang['express_index_help2']		= '상태 수정후 설정 -> 캐쉬 비우기를 하셔야 확인가능합니다.';