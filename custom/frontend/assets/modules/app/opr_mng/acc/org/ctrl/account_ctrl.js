(function ($, angular, _, APP) {

  APP.modules.ctrl.accountCtrl = accountCtrl;

  //accountCtrl
  function accountCtrl($scope, _, $timeout, XHR, PubSub, AccOrgListTableManager, API_URL_INFO) {
    var vm = this,
      listURL = APP.info.gURL + APP.URL_INFO.menu.OPR_MNG_ACC_ORG_LIST.link;
    var _isRegistration = vm.pageType === "registration";

    vm._state = {
      account_uuid: '',
      org_type: '0',
      org_id: 0,
      account_type: 0,
      id: '',
      password: '',
      status: 0,
      service: '',
      contact_name: '',
      contact_phone: '',
      contact_mail: ''
    };

    vm.auth = {
      majorOrganization: (vm.orgType === 'registration' && vm._state.org_type ) == '0' ? 1 : 0,
      minorOrganization:  0,
      organizationAccount: 0,
      category: 0,
      brand: 0,
      tdItem: 0,
      item: 0,
      batchItem: 0,
      mirrorVideo: 0,
      organizationVideo: 0,
      license: 0,
      masterAccount: 0,
      defaultVideo: 0
    };

    vm.pageType;
    vm.accountUuid;
    vm.accountNo;

    vm.adminList = [];
    vm.storeList = [];

    vm.parent_org_name = "";
    vm.org_name = "";

    vm.selectedStoreId = '';

    vm.serviceShop = false;
    vm.serviceCat = false;
    vm.serviceMirror = false;

    vm.password_confirm = '';
    vm.duplicateId = true;

    vm.isStore = APP.info.user.parent_org_id !== APP.info.user.org_id;

    vm.exports = {
      init: _init,
      submit: _submit,
      duplicateId: _duplicateId,
      changeType: _changeType,
      changeStore: _changeStore,
      getData: _getData,
      goListPage: _goListPage,
      deleteAcc: _deleteAcc
    };
    function _init() {
      console.log('init');
      //console.log(APP.info.user);
      if (_isRegistration) {
        _getStoreList()
      } else {
        _getAuth();
        _getData()
      }
      vm._state.org_id = APP.info.user.org_id;
      !(!vm.isStore && vm._state.org_type == '1') && (vm._state.account_type = 1);
    }
    _init();

    function _getAuth() {
      XHR.GET(API_URL_INFO.GET_AUTH + '?account_uuid=' + vm.accountUuid + '&show=debug').then(
        function (__suc) {
          //console.log(__suc);
          var _d = __suc.data.results[0];
          for(var i in _d){
            vm.auth[i] = _d[i];
          }
        }, function (__err) {
          console.log(__err);
        })
    }

    //입점업체 리스트 get
    function _getStoreList(){
      XHR.GET(API_URL_INFO.GET_SELECT_MINOR_ORGANIZATION + '?parent_org_id=' + APP.info.user.org_id + '&show=debug').then(
        function (__suc) {
          //console.log(__suc);
          vm.storeList = __suc.data.results;
        }, function (__err) {
          console.log(__err);
        })
    }

    // 수정을 위한 데이터 조회
    function _getData() {
      if (!vm.accountUuid)return;
      vm._state.account_uuid = vm.accountUuid;
      XHR.GET(API_URL_INFO.GET_ACCOUNT
        + '?account_uuid=' + vm._state.account_uuid
        + '&org_id=' + vm.orgId
        + '&org_type=' + vm.orgType
        + '&show=debug').then(
        function (__suc) {
          console.log(__suc.data.results[0]);
          var _ret = __suc.data.results[0];
            for(var i in _ret){
              if (i === "parent_org_name" || i === "org_name") {
                vm[i] = _ret[i];
              } else {
                vm._state[i] = _ret[i];
              }
            }
          var _arr_service = vm._state.service.split(",");
          for (var i in _arr_service) {
            switch (_arr_service[i].trim()) {
              case '0':
                vm.serviceShop = true;
                break;
              case '1':
                vm.serviceCat = true;
                break;
              case '2':
                vm.serviceMirror = true;
              default:
                break;
            }
          }
        }, function (__err) {
          console.log(__err);
        })
    }

    function _submit() {
      if (vm.pageType === "confirmation") {
        _goListPage();
        return;
      }
      if (_isRegistration) {
        if (vm._state.org_type == '1' && !vm._state.org_id) {
          alert('계정이 등록될 입점업체를 선택해 주십시오.\n입점업체는 필수 입력 항목입니다.');
          return;
        }
        if (!vm._state.id) {
          alert('사용할 아이디를 입력해 주십시오.\n아이디는 필수 입력 항목입니다.');
          return;
        }
        if (vm.duplicateId) {
          alert('아이디 중복 확인이 되지 않았습니다.\n아이디 중복확인을 해주시기 바랍니다.');
          return;
        }
      }
      if (!vm._state.password) {
        alert('패스워드를 입력해 주십시오.\n패스워드는 필수 입력 항목입니다.');
        return;
      }
      if (!vm.password_confirm) {
        alert('패스워드 확인이 되지 않았습니다.\n패스워드 확인을 진행해 주십시오');
        return;
      }
      if (vm._state.password !== vm.password_confirm) {
        alert('비밀번호와 비빌번호 확인이 일치하지 않습니다');
        return;
      }
      if(vm._state.password.match(/^(?=.*[a-zA-Z])((?=.*\d)|(?=.*\W)).{10,16}$|^(?=.*\d)(?=.*\W).{10,16}$/) === null){
        alert('비밀번호는 영문 대소문자/숫자/특수문자 중 두가지 이상 조합으로 10자리~16자리를 입력해야 합니다');
        return;
      }
      _submitReq();
    }

    function _submitReq(){
      var _service = [];
      vm.serviceShop && _service.push(0);
      vm.serviceCat && _service.push(1);
      vm.serviceMirror && _service.push(2);
      vm._state.service = _service.join(',');
      XHR.POST(_isRegistration ? API_URL_INFO.SET_ACCOUNT : API_URL_INFO.UPDATE_ACCOUNT, vm._state).then(
        function (__suc) {
          //console.log(__suc);
          _submitAuthReq(_isRegistration ? __suc.data.account_uuid : '');
        }, function (__err) {
          console.log(__err);
          if(__err.data.code == "ER_DUP_ENTRY: account_type"){
            alert('대표 운영자 아이디가 이미 등록된 업체입니다. 업체당 하나의 대표 운영자 아이디만 만들 수 있습니다.');
          }
          alert('계정 등록에 실패하였습니다');
        })
    }

    function _submitAuthReq(account_uuid) {
      var _data = _.extend({}, vm.auth, {"account_uuid": account_uuid || vm.accountUuid});
      XHR.POST(_isRegistration ? API_URL_INFO.SET_AUTH : API_URL_INFO.UPDATE_AUTH, _data).then(
        function (__suc) {
          console.log(__suc);
          vm.pageType === '' ? alert('계정 등록이 완료되었습니다') : alert('저장되었습니다');
          _goListPage();
        }, function (__err) {
          console.log(__err);
          alert('계정 등록에 실패하였습니다');
        })
    }

    function _duplicateId(){
      if (!vm._state.id) {
        alert('아이디를 입력해주세요');
        return;
      }
      XHR.GET(API_URL_INFO.GET_ACCOUNT_ID + '?account_id=' + vm._state.id + '&show=debug').then(
        function (__suc) {
          //console.log(__suc);
          vm.duplicateId = JSON.parse(__suc.data.results[0].isAccountId);
          !vm.duplicateId ? alert('사용 가능한 아이디입니다.') : alert('이미 등록된 아이디입니다.\n다른 아이디를 입력해 주시기 바랍니다.');
        }, function (__err) {
          console.log(__err);
          alert('아이디 중복 확인에 실패했습니다.');
        })
    }

    function _changeType() {
      vm._state.org_id = '';
      vm.selectedStoreId = '';
      if (vm._state.org_type == '0') {
        vm._state.account_type = '1';
        vm.auth.majorOrganization = 1;
        vm.auth.minorOrganization = 0;
      } else {
        vm.auth.majorOrganization = 0;
        vm.auth.minorOrganization = 1;
        vm.auth.category = 0;
        vm.auth.brand = 0;
      }
    }

    function _changeStore(){
      vm._state.org_id = vm.selectedStoreId;
    }

    function _goListPage(){
      $timeout(function(){
        window.location.replace( listURL );
      }, 200);
    }

    function _deleteAcc() {
      if (confirm("선택한" + vm._state.id + "를 삭제하시겠습니까?")) {
        AccOrgListTableManager.exports.deleteRow({
          ids: vm.accountNo,
          kind: 'account',
          status: 'deleted',
          show: 'debug,contact'
        }).then(function (__sucData) {
          //console.log(__sucData);
          _goListPage();
        }, function (__errData) {
          console.log(__errData);
          alert('계정 삭제에 실패했습니다.')
        });
      } else {
        return false;
      }
    }
  }

  accountCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'AccOrgListTableManager', 'API_URL_INFO'];
  accountCtrl.bindToController = {
    pageType: '@',
    accountUuid: '@',
    orgId: '@',
    orgType: '@'
  }
  accountCtrl.link = function(){
    return function (scope, elem, attrs, ctrl, transclude) {
      ctrl.type = attrs.type || '';
      (attrs.type === "") && scope.$watch('accountCtrl._state.org_type', function (n, o) {
        scope.accountCtrl.selectedStoreId = "";
        scope.accountCtrl._state.org_id = "";
      });
      scope.$watch('accountCtrl._state.id', function (n, o) {
        (n !== o) && (ctrl.duplicateId = true);
      });
    }
  }
})(window.jQuery, window.angular, window._, window.APP);
