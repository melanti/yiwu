<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if($item_edit_flag) { ?>
<table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12" class="nobg"> <div class="title nomargin">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span> </div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>새광고추가 버튼을 클릭하여 광고를 추가;</li>
            <li>마우스 롤 오버시 삭제 버튼을 해당 광고를 삭제;</li>
            <li>모든 내용 변경후 저장 버튼을 눌러서 적용;</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <?php } ?>
<div class="index_block adv_list">
    <?php if($item_edit_flag) { ?>
  <h3>광고모듈</h3>
  <?php } ?>
  <div cbtype="item_content" class="content">
    <?php if($item_edit_flag) { ?>
    <h5>내용：</h5>
    <?php } ?>
    <?php if(!empty($item_data['item']) && is_array($item_data['item'])) {?>
    <?php foreach($item_data['item'] as $item_key => $item_value) {?>
    <div cbtype="item_image" class="item"> <img cbtype="image" src="<?php echo getMbSpecialImageUrl($item_value['image']);?>" alt="">
      <?php if($item_edit_flag) { ?>
      <input cbtype="image_name" name="item_data[item][<?php echo $item_key;?>][image]" type="hidden" value="<?php echo $item_value['image'];?>">
      <input cbtype="image_type" name="item_data[item][<?php echo $item_key;?>][type]" type="hidden" value="<?php echo $item_value['type'];?>">
      <input cbtype="image_data" name="item_data[item][<?php echo $item_key;?>][data]" type="hidden" value="<?php echo $item_value['data'];?>">
      <a cbtype="btn_del_item_image" href="javascript:;"><i class="icon-trash"></i>삭제</a>
      <?php } ?>
    </div>
    <?php } ?>
    <?php } ?>
  </div>
  <?php if($item_edit_flag) { ?>
  <a cbtype="btn_add_item_image" class="btn-add" data-desc="640*240" href="javascript:;">새광고추가</a>
  <?php } ?>
</div>
