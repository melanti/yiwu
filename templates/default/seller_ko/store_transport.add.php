<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form method="post" id="tpl_form" name="tpl_form" action="index.php?act=store_transport&op=save">
    <input type="hidden" name="transport_id" value="<?php echo $output['transport']['id'];?>" />
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="type" value="<?php echo $_GET['type'];?>">
    <dl>
      <dt>
        <label for="J_TemplateTitle" class="label-like">제목</label>
      </dt>
      <dd>
        <input type="text"  class="text"  id="title" autocomplete="off"  value="<?php echo $output['transport']['title'];?>" name="title">
        <p class="J_Message" style="display:none" error_type="title"><i class="icon-exclamation-sign"></i>제목을 입력하여 주십시오.</p>
      </dd>
    </dl>
    <dl>
      <dt>배송료설정 : </dt>
      <dd style="line-height:20px !important;">지정하신 지역 외 배송료는 "기본배송료"를 따르게 됩니다.<br />
      유이한궈에서는 EMS권장해드립니다, 배송시 5KG이상이 되는 주문은 5KG단위로 묶어서 배송하시면 통관에 걸릴 확율이 거의 없습니다.<br />
      <a href="http://www.koreapost.go.kr/kpost/sub/subpage.jsp?contId=010103010700" target="_blank">우체국택배</a>페이지에서 [EMS 요금]->[비서류] 부분을 확인하셔서 배송료를 설정하시면 됩니다.<br />
      2015.7.1부터 적용된 요금은 0.5kg=18,100입니다. <br />
      즉 아래 기본배송료 0.5kg이내에 18,100원을 입력하시고 매 0.5kg 추가비용은 평균 2,000원을 입력하시면 적합하실거 같습니다.

      </dd>
    </dl>

   
    <dl>
      <dt></dt>
      <dd class="trans-line">
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input type="submit" id="submit_tpl" class="submit" value="저장" /></label>
    </div>
  </form>
  <div class="ks-ext-mask" style="position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 999; display:none"></div>
  <div id="dialog_areas" class="dialog-areas" style="display:none">
    <div class="ks-contentbox">
      <div class="title">지역선택<a class="ks-ext-close" href="javascript:void(0)">X</a></div>
      <form method="post">
        <ul id="J_CityList">
          <?php require(template('seller/store_transport_area'));?>
        </ul>
        <div class="bottom"> <a href="javascript:void(0);" class="J_Submit ncsc-btn ncsc-btn-green">확인</a> <a href="javascript:void(0);" class="J_Cancel ncsc-btn">취소</a> </div>
      </form>
    </div>
  </div>
  <div id="dialog_batch" class="dialog-batch" style="z-index: 9999; display:none">
    <div class="ks-contentbox">
      <div class="title">일괄관리<a class="ks-ext-close" href="javascript:void(0)">X</a></div>
      <form method="post">
        <div class="batch"><?php echo $lang['transport_note_1'].$lang['nc_colon'];?>
        <input class="w30 mr5 text" type="text" maxlength="4" autocomplete="off" data-field="start" value="0.5" name="express_start">
        <?php echo $lang['transport_note_2'];?>
        <input class="w60 text" type="text" maxlength="6" autocomplete="off" value="0" name="express_postage" data-field="postage"><em class="add-on"> 원 </em><?php echo $lang['transport_note_3'];?>
        <input class="w30 mr5 text" type="text" maxlength="4" autocomplete="off" value="0.5" data-field="plus" name="express_plus">
        <?php echo $lang['transport_note_4'];?>
        <input class="w60 text" type="text" maxlength="6" autocomplete="off" value="0" data-field="postageplus" name="express_postageplus"><em class="add-on"> 원 </em></div>
        <div class="J_DefaultMessage"></div>
        <div class="bottom"> <a href="javascript:void(0);" class="J_SubmitPL ncsc-btn ncsc-btn-green"><?php echo $lang['transport_tpl_ok'];?></a> <a href="javascript:void(0);" class="J_Cancel ncsc-btn"><?php echo $lang['transport_tpl_cancel'];?></a> </div>
      </form>
    </div>
  </div>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/transport_ko.js"></script>
<script>
$(function(){
	$('.trans-line').append(TransTpl.replace(/TRANSTYPE/g,'kd'));
	$('.tbl-except').append(RuleHead);
	<?php if (is_array($output['extend'])){?>
	<?php foreach ($output['extend'] as $value){?>

		<?php if ($value['is_default']==1){?>

			var cur_tr = $('.tbl-except').prev();
			$(cur_tr).find('input[data-field="start"]').val('<?php echo $value['snum'];?>');
			$(cur_tr).find('input[data-field="postage"]').val('<?php echo $value['sprice_ko'];?>');
			$(cur_tr).find('input[data-field="plus"]').val('<?php echo $value['xnum'];?>');
			$(cur_tr).find('input[data-field="postageplus"]').val('<?php echo $value['xprice_ko'];?>');

		<?php }else{?>

			StartNum +=1;
			cell = RuleCell.replace(/CurNum/g,StartNum);
			cell = cell.replace(/TRANSTYPE/g,'kd');
			$('.tbl-except').find('table').append(cell);
			$('.tbl-attach').find('.J_ToggleBatch').css('display','').html('일괄관리');

			var cur_tr = $('.tbl-except').find('table').find('tr:last');
			$(cur_tr).find('.area-group>p').html('<?php echo $value['area_name'];?>');
			$(cur_tr).find('input[type="hidden"]').val('<?php echo trim($value['area_id'],',');?>|||<?php echo $value['area_name'];?>');
			$(cur_tr).find('input[data-field="start"]').val('<?php echo $value['snum'];?>');
			$(cur_tr).find('input[data-field="postage"]').val('<?php echo $value['sprice_ko'];?>');
			$(cur_tr).find('input[data-field="plus"]').val('<?php echo $value['xnum'];?>');
			$(cur_tr).find('input[data-field="postageplus"]').val('<?php echo $value['xprice_ko'];?>');

		<?php }?>
	<?php }?>
	<?php }?>
});
</script>