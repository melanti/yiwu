(function($, angular, _, Sha256){
  'use strict';
  
  angular.module('sha256.service')
    .factory('sha256', ['$window', function($window) {
      return $window.Sha256;
    }]);
})(window.jQuery, window.angular, window._, window.Sha256);
