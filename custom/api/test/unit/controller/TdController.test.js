'use strict';

var request = require('supertest'),
should = require('should'),
_ = require('underscore'),
Q = require('q');

var tmpVersions,
lib_net,
TdController,
RequestService,
CommonService,
testListReqModel = {
    account_uuid: '1',
    start: '',
    count: '',
    from: '',
    item_uuid: '',
    td_ids: '',
    td_name: '',
    org_id: '',
    org_name: '',
    cat_id: '',
    td_sex: '',
    td_status: '',
    make_category_db_id: '',
    make_category_db_name: '',
    period_reg_start: '',
    period_reg_end: '',
    show: 'debug'
};

/*if (process.env['MODE'] == 'ALL_DEBUG') {
 //Operation Test
 // startTest();
 }*/
startTest();

function startTest() {
    describe('=== E. 3d cloth 테스트 케이스 ===\n', function () {
        testE0();
        testE1();

    });
}

function testE0() {
    describe('=== E.0 모듈 설정 === \n', function () {
        it('E.0.1 assignModule', function (done) {
            assignBasicModule();
            assignCtrlModule();

            done();
        });
    });
}

function testE1() {
    describe('=== E.1 3D 의상목록 테스트 === \n', function () {

        it('E.1.1 3D 의상목록 호출 : 10개씩 보기', function (done) {
            reqGetTdListByCnt(0, 10, 'E.1.1 3D 의상목록 호출 : 10개씩 보기', done);
        });

        it('E.1.2 3D 의상목록 호출 : 30개씩 보기', function (done) {
            reqGetTdListByCnt(0, 30, 'E.1.2 3D 의상목록 호출 : 30개씩 보기', done);
        });

        it('E.1.3 3D 의상목록 호출 : 50개씩 보기', function (done) {
            reqGetTdListByCnt(0, 50, 'E.1.3 3D 의상목록 호출 : 50개씩 보기', done);
        });

        it('E.1.4 3D 의상목록 호출 : 100개씩 보기', function (done) {
            reqGetTdListByCnt(0, 100, 'E.1.4 3D 의상목록 호출 : 100개씩 보기', done);
        });

        /*it('E.1.5 3D 의상목록 호출 : item_uuid 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                item_uuid: ''
            }), '?'),
            'E.1.5 3D 의상목록 호출 item_uuid 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });*/

        it('E.1.6 3D 의상목록 호출 : td_ids 검색', function (done) {
            reqGetTdList(
                CommonService.getQueryPath(_.extend({}, testListReqModel, {
                    start: 0,
                    count: 10,
                    td_ids: '1,2,3'
                }), '?'),
                'E.1.6 3D 의상목록 호출 : td_ids 검색 ERROR: ',
                function (body) {
                    assertGetTestListCallback(body);
                },
                done
            );

        });

        it('E.1.7 3D 의상목록 호출 : td_name 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                td_name: '3dc'
            }), '?'),
            'E.1.7 3D 의상목록 호출 : td_name 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.8 3D 의상목록 호출 : td_code 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                td_code: 'Wom'
            }), '?'),
            'E.1.8 3D 의상목록 호출 : td_code 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.9 3D 의상목록 호출 : org_id 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                org_id: '1'
            }), '?'),
            'E.1.9 3D 의상목록 호출 : org_id 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.10 3D 의상목록 호출 : cat_id 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                cat_id: '1'
            }), '?'),
            'E.1.10 3D 의상목록 호출 : cat_id 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.11 3D 의상목록 호출 : td_sex 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                td_sex: 'Woman'
            }), '?'),
            'E.1.11 3D 의상목록 호출 : td_sex 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.12 3D 의상목록 호출 : td_status 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                td_status: 'activated'
            }), '?'),
            'E.1.12 3D 의상목록 호출 : td_status 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.13 3D 의상목록 호출 : make_category_db_id 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                make_category_db_id: '42'
            }), '?'),
            'E.1.13 3D 의상목록 호출 : make_category_db_id 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.14 3D 의상목록 호출 : make_category_name 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                make_category_name: 'Wom'
            }), '?'),
            'E.1.14 3D 의상목록 호출 : make_category_name 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.15 3D 의상목록 호출 : period_reg_start 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                period_reg_start: '2016-08-09 02:33:00'
            }), '?'),
            'E.1.15 3D 의상목록 호출 : period_reg_start 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.16 3D 의상목록 호출 : period_reg_end 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                period_reg_end: '2016-08-09 02:34:00'
            }), '?'),
            'E.1.16 3D 의상목록 호출 : period_reg_end 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.17 3D 의상목록 호출 : period_reg_start,end 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                period_reg_start: '2016-08-09 02:33:00',
                period_reg_end: '2016-08-09 02:34:00'
            }), '?'),
            'E.1.17 3D 의상목록 호출 : period_reg_start,end 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });

        it('E.1.18 3D 의상목록 호출 : show=td_fitting 검색', function (done) {
            reqGetTdList(
            CommonService.getQueryPath(_.extend({}, testListReqModel, {
                start: 0,
                count: 10,
                show: 'debug,td_fitting'
            }), '?'),
            'E.1.18 3D 의상목록 호출 : show=td_fitting 검색 ERROR: ',
            function (body) {
                assertGetTestListCallback(body);
            },
            done
            );
        });
    });
}


function assignBasicModule() {
    tmpVersions = require('../../../package.json');
    lib_net = require('../../../api/controllers/libs/network.js');
    RequestService = require('../../util/RequestService.js');
    CommonService = require('../../util/CommonService.js');
}

function assignCtrlModule() {
    TdController = require('../../../api/controllers/TdController.js');
}

function reqGetTdList(reqParams, errLogTitle, assertCallback, done) {
    return CommonService.reqTestAPI({
        r: request,
        url: RequestService.API_URL.GET_TD_LIST + reqParams,
        method: 'get',
        expectStatus: 200,
        sailsSid: CommonService.getTestSid(),
        errLogTitle: errLogTitle,
        assertCallback: assertCallback,
        done: done
    });
}

function reqGetTdListByCnt(st, cnt, errLogStr, done) {
    return reqGetTdList(
    CommonService.getQueryPath(_.extend({}, testListReqModel, {start: (st || 0), count: cnt}), '?'),
    errLogStr + ' ERROR: ',
    function (body) {
        // sails.log.debug(errLogStr, body.results.length);
        assertGetTestListCallback(body);
        if (parseInt(body.info.total, 10) >= cnt) {
            should.equal(parseInt(body.results.length, 10), cnt);
        }
    },
    done
    );
}

function assertGetTestListCallback(body) {
    body.should.have.property('results');
    body.should.have.property('info');
    /*body.info.should.have.property('start');
    body.info.should.have.property('count');
    body.info.should.have.property('total');*/

    sails.log.debug(body);
}

function getUTCDateStr(date, separateStr) {
    var separateStr = separateStr || '-';
    var y = date.getUTCFullYear();
    var m = convertNumToStr(date.getUTCMonth() + 1);
    var d = convertNumToStr(date.getUTCDate());
    var h = convertNumToStr(date.getUTCHours());
    var mi = convertNumToStr(date.getUTCMinutes());
    var s = convertNumToStr(date.getUTCSeconds());
    return y + separateStr + m + separateStr + d + ' ' + h + ':' + mi + ':' + s;

    function convertNumToStr(num) {
        return ( num < 10) ? '0' + num : num;
    }
}


