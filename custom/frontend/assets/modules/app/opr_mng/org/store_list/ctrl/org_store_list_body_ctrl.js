
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.org.list.ctrl.storeAdminListBodyCtrl');
  APP.modules.org.list.ctrl.storeAdminListBodyCtrl = storeAdminListBodyCtrl;

  function storeAdminListBodyCtrl( $scope, _, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL, $sce ){
    var ctrl = this;
    ctrl.imgUrl = APP.info.imgURL;
    ctrl.gVersion = APP.info.gVersion;
    ctrl.getTotal;
    ctrl.getCount;
    ctrl.getStart;
    ctrl.getResults;
    ctrl.renderRegistSection = _renderRegistSection;
    ctrl.modificationStoreAdmin = _modificationStoreAdmin;

    _init();

    function _init(){
      $scope.$on('$destroy', function () {
      });
    }

    function _renderRegistSection(__time){
      return COMMON_UTIL.getLocalTimezoneStr(__time, '.');
    }

    function _modificationStoreAdmin(__data){
      var tmpURL = APP.info.gURL+'/opr_mng/org/store/modification?maxage=0';
      //test
      if( parseInt(APP.info.user.auth.minorOrganization,10)>=2 ){
        tmpURL = APP.info.gURL+'/opr_mng/org/store/modification?maxage=0';
      }else{
        tmpURL = APP.info.gURL+'/opr_mng/org/store/confirmation?maxage=0';
      }

      COMMON_UTIL
        .reqForm(
          tmpURL,
          'post',
          {account_uuid:__data.account_uuid, org_id:__data.org_id}
        );
    }

  }
  storeAdminListBodyCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL', '$sce'];
  storeAdminListBodyCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableBodyCtrl:true});
      APP.modules.directive.util
        .checkBoxHelper(scope, elem, attrs, ctrl, 'tableBodyCtrl.getResults()');
    }
  };
  storeAdminListBodyCtrl.bindToController= {
    getTotal:'&',
    getStart:'&',
    getCount:'&',
    getResults:'&'
  };

})(window.jQuery, window.angular, window._, window.APP);
