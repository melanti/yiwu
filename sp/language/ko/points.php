<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 积分功能公用
 */
$lang['admin_points_unavailable']	 		= '시스텀에서 포인트 기능 미시동';
$lang['admin_points_mod_tip']				= '포인트 수정';
$lang['admin_points_system_desc']			= '관리자 포인트 수동 설정';
$lang['admin_points_userrecord_error']		= '회원정보 오류';
$lang['admin_points_membername']			= '회원아이디';
$lang['admin_points_operatetype']			= '증가/차감유형';
$lang['admin_points_operatetype_add']		= '증가';
$lang['admin_points_operatetype_reduce']	= '차감';
$lang['admin_points_pointsnum']				= '포인트값';
$lang['admin_points_pointsdesc']			= '설명';
$lang['admin_points_pointsdesc_notice']		= '설명정보는 포인트 명세 페이지에 노출되며 회원 및 관리자가 확인할 수 있습니다.';

/**
 * 积分추가
 */
$lang['admin_points_member_error_again']	= '회원정보 오류, 다른 아이디를 입력하세요';
$lang['admin_points_points_null_error']		= '포인트 값을 추가하세요';
$lang['admin_points_points_min_error']		= '포인트 값은 반드시 0보다 커야됩니다.';
$lang['admin_points_points_short_error']	= '포인트 부족, 회원 현재 포인트는';
$lang['admin_points_addmembername_error']	= '회원 아이디를 입력하세요';
$lang['admin_points_member_tip_1']			= '회원';
$lang['admin_points_member_tip_2']			= ', 현재 보유 포인트는';
/**
 * 积分日志
 */
$lang['admin_points_log_title']			= '포인트 명세';
$lang['admin_points_adminname']				= '관리자 이름';
$lang['admin_points_stage']					= '조작단계';
$lang['admin_points_stage_regist']				= '가입';
$lang['admin_points_stage_login']				= '로그인';
$lang['admin_points_stage_comments']				= '상품댓글';
$lang['admin_points_stage_order']				= '주문결제';
$lang['admin_points_stage_system']				= '포인트 관리';
$lang['admin_points_stage_pointorder']		= '선물교환';
$lang['admin_points_stage_app']		= '포인트교환';
$lang['admin_points_addtime']				= '가입시간';
$lang['admin_points_addtime_to']				= '부터';
$lang['admin_points_log_help1']				= '포인트 명세, 회원, 관리자, 조작 포인트수, 조작시간등 정보;';



