(function($, angular, _){

  angular.module('App.items')
    .service('ThreeDTableManager', ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', 'COMMON_UTIL', 'PubSub', ThreeDTableManager]);

  function ThreeDTableManager(_, $q , $timeout, $http, XHR, API_URL_INFO, COMMON_UTIL, PubSub){
    var vm = this;

    var COUNT_RANGE = 5,
        VIEW_RANGE_LIST = ['10','20','30','50','100'];


    /*
     start	목록 시작 위치	정수	선택
     count	목록에 포함할 데이터 갯수	정수	선택
     from	요청 주체 ( 'admin' or NULL ) 검색 방식에 영향을 줌	문자열	선택
     td_name	3D의상 이름으로 검색 ( 전 구간 유사 검색 )	문자열	선택
     td_code	3D의상 코드로 검색 ( 앞 구간 유사 검색 )	문자열	선택
     org_id	제휴처 번호로 검색	문자열	선택
     org_name	제휴처 이름으로 검색	문자열	선택
     cat_id	클라이언트의 상품 번호로 검색	문자열	선택
     td_sex	3D 의상 성별로 검색 ( woman, man )	문자열	선택
     td_status	3D 의상 상태로 검색 ( ready, activated, disabled, deleted )	문자열	선택
     make_category_db_id	WEB에서 DB와 연동 할 때만 사용하는 3D 의상 제작 분류 번호 ( 주의: CAT, Mirror, APP에서 사용하는 제작분류번호와 다름 )	문자열	선택
     make_category_name	제작 분류 이름 ( WomCloTop001 )	문자열	선택
     period_reg_start	3D 의상 등록 기간으로 검색 시, 시작 일시 (YYYY-MM-DD HH:II:SS)	문자열	선택
     period_reg_end	3D 의상 등록 기간으로 검색 시, 마감 일시 (YYYY-MM-DD HH:II:SS)	문자열	선택
     show	데이터 출력 옵션 ( debug ... )
    */

    vm._state = {

      tableData: {

        options:{
          make_category_arr:[],
          make_category_cur_depth:0,

          from:'admin',
          td_name:'',
          td_code:'',
          org_id:'',
          org_name:'',
          cat_id:'',
          td_sex:'',
          td_status:'ready,activated,disabled',
          make_category_db_id:'',
          make_category_name:'',
          period_reg_start:'',
          period_reg_end:'',
          order:'registerd_date',
          order_type:'desc'
        },
        info: {
          start: 0,
          count: '2000',
          total: 0,
          sex: {
            "man": 0,
            "woman": 0
          }
        },
        results: []
      }
    };

    vm.exports = {
      COUNT_RANGE:COUNT_RANGE,
      VIEW_RANGE_LIST:VIEW_RANGE_LIST,
      init:_init,
      getCountRange:function(){return COUNT_RANGE;},
      getViewRangeList:function(){return VIEW_RANGE_LIST;},
      get3dCateList:_get3dCateList,
      getTableData:_getTableData,
      getTableStateInfo:_getTableStateInfo,
      deleteRow:_deleteRow,
      updateTableData:_updateTableData
    };

    function _init(){
    }



    function _updateTableData(__data){
      var deferred = $q.defer();

      var curTimeZone = COMMON_UTIL.getCurTimeZone();

      var tmp_period_reg_start = ( (COMMON_UTIL.isExist(__data.period_reg_start))?(__data.period_reg_start):vm._state.tableData.options.period_reg_start ),
        tmp_period_reg_end = ( (COMMON_UTIL.isExist(__data.period_reg_end))?(__data.period_reg_end):vm._state.tableData.options.period_reg_end );

      tmp_period_reg_start = tmp_period_reg_start.replace(/-/g, "/");
      tmp_period_reg_end = tmp_period_reg_end.replace(/-/g, "/");

      if(tmp_period_reg_start !=''){
        tmp_period_reg_start+=' 00:00:00';
        tmp_period_reg_start = COMMON_UTIL.getcomputedUTCDate(tmp_period_reg_start);
        // tmp_period_reg_start = new Date( new Date(tmp_period_reg_start).getTime()+((curTimeZone*60)*60*1000) ).toString();
      };
      if(tmp_period_reg_end !=''){
        tmp_period_reg_end+=' 23:59:59';
        tmp_period_reg_end = COMMON_UTIL.getcomputedUTCDate(tmp_period_reg_end);
        // tmp_period_reg_end = new Date( new Date(tmp_period_reg_end).getTime()+((curTimeZone*60)*60*1000) ).toString();
      };

      var tmpStart =(COMMON_UTIL.isExist(__data.start))?(__data.start):vm._state.tableData.info.start;
      var tmpCount =(COMMON_UTIL.isExist(__data.count))?(__data.count):vm._state.tableData.info.count;

      var tmpParam=[
        '?start='+( tmpStart ),
        '&count='+( tmpCount ),
        '&from='+( (COMMON_UTIL.isExist(__data.from))?(__data.from):vm._state.tableData.options.from ),
        '&order='+ vm._state.tableData.options.order ,
        '&order_type='+ vm._state.tableData.options.order_type ,
        '&td_name='+( (COMMON_UTIL.isExist(__data.td_name))?encodeURIComponent(__data.td_name):encodeURIComponent(vm._state.tableData.options.td_name) ),
        '&td_code='+( (COMMON_UTIL.isExist(__data.td_code))?encodeURIComponent(__data.td_code):encodeURIComponent(vm._state.tableData.options.td_code) ),
        '&item_uuid='+( (COMMON_UTIL.isExist(__data.item_uuid))?encodeURIComponent(__data.item_uuid):''),
        '&org_id='+( (COMMON_UTIL.isExist(__data.org_id))?(__data.org_id):vm._state.tableData.options.org_id ),
        '&org_name='+( (COMMON_UTIL.isExist(__data.org_name))?encodeURIComponent(__data.org_name):encodeURIComponent(vm._state.tableData.options.org_name) ),
        '&cat_id='+( (COMMON_UTIL.isExist(__data.cat_id))?(__data.cat_id):vm._state.tableData.options.cat_id ),
        '&td_sex='+( (COMMON_UTIL.isExist(__data.td_sex))?(__data.td_sex):vm._state.tableData.options.td_sex ),
        '&td_status='+( (COMMON_UTIL.isExist(__data.td_status))?(__data.td_status):vm._state.tableData.options.td_status ),
        '&make_category_db_id='+( (COMMON_UTIL.isExist(__data.make_category_db_id))?(__data.make_category_db_id):vm._state.tableData.options.make_category_db_id ),
        '&make_category_name='+( (COMMON_UTIL.isExist(__data.make_category_name))?encodeURIComponent(__data.make_category_name):encodeURIComponent(vm._state.tableData.options.make_category_name) ),
        '&period_reg_start='+encodeURIComponent( tmp_period_reg_start ),
        '&period_reg_end='+encodeURIComponent( tmp_period_reg_end ),
        '&show=thumbnail,td,categories,debug,td_fitting',
        '&account_uuid='+(window.APP.info.user.account_uuid)
      ].join('');

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_3DITEMS_LIST+tmpParam
        })
        .then(
          function(__sucData){
            if(__sucData.data.info){
              if( __sucData.data.info.start==null || __sucData.data.info.start=='null' )  {
                __sucData.data.info.start = parseInt(tmpStart);
              }
              __sucData.data.info.count = parseInt(tmpCount);
            }

            _setTableData(__sucData.data);
            deferred.resolve(_getTableData());
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _get3dCateList(__data){
      //GET_3D_CATEGORY_LIST

      var deferred = $q.defer();
      var tmpParam=[
        '?sex='+( (COMMON_UTIL.isExist(__data.sex))?(__data.sex):'all' ),
        '&parent_cate_id='+( (COMMON_UTIL.isExist(__data.parent_cate_id))?(__data.parent_cate_id):'' ),
        '&show=debug'
      ].join('');

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_3D_CATEGORY_LIST+tmpParam
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _getTableData(){
      return vm._state.tableData;
    }

    function _getTableStateInfo(__key){
      return vm._state.tableData.info[__key];
    }

    function _setTableData(__data){
      if(__data){
        vm._state.tableData = _.extend({}, vm._state.tableData, __data);

      }else{
        vm._state.tableData = _.extend({}, vm._state.tableData);
      }
      PubSub.trigger('ItemsSearchOpt:change',vm._state.tableData);
    }

    function _deleteRow(__data){
      var deferred = $q.defer();

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.CHANGE_STATUS,
          data: {ids: __data.items_ids, kind:'3d', status:'deleted', show:'debug,contact'}
        })
        .then(
          function(__sucData){
            PubSub.trigger('TOTAL_ITEMS_COUNT');
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

  }


})(window.jQuery, window.angular, window._);
