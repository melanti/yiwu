var request = require('supertest');
var should = require('should');

var tmpVersions,
  lib_net,
  AccountController,
  RequestService,
  CommonService;

if (process.env['MODE'] == 'ALL_DEBUG') {
  // Operation Test
  startTest();
}
// terminal -> NODE_ENV=local MODE=debug LOG_LEVEL=$1 mocha test/bootstrap.test.js
startTest();

function startTest() {
  describe('=== C. Account 테스트 케이스 ===\n', function () {
    testC0();
    testC1();
  });
}

function testC0() {
  describe('=== A.0 모듈 설정 === \n', function () {
    it('A.0.1 assignModule', function (done) {
      assignBasicModule();
      assignAccountCtrlModule();
      done();
    });
  });
}


function testC1() {
  describe('AccountController', function () {
    describe('verifyCode', function () {
      var _name = 'test003';
      it('1. should return success', function (done) {
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_VERIFI_CODE,
          method: 'post',
          expectStatus: 200,
          sendData: {key: _name, ip_addr: '127.0.0.1', show: 'debug'},
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "C.1.1 verify code success",
          assertCallback: function (res) {
            sails.log.debug(res);
            res.should.property("debug");
            res.should.property("code");
            res.should.property("key", _name);
          },
          done: done
        });
      });
      it('2. should return fail', function (done) {
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_VERIFI_CODE,
          method: 'post',
          expectStatus: 400,
          sendData: {ip_addr: '127.0.0.1', show: 'debug'},
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "C.1.2 verify code success",
          assertCallback: function (res) {
            sails.log.debug(res);
            res.should.property("code", "ER_UNDEFINED_PARAM_ERROR");
          },
          done: done
        });
      });
      it('3. should return fail', function (done) {
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_VERIFI_CODE,
          method: 'post',
          expectStatus: 400,
          sendData: {ip_addr: '127.0.0.1'},
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "C.1.3 verify code success",
          assertCallback: function (res) {
            sails.log.debug(res);
            res.should.property("code", "ER_UNDEFINED_PARAM_ERROR");
          },
          done: done
        });
      });
      it('4. should return fail', function (done) {
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_VERIFI_CODE,
          method: 'post',
          expectStatus: 400,
          sendData: {show: 'debug'},
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "C.1.4 verify code success",
          assertCallback: function (res) {
            sails.log.debug(res);
            res.should.property("code", "ER_UNDEFINED_PARAM_ERROR");
          },
          done: done
        });
      });
      it('5. should return fail', function (done) {
        CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_VERIFI_CODE,
          method: 'post',
          expectStatus: 400,
          sendData: {},
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "C.1.5 verify code success",
          assertCallback: function (res) {
            sails.log.debug(res);
            res.should.property("code", "ER_UNDEFINED_PARAM_ERROR");
          },
          done: done
        });
      });
    });

    describe('login', function () {
      var _name = 'test003';

      it('1. should return success', function (done) {
        verify(_name).then(function (val) {
          CommonService.reqTestAPI({
            r: request,
            url: RequestService.API_URL.CAT_LOGIN,
            method: 'post',
            expectStatus: 200,
            sendData: {
              id: _name,
              ip_addr: '127.0.0.1',
              show: 'debug',
              password: 'qwer1234',
              verify_code: val.code,
              service: 'cat'
            },
            sailsSid: CommonService.getTestSid(),
            errLogTitle: "C.1.2 login success",
            assertCallback: function (res) {
              //sails.log.debug(res);
              res.should.property("debug");
              res.results[0].should.property("account_uuid");
              res.results[0].should.property("client_name");
              res.results[0].should.property("secret");
            },
            done: done
          });
        })
      });

      it('2. should return fail - password blank', function (done) {
        verify(_name).then(function (val) {
          CommonService.reqTestAPI({
            r: request,
            url: RequestService.API_URL.CAT_LOGIN,
            method: 'post',
            expectStatus: 400,
            sendData: {
              id: _name,
              ip_addr: '127.0.0.1',
              show: 'debug',
              password: '',
              verify_code: val.code,
              service: 'cat'
            },
            sailsSid: CommonService.getTestSid(),
            errLogTitle: "C.2.2 login fail",
            assertCallback: function (res) {
              //sails.log.debug(res);
              res.should.property("code", "ER_BAD_BLANK_ERROR");
            },
            done: done
          });
        })
      });

      it('3. should return fail - id none', function (done) {
        verify(_name).then(function (val) {
          CommonService.reqTestAPI({
            r: request,
            url: RequestService.API_URL.CAT_LOGIN,
            method: 'post',
            expectStatus: 400,
            sendData: {
              ip_addr: '127.0.0.1',
              show: 'debug',
              password: '',
              verify_code: val.code,
              service: 'cat'
            },
            sailsSid: CommonService.getTestSid(),
            errLogTitle: "C.2.2 login fail",
            assertCallback: function (res) {
              //sails.log.debug(res);
              res.should.property("code", "ER_UNDEFINED_PARAM_ERROR");
            },
            done: done
          });
        })
      });

      it('4. should return fail - password none', function (done) {
        verify(_name).then(function (val) {
          CommonService.reqTestAPI({
            r: request,
            url: RequestService.API_URL.CAT_LOGIN,
            method: 'post',
            expectStatus: 400,
            sendData: {
              id: _name,
              ip_addr: '127.0.0.1',
              show: 'debug',
              verify_code: val.code,
              service: 'cat'
            },
            sailsSid: CommonService.getTestSid(),
            errLogTitle: "C.2.2 login fail",
            assertCallback: function (res) {
              //sails.log.debug(res);
              res.should.property("code", "ER_UNDEFINED_PARAM_ERROR");
            },
            done: done
          });
        })
      });
      function verify(_name) {
        return CommonService.reqTestAPI({
          r: request,
          url: RequestService.API_URL.GET_VERIFI_CODE,
          method: 'post',
          expectStatus: 200,
          sendData: {key: _name, ip_addr: '127.0.0.1', show: 'debug'},
          sailsSid: CommonService.getTestSid(),
          errLogTitle: "C.2.1 verify code success",
          assertCallback: function (res) {
            //sails.log.debug(res);
            res.should.property("debug");
            res.should.property("code");
            res.should.property("key", _name);
          },
          done: null
        })
      }

    });
  });
}


function assignBasicModule() {
  tmpVersions = require('../../../package.json');
  lib_net = require('../../../api/controllers/libs/network.js');
  RequestService = require('../../util/RequestService.js');
  CommonService = require('../../util/CommonService.js');
}

function assignAccountCtrlModule() {
  AccountController = require('../../../api/controllers/AccountController.js');
}
