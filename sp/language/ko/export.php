<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 불러내기语言包，只有在执行불러내기行为时，才会调用
 */

//브랜드
$lang['exp_brandid']		= '브랜드ID';
$lang['exp_brand']			= '브랜드';
$lang['exp_brand_cate']		= '유형';
$lang['exp_brand_img']		= '브랜드이미지';

//상품
$lang['exp_product']		= '상품';
$lang['exp_pr_cate']		= '카테고리';
$lang['exp_pr_brand']		= '브랜드';
$lang['exp_pr_price']		= '가격';
$lang['exp_pr_serial']		= '고유번호';
$lang['exp_pr_state']		= '상태';
$lang['exp_pr_type']		= '유형';
$lang['exp_pr_addtime']		= '발표시간';
$lang['exp_pr_store']		= '업체';
$lang['exp_pr_storeid']		= '업체ID';
$lang['exp_pr_wgxj']		= '위반내림';
$lang['exp_pr_sj']			= '진열';
$lang['exp_pr_xj']			= '미진열';
$lang['exp_pr_new']			= '새것';
$lang['exp_pr_old']			= '중고';

//유형
$lang['exp_type_name']		= '유형';

//규격
$lang['exp_spec']			= '규격';
$lang['exp_sp_content']		= '규격내용';

//업체
$lang['exp_store']			= '업체';
$lang['exp_st_name']		= '업체아이디';
$lang['exp_st_sarea']		= '거주지';
$lang['exp_st_grade']		= '레벨';
$lang['exp_st_adtime']		= '오픈시간';
$lang['exp_st_yxq']			= '유효기간';
$lang['exp_st_state']		= '상태';
$lang['exp_st_xarea']		= '상세주소';
$lang['exp_st_post']		= '우편번호';
$lang['exp_st_tel']			= '연락처';
$lang['exp_st_kq']			= '시동';
$lang['exp_st_shz']			= '심사중';
$lang['exp_st_close']		= '닫힘';

//会员
$lang['exp_member']			= '회원';
$lang['exp_mb_name']		= '실명';
$lang['exp_mb_jf']			= '포인트';
$lang['exp_mb_yck']			= '적립금';
$lang['exp_mb_jbs']			= '금액수';
$lang['exp_mb_sex']			= '성별';
$lang['exp_mb_ww']			= 'WANGWANG';
$lang['exp_mb_dcs']			= '로그인수';
$lang['exp_mb_rtime']		= '가입시간';
$lang['exp_mb_ltime']		= '마지막 로그인';
$lang['exp_mb_storeid']		= '업체ID';
$lang['exp_mb_nan']			= '남';
$lang['exp_mb_nv']			= '여';

//포인트 명세
$lang['exp_pi_member']		= '회원';
$lang['exp_pi_system']		= '관리자';
$lang['exp_pi_point']		= '포인트값';
$lang['exp_pi_time']		= '발생시간';
$lang['exp_pi_jd']			= '조작단계';
$lang['exp_pi_ms']			= '설명';
$lang['exp_pi_jfmx']		= '포인트 명세';

//적립금충전
$lang['exp_yc_no']			= '충전번호';
$lang['exp_yc_member']		= '회원명';
$lang['exp_yc_money']		= '충전금액';
$lang['exp_yc_pay']			= '결제방식';
$lang['exp_yc_ctime']		= '개설시간';
$lang['exp_yc_ptime']		= '결제시간';
$lang['exp_yc_paystate']	= '결제상태';
$lang['exp_yc_memberid']	= '회원ID';
$lang['exp_yc_yckcz']		= '적립금 충전';

//적립금인출
$lang['exp_tx_no']			= '출금번호';
$lang['exp_tx_member']		= '회원명';
$lang['exp_tx_money']		= '출금금액';
$lang['exp_tx_type']		= '출금방식';
$lang['exp_tx_ctime']		= '신청시간';
$lang['exp_tx_state']		= '출금상태';
$lang['exp_tx_memberid']	= '회원ID';
$lang['exp_tx_title']		= '적립금 출금';

//적립금명세
$lang['exp_mx_member']		= '회원';
$lang['exp_mx_ctime']		= '변경시간';
$lang['exp_mx_money']		= '금액';
$lang['exp_mx_av_money']	= '사용가능금액';
$lang['exp_mx_freeze_money']= '동결금액';
$lang['exp_mx_type']		= '금액유형';
$lang['exp_mx_system']		= '관리자';
$lang['exp_mx_stype']		= '사건유형';
$lang['exp_mx_mshu']		= '설명';
$lang['exp_mx_rz']			= '적립금 변경 일지';

//주문
$lang['exp_od_no']			= '주문번호';
$lang['exp_od_store']		= '업체';
$lang['exp_od_buyer']		= '소비자';
$lang['exp_od_xtimd']		= '주문시간';
$lang['exp_od_count']		= '총 주문금액';
$lang['exp_od_yfei']		= '배송료';
$lang['exp_od_paytype']		= '결제방식';
$lang['exp_od_state']		= '주문상태';
$lang['exp_od_storeid']		= '업체ID';
$lang['exp_od_selerid']		= '판매자ID';
$lang['exp_od_buyerid']		= '소비자ID';
$lang['exp_od_bemail']		= '소비자Email';
$lang['exp_od_sta_qx']		= '취소됨';
$lang['exp_od_sta_dfk']		= '결제대기';
$lang['exp_od_sta_dqr']		= '결제완료, 확인대기';
$lang['exp_od_sta_yfk']		= '결제완료';
$lang['exp_od_sta_yfh']		= '배송완료';
$lang['exp_od_sta_yjs']		= '정산완료';
$lang['exp_od_sta_dsh']		= '심사대기';
$lang['exp_od_sta_yqr']		= '확인완료';
$lang['exp_od_order']		= '주문';

//金币购买기록
$lang['exp_jbg_member']		= '회원명';
$lang['exp_jbg_store']		= '업체';
$lang['exp_jbg_jbs']		= '구매 현금수';
$lang['exp_jbg_money']		= '수요금액';
$lang['exp_jbg_gtime']		= '구매시간';
$lang['exp_jbg_paytype']	= '결제방식';
$lang['exp_jbg_paystate']	= '결제상태';
$lang['exp_jbg_storeid']	= '업체ID';
$lang['exp_jbg_memberid']	= '회원ID';
$lang['exp_jbg_wpay']		= '미지불';
$lang['exp_jbg_ypay']		= '결제완료';
$lang['exp_jbg_jbgm']		= '현금구매';

//金币日志
$lang['exp_jb_member']		= '회원';
$lang['exp_jb_store']		= '업체';
$lang['exp_jb_jbs']			= '현금수';
$lang['exp_jb_type']		= '변경유형';
$lang['exp_jb_btime']		= '변경시간';
$lang['exp_jb_mshu']		= '설명';
$lang['exp_jb_storeid']		= '업체ID';
$lang['exp_jb_memberid']	= '회원ID';
$lang['exp_jb_add']			= '증가';
$lang['exp_jb_del']			= '차감';
$lang['exp_jb_log']			= '현금일지';


?>