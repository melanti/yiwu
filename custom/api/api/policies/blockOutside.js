/**
 * blockOutside
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {
  var lib_net = require("../controllers/libs/network.js"),
    ip_addr = lib_net.get_ip(req, res);
  // User is allowed, proceed to the next policy,
  // or if this is the last policy, the controller
  if (process.env.NODE_ENV === sails.config.env.mode &&
    (req.host === sails.config.env.host || req.host === sails.config.env.host_admin)) {
    // User is not allowed
    // (default res.forbidden() behavior can be overridden in `config/403.js`)

    // checking the access from office network
    //if((process.env.NODE_ENV === "development" || process.env.NODE_ENV === "staging") &&
    //if (process.env.NODE_ENV === "development" && /^112\.221\.22\.[012]?[0-9][0-9]?$/.test(ip_addr)) {
    if (process.env.NODE_ENV === "development" && ip_addr === "219.251.8.108") {
      return next();
    } else if (process.env.NODE_ENV === "local") {
      return next();
    } else {
      return res.forbidden('You are not permitted to perform this action.');
    }
  } else {
    return next();
  }
};
