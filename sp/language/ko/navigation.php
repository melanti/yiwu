<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['navigation_index_del_succ']		= '메뉴 삭제 성공';
$lang['navigation_index_choose_del']	= '삭제할 내용을 선택하세요';
$lang['navigation_index_top']			= '상단';
$lang['navigation_index_center']		= '중앙';
$lang['navigation_index_bottom']		= '하단';
$lang['navigation_index_nav']			= '메뉴';
$lang['navigation_index_title']			= '제목';
$lang['navigation_index_location']		= '위치';
$lang['navigation_index_url']			= '링크';
$lang['navigation_index_open_new']		= '새창';
/**
 * 추가导航
 */
$lang['navigation_add_partner_null']	= '제목을 입력하세요';
$lang['navigation_add_sort_int']		= '카테고리 정렬은 반드시 숫자로 입력하세요';
$lang['navigation_add_url_wrong']		= '정확한 링크주소를 입력하세요';
$lang['navigation_add_goods_class_null']	= '상품 카테고리를 입력하세요';
$lang['navigation_add_article_class_null']	= '문장 카테고리를 입력하세요';
$lang['navigation_add_activity_null']	= '이벤트를 입력하세요';
$lang['navigation_add_back_to_list']	= '돌아가기';
$lang['navigation_add_again']			= '계속추가';
$lang['navigation_add_succ']			= '메뉴 추가 성공';
$lang['navigation_add_fail']			= '메뉴 추가 실패';
$lang['navigation_add_article_class']	= '문장 카테고리';
$lang['navigation_add_goods_class']		= '상품 카테고리';
$lang['navigation_add_custom']			= '사용자 정의 메뉴';
$lang['navigation_add_type']			= '메뉴유형';
$lang['navigation_add_activity']		= '이벤트';
/**
 * 수정导航
 */
$lang['navigation_edit_again']	= '본 메뉴 재수정';
$lang['navigation_edit_succ']	= '메뉴 수정 성공';
$lang['navigation_edit_fail']	= '메뉴 수정 실패';