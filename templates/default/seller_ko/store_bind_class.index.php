<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
<?php if ($notOwnShop = !checkPlatformStore()) { ?>
  <a href="javascript:void(0)" class="ncsc-btn ncsc-btn-green" nc_type="dialog" dialog_title="경영카테고리 추가" dialog_id="my_goods_brand_apply" dialog_width="580" uri="index.php?act=store_info&op=bind_class_add">경영카테고리 추가</a>
<?php } ?>
  </div>

<?php if (checkPlatformStoreBindingAllGoodsClass()) { ?>
<table class="ncsc-default-table">
  <tbody>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><span>모든 카테고리 연동 완료</span></div></td>
    </tr>
  </tbody>
</table>

<?php } else { ?>

<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w20"></th>
      <th colspan="3">판매카테고리</th>
      <th>비율</th>
<?php if ($notOwnShop) { ?>
      <th>상태</th>
      <th>작동</th>
<?php } ?>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($output['bind_list'])) { ?>
    <?php foreach($output['bind_list'] as $val) { ?>
    <tr class="bd-line">
      <td></td>
      <td class="w180 tl"><?php echo $val['class_1_name']; ?></td>
      <td class="w180 tl"><?php echo $val['class_2_name'] ? '>' : null; ?>&emsp;<?php echo $val['class_2_name']; ?></td>
      <td class="w180 tl"><?php echo $val['class_3_name'] ? '>' : null; ?>&emsp;<?php echo $val['class_3_name']; ?></td>
      <td class="w180"><?php echo $val['commis_rate'];?> %</td>
<?php if ($notOwnShop) { ?>
      <td class="w100"><?php echo $val['state'] == '1' ? '심사완료' : '심사중'; ?></td>
      <td class="nscs-table-handle">
      <?php if ($val['state'] == '0') {?>
      <span><a href="javascript:void(0)" class="btn-red" onclick="ajax_get_confirm('정말 삭제 하시겠습니까?', 'index.php?act=store_info&op=bind_class_del&bid=<?php echo $val['bid']; ?>');"><i class="icon-trash"></i><p>삭제</p></a></span>
     <?php } ?>
      </td>
<?php } ?>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php } ?>
  </tbody>
</table>

<?php } ?>
