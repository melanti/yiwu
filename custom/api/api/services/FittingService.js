var fs = require('fs');
var crypto = require('crypto');
var qr = require('qr-image');
var uuid = require('node-uuid');
var Q = require('q');
var rmdir = require('rmdir');
var mkdirp = require('mkdirp');
var probe = require('node-ffprobe');
var SFTPClient = require('ssh2').Client;

var SFTP_INFO = {
    host: sails.config.ftp.host,
    port: sails.config.ftp.port,
    username: sails.config.ftp.username,
    password: sails.config.ftp.password,
    cwd: sails.config.ftp.cwd,
    /*upload_dir:'/DAT',*/
    readyTimeout:10000000
};

module.exports = {
    uploadTemp: uploadTemp,
    removeTemp: removeTemp,
    getFittingFilename: getFittingFilename,
    computeHash: computeHash,
    genQR: genQR,
    s3Upload: s3Upload,
    s3CopyFolder: s3CopyFolder,
    s3Replace: s3Replace,
    s3DeleteFolder: s3DeleteFolder,
    sftpUpload: sftpUpload,
    genSerial: genSerial,
    getSQLForFitvFile: getSQLForFitvFile
};

function uploadTemp(req, videoInfo, file) {
    var deferred = Q.defer();
    var params = req.params.all();
    var ext = {
        video : params.video_ext || 'mp4',
        animation : params.animation_ext || 'bin',
        capture : params.capture_ext || 'jpg',
        background : params.background_ext || 'jpg'
    };

    req.file(file).upload({
        dirname: './' + videoInfo.localDir/* + '/' + getFittingFilename(videoInfo, file)*/,
        maxBytes: 2000000000
    }, function (err, uploadedFiles) {
        if (err || !uploadedFiles || uploadedFiles.length === 0) {
            deferred.reject(err);
        } else {
            var fileName = uploadedFiles[0].filename,
            fd = uploadedFiles[0].fd,
            /*ext = fileName.split('.').pop(),*/
            size = fs.statSync(fd).size;
            /*pow = parseInt(Math.floor(Math.log(bytes) / Math.log(1024))),
             size = (bytes / Math.pow(1024, pow)).toFixed(2);*/

            videoInfo.fileInfo[file] = {
                fd: fd,
                ext: ext[file],
                size: size
            };

            if (!videoInfo.files) {
                videoInfo.files = [];
            }

            videoInfo.files.push(file);

            /* get video duration */
            if (file === "video") {
                probe(fd, function (err, probeData) {
                    if (err) {
                        videoInfo.duration = 20;
                        deferred.resolve();
                    } else {
                        if (probeData && probeData.format && probeData.format.duration && probeData.format.duration !== "N/A") {
                            videoInfo.duration = probeData.format.duration.toFixed(0);
                        } else {
                            videoInfo.duration = 20;
                        }
                        deferred.resolve();
                    }
                });
                //videoInfo.duration = 20;
                //deferred.resolve();
            } else {
                deferred.resolve();
            }
        }
    });

    return deferred.promise;
}

function removeTemp(videoInfo) {
    var deferred = Q.defer();
    rmdir('./.tmp/uploads/' + videoInfo.localDir, function (err, dirs, files) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function getFittingFilename(videoInfo, file) {
    var filename,
    ext;
    if (videoInfo.fileInfo[file] && videoInfo.fileInfo[file].ext) {
        ext = videoInfo.fileInfo[file].ext;
    }

    switch (file) {
        case 'video':
            filename = 'Video.' + ext;
            return filename;
        case 'animation':
            filename = 'Animation.' + ext;
            return filename;
        case 'capture':
            filename = 'Capture.' + ext;
            return filename;
        case 'background':
            filename = 'Background.' + ext;
            return filename;
        case 'tag':
            filename = 'Tag.png';
            return filename;
    }
}

function computeHash(videoInfo, file) {
    var deferred = Q.defer();

    var hash = crypto.createHash("sha256");
    hash.setEncoding('hex');
    var filePath = videoInfo.fileInfo[file].fd;
    var fileReadStream = fs.createReadStream(filePath);

    fileReadStream.on('error', function (err) {
        deferred.reject(err);
    });

    fileReadStream.on('end', function () {
        hash.end();

        videoInfo.fileInfo[file].hash = hash.read();

        deferred.resolve();
    });

    fileReadStream.pipe(hash);
    return deferred.promise;
}

function genQR(videoInfo, file) {

    var deferred = Q.defer();

    var tmpDir = './.tmp/uploads/' + videoInfo.localDir;
    var qrPath = tmpDir + '/' + getFittingFilename(videoInfo, file);
    videoInfo.fileInfo.tag = {
        fd: qrPath
    };

    var qrData = qr.imageSync(videoInfo.videoSerial, {
        type: 'png'
    });

    mkdirp(tmpDir, function (err) {
        if (err) {
            deferred.reject(err);
        }
        fs.writeFile(qrPath, qrData, function (err) {
            deferred.resolve();
        });
    });
    return deferred.promise;
}

function s3MultiUpload(videoInfo) {
    var deferred = Q.defer(),
    files = videoInfo.files,
    promises = [];

    for (var i = 0; i < files.length; i++) {
        var promise = s3Upload(videoInfo, files[i]);
        promises.push(promise);
    }

    Q.all(promises)
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        deferred.reject(err);
    });

    return deferred.promise;
}

function s3Upload(videoInfo, file) {
    var deferred = Q.defer(),
    fileName = getFittingFilename(videoInfo, file),
    s3Dir = videoInfo.disk,
    s3Path = s3Dir + fileName,
    localPath = videoInfo.fileInfo[file].fd;

    videoInfo.S3.upload(localPath, s3Path, function (err, result) {
        if (err) {
            err = new Error('S3 upload failure...');
            deferred.reject(err);
        } else {
            deferred.resolve();
        }
    });

    return deferred.promise;
}



function s3CopyFolder(S3, oldDisk, newDisk) {
    var deferred = Q.defer();

    S3.copyFolder(oldDisk, newDisk, function (err, result) {
       if(err) {
           deferred.reject(err);
       } else {
           deferred.resolve();
       }
    });

    return deferred.promise;
}

function s3Replace(videoInfo) {
    var deferred = Q.defer();

    var files = videoInfo.files;

    Q.fcall(function () {
        return s3DeleteObjects(videoInfo);
    })
    .then(function () {
        return s3MultiUpload(videoInfo);
    })
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        deferred.reject(err);
    });

    return deferred.promise;
}

function s3DeleteObjects(videoInfo) {
    var deferred = Q.defer();

    var files = videoInfo.files;

    var promises = [];
    for (var i = 0; i < files.length; i++) {
        var fileName = getFittingFilename(videoInfo, files[i]);
        var target = videoInfo.disk + fileName;
        var promise = s3Delete(videoInfo.S3, target);
        promises.push(promise);
    }

    Q.all(promises)
    .then(function () {
        deferred.resolve();
    })
    .catch(function (err) {
        deferred.reject(err);
    });

    return deferred.promise;
}

function s3Delete(S3, target) {
    var deferred = Q.defer();

    S3.delete(target, function (err, result) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function s3DeleteFolder(S3, target) {
    var deferred = Q.defer();

    S3.deleteFolder(target, function (err, result) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function sftpUpload(videoInfo) {
    var deferred = Q.defer();


    sails.log.debug('uploadSftp ... ' + videoInfo.guid);


    var tmp_mkdir_path = '/' + SFTP_INFO.cwd + '/' + videoInfo.disk;
    var td_ctx = {
        mkdir_path: undefined,
        connection: undefined,
        sftp: undefined,
        dirData: undefined,
        doRemoveFiles: false,
        doRemoveDir: false
    };

    Q.fcall(_connectSFTPServer)
    .then(_assignConnAndSFTP)
    .then(_openDirectory)
    .then(_checkDirectory)
    .then(_checkRemoveFiles)
    .then(_checkRemoveDir)
    .then(_createDirectory)
    .then(_uploadFiles)
    .spread(_allSetttled)
    .catch(function (e) {
        sails.log.debug('fail::: FileServices.uploadFiles ER_FILE_UPLOAD: ', e);
        FileService.destroyConnectSFTP(td_ctx.connection, td_ctx.sftp);

        deferred.reject({code: 'ER_FILE_UPLOAD', status: 500});
    });

    function _connectSFTPServer(){
        return FileService.connectSFTP(SFTP_INFO, FileService.destroyConnectSFTP);
    }

    function _assignConnAndSFTP(results) {
        //예외처리때 destroy하기위한  connect, sftp instance의 mutable state
        td_ctx.connection = results.connection;
        td_ctx.sftp = results.sftp;
        td_ctx.mkdir_path = tmp_mkdir_path;
        return Q.all([results]);
    }

    function _openDirectory() {
        sails.log.debug('_openDirectory: ');
        return FileService.opendir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path);
    }

    function _checkDirectory(results) {
        sails.log.debug('_checkDirectory: ');
        if (results.hasDir) {
            if (results.dirData !== false && results.dirData.length > 0) {
                sails.log.debug('dir exist && file exist:: file remove, dir remove');

                try {
                    td_ctx.dirData = results.dirData;
                    td_ctx.doRemoveFiles = true;
                    td_ctx.doRemoveDir = true;

                } catch (e) {
                    sails.log.debug('WHAT???: ', e);
                    deferred.reject({code: 'ER_FILE_UPLOAD', status: 500});
                }

            } else {
                sails.log.debug('dir exist && file empty:: only dir remove');
                td_ctx.doRemoveFiles = false;
                td_ctx.doRemoveDir = true;
            }

        } else {
            sails.log.debug('dir empty && file empty:: ALL create');
            td_ctx.doRemoveFiles = false;
            td_ctx.doRemoveDir = false;
        }

        return td_ctx;
    }


    function _checkRemoveFiles() {
        // sails.log.debug('_checkRemoveFiles: ');
        if (td_ctx.doRemoveFiles) {
            FileService.unlinkFiles(
            td_ctx.connection,
            td_ctx.sftp,
            _.map(td_ctx.dirData, function (v, k) {
                return td_ctx.mkdir_path + '/' + v.filename;
            }))
            .then(function () {
                return Q.all([td_ctx]);
            })
            .catch(function (er) {
                sails.log.debug('unlinkFiles: catch error ', er);
                throw new Error('unlinkFiles catch error');
            });

        } else {
            return Q.all([td_ctx]);
        }
    }

    function _checkRemoveDir() {
        // sails.log.debug('_checkRemoveDir: ');
        if (td_ctx.doRemoveDir) {
            FileService.removedir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path)
            .then(function () {
                return Q.all([td_ctx]);
            })
            .catch(function (er) {
                sails.log.debug('unlinkFiles: catch error ', er);
                throw new Error('unlinkFiles catch error');
            });

        } else {
            return Q.all([td_ctx]);
        }
    }

    function _createDirectory() {
        // sails.log.debug('_createDirectory: ');
        // sails.log.debug('td_ctx.connection: ' , (typeof td_ctx.connection));
        // sails.log.debug('td_ctx.sftp: ' , (typeof td_ctx.sftp));
        // sails.log.debug('td_ctx.mkdir_path: ' , (typeof td_ctx.mkdir_path));
        return FileService.createDir(td_ctx.connection, td_ctx.sftp, td_ctx.mkdir_path);
    }

    function _uploadFiles() {
        var uploadFilePromises = [];
        var fileName;
        var localPath;
        videoInfo.files.push('tag');
        for(var i=0, iTotal = videoInfo.files.length; i<iTotal; ++i){
            fileName = getFittingFilename(videoInfo, videoInfo.files[i]);
            localPath = videoInfo.fileInfo[videoInfo.files[i]].fd;
            uploadFilePromises.push(FileService.createFileStream(
                td_ctx.connection,
                td_ctx.sftp,
                localPath,
                td_ctx.mkdir_path + '/' + fileName
            ));
        }
        return Q.all(uploadFilePromises);
    }

    function _allSetttled() {
        sails.log.debug('uploadFiles _allSettled: ');
        FileService.destroyConnectSFTP(td_ctx.connection, td_ctx.sftp);
        td_ctx = null;
        deferred.resolve();
    }


    return deferred.promise;
}

function genSerial() {
    /* 랜덤 string에서 가독성 떨어지는 문자열 제외함
     - 숫자 : 0, 1
     - 문자 : I, L, O
     */
    var chars = "23456789ABCDEFGHGKMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
    stringLength = 8,
    serial = '';

    for (var i = 0; i < stringLength; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        serial += chars.substring(rnum, rnum + 1);
    }

    return serial;
}

function getSQLForFitvFile(videoInfo, type) {


    var files = videoInfo.files;
    var qry;
    if (type === 'update') {
        qry = "replace into sv_fitv_file (fitv_no, fitv_file_category_no, fitv_file_extend, fitv_file_size, fitv_file_hash) values ";
    } else {
        qry = "insert into sv_fitv_file (fitv_no, fitv_file_category_no, fitv_file_extend, fitv_file_size, fitv_file_hash) values";
    }

    for (var i = 0; i < files.length; i++) {
        if (files[i] !== 'tag') {
            qry += "(" + videoInfo.fitvNo + ", (select fitv_file_category_no from sv_fitv_file_category where fitv_file_category_name='" + files[i] + "'),"
            + "'" + videoInfo.fileInfo[files[i]].ext + "','" + videoInfo.fileInfo[files[i]].size + "', '" + videoInfo.fileInfo[files[i]].hash + "')";
            if ((i !== files.length - 1) && files[i + 1] && (files[i + 1] !== 'tag')) {
                qry += ',';
            }
        }
    }

    return qry;
}


