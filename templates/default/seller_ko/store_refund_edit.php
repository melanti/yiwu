<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncsc-flow-layout">
  <div class="ncsc-flow-container">
    <div class="title">
      <h3>환불서비스</h3>
    </div>
    <div id="saleRefund">
      <div class="ncsc-flow-step">
        <dl class="step-first current">
          <dt>구매자 환불 신청</dt>
          <dd class="bg"></dd>
        </dl>
        <dl class="<?php echo $output['refund']['seller_time'] > 0 ? 'current':'';?>">
          <dt>판매자 환불 신청 처리</dt>
          <dd class="bg"> </dd>
        </dl>
        <dl class="<?php echo $output['refund']['admin_time'] > 0 ? 'current':'';?>">
          <dt>심사중, 환불완료</dt>
          <dd class="bg"> </dd>
        </dl>
      </div>
    </div>
    <div class="ncsc-form-default">
      <h3>구매자 환불 신청</h3>
      <dl>
        <dt>환불번호：</dt>
        <dd><?php echo $output['refund']['refund_sn']; ?></dd>
      </dl>
      <dl>
        <dt>신청인(구매자)：</dt>
        <dd><?php echo $output['refund']['buyer_name']; ?></dd>
      </dl>
      <dl>
        <dt>환불원인<?php echo $lang['nc_colon'];?></dt>
        <dd> <?php echo $output['refund']['reason_info_ko']; ?> </dd>
      </dl>
      <dl>
        <dt>환불금액<?php echo $lang['nc_colon'];?></dt>
        <dd><strong class="red"><?php echo number_format($output['refund']['refund_amount_ko']); ?>원</strong></dd>
      </dl>
      <dl>
        <dt>환불설명：</dt>
        <dd> <?php echo $output['refund']['buyer_message']; ?> </dd>
      </dl>
      <dl>
        <dt>증빙서류 업로드：</dt>
        <dd>
          <?php if (is_array($output['pic_list']) && !empty($output['pic_list'])) { ?>
          <ul class="ncsc-evidence-pic">
            <?php foreach ($output['pic_list'] as $key => $val) { ?>
            <?php if(!empty($val)){ ?>
            <li><a href="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>" cnbiztype="nyroModal" rel="gal" target="_blank"> <img class="show_image" src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>"></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
        </dd>
      </dl>
      <form id="post_form" method="post" action="index.php?act=store_refund&op=edit&refund_id=<?php echo $output['refund']['refund_id']; ?>">
        <input type="hidden" name="form_submit" value="ok" />
        <h3>판매자 처리 의견</h3>
        <dl>
          <dt><i class="required">*</i>처리여부<?php echo $lang['nc_colon'];?></dt>
          <dd>
            <label class="mr20">
              <input type="radio" class="radio vm" name="seller_state" value="2" />
              동의</label>
            <label>
              <input type="radio" class="radio vm" name="seller_state" value="3" />
              거절</label>
              <span class="error"></span>
          </dd>
        </dl>
        <dl>
          <dt><i class="required">*</i>비고<?php echo $lang['nc_colon'];?></dt>
          <dd>
            <textarea name="seller_message" rows="2" class="textarea w300"></textarea>
            <span class="error"></span>
            <p class="hint">한 번만 제출하실 수 있습니다. 신중히 선택해 주세요. <br>
              동의하시면 관리자 확인 후 , 해당금액을 예치금 형식으로 구매자에게 환불 합니다. <br>
              동의하지 않으시면 구매자는 쇼핑몰 관리자 문의 혹은 다시 신청을 할 수 있습니다. </p>
          </dd>
        </dl>
        <div class="bottom">
          <label class="submit-border">
            <a class="submit" id="confirm_button">확인</a>
          </label>
          <label class="submit-border">
            <a href="javascript:history.go(-1);" class="submit"><i class="icon-reply"></i>돌아가기</a>
          </label>
        </div>
      </form>
    </div>
  </div>
  <?php require template('seller/store_refund_right');?>
</div>
<script type="text/javascript">
$(function(){
    $("#confirm_button").click(function(){
        $("#post_form").submit();
    });
    $('#post_form').validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parentsUntil('dl').find('span.error'));
        },
		submitHandler:function(form){
			ajaxpost('post_form', '', '', 'onerror')
		},
        rules : {
            seller_state : {
                required   : true
            },
            seller_message : {
                required   : true
            }
        },
        messages : {
            seller_state  : {
                required  : '<i class="icon-exclamation-sign"></i><?php echo $lang['refund_seller_confirm_null'];?>'
            },
            seller_message  : {
                required   : '<i class="icon-exclamation-sign"></i><?php echo $lang['refund_message_null'];?>'
            }
        }
    });
});
</script>
