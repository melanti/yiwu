<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if($item_edit_flag) { ?>
<table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12" class="nobg"> <div class="title nomargin">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span> </div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>마우스 롤 오버시 수정 버튼으로 해당 내용을 수정;</li>
            <li>모든 내용 변경후 저장 버튼을 눌러 적용;</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <?php } ?>
<div class="index_block home1">
      <?php if($item_edit_flag) { ?>
  <h3>모형모듈A</h3>
  <?php } ?>
  <div class="title">
    <?php if($item_edit_flag) { ?>
    <h5>제목：</h5>
    <input id="home1_title" type="text" class="txt w200" name="item_data[title]" value="<?php echo $item_data['title'];?>">
    <?php } else { ?>
    <span><?php echo $item_data['title'];?></span>
    <?php } ?>
  </div>
  <div cbtype="item_content" class="content">
      <?php if($item_edit_flag) { ?>
    <h5>내용：</h5>
    <?php } ?>
    <div cbtype="item_image" class="item"> <img cbtype="image" src="<?php echo getMbSpecialImageUrl($item_data['image']);?>" alt="">
      <?php if($item_edit_flag) { ?>
      <input cbtype="image_name" name="item_data[image]" type="hidden" value="<?php echo $item_data['image'];?>">
      <input cbtype="image_type" name="item_data[type]" type="hidden" value="<?php echo $item_data['type'];?>">
      <input cbtype="image_data" name="item_data[data]" type="hidden" value="<?php echo $item_data['data'];?>">
      <a cbtype="btn_edit_item_image" data-desc="640*260" href="javascript:;"><i class="icon-edit"></i>수정</a>
      <?php } ?>
    </div>
  </div>
</div>
