<?php
/**
 * 货到付款지역설정
 *
 * by www.cnbiz.co.kr 开发调试*/

defined('InCNBIZ') or exit('Access Invalid!');

class offpay_areaControl extends SystemControl {
	public function __construct(){
		parent::__construct();
	}

	public function indexOp() {
	    $model_parea = Model('offpay_area');
	    $model_area = Model('area');
	    if (!defined('DEFAULT_PLATFORM_STORE_ID')) {
	        showMessage('자영업을 설정후 받은후 지불 기능을 시동하세요.','index.php?act=dashboard&op=aboutus','html','error',1,5000);
	    }
	    $store_id = DEFAULT_PLATFORM_STORE_ID;
	    if (chksubmit()) {
	        if (!preg_match('/^[\d,]+$/',$_POST['county'])) {
	            $_POST['county'] = '';
	        }
	        //内置자영업店ID
	        $area_info = $model_parea->getAreaInfo(array('store_id'=>$store_id));
            $data = array();
            $county = trim($_POST['county'],',');
            $data['area_id'] = serialize(explode(',',$county));
	        if (!$area_info) {
	            $data['store_id'] = $store_id;
	            $result = $model_parea->addArea($data);
	        } else {
	            $result = $model_parea->updateArea(array('store_id'=>$store_id),$data);
	        }
	        if ($result) {
	            showMessage('저장성공');
	        } else {
	            showMessage('저장실패','','html','error');
	        }
	    }
	    //取出支持货到付款的县ID及上级市ID
        $parea_info = $model_parea->getAreaInfo(array('store_id'=>$store_id));
        if (!empty($parea_info['area_id'])) {
            $parea_ids = @unserialize($parea_info['area_id']);
        }
        if (empty($parea_ids)) {
            $parea_ids = array();
        }
        //取出支持货到付款县ID的上级市ID
        $city_checked_child_array = array();
        $county_array = $model_area->getAreaList(array('area_deep'=>3,'area_id'=>array('in',$parea_ids)),'area_id,area_parent_id');
        foreach ($county_array as $v) {
            if (in_array($v['area_id'],$parea_ids)) {
                $city_checked_child_array[$v['area_parent_id']][] = $v['area_id'];
            }
        }
        Tpl::output('city_checked_child_array',$city_checked_child_array);
        //市级下面的县예不예전체支持货到付款，如果전체支持，默认选中，如果其中部分县支持货到付款，默认不选中但노출一个支付到付县的수량

        //格式 city_id => 下面支持到付的县ID수량
        $city_count_array = array();
        //格式 city_id => 예否选中true/false
        $city_checked_array = array();
        $list = $model_area->getAreaList(array('area_deep'=>3),'area_parent_id,count(area_id) as child_count','area_parent_id');
        foreach ($list as $k => $v) {
            $city_count_array[$v['area_parent_id']] = $v['child_count'];
        }
        foreach ($city_checked_child_array as $city_id => $city_child) {
            if (count($city_child) > 0) {
                if (count($city_child) == $city_count_array[$city_id]) {
                    $city_checked_array[$city_id] = true;
                }
            }
        }
        Tpl::output('city_checked_array',$city_checked_array);

        //取得省级지역及直属子지역(循环输出)
        require(BASE_DATA_PATH.'/area/area.php');
        foreach ($area_array as $k => $v) {
        	if ($v['area_parent_id'] != '0') {
        	    $area_array[$v['area_parent_id']]['child'][$k] = $v['area_name'];
        	    unset($area_array[$k]);
        	}
        }
        Tpl::output('province_array',$area_array);

        //计算哪些省需要默认选中(即该省下面的所有县都支持到付，即所有市都예选中상태)
        $province_array = $area_array;
        foreach ($province_array as $pid => $value) {
        	if (is_array($value['child'])) {
        	    foreach ($value['child'] as $k => $v) {
        	    	if (!array_key_exists($k, $city_checked_array)) {
        	    	    unset($province_array[$pid]);
        	    	    break;
        	    	}
        	    }
        	}
        }
        Tpl::output('province_checked_array',$province_array);

	    Tpl::showpage('offpay_area.index');
	}
}
