<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['stat_newmember']				= '신규회원';
$lang['stat_memberanalyze']			= '회원분석';
$lang['stat_scaleanalyze']			= '회원규모분석';
$lang['stat_memberlist']			= '회원세부사항';
$lang['stat_areaanalyze']			= '지역분석';
$lang['stat_buying']			    = '구매분석';

$lang['stat_newstore']				= '신규미니샵';
$lang['stat_storesale']			    = '미니샵판매량';
$lang['stat_storedegree']			= '미니샵등급';
$lang['stat_storelist']			    = '미니샵설명';

$lang['stat_goods_ranking']			= '상품통계순위';
$lang['stat_sale_income']			= '판매수입통계';
$lang['stat_predeposit']			= '예치금통계';
$lang['stat_goods_sale']			= '상품판매정보';
$lang['stat_class_sale']			= '카테고리 판매통계';

$lang['stat_sale_income']			= '판매수입통계';
$lang['stat_predeposit']			= '적립금통계';
$lang['stat_goods_sale']			= '상품판매명세';

$lang['stat_promotion']			    = '프로모션통계';
$lang['stat_group']			        = '공동구매통계';

$lang['stat_refund']			    = '환불통계';
$lang['stat_evalstore']			    = '업체동태댓글';

$lang['stat_sale']			        = '주문통계';
$lang['stat_storehotrank']			= '인기판매랭킹';
$lang['stat_storesales']			= '판매통계';
$lang['stat_storearea']			    = '지역분석';

$lang['stat_goods_pricerange']	    = '가격판매량';
$lang['stat_hotgoods']	            = '인기판매상품';

$lang['stat_industryscale']	        = '업계규모';
$lang['stat_industryrank']	        = '업계랭킹';
$lang['stat_industryprice']	        = '가격분석';
$lang['stat_industrygeneral']	    = '기본정보';
$lang['stat_generalindex']	        = '기본통계';
$lang['stat_pricerange_setting']    = '가격범위설정';
$lang['stat_setting']               = '통계설정';
$lang['stat_goodspricerange']       = '상품가격범위';
$lang['stat_orderpricerange']       = '주문금액범위';

$lang['stat_validorder_explain']	= "1. 아래 조건 중 한 항목이라도 부합하면 유효한 주문 입니다. <br>(1) 온라인 지불 방식을 통해 지물 및 결제완료한 주문 (2) 货到付款(상품을 받고 지불하는 방식) 방식으로 결제 완료 후 구매결정이 완료 된 주문";