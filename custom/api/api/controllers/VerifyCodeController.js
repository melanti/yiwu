/**
 * VerifyCodeController
 *
 * @description :: Server-side logic for managing verifycodes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  updateVerifyCode: function(req, res) {
    var debug = require("./libs/debug.js"),
      crypto = require('crypto'),
      lib_net = require("./libs/network.js"),
      chk_data = require("./libs/chkData.js"),
      random_uuid = require("./libs/uuid.js").setUUID(),
      key = req.param('key'),
      ip_addr = req.param('ip_addr'),
      status = 501;

    chk_data.chkParam(req, res, [key], function() {
      if (typeof ip_addr === "undefined") {
        ip_addr = lib_net.get_ip(req, res);
      }
      sails.log(ip_addr);
      var vcode_hash = crypto.createHash('sha256').update(random_uuid).digest('base64').replace(/\W/g, ''),
        vqry = "";
      vqry = "insert into sv_verify (verify_hash, verify_code, regist_ip, regist_date) " +
        "values(?, ?, INET_ATON('" + ip_addr + "'), now())";

      DB.query(vqry, [vcode_hash, key], function(err, results) {
        if (err) {
          //console.log(err);
          status = 500;
          return res.send(status, debug.wrap(req, res, status, err));
        } else {
          var rtn = {
            'key': key,
            'code': vcode_hash
          };
          status = 200;
          return res.send(status, debug.wrap(req, res, status, rtn));
        }
      });
    });
  }, // updateVerifyCode


  checkExistVerifyCode: function(req, res) {
    //get input
    var debug = require("./libs/debug.js"),
      chk_data = require("./libs/chkData.js"),
      key = req.param('key'),
      verify_code = req.param('verify_code'),
      err = {
        code: null,
        index: null
      },
      rtn = {
        key: null
      },
      status = 501;

    chk_data.chkParam(req, res, [key, verify_code], function() {
      // execute query
      var qry = "select count(*) as cnt from sv_verify where verify_hash=? and verify_no=?";
      DB.query(qry, [verify_code, key], function(err, results) {
        if (err) {
          status = 500;
          return res.send(status, debug.wrap(req, res, status, err));
        } else {
          if (parseInt(results[0].cnt, 10) > 0) {
            rtn.key = key;
            status = 200;
            return res.send(status, debug.wrap(req, res, status, rtn));
          } else {
            err.code = "ER_NOT_FOUND_ERROR";
            err.index = 0;
            status = 400;
            return res.send(status, debug.wrap(req, res, status, err));
          }
        }
      });
    });
  }
};
