<?php
/**
 * 공동구매관리
 *
 *
 *
 *
 * by www.cnbiz.co.kr 开发调试*/

defined('InCNBIZ') or exit('Access Invalid!');
class live_groupbuyControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('live');
	}

	public function indexOp(){
		$this->live_groupbuyOp();
	}

	/*
	 * 공동구매리스트
	 */
	public function live_groupbuyOp(){
		$condition	=	array();
		if(isset($_POST) && !empty($_POST)){
			//공동구매상태
			if(intval($_POST['groupbuy_state']) == 1){
				$condition['start_time']	=	array('gt',time());
			}elseif(intval($_POST['groupbuy_state']) == 2){
				$condition['start_time']	=	array('lt',time());
				$condition['end_time']	=	array('gt',time());
			}elseif(intval($_POST['groupbuy_state']) == 3){
				$condition['end_time']	=	array('lt',time());
			}

			//심사상태
			if(isset($_POST['audit']) && !empty($_POST['audit'])){
				$condition['is_audit']	=	intval($_POST['audit']);
			}

			Tpl::output('groupbuy_state',intval($_POST['groupbuy_state']));
			Tpl::output('is_audit',intval($_POST['audit']));
		}


		$model_live_groupbuy = Model('live_groupbuy');
		$list = $model_live_groupbuy->getList($condition);

		Tpl::output('list',$list);
		Tpl::output('show_page',$model_live_groupbuy->showpage());
		Tpl::showpage('livegroupbuy.list');
	}

	/*
	 * 삭제공동구매
	 */
	public function del_groupbuyOp(){
		if(isset($_POST)&&!empty($_POST)){
			$condition = array();
			$condition['groupbuy_id'] = array('in',$_POST['groupbuy_id']);

			$model_live_groupbuy = Model('live_groupbuy');
			$res = $model_live_groupbuy->del($condition);

			if($res){
				showMessage('공동구매 삭제 성공','index.php?act=live_groupbuy','','succ');
			}else{
				showMessage('공동구매 삭제 실패','index.php?act=live_groupbuy','','error');
			}
		}
	}

	/*
	 * 심사
	 */
	public function auditOp(){
		$model_live_groupbuy = Model('live_groupbuy');
		$res = $model_live_groupbuy->edit(array('groupbuy_id'=>intval($_GET['groupbuy_id'])),array('is_audit'=>intval($_GET['is_audit'])));

		if($res){
			$this->log('공동구매 심사 성공[ID:'.intval($_GET['groupbuy_id']).']',1);
			showMessage('심사성공','index.php?act=live_groupbuy','','succ');
		}else{
			showMessage('심사실패','index.php?act=live_groupbuy','','error');
		}
	}

	/*
	 * 취소공동구매
	 */
	public function cancelOp(){
		$model_live_groupbuy = Model('live_groupbuy');
		$res = $model_live_groupbuy->edit(array('groupbuy_id'=>intval($_GET['groupbuy_id'])),array('is_open'=>2));//취소공동구매

		if($res){
			$this->log('공동구매 취소[ID:'.intval($_GET['groupbuy_id']).']',1);
			showMessage('조작성공','index.php?act=live_groupbuy','','succ');
		}else{
			showMessage('조작실패','index.php?act=live_groupbuy','','error');
		}

	}

	/*
	 * 추천관리
	 */
	public function ajaxOp(){
		$model_live_groupbuy = Model('live_groupbuy');
		$res = $model_live_groupbuy->edit(array('groupbuy_id'=>intval($_GET['id'])),array('is_hot'=>intval($_GET['value'])));

		if($res){
			echo 'true';exit;
		}else{
			echo 'false';exit;
		}
	}


	/*
	 * 보기공동구매券
	 */
	public function groupbuyvoucherOp(){
		$groupbuy_id	= intval($_GET['groupbuy_id']);
		$model 		= Model();

		//공동구매
		$groupbuy = $model->table('live_groupbuy')->where(array('groupbuy_id'=>$groupbuy_id))->find();
		if(empty($groupbuy)){
			showMessage('공동구매가 존재하지 않습니다.','index.php?act=live_groupbuy','','error');
		}

		$condition					=	array();//검색条件
		$condition['live_order.item_id']	=	$groupbuy_id;

		//获得数据
		$field  = 'live_order.order_sn,live_order.store_name,live_order.item_name,live_order.member_name,live_order_pwd.state,live_order_pwd.use_time,live_order_pwd.order_pwd';
		$on		= 'live_order_pwd.order_id = live_order.order_id';
		$model->table('live_order_pwd,live_order')->field($field);
		$list = $model->join('left')->on($on)->where($condition)->page(20)->order('order_item_id desc')->select();
		Tpl::output('list',$list);

		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('livevoucher.list');
	}
}
