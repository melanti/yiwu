<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['bill_manage'];?>정산관리</h3>
		<ul class="tab-base">
		<li><a href="index.php?act=bill"><span>정산관리</span></a></li>
		<li><a href="index.php?act=bill&op=show_statis&os_month=<?php echo $output['bill_info']['os_month'];?>"><span><?php echo $output['bill_info']['os_month'];?>기  업체 명세서</span></a></li>
		<li><a class="current" href="JavaScript:void(0);"><span>계산서명세</span></a></li>
		</ul>
    </div>
  </div>
<div class="fixed-empty"></div>  
    <table class="table tb-type2 noborder search">
      <tbody>
      <tr><td>업체 - <?php echo $output['bill_info']['ob_store_name'];?>（ID：<?php echo $output['bill_info']['ob_store_id'];?>） <?php echo $output['bill_info']['os_month'];?> 기 정산내역&emsp;
      <?php if ($output['bill_info']['ob_state'] == BILL_STATE_STORE_COFIRM){?>
      <a class="btns" onclick="if (confirm('심사후 취소할 수 없습니다, 정말 심사하시겠습니까??')){return true;}else{return false;}" href="index.php?act=bill&op=bill_check&ob_no=<?php echo $_GET['ob_no'];?>"><span><?php echo $lang['nc_exdport'];?>심사</span></a>
       <?php }elseif ($output['bill_info']['ob_state'] == BILL_STATE_SYSTEM_CHECK){?>
		<a class="btns" href="index.php?act=bill&op=bill_pay&ob_no=<?php echo $_GET['ob_no'];?>"><span><?php echo $lang['nc_exdport'];?>결제완료</span></a>
       <?php }elseif ($output['bill_info']['ob_state'] == BILL_STATE_SUCCESS){?>
      <a class="btns" target="_blank" href="index.php?act=bill&op=bill_print&ob_no=<?php echo $_GET['ob_no'];?>"><span><?php echo $lang['nc_exposrt'];?>프린트</span></a>
      <?php }?>
      </td></tr>
        <tr><td><?php echo $lang['order_time_from'];?>정산번호<?php echo $lang['nc_colon'];?><?php echo $output['bill_info']['ob_no'];?></td></tr>
      	<tr><td>시간범위<?php echo $lang['nc_colon'];?><?php echo date('Y-m-d',$output['bill_info']['ob_start_date']);?> &nbsp;부터&nbsp; <?php echo date('Y-m-d',$output['bill_info']['ob_end_date']);?>
      	</td></tr><tr>
      <td>발급날자<?php echo $lang['nc_colon'];?><?php echo date('Y-m-d',$output['bill_info']['ob_create_date']);?></td></tr><tr>
      <td>플랫폼 결제 금액：<?php echo number_format($output['bill_info']['ob_result_totals_ko']);?> = <?php echo number_format($output['bill_info']['ob_order_totals_ko']);?> (주문금액) - <?php echo number_format($output['bill_info']['ob_commis_totals_ko']);?> (수수료금액) - <?php echo number_format($output['bill_info']['ob_order_return_totals_ko']);?> (환불금액) + <?php echo number_format($output['bill_info']['ob_commis_return_totals_ko']);?> (환불수수료) - <?php echo number_format($output['bill_info']['ob_store_cost_totals_ko']);?> (업체 프로모션 비용)</td>
      </tr>
      <tr><td>정산상태：<?php echo billStateKo($output['bill_info']['ob_state']);?>
      <?php if ($output['bill_info']['ob_state'] == BILL_STATE_SUCCESS){?>
      	&emsp;정산날자<?php echo $lang['nc_colon'];?><?php echo date('Y-m-d',$output['bill_info']['ob_pay_date']);?>，정산메모<?php echo $lang['nc_colon'];?><?php echo $output['bill_info']['ob_pay_content'];?>
      <?php }?>
      </td></tr>
      </tbody>
    </table>
   <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space">
        <th colspan="12"></th>
      </tr>
    </tbody>
  </table>
<?php include template($output['tpl_name']);?>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript">
$(function(){
    $('#query_start_date').datepicker({dateFormat:'yy-mm-dd',minDate: "<?php echo date('Y-m-d',$output['bill_info']['ob_start_date']);?>",maxDate: "<?php echo date('Y-m-d',$output['bill_info']['ob_end_date']);?>"});
    $('#query_end_date').datepicker({dateFormat:'yy-mm-dd',minDate: "<?php echo date('Y-m-d',$output['bill_info']['ob_start_date']);?>",maxDate: "<?php echo date('Y-m-d',$output['bill_info']['ob_end_date']);?>"});
    $('#ncsubmit').click(function(){
    	$('input[name="op"]').val('show_bill');$('#formSearch').submit();
    });
});
</script> 
