
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.org.ctrl.orgAdminMainCtrl');
  APP.modules.org.ctrl.orgAdminMainCtrl = orgAdminMainCtrl;

  //orgAdminMainCtrl
  function orgAdminMainCtrl($scope, $location, _, $timeout, XHR, PubSub, OrgAdminManager, API_URL_INFO, COMMON_UTIL, Validator, PopupService){
    var vm = this,
        orgAdminManager = OrgAdminManager.exports,
        listURL = APP.info.gURL+'/opr_mng/org/admin/list';

    vm.pageType;  //registration(등록) , modification(수정) , confirmation(보기)
    vm.authType;  //admin, store
    vm.accountUuid;
    vm.orgId;
    vm.contact_uuid;

    vm._state={
      org_certification:'',
      org_name:'',
      service_type:0,
      status:0,
      contact_name:'',
      contact_phone:'',
      contact_mail:'',
      contact_homepage:'',
      language:null,
      currency:null,
      utc:null,
      utc_gap:null,
      show:'debug'
    };

    vm._options = {
      languageList:[],
      currencyList:[],
      utcList:null,
      utcFilteredList:null,
    };

    vm.exports={
      imgUrl:APP.info.imgURL,
      gVersion:APP.info.gVersion,
      getUTCStr:_getUTCStr,
      onChangeUTC:_onChangeUTC,
      getMajorOrganization:_getMajorOrganization,
      setMajorOrganization:_setMajorOrganization,
      updateMajorOrganization:_updateMajorOrganization,
      goListPage:_goListPage
    };

    _init();

    function _init(){
      console.log('orgAdminMainCtrl: init' );
      console.log('vm.pageType: ' ,vm.pageType);
      console.log('vm.authType: ' ,vm.authType);
      console.log('vm.accountUuid: ' ,vm.accountUuid);
      console.log('vm.orgId: ' ,vm.orgId);


      switch(vm.pageType){
        case 'registration':
          _initPage();
          break;

        case 'modification':
          _initPage();
          _getMajorOrganization();
          break;

        case 'confirmation':
          _getMajorOrganization();
          break;
      }

      function _initPage(){
        orgAdminManager.init();
        vm._state = orgAdminManager.getState();

        orgAdminManager.getUTC()
          .then(
            function(__sucData){
              console.log('getUTC __sucData: ' , __sucData);
              vm._options.utcList = __sucData.data.results;
              vm._options.utcFilteredList = _.each(__sucData.data.results.concat(), function(v, k, l){
                // v.UTC_gap = _convertStrToNumberUTCValue(v.UTC_gap);
                return v;
              });

              if( vm.pageType==='registration' ){
                vm._state.utc = vm._options.utcFilteredList[0].UTC_no;
                vm._state.utc_gap = vm._options.utcFilteredList[0].UTC_gap;
              }else{
                _onChangeUTC();
              }

            },
            function(__failData){
              console.log('getUTC __failData: ' , __failData);
            }
          );

        orgAdminManager.getCurrency()
          .then(
            function(__sucData){
              console.log('getCurrency __sucData: ' , __sucData);
              vm._options.currencyList = __sucData.data.results;
            },
            function(__failData){
              console.log('getCurrency __failData: ' , __failData);
            }
          );

        orgAdminManager.getLanguage()
          .then(
            function(__sucData){
              console.log('getLanguage __sucData: ' , __sucData);
              vm._options.languageList = __sucData.data.results;
            },
            function(__failData){
              console.log('getLanguage __failData: ' , __failData);
            }
          );

        // if( vm._options.languageList.length>0 ) vm._state.language = vm._options.languageList[0].value;
        // if( vm._options.currencyList.length>0 ) vm._state.currency = vm._options.currencyList[0].value;
        // if( vm._options.utcList.length>0 ) vm._state.utc = vm._options.utcList[0].value;
      }

    }

    function _getUTCStr(){
      return 'UTC '+APP.info.user.timezone;
    }

    function _onChangeUTC(){
      console.log('_onChangeUTC vm._state.utc: ', vm._state.utc);

      if(vm._options.utcFilteredList && vm._options.utcFilteredList.length>0){
        var tmpval = _.find( vm._options.utcFilteredList, function(v){return parseInt(vm._state.utc, 10)===parseInt(v.UTC_no, 10)} );
        if(tmpval) {
          vm._state.utc_gap = tmpval.UTC_gap;
        }
      }
    }

    function _getMajorOrganization(){
      orgAdminManager
        .getMajorOrganization({
          account_uuid:vm.accountUuid,
          org_id:vm.orgId
        })
        .then(
          function(__sucData){
            console.log('orgAdminMainCtrl _getMajorOrganization __sucData: ' , __sucData);
            vm._state = _exceptionStateValue(_.extend(vm._state, __sucData.results[0]));
            vm.contact_uuid = __sucData.results[0].contact_uuid;
            $('#org_frm').removeClass('opacity_hide');
          },
          function (__failData) {
            PopupService.showPopup({type:'alert', msg:'정보를 가져오지 못했습니다.'});
            console.log('__failData: ' , __failData);
          }
        );
    }

    function _setMajorOrganization(){
      console.log('orgAdminMainCtrl _setMajorOrganization: ');
      _onChangeUTC();

      orgAdminManager
        .setMajorOrganization( _.extend({},vm._state) )
        .then(
          function(__sucData){
            console.log('orgAdminMainCtrl _setMajorOrganization __sucData: ' , __sucData);

            PopupService
              .showPopup({type:'alert', msg:'등록을 완료헀습니다.', afterCb:window.location.reload.bind(window.location)});

          },
          function (__failData) {
            if(__failData.data && __failData.data.code==="ER_DUP_ENTRY"){
              PopupService.showPopup({type:'alert', msg:'이미 등록된 사업자 등록번호입니다.'});
            }else{
              PopupService.showPopup({type:'alert', msg:'등록을 하지 못했습니다.'});
            }
            console.log('__failData: ' , __failData);
          }
        );

    }

    function _updateMajorOrganization(){
      _onChangeUTC();

      orgAdminManager
        .updateMajorOrganization( _.extend({},vm._state,{account_uuid:vm.contact_uuid,org_id:vm.orgId}) )
        .then(
          function(__sucData){
            console.log('orgAdminMainCtrl _updateMajorOrganization __sucData: ' , __sucData);

            PopupService
              .showPopup({type:'alert', msg:'수정을 완료헀습니다.'})
              .then(function(){
                window.location.replace(APP.info.gURL+APP.URL_INFO.menu.OPR_MNG_ORG_ADMIN_MODIFICATION.link);
              });

          },
          function (__failData) {
            if(__failData.data && __failData.data.code==="ER_DUP_ENTRY"){
              PopupService.showPopup({type:'alert', msg:'이미 등록된 사업자 등록번호입니다.'});
            }else{
              PopupService.showPopup({type:'alert', msg:'등록을 하지 못했습니다.'});
            }
            console.log('__failData: ' , __failData);
          }
        );

    }

    function _goListPage(){
      window.location.replace( listURL );
    }

    function _exceptionStateValue(__state){
      if(__state.contact_homepage===null) __state.contact_homepage = '';
      if(__state.contact_mail===null) __state.contact_mail = '';
      if(__state.contact_name===null) __state.contact_name = '';
      if(__state.contact_phone===null) __state.contact_phone = '';
      if(__state.language===null || __state.language==='' || __state.language===undefined) __state.language = vm._options.languageList[0].lang_cd;
      if(__state.currency===null || __state.currency==='' || __state.currency===undefined) __state.currency = vm._options.currencyList[0].currency_cd;
      if(__state.utc===null || __state.utc==='' || __state.utc===undefined) __state.utc = _convertStrToNumberUTCValue(vm._options.utcList[0].UTC_gap);
      return __state;
    }

    function _convertStrToNumberUTCValue(__utcStr){
      return Number( __utcStr.substr(0,3) );
    }

    function _convertNumberToStringUTCValue(__utcNum){
      var plusMinus = (String(__utcNum).indexOf('-')===-1)?'+':'-';
      var hour = (Math.abs(__utcNum)<10)?'0'+Math.abs(__utcNum):Math.abs(__utcNum);
      var minute = '00';
      return plusMinus + hour + ':' + minute;
    }

  }
  orgAdminMainCtrl.$inject = ['$scope', '$location', '_', '$timeout', 'XHR', 'PubSub', 'OrgAdminManager', 'API_URL_INFO', 'COMMON_UTIL', 'Validator', 'PopupService'];
  orgAdminMainCtrl.bindToController = {
    pageType:'@',
    authType:'@',
    accountUuid:'@',
    orgId:'@'
  };
  orgAdminMainCtrl.link = function () {
    return function (scope, elem, attrs, ctrl, transclude) {

    }
  };




})(window.jQuery, window.angular, window._, window.APP);
