(function($, angular, _, APP){


  /*

   Field Name	Info	Type

    admin_uuid	the master account uuid(login user)	String
    secret	the admin account secret data (in the user login session data)	String
    verify_code	user verification code made by admin_uuid	String

   parent_org_id	the parent organization id	String
   org_certification	the organization's certificate for business registration	String
   org_name	the organization name	String
   status	0 : activated, 1 : disabled	Integer
   contact_name	the name to contact	String
   contact_phone	the phone number to contact	String
   contact_mail	the email to contact	String
   contact_homepage	the homepage url to contact	String
   purchase_url	the purchase item url	String
   show	for viewing some information(debug ...)

  */

  angular.module('App.org')
    .service('StoreAdminManager', StoreAdminManager);

  function StoreAdminManager(_, $q , $timeout, $http, XHR, API_URL_INFO, COMMON_UTIL){
    var vm = this;

    vm._state = {
      parent_org_id:APP.info.user.parent_org_id,
      org_certification:'',
      org_name:'',
      status:0,
      contact_name:'',
      contact_phone:'',
      contact_mail:'',
      contact_homepage:'',
      purchase_url:'',
      show:'debug'
    };

    vm.exports = {
      init:_init,
      getState:_getState,
      setState:_setState,
      setMinorOrganization:_setMinorOrganization,
      updateMinorOrganization:_updateMinorOrganization,
      deleteOrgAdmin:_deleteOrgAdmin,
      getMinorOrganization:_getMinorOrganization
    };

    function _init(){
      console.log( 'StoreAdminManager.init' );
    }

    function _getState(){
      return vm._state;
    }

    function _setState(__passingData){
      vm._state = _.extend( vm._state, __passingData );
    }

    function _setMinorOrganization(__data){
      var deferred = $q.defer();
      console.log('StoreAdminManager _setMinorOrganization __data: ' , __data);
      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.SET_MINOR_ORGANIZATION,
          data:__data
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _updateMinorOrganization(__data){
      var deferred = $q.defer();
      console.log('StoreAdminManager _updateMinorOrganization __data: ' , __data);
      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.UPDATE_MINOR_ORGANIZATION,
          data:__data
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _deleteOrgAdmin(__data){

    }

    function _getMinorOrganization(__data){
      var deferred = $q.defer();
      console.log('StoreAdminManager _getOrgAdmin __data: ' , __data);
      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_MINOR_ORGANIZATION+'?account_uuid='+(__data.account_uuid || '')+'&org_id='+(__data.org_id || '')
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }


  }
  StoreAdminManager.$inject= ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', 'COMMON_UTIL'];


})(window.jQuery, window.angular, window._, window.APP);
