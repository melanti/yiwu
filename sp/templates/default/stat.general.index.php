<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<link href="<?php echo ADMIN_TEMPLATES_URL;?>/css/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="<?php echo ADMIN_TEMPLATES_URL;?>/css/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->
<style>
	.new-general td{ font-size: 14px; }
</style>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['nc_statgeneral'];?></h3>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  
   <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th class="nobg" colspan="12"><div class="title"><h5><?php echo $lang['nc_prompts'];?></h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
        <ul>
            <li><?php echo $lang['stat_validorder_explain'];?></li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  
  <table class="table tb-type2 new-general">
	<thead class="thead">
		<tr class="space">
			<th colspan="15"><?php echo @date('Y-m-d',$output['stat_time']);?>최근정보</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				주문금액&nbsp;<i title="유효 주문 총금액" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['orderamount'];?>원</b>
			</td>
			<td>
				주문회원수&nbsp;<i title="유효 주문 회원수" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['ordermembernum'];?></b>
			</td>
			<td>
				주문량&nbsp;<i title="유효 주문 총 수량" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['ordernum'];?></b>
			</td>
			<td>
				주문상품수&nbsp;<i title="유효 주문 상품 총 수량" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['ordergoodsnum'];?></b>
			</td>
		</tr>
		<tr>
			<td>
				평균가격&nbsp;<i title="유효 주문 상품 평균 단가" class="tip icon-question-sign"></i>
				<br><b><?php echo number_format($output['statnew_arr']['priceavg']*C('site_cur'));?>원</b>
			</td>
			<td>
				평균매주문가&nbsp;<i title="유효 단 주문 평균가" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['orderavg'];?>원</b>
			</td>
			<td>
				신규회원&nbsp;<i title="신규 회원수" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['newmember'];?></b>
			</td>
			<td>
				총회원수&nbsp;<i title="총 회원수" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['membernum'];?></b>
			</td>
		</tr>
		<tr>
			<td>
				신규업체&nbsp;<i title="신규 가입된 업체수" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['newstore'];?></b></td>
			<td>
				업체수량&nbsp;<i title="모든 업체수" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['storenum'];?></b></td>
			<td>
				추가상품&nbsp;<i title="신규 상품수" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['newgoods'];?></b></td>
			<td>
				상품수량&nbsp;<i title="총 상품수" class="tip icon-question-sign"></i>
				<br><b><?php echo $output['statnew_arr']['goodsnum'];?></b></td>
		</tr>
	</tbody>
  </table>
  
  <table class="table tb-type2">
	<thead class="thead">
		<tr class="space">
			<th colspan="15"><?php echo @date('Y-m-d',$output['stat_time']);?>매출</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><div id="container" class="w100pre close_float" style="height:400px"></div></td>
		</tr>
	</tbody>
  </table>
  
  
  <div class="w40pre floatleft">
  	<table class="table tb-type2">
    	<thead class="thead">
    		<tr class="space">
    			<th colspan="15">7일내 업체 매출TOP30&nbsp;<i title="어제부터 7일내 30명 업체 매출 랭킹" class="tip icon-question-sign"></i></th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td>번호</td>
    			<td>업체명</td>
    			<td>주문금액</td>
    		</tr>
    		<?php foreach((array)$output['storetop30_arr'] as $k=>$v){ ?>
    		<tr>
    			<td><?php echo $k+1;?></td>
    			<td><?php echo $v['store_name'];?></td>
    			<td><?php echo number_format($v['orderamount']);?>원</td>
    		</tr>
    		<?php } ?>
    	</tbody>
      </table>
  </div>
  
  <div class="w50pre floatleft" style="margin-left: 50px;">
  	<table class="table tb-type2">
    	<thead class="thead">
    		<tr class="space">
    			<th colspan="15">7일내 상품 매출TOP30&nbsp;<i title="어제부터 7일내 30개 상품 랭킹" class="tip icon-question-sign"></i></th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td>번호</td>
    			<td>상품명</td>
    			<td>판매량</td>
    		</tr>
    		<?php foreach((array)$output['goodstop30_arr'] as $k=>$v){ ?>
    		<tr>
    			<td><?php echo $k+1;?></td>
    			<td class="alignleft"><a href='<?php echo urlShop('goods', 'index', array('goods_id' => $v['goods_id']));?>' target="_blank"><?php echo $v['goods_name_ko'];?></a></td>
    			<td><?php echo $v['ordergoodsnum'];?></td>
    		</tr>
    		<?php } ?>
    	</tbody>
      </table>
  </div>
  <div class="close_float"></div>
</div>

<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/statistics.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>

<script>
$(function () {
	//Ajax提示
    $('.tip').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'top',
        offsetY: 5,
        allowTipHover: false
    });
    
	$('#container').highcharts(<?php echo $output['stattoday_json'];?>);
});
</script>