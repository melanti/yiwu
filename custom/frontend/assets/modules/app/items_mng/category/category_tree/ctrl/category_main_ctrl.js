(function ($, angular, _, APP) {

  APP.modules.ctrl.categoryMainCtrl = categoryMainCtrl;

  //categoryMainCtrl
  function categoryMainCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO, CategoryManager, COMMON_UTIL){
    var vm = this,
        categoryManager = CategoryManager.exports;

    console.log('CategoryManager: ' ,CategoryManager);

    vm.isShowCateInfo = false;
    vm.isShowCateDefaultInfo = false;
    vm.isShowCateNew = false;
    vm.curRow;
    vm.newRow;
    vm.authCategory;
    vm.accountType;
    vm.newRowTemplate = {
      cate_name:'',
      parent_cate_id:'',
      cate_status:'activated',
      order_by:'reg_date_desc'  //현재(2016.04.12) sorting value는 reg_date_desc로 고정.
    };

    vm._state = categoryManager.getState();

    vm.exports = {
      getIsChangingCategory:categoryManager.getIsChangingCategory,
      getFilteredResults:categoryManager.getFilteredResults,
      getDpt01ItemsCount:categoryManager.getDpt01ItemsCount,
      getResultsRow:categoryManager.getResultsRow,
      getAuthCategory:function(){return vm.authCategory;},
      getAccountType:function(){return vm.accountType;},
      setFilteredResults:_setFilteredResults,
      setDefaultConfig:_setDefaultConfig,
      setCategory:_setCategory,
      setShowCateInfo:_setShowCateInfo,
      setCurRow:_setCurRow,
      updateCategoryResultPriorty:categoryManager.updateCategoryResultPriorty,
      updateCategory:_updateCategory,
      updateCategoryDepth:_updateCategoryDepth,
      stopUpdateCategoryDepth:categoryManager.stopUpdateCategoryDepth,
      changeStatus:_changeStatus,
      moveCagegory:categoryManager.moveCagegory,
      showCateNew:_showCateNew
    };


    _init();

    function _init(){
      //console.log('categoryMainCtrl _init');
      categoryManager.init();
      console.log('vm.authCategory: ' , vm.authCategory, typeof vm.authCategory);

      vm.authCategory = (String(APP.info.user.auth.category).concat(''));
      vm.accountType = String(APP.info.user.account_type).concat('');
      if(vm.accountType==='shop'){
        vm.authCategory = '1';
      }
    }

    function _setFilteredResults(__data){
      categoryManager.setFilteredResults(__data);
    }

    function _setCurRow(__data){
      var tmpRow = categoryManager.getFilteredResultsRow(__data);
      if(tmpRow){
        vm.curRow = tmpRow.row;
        vm.curRow.item_sort = 'reg_date_desc';  //현재(2016.04.12) sorting value는 reg_date_desc로 고정.
      }
    }

    function _showCateNew(){
      var chkIsPrevShowCateInfo = vm.isShowCateInfo;

      if(chkIsPrevShowCateInfo){
        if(vm.curRow && vm.curRow.parentId===undefined){
          //console.log('depth02 NEW');
          _setShowCateInfo('new');
          _setNewRow({parentId:vm.curRow.id, parentName:vm.curRow.name});
        }else{
          alert( '분류는 2단계까지만 가능합니다.' );  //'분류는 2단계까지만 가능합니다.'
        }

      }else{
        //console.log('depth01 NEW');
        _setShowCateInfo('new');
        _setNewRow();
      }
    }


    function _setShowCateInfo(__opt){
      //console.log('_setShowCateInfo: ' , __opt);
      vm.isShowCateDefaultInfo = false;
      vm.isShowCateNew = false;
      vm.isShowCateInfo = false;
      //vm.newRow = null;

      switch(__opt){
        case 'default':
          vm.isShowCateDefaultInfo = true;
          break;
        case 'new':
          vm.isShowCateNew = true;

          break;
        case 'info':
          vm.isShowCateInfo = true;
          break;
        default:
          break;
      }
    }


    function _setNewRow(__opt){
      if(__opt){
        vm.newRow = _.clone(_.extend({},vm.newRowTemplate,{parent_cate_id:__opt.parentId}));
      }else{
        vm.newRow = _.clone(_.extend({},vm.newRowTemplate,{parent_cate_id:''}));
      }
    }

    function _setDefaultConfig(){
      //show_2depth , show_no_item , show

      categoryManager
        .setDefaultConfig({
          show_2depth:vm._state.defaultSetting.show_2depth,
          show_no_item:vm._state.defaultSetting.show_no_item,
          show:'debug'
        })
        .then(
          function(__sucData){
            alert( '저장되었습니다'); //'저장되었습니다'
            categoryManager.getDefaultConfig();
          },
          function(__failData){
            console.log('categoryMainCtrl _setDefaultConfig fail: ' , __failData);
          }
        );
    }

    function _setCategory(__opt){
      if( COMMON_UTIL.isExist(vm.newRow.cate_name) ){
      }else{
        alert( '분류명을 입력해주세요.');  //'분류명을 입력해주세요.'
        return false;
      }

      categoryManager.setCategory(__opt)
        .then(
          function(__sucData){
            _setShowCateInfo();
            alert( '저장되었습니다'); //'저장되었습니다'
            return categoryManager.getCateList();
          },
          function(__failData){
            console.log('categoryMainCtrl _setCategory setCategory: error' , __failData);
          }
        )
        .then(
          function(__sucData){
            //console.log('load list.');
          },
          function(__failData){
            console.log('categoryMainCtrl _setCategory getCateList: error' , __failData);
          }
        );
    }

    function _updateCategory(__opt){
      categoryManager.updateCategory(__opt)
        .then(
          function(__sucData){
            _setShowCateInfo();
            alert( '저장되었습니다'); //'저장되었습니다'
            return categoryManager.getCateList();
          },
          function(__failData){
            console.log('categoryMainCtrl _updateCategory updateCategory: error' , __failData);
          }
        )
        .then(
          function(__sucData){
          },
          function(__failData){
            console.log('categoryMainCtrl updateCategory getCateList: error' , __failData);
          }
        );
    }

    function _updateCategoryDepth(__opt){

      categoryManager.updateCategoryDepth(__opt)
        .then(
          function(__sucData){
            //return categoryManager.getCateList();
          },
          function(__failData){
            console.log('categoryMainCtrl _updateCategoryDepth updateCategoryDepth: error' , __failData);
          }
        )
        .then(
          function(__sucData){
          },
          function(__failData){
            console.log('categoryMainCtrl _updateCategoryDepth getCateList: error' , __failData);
          }
        );
    }

    function _changeStatus(){
      var comfirmMsg = '정말 삭제하시겠습니까?', completeMsg = '삭제되었습니다.', errMsg = '삭제를 하지 못했습니다.';  //'정말 삭제하시겠습니까?'  '삭제되었습니다.' '삭제를 하지 못했습니다.'

      if( vm.isShowCateInfo && vm.curRow && COMMON_UTIL.isExist(vm.curRow.id) && parseInt(vm.curRow.id)>=0 ){

        if (confirm(comfirmMsg) == true){
          categoryManager.changeStatus({ids:vm.curRow.id})
            .then(
              function(__sucData){
                _setShowCateInfo();
                alert(completeMsg);
                return categoryManager.getCateList();
              },
              function(__failData){
                alert(errMsg);
                console.log('categoryMainCtrl _updateCategory updateCategory: error' , __failData);
              }
            )
            .then(
              function(__sucData){
              },
              function(__failData){
                alert(errMsg);
                console.log('categoryMainCtrl updateCategory getCateList: error' , __failData);
              }
            );

        }

      }else{
        alert( '삭제할 분류를 선택해주세요' );  //'삭제할 분류를 선택해주세요'
      }

    }


  }

})(window.jQuery, window.angular, window._, window.APP);
