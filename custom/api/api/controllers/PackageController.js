module.exports = {
	checkInfo: function(req, res) {
		var debug = require("./libs/debug.js"),
			chk_data = require("./libs/chkData.js"),
			pkg_name = req.param('pkg_name'),
			os = req.param('os'),
			version = parseFloat(req.param('version')),
			status = 501;

		var query = 'SELECT A.software_url AS app_link, ' +
				'IF( A.software_version > ? AND A.software_abs_yn = "Y" , "true", "false" ) AS update_flag, ' +
				'A.software_version AS version, ' +
				'IF(A.software_status = "activated", "true", "false" ) sdk_permission ' +
				'FROM ' +
				'sv_software_deploy A ' +
				'WHERE A.software_code = ? ' +
				'AND A.software_name = ?',
			result,
			error = {};

		chk_data.chkParam(req, res, [pkg_name, os, version], function() {
			DB.query(query, [version, pkg_name, os], function(err, results){
				if(err){
					status = 500;
					return res.send(status, debug.wrap(req, res, status, err));
				}else{
					if(results.length === 0){
						status = ErrorHandler.NOT_FOUND_DATA.status;
						error.code = ErrorHandler.NOT_FOUND_DATA.code;
						error.index = 0;
						return res.send(status, debug.wrap(req, res, status, error));
					}
					status = 200;
					result = results[0];
					result.update_flag = (results[0].update_flag === "true")? true : false;
					result.sdk_permission = (results[0].sdk_permission === "true")? true : false;

					if(results[0].version.indexOf('.') == -1){
						result.version = parseInt(results[0].version, 10);
					}

					return res.send(status, debug.wrap(req, res, status, result));
				}
			});
		});
	},
	checkInfoV2: function(req, res) {
		var debug = require("./libs/debug.js"),
			chk_data = require("./libs/chkData.js"),
			pkg_name = req.param('pkg_name'),
			os = req.param('os'),
			version = parseFloat(req.param('version')),
			status = 501;

		var query = 'SELECT A.software_name os,  ' +
				'A.software_url AS app_link,  ' +
				'A.software_version AS version, ' +
				'A.software_abs_version AS required_version, ' +
				'IF(A.software_status = "activated", "true", "false" ) sdk_permission ' +
				'FROM ' +
				'sv_software_deploy A ' +
				'WHERE A.software_code = ? ' +
				'AND A.software_name = ?',
			result,
			error = {};

		chk_data.chkParam(req, res, [pkg_name, os], function() {
			DB.query(query, [pkg_name, os], function(err, results){
				if(err){
					status = 500;
					return res.send(status, debug.wrap(req, res, status, err));
				}else{
					if(results.length === 0){
						status = ErrorHandler.NOT_FOUND_DATA.status;
						error.code = ErrorHandler.NOT_FOUND_DATA.code;
						error.index = 0;
						return res.send(status, debug.wrap(req, res, status, error));
					}
					status = 200;
					result = results[0];
					result.sdk_permission = (results[0].sdk_permission === "true")? true : false;

					return res.send(status, debug.wrap(req, res, status, result));
				}
			});
		});
	},
	checkBlendShapeVersion: function(req, res) {
		var debug = require("./libs/debug.js"),
			chk_data = require("./libs/chkData.js"),
			show = req.param('show'),
			status = 501;

		var query = "SELECT software_version blendshape_version " +
				"FROM sv_software_deploy " +
				"WHERE software_service = 'blend_shape' " +
				"AND software_status = 'activated' ",
			result_wrap = {},
			error = {};

		DB.query(query, function(err, results){
			if(err){
				status = 500;
				return res.send(status, debug.wrap(req, res, status, err));
			}else{
				if(results.length === 0){
					status = ErrorHandler.NOT_FOUND_DATA.status;
					error.code = ErrorHandler.NOT_FOUND_DATA.code;
					error.index = 0;
					return res.send(status, debug.wrap(req, res, status, error));
				}
				status = 200;
				result_wrap.results = results;


				return res.send(status, debug.wrap(req, res, status, result_wrap));
			}
		});
	}
};
