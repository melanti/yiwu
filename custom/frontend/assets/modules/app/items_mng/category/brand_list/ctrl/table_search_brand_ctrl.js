
(function($, angular, _, APP){

  APP.modules.ctrl.tableSearchBrandCtrl = tableSearchBrandCtrl;

  //tableSearchBrandCtrl
  function tableSearchBrandCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO){
    var vm = this;

    vm.query_opt;
    vm.query;
    vm.status='all';

    vm.getCount;
    vm.getStart;
    vm.getQueryOpt;
    vm.getQuery;
    vm.getStatus;
    vm.updatePage;
    vm.updateQueryOpt;
    vm.updateQuery;
    vm.updateStatus;

    vm.onUpdateQueryOpt = function(){
       vm.updateQueryOpt({options:vm.query_opt});
    };

    vm.onUpdateQuery = function(){
       vm.updateQuery({options:vm.query});
    };

    vm.onUpdateStatus = function(){
       vm.updateStatus({options:vm.status});
    };

    _init();

    function _init(){
      vm.query_opt = vm.getQueryOpt();
      vm.status = vm.getStatus();
    }

  }

  tableSearchBrandCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO'];
  tableSearchBrandCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableSearchCtrl:true});
    }
  };
  tableSearchBrandCtrl.bindToController= {
    getCount:'&',
    getStart:'&',
    getQueryOpt:'&',
    getQuery:'&',
    getStatus:'&',
    updatePage:'&',
    updateQueryOpt:'&',
    updateQuery:'&',
    updateStatus:'&'
  };

})(window.jQuery, window.angular, window._, window.APP);
