<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/base.css" rel="stylesheet" type="text/css">
<table style="width:600px;font-size:15px;line-height:40px;margin-top:50px;" align="center">
<tr><td colspan="2"><img height="32" src="<?php echo C('member_logo') == ''?UPLOAD_SITE_URL.DS.ATTACH_COMMON.DS.C('site_logo'):UPLOAD_SITE_URL.DS.ATTACH_COMMON.DS.C('member_logo'); ?>"></td></tr>
<tr><td colspan="4" style="border-bottom:1px solid #ccc"><h3 style="font-size:20px;line-height:60px"><?php echo C('site_name');?> - E-쿠폰 주문 결산장부</h3></td></tr>
<tr>
<td width="80px">판매자</td><td colspan="3"><?php echo $output['bill_info']['ob_store_name'];?></td>
</tr>
<tr><td>결산번호</td><td width="130px"><?php echo $output['bill_info']['ob_no'];?></td><td width="100px">결산범위</td><td><?php echo date('Y-m-d',$output['bill_info']['ob_start_date']);?> &nbsp;부터&nbsp; <?php echo date('Y-m-d',$output['bill_info']['ob_end_date']);?></td></tr>
<tr><td>생성시간</td><td><?php echo date('Y-m-d',$output['bill_info']['ob_create_date']);?></td>
<td>결산상태</td><td><?php echo billStateKo($output['bill_info']['ob_state']);?></td></tr>
<?php if ($output['bill_info']['ob_state'] == BILL_STATE_SUCCESS){?>
<tr>
<td>
결산일</td><td><?php echo date('Y-m-d',$output['bill_info']['ob_pay_date']);?>
</td>
</tr>
<?php }?>
<tr><td>판매자미수금</td><td colspan="3"><?php echo number_format($output['bill_info']['ob_result_totals_ko']);?></td></tr>
<tr><td>결산상세</td><td colspan="3"><?php echo number_format($output['bill_info']['ob_order_totals_ko']);?> (소비금액) - <?php echo number_format($output['bill_info']['ob_commis_totals_ko']);?> (수수료)</td></tr>
</table>