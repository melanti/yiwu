(function($, angular, _){

  angular.module('App.items')
    .service('CategoryManager', ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', 'APP_INFO_DATA', 'COMMON_UTIL', CategoryManager]);

  function CategoryManager(_, $q , $timeout, $http, XHR, API_URL_INFO, APP_INFO_DATA, COMMON_UTIL){
    var vm = this;
    var swapCanceler;

    vm._state = {
      ischangingCategory:false,
      isMoveCategory:false,
      defaultSetting:{
        show_2depth: true,
        show_no_item: true
      },
      curCategory:{},
      results: [],
      filteredResults: []
    };

    vm.exports = {
      init:_init,
      getIsChangingCategory:_getIsChangingCategory,
      getDefaultSetting:_getDefaultSetting,
      getState:function(){ return vm._state; },
      getDpt01ItemsCount:_getDpt01ItemsCount,
      getCurCategory:_getCurCategory,
      getDefaultConfig:_getDefaultConfig,
      getCateList:_getCateList,
      getResults:_getResults,
      getResultsRow:_getResultsRow,
      getFilteredResultsRow:_getFilteredResultsRow,
      getFilteredResults:_getFilteredResults,
      setDefaultConfig:_setDefaultConfig,
      setCategory:_setCategory,
      setFilteredResults:_setFilteredResults,
      updateCategoryResultPriorty:_updateCategoryResultPriorty,
      updateCategory:_updateCategory,
      updateCategoryDepth:_updateCategoryDepth,
      stopUpdateCategoryDepth:_stopUpdateCategoryDepth,
      changeStatus:_changeStatus,
      resetResults:_resetResults,
      moveCagegory:function(){ vm._state.isMoveCategory = true; }
    };

    function _init(){
      if(chkUserType){
        _start();
      }
    }

    function _start(){
      _getCateList().then(
        function(__sucData){
        },
        function(__errData){
          console.log('__errData: ' , __errData);

        }
      );

      _getDefaultConfig().then(
        function(__sucData){
        },
        function(__errData){
          console.log('__errData: ' , __errData);

        }
      );
    }


    function _getDefaultSetting(){
      return vm._state.defaultSetting;
    }

    function _getCurCategory(){
      return vm._state.curCategory;
    }

    function _getDefaultConfig(){
      var deferred = $q.defer(),
          show='debug';

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_CATEGORY_DEFAULT+'?show='+show
        })
        .then(
          function(__sucData){
            var tmp_show_2depth = ( (__sucData.data.results[0].show_2depth)==1 || (__sucData.data.results[0].show_2depth)=='1' )?true:false;
            var tmp_show_no_item = ( (__sucData.data.results[0].show_no_item)==1 || (__sucData.data.results[0].show_no_item)=='1' )?true:false;

            vm._state.defaultSetting.show_2depth = tmp_show_2depth;
            vm._state.defaultSetting.show_no_item = tmp_show_no_item;
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _setDefaultConfig(__list){
      // show_2depth , show_no_item , show
      var deferred = $q.defer(),
          show_2depth = __list.show_2depth,
          show_no_item = __list.show_no_item,
          show='debug', data;

      data = {
        show_2depth:show_2depth,
        show_no_item:show_no_item,
        show:show
      };

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.UPDATE_CATEGORY_DEFAULT,
          data:data
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }


    function _getResults(){
      return vm._state.results;
    }

    //TODO
    //APP.info.user.account_type==='shop' 권한 체킹확인
    function chkUserType(){
      if( APP.info.user && APP.info.user.account_type==='shop' && $(document.body).attr('data-page-id')=='category-operation' ){
        alert('페이지 권한이 없습니다.');
        window.location.href=APP.info.gURL+APP.URL_INFO.menu.ITEMS_MNG_ITEMS_LIST.link;
        return false;
      }
      return true;
    }

    function _getCateList(){
      var deferred = $q.defer(),
          status = 'all',
          show='debug,item_count';

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_CATEGORY_LIST+'?sex=all&status='+status+'&show='+show
        })
        .then(
          function(__sucData){
            _resetResults(__sucData.data.results);
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _resetResults(__data){
      vm._state.isMoveCategory = false;
      _setCateResult(__data);
      _setFilteredResults(__data);
    }

    function _setCateResult(__data){
      vm._state.results = __data;
    }

    function _setFilteredResults(__opt){
      vm._state.filteredResults = __opt;
      _updateCategoryResultPriorty(vm._state.filteredResults);
    }

    function _getFilteredResults(){
      return vm._state.filteredResults;
    }


    function _getResultsRow(__id){
      var tmpRow, compareId = parseInt(__id,10);

      vm._state.results.forEach(function(ele){
        if(parseInt(ele.id,10)===compareId){
          tmpRow = {row:_.clone(ele), depth:0};
        }else{
          if(ele.children && ele.children.length>0){
            ele.children.forEach(function(eleChild){
              if(parseInt(eleChild.id,10)===compareId){
                tmpRow = {row:_.clone(_.extend({},eleChild,{parentName:ele.name, parentId:ele.id})), depth:1};
              }
            });
          }
        }
      });

      return tmpRow;
    }

    function _getFilteredResultsRow(__id){
      var tmpRow, compareId = parseInt(__id,10);

      vm._state.filteredResults.forEach(function(ele){
        if(parseInt(ele.id,10)===compareId){
          tmpRow = {row:_.clone(ele), depth:0};
        }else{
          if(ele.children && ele.children.length>0){
            ele.children.forEach(function(eleChild){
              if(parseInt(eleChild.id,10)===compareId){
                tmpRow = {row:_.clone(_.extend({},eleChild,{parentName:ele.name, parentId:ele.id})), depth:1};
              }
            });
          }
        }
      });

      return tmpRow;
    }

    function _getDpt01ItemsCount(__list){
      var cnt = 0;
      if(__list.item_count>=0) cnt += parseInt(__list.item_count, 10);

      if(__list.children && __list.children.length>0){
        _.each(__list.children,
          function(ele, idx, iList){
            if( COMMON_UTIL.isExist(ele.item_count) && ele.item_count>=0){
              cnt += parseInt(ele.item_count, 10);
            }else{
            }
          }
        );

      }
      return cnt;
    }

    function _getIsChangingCategory(){
      return vm._state.ischangingCategory;
    }

    function _setCategory(__list){
      // cate_name , parent_cate_id , cate_status , order_by , show
      var deferred = $q.defer(),
          cate_name = __list.cate_name,
          parent_cate_id = __list.parent_cate_id || '',
          cate_status = __list.cate_status,
          order_by = __list.order_by,
          show='debug', data;

      data = {
        cate_name:cate_name,
        parent_cate_id:parent_cate_id,
        cate_status:cate_status,
        order_by:order_by,
        show:show
      };

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.SET_CATEGORY,
          data:data
        })
        .then(
          function(__sucData){
            //_setCateResult(__sucData.data);
            //_setFilteredResults(__sucData.data.results);
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _updateCategory(__list){
      // category_id , name , cate_status , order_by, show

      var deferred = $q.defer(),
          category_id = __list.id,
          liname = __list.name,
          cate_status = __list.status,
          order_by = __list.item_sort,
          show='debug', data;

      data = {
        cate_id:category_id,
        cate_name:liname,
        cate_status:cate_status,
        order_by:order_by,
        show:show
      };

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.UPDATE_CATEGORY,
          data:data
        })
        .then(
          function(__sucData){
            //_setCateResult(__sucData.data);
            //_setFilteredResults(__sucData.data.results);
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _updateCategoryDepth(__list){
      // cate_data , show

      _.each(__list.cate_data.categories, function(ele, idx, li){
        if(ele.$scope) delete ele.$scope;
        if(ele.$$hashKey) delete ele.$$hashKey;

        if(ele.children && ele.children.length>0){
          _.each(ele.children, function(jele, jidx, jli){
            if(jele.$scope) delete jele.$scope;
            if(jele.$$hashKey) delete jele.$$hashKey;
          });
        }
      });

      var deferred = $q.defer(),
          cate_data = JSON.stringify(_.clone(__list.cate_data)),
          show='debug,item_count',
          data;

      _stopUpdateCategoryDepth();
      swapCanceler = $q.defer();
      data = {
        cate_data:(cate_data),
        show:show
      };

      vm._state.ischangingCategory = true;

      /*XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.UPDATE_CATEGORY_DEPTH,
          data:data,
          cancelID:__list.cancelID
        })
        .then(
          function(__sucData){
            vm._state.ischangingCategory = false;
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            vm._state.ischangingCategory = false;
            deferred.reject(__errData);
          }
        );*/

      $http.apply(null, [{
          method: 'post',
          url: API_URL_INFO.UPDATE_CATEGORY_DEPTH,
          data:data,
          timeout:swapCanceler.promise
        }])
        .success(function (__res) {
          vm._state.ischangingCategory = false;
          swapCanceler = null;
          try {
            if (__res.debug && (__res.debug.status == '200' || __res.debug.status == 200)) {
              _resetResults(__res.results);
              deferred.resolve(__res);
            } else {
              deferred.reject(__res);
            }
          } catch (e) {
            deferred.reject(__res);
          }

        })
        .error(function (__res) {
          vm._state.ischangingCategory = false;
          swapCanceler = null;
          deferred.reject(__res);
        });

      return deferred.promise;
    }

    function _stopUpdateCategoryDepth(){
      if(swapCanceler) {
        swapCanceler.resolve();
        swapCanceler = null;
      }
    }


    function _updateCategoryResultPriorty(__result){
      _.each(__result,
        function(i, iIndex, iList){
          i.priority = iIndex+1;

          if(i.children){
            _.each(i.children , function(j, jIndex, jList){
              j.priority = jIndex+1;
            });
          }

        });
    }

    function _changeStatus(__list){

      var deferred = $q.defer();

      /*
       ids	ids (separated with comma) - 예) 3,9,7	String
       kind	item, category, brand, 3d	String
       status	status(activated, disabled, deleted)	String
       show	for viewing some information(debug ...)	String
       */

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.CHANGE_STATUS,
          data: {ids: String(__list.ids), kind:'category', status:'deleted', show:'debug,contact'}
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }


  }


})(window.jQuery, window.angular, window._);


