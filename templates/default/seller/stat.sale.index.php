<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert mt10">
	<ul class="mt5">
		<li>1. 아래 조건 중 한 항목이라도 부합하면 유효한 주문 입니다. <br>(1) 온라인 지불 방식을 통해 지물 및 결제완료한 주문 (2) 货到付款(상품을 받고 지불하는 방식) 방식으로 결제 완료 후 구매결정이 완료 된 주문</li>
		<li>2. 아래 차트는 검색조건에 부합하는 유효한 주문 중, 주문 총 액과  시간대별 주문 상황과 그 전 시간대의 주문 상황을 비교해서 나타냅니다.</li>
        <li>3. 아래 통계표는 검색조건에 부합하는 모든 유효한 주문기록을 나타내며, "Excel"을 클릭하시면 엑셀파일로 주문기록을 보실 수 있습니다.</li>
    </ul>
</div>
<form method="get" action="index.php" target="_self">
  <table class="search-form">
    <input type="hidden" name="act" value="statistics_sale" />
    <input type="hidden" name="op" value="sale" />
    <tr>
    	<td class="tr">
    		<div class="fr">
    			<label class="submit-border"><input type="submit" class="submit" value="검색" /></label>
    		</div>
    		<div class="fr">&nbsp;
              	<select name="order_type" id="order_type" class="querySelect">
                  <option value="" <?php echo $_REQUEST['order_type']==''?'selected':''; ?>>선택</option>
                  <option value="<?php echo ORDER_STATE_NEW; ?>" <?php echo $_REQUEST['order_type']!='' && $_REQUEST['order_type']==ORDER_STATE_NEW?'selected':''; ?>>결제대기</option>
                  <option value="<?php echo ORDER_STATE_PAY; ?>" <?php echo $_REQUEST['order_type']!='' && $_REQUEST['order_type']==ORDER_STATE_PAY?'selected':''; ?>>발송대기</option>
                  <option value="<?php echo ORDER_STATE_SEND; ?>" <?php echo $_REQUEST['order_type']!='' && $_REQUEST['order_type']==ORDER_STATE_SEND?'selected':''; ?>>수령대기</option>
                  <option value="<?php echo ORDER_STATE_SUCCESS; ?>" <?php echo $_REQUEST['order_type']!='' && $_REQUEST['order_type']==ORDER_STATE_SUCCESS?'selected':''; ?>>거래완료</option>
                  <option value="<?php echo ORDER_STATE_CANCEL; ?>" <?php echo $_REQUEST['order_type']!='' && $_REQUEST['order_type']==ORDER_STATE_CANCEL?'selected':''; ?>>취소</option>
                </select>
    		</div>
    		<div class="fr">
        		<div class="fl" style="margin-right:3px;">
            		<select name="search_type" id="search_type" class="querySelect">
            			<option value="day" <?php echo $output['search_arr']['search_type']=='day'?'selected':''; ?>>일간별</option>
            			<option value="week" <?php echo $output['search_arr']['search_type']=='week'?'selected':''; ?>>주간별</option>
            			<option value="month" <?php echo $output['search_arr']['search_type']=='month'?'selected':''; ?>>월간별</option>
            		</select>
        		</div>
        		<div id="searchtype_day" style="display:none;" class="fl">
        			<input type="text" class="text w70" name="search_time" id="search_time" value="<?php echo @date('Y-m-d',$output['search_arr']['day']['search_time']);?>" /><label class="add-on"><i class="icon-calendar"></i></label>
                </div>
                <div id="searchtype_week" style="display:none;" class="fl">
                  	<select name="searchweek_year" class="querySelect">
                  		<?php foreach ($output['year_arr'] as $k=>$v){?>
                  		<option value="<?php echo $k;?>" <?php echo $output['search_arr']['week']['current_year'] == $k?'selected':'';?>><?php echo $v; ?></option>
                  		<?php } ?>
                    </select>
                    <select name="searchweek_month" class="querySelect">
                    	<?php foreach ($output['month_arr'] as $k=>$v){?>
                  		<option value="<?php echo $k;?>" <?php echo $output['search_arr']['week']['current_month'] == $k?'selected':'';?>><?php echo $v; ?></option>
                  		<?php } ?>
                    </select>
                    <select name="searchweek_week" class="querySelect">
                    	<?php foreach ($output['week_arr'] as $k=>$v){?>
                  		<option value="<?php echo $v['key'];?>" <?php echo $output['search_arr']['week']['current_week'] == $v['key']?'selected':'';?>><?php echo $v['val']; ?></option>
                  		<?php } ?>
                    </select>
              </div>
              <div id="searchtype_month" style="display:none;" class="fl">
                  	<select name="searchmonth_year" class="querySelect">
                  		<?php foreach ($output['year_arr'] as $k=>$v){?>
                  		<option value="<?php echo $k;?>" <?php echo $output['search_arr']['month']['current_year'] == $k?'selected':'';?>><?php echo $v; ?></option>
                  		<?php } ?>
                    </select>
                    <select name="searchmonth_month" class="querySelect">
                    	<?php foreach ($output['month_arr'] as $k=>$v){?>
                  		<option value="<?php echo $k;?>" <?php echo $output['search_arr']['month']['current_month'] == $k?'selected':'';?>><?php echo $v; ?></option>
                  		<?php } ?>
                    </select>
              </div>
    		</div>
    	</td>
    </tr>
  </table>
</form>

<div class="alert alert-info mt10" style="clear:both;">
	<ul class="mt5">
    <li>
    	<span class="w210 fl h30" style="display:block;">
    		<i title="검색조건에 부합하는 주문 총 금액" class="tip icon-question-sign"></i>
    		총 주문금액：<strong><?php echo $output['statcount_arr']['orderamount'];?>원</strong>
    	</span>
		<span class="w210 fl h30" style="display:block;">
			<i title="검색조건에 부합하는 주문 수량" class="tip icon-question-sign"></i>
			총 주문수량：<strong><?php echo $output['statcount_arr']['ordernum'];?></strong>
		</span>
    </li>
    </ul>
    <div style="clear:both;"></div>
</div>

<div id="stat_tabs" class="ui-tabs" style="min-height:500px;padding-top:10px;">
	<div class="tabmenu">
      	<ul class="tab pngFix">
      		<li><a href="#orderamount_div" nc_type="showdata" data-param='{"type":"orderamount"}'>주문금액</a></li>
        	<li><a href="#ordernum_div" nc_type="showdata" data-param='{"type":"ordernum"}'>주문량</a></li>
        </ul>
    </div>
    <!-- 下单金额 -->
    <div id="orderamount_div" style="width:930px;"></div>
    <!-- 下单量 -->
    <div id="ordernum_div" style="width:930px;"></div>
</div>

<div id="statlist" class=""></div>

<script charset="utf-8" type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/ui.core.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/ui.tabs.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/statistics.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>

<script type="text/javascript">
//展示搜索时间框
function show_searchtime(){
	s_type = $("#search_type").val();
	$("[id^='searchtype_']").hide();
	$("#searchtype_"+s_type).show();
}

$(function(){
	//Ajax提示
    $('.tip').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'top',
        offsetY: 5,
        allowTipHover: false
    });
    
	//切换登录卡
	$('#stat_tabs').tabs();
	
	//统计数据类型
	var s_type = $("#search_type").val();
	$('#search_time').datepicker({dateFormat: 'yy-mm-dd'});

	show_searchtime();
	$("#search_type").change(function(){
		show_searchtime();
	});
	
	//更新周数组
	$("[name='searchweek_month']").change(function(){
		var year = $("[name='searchweek_year']").val();
		var month = $("[name='searchweek_month']").val();
		$("[name='searchweek_week']").html('');
		$.getJSON('index.php?act=index&op=getweekofmonth',{y:year,m:month},function(data){
	        if(data != null){
	        	for(var i = 0; i < data.length; i++) {
	        		$("[name='searchweek_week']").append('<option value="'+data[i].key+'">'+data[i].val+'</option>');
			    }
	        }
	    });
	});

	$('#ordernum_div').highcharts(<?php echo $output['stat_json']['ordernum'];?>);
	$('#orderamount_div').highcharts(<?php echo $output['stat_json']['orderamount'];?>);
	
	$('#statlist').load('index.php?act=statistics_sale&op=salelist&t=<?php echo $output['searchtime'];?>&order_type=<?php echo $_REQUEST['order_type'];?>');
});
</script>