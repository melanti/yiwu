var Q = require('q');

module.exports = {
    escape: function(str) {
      return str.replace(/[\'\;]/, "");
    },
    queryOnPromise: function( qry, qry_val_arr ){
        //return console.log(qry);

        return Q.denodeify(DB.query)(qry, qry_val_arr);
    },

    queryOnPromiseV2: function( qry_mapper, qry_params ) {
        var deferred = Q.defer();
        var sqlMapper = MybatisService.getSqlMapper(qry_mapper, qry_params);

        sails.log.debug('sql:::', sqlMapper.sql);
        sails.log.debug('params:::', sqlMapper.parametros);

        return Q.denodeify(DB.query)(sqlMapper.sql, sqlMapper.parametros);
    }
};
