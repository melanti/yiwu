<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * 系统内部信息
 */
$lang['album_parameter_error']			= "오류";
$lang['album_class_save_succeed']		= "앨범 생성 성공";
$lang['album_class_save_lose']			= "앨범 생성 실패";
$lang['album_class_edit_succeed']		= "앨범 수정 성공";
$lang['album_class_edit_lose']			= "앨범 수정 성공";
$lang['album_class_file_del_succeed']	= "앨범 삭제성공";
$lang['album_class_file_del_lose']		= "앨범삭제 실패 ";
$lang['album_class_pic_del_succeed']	= "이미지 삭제 성공";
$lang['album_class_pic_del_lose']		= "이미지 삭제 실패";
$lang['album_class_pic_move_succeed']	= "이미지 이동 성공";
$lang['album_class_pic_move_lose']		= "이미지 이동 실패";
$lang['album_class_setting_wm']			= "워터마크 설정부터 하셔야됩니다.";
$lang['album_pic_plus_wm_succeed']		= "워터마크 추가 성공";
$lang['album_default_album']			= "기본앨범";
$lang['album_replace_same_type']		= "교체 업로드시 기존 이미지 확장명과 동일하셔야됩니다.";
/**
 * 用户中心页面
 */
$lang['album_batch_upload']				= "일괄업로드";
$lang['album_sort']						= "정렬방식";
$lang['album_batch_upload_description'] = "Jpg、Gif、Png로된 확장명만 가능합니다.";
$lang['album_batch_upload_description_1'] = "의 이미지 업로드; 다수 이미지 확인시 ctrl키 혹은 shift로 선택하시면 됩니다.";
$lang['album_class_list_img_upload']		= "이미지 업로드";
$lang['album_class_list_sel_img_class']		= "앨범선택";

$lang['album_sort_time_desc']				= "생성시간 올림순";
$lang['album_sort_time_asc']				= "생성시간 내림순";
$lang['album_sort_class_name_desc']			= "앨범명 내림순";
$lang['album_sort_class_name_asc']			= "앨범명 올림순";
$lang['album_sort_desc']					= "정렬 올림순";
$lang['album_sort_asc']						= "정렬 내림순";
$lang['album_class_pic_altogether']			= "총";
$lang['album_class_pic_folio']				= "장";
$lang['album_class_edit']					= "수정";
$lang['album_class_delete']					= "삭제";
$lang['album_class_delete_confirm_message']	= "앨범을 삭제하시겠습니까?\\n주의：앨범내의 이미지는 기본 앨범으로 이동됩니다.";

$lang['album_plist_batch_processing']		= "일괄처리";
$lang['album_plist_cancel_batch_processing']= "일괄처리 취소";
$lang['album_plist_check_all']				= "전체선택";
$lang['album_plist_cancel']					= "취소";
$lang['album_plist_inverse']				= "반전선택";
$lang['album_plist_add_watermark']			= "워터마크 추가";
$lang['album_sort_upload_time_desc']		= "업로드 시간 내림순";
$lang['album_sort_upload_time_asc']			= "업로드 시간 올림순";
$lang['album_sort_img_size_desc']			= "사이즈 내림순";
$lang['album_sort_img_siza_asc']			= "사이즈 올림순";
$lang['album_sort_img_name_desc']			= "이름 내림순";
$lang['album_sort_img_name_asc']			= "이름 올림순";
$lang['album_plist_upload_time']			= "업로드 시간";
$lang['album_plist_original_size']			= "원본 사이즈";
$lang['album_plist_small_size']				= "작은 사이즈";
$lang['album_plist_set_to_cover']			= "봉면설정";
$lang['album_plist_delete_confirm_message'] = "정말 삭제하시겠습니까?\\n주의：사용중인 이미까지 같이 삭제됩니다.";
$lang['album_plist_delete_img']				= "이미지삭제";
$lang['album_plist_operation_accessed']		= "조작성공";
$lang['album_plist_not_set_same_image']		= "이미 봉면으로 설정된 이미지입니다.";
$lang['album_plist_select_img']				= "이미지를 선택하세요";
$lang['album_plist_double_click_edit']		= "이름을 더블 클릭하여 수정할 수 있습니다.";
$lang['album_plist_replace_same_type']		= "주의：교체 업로드시 기존 이미지 확장명과 동일하셔야됩니다.";

$lang['album_plist_replace_upload']			= "교체업로드";
$lang['album_plist_submit']					= "확인";

$lang['album_plist_move_album']				= "이미지 이동";
$lang['album_plist_move_album_change']		= "앨범선택";
$lang['album_plist_move_album_begin']		= "이관 시작";
$lang['album_plist_move_album_only_one']	= "앨범이 하나네요, 빨리";
$lang['album_plist_move_album_only_two']	= "가서 추가하세요!";

$lang['album_upload_complete_one']			= "";
$lang['album_upload_complete_two']			= "장 업로드 완료";
$lang['album_upload_file_tips']				= "이미지 업로드 제한이 만기되면 미니샵 등급을 승급하셔야됩니다.";

$lang['album_pinfo_img_name']					= "이미지 이름";
$lang['album_pinfo_img_attribute']				= "이미지 속정";
$lang['album_pinfo_upload_time']				= "업로드 시간";
$lang['album_pinfo_class_name']					= "앨범명";
$lang['album_pinfo_original_size']				= "원본 크기";
$lang['album_pinfo_original_spec']				= "원본 사이즈";
$lang['album_pinfo_see_src']					= "원본";
$lang['album_pinfo_see_max']					= "대";
$lang['album_pinfo_see_mid']					= "중";
$lang['album_pinfo_see_small']					= "소";
$lang['album_pinfo_see_tiny']					= "미니";
$lang['album_pinfo_see']						= "보기";
$lang['album_pinfo_photos_switch_effect']		= "이미지를 선택하여 확인하세요";
$lang['album_pinfo_level_slip_into']			= "가로";
$lang['album_pinfo_vertical_slip_into']			= "세로";
$lang['album_pinfo_contraction_amplification']	= "축소/확대";
$lang['album_pinfo_crossfade']					= "그라데이션";
$lang['album_pinfo_null']						= "무";
$lang['album_pinfo_original_copy_succeed']		= "이미지 주소를 클립보드에 복사하였습니다.";
$lang['album_pinfo_small_copy_succeed']			= "섬네일 주소를 클립보드에 복사하였습니다.";
$lang['album_pinfo_autoplay']					= "Auto";
$lang['album_pinfo_suspend']					= "Pass";

$lang['album_class_add']				= "앨범생성";
$lang['album_class_deit']				= "앨범수정";
$lang['album_class_add_name']			= "앨범명";
$lang['album_class_add_des']			= "설명";
$lang['album_class_add_sort']			= "정렬";
$lang['album_class_add_submit']			= "확인";
$lang['album_class_add_name_null']		= "앨범명을 입력하세요";
$lang['album_class_add_name_max']		= "앨범명은 반드시 20자 이내로 입력하세요";
$lang['album_class_add_name_repeat']	= "앨범명이 존재합니다.";
$lang['album_class_add_des_max']		= "설명은 반드시 100자 이내로 입력하세요";
$lang['album_class_add_sort_digits']	= "정렬은 반드시 숫자로 입력하세요";
$lang['album_class_add_max_20']			= "최대 20개 앨범만 추가할 수 있습니다.";
$lang['album_class_save_max_20']		= "최대 20개 앨범만 추가할 수 있습니다, 앨범 저장 실패!";

$lang['store_goods_album_goods_pic']			= '상품이미지';
$lang['store_goods_album_select_from_album']	= '앨범에서 선택';
$lang['store_goods_album_users']				= '회원앨범';
$lang['store_goods_album_all_photo']			= '전체이미지';
?>