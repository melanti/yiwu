
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.acc.org.list.ctrl.accOrgListBodyCtrl');
  APP.modules.org.list.ctrl.accOrgListBodyCtrl = accOrgListBodyCtrl;

  function accOrgListBodyCtrl( $scope, _, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL, $sce ){
    var ctrl = this;
    ctrl.imgUrl = APP.info.imgURL;
    ctrl.gVersion = APP.info.gVersion;
    ctrl.getTotal;
    ctrl.getCount;
    ctrl.getStart;
    ctrl.getResults;
    ctrl.renderRegistSection = _renderRegistSection;
    ctrl.modificationAccOrg = _modificationAccOrg;
    ctrl.isStore = APP.info.user.parent_org_id !== APP.info.user.org_id;

    _init();

    function _init(){
      $scope.$on('$destroy', function () {
      });
    }

    function _renderRegistSection(__time){
      return COMMON_UTIL.getLocalTimezoneStr(__time, '.');
    }
    function _modificationAccOrg(__data){
      //TODO 권한에따라 조회 or 수정
      var tmpURL = APP.info.gURL + '/opr_mng/acc/org/' + ((APP.info.user.auth.organizationAccount == 2) ? 'modification' : 'confirmation')+'?maxage=0'
      COMMON_UTIL
        .reqForm(
          tmpURL,
          'post',
          {
            account_uuid: __data.account_uuid,
            account_no: __data.account_no,
            org_id: __data.org_id,
            org_type: __data.org_type
          }
        );
    }

  }
  accOrgListBodyCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL', '$sce'];
  accOrgListBodyCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableBodyCtrl:true});
      APP.modules.directive.util
        .checkBoxHelper(scope, elem, attrs, ctrl, 'tableBodyCtrl.getResults()');
    }
  };
  accOrgListBodyCtrl.bindToController= {
    getTotal:'&',
    getStart:'&',
    getCount:'&',
    getResults:'&'
  };

})(window.jQuery, window.angular, window._, window.APP);
