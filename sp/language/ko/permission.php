<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['menu_setting'] = '설정';
$lang['menu_goods'] = '상품';
$lang['menu_store'] = '미니샵';
$lang['menu_member'] = '회원';
$lang['menu_trade'] = '거래';
$lang['menu_website'] = '웹 사이트';
$lang['menu_extend'] = '확장';
$lang['menu_base_setting'] = '네트워크 설정';
$lang['menu_area_setting'] = '지역설정';
$lang['menu_payment_setting'] = '결제방식';
$lang['menu_theme_setting'] = '주제설정';
$lang['menu_template_setting'] = '탬플릿 편집';
$lang['menu_mailtemplate_setting'] = '탬플릿 알림';
$lang['menu_category_goods'] = '분류 관리';
$lang['menu_brand_goods'] = '브랜드 관리';
$lang['menu_list_goods'] = '상품 관리';
$lang['menu_recommend_goods'] = '추천유형';
$lang['menu_grade_store'] = '미니샵 클래스';
$lang['menu_category_store'] = '미니샵 분류';
$lang['menu_list_store'] = '미니샵관리';
$lang['menu_list_member'] = '회원관리';
$lang['menu_admin_member'] = '관리자 관리';
$lang['menu_notice_member'] = '회원통지';
$lang['menu_list_trade'] = '주문관리';
$lang['menu_news_category_website'] = '문장분류';
$lang['menu_news_list_website'] = '게시글관리';
$lang['menu_partner_website'] = '파트너';
$lang['menu_navigation_website'] = '페이지네비게이션';
$lang['menu_db_website'] = '데이터베이스';
$lang['menu_groupbuy_website'] = '단체구매 관리';
$lang['menu_consulting_website'] = '컨설팅';
$lang['menu_share_link_website'] = '공유링크';
$lang['menu_system_set'] = '시스템설정';
$lang['menu_base_information'] = '기본정보';
$lang['menu_email'] = 'email';
$lang['menu_verify'] = '확인코드';
$lang['menu_store_setting'] = '미니샵설정';
$lang['menu_credit'] = '신용평가';
$lang['menu_subdomain'] = '서브도메인';


