(function ($, angular, _, APP) {

  APP.modules.ctrl.categoryListCtrl = categoryListCtrl;

  //categoryListCtrl
  function categoryListCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO, CategoryManager){
    var vm = this,
      categoryManager = CategoryManager.exports;

    vm.getResults;
    vm.getFilteredResults;
    vm.setFilteredResults;
    vm.getDpt01ItemsCount;
    vm.setCurRow;
    vm.setShowCateInfo;
    vm.updateCategoryDepth;
    vm.stopUpdateCategoryDepth;

    _init();

    function _init(){
      //console.log('categoryListCtrl _init');
    }

  }

})(window.jQuery, window.angular, window._, window.APP);
