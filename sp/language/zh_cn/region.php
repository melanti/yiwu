<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['region_index_modify_succ']	= '지역 데이터 수정 성공';
$lang['region_index_config']		= '지역보기';
$lang['region_index_export']		= '불러내기';
$lang['region_index_import']		= '불러오기';
$lang['region_index_import_tip']	= '기존 데이터를 지우신뒤 불러오기를 진행해주세요\n정말 불러오시겠습니까?';
$lang['region_index_import_region']	= '전지역데이터 불러오기【3급】';
$lang['region_index_choose_region']	= '다음지역 보기';
$lang['region_index_province']		= '성';
$lang['region_index_city']			= '도시';
$lang['region_index_county']		= '현';
$lang['region_index_name']			= '지역이름';
$lang['region_index_help2']			= '추가/삭제/수정후 저장 버튼을 눌러야만 적용됩니다.';
$lang['region_index_del_tip']		= '내용 삭제시 하위 내용까지 삭제됩니다, 계속 진행하시겠습니까?';
$lang['region_index_add']			= '새지역추가';
/**
 * 불러오기
 */
$lang['region_import_succ']				= '불러오기 성공';
$lang['region_import_csv_null']			= '불러오실 파일이 존재하지 않습니다.';
$lang['region_import_choose_file']		= '파일을 선택하세요';
$lang['region_import_file_tip']			= '물러올때 속도가 드리면 파일을 여러개로 분리해서 올려주세요.';
$lang['region_import_choose_code']		= '파일번호을 선택하세요';

$lang['region_import_file_format']		= '파일양식';
$lang['region_import_first']			= '1급지역';
$lang['region_import_second']			= '2급지역';
$lang['region_import_third']			= '3급지역';
$lang['region_import_example_download']	= '샘플파일 다운로드';
$lang['region_import_download_tip']		= '불러오기 샘플파일 다운로드';
$lang['region_import_help3']			= '파일이 클때 파일코드를 UTF-8로 교체후 다시 업로드해주세요, 그러면 시간이 많이 줄어듭니다.';
/**
 * 불러내기
 */
$lang['region_export_trans']			= '지역 카테고리 데이터를 불러내시겠습니까?';
$lang['region_export_trans_tip']		= '불러내기 내용은 .CSV파일입니다.';