<?php
/**
 * 积分兑换주문관리
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class pointorderControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('pointorder');
	}
	/**
	 * 积分兑换리스트
	 */
	public function pointorder_listOp(){
	    $model_pointorder = Model('pointorder');
	    //获取兑换주문상태
	    $pointorderstate_arr = $model_pointorder->getPointOrderStateBySign();
		$where = array();
		//兑换单号
		$pordersn = trim($_GET['pordersn']);
		if ($pordersn){
			$where['point_ordersn'] = array('like',"%{$pordersn}%");
		}
		//兑换회원아이디
		$pbuyname = trim($_GET['pbuyname']);
		if (trim($_GET['pbuyname'])){
			$where['point_buyername'] = array('like',"%{$pbuyname}%");
		}
		//주문상태
		if (trim($_GET['porderstate'])){
			$where['point_orderstate'] = $pointorderstate_arr[$_GET['porderstate']][0];
		}
		//검색兑换주문리스트
		$order_list = $model_pointorder->getPointOrderList($where, '*', 10, 0, 'point_orderid desc');
		
		//信息输出
		Tpl::output('pointorderstate_arr',$pointorderstate_arr);
		Tpl::output('order_list',$order_list);
		Tpl::output('show_page',$model_pointorder->showpage());
		Tpl::showpage('pointorder.list');
	}
	
	/**
	 * 삭제兑换주문정보
	 */
	public function order_dropOp(){
	    $data = Model('pointorder')->delPointOrderByOrderID($_GET['order_id']);
	    if ($data['state']){
	        showDialog(L('admin_pointorder_del_success'),'index.php?act=pointorder&op=pointorder_list','succ');
	    } else {
	        showDialog($data['msg'],'index.php?act=pointorder&op=pointorder_list','error');
	    }
	}
	
	/**
	 * 취소兑换
	 */
	public function order_cancelOp(){
	    $model_pointorder = Model('pointorder');
	    //주문취소
	    $data = $model_pointorder->cancelPointOrder($_GET['id']);
	    if ($data['state']){
	        showDialog(L('admin_pointorder_cancel_success'),'index.php?act=pointorder&op=pointorder_list','succ');
	    }else {
	        showDialog($data['msg'],'index.php?act=pointorder&op=pointorder_list','error');
	    }
	}
	
	/**
	 * 发货
	 */
	public function order_shipOp(){
		$order_id = intval($_GET['id']);
		if ($order_id <= 0){
			showDialog(L('admin_pointorder_parameter_error'),'index.php?act=pointorder&op=pointorder_list','error');
		}
		$model_pointorder = Model('pointorder');
		//获取주문상태
		$pointorderstate_arr = $model_pointorder->getPointOrderStateBySign();
		 
		//검색주문정보
		$where = array();
		$where['point_orderid'] = $order_id;
		$where['point_orderstate'] = array('in',array($pointorderstate_arr['waitship'][0],$pointorderstate_arr['waitreceiving'][0]));//배송대기和已经发货상태
		$order_info = $model_pointorder->getPointOrderInfo($where);
		if (!$order_info){
		    showDialog(L('admin_pointorderd_record_error'),'index.php?act=pointorder&op=pointorder_list','error');
		}
		if (chksubmit()){
			$obj_validate = new Validate();
			$validate_arr[] = array("input"=>$_POST["shippingcode"],"require"=>"true","message"=>L('admin_pointorder_ship_code_nullerror'));
			$obj_validate->validateparam = $validate_arr;
			$error = $obj_validate->validate();
			if ($error != ''){
				showDialog(L('error').$error,'index.php?act=pointorder&op=pointorder_list','error');
			}
			//发货
			$data = $model_pointorder->shippingPointOrder($order_id, $_POST, $order_info);
			if ($data['state']){
				showDialog('배송 수정 성공','index.php?act=pointorder&op=pointorder_list','succ');
			}else {
				showDialog($data['msg'],'index.php?act=pointorder&op=pointorder_list','error');
			}
		} else {
		    $express_list = Model('express')->getExpressList();
			Tpl::output('express_list',$express_list);
		    Tpl::output('order_info',$order_info);
			Tpl::showpage('pointorder.ship');
		}
	}
	/**
	 * 兑换信息详细
	 */
	public function order_infoOp(){
		$order_id = intval($_GET['order_id']);
		if ($order_id <= 0){
			showDialog(L('admin_pointorder_parameter_error'),'index.php?act=pointorder&op=pointorder_list','error');
		}
		//검색주문정보
		$model_pointorder = Model('pointorder');
		$order_info = $model_pointorder->getPointOrderInfo(array('point_orderid'=>$order_id));
		if (!$order_info){
			showDialog(L('admin_pointorderd_record_error'),'index.php?act=pointorder&op=pointorder_list','error');
		}
		$orderstate_arr = $model_pointorder->getPointOrderState($order_info['point_orderstate']);
		$order_info['point_orderstatetext'] = $orderstate_arr[1];
		
		//검색兑换주문수취인地址
		$orderaddress_info = $model_pointorder->getPointOrderAddressInfo(array('point_orderid'=>$order_id));
		Tpl::output('orderaddress_info',$orderaddress_info);
		
		//兑换상품信息
		$prod_list = $model_pointorder->getPointOrderGoodsList(array('point_orderid'=>$order_id));
		Tpl::output('prod_list',$prod_list);
		
		//物流公司信息
		if ($order_info['point_shipping_ecode'] != ''){
		    $data = Model('express')->getExpressInfoByECode($order_info['point_shipping_ecode']);
		    if ($data['state']){
		        $express_info = $data['data']['express_info'];
		    }
		    Tpl::output('express_info',$express_info);
		}
		
		Tpl::output('order_info',$order_info);
		Tpl::showpage('pointorder.info');
	}
}
