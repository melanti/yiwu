<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>업체도움</h3>
      <ul class="tab-base">
        <li><a href="index.php?act=help_store&op=help_store"><span><?php echo '내용';?></span></a></li>
        <li><a href="index.php?act=help_store&op=help_type"><span><?php echo '도움유형';?></span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span><?php echo '내용추가';?></span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="post_form" method="post" name="form1">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
      	<tr class="noborder">
          <td colspan="2" class="required"><label class="validation" for="help_title">제목:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input id="help_title" name="help_title" value="" class="txt" type="text"></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="type_id">도움유형:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><select name="type_id" id="type_id">
              <option value=""><?php echo $lang['nc_please_choose'];?>...</option>
              <?php if(!empty($output['type_list']) && is_array($output['type_list'])){ ?>
              <?php foreach($output['type_list'] as $key => $val){ ?>
              <option value="<?php echo $val['type_id'];?>"><?php echo $val['type_name'];?></option>
              <?php } ?>
              <?php } ?>
            </select></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="help_sort"><?php echo $lang['nc_sort'];?>:</label>
            </td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="255" name="help_sort" id="help_sort" class="txt"></td>
          <td class="vatop tips">0~255이내의 숫자를 입력하세요, 작은 숫자로 정렬됩니다.</td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label for="help_url">링크주소:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="" name="help_url" id="help_url" class="txt"></td>
          <td class="vatop tips">링크주소 입력후 제목을 클릭시 내용없이 바로 해당 링크로 이동합니다, 링크양식은 HTTP://로 시작해주세요.</td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation">내용:</label></td>
        </tr>
        <tr class="noborder">
          <td colspan="2" class="vatop rowform"><?php showEditor('content');?></td>
        </tr>
        <tr>
          <td colspan="2" class="required">이미지 업로드:</td>
        </tr>
        <tr class="noborder">
          <td colspan="3" id="divComUploadContainer"><input type="file" multiple="multiple" id="fileupload" name="fileupload" /></td>
        </tr>
        <tr>
          <td colspan="2" class="required">보유이미지:</td>
        <tr>
          <td colspan="2">
            <ul id="thumbnails" class="thumblists">
              <?php if(!empty($output['pic_list']) && is_array($output['pic_list'])){?>
              <?php foreach($output['pic_list'] as $key => $val){ ?>
              <li id="pic_<?php echo $val['upload_id'];?>" class="picture" >
                <input type="hidden" name="file_id[]" value="<?php echo $val['upload_id'];?>" />
                <div class="size-64x64"><span class="thumb"><i></i>
                    <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_ARTICLE.'/'.$val['file_name'];?>" onload="javascript:DrawImage(this,64,64);"/></span></div>
                <p><span><a href="javascript:insert_editor('<?php echo $val['file_name'];?>');">삽입</a></span><span><a href="javascript:del_file_upload('<?php echo $val['upload_id'];?>');"><?php echo $lang['nc_del'];?></a></span></p>
              </li>
              <?php } ?>
              <?php } ?>
            </ul>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15" ><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script>
var UPLOAD_ARTICLE_URL = "<?php echo UPLOAD_SITE_URL.'/'.ATTACH_ARTICLE.'/'; ?>";
//按钮先执行验证再提交表单
$(function(){
	$("#submitBtn").click(function(){
        if($("#post_form").valid()){
            $("#post_form").submit();
    	}
	});
	$("#post_form").validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        rules : {
            help_title : {
                required : true
            },
            type_id : {
                required : true
            },
            help_sort : {
                required : true,
                digits   : true
            },
			help_url : {
				url : true
            },
			content : {
                required   : true
            }
        },
        messages : {
            help_title : {
                required : "유형명을 입력하세요"
            },
            type_id : {
                required : "도움유형을 선택하세요"
            },
            help_sort  : {
                required : "정렬은 반드시 숫자로 입력하세요",
                digits   : "정렬은 반드시 숫자로 입력하세요"
            },
            help_url : {
                url : "정확한 링크주소를 입력하세요"
            },
            content : {
                required : "내용을 입력하세요"
            }
        }
	});
    // 이미지 업로드
    $('#fileupload').each(function(){
        $(this).fileupload({
            dataType: 'json',
            url: 'index.php?act=help_store&op=upload_pic&item_id=0',
            done: function (e,data) {
                if(data != 'error'){
                	add_uploadedfile(data.result);
                }
            }
        });
    });
});

function add_uploadedfile(file){
    var newImg = '<li id="pic_' + file.file_id + '" class="picture"><input type="hidden" name="file_id[]" value="' + file.file_id
        + '" /><div class="size-64x64"><span class="thumb"><i></i><img src="'+UPLOAD_ARTICLE_URL
        + file.file_name + '" width="64px" height="64px"/></span></div><p><span><a href="javascript:insert_editor(\'' + file.file_name +
        '\');">삽입</a></span><span><a href="javascript:del_file_upload(' + file.file_id + ');"><?php echo $lang['nc_del'];?></a></span></p></li>';
    $('#thumbnails').prepend(newImg);
}
function insert_editor(file_name){
	KE.appendHtml('content', '<img src="'+UPLOAD_ARTICLE_URL+ file_name + '">');
}
function del_file_upload(file_id){
    if(!window.confirm('<?php echo $lang['nc_ensure_del'];?>')){
        return;
    }
    $.getJSON('index.php?act=help_store&op=del_pic&file_id=' + file_id, function(result){
        if(result){
            $('#pic_' + file_id).remove();
        }
    });
}
</script>
