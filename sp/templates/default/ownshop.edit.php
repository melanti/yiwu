<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>자영업 업체</h3>
      <ul class="tab-base">
        <li><a href="index.php?act=ownshop&op=list"><span>관리</span></a></li>
        <li><a href="index.php?act=ownshop&op=add"><span>추가</span></a></li>
        <li><a href="javascript:;" class="current"><span>수정</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>자영업 업체의 시동상태 여부를 수정할 수 있습니다.</li>
            <li>자영업 업체의 관리자 로그인 아이디를 수정할 수 있습니다.</li>
            <li>자영업 업체의 비밀번호 수정시 회원관리에서 검색후 수정하시길 바랍니다.</li>
            <li>"모든연동항목"을 "아니오"로 설정시 해당 모든 상품은 내림으로 자동 설정됩니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form id="store_form" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="store_id" value="<?php echo $output['store_array']['store_id']; ?>" />
    <table class="table tb-type2">
      <tbody>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="store_name">업체명:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="<?php echo $output['store_array']['store_name'];?>" id="store_name" name="store_name" class="txt" /></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label for="store_name">오픈시간:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><?php echo ($t = $output['store_array']['store_time'])?@date('Y-m-d',$t):'';?></td>
          <td class="vatop tips"></td>
        </tr>
        <tr class="noborder">
          <td colspan="2" class="required"><label>업체아이디:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><?php echo $output['store_array']['member_name'];?></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="seller_name">점장판매자계정:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="<?php echo $output['store_array']['seller_name'];?>" id="seller_name" name="seller_name" class="txt" /></td>
          <td class="vatop tips">업체 관리자 로그인시 필요한 아이디</td>
        </tr>
      </tbody>
      <tbody>
        <tr>
          <td colspan="2" class="required"><label>
            <label for="bind_all_gc">모든연동항목:</label>
            </label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform onoff">
            <label for="bind_all_gc1" class="cb-enable <?php if ($output['store_array']['bind_all_gc'] == '1'){ ?>selected<?php } ?>" ><span>예</span></label>
            <label for="bind_all_gc0" class="cb-disable <?php if($output['store_array']['bind_all_gc'] == '0'){ ?>selected<?php } ?>" ><span>아니오</span></label>
            <input id="bind_all_gc1" name="bind_all_gc" <?php if($output['store_array']['bind_all_gc'] == '1'){ ?>checked="checked"<?php } ?> value="1" type="radio">
            <input id="bind_all_gc0" name="bind_all_gc" <?php if($output['store_array']['bind_all_gc'] == '0'){ ?>checked="checked"<?php } ?> value="0" type="radio">
          </td>
          <td class="vatop tips"></td>
        </tr>
      </tbody>
      <tbody>
        <tr>
          <td colspan="2" class="required"><label>
            <label for="state">상태:</label>
            </label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform onoff"><label for="store_state1" class="cb-enable <?php if($output['store_array']['store_state'] == '1'){ ?>selected<?php } ?>" ><span><?php echo $lang['open'];?></span></label>
            <label for="store_state0" class="cb-disable <?php if($output['store_array']['store_state'] == '0'){ ?>selected<?php } ?>" ><span><?php echo $lang['close'];?></span></label>
            <input id="store_state1" name="store_state" <?php if($output['store_array']['store_state'] == '1'){ ?>checked="checked"<?php } ?> onclick="$('#tr_store_close_info').hide();" value="1" type="radio">
            <input id="store_state0" name="store_state" <?php if($output['store_array']['store_state'] == '0'){ ?>checked="checked"<?php } ?> onclick="$('#tr_store_close_info').show();" value="0" type="radio"></td>
          <td class="vatop tips"></td>
        </tr>
      </tbody>
      <tbody id="tr_store_close_info">
        <tr >
          <td colspan="2" class="required"><label for="store_close_info">닫힌원인:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><textarea name="store_close_info" rows="6" class="tarea" id="store_close_info"><?php echo $output['store_array']['store_close_info'];?></textarea></td>
          <td class="vatop tips"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15"><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript">
$(function(){

    $('input[name=store_state][value=<?php echo $output['store_array']['store_state'];?>]').trigger('click');

    //按钮先执行验证再提交表单
    $("#submitBtn").click(function(){
        if($("#store_form").valid()){
            $("#store_form").submit();
        }
    });

    $('#store_form').validate({
        errorPlacement: function(error, element){
            error.appendTo(element.parentsUntil('tr').parent().prev().find('td:first'));
        },
        rules : {
            store_name: {
                required : true,
                remote : '<?php echo urlAdmin('ownshop', 'ckeck_store_name', array('store_id' => $output['store_array']['store_id']))?>'
            },
            seller_name: {
                required : true,
                remote   : {
                    url : 'index.php?act=ownshop&op=check_seller_name&id=<?php echo $output['store_array']['store_id']; ?>',
                    type: 'get',
                    data:{
                        seller_name : function(){
                            return $('#seller_name').val();
                        }
                    }
                }
            }
        },
        messages : {
            store_name: {
                required: '업체명을 입력하세요',
                remote : '업체명이 존재합니다.'
            },
            seller_name: {
                required : '점장판매자 아이디를 입력하세요',
                remote   : '이미 존재하는 아이디입니다.'
            }
        }
    });
});
</script>
