// dependencies external library
var Q = require("q");
var fs = require('fs');
var unzip = require('unzip2');
var is = require("is_js");
var _ = require("underscore");
var request = require('supertest');
var should = require('should');
var crypto = require('crypto');
var SFTPClient = require('ssh2').Client;

// dependencies internal module(and service)
var lib_vc;
var UUID;
var debug;
var SQLService;
var FileService;
var ErrorHandler;
var ValidatorService;
var QueryService;
var CommonService;
var RequestService;

var testLoginReqData = {
    id:'test003',
    pw:'qwer1234',
    account_uuid:'11',
    secret:'',
    key:'',
    verify_code:'',
    show:'debug'
};

//goods_common_id:8(new) , goods_id:11(old)
var testReqData = {
    secret:'',
    account_uuid:'',
    key:'',
    verify_code:'',
    item_id:'11',
    td_ids:'73',
    td_rep_ids:'73',
    show:'debug'
};

startTest();

function startTest() {

    describe('=== assignModule ===\n', function () {
        it('before assignModule', function (done) {
            assignBasicModule();
            done();
        });
    });

    describe('=== TestController Test ===\n', function () {

        test_login();
        // test_001();
        test_attachTd();

        // test_sftp();
        // test_reduce_promise();
    });
}

function test_reduce_promise(){
    describe('===test_reduce_promise === \n', function () {
        it('test_reduce_promise', function(done){
            var funcs = [
                function (){
                    var deferred = Q.defer();
                    sails.log.debug('11');
                    deferred.resolve('1');
                    return deferred.promise;
                },
                function (){
                    var deferred = Q.defer();
                    sails.log.debug('22');
                    deferred.resolve('2');
                    return deferred.promise;
                },
                function (){
                    var deferred = Q.defer();
                    sails.log.debug('33');
                    deferred.resolve('3');
                    return deferred.promise;
                }

            ];

            funcs
                .reduce( Q.when, Q({}) )
                .then(function(){
                    sails.log.debug('ALL SETTLED');
                    done();
                });



        });
    });
}

function test_sftp(){
    describe('===sftp === \n', function () {
        it('sftp', function(done){

            var SFTP_INFO = {
                host: sails.config.ftp.host,
                port: sails.config.ftp.port,
                username: sails.config.ftp.username,
                password: sails.config.ftp.password,
                cwd: sails.config.ftp.cwd,
                upload_dir:'/DAT',
                readyTimeout:200000
            };

            var td_cd = 'KidCloTop001000001'+'';
            var td_cd = 'testsjh'+'';
            var mkdir_path = '/'+SFTP_INFO.cwd+SFTP_INFO.upload_dir+'/'+td_cd;
            var td_ctx = {};

            sails.log.debug('mkdir_path: ' ,mkdir_path);

            Q.fcall(_connectSFTPServer)
                .then(_assignConnAndSFTP)
                .then(_openDirectory)
                .then(_checkDirectory)
                // .then(_createDirectory)
                // .then(_uploadTdFiles)
                .spread(_allSetttled)
                .catch(function(e){
                    console.log('fail::: FileServices.uploadThreedFiles ER_FILE_UPLOAD: ' , e);
                    _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );
                    done();

                });


            function _connectSFTPServer(){
                return _connectSFTP(SFTP_INFO, _destroyConnectSFTP);
            }

            function _assignConnAndSFTP(results){
                //예외처리때 destroy하기위한  connect, sftp instance의 mutable state
                td_ctx = results;
                return Q.all([results]);
            }

            function _openDirectory(results){
                console.log('_readDirectory: ');
                return opendir(results[0].connection, results[0].sftp, mkdir_path);
            }

            function _checkDirectory(results){
                if(results.hasDir){
                    if(results.dirData && results.dirData !==false && results.dirData.length>0){
                        console.log('dir exist && file exist:: file remove, dir remove');

                        unlinkFiles(
                            results.connection,
                            results.sftp,
                            _.map(results.dirData, function(v,k){return mkdir_path+'/'+v.filename;}))
                            .then(
                                function(rel){
                                    console.log('unlinkFiles: COMPLETE', rel);
                                    return _removeDirectory(results);
                                },
                                function(err){
                                    console.log('unlinkFiles: FAIl', err);
                                    throw new Error('unlinkFiles Error');
                                }
                            );



                    }else{
                        console.log('dir exist && file empty:: only dir remove');
                        return _removeDirectory(results);
                    }


                }else{
                    console.log('dir empty && file empty:: ALL create');
                    return results;
                }
            }

            function _createDirectory(results){
                console.log('_createDirectory: ');
                return _createDir(results.connection, results.sftp, mkdir_path);
            }

            function _removeDirectory(results) {
                console.log('_removeDirectory: ');
                return removedir(results.connection, results.sftp, mkdir_path);
            }


            function _uploadTdFiles(results){
                /*
                    [
                    ...
                     { filename: 'preview90.jpg',
                     longname: '-rw-rw-r--    1 www      www         42025 Aug 17 11:24 preview90.jpg',
                     attrs:
                     { mode: 33204,
                     permissions: 33204,
                     uid: 1001,
                     gid: 1001,
                     size: 42025,
                     atime: 1471437659,
                     mtime: 1471433066 } }
                    ...
                    ]
                */


                var uploadFilePromises = [];
                for(var i=0, iTotal = source.upload_req_list.length; i<iTotal; ++i){
                    uploadFilePromises.push(_createFileStream(
                        results.connection,
                        results.sftp,
                        source.upload_req_list[i],
                        mkdir_path+'/'+source.upload_req_file[i]
                    ));
                }
                return Q.all(uploadFilePromises);
            }

            function _allSetttled(){
                console.log('uploadThreedFiles _allSetttled: ' );
                _destroyConnectSFTP( td_ctx.connection, td_ctx.sftp );
                done();
            }

            function _destroyConnectSFTP(c, f){
                try{
                    if(f) f.end();
                    if(c){
                        c.end();
                        c.destroy();
                    }
                    c = null;
                    f = null;
                }catch(e){

                }
            }


            function _connectSFTP(conn_info, destroy_fnc){
                var deferred = Q.defer();

                var conn = new SFTPClient();
                var sftp;

                conn.on('ready', function(){
                    conn.sftp(function(err, ftp) {
                        if (err) {
                            console.log('err SFTP_CONNECTION sftp: ' , err);
                            destroy_fnc(conn, sftp);
                            deferred.reject(err);
                            return false;
                        }
                        sftp = ftp;
                        deferred.resolve({connection:conn, sftp:ftp});
                    });
                });

                conn.on('error', function(err){
                    if (err) {
                        console.log('err SFTP_CONNECTION : ' , err);
                        destroy_fnc(conn, sftp);
                        deferred.reject(err);
                    }
                });

                conn.on('end', function(){
                    console.log('end conn : ' );
                    destroy_fnc(conn, sftp);
                });

                conn.connect(conn_info);

                return deferred.promise;
            }

            function _destroyConnectSFTP(c, f){
                try{
                    if(f) f.end();
                    if(c){
                        c.end();
                        c.destroy();
                    }
                    c = null;
                    f = null;
                }catch(e){

                }
            }

            function _createDir(conn, sftp, dir){
                var d = Q.defer();
                try{
                    sftp.mkdir(dir, function(err){
                        if(err){
                            //throw err;
                            console.log('fail, _createDir: ' , err);
                            d.reject(err);

                        }else{
                            console.log('success, _createDir');
                            d.resolve({connection:conn, sftp:sftp, dir:dir});
                        }

                    });

                }catch(e){
                    d.reject(e);
                }

                return d.promise;
            }

            function opendir(conn, sftp, dir){
                var d = Q.defer();

                try{
                    sftp.opendir(dir, function(err, data){
                        if(err){
                            //throw err;
                            console.log('fail, opendir: ' , err);
                            d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:[], hasDir:false});
                        }else{
                            console.log('success, opendir: ', data);


                            sftp.readdir(data, function(err, list) {
                                if(err){
                                    //throw err;
                                    console.log('fail, readdir: ' , err);
                                    d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:[], hasDir:true});
                                }else{
                                    console.log('success, readdir: ', list);
                                    d.resolve({connection:conn, sftp:sftp, dir:dir, dirData:list, hasDir:true});
                                }
                            });


                        }

                    });

                }catch(e){
                    d.reject(e);
                }

                return d.promise;
            }

            function removedir(conn, sftp, dir){
                var d = Q.defer();
                try{
                    sftp.rmdir(dir, function(err, list) {
                        if(err){
                            //throw err;
                            console.log('fail, rmdir: ' , err);
                            d.resolve({connection:conn, sftp:sftp, dir:dir});
                        }else{
                            console.log('success, rmdir: ', list);
                            d.resolve({connection:conn, sftp:sftp, dir:dir});
                        }
                    });
                }catch(e){
                    d.reject(e);
                }
                return d.promise;
            }

            function unlinkFile(conn, sftp, dir){
                var d = Q.defer();
                try{
                    sftp.unlink(dir, function(err, list) {
                        if(err){
                            //throw err;
                            console.log('fail, unlink: ' , err);
                            d.resolve({connection:conn, sftp:sftp, dir:dir});
                        }else{
                            console.log('success, unlink: ', list);
                            d.resolve({connection:conn, sftp:sftp, dir:dir});
                        }
                    });
                }catch(e){
                    d.reject(e);
                }
                return d.promise;
            }

            function unlinkFiles(conn, sftp, filesNameArr){
                var tmp_promises;
                tmp_promises = _.map(
                    filesNameArr,
                    function(v, k){
                        return unlinkFile(conn, sftp, v);
                    }
                );

                return Q.all(tmp_promises);
            }

        });
    });
}

function test_login() {
    describe('===LOGIN === \n', function () {
        it('login', function(done){
            verify(
                testLoginReqData.id,
                function(res){
                    testLoginReqData.key = res.key;
                    testLoginReqData.verify_code = res.code;
                }
            ).then(function(){

                CommonService.reqTestAPI({
                    r: request,
                    url: RequestService.API_URL.CAT_LOGIN,
                    method: 'post',
                    expectStatus: 200,
                    sendData: {
                        id: testLoginReqData.id,
                        password: testLoginReqData.pw,
                        verify_code: testLoginReqData.verify_code,
                        service: 'cat',
                        ip_addr: '127.0.0.1',
                        show: 'debug'
                    },
                    sailsSid: CommonService.getTestSid(),
                    errLogTitle: "C.2.2 login fail",
                    assertCallback: function (res) {
                        sails.log.debug('CAT_LOGIN: ',res);

                        testLoginReqData.secret = res.results[0].secret;
                        testLoginReqData.account_uuid = res.results[0].account_uuid;

                        testReqData.secret = res.results[0].secret;
                        testReqData.account_uuid = res.results[0].account_uuid;
                    },
                    done: done
                });

            });

        });
    });
}


function test_attachTd() {
    describe('=== ATTACH_TD === \n', function () {
        it('ATTACH_TD', function(done){
            // testReqData = {
            //     secret:'',
            //     account_uuid:'',
            //     key:'',
            //     verify_code:'',
            //     item_id:'11',
            //     td_ids:'73',
            //     td_rep_ids:'73',
            //     show:'debug'
            // };

            // delete testReqData.item_id;
            // testReqData.item_id='123123';

            var ATATCH_TASK_MODEL = {
                ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY,
                params:testReqData,
                data:{
                    goods_id:'',
                    goods_commonid:'',
                    goods_name:'',

                    store_id:'',
                    store_name:'',

                    brand_id:'',
                    brand_name:''
                }
            };

            verify(testReqData.account_uuid)
                .then(
                    _taskSideEffect(function(res){
                        sails.log.debug('SideEffect 01: ' , res);

                        ATATCH_TASK_MODEL.params.key = res[0].key;
                        ATATCH_TASK_MODEL.params.verify_code = res[0].code;

                        sails.log.debug('ATATCH_TASK_MODEL.params 0: ' , ATATCH_TASK_MODEL.params);
                    })
                )
                .then(
                    _taskMapData(function(arg){
                        sails.log.debug('_taskMapData arg: ' , arg);
                        sails.log.debug('ATATCH_TASK_MODEL.params 1: ' , ATATCH_TASK_MODEL.params);

                        return arg
                    })
                )
                .then(function(){
                    return CommonService.reqTestAPI({
                        r: request,
                        url: RequestService.API_URL.ATTACH_TD,
                        method: 'post',
                        expectStatus: 200,
                        sendData: _.omit(ATATCH_TASK_MODEL.params,['key']),
                        sailsSid: CommonService.getTestSid(),
                        errLogTitle: "ATTACH_TD Error",
                        assertCallback: function (res) {
                            sails.log.debug('ATTACH_TD assertCallback: ',res);
                            res.should.property("debug");
                        },
                        done: done
                    });
                })
                .catch(function (e) {
                    sails.log.debug('ATTACH_TD Error: ' , e);
                    done();
                });

        });
    });
}

function test_001() {
    describe('=== A.0 모듈 설정 === \n', function () {

        it('A.0.1 test attach TD', function (done) {
            var ATATCH_TASK_MODEL = {
                ATTACH_TD_QRY:QueryService.ATTACH_TD_QRY,
                params:_.omit(testReqData, ['show']),
                data:{
                    goods_id:'',
                    goods_commonid:'',
                    goods_name:'',

                    store_id:'',
                    store_name:'',

                    brand_id:'',
                    brand_name:''
                }
            };

            verify(testReqData.account_uuid)
                .then(
                    _taskSideEffect(function(res){
                        sails.log.debug('SideEffect 01: ' , res);

                        ATATCH_TASK_MODEL.params.key = res[0].key;
                        ATATCH_TASK_MODEL.params.verify_code = res[0].code;
                    })
                )
                .then(
                    _taskMapData(function(arg){
                        sails.log.debug('_taskMapData arg: ' , arg);
                        return arg
                    })
                )

                //start : validate params  , verify code
                .then(_taskValidateParams)
                .then(
                    _invokePromise(
                        lib_vc.chkVerifyCodeOnPromise,
                        function(results){
                            return [{
                                key:results.data.account_uuid,
                                verify_code:results.data.verify_code,
                                account_uuid:results.data.account_uuid,
                                secret:results.data.secret
                            }];

                        }
                    )
                ).then(
                    _taskSideEffect(function(res){
                        String(res[0].code).toLowerCase().should.equal('success');
                    })
                )

                //1.  Check Store
                .then(_taskCheckStoreAndSeller)
                .then(
                    _taskSideEffect(function(res){
                        sails.log.debug('_taskCheckStoreAndSeller validate: ' , res);

                        res[0][0].should.property("store_id");
                        should.exist(res[0][0].store_id);

                    })
                )
                .then(
                    _taskSideEffect(function(res){
                        ATATCH_TASK_MODEL.data.store_id = res[0][0][0].store_id;
                    })
                )

                //2.  Check Goods
                .then(_taskCheckGoods)
                .then(
                    _taskSideEffect(function(res){
                       sails.log.debug('_taskCheckGoods validate: ' , res);

                        should.exist(res[0][0].goods_id);
                        should.exist(res[0][0].goods_commonid);
                        should.exist(res[0][0].store_id);
                        should.exist(res[0][0].store_name);
                        should.exist(res[0][0].brand_id);
                        should.exist(res[0][0].brand_name);

                    })
                ).then(
                    _taskSideEffect(function(res){
                        sails.log.debug('_taskCheckGoods 2: ' , res);

                        var resObj = res[0][0][0];

                        ATATCH_TASK_MODEL.data.goods_id = resObj.goods_id;
                        ATATCH_TASK_MODEL.data.goods_commonid = resObj.goods_commonid;
                        ATATCH_TASK_MODEL.data.goods_name = resObj.goods_name;
                        ATATCH_TASK_MODEL.data.store_id = resObj.store_id;
                        ATATCH_TASK_MODEL.data.store_name = resObj.store_name;
                        ATATCH_TASK_MODEL.data.brand_id = resObj.brand_id;
                        ATATCH_TASK_MODEL.data.brand_name = resObj.brand_name;

                        sails.log.debug('ATATCH_TASK_MODEL: ' , ATATCH_TASK_MODEL.data);

                    })
                )

                //3.  Check 3DC
                .then(_taskCheckHaving3DC)
                .then(
                    _taskSideEffect(function(res){
                        sails.log.debug('_taskCheckHaving3DC validate: ' , res);

                        should.exist(res[0][0].having_3dc);
                        if( parseInt(res[0][0].having_3dc)<1 ){
                            throw new Error('having_3dc NONE');
                        }
                    })
                )

                //4.  replace
                .then(_taskRegist3DCExposure)
                .then(
                    _taskSideEffect(function(res){
                        sails.log.debug('_taskRegist3DCExposure validate: ' , res);

                    })
                )

                //5.  delete
                .then(_taskDelete3DCExposure)
                .then(
                    _taskSideEffect(function(res){
                        sails.log.debug('_taskDelete3DCExposure validate: ' , res);

                    })
                )

                .then(function(){
                    sails.log.debug('All Settled');
                    done();

                })
                .catch(function (e) {
                    sails.log.debug('Error: ' , e);
                    done();
                });



            function _taskValidateParams(){
                sails.log.debug('ATATCH_TASK_MODEL.params:::: ' , ATATCH_TASK_MODEL.params);

                return ValidatorService.validatePromise(
                    {
                        data:ATATCH_TASK_MODEL.params,
                        validator:[
                            {key:'account_uuid' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                            {key:'secret' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                            {key:'verify_code' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                            {key:'item_id' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                            {key:'td_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                            {key:'td_rep_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }}
                        ]
                    });
            }

            function _taskCheckStoreAndSeller(){
                return SQLService
                    .queryOnPromise(
                        ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_SELLER,
                        [ATATCH_TASK_MODEL.params.account_uuid]
                    );
            }

            function _taskCheckGoods(){
                return SQLService
                    .queryOnPromise(
                        ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_GOODS,
                        [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.params.item_id]
                    );
            }

            function _taskCheckHaving3DC(){
                return SQLService
                    .queryOnPromise(
                        ATATCH_TASK_MODEL.ATTACH_TD_QRY.SELECT_FROM_SV_3DC,
                        [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.params.td_rep_ids]
                    );
            }

            function _taskRegist3DCExposure(){
                var td_id_arr = ATATCH_TASK_MODEL.params.td_ids.split(',');
                var added_qry = (_.map(td_id_arr, function(v){
                    return [
                        "(",
                        "'",ATATCH_TASK_MODEL.data.goods_id,"'",',',
                        "'",ATATCH_TASK_MODEL.data.goods_commonid,"'",',',
                        "'",ATATCH_TASK_MODEL.data.goods_name,"'",',',
                        "'",ATATCH_TASK_MODEL.data.store_id,"'",',',
                        "'",ATATCH_TASK_MODEL.data.store_name,"'",',',
                        "'",ATATCH_TASK_MODEL.data.brand_id,"'",',',
                        "'",ATATCH_TASK_MODEL.data.brand_name,"'",',',
                        "'",v,"'",',',
                        ((parseInt(v)===parseInt(ATATCH_TASK_MODEL.params.td_rep_ids))?'true':'false'),',',
                        'unix_timestamp(now())',
                        ")"
                    ].join('');

                }) ).join(',');

                return SQLService
                    .queryOnPromise(
                        ATATCH_TASK_MODEL.ATTACH_TD_QRY.REPLACE_SV_3DV_EXPOSURE+added_qry,
                        []
                    );
            }

            function _taskDelete3DCExposure(){
                return SQLService
                    .queryOnPromise(
                        ATATCH_TASK_MODEL.ATTACH_TD_QRY.DELETE_SV_3DV_EXPOSURE,
                        [ATATCH_TASK_MODEL.data.store_id, ATATCH_TASK_MODEL.params.item_id, ATATCH_TASK_MODEL.params.td_ids]
                    );
            }



        });



        /*
         Q.fcall(
         function(){

         return '';
         }
         ).then(
         _taskSideEffect(function(results){

         })
         ).then(function(results){
         return Q.fcall(
         ValidatorService.validatePromise,
         {
         data:_.omit(testReqData, ['show']),
         validator:[
         {key:'account_uuid' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
         {key:'secret' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
         {key:'verify_code' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
         {key:'item_id' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
         {key:'td_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
         {key:'td_rep_ids' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }}
         ]
         }
         );
         }).then(
         _taskCheckVerifyCode()
         ).then(
         _taskSideEffect(sails.log.debug)
         ).then(
         function(results){
         sails.log.debug('results: ' , results);
         done();

         }).catch(function(e){
         sails.log.debug('Error: ' , e);
         done();

         });*/

        function _taskCheckVerifyCode(){
            return _invokePromise(
                lib_vc.chkVerifyCodeOnPromise,
                function(results){
                    return [{
                        key:results.data.account_uuid,
                        verify_code:results.data.verify_code,
                        account_uuid:results.data.account_uuid,
                        secret:results.data.secret
                    }];
                }
            );
        }



    });



}
/*
 function(results){
 sails.log.debug('results; ' , results);
 var data = results.data;

 return lib_vc.chkVerifyCodeOnPromise({
 key:data.account_uuid,
 verify_code:data.verify_code,
 account_uuid:data.account_uuid,
 secret:data.secret
 });

 }
*/


function assignBasicModule() {
    lib_vc = require("../../../api/controllers/libs/verifyCode.js");
    UUID = require("../../../api/controllers/libs/uuid.js");
    debug = require("../../../api/controllers/libs/debug.js");
    SQLService = require('../../../api/services/SQLService');
    FileService = require('../../../api/services/FileService');
    ErrorHandler = require('../../../api/services/ErrorHandler');
    ValidatorService = require('../../../api/services/ValidatorService');
    QueryService = require('../../../api/services/QueryService');
    RequestService = require('../../util/RequestService.js');
    CommonService = require('../../util/CommonService.js');
    console.log('assignBasicModule');
}


//util fnc
function verify(_name, _fnc, _done) {
    return CommonService.reqTestAPI({
        r: request,
        url: RequestService.API_URL.GET_VERIFI_CODE,
        method: 'post',
        expectStatus: 200,
        sendData: {key: _name, ip_addr: '127.0.0.1', show: 'debug'},
        sailsSid: CommonService.getTestSid(),
        errLogTitle: "C.2.1 verify code success",
        assertCallback: function (res) {
            //sails.log.debug(res);
            res.should.property("debug");
            res.should.property("code");
            res.should.property("key", _name);
            if(_fnc){
                _fnc(res);
            }
        },
        done: (_done || null)
    })
}

function _taskSideEffect(func){
    return function(){
        if(func) func(arguments);
        return arguments;
    };
}

function _taskMapData(mapData){
    return function(){
        if(mapData){
            if(typeof mapData==='function'){
                return mapData(arguments);
            }else{
                return mapData
            }
        }else{
            return arguments;
        }
    };
}


function _invokePromise(fnc, valFnc){
    return function(results){
        return Q.fapply(fnc, valFnc(results) );
    }
}