<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>

<table class="ncsc-default-table">
  <thead>
    <tr><th class="w30"></th>
      <th class="tl">이벤트명</th>
      <th class="w250">시작시간&nbsp;-&nbsp;종료시간</th>
      
      <th class="w300">이벤트내용</th>
      <th class="w110">상태</th>
    </tr>
  </thead>
  <tbody>
    <tr class="bd-line"><td></td>
      <td class="tl"><dl class="goods-name"><dt><?php echo $output['mansong_info']['mansong_name_ko'];?></dt></dl></td>
      <td><p><?php echo date('Y-m-d H:i',$output['mansong_info']['start_time']);?></p><p>부터</p><p><?php echo date('Y-m-d H:i',$output['mansong_info']['end_time']);?></p></td>
      <td><ul class="ncsc-mansong-rule-list">
          <?php if(!empty($output['list']) && is_array($output['list'])){?>
          <?php foreach($output['list'] as $key=>$val){?>
          <li>
              충족금액<strong><?php echo number_format($val['price_ko']);?></strong>원，&nbsp;
              <?php if(!empty($val['discount_ko'])) { ?>
                  할인금액<strong><?php echo number_format($val['discount']);?></strong>원，&nbsp;
              <?php } ?>
              <?php if(empty($val['goods_id'])) { ?>
                    <!-- <?php echo $lang['gift_name'];?> <span><?php echo $lang['text_not_join'];?></span> -->
              <?php } else { ?>
              		사은품<a href="<?php echo $val['goods_url'];?>" title="<?php echo $val['mansong_goods_name_ko'];?>" target="_blank" class="goods-thumb"> <img src="<?php echo cthumb($val['goods_image']);?>"/> </a>
              <?php } ?>
          </li>
          <?php }?>
          <?php }?>
        </ul></td>
      <td><?php echo $output['mansong_info']['mansong_state_text'];?></td>
    </tr>
  <tbody> 
</table>
