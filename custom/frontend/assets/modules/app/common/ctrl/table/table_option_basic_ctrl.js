
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.ctrl.tableOptionsBasicCtrl');
  APP.modules.ctrl.tableOptionsBasicCtrl = tableOptionsBasicCtrl;

  function tableOptionsBasicCtrl( $scope, _, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL, $sce ){
    var ctrl = this;
    ctrl.imgUrl = APP.info.imgURL;
    ctrl.gVersion = APP.info.gVersion;
    ctrl.count;

    ctrl.getTotal;
    ctrl.getStart;
    ctrl.getCount;
    ctrl.updatePage;
    ctrl.onChangeCount = _onChangeCount;

    _init();

    function _init(){
      ctrl.count=ctrl.getCount();
      $scope.$on('$destroy', function () {
      });
      PubSub.subscribeOnce('tableComponent:bootstrap:complete', _completeBootstrap);
    }

    function _completeBootstrap(topic, data){
      if(data.success){
      }
    }

    function _onChangeCount(){
      console.log('_onChangeCount: ');
      ctrl.updatePage({
        options:{
          start:0, count:window.angular.copy(ctrl.count)
        }
      });
    }

  }
  tableOptionsBasicCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL', '$sce'];
  tableOptionsBasicCtrl.bindToController = {
    getTotal:'&',
    getStart:'&',
    getCount:'&',
    updatePage:'&'
  };
  tableOptionsBasicCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      /*var isFst = true;

       scope.$watch('tableOptionCtrl.count',
       function(__newVal, __oldVal){
       if(__newVal===__oldVal){
       }else{
       if(isFst){
       isFst = false;
       }else{
       scope.tableOptionCtrl.updatePage({
       options:{
       start:0, count:window.angular.copy(__newVal)
       }
       });
       }
       }
       });*/

      PubSub.trigger('tableComponent:bootstrap',{tableOptionCtrl:true});
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
