<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['snsalbum_album_setting']	= '앨범설정';
$lang['snsalbum_allow_upload_max_count']	= '이미지 최대 업로드수';
$lang['snsalbum_allow_upload_max_count_tip']= '0은 무제한';
$lang['snsalbum_pls_input_figures']			= '숫자로 입력하세요';
$lang['snsalbum_class_list']	= '앨범리스트';
$lang['snsalbum_class_name']	= '앨범명';
$lang['snsalbum_member_name']	= '회원아이디';
$lang['snsalbum_add_time']		= '만든시간';
$lang['snsalbum_pic_count']		= '이미지수량';
$lang['snsalbum_pic_list']		= '이미지리스트';
$lang['snsalbum_pic_name']		= '이미지명';
$lang['snsalbum_choose_need_del_img']		= '삭제하실 이미지를 선택하세요';