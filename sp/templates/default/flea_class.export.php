<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>벼룩카테고리</h3>
      <ul class="tab-base">
        <li><a href="index.php?act=flea_class&op=goods_class"><span>관리</span></a></li>
        <li><a href="index.php?act=flea_class&op=goods_class_add" ><span>추가</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>불러내기</span></a></li>
        <li><a href="index.php?act=flea_class&op=goods_class_import"><span>불러오기</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
    <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th class="nobg" colspan="12"><div class="title"><h5>조작 알림</h5><span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
        <ul>
            <li>불러낼 내용은 상품 카테고리 정보의 .csv파일입니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form method="post" name="form1">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="if_convert" value="1" />
    <table class="table tb-type2">
    <thead>
        <tr class="thead">
          <th>상품 카테고리 데이터를 불러내시겠습니까?</th>
      </tr></thead>
      <tfoot><tr class="tfoot">
        <td><a href="JavaScript:document.form1.submit();" class="btn"><span>불러내기</span></a></td>
      </tr></tfoot>
    </table>
  </form>
</div>
