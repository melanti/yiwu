<?php
/**
 * HSCODE
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class hscode_userControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('hscode');
	}

	public function indexOp(){
		if(!empty($_GET['article_abstract'])) {
			$condition['article_abstract'] = array('like', '%'.$_GET['article_abstract'].'%');
		}
		if(!empty($_GET['article_class_id'])) {
			$condition['article_class_id'] = $_GET['article_class_id'];
		}

		$model_article = Model('hscode_article');
		$article_list = $model_article->getList($condition, 10, 'article_id desc');

		$this->show_menu('list');
		$model = Model('hscode');
		$cat = $model->getList(TRUE);
		Tpl::output('show_page',$model_article->showpage(2));
		Tpl::output('list',$article_list);
		Tpl::output('categorys',$cat);
		Tpl::showpage("hscode_user.index");
	}

	public function addOp(){
		if(intval($_GET['article_id']) > 0) {
		        $model = Model('hscode_article');
        			$param['article_id'] = intval($_GET['article_id']);
		        $result = $model->getOne($param);
			Tpl::output('info',$result);
		}
		$model = Model('hscode');
		$cat = $model->getList(TRUE);
		$this->show_menu('list_add');
		Tpl::output('categorys',$cat);
		Tpl::showpage("hscode_user.add");
	}

	public function dropOp(){
	        $article_id = trim($_POST['article_id']);
	        $model_article = Model('hscode_article');
	        $condition = array();
	        $condition['article_id'] = array('in',$article_id);
	        $result = $model_article->drop($condition);
	        if($result) {
	            showMessage(Language::get('nc_common_del_succ'),'');
	        } else {
	            showMessage(Language::get('nc_common_del_fail'),'','','error');
	        }
	}


    /**
     * hscode카테고리저장
     **/
    public function saveOp() {

        $param = array();
        $param['article_class_id'] = intval($_POST['class_name']);
        $param['article_title'] = trim($_POST['hscode']);
        $param['article_abstract'] = trim($_POST['hsname']);
        $param['article_content'] = trim($_POST['hsdes']);
        $model = Model('hscode_article');
        if(intval($_POST['artid'])>0)
        {
        	$condition['article_id'] = intval($_POST['artid']);
        	$result = $model->modify($param,$condition);
        }
        else
        {
        	$result = $model->save($param);

        }
        if($result) {
            showMessage("성공",'index.php?act=hscode_user&op=index');
        } else {
           showMessage(Language::get('class_add_fail'),'index.php?act=hscode_user&op=index','','error');
        }


    }

    /**
     * hscode카테고리정렬修改
     */
    public function updateOp() {
        if(intval($_GET['id']) <= 0) {
            echo json_encode(array('result'=>FALSE,'message'=>Language::get('param_error')));
            die;
        }
        $new_sort = intval($_GET['value']);
        if ($new_sort > 255){
            echo json_encode(array('result'=>FALSE,'message'=>Language::get('class_sort_error')));
            die;
        } else {
            $model_class = Model("hscode");
            $result = $model_class->modify(array('class_sort'=>$new_sort),array('class_id'=>$_GET['id']));
            if($result) {
                echo json_encode(array('result'=>TRUE,'message'=>'class_add_success'));
                die;
            } else {
                echo json_encode(array('result'=>FALSE,'message'=>Language::get('class_add_fail')));
                die;
            }
        }
    }

	private function show_menu($menu_key) {
		$menu_array = array(
			'list'=>array('menu_type'=>'link','menu_name'=>'리스트','menu_url'=>'index.php?act=hscode_user&op=index'),
			'list_add'=>array('menu_type'=>'link','menu_name'=>'추가','menu_url'=>'index.php?act=hscode_user&op=add'),
		);

		$menu_array[$menu_key]['menu_type'] = 'text';
		Tpl::output('menu',$menu_array);
	}


}
?>