<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="goods-gallery" cnbiztype="gallery-<?php echo $output['color_id'];?>"> <a class="sample_demo" href="index.php?act=store_album&op=pic_list&item=goods_image&color_id=<?php echo $output['color_id'];?>" style="display:none;">완료</a>
  <div class="nav"><span class="l">사용자앨범  >
    <?php if(isset($output['class_name']) && $output['class_name'] != ''){echo $output['class_name'];}else{?>
    모든이미지
    <?php }?>
    </span><span class="r">
    <select name="jumpMenu" style="width:100px;">
      <option value="0" style="width:80px;">선택</option>
      <?php foreach($output['class_list'] as $val) { ?>
      <option style="width:80px;" value="<?php echo $val['aclass_id']; ?>" <?php if($val['aclass_id']==$_GET['id']){echo 'selected';}?>><?php echo $val['aclass_name']; ?></option>
      <?php } ?>
    </select>
    </span></div>
  <ul class="list">
    <?php if(!empty($output['pic_list'])){?>
    <?php foreach ($output['pic_list'] as $v){?>
    <li onclick="insert_img('<?php echo $v['apic_cover'];?>','<?php echo thumb($v, 240);?>', <?php echo $output['color_id'];?>);"><a href="JavaScript:void(0);"><img src="<?php echo thumb($v, 240);?>" title='<?php echo $v['apic_name']?>'/></a></li>
    <?php }?>
    <?php }else{?>
    내용이 없습니다.
    <?php }?>
  </ul>
  <div class="pagination"><?php echo $output['show_page']; ?></div>
</div>
<script>
$(document).ready(function(){
	$('div[cnbiztype="gallery-<?php echo $output['color_id'];?>"]').find('.demo').unbind().ajaxContent({
		event:'click', //mouseover
		loaderType:'img',
		loadingMsg:'<?php echo SHOP_TEMPLATES_URL;?>/images/loading.gif',
		target:'div[cnbiztype="album-<?php echo $output['color_id'];?>"]'
	});
	$('div[cnbiztype="gallery-<?php echo $output['color_id'];?>"]').find('select[name="jumpMenu"]').unbind().change(function(){
		var $_select_submit = $('div[cnbiztype="gallery-<?php echo $output['color_id'];?>"]').find('.sample_demo');
		var $_href = $_select_submit.attr('href') + "&id=" + $(this).val();
		$_select_submit.attr('href',$_href);
		$_select_submit.unbind().ajaxContent({
			event:'click', //mouseover
			loaderType:'img',
			loadingMsg:'<?php echo SHOP_TEMPLATES_URL;?>/images/loading.gif',
			target:'div[cnbiztype="album-<?php echo $output['color_id'];?>"]'
		});
		$_select_submit.click();
	});
});
</script>