<?php
defined('InCNBIZ') or exit('Access Invalid!');

//common.php 调用

$lang['nc_comm_workarea']	=  '짜르기';
$lang['nc_comm_cut_view']	=  '미리보기';
$lang['nc_comm_op_help']	=  'TIP';
$lang['nc_comm_op_help_tip']=  '짜르기 부분에서 선택 내역을 확대/축소하여 비율을 설정하시면 됩니다, 반드시 저장 버튼을 클릭하셔야만 변경된 이미지가 적용됩니다.';