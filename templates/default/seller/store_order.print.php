<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/base.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/seller_center.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
body { background: #FFF none;
}
</style>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.printarea.js" charset="utf-8"></script>
<title>인쇄--<?php echo $output['store_info']['store_name'];?>송장</title>
</head>

<body>
<?php if (!empty($output['order_info'])){?>
<div class="print-layout">
  <div class="print-btn" id="printbtn" title="잉크 젯 혹은 레이저 프린터 중 선택 하신 후 <br/>용지에 따라서 <br/>송장이 설정/인쇄 됩니다."><i></i><a href="javascript:void(0);">인쇄</a></div>
  <div class="a5-size"></div>
  <dl class="a5-tip">
    <dt>
      <h1>A5</h1>
      <em>Size: 210mm x 148mm</em></dt>
    <dd> A5 용지를 사용하시면 가로로 인쇄되며, 가장자리 간격이 없을 경우 각 A5 1장에 출력됩니다.></dd>
  </dl>
  <div class="a4-size"></div>
  <dl class="a4-tip">
    <dt>
      <h1>A4</h1>
      <em>Size: 210mm x 297mm</em></dt>
    <dd> A4 용지를 사용하시면 가로로 인쇄되며, 가장자리 간격이 없을 경우 각 A4 2장에 출력됩니다.</dd>
  </dl>
  <div class="print-page">
    <div id="printarea">
      <?php foreach ($output['goods_list'] as $item_k =>$item_v){?>
      <div class="orderprint">
        <div class="top">
          <?php if (empty($output['store_info']['store_label'])){?>
          <div class="full-title"><?php echo $output['store_info']['store_name'];?> 송장</div>
          <?php }else {?>
          <div class="logo" ><img src="<?php echo $output['store_info']['store_label']; ?>"/></div>
          <div class="logo-title"><?php echo $output['store_info']['store_name'];?>송장</div>
          <?php }?>
        </div>
        <table class="buyer-info">
          <tr>
            <td><?php echo '주문번호'.$lang['nc_colon'];?><?php echo $output['order_info']['order_sn'];?></td>
            <td><?php echo $lang['member_printorder_orderadddate'].$lang['nc_colon'];?><?php echo @date('Y-m-d',$output['order_info']['add_time']);?></td>
            <td><?php if ($output['order_info']['shippin_code']){?>
              <span><?php echo $lang['member_printorder_shippingcode'].$lang['nc_colon']; ?><?php echo $output['order_info']['shipping_code'];?></span>
              <?php }?></td>
          </tr>
        </table>
        <table class="order-info">
          <thead>
            <tr>
              <th class="w40">번호</th>
              <th class="tl">상품명</th>
              <th class="w70 tl">단가(원화)</th>
              <th class="w50">수량</th>
              <th class="w70 tl">소계(위안)</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($item_v as $k=>$v){?>
            <tr>
              <td><?php echo $k;?></td>
              <td class="tl"><?php echo $v['goods_name_ko'];?></td>
              <td class="tl"><?php echo number_format($v['goods_price_ko']);?>원</td>
              <td><?php echo $v['goods_num'];?></td>
              <td class="tl"><?php echo number_format($v['goods_all_price_ko']);?>원</td>
            </tr>
            <?php }?>
            <tr>
              <th></th>
              <th colspan="2" class="tl">합계</th>
              <th><?php echo $output['goods_all_num'];?></th>
              <th class="tl"><?php echo number_format($output['goods_total_price_ko']);?>원</th>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="10"><span><?php echo "합계".$lang['nc_colon'];?><?php echo number_format($output['goods_total_price_ko']);?>원</span><span><?php echo '배송료'.$lang['nc_colon'];?><?php echo number_format($output['order_info']['shipping_fee_ko']);?>원</span><span><?php echo  '할인'.$lang['nc_colon'];?><?php echo number_format($output['promotion_amount']);?>원</span><span><?php echo '주문 총 액'.$lang['nc_colon'];?><?php echo number_format($output['order_info']['order_amount_ko']);?>원</span><span><?php echo'업체명'.$lang['nc_colon'];?><?php echo $output['store_info']['store_name'];?></span>
                <?php if (!empty($output['store_info']['store_qq'])){?>
                <span>QQ：<?php echo $output['store_info']['store_qq'];?></span>
                <?php }elseif (!empty($output['store_info']['store_ww'])){?>
                <span><?php echo $lang['member_printorder_shopww'].$lang['nc_colon'];?><?php echo $output['store_info']['store_ww'];?></span>
                <?php }?></th>
            </tr>
          </tfoot>
        </table>
        <?php if (empty($output['store_info']['store_stamp'])){?>
        <div class="explain">
          <?php echo $output['store_info']['store_printdesc'];?>
        </div>
        <?php }else {?>
        <div class="explain">
          <?php echo $output['store_info']['store_printdesc'];?>
        </div>
        <div class="seal"><img src="<?php echo $output['store_info']['store_stamp'];?>" onload="javascript:DrawImage(this,120,120);"/></div>
        <?php }?>
        <div class="tc page"> <?php echo $item_k;?>페이지/총<?php echo count($output['goods_list']);?>페이지</div>
      </div>
      <?php }?>
    </div>
    <?php }?>
  </div>
</div>
</body>
<script>
$(function(){
  $("#printbtn").click(function(){
  $("#printarea").printArea();
  });
});

//打印提示
$('#printbtn').poshytip({
  className: 'tip-yellowsimple',
  showTimeout: 1,
  alignTo: 'target',
  alignX: 'center',
  alignY: 'bottom',
  offsetY: 5,
  allowTipHover: false
});
</script>
</html>