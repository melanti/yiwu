(function($, angular, _){
  'use strict';
  
  angular.module('underscore.service')
    .factory('_', ['$window', function($window) {
      return $window._; // assumes underscore has already been loaded on the page
    }]);
})(window.jQuery, window.angular, window._);
