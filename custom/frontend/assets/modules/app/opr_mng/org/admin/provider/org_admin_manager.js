(function($, angular, _, APP){

  /*

   Api url sample - http://api-host/org/setMajorOrganization
   Required filed information (POST)
   Field Name	Info	Type


   X admin_uuid	the master account uuid(login user)	String
   X secret	the admin account secret data (in the user login session data)	String
   X verify_code	user verification code made by admin_uuid	String

   org_certification	the organization's certificate for business registration	String
   org_name	the organization name	String
   service_type	0 : shop, 1 : sdk	Integer
   status	0 : activated, 1 : disabled	Integer
   contact_name	the name to contact	String
   contact_phone	the phone number to contact	String
   contact_mail	the email to contact	String
   contact_homepage	the homepage url to contact	String
   language	the locale language value(2bytes code : en, ko ....)	String
   currency	the currency value (e.x. : KRW, USD...)	String
   utc	the timezone value	String

   show	for viewing some information(debug ...)	String


   Parameter
   필수 Field : admin_uuid, secret, verify_code, org_certification, org_name, status, service_type, language, currency, utc

  */

  /*
  TODO

  1. model 확인 , 정의

  2. 필요 API 정의

  3. pageType (registration , modification, view) 에 따라 API, 분기

  4. validator 만들기

  5. 완성후 ajax API는 따로 뺴기

  */

  angular.module('App.org')
    .service('OrgAdminManager', OrgAdminManager);

  function OrgAdminManager(_, $q , $timeout, $http, XHR, API_URL_INFO, COMMON_UTIL){
    var vm = this;

    vm._state = {
      org_certification:'',
      org_name:'',
      service_type:0,
      status:0,
      contact_name:'',
      contact_phone:'',
      contact_mail:'',
      contact_homepage:'',
      language:null,
      currency:null,
      utc:null,
      utc_gap:null,
      show:'debug'
    };

    vm.exports = {
      init:_init,
      getState:_getState,
      setState:_setState,
      getCurrency:_getCurrency,
      getUTC:_getUTC,
      getLanguage:_getLanguage,
      setMajorOrganization:_setMajorOrganization,
      updateMajorOrganization:_updateMajorOrganization,
      deleteOrgAdmin:_deleteOrgAdmin,
      getMajorOrganization:_getMajorOrganization
    };

    function _init(){
      console.log( 'OrgAdminManager.init' );
    }

    function _getState(){
      return vm._state;
    }

    function _setState(__passingData){
      vm._state = _.extend( vm._state, __passingData );
    }

    function _setMajorOrganization(__data){
      var deferred = $q.defer();
      console.log('OrgAdminManager _setMajorOrganization __data: ' , __data);
      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.SET_MAJOR_ORGANIZATION,
          data:__data
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _updateMajorOrganization(__data){
      var deferred = $q.defer();
      console.log('OrgAdminManager _updateMajorOrganization __data: ' , __data);
      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.UPDATE_MAJOR_ORGANIZATION,
          data:__data
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _deleteOrgAdmin(__data){

    }

    function _getMajorOrganization(__data){
      var deferred = $q.defer();
      console.log('OrgAdminManager _getOrgAdmin __data: ' , __data);
      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_MAJOR_ORGANIZATION+'?account_uuid='+(__data.account_uuid || '')+'&org_id='+(__data.org_id || '')
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _getCurrency(){
      return XHR.REQ({method: 'get', url: API_URL_INFO.GET_CURRENCY});
    }

    function _getUTC(){
      return XHR.REQ({method: 'get', url: API_URL_INFO.GET_UTC});
    }

    function _getLanguage(){
      return XHR.REQ({method: 'get', url: API_URL_INFO.GET_LANGUAGE});
    }


  }
  OrgAdminManager.$inject= ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', 'COMMON_UTIL'];


})(window.jQuery, window.angular, window._, window.APP);
