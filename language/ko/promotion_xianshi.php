<?php
defined('InCNBIZ') or exit('Access Invalid!');

$lang['promotion_unavailable'] = '상품 프로모션 미시동';

$lang['promotion_xianshi'] = '限时折扣';

$lang['promotion_active_list'] 	= '이벤트 목록';
$lang['promotion_quota_list'] 	= '세트리스트';
$lang['promotion_join_active'] 	= '프로모션 추가';
$lang['promotion_buy_product'] 	= '구매';
$lang['promotion_goods_manage'] = '상품관리';
$lang['promotion_add_goods'] 	= '상품추가';


$lang['state_new'] = '新申请';
$lang['state_verify'] = '已审核';
$lang['state_cancel'] = '已取消';
$lang['state_verify_fail'] = '심사실패';
$lang['xianshi_state_unpublished'] = '未发布';
$lang['xianshi_state_published'] = '已发布';
$lang['xianshi_state_cancel'] = '已取消';
$lang['all_state'] = '全部状态';


$lang['xianshi_quota_start_time'] = '开始时间';
$lang['xianshi_quota_end_time'] = '结束时间';
$lang['xianshi_quota_times_limit'] = '活动次数限制';
$lang['xianshi_quota_times_published'] = '已发布活动次数';
$lang['xianshi_quota_times_publish'] = '剩余活动次数';
$lang['xianshi_quota_goods_limit'] = '商品限制';
$lang['xianshi_name'] = '活动名称';
$lang['xianshi_apply_date'] = '套餐申请时间';
$lang['xianshi_apply_quantity'] = '申请数量（月）';
$lang['apply_date'] = '申请时间';
$lang['apply_quantity'] = '申请数量';
$lang['apply_quantity_unit'] = '（包月）';
$lang['xianshi_discount'] = '折扣';
$lang['xianshi_buy_limit'] = '购买限制';

$lang['start_time'] = '开始时间';
$lang['end_time'] = '结束时间';
$lang['xianshi_list'] = '限时折扣';
$lang['mansong_list'] = '满即送';
$lang['xianshi_add'] = '프로모션 추가';
$lang['xianshi_index'] = '活动列55表';
$lang['xianshi_quota'] = '套餐列表';
$lang['xianshi_apply'] = '申请列表';
$lang['xianshi_manage'] = '活动管理';
$lang['xianshi_quota_add'] = '구매';
$lang['xianshi_quota_add_quantity'] = '套餐购买数量';
$lang['xianshi_quota_add_confirm'] = '정말 구매하시겠습니까? 총 지불금액 :';
$lang['goods_add'] = '添加商品';
$lang['choose_goods'] = '选择活动商品';
$lang['goods_name'] = '商品名称';
$lang['goods_store_price'] = '商品价格';
$lang['xianshi_goods_selected'] = '已选商品';
$lang['xianshi_publish'] = '发布活动';
$lang['ensure_publish'] = '确认发布该活动?';
$lang['xianshi_no_goods'] = '您还没有选择活动商品';
$lang['xianshi_goods_exist'] = '已参加';

$lang['xianshi_price'] = '购买限时折扣所需金币数';
$lang['xianshi_explain1'] = '1、点击购买套餐和套餐续费按钮可以购买或续费套餐';
$lang['xianshi_explain2'] = '2、点击프로모션 추가按钮可以添加限时折扣活动，点击管理按钮可以对限时折扣活动内的商品进行管理';
$lang['xianshi_explain3'] = '3、点击删除按钮可以删除限时折扣活动';
$lang['xianshi_manage_goods_explain1'] = '1、限时折扣商品的时间段不能重叠';
$lang['xianshi_manage_goods_explain2'] = '2、点击添加商品按钮可以搜索并添加参加活动的商品，点击删除按钮可以删除该商品';
$lang['xianshi_price_explain1'] = '购买单位为月(30天)，您可以在所购买的周期内发布限时折扣活动';
$lang['xianshi_price_explain2'] = '每月(30天)您需要支付';
$lang['xianshi_add_explain1'] = '您本期还可以创建%s个活动';
$lang['xianshi_add_start_time_explain'] = '시작시간은 %s';
$lang['xianshi_add_end_time_explain'] = '종료시간은 %s';
$lang['xianshi_discount_explain'] = '折扣必须为0.1-9.9之间的数字';
$lang['xianshi_buy_limit_explain'] = '购买限制必须为正整数';
$lang['time_error'] = '时间格式错误';
$lang['param_error'] = '参数错误';
$lang['greater_than_start_time'] = '종료시간은 시작시간보다 커야됩니다.';
$lang['xianshi_price_error'] = '不能为空且必须为正整数';
$lang['xianshi_name_explain'] = '活动名称将显示在限时折扣活动列表中，方便商家管理使用，最多可输入25个字符。';
$lang['xianshi_title_explain'] = '活动标题是商家对限时折扣活动的别名操作，请使用例如“新品打折”、“月末折扣”类短语表现，最多可输入10个字符；<br/>非必填选项，留空商品优惠价格前将默认显示“限时折扣”字样。';
$lang['xianshi_explain_explain'] = '活动描述是商家对限时折扣活动的补充说明文字，在商品详情页-优惠信息位置显示；<br/>非必填选项，最多可输入30个字符。';
$lang['xianshi_name_error'] = '活动名称不能为空';
$lang['xianshi_quota_quantity_error'] = '수량을 1~12 사이의 정수로 입력해 주세요.';
$lang['xianshi_quota_add_success'] = '기간한정세일 패키지 구매 성공';
$lang['xianshi_quota_add_fail'] = '기간한정세일 패키지 구매신청 실패';
$lang['xianshi_quota_add_fail_nogold'] = '기간한정세일 패키지 구매신청 실패, 잔액이 부족합니다.';
$lang['xianshi_quota_current_error'] = '사용가능한 기간한정세일 패키지가 없습니다. 먼저 구매해 주세요.';
$lang['xianshi_quota_current_error1'] = '아직 "기간한정세일" 패키지를 신청하지 않으셨거나 기한이 만료되었습니다. <br/>패키지를 구매해 주세요.';
$lang['xianshi_quota_current_error2'] = '이미 구매하셨습니다.';
$lang['xianshi_quota_current_error3'] = '이미 사용이 완료되어 다시 등록하실 수 없습니다.';
$lang['xianshi_add_success'] = '기간한정세일 이벤트가 추가되었습니다. ';
$lang['xianshi_active_status'] = '活动状态';
$lang['xianshi_add_fail'] = '限时折扣活动添加失败';
$lang['xianshi_goods_none'] = '您还没有프로모션 추가商品';
$lang['xianshi_goods_add_success'] = '限时折扣活动商品添加成功';
$lang['xianshi_goods_add_fail'] = '限时折扣活动商品添加失败';
$lang['xianshi_goods_delete_success'] = '限时折扣活动商品删除成功';
$lang['xianshi_goods_delete_fail'] = '限时折扣活动商品删除失败';
$lang['xianshi_goods_cancel_fail'] = '限时折扣活动商品取消失败';
$lang['xianshi_goods_limit_error'] = '已经超过了活动商品数限制';
$lang['xianshi_goods_is_exist'] = '该商品已经参加了本期限时折扣，请选择其它商品';
$lang['xianshi_publish_success'] = '限时折扣活动发布成功';
$lang['xianshi_publish_fail'] = '限时折扣活动发布失败';
$lang['xianshi_cancel_success'] = '限时折扣活动取消成功';
$lang['xianshi_cancel_fail'] = '限时折扣活动取消失败';

$lang['setting_save_success'] = '设置완료';
$lang['setting_save_fail'] = '设置保存失败';

$lang['text_month'] = '月';
$lang['text_gold'] = '金币';
$lang['text_jian'] = '件';
$lang['text_ci'] = '次';
$lang['text_goods'] = '商品';
$lang['text_normal'] = '正常';
$lang['text_invalidation'] = '失效';
$lang['text_default'] = '默认';
$lang['text_add'] = '添加';

$lang['xianshi_apply_verify_success_glog_desc'] = '购买限时折扣活动%s个月，单价%s金币，总共花费%s金币';
