<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <div class="alert"><strong>TIP：</strong>구매 전과 구매 후, 고객이 온라인으로 문의할 수 있는 계정을 입력하시고 "저장"해 주세요. 
    </li>
  </div>
  <form method="post" action="index.php?act=store_callcenter&op=save" id="callcenter_form" onsubmit="ajaxpost('callcenter_form','','','onerror')" class="ncs-message">
    <input type="hidden" name="form_submit" value="ok" />
    <dl cnbiztype="pre">
      <dt>구매전 문의：</dt>
      <dd>
        <div class="ncs-message-title"><span class="name">판매자 닉네임</span><span class="tool">메신저 종류</span><span class="number">계정</span></div>
        <?php if(empty($output['storeinfo']['store_presales'])){?>
        <div class="ncs-message-list"><span class="name tip" title="기본 값 사용 혹은 닉네임을 수정하세요">
          <input type="text" class="text w60" value="구매전1" name="pre[1][name]" maxlength="10" />
          </span><span class="tool tip" title="메신저 종류를 선택해 주세요.">
          <select name="pre[1][type]">
            <option value="0">-선택하세요-</option>
            <option value="1">QQ</option>
            <option value="2">WangWang</option>
            <option value="3">시스템 자체IM</option>
          </select>
          </span><span class="number tip" title="선택한 메신저 종류에 정확한 계정을 입력해 주세요.">
          <input name="pre[1][num]" type="text" class="text w180" maxlength="25" />
          </span><span class="del"><a cnbiztype="del" href="javascript:void(0);" class="ncsc-btn"><i class="icon-trash"></i>삭제</a></span></div>
        <?php }else{?>
        <?php foreach ($output['storeinfo']['store_presales'] as $key=>$val){?>
        <div class="ncs-message-list"><span class="name tip" title="기본 값 사용 혹은 닉네임을 수정하세요.">
          <input type="text" class="text w60" value="<?php echo $val['name'];?>" name="pre[<?php echo $key;?>][name]" maxlength="10" />
          </span><span class="tool tip" title="메신저 종류를 선택해 주세요">
          <select name="pre[<?php echo $key;?>][type]">
            <option value="1" <?php if($val['type'] == 1){?>selected="selected"<?php }?>>QQ</option>
            <option value="2" <?php if($val['type'] == 2){?>selected="selected"<?php }?>>WangWang</option>
            <option value="3" <?php if($val['type'] == 3){?>selected="selected"<?php }?>>시스템 자체IM</option>
          </select>
          </span><span class="number tip" title="선택한 메신저 종류에 정확한 계정을 입력해 주세요.">
          <input name="pre[<?php echo $key;?>][num]" type="text" class="text w180" value="<?php echo $val['num'];?>" maxlength="25" />
          </span><span class="del"><a cnbiztype="del" href="javascript:void(0);" class="ncsc-btn"><i class="icon-trash"></i>삭제</a></span> </div>
        <?php }?>
        <?php }?>
        <p><span><a href="javascript:void(0);" onclick="add_service('pre');" class="ncsc-btn ncsc-btn-acidblue mt10"><i class="icon-plus"></i>고객 서비스 추가</a></span></p>
      </dd>
    </dl>
    <dl cnbiztype="after" >
      <dt>구매후 문의</dt>
      <dd>
        <div class="ncs-message-title"><span class="name">판매자 닉네임</span><span class="tool">메신저 종류</span><span class="number">계정</span></div>
        <?php if(empty($output['storeinfo']['store_aftersales'])){?>
        <div class="ncs-message-list"><span class="name tip" title="기본 값 사용 혹은 닉네임을 수정하세요.">
          <input type="text" class="text w60" value="구매후1" name="after[1][name]" maxlength="10" />
          </span><span class="tool tip" title="메신저 종류를 선택해 주세요">
          <select name="after[1][type]">
            <option value="0">-선택하세요-</option>
            <option value="1">QQ</option>
            <option value="2">WangWang</option>
            <option value="3">시스템 자체IM</option>
          </select>
          </span><span class="number tip" title="선택한 메신저 종류에 정확한 계정을 입력해 주세요.">
          <input type="text" class="text w180" name="after[1][num]" maxlength="25" />
          </span><span><a cnbiztype="del" href="javascript:void(0);" class="ncsc-btn"><i class="icon-trash"></i>삭제</a></span> </div>
        <?php }else{?>
        <?php foreach($output['storeinfo']['store_aftersales'] as $key=>$val){?>
        <div class="ncs-message-list"><span class="name tip" title="기본 값 사용 혹은 닉네임을 수정하세요.">
          <input type="text" class="text w60" value="<?php echo $val['name'];?>" name="after[<?php echo $key;?>][name]" maxlength="10" />
          </span><span class="tool tip" title="메신저 종류를 선택해 주세요">
          <select name="after[<?php echo $key;?>][type]">
            <option value="1" <?php if($val['type'] == 1){?>selected="selected"<?php }?>>QQ</option>
            <option value="2" <?php if($val['type'] == 2){?>selected="selected"<?php }?>>WangWang</option>
            <option value="3" <?php if($val['type'] == 3){?>selected="selected"<?php }?>>시스템 자체IM</option>
          </select>
          </span><span class="number tip" title="선택한 메신저 종류에 정확한 계정을 입력해 주세요.">
          <input type="text" class="text w180" name="after[<?php echo $key;?>][num]" maxlength="25" value="<?php echo $val['num'];?>" />
          </span><span class="del"><a cnbiztype="del" href="javascript:void(0);" class="ncsc-btn"><i class="icon-trash"></i>삭제</a></span> </div>
        <?php }?>
        <?php }?>
        <p><span><a href="javascript:void(0);" onclick="add_service('after');" class="ncsc-btn ncsc-btn-acidblue mt10"><i class="icon-plus"></i>고객 서비스 추가</a></span></p>
      </dd>
    </dl>
    <dl >
      <dt><em class="pngFix">서비스 이용시간</em></dt>
      <dd>
        <div class="ncs-message-title"><span>예：(이용시간 AM 10:00 - PM 18:00)</span></div>
        <div>
          <textarea name="working_time" class="textarea w500 h50"><?php echo $output['storeinfo']['store_workingtime'];?></textarea>
        </div>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input type="submit" class="submit" value="<?php echo'저장';?>" /></label>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>
<script>
    var seller_option = '';
    <?php if (is_array($output['seller_list']) && !empty($output['seller_list'])) { ?>
    <?php foreach ($output['seller_list'] as $key => $val) { ?>
        seller_option += '<option value="<?php echo $val['member_id'];?>"><?php echo $val['seller_name'];?></option>';
    <?php } ?>
    <?php } ?>
  $('#callcenter_form').find('.tool select').live('change', function(){
    var obj = $(this).parent().parent();
    var input_obj = obj.find(".number input");
    var input_name = input_obj.attr("name");
    var input_val = input_obj.val();
    var select_val = $(this).val();
    if ( select_val == 3 ) {//选择시스템 자체IM时出现下拉列表并隐藏文本框
        obj.find(".number").html('<input type="hidden" name="'+input_name+'" value="'+input_val+'" /><select name="'+
                  input_name+'">'+seller_option+'</select>');
        obj.find(".number select").val(input_val);
    } else {
        obj.find(".number").html('<input class="text w180" type="text" name="'+input_name+'" value="'+input_val+'" />');
    }
  });
  $('#callcenter_form').find('.tool select').trigger("change");//初始化已有数据
$(function(){
  $('#callcenter_form').find('a[cnbiztype="del"]').live('click', function(){
    $(this).parents('div:first').remove();
  });
  titleTip();
});
function add_service(param){
  if(param == 'pre'){
    var text = '<?php echo $lang['store_callcenter_presales'];?>';
  }else if(param == 'after'){
    var text = '<?php echo $lang['store_callcenter_aftersales'];?>';
  }
  obj = $('dl[cnbiztype="'+param+'"]').children('dd').find('p');
  len = $('dl[cnbiztype="'+param+'"]').children('dd').find('div').length;
  key = 'k'+len+Math.floor(Math.random()*100);
  var add_html = '';
  add_html += '<div class="ncs-message-list">';
  add_html += '<span class="name tip" title="<?php echo $lang['store_callcenter_name_title'];?>">';
  add_html += '<input type="text" class="text w60" value="'+text+len+'" name="'+param+'['+key+'][name]" /></span>';
  add_html += '<span class="tool tip" title="<?php echo $lang['store_callcenter_tool_title'];?>"><select name="'+param+'['+key+'][type]">';
  add_html += '<option class="" value="0"><?php echo $lang['store_callcenter_please_choose'];?></option><option value="1">QQ</option>';
  add_html += '<option value="2"><?php echo $lang['store_callcenter_wangwang'];?></option><option value="3">시스템 자체IM</option></select></span>';
  add_html += '<span class="number tip" title="<?php echo $lang['store_callcenter_number_title'];?>"><input class="text w180" type="text" name="'+param+'['+key+'][num]" /></span>';
  add_html += '<span class="del"><a cnbiztype="del" href="javascript:void(0);" class="ncsc-btn"><i class="icon-trash"></i>삭제</a></span>';
  add_html += '</div>';
  obj.before(add_html);
  titleTip();
}
function titleTip(){
  //title提示
  $('.tip').unbind().poshytip({
    className: 'tip-yellowsimple',
    showTimeout: 1,
    alignTo: 'target',
    alignX: 'center',
    alignY: 'top',
    offsetX: 5,
    offsetY: 0,
    allowTipHover: false
  });
}
</script>