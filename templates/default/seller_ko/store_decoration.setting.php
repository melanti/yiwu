<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form id="form_setting" method="post" action="<?php echo urlShop('store_decoration', 'decoration_setting_save');?>">
    <dl>
      <dt>레이아웃 직접 디자인 설정<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <label for="store_decoration_switch_on" class="mr30">
          <input id="store_decoration_switch_on" type="radio" class="radio vm mr5" name="store_decoration_switch" value="1" <?php echo $output['store_decoration_switch'] > 0?'checked':'';?>>
          예</label>
        <label for="store_decoration_switch_off">
          <input id="store_decoration_switch_off" type="radio" class="radio vm mr5" name="store_decoration_switch" value="0" <?php echo $output['store_decoration_switch'] == 0?'checked':'';?>>
          아니오</label>
        <p class="hint">미니샵 디자인 직접 관리 사용여부 선택；<br/>
          만약 사용을 하시면, 미니샵 메인화면 배경, 레이아웃 등 모두 디자인관리 템플릿의 설정 내에서 진행되어 보여지게 됩니다. <br/>
          만약 사용하시지 않으신다면,<a href="index.php?act=store_setting&op=theme">“미니샵레이아웃”</a> 에서 기본 레이아웃을 설정하시면 됩니다.</p>
      </dd>
    </dl>
    <dl>
      <dt>디자인 내용만 보이기<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <label for="store_decoration_only_on" class="mr30">
          <input id="store_decoration_only_on" type="radio" class="radio vm mr5" name="store_decoration_only" value="1" <?php echo $output['store_decoration_only'] > 0?'checked':'';?>>
          예</label>
        <label for="store_decoration_only_off">
          <input id="store_decoration_only_off" type="radio" class="radio vm mr5" name="store_decoration_only" value="0" <?php echo $output['store_decoration_only'] == 0?'checked':'';?>>
          아니오</label>
        <p class="hint">"예" 선택시, 해당 미니샵의 메인화면에는 직접 설정한 내용만이 나타나게 됩니다.<br/>
         "아니오" 선택시, 기본 표준 스타일 템플릿을 바탕으로 화면상에 내용이 나타납니다. 즉, 좌측에는 미니샵 설명, 판매순서, 우측에는 광고, 최신상품, 추천상품 틍의 미니샵 정보가 나타나게 됩니다.</p>
      </dd>
    </dl>
    <dl>
      <dt>미니샵디자인관리<?php echo $lang['nc_colon'];?></dt>
      <dd> <a href="<?php echo urlShop('store_decoration', 'decoration_edit', array('decoration_id' => $output['decoration_id']));?>" class="ncsc-btn ncsc-btn-acidblue mr5" target="_blank"><i class="icon-puzzle-piece"></i>디자인</a> <a id="btn_build" href="<?php echo urlShop('store_decoration', 'decoration_build', array('decoration_id' => $output['decoration_id']));?>" class="ncsc-btn ncsc-btn-orange" target="_blank"><i class="icon-magic"></i>생성</a>
        <p class="hint">"디자인" 선택시, 미니샵 메인화면 디자인을 설정할 수 있는 새창이 뜨게됩니다.<br/>
          완성 후, "생성"을 클릭하시면 "직접 디자인"한 템플릿이 보관됩니다.</p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border">
        <input id="btn_submit" type="button" class="submit" value="완료" />
      </label>
    </div>
  </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_submit').on('click', function() {
            ajaxpost('form_setting', '', '', 'onerror');
        });

        $('#btn_build').on('click', function() {
            $.getJSON($(this).attr('href'), function(data) {
                if(typeof data.error == 'undefined') {
                    showSucc(data.message);
                } else {
                    showError(data.error);
                }
            });
            return false;
        });
    });
</script> 
