<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="wrap">
  <div class="tabmenu">
    <?php include template('layout/submenu');?>
  </div>
  <div class="alert alert-block mt10">
    <h4>사용방법：</h4>
    <ul>
      <li>1. 시스템 기본 등록 내용: 체크표시를 해제하시면 등록이 자동으로 저장되지 않습니다.</li>
      <li>2. 카테고리명 현황소식을 직접 편집 수정하실 수 있습니다. 입력하시지 않으시면 시스템 기본 설정시의 메세지 내용으로 보여집니다.</li>
    </ul>
  </div>
  <form method="post" id="form_snssetting" action="index.php?act=store_sns&op=setting">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="ncsc-default-table">
      <thead>
        <tr>
          <th class="w100">자동</th>
          <th class="w400">메세지입력</th>
          <th>시스템기본템플릿</th>
        </tr>
      </thead>
      <tbody>
        <tr class="bd-line">
          <td><input type="checkbox" name="new" <?php if(!isset($output['sauto_info']) || $output['sauto_info']['sauto_new'] == 1){?>checked="checked"<?php }?> value="1" /></td>
          <td class="tl">
          <strong>새상품등록</strong>
          <textarea nctype="charcount" class="textarea w400" name="new_title" rows="2" id="new_title"><?php echo $output['sauto_info']['sauto_newtitle'];?></textarea>
            <div id="charcount_new_title" class="tr w400"></div></td>
          <td><p><?php echo $lang['nc_store_auto_share_new1'];?></p>
            <p><?php echo $lang['nc_store_auto_share_new2'];?></p>
            <p><?php echo $lang['nc_store_auto_share_new3'];?></p>
            <p><?php echo $lang['nc_store_auto_share_new4'];?></p>
            <p><?php echo $lang['nc_store_auto_share_new5'];?></p></td>
        </tr>
       
        <tr class="bd-line">
          <td><input type="checkbox" name="xianshi" <?php if(!isset($output['sauto_info']) || $output['sauto_info']['sauto_xianshi'] == 1){?>checked="checked"<?php }?> value="1" /></td>
          <td class="tl"><strong>기간한정세일</strong><textarea nctype="charcount" class="w400" name="xianshi_title" rows="2" id="xianshi_title"><?php echo $output['sauto_info']['sauto_xianshititle'];?></textarea>
            <div id="charcount_xianshi_title" class="tr w400"></div></td>
          <td><p><?php echo $lang['nc_store_auto_share_xianshi1'];?></p>
            <p><?php echo $lang['nc_store_auto_share_xianshi2'];?></p>
            <p><?php echo $lang['nc_store_auto_share_xianshi3'];?></p>
            <p><?php echo $lang['nc_store_auto_share_xianshi4'];?></p>
            <p><?php echo $lang['nc_store_auto_share_xianshi5'];?></p></td>
        </tr>
        <tr class="bd-line">
          <td><input type="checkbox" name="mansong" <?php if(!isset($output['sauto_info']) || $output['sauto_info']['sauto_mansong'] == 1){?>checked="checked"<?php }?> value="1" /></td>
          <td class="tl"><strong>금액별 사은품 증정</strong><textarea nctype="charcount" class="w400" name="mansong_title" rows="2" id="mansong_title"><?php echo $output['sauto_info']['sauto_mansongtitle'];?></textarea>
            <div id="charcount_mansong_title" class="tr w400"></div></td>
          <td><p><?php echo $lang['nc_store_auto_share_mansong1'];?></p>
            <p><?php echo $lang['nc_store_auto_share_mansong2'];?></p>
            <p><?php echo $lang['nc_store_auto_share_mansong3'];?></p>
            <p><?php echo $lang['nc_store_auto_share_mansong4'];?></p>
            <p><?php echo $lang['nc_store_auto_share_mansong5'];?></p></td>
        </tr>
        <tr class="bd-line">
          <td><input type="checkbox" name="bundling" <?php if(!isset($output['sauto_info']) || $output['sauto_info']['sauto_bundling'] == 1){?>checked="checked"<?php }?> value="1" /></td>
          <td class="tl"><strong>할인세트</strong><textarea nctype="charcount" class="w400" name="bundling_title" rows="2" id="bundling_title"><?php echo $output['sauto_info']['sauto_bundlingtitle'];?></textarea>
            <div id="charcount_bundling_title" class="tr w400"></div></td>
          <td><p><?php echo $lang['nc_store_auto_share_bundling1'];?></p>
            <p><?php echo $lang['nc_store_auto_share_bundling2'];?></p>
            <p><?php echo $lang['nc_store_auto_share_bundling3'];?></p>
            <p><?php echo $lang['nc_store_auto_share_bundling4'];?></p>
            <p><?php echo $lang['nc_store_auto_share_bundling5'];?></p></td>
        </tr>
        <tr class="bd-line">
          <td><input type="checkbox" name="groupbuy" <?php if(!isset($output['sauto_info']) || $output['sauto_info']['sauto_groupbuy'] == 1){?>checked="checked"<?php }?> value="1" /></td>
          <td class="tl"><strong>공동구매</strong><textarea nctype="charcount" class="w400" name="groupbuy_title" rows="2" id="groupbuy_title"><?php echo $output['sauto_info']['sauto_bundlingtitle'];?></textarea>
            <div id="charcount_groupbuy_title" class="tr w400"></div></td>
          <td><p><?php echo $lang['nc_store_auto_share_groupbuy1'];?></p>
            <p><?php echo $lang['nc_store_auto_share_groupbuy2'];?></p>
            <p><?php echo $lang['nc_store_auto_share_groupbuy3'];?></p>
            <p><?php echo $lang['nc_store_auto_share_groupbuy4'];?></p>
            <p><?php echo $lang['nc_store_auto_share_groupbuy5'];?></p></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="20" class="bottom"><input type="submit" value="저장" class="submit mt10 mb5" style="display: inline-block;"></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script> 
<script type="text/javascript">
$(function(){
	$('#form_snssetting').validate({
		errorLabelContainer: $('#warning'),
		invalidHandler: function(form, validator) {
			$('#warning').show();
		},
		submitHandler:function(form){
			ajaxpost('form_snssetting', '', '', 'onerror');
		},
		rules : {
			new_title : {
				maxlength : 140
			},
			coupon_title : {
				maxlength : 140
			},
			xianshi_title : {
				maxlength : 140
			},
			mansong_title : {
				maxlength : 140
			},
			bundling_title : {
				maxlength : 140
			},
			groupbuy_title : {
				maxlength : 140
			}
		},
		messages : {
			new_title : {
				maxlength : '최대 140자 이내 입력'
			},
			coupon_title : {
				maxlength : '최대 140자 이내 입력'
			},
			voucher_title : {
				maxlength : '최대 140자 이내 입력'
			},
			mansong_title : {
				maxlength : '최대 140자 이내 입력'
			},
			bundling_title : {
				maxlength : '최대 140자 이내 입력'
			},
			groupbuy_title : {
				maxlength : '최대 140자 이내 입력'
			}
		}
	});

	//评论字符个数动态计算
	$('*[nctype="charcount"]').each(function(){
		$(this).charCount({
			allowed: 140,
			warning: 10,
			counterContainerID:'charcount_'+$(this).attr('id'),
			firstCounterText:'',
			endCounterText:'자 입력하실 수 있습니다. ',
			errorCounterText:'<?php echo $lang['sns_charcount_tip3'];?>'
		});
	});
});
</script>