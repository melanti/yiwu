(function($, angular, _){

  angular.module('App.items')
    .service('BrandTableManager', ['_', '$q' , '$timeout', '$http', 'XHR', 'API_URL_INFO', BrandTableManager]);

  function BrandTableManager(_, $q , $timeout, $http, XHR, API_URL_INFO){
    var vm = this;

    var COUNT_RANGE = 5,
        VIEW_RANGE_LIST = ['10','20','30','50','100'];

    vm._state = {
      tableData: {
        options:{
          query_opt:'name', // name , code
          query:'',
          status:'all'  //   all(default), activated, disabled
        },
        info: {
          start: 0,
          count: '20',
          total: 0
        },
        results: []
      }
    };

    vm.exports = {
      COUNT_RANGE:COUNT_RANGE,
      VIEW_RANGE_LIST:VIEW_RANGE_LIST,
      init:_init,
      getCountRange:function(){return COUNT_RANGE;},
      getViewRangeList:function(){return VIEW_RANGE_LIST;},
      getTableData:_getTableData,
      getTableStateInfo:_getTableStateInfo,
      deleteRow:_deleteRow,
      updateQueryOpt:_updateQueryOpt,
      updateQuery:_updateQuery,
      updateStatus:_updateStatus,
      updateTableData:_updateTableData
    };

    function _init(){
    }

    function _updateTableData(__data){
      var deferred = $q.defer();

      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_BRAND_LIST+'?start='+__data.start+'&count='+__data.count+'&query_opt='+__data.query_opt+'&query='+(__data.query)+'&status='+__data.status+'&show=debug,contact,item_count'
        })
        .then(
          function(__sucData){
            if(__sucData.data.info){
              if( __sucData.data.info.start==null || __sucData.data.info.start=='null' )  {
                __sucData.data.info.start = parseInt(__data.start);
              }
              if( __sucData.data.info.count==null || __sucData.data.info.count=='null' )  {

              }
              __sucData.data.info.count = parseInt(__data.count);
            }
            _setTableData(__sucData.data);
            deferred.resolve(_getTableData());
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }


    function _deleteRow(__data){
      var deferred = $q.defer();

      /*
       ids	ids (separated with comma) - 예) 3,9,7	String
       kind	item, category, brand, 3d	String
       status	status(activated, disabled, deleted)	String
       show	for viewing some information(debug ...)	String
       */

      XHR
        .REQ({
          method: 'post',
          url: API_URL_INFO.CHANGE_STATUS,
          data: {ids: __data.brand_ids, kind:'brand', status:'deleted', show:'debug,contact'}
        })
        .then(
          function(__sucData){
            deferred.resolve(__sucData.data);
          },
          function(__errData){
            deferred.reject(__errData);
          }
        );

      return deferred.promise;
    }

    function _getTableData(){
      return vm._state.tableData;
    }

    function _getTableStateInfo(__key){
      return vm._state.tableData.info[__key];
    }

    function _updateQueryOpt(__query_opt){
      vm._state.tableData.options.query_opt =  __query_opt;
    }

    function _updateQuery(__query){
      vm._state.tableData.options.query =  __query;
    }

    function _updateStatus(__status){
      vm._state.tableData.options.status =  __status;
    }

    function _setTableData(__data){
      if(__data){
        //console.log('sortResultData: ' , sortResultData);
        vm._state.tableData = _.extend({}, vm._state.tableData, {info:__data.info , debug:__data.debug , results:_.sortBy(__data.results, 'reg_date').reverse()} );

      }else{
        vm._state.tableData = _.extend({}, vm._state.tableData);
      }
    }

  }


})(window.jQuery, window.angular, window._);
