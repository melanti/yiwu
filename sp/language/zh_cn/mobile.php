<?php
defined('InCNBIZ') or exit('Access Invalid!');



/**
 * index
 */
$lang['link_index_del_succ']	= '삭제성공';
$lang['link_index_choose_del']	= '삭제할 내용을 선택하세요!';
$lang['link_index_mb_ad']		= '광고관리';
$lang['link_index_title']		= '제목';
$lang['link_index_category']	= '카테고리';
$lang['link_index_pic_sign']	= '전시이미지';
$lang['link_all']               = '전체';
$lang['link_help1']				= '설정된 광고정보는 모바일 메인에 노출됩니다.';
/**
 * 추가
 */
$lang['link_add_count_limit']	= '최대4개까지 동시 발표가능';
$lang['link_add_title_null']	= '타이틀은 비워두실 수 없습니다';
$lang['link_add_sort_int']		= '숫자정렬만 가능합니다.';
$lang['link_add_back_to_list']	= '리스트로 가기';
$lang['link_add_again']			= '계속해서 추가하기';
$lang['link_add_succ']			= '추가성공';
$lang['link_add_fail']			= '추가실패';
$lang['link_add_select']		= '-선택-';
$lang['link_add_name']			= '상품리스트에 표시될 타이틀';
$lang['link_add_href']			= '광고클릭후 해당카테고리 상품검색 ';
$lang['link_add_sign']			= '광고이미지，권장크기 620px * 160px';
$lang['link_add_sort_tip']		= '작은숫자가 더앞으로';
$lang['link_add_category_null']	= '분류를 선택하세요';
$lang['link_add_pic_null']		= '광고이미지를 선택하세요';
$lang['link_add_pic_sign_error']	= '이미지 확장자는 gif, jpg, jpeg, png만 가능 ';

/**
 * index
 */
$lang['link_index_mb_category']	= '이미지 카테고리 설정';
$lang['link_index_category']	= '분류';
$lang['link_help1']				= '설정하신 광고는 모바일 메인에 표시됩니다.';
/**
 * 추가
 */
$lang['link_add_category_exist']= '이미 등록된 카테고리입니다, 수정 혹은 삭제로 해당 정보를 조작할 수 잇습니다.';
$lang['link_add_category_null']	= '카테고리를 선택하세요';

/**
 * index
 */
$lang['home_index_del_succ']	= '삭제성공。';
$lang['home_index_choose_del']	= '삭제할 내용을 선택하세요!';
$lang['home_index_mb_ad']		= '광고관리';
$lang['home_index_title']		= '제목';
$lang['home_index_desc']		= '설명';
$lang['home_index_keyword']		= '키워드';
$lang['home_index_category']	= '카테고리';
$lang['home_index_pic_sign']	= '전시이미지';
$lang['home_all']               = '전체';
$lang['home_help1']				= '모바일 메인내용 설정';
/**
 * 추가
 */
$lang['home_add_count_limit']	= '최대6까지 동시 발표 가능';
$lang['home_add_null']			= '내용을 입력해 주세요';
$lang['home_add_maxlength']		= '6자이상 불가능';
$lang['home_add_sort_int']		= '숫자정렬만 가능합니다';
$lang['home_add_back_to_list']	= '리스트 보기';
$lang['home_add_again']			= '계속해서 추가하기';
$lang['home_add_succ']			= '추가 성공';
$lang['home_add_fail']			= '추가 실패';
$lang['home_add_select']		= '-선택-';
$lang['home_add_name']			= '상품리스트에 표시될 타이틀';
$lang['home_add_desc']			= '상품리스트에 표시될 묘사';
$lang['home_add_keyword']		= '상품리스트에 표시될 키워드';
$lang['home_add_href']			= '광고클릭후 해당카테고리 상품검색';
$lang['home_add_sign']			= '광고이미지，권장크기 80px * 90px';
$lang['home_add_sort_tip']		= '작은숫자가 더앞으로';
$lang['home_add_category_null']	= '분류를 선택해 주세요';
$lang['home_add_pic_null']		= '광고이미지를 선택해 주세요';
$lang['home_add_pic_sign_error']	= '이미지 확장자는gif, jpg, jpeg, png만 가능';
$lang['home_edit_again']	= '다시편집하기';
$lang['home_edit_succ']		= '저장';
$lang['home_edit_fail']		= '저장 실패';
/**
 * 수정
 */
$lang['link_edit_again']	= '재수정';
$lang['link_edit_succ']		= '저장 성공';
$lang['link_edit_fail']		= '저장 실패';

/**
 * 의견트랙백
 */
$lang['feedback_mange_title'] 	= '의견트랙백';
$lang['feedback_del_succ'] 		= '삭제성공';
$lang['feedback_del_fiald'] 	= '삭제실패';
$lang['feedback_index_content'] = '트랙백 내용';
$lang['feedback_index_time'] 	= '시간';
$lang['feedback_index_from'] 	= 'From';

?>
