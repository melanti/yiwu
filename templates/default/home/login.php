<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/taglibs.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/tabulous.js"></script>

<div class="nc-login-layout">
  <div class="left-pic"><img src="<?php echo $output['lpic'];?>"  border="0"></div>
  <div class="nc-login">
  <!-- <div class="arrow"></div> -->
    <div class="nc-login-mode">
      <ul class="tabs-nav">
        <li><a href="#default" class="tabulous_active">用户登录<i></i></a></li>
                <li><a href="#mobile">手机动态码登录<i></i></a></li><span class="tabulousclear"></span>
              </ul>
      <div class="tabs-container" id="tabs_container" style="height: 268px;">
        <div class="tabs-content" id="default" style="position: absolute; top: 0px;">
          <form action="index.php?act=login&op=login" method="post" class="nc-login-form" id="login_form">
        <?php Security::getToken();?>
        <input type="hidden" name="form_submit" value="ok" />
        <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
            <dl>
              <dt>账&nbsp;&nbsp;&nbsp;号：</dt>
              <dd>
                <input type="text" id="user_name" placeholder="可使用已注册的用户名或手机号登录" name="user_name" autocomplete="off" class="text" value="">
              </dd>
            </dl>
            <dl>
              <dt>密&nbsp;&nbsp;&nbsp;码：</dt>
              <dd>
                <input type="password" id="password" placeholder="6-20个大小写英文字母、符号或数字" autocomplete="off" name="password" class="text" value="">
              </dd>
            </dl>
                        <div class="code-div mt15">
              <dl>
                <dt>验证码：</dt>
                <dd>
                  <input type="text" size="10" id="captcha" placeholder="输入验证码" class="text w100" autocomplete="off" name="captcha" value="">                  
                </dd>
              </dl>
              <span><img id="codeimage" name="codeimage" src="<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>"> <a onclick="javascript:document.getElementById('codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();" href="javascript:void(0)" class="makecode">看不清，换一张</a></span></div>
                        <div class="handle-div">
            <span class="auto"><input type="checkbox" value="1" name="auto_login" class="checkbox">七天自动登录<em style="display: none;">请勿在公用电脑上使用</em></span>
            <a href="index.php?act=login&op=forget_password" class="forget">忘记密码？</a></div>
            <div class="submit-div">
              <input type="submit" value="登&nbsp;&nbsp;&nbsp;录" class="submit">
            <input type="hidden" value="<?php echo $_GET['ref_url']?>" name="ref_url">
            </div>
          </form>
        </div>
        <div class="tabs-content hideflip" id="mobile" style="position: absolute; top: 0px;">
          <form action="index.php?act=sms_login&op=login" class="nc-login-form" method="post" id="post_form">
        <?php Security::getToken();?>
        <input type="hidden" name="form_submit" value="ok" />
        <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
            <dl>
              <dt>手机号：</dt>
              <dd>
                <input type="text" value="" autocomplete="off" placeholder="可填写已注册的手机号接收短信" id="phone" class="text" name="phone">              
              </dd>
            </dl>
            <div class="code-div">
              <dl>
                <dt>验证码：</dt>
                <dd>
                  <input type="text" size="10" id="image_captcha" placeholder="输入验证码" class="text w100" name="captcha" value="">
                 
                </dd>
              </dl>
              <span><img id="sms_codeimage" name="codeimage" title="看不清，换一张" src="<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>"> <a onclick="javascript:document.getElementById('sms_codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();" href="javascript:void(0);" class="makecode">看不清，换一张</a></span> </div>
            
            <div id="sms_text" class="tiptext">正确输入上方验证码后，点击<span> <a onclick="get_sms_captcha('2')" href="javascript:void(0);"><i class="icon-mobile-phone"></i>发送手机动态码</a></span>，查收短信将系统发送的“6位手机动态码”输入到下方验证后登录。</div>
            <dl>
              <dt>动态码：</dt>
              <dd>
                <input type="text" size="15" id="sms_captcha" placeholder="输入6位手机动态码" class="text" autocomplete="off" name="sms_captcha" value="">
                
              </dd>
            </dl>
            <div class="submit-div">
                <input type="submit" value="登&nbsp;&nbsp;&nbsp;录" class="submit" id="submit">
            <input type="hidden" value="<?php echo $_GET['ref_url']?>" name="ref_url">
              </div>
          </form>
        </div>
              </div>
    </div>
    <div id="demo-form-site" class="nc-login-api">
            <h4>您可以用合作伙伴账号登录：</h4>
            <?php if ($output['setting_config']['qq_isuse'] == 1 || $output['setting_config']['sina_isuse'] == 1){?>
          <?php if ($output['setting_config']['qq_isuse'] == 1){?>
          <a href="<?php echo SHOP_SITE_URL;?>/api.php?act=toqq" title="QQ账号登录" class="qq"><i></i>QQ</a>
          <?php } ?>
          <?php if ($output['setting_config']['sina_isuse'] == 1){?>
          <a href="<?php echo SHOP_SITE_URL;?>/api.php?act=tosina" title="新浪微博账号登录" class="sina"><i></i>新浪微博</a>
          <?php } ?>
                  <!-- <a class="wx" title="微信账号登录" onclick="ajax_form('weixin_form', '微信账号登录', 'index.php?act=connect_wx&op=index', 360);" href="javascript:void(0);"><i></i>微信</a> -->
      <?php } ?>
     </div>
  </div>
  <div class="clear"></div>
</div>


<script>
$(function(){
    jQuery.validator.addMethod("mobile", function(value, element) {
      return this.optional(element) || /^1[3|4|5|8]\d{9}$/.test(value);
    }, "mobile number error"); 
  //初始化Input的灰色提示信息  
  //$('input[tipMsg]').inputTipText({pwd:'password'});
  //登录方式切换
  $('.nc-login-mode').tabulous({
     effect: 'flip'//动画反转效果
  }); 
  var div_form = '#default';
  $(".nc-login-mode .tabs-nav li a").click(function(){
        if($(this).attr("href") !== div_form){
            div_form = $(this).attr('href');
            $(""+div_form).find(".makecode").trigger("click");
      }
  });
  
  $("#login_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
      submitHandler:function(form){
          ajaxpost('login_form', '', '', 'onerror');
      },
        onkeyup: false,
    rules: {
        user_name: {
          required:true
        },
        password: {
          required:true
        }
        <?php if(C('captcha_status_login') == '1') { ?>
        ,captcha : {
                required : true,
                remote   : {
                    url : '<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                          document.getElementById('codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            }
      <?php } ?>
          },
    messages: {
      user_name: "请输入已注册的用户名或手机号",
      password: "密码不能为空",
      captcha : {
        required : '验证码错误',
        remote   : '验证码错误'
      }
    }
  });

    // 勾选自动登录显示隐藏文字
    $('input[name="auto_login"]').click(function(){
        if ($(this).attr('checked')){
            $(this).attr('checked', true).next().show();
        } else {
            $(this).attr('checked', false).next().hide();
        }
    });
});
</script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/connect_sms.js" charset="utf-8"></script>
<script>
$(function(){
  $("#post_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
      submitHandler:function(form){
          ajaxpost('post_form', '', '', 'onerror');
      },
        onkeyup: false,
    rules: {
      phone: {
                required : true,
                mobile : true
            }
      <?php if(C('captcha_status_login') == '1') { ?>
      ,captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : '<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#image_captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                          document.getElementById('sms_codeimage').src='<?php echo SHOP_SITE_URL?>/index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            }
      <?php } ?>
      ,sms_captcha: {
                required : function(element) {
                    return $("#image_captcha").val().length == 4;
                },
                minlength: 6
            }
    },
    messages: {
      phone: {
                required : '请输入正确的手机号',
                mobile : '请输入正确的手机号'
            },
      captcha : {
                required : '验证码错误',
                minlength: '验证码错误',
        remote   : '验证码错误'
            },
      sms_captcha: {
                required : '请输入六位短信动态码',
                minlength: '请输入六位短信动态码'
            }
    }
  });
});
</script>