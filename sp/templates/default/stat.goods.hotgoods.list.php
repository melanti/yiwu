<?php defined('InCNBIZ') or exit('Access Invalid!');?>
	<div id="container_<?php echo $output['stat_field'];?>" class="w100pre close_float" style="height:400px"></div>
	
  <table class="table tb-type2 nobdb">
  	<thead class="thead">
		<tr class="space">
			<th colspan="15">인기판매상품TOP50</th>
		</tr>
		<tr class="thead sortbar-array">
			<th class="align-center w18pre">번호</th>
            <th class="align-center">상품명</th>
            <th class="align-center"><?php echo $output['sort_text'];?></th>
        </tr>
    </thead>
    <tbody id="datatable">
    <?php if(!empty($output['statlist'])){ ?>
        <?php foreach ((array)$output['statlist'] as $k=>$v){?>
          <tr class="hover">
          	<td><?php echo $v['sort'];?></td>
          	<td class="alignleft"><a href='<?php echo urlShop('goods', 'index', array('goods_id' => $v['goods_id']));?>' target="_blank"><?php echo $v['goods_name'];?></a></td>
          	<td><?php echo $v[$output['stat_field']];?></td>
          </tr>
        <?php } ?>
    <?php } else {?>
        <tr class="no_data">
        	<td colspan="11"><?php echo $lang['no_record']; ?></td>
        </tr>
    <?php }?>
    </tbody>
  </table>

<script>
$(function () {
	$('#container_<?php echo $output['stat_field'];?>').highcharts(<?php echo $output['stat_json'];?>);
});
</script>