<?php
/**
 * 번역자
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class translate_myControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('translate');
	}

	public function indexOp(){
        $model_goods = Model ( 'goods' );
        /**
         * 处理상품 카테고리
         */
        $choose_gcid = ($t = intval($_REQUEST['choose_gcid']))>0?$t:0;
        $gccache_arr = Model('goods_class')->getGoodsclassCache($choose_gcid,3);
	    Tpl::output('gc_json',json_encode($gccache_arr['showclass']));
		Tpl::output('gc_choose_json',json_encode($gccache_arr['choose_gcid']));
        /**
         * 검색条件
         */
        $where = array();
        if ($_GET['search_goods_name'] != '') {
            $where['goods_name_ko'] = array('like', '%' . trim($_GET['search_goods_name']) . '%');
        }
        if (intval($_GET['search_commonid']) > 0) {
            $where['goods_commonid'] = intval($_GET['search_commonid']);
        }

        $getAdminInfo = $this->getAdminInfo();

        $where['t_uid'] = intval($getAdminInfo['id']);

        $trans_type = $getAdminInfo['t_type'];  //관리자 테이블에서 유형을 얻는다.
        if($trans_type==1)
        {
            $trans_status = "1,11";
            $v_trans_status = 4;
            $f_trans_status = 7;
        }
        elseif($trans_type==2)
        {
            $trans_status = "2,11";
            $v_trans_status = 5;
            $f_trans_status = 8;
        }
        elseif($trans_type==3)
        {
            $trans_status = "3,11";
            $v_trans_status = 6;
            $f_trans_status = 9;
        }
        else
        {
            $trans_status = "3,11";
            $v_trans_status = 6;
            $f_trans_status = 9;
        }

        $setVerify = array(
            '1' => '번역중',
            '2' => '디자인중',
            '3' => '번역디자인중',
            '4' => '번역완료',
            '5' => '디자인완료',
            '6' => '번역디자인완료',
            '7' => '번역승인완료',
            '8' => '디자인승인완료',
            '9' => '번역디자인승인완료',
            '10' => '최종승인완료',
            '11' => '위반',
            '12'=>'미분배'
        );

        switch ($_GET['type']) {
            case 'lockup':
                $where['t_status'] = intval($f_trans_status);
                $goods_list = $model_goods->getGoodsCommonListForTranslate($where);
                break;
            // 심사대기중
            case 'waitverify':
                $where['t_status'] = intval($v_trans_status);
                $goods_list = $model_goods->getGoodsCommonListForTranslate($where, '*', 10, 'goods_commonid desc');
                break;
            // 전체상품
            default:
                $where['t_status'] = array("in",$trans_status);
                $goods_list = $model_goods->getGoodsCommonListForTranslate($where);
                break;
        }

        Tpl::output('getVerify', $setVerify);
        Tpl::output('goods_list', $goods_list);
                // 计算库存
        $storage_array = $model_goods->calculateStorage($goods_list);
        Tpl::output('storage_array', $storage_array);

        Tpl::output('page', $model_goods->showpage(10));

        // 브랜드

        switch ($_GET['type']) {
            // 禁售
            case 'lockup':
                Tpl::showpage('translate_my.lockup');
                break;
            // 심사대기중
            case 'waitverify':
                Tpl::showpage('translate_my.waitverify');
                break;
            // 전체상품
            default:
                Tpl::showpage('translate_my.index');
                break;
            }
    }

    public function calcOp(){

            $model_goods = Model('goods');
        $getAdminInfo = $this->getAdminInfo();
        $t_uid = intval($_GET['tuid']);
        //정산 요청
        if(!empty($t_uid) && $getAdminInfo['id']==$t_uid){
            $bill_time = time();

            $insert = array();
            $insert['bill_uid'] = $getAdminInfo['id'];
            $insert['bill_time'] = $bill_time;
            $insert['bill_total_price'] = 0;
            $insert['bill_status'] = 1;


            $bill_id = $model_goods->addTranslateBill($insert);
            if($bill_id)
            {
                $update = array();
                $update['t_adj_starttime'] = $bill_time;
                $update['t_bill_id'] = $bill_id;

                $where = array();
                $where['t_uid'] = $t_uid;
                $where['t_status'] = array("in", "7,8,9");


                 $model_goods->editTranslateById($update,$where);

            }


             showDialog("정산요청 완료",'index.php?act=translate_my&op=calc','succ');
            exit;
        }



        $t_type_name = array(
            1=>'번역',
            2=>'디자인',
            3=>'디자인+번역'
            );
        $model = Model();
        $parray = array();

        $condition = array('t_uid'=>$getAdminInfo['id']);
        $parray['cnt_all'] = $model->table('translate')->where($condition)->count();


        $condition = array('t_uid'=>$getAdminInfo['id'],'t_status'=>array('in',array(1,2,3,11)));
        $parray['cnt_v'] = $model->table('translate')->where($condition)->count();

        $condition = array('t_uid'=>$getAdminInfo['id'],'t_status'=>array('in',array(4,5,6)));
        $parray['cnt_w'] = $model->table('translate')->where($condition)->count();


        $condition = array('t_uid'=>$getAdminInfo['id'],'t_status'=>array('in',array(7,8,9)),'t_bill_id'=>0);
        $parray['cnt_o'] = $model->table('translate')->where($condition)->count();

        $parray['total'] = $parray['cnt_o']*$getAdminInfo['t_price'];

        // $admin_info = $model->table('admin')->where(array('admin_id'=>$pstUserId))->find();
        $cond = array('bill_uid'=>$getAdminInfo['id']);
        $rs_bill = $model_goods->getTransBillList($cond);  //여기는 정산 내역을 불러오는 부분입니다.
        $bill_histore_list = array();
        foreach($rs_bill as $k=>$v):
            $condition1 = array('t_bill_id'=>$v['bid']);
            $v['total'] = $model->table('translate')->where($condition1)->count();
            $bill_histore_list[$k] = $v;
        endforeach;

        Tpl::output('bills', $bill_histore_list);
        Tpl::output('price', $parray);
        Tpl::output('ttype', $t_type_name);
        Tpl::output('userinfo', $getAdminInfo);
        Tpl::showpage('translate_my.calc');
    }

    public function translateOp()
    {
        if(chksubmit()){

                $goods_commonid = intval($_POST['goods_commonid']);

                $model_goods = Model('goods');
                $goods_info = $model_goods->getGoodeCommonInfoByID($goods_commonid);
                $getAdminInfo = $this->getAdminInfo();

                $param  = array();
                $trans_type = $getAdminInfo['t_type'];
                $trans_user = $getAdminInfo['name'];
                $trans_uid = $getAdminInfo['id'];
                if($trans_type==1)
                {
                    $trans_status = 4;
                }
                elseif($trans_type==2)
                {
                    $trans_status = 5;
                }
                elseif($trans_type==3)
                {
                    $trans_status = 6;
                }
                else
                {
                    $trans_status = 6;
                }
                $translate_id    = intval($_POST['translate_id']);
                $goods_commonid    = intval($_POST['goods_commonid']);
                $param['t_commonid'] = trim($_POST['goods_commonid']);
                $param['t_goods_name'] = trim($_POST['goods_name']);
                $param['t_goods_jingle'] = trim($_POST['goods_jingle']);
                $param['t_goods_body']= trim($_POST['goods_body']);
                $param['t_status']  = $trans_status;
                $param['t_type']  = $trans_type;
                $param['t_uid']  = $trans_uid;
                $param['t_user']  = $trans_user;
                $param['t_transtime']  = time();
                $model_goods  = Model('goods');

                $result1 = $model_goods->editTranslate($param,array('aid'=>$translate_id));

                $param1['goods_name'] = trim($_POST['goods_name']);
                $param1['goods_jingle'] = trim($_POST['goods_jingle']);
                $param1['goods_body']= trim($_POST['goods_body']);
                $param1['goods_body_ko']= !empty($goods_info['goods_body_ko']) ? $goods_info['goods_body_ko'] : $goods_info['goods_body'];
                $param1['aid'] = $translate_id;

                $result = $model_goods->editGoodsCommon($param1,array('goods_commonid'=>$goods_commonid));

                if ($result){
                    /**
                     * 업데이트이미지信息ID
                     */
                    $model_upload = Model('upload');
                    if (is_array($_POST['file_id'])){
                        foreach ($_POST['file_id'] as $k => $v){
                            $v = intval($v);
                            $update_array = array();
                            $update_array['upload_id'] = $v;
                            $update_array['item_id'] = intval($_POST['translate_id']);
                            $model_upload->update($update_array);
                            unset($update_array);
                        }
                    }


                    $this->log($getAdminInfo['name'].'님이 상품 번역 등록[ID:'.$_POST['goods_commonid'].']',1);
                    if(empty($_GET['tp'])):
                        showMessage("성공적으로 등록되었습니다.","index.php?act=translate_my");
                    else:
                        showMessage("성공적으로 등록되었습니다.","index.php?act=translate_my&type=waitverify");
                    endif;
                    exit;
             }
         }


        $common_id = intval($_GET['goods_id']);

        $model_goods = Model('goods');
        $goods_info = $model_goods->getTranslateInfoByID($common_id);

        $model_upload = Model('upload');
        $condition['upload_type'] = '7';
        $condition['item_id'] = $goods_info['aid'];
        $file_upload = $model_upload->getUploadList($condition);
        if (is_array($file_upload)){
            foreach ($file_upload as $k => $v){
                $tmps_ext = explode(".", $file_upload[$k]['file_name']);
                $tmps_ext = $tmps_ext[count($tmps_ext) - 1];
                $t_ext = strtolower($tmps_ext);
                $allow_noimg=array('hwp','xls','xlsx','zip','rar','doc','docx');

                if(!in_array($t_ext,$allow_noimg))
                {
                    $file_upload[$k]['cfile'] = true;
                }
                else
                {
                    $file_upload[$k]['cfile'] = false;
                }
                $file_upload[$k]['upload_path'] = UPLOAD_SITE_URL.'/translate/'.$file_upload[$k]['file_name'];
            }
        }
        Tpl::output('file_upload',$file_upload);
        Tpl::output('goods', $goods_info);
        if(!empty($_GET['type']) && $_GET['type']=="yes")
        {
            Tpl::showpage('translate_my.view');
        }
        else
        {
            Tpl::showpage('translate_my.translate');
        }

    }

    /**
     * 시스텀文章이미지 업로드
     */
    public function translate_pic_uploadOp(){
        /**
         * 上传이미지
         */
        $upload = new UploadFile();
        $upload->set('default_dir','translate');

        $result = $upload->upfile('fileupload');
        if ($result){
            $_POST['pic'] = $upload->file_name;
        }else {
            echo 'error';exit;
        }
        /**
         * 模型实例化
         */
        $model_upload = Model('upload');
        /**
         * 이미지数据入库
        */
        $insert_array = array();
        $insert_array['file_name'] = $_POST['pic'];
        $insert_array['upload_type'] = '7';
        $insert_array['file_size'] = $_FILES['fileupload']['size'];
        $insert_array['item_id'] = intval($_POST['goods_id']);
        $insert_array['upload_time'] = time();
        $result = $model_upload->add($insert_array);
        if ($result){
            $data = array();
            $data['file_id'] = $result;
            $data['file_name'] = $_POST['pic'];
            $data['file_path'] = $_POST['pic'];
            $allow_noimg=array('hwp','xls','xlsx','zip','rar','doc','docx');

            if(!in_array($upload->extn,$allow_noimg))
            {
                $data['file_ext'] = true;
            }
            else
            {
                $data['file_ext'] = false;
            }
            /**
             * 整理为json格式
             */
            $output = json_encode($data);
            echo $output;
        }

    }
    /**
     * ajax조작
     */
    public function ajaxOp(){
        switch ($_GET['branch']){
            /**
             * 삭제文章이미지
             */
            case 'del_file_upload':
                if (intval($_GET['file_id']) > 0){
                    $model_upload = Model('upload');
                    /**
                     * 삭제이미지
                     */
                    $file_array = $model_upload->getOneUpload(intval($_GET['file_id']));
                    @unlink(BASE_UPLOAD_PATH.DS.'translate'.DS.$file_array['file_name']);
                    /**
                     * 삭제信息
                     */
                    $model_upload->del(intval($_GET['file_id']));
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
        }
    }

    /**
     * 번역심사
     */
    public function remarkOp() {
        $model_goods = Model('goods');
        $aid = intval($_GET['id']);

        $rs = $model_goods->getTranslateRow(array('aid'=>$aid));

        Tpl::output('data',$rs);
        Tpl::showpage('translate.remark', 'null_layout');
    }

}
?>