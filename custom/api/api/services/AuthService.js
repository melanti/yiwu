var crypto = require('crypto');
var debug = require('../controllers/libs/debug.js');
var key = sails.config.auth.key;
var iv = sails.config.auth.iv;
var AES = 'aes-128-cbc';

module.exports = {
    checkCode: _checkCode,
    encrypt: _encrypt,
    decrypt: _decrypt
};

function _checkCode(req,res,next) {

    var account_uuid = req.param('account_uuid');
    sails.log.debug('_checkCode :::', req.param('account_uuid'));

    try {

        var decrypt = _decrypt(account_uuid);
        req.params.account_uuid = decrypt;
        sails.log.debug('decrypt ::: ', req.params.account_uuid);
        next();
    } catch(e) {
        var status = 400;
        return res.send(status, debug.wrap(req, res, status, new CustomError(400, 'ER_AUTHENTICATION', '')));
    }
}

function _encrypt(text) {
    var cipher = crypto.createCipheriv(AES, key, iv);
    var encrypted = cipher.update(text, 'utf8', 'base64');
    encrypted += cipher.final('base64');

    return encrypted;
}

function _decrypt(text) {
    var decipher = crypto.createDecipheriv(AES, key, iv);
    var decrypted = decipher.update(text, 'base64', 'utf8');
    decrypted += decipher.final('utf8');

    return decrypted;
}