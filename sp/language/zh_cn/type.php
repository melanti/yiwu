<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['type_index_related_fail']			= '부분적 정보추가 가 실패 하였습니다. 다시한번 시도해 주세요.';
$lang['type_index_continue_to_dd']			= '계속해서 유형추가';
$lang['type_index_return_type_list']		= '유형목록으로 가기';
$lang['type_index_del_succ']				= '유형삭제 성공';
$lang['type_index_del_fail']				= '유형삭제 실패';
$lang['type_index_del_related_attr_fail']	= '관련 속성 삭제';
$lang['type_index_del_related_brand_fail']	= '관련 브랜드 삭제 실패';
$lang['type_index_del_related_type_fail']	= '관련 규격 삭제 실패';
$lang['type_index_type_name']				= '유형';
$lang['type_index_no_checked']				= '설정할 데이터 항목을 선택해 주세요';
$lang['type_index_prompts_one']				= '관리자가 상품분류추가시 유형을 선택해야 합니다. 앞단분류상품목록 페이지는 유형을 통해 검색을 생성하고 유저가 상품검색시 더 편리합니다.';
/**
 * 추가属性
 */
$lang['type_add_related_brand']				= '관련 브랜드를 선택하세요';
$lang['type_add_related_spec']				= '관련 규격을 선택하세요';
$lang['type_add_remove']					= '삭제';
$lang['type_add_name_no_null']				= '유형 이름을 입력하세요';
$lang['type_add_name_max']					= '유형 이름은 1~20자 이내로 입력하세요';
$lang['type_add_sort_no_null']				= '유형 정렬을 입력하세요';
$lang['type_add_sort_no_digits']			= '정수로 입력하세요';
$lang['type_add_sort_desc']					= '자연수를 입력하세요. 숫자가 작은 순위로 정렬됩니다.';
$lang['type_add_spec_name']					= '규격이름';
$lang['type_add_spec_value']				= '규격값';
$lang['type_add_spec_null_one']				= '등록된 규격이 없습니다.';
$lang['type_add_spec_null_two']				= '규격을 추가하세요!';
$lang['type_add_brand_null_one']			= '등록된 브랜드가 없습니다.';
$lang['type_add_brand_null_two']			= '브랜드를 추가하세요!';
$lang['type_add_attr_add']					= '속성추가';
$lang['type_add_attr_add_one']				= '한개속성 추가';
$lang['type_add_attr_add_one_value']		= '한개속성값 추가';
$lang['type_add_attr_name']					= '속성이름';
$lang['type_add_attr_value']				= '속성 선택값';
$lang['type_add_prompts_one']				= '관련 규격은 필수사항이 아닙니다, 상품 발표시 해당 규격이 노출됩니다, 미입력시 규격은 노출안됩니다.';
$lang['type_add_prompts_two']				= '관련 브랜드는 필수사항이 아닙니다, 상품 발표시 노출됩니다.';
$lang['type_add_prompts_three']				= '속성값은 다수 추가가 가능합니다, 매 속성값은 콤마","로 분리해주세요.';
$lang['type_add_prompts_four']				= '선택된 속성값의 "노출"은 상품 리스트 페이지에 노출됩니다.';
$lang['type_add_spec_must_choose']			= '최소 하나의 규격을 선택하세요';
$lang['type_common_checked_hide']			= '미선택항 숨김';
$lang['type_common_checked_show']			= '전체노출';
$lang['type_common_belong_class']			= '소속카테고리';
$lang['type_common_belong_class_tips']		= '카테고리 선택, 상위/하위 카테고리를 선택하세요';
/**
 * 수정属性
 */
$lang['type_edit_type_value_null']			= '유형값 정보를 입력하세요';
$lang['type_edit_type_value_del_fail']		= '유형값 정보 삭제 실패.';
$lang['type_edit_type_attr_edit']			= '속성수정';
$lang['type_edit_type_attr_is_show']		= '노출여부';
$lang['type_edit_type_attr_name_no_null']	= '속성값 이름을 입력하세요';
$lang['type_edit_type_attr_name_max']		= '속성값 이름은 최대 10자입니다.';
$lang['type_edit_type_attr_sort_no_null']	= '정렬을 입력하세요';
$lang['type_edit_type_attr_sort_no_digits']	= '정렬은 반드시 숫자로 입력하세요';
$lang['type_edit_type_attr_edit_succ']		= '속성 수정 성공';
$lang['type_edit_type_attr_edit_fail']		= '속성 수정 실패';
$lang['type_attr_edit_name_desc']			= '자주 사용하는 상품 속성을 입력하세요; 예; 목재,가격범위등.';
$lang['type_attr_edit_sort_desc']			= ' 자연수를 입력하세요. 속성 리스트는 숫자가 작은 순위로 우선 노출됩니다.';