<?php
/**
 * 登录
 *
 * 包括 登录 验证 退出 조작
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class LoginControl extends SystemControl {

	/**
	 * 不进行父类的登录验证，所以증가构造方法重写了父类的构造方法
	 */
	public function __construct(){
		Language::read('common,layout,login');
	    $result = chksubmit(true,true,'num');
		if ($result){
		    if ($result === -11){
		        showMessage('알 수 없는 오류!');
		    }elseif ($result === -12){
		        showMessage(L('login_index_checkcode_wrong'));
		    }
		    if (process::islock('admin')) {
		        showMessage('도배조작으로 인해 잠시후 다시 시도해주세요!');
		    }
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
			array("input"=>$_POST["user_name"],		"require"=>"true", "message"=>L('login_index_username_null')),
			array("input"=>$_POST["password"],		"require"=>"true", "message"=>L('login_index_password_null')),
			array("input"=>$_POST["captcha"],		"require"=>"true", "message"=>L('login_index_checkcode_null')),
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(L('error').$error);
			}else {

				$model_admin = Model('admin');
				$array	= array();
				$array['admin_name']	= $_POST['user_name'];
				$array['admin_password']= md5(trim($_POST['password']));
				$admin_info = $model_admin->infoAdmin($array);
				if(is_array($admin_info) and !empty($admin_info)) {

					$this->systemSetKey(array(
						'name'=>$admin_info['admin_name'], 
						'id'=>$admin_info['admin_id'],
						'gid'=>$admin_info['admin_gid'],
						'sp'=>$admin_info['admin_is_super'],
						't_type'=>$admin_info['admin_trans_type'],
						't_price'=>$admin_info['admin_trans_price'],
						't_memo'=>$admin_info['admin_trans_memo'],
						't_status'=>$admin_info['admin_trans_status']
						));
					$update_info	= array(
					'admin_id'=>$admin_info['admin_id'],
					'admin_login_num'=>($admin_info['admin_login_num']+1),
					'admin_login_time'=>TIMESTAMP
					);
					$model_admin->updateAdmin($update_info);
					$this->log(L('nc_login'),1);
					process::clear('admin');
					
					
					/****************************************************************
					 *  추가된부분
					 *
					 *  시스템 관리자 로그인시 생성된 시크릿키를 시크릿키 테이블에 저장하고
					 *  $admin_info['secret_key']에 시크릿키를 추가한후 HTML 페이지에 전달
					 ***************************************************************/					
					if (method_exists('Security', 'createSecretKey')) {
						$model_secret = Model("secret");
						
						// 시크릿키 저장
						$secret_key = Security::createSecretKey();
						if ($secret_key) {
							$array_input = array();
							$array_input['member_id'] = $admin_info['admin_id'];
							$array_input['secret_hash'] = $secret_key;
							$array_input['regist_ip'] = sprintf('%u', ip2long($_SERVER['REMOTE_ADDR']));  //  decode long2ip($ip);
							$array_input['regist_date'] = TIMESTAMP;
							$_SESSION['account_type'] = 'admin';
							$save_secretkey = $model_secret->saveSecretkey($array_input);
							if ($save_secretkey) {
								$_SESSION['member_id'] = $admin_info['admin_id'];
								$_SESSION['secret_key'] = $secret_key;
								$admin_info['secret_key'] = $secret_key;
								
								$encrypt_key = Security::getCryptMemberId($admin_info['admin_id']);
								log::record("system admin account uuid ". $encrypt_key, LOG::ERR);
								$_SESSION['account_uuid'] = $encrypt_key;	
								
							} else {
								$message = "secret_key save fail at system admin login";
								log::record($message, LOG::ERR);
								
								setNcCookie('sys_key','',-1,'',null);
								session_unset();
								session_destroy();
								showMessage('인증 오류');
								@header("Location: index.php");								
							}
						} else {
							$message = "secret_key save fail at seller login";
							log::record($message, LOG::ERR);
							
							setNcCookie('sys_key','',-1,'',null);
							session_unset();							
							session_destroy();
							showMessage('인증 오류');
							@header("Location: index.php");
							
						}
					}					
					
					@header('Location: index.php');exit;
				}else {
				    process::addprocess('admin');
					showMessage(L('login_index_username_password_wrong'),'index.php?act=login&op=login');
				}
			}
		}
		Tpl::output('html_title',L('login_index_need_login'));
		Tpl::showpage('login','login_layout');
	}
	public function loginOp(){}
	public function indexOp(){}
}
