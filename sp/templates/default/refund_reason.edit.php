<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['refund_manage'];?></h3>
      <ul class="tab-base">
        <li><a href="index.php?act=refund&op=refund_manage"><span><?php echo '처리대기';?></span></a></li>
        <li><a href="index.php?act=refund&op=refund_all"><span><?php echo '전체기록';?></span></a></li>
        <li><a href="index.php?act=refund&op=reason"><span><?php echo '환불반품원인';?></span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span><?php echo '수정원인';?></span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="post_form" method="post" name="form1" action="index.php?act=refund&op=edit_reason&reason_id=<?php echo $output['reason']['reason_id']; ?>">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <tbody>
      <tr class="noborder">
          <td colspan="2" class="required"><label class="validation" for="reason_info_ko">원인(한국어):</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input id="reason_info_ko" name="reason_info_ko" value="<?php echo $output['reason']['reason_info_ko']?>" class="txt" type="text"></td>
          <td class="vatop tips"></td>
        </tr>
      <tr class="noborder">
          <td colspan="2" class="required"><label class="validation" for="reason_info">원인(중국어):</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input id="reason_info" name="reason_info" value="<?php echo $output['reason']['reason_info']?>" class="txt" type="text"></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="sort"><?php echo $lang['nc_sort'];?>:</label>
            </td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="<?php echo $output['reason']['sort']?>" name="sort" id="sort" class="txt"></td>
          <td class="vatop tips">0~255이내의 숫자를 입력하세요, 작은 숫자로 정렬됩니다.</td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15" ><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
	$("#submitBtn").click(function(){
        if($("#post_form").valid()){
            $("#post_form").submit();
    	}
	});
	$("#post_form").validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        rules : {
            reason_info_ko : {
                required : true
            },
            sort : {
                required : true,
                digits   : true
            }
        },
        messages : {
            reason_info_ko : {
                required : "원인 내용을 입력하세요"
            },
            sort  : {
                required : "정렬은 반드시 숫자로 입력하세요",
                digits   : "정렬은 반드시 숫자로 입력하세요"
            }
        }
	});
});

</script>
