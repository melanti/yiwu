<?php
/**
 * 번역자
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class translate_statsControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('translate');
	}

	public function indexOp(){
                Tpl::showpage('translate_stats.index');
	}
}
?>