<?php defined('InCNBIZ') or exit('Access Invalid!');?>

  <div class="tabmenu">
    <?php include template('layout/submenu');?>
  </div>
  <div class="ncsc-form-default">
    <form method="post" action="index.php?act=store_deliver_set&op=print_set" enctype="multipart/form-data" id="my_store_form">
      <input type="hidden" name="form_submit" value="ok" />
      <dl class="setup">
        <dt>메세지입력</dt>
        <dd><textarea name="store_printdesc" cols="150" rows="3" class="textarea w400" id="store_printdesc"><?php echo $output['store_info']['store_printdesc'];?></textarea>
          <p class="hint"> 메세지는 주문서의 하단에 출력되어 나타납니다. 미니샵소개 혹은 배송, 환불, 교환등의 정보를 정확히 입력해 주세요.<br/><span class="orange">최대100자</span></p>
        </dd>
      </dl>
      <dl class="setup">
        <dt>워터마크이미지</dt>
        <dd>
          <input type="hidden" name="store_stamp_old" value="<?php echo $output['store_info']['store_stamp'];?>" />
          <div class="ncsc-upload-thumb watermark-pic">
          	<p>
          	<img src="<?php if(!empty($output['store_info']['store_stamp'])){echo UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$output['store_info']['store_stamp'];}?>" nc_type="store_stamp" /></p> </div>
          <p>
            <input name="store_stamp" type="file"  hidefocus="true" nc_type="change_store_stamp"/>
          </p>
          <p class="hint">워터마크 이미지는 주문서의 우측 하단에 출력되어 나타납니다. <br>사이즈 120x120, GIF/PNG 형식의 이미지를 선택하여 주시고, 이 이미지는 해당 미니샵의 전자 인장으로 사용되게 됩니다.
          </p>
        </dd>
      </dl>
      <div class="bottom">
          <label class="submit-border"><input type="submit" class="submit" value="저장" /></label>
       </div>
    </form>
  </div>

<script type="text/javascript">
$(function(){
	$('input[nc_type="change_store_stamp"]').change(function(){
		var src = getFullPath($(this)[0]);
		$('img[nc_type="store_stamp"]').attr('src', src);
	});
	$('#my_store_form').validate({
    	submitHandler:function(form){
    		ajaxpost('my_store_form', '', '', 'onerror')
    	},
		rules : {
    		store_printdesc: {
    			required: true,
    			rangelength:[0,100]
    	    },
        },
        messages : {
        	store_printdesc: {
        		required: '<i class="icon-exclamation-sign"></i><?php echo $lang['store_printsetup_desc_error'];?>',
		        rangelength:'<?php echo $lang['store_printsetup_desc_error'];?>'
		    }
        }
    });
});
</script>