<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/template.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(document).ready(function(){

        // 当前编辑对象，默认为空
        $edit_item = {};

        //现实商品搜索
        $('#btn_show_goods_select').on('click', function() {
            $('#div_goods_select').show();
        });

        //隐藏商品搜索
        $('#btn_hide_goods_select').on('click', function() {
            $('#div_goods_select').hide();
        });

        //搜索商品
        $('#btn_search_goods').on('click', function() {
            var url = "<?php echo urlShop('store_promotion_xianshi', 'goods_select');?>";
            url += '&' + $.param({goods_name: $('#search_goods_name').val()});
            $('#div_goods_search_result').load(url);
        });
        $('#div_goods_search_result').on('click', 'a.demo', function() {
            $('#div_goods_search_result').load($(this).attr('href'));
            return false;
        });

        //添加限时折扣商品弹出窗口 
        $('#div_goods_search_result').on('click', '[cnbiztype="btn_add_xianshi_goods"]', function() {
            $('#dialog_goods_id').val($(this).attr('data-goods-id'));
            $('#dialog_goods_name').text($(this).attr('data-goods-name'));
            $('#dialog_goods_price').text($(this).attr('data-goods-price'));
            $('#dialog_input_goods_price').val($(this).attr('data-goods-price'));
            $('#dialog_goods_img').attr('src', $(this).attr('data-goods-img'));
            $('#dialog_add_xianshi_goods').nc_show_dialog({width: 450, title: '상품추가'});
            $('#dialog_xianshi_price').val('');
            $('#dialog_add_xianshi_goods_error').hide();
        });

        //添加限时折扣商品
        $('#div_goods_search_result').on('click', '#btn_submit', function() {
            var goods_id = $('#dialog_goods_id').val();
            var xianshi_id = <?php echo $_GET['xianshi_id'];?>;
            var goods_price = getNum($('#dialog_input_goods_price').val());
            var xianshi_price = Number($('#dialog_xianshi_price').val());
            if(!isNaN(xianshi_price) && xianshi_price > 0) {    // && xianshi_price < goods_price
                $.post('<?php echo urlShop('store_promotion_xianshi', 'xianshi_goods_add');?>', 
                    {goods_id: goods_id, xianshi_id: xianshi_id, xianshi_price: xianshi_price},
                    function(data) {
                        if(data.result) {
                            $('#dialog_add_xianshi_goods').hide();
                            $('#xianshi_goods_list').prepend(template.render('xianshi_goods_list_template', data.xianshi_goods)).hide().fadeIn('slow');
                            $('#xianshi_goods_list_norecord').hide();
                            showSucc(data.message);
                        } else {
                            showError(data.message);
                        }
                    }, 
                'json');
            } else {
                $('#dialog_add_xianshi_goods_error').show();
            }
        });

        //编辑限时活动商品
        $('#xianshi_goods_list').on('click', '[cnbiztype="btn_edit_xianshi_goods"]', function() {
            $edit_item = $(this).parents('tr.bd-line');
            var xianshi_goods_id = $(this).attr('data-xianshi-goods-id');
            var xianshi_price = $edit_item.find('[cnbiztype="xianshi_price"]').text();
            var goods_price = $(this).attr('data-goods-price');
            $('#dialog_xianshi_goods_id').val(xianshi_goods_id);
            $('#dialog_edit_goods_price').text(goods_price);
            $('#dialog_edit_xianshi_price').val(xianshi_price);
            $('#dialog_edit_xianshi_goods').nc_show_dialog({width: 450, title: '가격변경'});
        });

        $('#btn_edit_xianshi_goods_submit').on('click', function() {
            var xianshi_goods_id = $('#dialog_xianshi_goods_id').val();
            var xianshi_price = getNum($('#dialog_edit_xianshi_price').val());
            var goods_price = getNum($('#dialog_edit_goods_price').text());
            if(!isNaN(xianshi_price) && xianshi_price > 0 && xianshi_price < goods_price) {
                $.post('<?php echo urlShop('store_promotion_xianshi', 'xianshi_goods_price_edit');?>',
                    {xianshi_goods_id: xianshi_goods_id, xianshi_price: xianshi_price},
                    function(data) {
                        if(data.result) {
                            $edit_item.find('[cnbiztype="xianshi_price"]').text(data.xianshi_price_ko);
                            $edit_item.find('[cnbiztype="xianshi_discount"]').text(data.xianshi_discount);
                            $('#dialog_edit_xianshi_goods').hide();
                        } else {
                            showError(data.message);
                        }
                    }, 'json'
                ); 
            } else {
                $('#dialog_edit_xianshi_goods_error').show();
            }
        });

        //삭제限时活动商品
        $('#xianshi_goods_list').on('click', '[cnbiztype="btn_del_xianshi_goods"]', function() {
            var $this = $(this);
            if(confirm('삭제하시겠습니까？')) {
                var xianshi_goods_id = $(this).attr('data-xianshi-goods-id');
                $.post('<?php echo urlShop('store_promotion_xianshi', 'xianshi_goods_delete');?>',
                    {xianshi_goods_id: xianshi_goods_id},
                    function(data) {
                        if(data.result) {
                            $this.parents('tr').hide('slow', function() {
                                var xianshi_goods_count = $('#xianshi_goods_list').find('.bd-line:visible').length;
                                if(xianshi_goods_count <= 0) {
                                    $('#xianshi_goods_list_norecord').show();
                                }
                            });
                        } else {
                            showError(data.message);
                        }
                    }, 'json'
                );
            }
        });

        //渲染限时折扣商品列表
        xianshi_goods_array = $.parseJSON('<?php echo json_encode($output['xianshi_goods_list']);?>');
        if(xianshi_goods_array.length > 0) {
            var xianshi_goods_list = '';
            $.each(xianshi_goods_array, function(index, xianshi_goods) {
                xianshi_goods_list += template.render('xianshi_goods_list_template', xianshi_goods);
            });
            $('#xianshi_goods_list').prepend(xianshi_goods_list);
        } else {
            $('#xianshi_goods_list_norecord').show();
        }
    });
</script>
<div class="tabmenu">
    <?php include template('layout/submenu');?>
    <?php if($output['xianshi_info']['editable']) { ?>
    <a id="btn_show_goods_select" class="ncsc-btn ncsc-btn-green" href="javascript:;"><i></i>상품추가</a> </div>
    <?php } ?>
<table class="ncsc-default-table">
  <tbody>
    <tr>
      <td class="w90 tr"><strong><?php echo '이벤트명'.$lang['nc_colon'];?></strong></td>
      <td class="w120 tl"><?php echo $output['xianshi_info']['xianshi_name_ko'];?></td>
      <td class="w90 tr"><strong><?php echo '시작시간'.$lang['nc_colon'];?></strong></td>
      <td class="w120 tl"><?php echo date('Y-m-d H:i',$output['xianshi_info']['start_time']);?></td>
      <td class="w90 tr"><strong><?php echo '종료시간'.$lang['nc_colon'];?></strong></td>
      <td class="w120 tl"><?php echo date('Y-m-d H:i',$output['xianshi_info']['end_time']);?></td>
      <td class="w90 tr"><strong><?php echo '구매한정'.$lang['nc_colon'];?></strong></td>
      <td class="w120 tl"><?php echo $output['xianshi_info']['lower_limit'];?></td>
      <td class="w90 tr"><strong><?php echo '상태'.$lang['nc_colon'];?></strong></td>
      <td class="w120 tl"><?php echo $output['xianshi_info']['xianshi_state_text'];?></td>
    </tr>
</table>
<div class="alert">
  <strong>TIP<?php echo $lang['nc_colon'];?></strong>
  <ul>
    <li>1. 기간한정세일 상품의 시간은 중복될 수 없습니다.</li>
    <li>2. 상품 추가/삭제를 통하여 편집하실 수 있습니다.</li>
  </ul>
</div>
<!-- 商品搜索 -->
<div id="div_goods_select" class="div-goods-select" style="display: none;">
    <table class="search-form">
      <tr><th class="w150"><strong>1단계 : 미니샵내상품검색</strong></th><td class="w160"><input id="search_goods_name" type="text w150" class="text" name="goods_name" value=""/></td>
        <td class="w70 tc"><a href="javascript:void(0);" id="btn_search_goods" class="ncsc-btn"/><i class="icon-search"></i>검색</a></td><td class="w10"></td><td><p class="hint">바로 검색을 클릭하시면 미니샵 내 모든 상품이 나타납니다. (특수상품은 추가하실 수 없습니다.)</p></td>
      </tr>
    </table>
  <div id="div_goods_search_result" class="search-result"></div>
  <a id="btn_hide_goods_select" class="close" href="javascript:void(0);">X</a> </div>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w10"></th>
      <th class="w50"></th>
      <th class="tl">상품명</th>
      <th class="w90">상품가</th>
      <th class="w120">할인가</th>
      <th class="w120">할인율</th>
      <th class="w120">편집</th>
    </tr>
  </thead>
  <tbody id="xianshi_goods_list">
    <tr id="xianshi_goods_list_norecord" style="display:none">
      <td class="norecord" colspan="20"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
  </tbody>
</table>
<div class="bottom">
  <label class="submit-border"><input type="submit" class="submit" id="submit_back" value="리스트로 돌아가기" onclick="window.location='index.php?act=store_promotion_xianshi&op=xianshi_list'"></label>
</div>
<div id="dialog_edit_xianshi_goods" class="eject_con" style="display:none;">
    <input id="dialog_xianshi_goods_id" type="hidden">
    <dl><dt>상품가：</dt><dd><span id="dialog_edit_goods_price"></dd>
    </dl>
    <dl><dt>할인가：</dt><dd><input id="dialog_edit_xianshi_price" type="text" class="text w70"><em class="add-on">원</em>
    <p id="dialog_edit_xianshi_goods_error" style="display:none;"><label for="dialog_edit_xianshi_goods_error" class="error"><i class='icon-exclamation-sign'></i>할인율을 입력해 주세요. 상품가격보다는 낮은 가격이어야 합니다.</label></p>
    </dl>    
    <div class="eject_con">
        <div class="bottom pt10 pb10"><a id="btn_edit_xianshi_goods_submit" class="submit" href="javascript:void(0);">완료</a></div>
    </div>
</div>
<script id="xianshi_goods_list_template" type="text/html">
<tr class="bd-line">
    <td></td>
    <td><div class="pic-thumb"><a href="<%=goods_url%>" target="_blank"><img src="<%=image_url%>" alt=""></a></div></td>
    <td class="tl"><dl class="goods-name"><dt><a href="<%=goods_url%>" target="_blank"><%=goods_name_ko%></a></dt></dl></td>
    <td><%=goods_price_ko%>원</td>
    <td><span cnbiztype="xianshi_price"><%=xianshi_price_ko%></span>원</td>
    <td><span cnbiztype="xianshi_discount"><%=xianshi_discount%></span></td>
    <td class="nscs-table-handle">
    <?php if($output['xianshi_info']['editable']) { ?>
    <span><a cnbiztype="btn_edit_xianshi_goods" class="btn-blue" data-xianshi-goods-id="<%=xianshi_goods_id%>" data-goods-price="<%=goods_price_ko%>" href="javascript:void(0);"><i class="icon-edit"></i><p>편집</p></a></span>
        <span><a cnbiztype="btn_del_xianshi_goods" class="btn-red" data-xianshi-goods-id="<%=xianshi_goods_id%>" href="javascript:void(0);"><i class="icon-trash"></i><p>삭제</p></a></span>
    <?php } ?>
    </td>
</tr>
</script> 
