<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['document_index_document']		= '회원약관';
$lang['document_index_title']			= '제목';
$lang['document_index_content']			= '문장내용';
$lang['document_index_pic_upload']		= '이미지 업로드';
$lang['document_index_batch_upload']	= '일괄업로드';
$lang['document_index_normal_upload']	= '일반업로드';
$lang['document_index_uploaded_pic']	= '보유이미지';
$lang['document_index_insert']			= '삽입수정';
$lang['document_index_title_null']		= '문장제목을 입력하세요';
$lang['document_index_content_null']	= '문장내용을 입력하세요';
$lang['document_index_del_fail']		= '삭제실패';
$lang['document_index_help1']			= '해당 조작 부분에서 상세 내용을 확인할 수 있습니다, 예: 회원가입시 본 약관을 확인할 수 있습니다.';
/**
 * 수정시스텀문장
 */
$lang['document_edit_back_to_list']		= '돌아가기';
$lang['document_edit_again']			= '본 문장 재수정';
$lang['document_edit_time']				= '시간';
$lang['article_add_img_wrong']      = '이미지 허용 확장명:png,gif,jpeg,jpg';
/**
 * iframe上传
 */
$lang['document_iframe_upload_fail']	= '업로드 실패';
$lang['document_iframe_upload']         = '업로드';