<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php if(!empty($output['list']) && is_array($output['list'])){?>
<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<style type="text/css">
.mttable{
border:1px solid #cad9ea;
color:#666;
}
.mttable th {
background-repeat:repeat-x;
height:30px;
font-weight:bold;
}
.mttable td,.mttable th{
border:1px solid #cad9ea;
padding:0 1em 0;
}
.mttable tr.alter{
background-color:#f5fafe;
} 
</style>

  <table class="mttable" width="100%">
  <col width="25%" />
  <col />
  <col width="35%" />
  <col width="15%" />
	  	<tr>
	  		<th>HS코드</th>
	  		<th>품명</th>
	  		<th>Description</th>
	  		<th>설정</th>
		</tr>
  <?php foreach($output['list'] as $key=>$val){?>
		<tr>
			<td><?php echo $val['article_title'];?></td>
			<td><?php echo $val['article_abstract'];?></td>
			<td><?php echo $val['article_content'];?></td>
			<td><a cnbiztype="btn_add_hscode_article" data-hscode-id="<?php echo $val['article_id'];?>" href="javascript:void(0);" class="ncsc-btn-mini ncsc-btn-green"><i class="icon-ok-circle "></i>선택</a> </td>
		</tr>
  <?php } ?>
</table>
<div class="pagination"><?php echo $output['show_page']; ?></div>
<?php } else { ?>
<div>내용이 없습니다.</div>
<?php } ?>
