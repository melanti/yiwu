<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>벼룩카테고리</h3>
      <ul class="tab-base">
        <li><a href="index.php?act=flea_class&op=goods_class"><span>관리</span></a></li>
        <li><a href="index.php?act=flea_class&op=goods_class_add" ><span>추가</span></a></li>
        <li><a href="index.php?act=flea_class&op=goods_class_export" ><span>불러내기</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>불러오기</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="post" enctype="multipart/form-data" name="form1">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="charset" value="gbk" />
    <table class="table tb-type2">
      <tbody>
        <tr class="noborder">
          <td colspan="2" class="required"><label>파일을 선택하세요:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><span class="type-file-box">
            <input type="file" name="csv" id="csv" class="type-file-file"  size="30"  />
            </span></td>
          <td class="vatop tips">물러올때 속도가 드리면 파일을 여러개로 분리해서 올려주세요.</td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label>파일양식:</label>
            <a href="../resource/examples/flea_class.csv" class="btns"><span>불러오기 샘플파일 다운로드</span></a></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><table border="1" cellpadding="3" cellspacing="3" bordercolor="#CCC">
              <tbody>
                <tr>
                  <td bgcolor="#EFF8F8">정렬</td>
                  <td bgcolor="#FFFFEC">1급카테고리</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                </tr>
                <tr>
                  <td bgcolor="#EFF8F8">정렬</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                  <td bgcolor="#FFFFEC">2급카테고리</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                </tr>
                <tr>
                  <td bgcolor="#EFF8F8">정렬</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                  <td bgcolor="#FFFFEC">3급카테고리</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                </tr>
                <tr>
                  <td bgcolor="#EFF8F8">정렬</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                  <td bgcolor="#FFFFEC">&nbsp;</td>
                  <td bgcolor="#FFFFEC">4급카테고리</td>
                </tr>
                <tr>
                  <td bgcolor="#EFF8F8">정렬</td>
                  <td bgcolor="#FFFFEC">5급카테고리</td>
                  <td bgcolor="#FFFFEC"></td>
                  <td bgcolor="#FFFFEC"></td>
                </tr>
              </tbody>
            </table></td>
          <td class="vatop tips"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="2"><a href="JavaScript:document.form1.submit();" class="btn"><span>불러오기</span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>

<script type="text/javascript">
	$(function(){
    var textButton="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='' class='type-file-button' />"
	$(textButton).insertBefore("#csv");
	$("#csv").change(function(){
	$("#textfield1").val($("#csv").val());
	});
});
</script> 
