<?php
/**
 * 我的代金券
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2014 Cb Inc. (http://www.cnbiz.co.kr)
 * @license    http://www.cnbiz.co.kr
 * @link       http://www.cnbiz.co.kr
 * @since      File available since Release v1.1
 */

use Cb\Tpl;

defined('InCNBIZ') or exit('Access Invalid!');

class member_redpacketControl extends mobileMemberControl {

	public function __construct() {
		parent::__construct();
	}

    /**
     * 地址列表
     */
    public function redpacket_listOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], $_POST['voucher_state'], $this->page);
        $page_count = $model_voucher->gettotalpage();

        output_data(array('redpacket_list' => $voucher_list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function pdrechargelistOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], $_POST['voucher_state'], $this->page);
        $page_count = $model_voucher->gettotalpage();

        output_data(array('list' => $voucher_list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function pdcashlistOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], $_POST['voucher_state'], $this->page);
        $page_count = $model_voucher->gettotalpage();

        output_data(array('list' => $voucher_list), mobile_page($page_count));
    }

    /**
     * 地址列表
     */
    public function rcblogOp() {
        $model_voucher = Model('voucher');
        $voucher_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], $_POST['voucher_state'], $this->page);
        $page_count = $model_voucher->gettotalpage();

        output_data(array('list' => $voucher_list), mobile_page($page_count));
    }


}
