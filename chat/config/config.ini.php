<?php
defined('InCNBIZ') or exit('Access Invalid!');
define('NODE_SITE_URL','http://b2b2c.cnbiz.co.kr:8090');
define('CHAT_SITE_URL','http://b2b2c.cnbiz.co.kr/chat');

define('CHAT_TEMPLATES_URL',CHAT_SITE_URL.'/templates/default');
define('CHAT_RESOURCE_URL',CHAT_SITE_URL.'/resource');
