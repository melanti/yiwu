
(function($, angular, _, APP){
  'use strict';

  APP.modules.ctrl.tdPrevModalSmallCtrl = tdPrevModalSmallCtrl;

  //tdPrevModalCtrl
  function tdPrevModalSmallCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO){
    var vm = this;

    vm.imgArr=[];
    vm.imgArrLen=0;
    vm.curIdx=0;
    vm.isShow = false;

    vm.showNext = _showNext;
    vm.showPrev = _showPrev;

    _init();

    function _init(){
      // console.log('tdPrevModalCtrl _init');

      $scope.$on('$destroy', function () {
      });

      PubSub.subscribe('tdPrev:show', _onShowTdPrev);
      PubSub.subscribe('tdPrev:hide', _onHideTdPrev);

    }

    function _onShowTdPrev(topic, data){
      vm.isShow = true;
      vm.curIdx=0;
      if(data && data.imgArr){
        vm.imgArr=data.imgArr.concat();
        vm.imgArrLen=vm.imgArr.length;
      }else{
      }

      // $('#tdPreviewModal').modal("show");
    }

    function _onHideTdPrev(topic, data){
      vm.isShow = true;
      // $('#tdPreviewModal').modal("hide");
    }

    function _showNext(){
      ++vm.curIdx;
      if(vm.curIdx>=vm.imgArrLen){
        vm.curIdx = 0;
      }
    }

    function _showPrev(){
      --vm.curIdx;
      if(vm.curIdx<0){
        if(vm.imgArrLen<=0) {
          vm.curIdx = 0;
        }else{
          vm.curIdx = vm.imgArrLen-1;
        }
      }
    }


  }

  tdPrevModalSmallCtrl.$inject = ['$scope', '$q', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO'];
  tdPrevModalSmallCtrl.bindToController = {
    getTotal:'&',
    getStart:'&',
    getCount:'&',
    getReqModel:'&',
    setReqModel:'&',
    getFilter:'&',
    setFilter:'&',
    updatePage:'&'
  };
  tdPrevModalSmallCtrl.link = function () {
    return function (scope, elem, attrs, ctrl, transclude) {
      $('#tdPreviewModal').on('show show.bs.modal', function() {
        //console.log('show.bs.modal');
        //scope.$apply();
      });

      $('#tdPreviewModal').on('shown shown.bs.modal', function() {
        // console.log('shown.bs.modal');
        scope.$apply();
      });

      $('#tdPreviewModal').on('hide hide.bs.modal', function() {
        //console.log('hide.bs.modal');
        //scope.$apply();
      });

      $('#tdPreviewModal').on('hidden hidden.bs.modal', function() {
        // console.log('hidden.bs.modal');
        scope.$apply();
      });
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
