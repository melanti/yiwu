<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['store'];?></h3>
      <ul class="tab-base">
        <li><a href="index.php?act=store&op=store" ><span><?php echo $lang['manage'];?></span></a></li>
        <li><a href="index.php?act=store&op=store_joinin" ><span><?php echo $lang['pending'];?></span></a></li>
        <li><a href="index.php?act=store&op=reopen_list" ><span>연장계약</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>경영항목신청</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="get" name="formSearch">
    <input type="hidden" value="store" name="act">
    <input type="hidden" value="store_bind_class_applay_list" name="op">
    <table class="tb-type1 noborder search">
      <tbody>
        <tr>
          <th><label for="store_name"><?php echo $lang['store_name'];?>ID</label></th>
          <td><input type="text" value="" name="store_id" id="store_id" class="txt"></td>
          <th>심사상태</th>
          <td>
          <select name="state">
          <option value=""><?php echo $lang['nc_please_choose'];?>...</option>
          <option <?php if ($_GET['state'] == '0') echo 'selected';?> value="0">심사대기</option>
          <option <?php if ($_GET['state'] == '1') echo 'selected';?> value="1">심사완료</option>
          </select>
          </td>
          <td><a href="javascript:document.formSearch.submit();" class="btn-search " title="<?php echo $lang['nc_query'];?>">&nbsp;</a>
        </tr>
        </tbody>
    </table>
</form>
<table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li>새로 신청한 업체 경영항목에 심사/삭제를 할 수 있습니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <form method="post" id="store_form" name="store_form">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <thead>
        <tr class="thead">
          <th class="align-center" colspan="3">경영항목</th>
          <th><?php echo $lang['store_name'];?></th>
          <th><?php echo $lang['store_user_name'];?></th>
          <th>수수료율</th>
          <th class="align-center"><?php echo $lang['operation'];?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!empty($output['bind_list']) && is_array($output['bind_list'])){ ?>
        <?php foreach($output['bind_list'] as $k => $v){ ?>
        <tr class="hover edit">
          <td><?php echo $v['class_1_name'];?></td>
          <td><?php echo $v['class_2_name'] ? '>' : null;?> <?php echo $v['class_2_name'];?></td>
          <td><?php echo $v['class_3_name'] ? '>' : null;?> <?php echo $v['class_3_name'];?></td>
          <td><?php echo $output['bind_store_list'][$v['store_id']]['store_name'];?>[ID:<?php echo $v['store_id'];?>]</td>
          <td><?php echo $output['bind_store_list'][$v['store_id']]['seller_name'];?></td>
          <td class="w150"><?php echo $v['commis_rate'];?> %</td>
          <td class="w72 align-center">
          <?php if ($v['state'] == '0') {?>
          <a href="javascript:if(confirm('정말 심사하시겠습니까?'))window.location = 'index.php?act=store&op=store_bind_class_applay_check&bid=<?php echo $v['bid'];?>&store_id=<?php echo $v['store_id'];?>';">심사</a> |
          <?php } ?>
          <a href="javascript:if(confirm('<?php echo $v['state'] == '1' ? '심사 통과된 항목을 삭제하시면 업체에 영향을 끼칠수 있습니다,' : null;?>정말 삭제하시겠습니까?'))window.location = 'index.php?act=store&op=store_bind_class_applay_del&bid=<?php echo $v['bid'];?>&store_id=<?php echo $v['store_id'];?>';"><?php echo $lang['nc_del'];?></a>
          </td>
        </tr>
        <?php } ?>
        <?php }else { ?>
        <tr class="no_data">
          <td colspan="10"><?php echo $lang['nc_no_record'];?></td>
        </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td></td>
          <td colspan="15">
              <?php if(!empty($output['bind_list']) && is_array($output['bind_list'])){ ?>
              <div class="pagination"><?php echo $output['page'];?></div>
              <?php } ?>
          </td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
