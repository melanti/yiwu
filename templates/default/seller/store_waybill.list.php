<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
  <a title="템플릿만들기" class="ncsc-btn ncsc-btn-green" href="<?php echo urlShop('store_waybill', 'waybill_add');?>">템플릿추가</a> </div>
<div class="alert alert-block mt10">
  <ul class="mt5">
    <li>1. 판매자는 이미 프린트 템플릿을 등록하셨습니다.</li>
    <li>2. 우측 상단에 있는 "템플릿추가"를 클릭하시고 판매자의 템플릿을 만들어 주십시오.</li>
    <li>3. "설계"를 클릭하시면 송장 템플릿구도가 설계되며, "미리보기"를 클릭하시면 미리보기 및 프린트가 가능합니다. 내용 편집을 원하실 경우 "편집" 을 눌러서 진행해 주세요.</li>
    <li>4.  설계를 완료한 후, 편집 중의 템플릿 상태를 수정하신 후, 판매자는 해당템플릿과 연동하시면 송장 프린트를 진행하실 수 있습니다.</li>
    <li>5. "삭제" 버튼으로 현재 템플릿을 삭제하실 수 있으며, 삭제 후에는 해당 템플릿이 자동적으로 연동 해제됩니다. 신중히 처리하여 주십시오.</li>
  </ul>
</div>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w30"></th>
      <th class="w120 tl">템플릿명</th>
      <th class="w120 tl">물류회사</th>
      <th class="tl">송장이미지</th>
      <th class="w80">상단여백</th>
      <th class="w100">좌측여백</th>
      <th class="w80">사용</th>
      <th class="w200">상태</th>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($output['waybill_list']) && is_array($output['waybill_list'])){?>
    <?php foreach($output['waybill_list'] as $key => $value){?>
    <tr class="bd-line">
      <td></td>
      <td class="tl"><?php echo $value['waybill_name'];?></td>
      <td class="tl"><?php echo $value['express_name'];?></td>
      <td class="tl"><div class="waybill-img-thumb"><a class="nyroModal" rel="gal" href="<?php echo $value['waybill_image_url'];?>"><img src="<?php echo $value['waybill_image_url'];?>"></a></div>
        <div class="waybill-img-size">
          <p>너비：<?php echo $value['waybill_width'];?>(mm)</p>
          <p>높이：<?php echo $value['waybill_height'];?>(mm)</p>
        </div></td>
        </td>
      <td><?php echo $value['waybill_top'];?></td>
      <td><?php echo $value['waybill_left'];?></td>
      <td><?php echo $value['waybill_usable_text'];?></td>
      <td class="nscs-table-handle"><span><a href="<?php echo urlShop('store_waybill', 'waybill_design', array('waybill_id' => $value['waybill_id']));?>" class="btn-orange"><i class="icon-edit"></i>
        <p>설정</p>
        </a></span><span><a href="<?php echo urlShop('store_waybill', 'waybill_test', array('waybill_id' => $value['waybill_id']));?>" class="btn-acidblue" target="_blank"><i class="icon-print"></i>
        <p>미리보기</p>
        </a></span><span><a href="<?php echo urlShop('store_waybill', 'waybill_edit', array('waybill_id' => $value['waybill_id']));?>" class="btn-blue"><i class="icon-edit"></i>
        <p>편집</p>
        </a></span><span><a href="javascript:;" nctype="btn_del" data-waybill-id="<?php echo $value['waybill_id'];?>" class="btn-red"><i class="icon-trash"></i>
        <p>삭제</p>
        </a></span></td>
    </tr>
    <?php }?>
    <?php }else{?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php }?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
  </tfoot>
</table>
<form id="del_form" action="<?php echo urlShop('store_waybill', 'waybill_del');?>" method="post">
  <input type="hidden" id="del_waybill_id" name="waybill_id">
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/custom.min.js" charset="utf-8"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/styles/nyroModal.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">
    $(document).ready(function() {
        $('[nctype="btn_del"]').on('click', function() {
            if(confirm('정말 삭제 하시겠습니까?')) {
                $('#del_waybill_id').val($(this).attr('data-waybill-id'));
                $('#del_form').submit();
            }
        });

        //点击查看大图	
    	$('.nyroModal').nyroModal();
    });
</script> 
