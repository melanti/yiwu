var Q = require('q');
var http = require('http');
var request = require('request');
var chkData = require("./libs/chkData.js");
var debug = require("./libs/debug.js");
var is = require("is_js");

var SQLService = require('../services/SQLService');
var ErrorHandler = require('../services/ErrorHandler');
var ValidatorService = require('../services/ValidatorService');
var QueryService = require('../services/QueryService');
var CsvConvertService = require('../services/CsvConvertService');

module.exports = {
    getMakeCateUrl: _getMakeCateUrl,
    list: _list,
    getCateCSV:_getCateCSV
};

function _getMakeCateUrl(req, res) {
    var MAKE_CATE_MODEL = {
        //Dependency Object
        MAKE_CATE_QRY: QueryService.MAKE_CATE_QRY,
        req:req,
        res:res,
        params:req.allParams(),

        account_uuid:req.param("account_uuid") || "",
        ver:req.param("ver"),
        pkg_name:req.param("pkg_name") || "net.fxgear.fitnshop",
        show:req.param("show") || "",
        down_path:sails.config.ftp.domain,
        down_file:"MakeCategory_v.{ver}_FX0.csv"
    };

    //promise tasks
    Q.fcall(_taskValidateParams)                    // 1. parameter 검증
        .spread(_taskCheckSwVersion)                // 2. software version 체킹*/
        .spread(_taskAllSettled)                    // 3. 완료
        .catch(_taskDefaultError);                  // ** default error excute


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //task method START
    function _taskValidateParams(){
        return Q.all([
            ValidatorService.validatePromise(
            {
                data:MAKE_CATE_MODEL,
                passingData:MAKE_CATE_MODEL,
                validator:[
                    {key:'account_uuid' , validation:{ list:[{fnc:is.not.undefined, code:'ER_UNDEFINED_PARAM_ERROR'},
                                                             {fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                    /*{key:'ver' , validation:{ list:[{fnc:is.not.undefined, code:'ER_UNDEFINED_PARAM_ERROR'},
                                                    {fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }}*/
                ]
            }
            )
        ]);
    }

    function _taskCheckSwVersion() {
        var deferred = Q.defer();
        if(MAKE_CATE_MODEL.ver) {
            deferred.resolve(MAKE_CATE_MODEL);
        } else {
            SQLService
                .queryOnPromise(
                    QueryService.MAKE_CATE_QRY.SELECT_SW_VERSION
                ).then(function (results) {
                    if(results.length === 0) {
                        deferred.reject(ErrorHandler.NOT_FOUND_DATA.code);
                    }else{
                        MAKE_CATE_MODEL.ver=results[0].ver;
                        deferred.resolve(MAKE_CATE_MODEL);
                    }
                }).catch(function (e) {
                    sails.log.debug(e);
                    deferred.reject('ER_NETWORK_CHECK_MEMBER');
                });
        }

        return deferred.promise;
    }

    function _taskAllSettled() {
        var download_url = MAKE_CATE_MODEL.down_path + "/default/cat/category/" + MAKE_CATE_MODEL.down_file.replace("{ver}", MAKE_CATE_MODEL.ver);
        return res.send(
            200,
            debug.wrap(
                MAKE_CATE_MODEL.req,
                MAKE_CATE_MODEL.res,
                200,
                {
                    download_url:download_url
                }
            )
        );
    }

    function _taskDefaultError(__err){
        sails.log.debug('getMakeCateUrl ERROR: ' , __err);

        var err_code = 'ERR_FAIL';
        var tmpStatus = 500;

        //  TODO : ERROR format 정규화 및 response 처리
        // ER_SECRET_ERROR , ER_VERIFY_ERROR , ER_BAD_BLANK_ERROR|ER_UNDEFINED_PARAM_ERROR ,
        // ER_NOT_MATCH : account_uuid , ER_NOT_MATCH : make_category_name , ER_UNDEFINED Error: package.bin is invalid format


        try{
            /*if( __err.stack.indexOf('ER_EMPTY_FILE')!==-1 ){
                tmpStatus = 400;
                err_code = 'ER_EMPTY_FILE';
            }else if(__err.stack.indexOf('ER_NOT_MATCH_MEMBER_ID_AND_ACCOUNT_ID')!==-1){
                tmpStatus = 400;
                err_code = 'ER_NOT_MATCH_MEMBER_ID_AND_ACCOUNT_ID';
            }else if( __err.stack.indexOf(ErrorHandler.NOT_FOUND_DATA.code + ": make_category_name ")!==-1 ){
                tmpStatus = 400;
                err_code = ErrorHandler.NOT_FOUND_DATA.code + ": make_category_name ";
            }else{*/
                tmpStatus = 400;
                err_code = __err;
            /*}*/
        }catch(e){
        }

        res.status(tmpStatus);
        return res.send(tmpStatus, debug.wrap(req, res, (__err.status || 500), {code:err_code} ));
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

function _list(req, res) {
    var debug = require("./libs/debug.js"),
        conv_data = require("./libs/convData.js"),
        account_uuid = req.param("account_uuid") || "",
        brand_id = req.param("brand_id") || "",
        /*cate_status = req.param("status") || "all",*/
        sex = req.param("sex") || "all",
        //start = req.param("start") || 0,
        //count = req.param("count") || 10,
        kids_yn = req.param("kids_yn") || 0,
        cate_status = req.param("status") || "all",
        show = req.param("show") || "",
        qry = "",
        rtn_val = {},
        arr_param = [],
        status = 501,
        error = {};

    qry += "select gc_id_1 ";
    qry += "     , (select gc_name ";
    qry += "          from sv_goods_class ";
    qry += "         where gc_id = gc_id_1) as gc_name_1 ";
    qry += "     , (select gc_sort ";
    qry += "          from sv_goods_class ";
    qry += "         where gc_id = gc_id_1) as gc_sort_1 ";
    qry += "     , gc_id_2 ";
    qry += "     , (select gc_name ";
    qry += "          from sv_goods_class ";
    qry += "         where gc_id = gc_id_2) as gc_name_2 ";
    qry += "     , (select gc_sort ";
    qry += "          from sv_goods_class ";
    qry += "         where gc_id = gc_id_2) as gc_sort_2 ";
    qry += "     , gc_id_3 ";
    qry += "     , (select gc_name ";
    qry += "          from sv_goods_class ";
    qry += "         where gc_id = gc_id_3) as gc_name_3 ";
    qry += "     , (select gc_sort ";
    qry += "          from sv_goods_class ";
    qry += "         where gc_id = gc_id_3) as gc_sort_3 ";
    //qry += "     , count(*) as cnt  ";
    qry += "  from sv_goods_common as a ";
    qry += " where goods_state = 1 ";
    qry += "   and goods_verify = 1 ";
    if(brand_id) {
        qry += " and brand_id = ? ";
        arr_param.push(brand_id);
    }

    if(sex != "all") {

        switch (sex) {
            case "man":
            case "woman":
                if(kids_yn === 1 || kids_yn === 0) {
                    qry += " and goods_sex in (?, 'unisex') ";
                    qry += " and goods_kids = ? ";
                    arr_param.push(sex);
                    arr_param.push(kids_yn);
                } else {
                    qry += " and goods_sex in (?, 'unisex') ";
                    arr_param.push(sex);
                }
                break;
            case "unisex":
                if(kids_yn === 1 || kids_yn === 0) {
                    qry += " and goods_sex in (?) ";
                    qry += " and goods_kids = ?";
                    arr_param.push(sex);
                    arr_param.push(kids_yn)
                } else {
                    qry += " and goods_sex in (?) ";
                    arr_param.push(sex);
                }
                break;
            default :
                qry += " and goods_sex in (?) ";
                arr_param.push(sex);
                break;
        }
    }

    if(kids_yn != "all") {
        qry += " and goods_kids = ? ";
        arr_param.push(kids_yn);
    }

    qry += " group by gc_id_1, gc_id_2, gc_id_3 ";
    qry += " order by gc_sort_1, gc_sort_2, gc_sort_3 ";

    if(cate_status !== "all" && cate_status !== "activated") {
        qry += " limit 0";
    }

    DB.query(qry, arr_param, function (err, results) {
        if (err) {
            status = 500;
            return res.send(status, debug.wrap(req, res, status, err));
        } else {
            status = 200;
            rtn_val.results = conv_data.listCate(results, show);
            return res.send(status, debug.wrap(req, res, status, rtn_val));
        }
    });

}

function _getCateCSV(req, res){

    console.log('_getCateCSV: ' , req.allParams());

    var MAKE_CATE_MODEL = {
        //Dependency Object
        MAKE_CATE_QRY: QueryService.MAKE_CATE_QRY,
        req:req,
        res:res,
        params:req.allParams(),

        account_uuid:req.param("account_uuid") || "",
        ver:req.param("ver") || "1.0.0",
        pkg_name:req.param("pkg_name") || "net.fxgear.fitnshop",
        show:req.param("show") || "",
        down_path:sails.config.ftp.domain,
        down_file:"MakeCategory_v.{ver}_FX0.csv"
    };

    //promise tasks
    Q.fcall(_taskValidateParams)                    // 1. parameter 검증
        .spread(_taskCheckSwVersion)                // 2. software version 체킹*/
        .spread(_taskAllSettled)                    // 3. 완료
        .catch(_taskDefaultError);                  // ** default error excute


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //task method START
    function _taskValidateParams(){
        return Q.all([
            ValidatorService.validatePromise(
                {
                    data:MAKE_CATE_MODEL,
                    passingData:MAKE_CATE_MODEL,
                    validator:[
                        {key:'account_uuid' , validation:{ list:[{fnc:is.not.empty, code:'ER_UNDEFINED_PARAM_ERROR'}] }},
                    ]
                }
            )
        ]);
    }

    function _taskCheckSwVersion() {
        var deferred = Q.defer();
        SQLService
            .queryOnPromise(
                QueryService.MAKE_CATE_QRY.SELECT_SW_VERSION
            ).then(function (results) {
            if(results.length === 0) {
                deferred.reject(ErrorHandler.NOT_FOUND_DATA.code);
            }else{
                MAKE_CATE_MODEL.ver=results[0].ver;
                deferred.resolve(MAKE_CATE_MODEL);
            }
        }).catch(function (e) {
            console.log(e);
            deferred.reject('ER_NETWORK_CHECK_MEMBER');
        });
        return deferred.promise;
    }

    function _taskAllSettled() {
        var download_url = MAKE_CATE_MODEL.down_path + "/default/cat/category/" + MAKE_CATE_MODEL.down_file.replace("{ver}", MAKE_CATE_MODEL.ver);

        try{
            request.get(download_url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    // Continue with your processing here.
                    var relJSON = CsvConvertService.CSV2JSON(body);
                    var convertedDataSet = CsvConvertService.convertDepthJSON(relJSON);

                    var depth01 = _.filter(relJSON.concat(), function(v,k,l){
                        return (v.CODE && v.CODE.length===3);
                    });

                    // CODE가 6글자일때(relJSON의 DEPTH_1),  DEPTH_1의 ko,en,zh-cn글자 앞에 DEPTH_0의 글자를 합침
                    convertedDataSet.data.forEach(function(val){
                        if( parseInt(val.DEPTH,10)===0 ){
                            var curDpt01 = _.filter(depth01, function(v){return v.CODE===val.DEPTH_0});

                            console.log('val: ' , val);
                            console.log('curDpt01[0]: ' , curDpt01);

                            if(val.ko!=='' && curDpt01.length>0){
                                val.ko =  curDpt01[0].ko +' '+val.ko;
                            }
                            if(val.en!=='' && curDpt01.length>0){
                                val.en =  curDpt01[0].en +' '+val.en;
                            }
                            if(val['zh-cn']!=='' && curDpt01.length>0){
                                val['zh-cn'] +=  curDpt01[0]['zh-cn'] +' '+val['zh-cn'];
                            }
                        }
                    });

                    // sails.log.debug('convertedDataSet: ' , convertedDataSet);

                    return res.json({debug:{status:200}, results:[convertedDataSet]});

                }else{
                    res.status(500);
                    return res.json({debug:{status:500}, code:'SERVER_CSV_ERR_GETMAKECATEURL' });
                }
            });
        }catch(e){
            res.status(500);
            return res.json({debug:{status:500}, code:'SERVER_CSV_ERR_GETMAKECATEURL' });
        }

    }

    function _taskDefaultError(__err){
        sails.log.debug('getMakeCateUrl ERROR: ' , __err);

        var err_code = 'ERR_FAIL';
        var tmpStatus = 500;
        return res.send(tmpStatus, debug.wrap(req, res, (__err.status || tmpStatus), {code:err_code} ));
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
    sails.log.debug('_getCateCSV: ',req.host, req.port);


    var options = {
            host: req.host,
            port: req.port,
            path: (('/category/getMakeCateUrl')+'?account_uuid='+(req.param('account_uuid'))+'&pkg_name=net.fxgear.fitnshop&show=debug'),
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Pragma': 'no-cache',
                'Cache-control': 'no-cache'
            }
        },
        json_output = "";

    if( options.path.indexOf('?')===-1 ){
        options.path += '?_='+new Date().getTime();
    }else{
        options.path += '&_='+new Date().getTime();
    }


    options.headers['service_type'] = 'admin';
    sails.log.debug('_getCateCSV: ',options.host, req.port);
    var post_req = http.request(options, function (post_res) {

        post_res.on('data', function (chunk) {
            json_output += chunk;
        });

        post_res.on('end', function () {
            try {
                sails.log.debug('json_output:  ', json_output);

                var rel = JSON.parse(json_output);
                if(rel.download_url){

                    sails.log.debug('rel.download_url: ' , rel.download_url);

                    request.get(rel.download_url, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            // Continue with your processing here.
                            var relJSON = CsvConvertService.CSV2JSON(body);
                            var convertedDataSet = CsvConvertService.convertDepthJSON(relJSON);



                            var depth01 = _.filter(relJSON.concat(), function(v,k,l){
                                return (v.CODE && v.CODE.length===3);
                            });

                            // CODE가 6글자일때(relJSON의 DEPTH_1),  DEPTH_1의 ko,en,zh-cn글자 앞에 DEPTH_0의 글자를 합침
                            convertedDataSet.data.forEach(function(val){
                                if( parseInt(val.DEPTH,10)===0 ){
                                    var curDpt01 = _.filter(depth01, function(v){return v.CODE===val.DEPTH_0});

                                    console.log('val: ' , val);
                                    console.log('curDpt01[0]: ' , curDpt01);

                                    if(val.ko!=='' && curDpt01.length>0){
                                        val.ko =  curDpt01[0].ko +' '+val.ko;
                                    }
                                    if(val.en!=='' && curDpt01.length>0){
                                        val.en =  curDpt01[0].en +' '+val.en;
                                    }
                                    if(val['zh-cn']!=='' && curDpt01.length>0){
                                        val['zh-cn'] +=  curDpt01[0]['zh-cn'] +' '+val['zh-cn'];
                                    }
                                }
                            });

                            // sails.log.debug('convertedDataSet: ' , convertedDataSet);

                            return res.json({debug:{status:200}, results:[convertedDataSet]});

                        }else{
                            res.status(500);
                            return res.json({debug:{status:500}, code:'SERVER_CSV_ERR_GETMAKECATEURL' });
                        }
                    });

                }else{
                    res.status(500);
                    return res.json({debug:{status:500}, code:'EMPTY_DOWNLOAD_URL_GETMAKECATEURL' });
                }

            } catch (e) {
                res.status(500);
                return res.json({debug:{status:500}, code:'SERVER_ERR_GETMAKECATEURL' });
            }
        });
    });

    post_req.end();*/


}

