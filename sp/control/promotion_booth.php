<?php
/**
 * 한시할인관리
 *
 *
 *
 **by J.K*/

defined('InCNBIZ') or exit('Access Invalid!');
class promotion_boothControl extends SystemControl{
    public function __construct(){
        parent::__construct();
        //检查심사功能예否시동
        if (intval($_GET['promotion_allow']) !== 1 && intval(C('promotion_allow')) !== 1){
            $url = array(
                array(
                    'url'=>'index.php?act=dashboard&op=welcome',
                    'msg'=>L('close'),
                ),
                array(
                    'url'=>'index.php?act=promotion_bundling&promotion_allow=1',
                    'msg'=>L('open'),
                ),
            );
            showMessage('상품 프로모션 기능이 미시동 상태입니다.', $url, 'html', 'succ', 1, 6000);
        }
    }

    /**
     * 默认Op
     */
    public function indexOp() {
        //自动시동할인셋트
        if (intval($_GET['promotion_allow']) === 1){
            $model_setting = Model('setting');
            $update_array = array();
            $update_array['promotion_allow'] = 1;
            $model_setting->updateSetting($update_array);
        }
        $this->goods_listOp();
    }

    public function goods_listOp() {
        /**
         * 处理상품 카테고리
         */
        $choose_gcid = ($t = intval($_REQUEST['choose_gcid']))>0?$t:0;
        $gccache_arr = Model('goods_class')->getGoodsclassCache($choose_gcid,3);
	    Tpl::output('gc_json',json_encode($gccache_arr['showclass']));
		Tpl::output('gc_choose_json',json_encode($gccache_arr['choose_gcid']));

        $model_booth = Model('p_booth');
        $where = array();
        if (intval($_GET['choose_gcid']) > 0) {
            $where['gc_id'] = intval($_GET['choose_gcid']);
        }
        $goods_list = $model_booth->getBoothGoodsList($where, 'goods_id', 10);
        if (!empty($goods_list)) {
            $goodsid_array = array();
            foreach ($goods_list as $val) {
                $goodsid_array[] = $val['goods_id'];
            }
            $goods_list = Model('goods')->getGoodsList(array('goods_id' => array('in', $goodsid_array)));
        }
        Tpl::output('gc_list', Model('goods_class')->getGoodsClassForCacheModel());
        Tpl::output('goods_list', $goods_list);
        Tpl::output('show_page', $model_booth->showpage(2));

        // 输出자영업 업체IDS
        Tpl::output('flippedOwnShopIds', array_flip(model('store')->getOwnShopIds()));
        Tpl::showpage('promotion_booth_goods.list');
    }

    /**
     * 套餐리스트
     */
    public function booth_quota_listOp() {
        $model_booth = Model('p_booth');
        $where = array();
        if ($_GET['store_name'] != '') {
            $where['store_name'] = array('like', '%'.trim($_GET['store_name']).'%');
        }
        $booth_list = $model_booth->getBoothQuotaList($where, '*', 10);

        // 상태数组
        $state_array = array(0=>L('close') , 1=>L('open'));
        Tpl::output('state_array', $state_array);

        Tpl::output('booth_list', $booth_list);
        Tpl::output('show_page', $model_booth->showpage(2));
        Tpl::showpage('promotion_booth_quota.list');
    }

    /**
     * 삭제추천상품
     */
    public function del_goodsOp() {
        $where = array();
        // 验证id예否正确
        if (is_array($_POST['goods_id'])) {
            foreach ($_POST['goods_id'] as $val) {
                if (!is_numeric($val)) {
                    showDialog(L('nc_common_del_fail'));
                }
            }
            $where['goods_id'] = array('in', $_POST['goods_id']);
        } elseif(intval($_GET['goods_id']) >= 0) {
            $where['goods_id'] = intval($_GET['goods_id']);
        } else {
            showDialog(L('nc_common_del_fail'));
        }

        $rs = Model('p_booth')->delBoothGoods($where);
        if ($rs) {
            showDialog(L('nc_common_del_succ'), 'reload', 'succ');
        } else {
            showDialog(L('nc_common_del_fail'));
        }
    }

    /**
     * 설정
     */
    public function booth_settingOp() {
        // 实例化模型
        $model_setting = Model('setting');

        if (chksubmit()){
            // 验证
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["promotion_booth_price"], "require"=>"true", 'validator'=>'Number', "message"=>'가격을 입력하세요'),
                array("input"=>$_POST["promotion_booth_goods_sum"], "require"=>"true", 'validator'=>'Number', "message"=>'1보다 큰 정수를 입력하세요'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }

            $data['promotion_booth_price'] = intval($_POST['promotion_booth_price']);
            $data['promotion_booth_goods_sum'] = intval($_POST['promotion_booth_goods_sum']);

            $return = $model_setting->updateSetting($data);
            if($return){
                $this->log(L('nc_set').' 추천위치');
                showMessage(L('nc_common_op_succ'));
            }else{
                showMessage(L('nc_common_op_fail'));
            }
        }

        // 검색setting리스트
        $setting = $model_setting->GetListSetting();
        Tpl::output('setting',$setting);

        Tpl::showpage('promotion_booth.setting');
    }
}
