<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form id="add_form" action="index.php?act=store_groupbuy&op=groupbuy_save&vr=1" method="post" enctype="multipart/form-data">
    <dl>
      <dt><i class="required">*</i>공동구매명(한국어)： </dt>
      <dd>
        <input class="w400 text" name="groupbuy_name_ko" type="text" id="groupbuy_name_ko" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt>공동구매명(중국어)： </dt>
      <dd>
        <input class="w400 text" name="groupbuy_name" type="text" id="groupbuy_name" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt>공동구매 부주제<?php echo $lang['nc_colon'];?>(한국어)</dt>
      <dd>
        <input class="w400 text" name="remark_ko" type="text" id="remark_ko" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 부주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt>공동구매 부주제<?php echo $lang['nc_colon'];?>(중국어)</dt>
      <dd>
        <input class="w400 text" name="remark" type="text" id="remark" value="" maxlength="30"  />
        <span></span>
        <p class="hint">공동구매 부주제명 최대 30자 이내</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>시작시간<?php echo $lang['nc_colon'];?>
      <dd>
          <input id="start_time" name="start_time" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em><span></span>
          <p class="hint"><?php echo '공동구매 시작시간 '.date('Y-m-d H:i', $output['groupbuy_start_time']);?> 이후</p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>종료시간<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <input id="end_time" name="end_time" type="text" class="text w130"/><em class="add-on"><i class="icon-calendar"></i></em><span></span>
          <p class="hint">
            공동구매 종료시간은 E-쿠폰 유효기간과 공동구매 세트 유효기간을 넘길 수 없습니다. 
            <span id="vr-expire-time"></span>
<?php if (!$output['isOwnShop']) { ?>
            
            <span>（<?php echo date('Y-m-d H:i', $output['current_groupbuy_quota']['end_time']); ?>）</span>
<?php } ?>
          </p>
      </dd>
    </dl>

    <dl>
      <dt><i class="required">*</i>공동구매상품： </dt>
      <dd>
      <div cnbiztype="groupbuy_goods_info" class="selected-group-goods " style="display:none;">
      <div class="goods-thumb"><img id="groupbuy_goods_image" src=""/></div>
          <div class="goods-name">
          <a cnbiztype="groupbuy_goods_href" id="groupbuy_goods_name" href="" target="_blank"></a>
          </div>
          <div class="goods-price">쇼핑몰가：<span cnbiztype="groupbuy_goods_price"></span>원</div>
      </div>
      <a href="javascript:void(0);" id="btn_show_search_goods" class="ncsc-btn ncsc-btn-acidblue">상품선택</a>
      <input id="groupbuy_goods_id" name="groupbuy_goods_id" type="hidden" value=""/>
      <span></span>
      <div id="div_search_goods" class="div-goods-select mt10" style="display: none;">
          <table class="search-form">
              <tr>
                  <th class="w150">
                      <strong>1단계 : 미니샵 내 상품 선택</strong>
                  </th>
                  <td class="w160">
                      <input id="search_goods_name" type="text w150" class="text" name="goods_name" value=""/>
                  </td>
                  <td class="w70 tc">
                      <a href="javascript:void(0);" id="btn_search_goods" class="ncsc-btn"/><i class="icon-search"></i>검색</a></td>
                    <td class="w10"></td>
                    <td>
                        <p class="hint">입력하지 않으시고 검색하시면 미니샵 내 모든 E-쿠폰 상품 상품이 보여지게 됩니다. </p>
                    </td>
                </tr>
            </table>
            <div id="div_goods_search_result" class="search-result" style="width:739px;"></div>
            <a id="btn_hide_search_goods" class="close" href="javascript:void(0);">X</a>
        </div>
        <p class="hint">원하시는 공동구매 상품을 선택해 주세요.</p>
        </dd>
    </dl>
    <dl cnbiztype="groupbuy_goods_info" style="display:none;">
      <dt><?php echo $lang['groupbuy_index_store_price'].$lang['nc_colon'];?></dt>
      <dd><span cnbiztype="groupbuy_goods_price"></span>원</dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>공동구매가：</dt>
      <dd>
        <input class="w70 text" id="groupbuy_price_ko" name="groupbuy_price_ko" type="text" value=""/><em class="add-on">원</em> <span></span>
        <p class="hint">공동구매가 진행될 때 해당 상품의 판매가격을 입력하여 주십시오. <br>0.01~1000000사이의 숫자(단위:원）<br>공동구매가는 배송료를 포함한 가격이며, 공동구매 상품은 시스템에서 기본배송료가 포함되지 않습니다.</p>
      </dd>
    </dl>
    <dl>
     <dt><i class="required">*</i>공동구매이미지<?php echo $lang['nc_colon'];?></dt>
      <dd>
      <div class="ncsc-upload-thumb groupbuy-pic">
          <p><i class="icon-picture"></i>
          <img cnbiztype="img_groupbuy_image" style="display:none;" src=""/></p>
      </div>
        <input cnbiztype="groupbuy_image" name="groupbuy_image" type="hidden" value="">
        <div class="ncsc-upload-btn">
            <a href="javascript:void(0);">
                <span>
                    <input type="file" hidefocus="true" size="1" class="input-file" name="groupbuy_image" cnbiztype="btn_upload_image"/>
                </span>
                <p><i class="icon-upload-alt"></i>업로드</p>
            </a>
        </div>
        <span></span>
         <p class="hint">공동구매 페이지의 이미지로 사용됩니다. <br>너비: 440px 높이:293px 크기 1M이내의 이미지<br>JPG,GIF,PNG 형식 파일 가능</p>
        </dd>
    </dl>
    <dl>
     <dt>공동구매추천이미지<?php echo $lang['nc_colon'];?></dt>
      <dd>
      <div class="ncsc-upload-thumb groupbuy-commend-pic">
          <p><i class="icon-picture"></i>
          <img cnbiztype="img_groupbuy_image" style="display:none;" src=""/></p>
      </div>
        <input cnbiztype="groupbuy_image" name="groupbuy_image1" type="hidden" value="">
        <span></span>
        <div class="ncsc-upload-btn">
            <a href="javascript:void(0);">
                <span>
                    <input type="file" hidefocus="true" size="1" class="input-file" name="groupbuy_image" cnbiztype="btn_upload_image"/>
                </span>
                <p><i class="icon-upload-alt"></i>업로드</p>
            </a>
        </div>
        <p class="hint">공동구매 페이지 측면 추천 위치에 사용되게 됩니다.<br>너비: 210px 높이: 180px 크기 1M이내의 이미지<br>JPG,GIF,PNG 형식 파일 가능</p>
        </dd>
    </dl>
    <dl>
      <dt>카테고리： </dt>
      <dd>
        <select id="class" name="class" class="w80">
          <option value="">선택</option>
          <?php if (!empty($output['classlist'])) { ?>
          <?php foreach ($output['classlist'] as $class) { ?>
          <option value="<?php echo $class['class_id']; ?>"><?php echo $class['class_name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
        <select id="s_class" name="s_class" class="w80">
          <option value="">선택</option>
        </select>
        <span></span>
        <p class="hint">해당 E-쿠폰 상품 공동구매 카테고리를 선택해 주세요.</p>
      </dd>
    </dl>
    <dl>
      <dt>공동구매지역<?php echo $lang['nc_colon'];?></dt>
      <dd>
        <select id="city" name="city" class="w80">
          <option value="">선택</option>
          <?php if (!empty($output['arealist'])) { ?>
          <?php foreach ($output['arealist'] as $area) { ?>
          <option value="<?php echo $area['area_id']; ?>"><?php echo $area['area_name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
        <select id="area" name="area" class="w80">
          <option value="">선택</option>
        </select>
        <select id="mall" name="mall" class="w80">
          <option value="">선택</option>
        </select>
        <span></span>
        <p class="hint">해당 E-쿠폰 상품 공동구매 지역 카테고리를 선택해 주세요.</p>
      </dd>
    </dl>
    <dl>
      <dt>E-쿠폰 수량 : </dt>
      <dd>
        <input class="w70 text" id="virtual_quantity" name="virtual_quantity" type="text" value="0"/>
        <span></span>
        <p class="hint">E-쿠폰 구매 수량을 입력하여 주십시오. 실제 거래기록에는 영향을 주지 않습니다.</p>
      </dd>
    </dl>
    <dl>
      <dt>1인 구매 한정 수량 ：</dt>
      <dd>
        <input class="w70 text" id="upper_limit" name="upper_limit" type="text" value="0"/>
        <span></span>
        <p class="hint">각 구매자 ID 마다 구매할 수 있는 최대 수량을 입력하여 주십시오.<br>'0'을 입력하시면 수량 제한없음을 뜻합니다.</p>
      </dd>
    </dl>
    <dl>
      <dt>내용</dt>
      <dd>
        <?php showEditor('groupbuy_intro','','740px','360px','','false',false);?>
        <p class="hr8"><a class="des_demo ncsc-btn" href="index.php?act=store_album&op=pic_list&item=groupbuy"><i class="icon-picture"></i>앨범에서선택</a></p>
        <p id="des_demo" style="display:none;"></p>
      </dd>
    </dl>
    <div class="bottom"><label class="submit-border">
      <input type="submit" class="submit" value="완료"></label>
    </div>
  </form>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){

$("select[name=class]").change(function(){
    var class_id = $(this).val();
    $.ajax({
        type:'GET',
        url:'index.php?act=store_groupbuy&op=ajax_vr_class&class_id='+class_id,
        success:function(json){
            var html = '<option value="">'+'선택'+'</option>';
            if(json){
                var data = eval("("+json+")");
                $.each(data,function(i,val){
                    html+='<option value="'+val.class_id+'">'+val.class_name+'</option>';
                });
            }
            $("select[name=s_class]").html(html);
        }
    });
});

$("select[name=city]").change(function(){
    var area_id = $(this).val();
    $.ajax({
        type:'GET',
        url:'index.php?act=store_groupbuy&op=ajax_vr_area&area_id='+area_id,
        success:function(json){
            var html = '<option value="">'+'선택'+'</option>';
            var mall = '<option value="">'+'선택'+'</option>';
            if(json){
                var data = eval("("+json+")");
                $.each(data,function(i,val){
                    html+='<option value="'+val.area_id+'">'+val.area_name+'</option>';
                });
            }
            $("select[name=area]").html(html);
            $("select[name=mall]").html(mall);
        }
    });
});

$("select[name=area]").change(function(){
    var area_id = $(this).val();
    $.ajax({
        type:'GET',
        url:'index.php?act=store_groupbuy&op=ajax_vr_area&area_id='+area_id,
        success:function(json){
            var html = '<option value="">'+'선택'+'</option>';
            if(json){
                var data = eval("("+json+")");
                $.each(data,function(i,val){
                    html+='<option value="'+val.area_id+'">'+val.area_name+'</option>';
                });
            }
            $("select[name=mall]").html(html);
        }
    });
});

    $('#start_time').datetimepicker({
        controlType: 'select'
    });

    $('#end_time').datetimepicker({
        controlType: 'select'
    });

    $('#btn_show_search_goods').on('click', function() {
        $('#div_search_goods').show();
    });

    $('#btn_hide_search_goods').on('click', function() {
        $('#div_search_goods').hide();
    });

    //搜索商品
    $('#btn_search_goods').on('click', function() {
        var url = "<?php echo urlShop('store_groupbuy', 'search_vr_goods'); ?>";
        url += '&' + $.param({goods_name: $('#search_goods_name').val()});
        $('#div_goods_search_result').load(url);
    });

    $('#div_goods_search_result').on('click', 'a.demo', function() {
        $('#div_goods_search_result').load($(this).attr('href'));
        return false;
    });

    var vrExpireTime = 0;
    var vrLimitNum = 0;

    //选择商品
    $('#div_goods_search_result').on('click', '[cnbiztype="btn_add_groupbuy_goods"]', function() {
        var goods_commonid = $(this).attr('data-goods-commonid');
        $.get('<?php echo urlShop('store_groupbuy', 'groupbuy_goods_info'); ?>', {goods_commonid: goods_commonid}, function(data) {
            if(data.result) {
                $('#groupbuy_goods_id').val(data.goods_id);
                $('#groupbuy_goods_image').attr('src', data.goods_image);
                $('#groupbuy_goods_name').text(data.goods_name);
                $('[cnbiztype="groupbuy_goods_price"]').text(data.goods_price);
                $('[cnbiztype="groupbuy_goods_href"]').attr('href', data.goods_href);
                $('[cnbiztype="groupbuy_goods_info"]').show();
                $('#div_search_goods').hide();
                // vr
                vrExpireTime = '' + data.virtual_indate;
                $('#vr-expire-time').html('（'+data.virtual_indate_str+'）');
                vrLimitNum = data.virtual_limit;
            } else {
                showError(data.message);
            }
        }, 'json');
    });

    //업로드
    $('[cnbiztype="btn_upload_image"]').fileupload({
        dataType: 'json',
            url: "<?php echo urlShop('store_groupbuy', 'image_upload');?>",
            add: function(e, data) {
                $parent = $(this).parents('dd');
                $input = $parent.find('[cnbiztype="groupbuy_image"]');
                $img = $parent.find('[cnbiztype="img_groupbuy_image"]');
                data.formData = {old_groupbuy_image:$input.val()};
                $img.attr('src', "<?php echo SHOP_TEMPLATES_URL.'/images/loading.gif';?>");
                data.submit();
            },
            done: function (e,data) {
                var result = data.result;
                $parent = $(this).parents('dd');
                $input = $parent.find('[cnbiztype="groupbuy_image"]');
                $img = $parent.find('[cnbiztype="img_groupbuy_image"]');
                if(result.result) {
                    $img.prev('i').hide();
                    $img.attr('src', result.file_url);
                    $img.show();
                    $input.val(result.file_name);
                } else {
                    showError(data.message);
                }
            }
    });

    jQuery.validator.methods.lessThanVrLimitNum = function(value, element) {
        var v = parseInt(value) || 0;
        if (v > 0 && vrLimitNum && vrLimitNum > 0 && v > vrLimitNum)
            return false;
        return true;
    };

    jQuery.validator.methods.lessThanVrExpireTime = function(value, element) {
        var ts = new Date(Date.parse(value.replace(/-/g, "/"))).getTime() / 1000;
        // console.log(vrExpireTime);
        // console.log(ts);
        return vrExpireTime > ts;
    };

    jQuery.validator.methods.greaterThanDate = function(value, element, param) {
        var date1 = new Date(Date.parse(param.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };

    jQuery.validator.methods.lessThanDate = function(value, element, param) {
        var date1 = new Date(Date.parse(param.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 > date2;
    };

    jQuery.validator.methods.greaterThanStartDate = function(value, element) {
        var start_date = $("#start_time").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };

    jQuery.validator.methods.checkGroupbuyGoods = function(value, element) {
        var start_time = $("#start_time").val();
        var result = true;
        $.ajax({
            type:"GET",
            url:'<?php echo urlShop('store_groupbuy', 'check_groupbuy_goods');?>',
            async:false,
            data:{start_time: start_time, goods_id: value},
            dataType: 'json',
            success: function(data){
                if(!data.result) {
                    result = false;
                }
            }
        });
        return result;
    };

    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span');
            error_td.append(error);
        },
        onfocusout: false,
    	submitHandler:function(form){
    		ajaxpost('add_form', '', '', 'onerror');
    	},
        rules : {
            groupbuy_name: {
                required : true
            },
            start_time : {
                required : true,
                greaterThanDate : '<?php echo date('Y-m-d H:i',$output['groupbuy_start_time']);?>'
            },
            end_time : {
                required : true,
<?php if (!$output['isOwnShop']) { ?>
                lessThanDate : '<?php echo date('Y-m-d H:i',$output['current_groupbuy_quota']['end_time']);?>',
<?php } ?>
                lessThanVrExpireTime : true,
                greaterThanStartDate : true
            },
            groupbuy_goods_id: {
                required : true,
                checkGroupbuyGoods: true
            },
            groupbuy_price_ko: {
                required : true,
                number : true,
                min : 0.01,
                max : 1000000
            },
            virtual_quantity: {
                required : true,
                digits : true
            },
            upper_limit: {
                required : true,
                digits : true,
                lessThanVrLimitNum : true
            },
            groupbuy_image: {
                required : true
            }
        },
        messages : {
            groupbuy_name_ko: {
                required : '<i class="icon-exclamation-sign"></i>공동구매명을 입력하세요.'
            },
            start_time : {
                required : '<i class="icon-exclamation-sign"></i>공동구매 시작시간을 입력해주세요. ',
                greaterThanDate : '<i class="icon-exclamation-sign"></i><?php echo sprintf('공동구매 최소 시작시간{0}',date('Y-m-d H:i',$output['current_groupbuy_quota']['start_time']));?>'
            },
            end_time : {
                required : '<i class="icon-exclamation-sign"></i>공동구매 종료시간을 입력해주세요. ',
<?php if (!$output['isOwnShop']) { ?>
                lessThanDate : '<i class="icon-exclamation-sign"></i><?php echo sprintf('공동구매 최대 종료시간{0}',date('Y-m-d H:i',$output['current_groupbuy_quota']['end_time']));?>',
<?php } ?>
                lessThanVrExpireTime : '<i class="icon-exclamation-sign"></i>종료시간은 E-쿠폰상품 유효기간 이전 이어야 합니다. ',
                greaterThanStartDate : '<i class="icon-exclamation-sign"></i>종료시간은 시작시간 이후로 입력해 주세요. '
            },
            groupbuy_goods_id: {
                required : '<i class="icon-exclamation-sign"></i><?php echo $lang['group_goods_error'];?>',
                checkGroupbuyGoods: '해당상품은 이미 프로모션 진행중 입니다. '
            },
            groupbuy_price_ko: {
                required : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.',
                number : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.',
                min : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.',
                max : '<i class="icon-exclamation-sign"></i>정확한 공동구매 가격을 입력하세요.'
            },
            virtual_quantity: {
                required : '<i class="icon-exclamation-sign"></i>E-쿠폰 상품 상품 공동구매 수량을 "정수"로 입력하세요.',
                digits : '<i class="icon-exclamation-sign"></i>E-쿠폰 상품 상품 공동구매 수량을 "정수"로 입력하세요.'
            },
            upper_limit: {
                required : '<i class="icon-exclamation-sign"></i>수량을 "정수"로 입력하세요.',
                digits : '<i class="icon-exclamation-sign"></i>수량을 "정수"로 입력하세요.',
                lessThanVrLimitNum : 'E-쿠폰상품 공동구매 한정 수량은 해당 E-쿠폰상품 수량보다 많을 수 없습니다. '
            },
            groupbuy_image: {
                required : '<i class="icon-exclamation-sign"></i>이미지를 등록해 주세요. '
            }
        }
    });

	$('#li_1').click(function(){
		$('#li_1').attr('class','active');
		$('#li_2').attr('class','');
		$('#demo').hide();
	});

	$('#goods_demo').click(function(){
		$('#li_1').attr('class','');
		$('#li_2').attr('class','active');
		$('#demo').show();
	});

	$('.des_demo').click(function(){
		if($('#des_demo').css('display') == 'none'){
            $('#des_demo').show();
        }else{
            $('#des_demo').hide();
        }
	});

    $('.des_demo').ajaxContent({
        event:'click', //mouseover
            loaderType:"img",
            loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/loading.gif",
            target:'#des_demo'
    });
});

function insert_editor(file_path){
	KE.appendHtml('goods_body', '<img src="'+ file_path + '">');
}
</script>
