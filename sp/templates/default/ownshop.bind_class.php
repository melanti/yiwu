<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>자영업 업체</h3>
      <ul class="tab-base">
        <li><a href="index.php?act=ownshop&op=list"><span>관리</span></a></li>
        <li><a href="index.php?act=ownshop&op=add"><span>추가</span></a></li>
        <li><a href="javascript:void(0);" class="current"><span>경영항목</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
<form id="form_bd" name="" method="post" action="index.php?act=ownshop&op=bind_class&id=<?php echo $_GET['id'];?>">
<input type="hidden" value="ok" name="form_submit">
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div>
        </th>
      </tr>
      <tr>
        <td><ul>
            <li>업체 경영항목 삭제시 해당 상품은 내림상태로 변경됩니다.</li>
            <li>모든 수정은 바로 적용됩니다.</li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <table class="table tb-type2">
    <thead class="thead">
      <tr class="space">
        <th colspan="15">업체명：<?php echo $output['store_info']['store_name'];?></th>
      </tr>
    </thead>
    <thead>
      <tr class="thead">
        <th></th>
        <th>카테고리1</th>
        <th>카테고리2</th>
        <th>카테고리3</th>
        <th>수수료율</th>
        <th class="align-center"><?php echo $lang['nc_handle'];?></th>
      </tr>
    </thead>
    <tbody>
      <?php if(!empty($output['store_bind_class_list']) && is_array($output['store_bind_class_list'])){ ?>
      <?php foreach($output['store_bind_class_list'] as $key => $value){ ?>
      <tr class="hover edit">
        <td><input class="checkitem" type="checkbox" value="<?php echo $value['bid'];?>" name="bid[]"></td>
        <td class="w25pre"><?php echo $value['class_1_name'];?></td>
        <td class="w25pre"><?php echo $value['class_2_name'];?></td>
        <td class="w25pre"><?php echo $value['class_3_name'];?></td>
        <td class="sort"><span nc_type="commis_rate" column_id="<?php echo $value['bid'];?>" title="<?php echo $lang['nc_editable'];?>" class="editable " style="vertical-align: middle; margin-right: 4px;"><?php echo $value['commis_rate'];?></span>% </td>
        <td class="w60 align-center"><a cbtype="btn_del_store_bind_class" href="javascript:;" data-bid="<?php echo $value['bid'];?>">삭제</a></td>
      </tr>
      <?php } ?>
      <?php }else { ?>
      <tr class="no_data">
        <td colspan="10"><?php echo $lang['nc_no_record'];?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
    <tr class="tfoot">
    <td>
    <input id="checkallBottom" class="checkall" type="checkbox">
    </td>
    <td colspan="15">
    <label for="checkallBottom">全选</label>
    <a class="btn" onclick="if(confirm('您确定要삭제该업체등급吗？')){$('#form_bd').submit();}" href="JavaScript:void(0);">
    <span>삭제</span>
    </a>
    <?php if(!empty($output['store_bind_class_list']) && is_array($output['store_bind_class_list'])){ ?>
    <div class="pagination"><?php echo $output['showpage'];?></div>
    <?php } ?>
    </td>
    </tr>
    </tfoot>
  </table>
  </form>
  <table class="table tb-type2" >
    <thead class="thead">
      <tr class="space">
        <th colspan="15"><span>경영항목 추가</span></th>
      </tr>
    </thead>
    <tbody>
      <tr class="noborder">
        <td class="required" colspan="2" >카테고리 선택：</td>
      </tr>
      <tr class="noborder">
        <td colspan="2" id="gcategory" class="vatop rowform"><select id="gcategory_class1" style="width: auto;">
            <option value="0">선택하세요</option>
            <?php if(!empty($output['gc_list']) && is_array($output['gc_list']) ) {?>
            <?php foreach ($output['gc_list'] as $gc) {?>
            <option value="<?php echo $gc['gc_id'];?>" data-explain="<?php echo $gc['commis_rate']; ?>"><?php echo $gc['gc_name'];?></option>
            <?php }?>
            <?php }?>
          </select><span id="error_message" style="color:red;"></span></td>
      </tr>
      <tr>
        <td class="required" colspan="2" >수수료율(0-100사이의 정수를 입력하세요)：</td>
      </tr>
      <tr class="noborder">
        <td class="vatop rowform"><form id="add_form" action="<?php echo urlAdmin('ownshop', 'bind_class_add');?>" method="post">
            <input name="store_id" type="hidden" value="<?php echo $output['store_info']['store_id'];?>">
            <input id="goods_class" name="goods_class" type="hidden" value="">
            <input id="commis_rate" name="commis_rate" class="w60" type="text" value="0" />
            % <span id="error_message1" style="color:red;"></span>
          </form></td>
        <td class="vatop tips"></td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="2"><a id="btn_add_category" class="btn" href="JavaScript:void(0);" /><span>확인</span></a></td>
      </tr>
    </tfoot>
  </table>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.edit.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){
    gcategoryInit("gcategory");

    // 提交新추가的항목
    $('#btn_add_category').on('click', function() {
        $('#error_message').hide();
        $('#error_message1').hide();
        var category_id = '';
        var validation = true;
        $('#gcategory').find('select').each(function() {
            if(parseInt($(this).val(), 10) > 0) {
                category_id += $(this).val() + ',';
            }
        });
        if (parseInt($('#gcategory').find('select').eq(0).val()) == 0) {
        	validation = false;
        }
        if(!validation) {
            $('#error_message').text('카테고리를 선택하세요');
            $('#error_message').show();
            return false;
        }

        var commis_rate = parseInt($('#commis_rate').val(), 10);
        if(isNaN(commis_rate) || commis_rate < 0 || commis_rate > 100) {
            $('#error_message1').text('수수료율을 입력하세요');
            $('#error_message1').show();
            return false;
        }

        $('#goods_class').val(category_id);
        $('#add_form').submit();
    });

    $('#gcategory select').live('change', function() {
        var cr = $(this).children(':selected').attr('data-explain');
        $('#commis_rate').val(parseInt(cr) || 0);
    });

    // 삭제现有항목
    $('[cbtype="btn_del_store_bind_class"]').on('click', function() {
        if(confirm('모든 상품이 내림 상태로 변경됩니다, 정말 삭제하시겠습니까?')) {
            var bid = $(this).attr('data-bid');
            $this = $(this);
            $.post('<?php echo urlAdmin('ownshop', 'bind_class_del');?>', {bid: bid}, function(data) {
                 if(data.result) {
                     $this.parents('tr').hide();
                 } else {
                     showError(data.message);
                 }
            }, 'json');
        }
    });

    // 修改수수료율
    $('span[nc_type="commis_rate"]').inline_edit({act: 'ownshop',op: 'bind_class_update'});
});

</script>
