<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['article_class_index_del_succ']	= '카테고리 삭제 성공.';
$lang['article_class_index_choose']		= '삭제할 내용을 선택하세요!';
$lang['article_class_index_class']		= '문장 카테고리';
$lang['article_class_index_name']		= '카테고리명';
$lang['article_class_index_ensure_del']	= '본 카테고리 삭제시 하위 카테고리도 같이 삭제됩니다, 정말 삭제하시겠습니까?';
$lang['article_class_index_help1']		= '관리자 새로운 문장 추가시 카테고리를 선택할 수 있습니다.';
$lang['article_class_index_help2']		= '기본 문장 카테고리는 삭제할 수 없습니다.';
/**
 * 카테고리추가
 */
$lang['article_class_add_sup_class']	= '상위카테고리';
$lang['article_class_add_sup_class_notice']	= '만약에 상위 카테고리를 선택시 추가되는 카테고리는 서브 카테고리가 됩니다.';
$lang['article_class_add_sup_class_notice2']	= '만약에 상위 카테고리를 선택시 수정되는 카테고리는 서브 카테고리가 됩니다.';
$lang['article_class_add_update_sort']	= '순위 업데이트';
$lang['article_class_add_name_null']	= '카테고리명을 입력하세요';
$lang['article_class_add_name_exists']	= '이미 존재합니다, 다른 이름을 사용해주세요';
$lang['article_class_add_sort_int']		= '순위는 반드시 숫자만 가능합니다.';
$lang['article_class_add_back_to_list']	= '카테고리 리스트 돌아가기';
$lang['article_class_add_class']		= '새 카테고리 계속 추가';
$lang['article_class_add_succ']			= '카테고리 추가 성공!';
$lang['article_class_add_fail']			= '카테고리 추가 실패!';
/**
 * 수정카테고리
 */
$lang['article_class_edit_again']		= '본 카테고리 재수정';
$lang['article_class_edit_succ']		= '카테고리 수정 성공';
$lang['article_class_edit_fail']		= '카테고리 수정 실패';
/**
 * 삭제카테고리
 */
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';
$lang['article_class_del_']	= '';