
(function($, angular, _, APP){
  'use strict';

  APP.createNS('APP.modules.org.list.ctrl.accOrgListSearchCtrl');
  APP.modules.org.list.ctrl.accOrgListSearchCtrl = accOrgListSearchCtrl;

  function accOrgListSearchCtrl($scope, $q, _, $timeout, XHR, PubSub, AccOrgListTableManager, API_URL_INFO, COMMON_UTIL, $sce) {
    var ctrl = this;

    ctrl.reqModel = {
      search_type:0, //{ 0 | 1 | 2 | 3 | 4} ( 전체 |코드 | 사업자 등록번호 | 운영업체 명칭 | 담당자명 )
      search_keyword:'',
      service_type:3, //0 :shop, 1:cat , 2:mirror, 3:all
      status:2, //0 : activated, 1 :disabled, 2 : all,
      org_id: ''
    };

    ctrl.getTotal;
    ctrl.getStart;
    ctrl.getCount;
    ctrl.getReqModel;
    ctrl.setReqModel;
    ctrl.updatePage;
    ctrl.updateList = _updateList;
    ctrl.storeList = [];
    ctrl.storeOrgId = "";

    _init();

    function _init(){
      _registWatcher();
      _getStoreList();

      $timeout(function(){
        ctrl.reqModel.search_type = 2;
        ctrl.reqModel.search_type = 0;
      }, 2000);
      ctrl.setReqModel({options:_.extend({},ctrl.reqModel)});
    }

    function _getStoreList() {
      if (APP.info.user.org_id !== APP.info.user.parent_org_id) {
        return;
      }
      var params = COMMON_UTIL.getQueryPath({
        parent_org_id: APP.info.user.org_id
      }, '?'); //obj, prefix, postfix)
      XHR
        .REQ({
          method: 'get',
          url: API_URL_INFO.GET_SELECT_MINOR_ORGANIZATION + params
        })
        .then(
          function (__sucData) {
            console.log(__sucData);
            ctrl.storeList = __sucData.data.results;
          },
          function (__errData) {
            console.log(__errData);
          }
        );
    }

    function _registWatcher(){
      $scope.$on('$destroy', function () {
      });

      $scope.$watch( 'tableSearchCtrl.options.query_opt', function _watchQueryOpt(__newVal , __oldVal){
        if(__newVal===__oldVal){
        }else{
          ctrl.reqModel.search_keyword = '';
        }
      });

      // PubSub.subscribeOnce('tableComponent:bootstrap:complete', function _completeBootstrap(topic, data){
      //   if(data.success){
      //   }
      // });

      PubSub.subscribe('table:change',function _onChangeTableData(topic, data){
        console.log('_onChangeTableData: ' , data);
        ctrl.reqModel = _.extend({},data.reqModel);
      });
    }

    function _updateList(){
      ctrl.reqModel.org_id = ctrl.storeOrgId;

      if (ctrl.reqModel.search_type !== 0 && ctrl.reqModel.search_keyword === '') {
        alert('검색어를 입력해주세요.');
        $('#search_keyword').focus();
        return false;
      }
      ctrl.setReqModel({options: _.extend({}, ctrl.reqModel)});
      ctrl.updatePage({options: {start: 0}});
    }

  }

  accOrgListSearchCtrl.$inject = ['$scope', '$q', '_', '$timeout', 'XHR', 'PubSub', 'AccOrgListTableManager', 'API_URL_INFO', 'COMMON_UTIL', '$sce'];
  accOrgListSearchCtrl.bindToController = {
    getTotal:'&',
    getStart:'&',
    getCount:'&',
    getReqModel:'&',
    setReqModel:'&',
    updatePage:'&'
  };
  accOrgListSearchCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableSearchCtrl:true});
    }
  }

})(window.jQuery, window.angular, window._, window.APP);
