/**
 * grunt/pipeline.js
 *
 * The order in which your css, javascript, and template files should be
 * compiled and linked from your views and static HTML files.
 *
 * (Note that you can take advantage of Grunt-style wildcard/glob/splat expressions
 * for matching multiple files.)
 *
 * For more information see:
 *   https://github.com/balderdashy/sails-docs/blob/master/anatomy/myApp/tasks/pipeline.js.md
 */

// Path to public folder
var tmpPath = '.tmp/public/',
  libPath = 'modules/dependencies',
  appPath = 'modules/app';

// CSS files to inject in order
//
// (if you're using LESS with the built-in default config, you'll want
//  to change `assets/styles/importer.less` instead.)
var cssFilesToInject = [
  'styles/**/*.css'
];

// Client-side javascript files to inject in order
// (uses Grunt-style wildcard/glob/splat expressions)

var jsFilesToInjectBase = [

  // Load sails.io before everything else
  // libPath+'/sails.io/sails.io.js',


  // libPath+'/jquery.1.11.3/jquery.min.js',

  // libPath+'/modernizr/modernizr.min.js',
  // libPath+'/jquery.cookie/jquery.cookie.js',
  // libPath+'/jquery.transit/jquery.transit.min.js',
  // libPath+'/jquery.datepicker/jquery.datepicker.js',
  // libPath+'/jquery.nestable/jquery.nestable.js',
  // libPath+'/is_js/is.min.js',
  // libPath+'/underscore.1.8.3/underscore-min.js',
  // libPath+'/crypto/sha256.js',
  // libPath+'/bootstrap.3.3.6/bootstrap.min.js',
  //
  // libPath+'/angular.1.4.9/angular.min.js',
  // libPath+'/angular-ui-router-0.2.15/angular-ui-router.min.js',
  // libPath+'/angular.ui.date/angular.ui.date.js',
  // libPath+'/angular.sanitize/angular.sanitize.min.js',
  // libPath+'/angular-message/angular-message-1.4.9.js',
  // libPath+'/angular-pubsub/angular-pubsub.js'

];

var jsFilesToInject = [

  appPath+'/common/app_common.js',
  appPath+'/common/provider/sha256_service.js',
  appPath+'/common/provider/underscore_service.js',
  appPath+'/common/provider/app_common_service.js',
  appPath+'/common/filter/app_common_filter.js',
  appPath+'/common/directive/table_directive_util.js',
  appPath+'/common/directive/**/*',
  appPath+'/common/ctrl/table/*',
  appPath+'/common/provider/operation_table_manager.js',
  appPath+'/common/ctrl/td_prev_modal_small_ctrl.js',
  appPath+'/common/ctrl/threeD_dash_board_ctrl.js',
  appPath+'/items_mng/items/items_list/provider/**.js',
  appPath+'/items_mng/items/td_list/provider/**.js',
  appPath+'/items_mng/items/td_list/ctrl/**.js',
  appPath+'/items_mng/items/td_list/threeD_list_modal.js'
  /////////////////////////////////////////////////////////////////////////////////////////////

  // appPath+'/login/ctrl/**/*',
  // appPath+'/login/login.js',
  //
  // appPath+'/opr_mng/org/admin_list/provider/!**.js',
  // appPath+'/opr_mng/org/admin/provider/!**.js',
  // appPath+'/opr_mng/org/admin/ctrl/!**.js',
  //
  // appPath+'/opr_mng/org/store/provider/!**.js',
  // appPath+'/opr_mng/org/store/ctrl/!**.js',
  //
  // appPath+'/opr_mng/org/store_list/provider/!**.js',
  // appPath+'/opr_mng/org/store_list/ctrl/!**.js',
  //
  // appPath+'/opr_mng/acc/org_list/provider/!**.js',
  // appPath+'/opr_mng/acc/org_list/ctrl/!**.js',
  // appPath+'/opr_mng/acc/org/ctrl/account_ctrl.js',
  //
  //
  // appPath+'/items_mng/category/category_tree/provider/!**.js',
  // appPath+'/items_mng/category/category_tree/ctrl/!**.js',
  //
  // appPath+'/items_mng/category/brand/ctrl/!**.js',
  //
  // appPath+'/items_mng/category/brand_list/provider/**.js',
  // appPath+'/items_mng/category/brand_list/ctrl/!**.js',
  //
  // appPath+'/items_mng/items/item/provider/!**.js',
  // appPath+'/items_mng/items/item/ctrl/!**.js',
  //

  // appPath+'/items_mng/items/items_list/ctrl/!**.js',
  //
  // appPath+'/opr_mng/org/admin/admin.js',
  // appPath+'/opr_mng/org/store/store.js',
  // appPath+'/opr_mng/org/store_list/store_list.js',
  // appPath+'/opr_mng/acc/org/account.js',
  // appPath+'/opr_mng/acc/org_list/org_list.js',
  //
  // appPath+'/items_mng/category/brand/brand_registration.js',
  //
  // appPath+'/items_mng/category/brand_list/brand_list.js',
  //
  // appPath+'/items_mng/category/category_tree/category_tree.js',
  // appPath+'/items_mng/items/item/item_registration.js',
  // appPath+'/items_mng/items/items_list/items_list.js',






];

var jsFilesToInjectAll = jsFilesToInjectBase.concat(jsFilesToInject);


// Client-side HTML templates are injected using the sources below
// The ordering of these templates shouldn't matter.
// (uses Grunt-style wildcard/glob/splat expressions)
//
// By default, Sails uses JST templates and precompiles them into
// functions for you.  If you want to use jade, handlebars, dust, etc.,
// with the linker, no problem-- you'll just want to make sure the precompiled
// templates get spit out to the same file.  Be sure and check out `tasks/README.md`
// for information on customizing and installing new tasks.
var templateFilesToInject = [
  'templates/**/*.html'
];


// Prefix relative paths to source files so they point to the proper locations
// (i.e. where the other Grunt tasks spit them out, or in some cases, where
// they reside in the first place)
module.exports.cssFilesToInject = cssFilesToInject.map(transformPath);
module.exports.jsFilesToInject = jsFilesToInject.map(transformPath);
module.exports.jsFilesToInjectBase = jsFilesToInjectBase.map(transformPath);
module.exports.jsFilesToInjectAll = jsFilesToInjectAll.map(transformPath);
module.exports.templateFilesToInject = templateFilesToInject.map(transformPath);

// Transform paths relative to the "assets" folder to be relative to the public
// folder, preserving "exclude" operators.
function transformPath(path) {
  return (path.substring(0,1) == '!') ? ('!' + tmpPath + path.substring(1)) : (tmpPath + path);
}

