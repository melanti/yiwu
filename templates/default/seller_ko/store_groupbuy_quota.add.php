<?php defined('InCNBIZ') or exit('Access Invalid!'); ?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
  <form id="add_form" action="<?php echo urlShop('store_groupbuy', 'groupbuy_quota_add_save');?>" method="post">
    <dl>
      <dt><i class="required">*</i>구매수량</dt>
      <dd>
          <input id="groupbuy_quota_quantity" name="groupbuy_quota_quantity" type="text" class="text w30" /><em class="add-on">개월</em><span></span>
        <p class="hint">구매단위 : 월(30일) 해당 구매 기한 내에 공동구매를 등록/진행 하실 수 있습니다.</p>
        <p class="hint"><?php $output['setting_config']['groupbuy_price'].'원/월';?> ; </p>
        <p class="hint"><strong style="color: red">관련 비용은 정산시 차감됩니다. </strong></p>
      </dd>
    </dl>
    <div class="bottom">
      <label class="submit-border"><input id="submit_button" type="submit" class="submit" value="완료"></label>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){
    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span');
            error_td.append(error);
        },
    	submitHandler:function(form){
            var unit_price = <?php echo C('groupbuy_price');?>;
            var quantity = $("#groupbuy_quota_quantity").val();
            var price = unit_price * quantity;
            showDialog('정말 구매하시겠습니까? 총 지불금액 : '+price+'원', 'confirm', '', function(){
            	ajaxpost('add_form', '', '', 'onerror');
            	});
    	},
        rules : {
            groupbuy_quota_quantity : {
                required : true,
                digits : true,
                min : 1
            }
        },
        messages : {
            groupbuy_quota_quantity : {
                required : "<i class='icon-exclamation-sign'></i>수량을 숫자로 입력해 주세요.",
                digits : "<i class='icon-exclamation-sign'></i>수량을 숫자로 입력해 주세요.",
                min : "<i class='icon-exclamation-sign'></i>수량을 숫자로 입력해 주세요."
            }
        }
    });
});
</script>
