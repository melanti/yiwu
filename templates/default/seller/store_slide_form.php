<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default"> <div class="alert">
      <ul>
        <li>1. 최대 5장의 슬라이드 이미지를 등록하실 수 있습니다.</li>
        <li>2. JPEG,JPG,GIF,PNG 형식 파일 / 너비 998px 높이 300px / 크기 1.00M의 이미지가 가능합니다. <br>두장 이상의 이미지를 업로드 하셔야 슬라이드 이미지로 나타납니다. </li>
        <li>3. 등록 완료 후, 꼭 완료 버튼을 클릭해 주셔야 합니다. </li>
        <li>4. URL주소를 사용할 경우 "http://"를 포함해야 합니다.</li>
      </ul>
    </div>
  <div class="flexslider">
    <ul class="slides">
      <?php if(!empty($output['store_slide']) && is_array($output['store_slide'])){?>
      <?php for($i=0;$i<5;$i++){?>
      <?php if($output['store_slide'][$i] != ''){?>
      <li><a <?php if($output['store_slide_url'][$i] != '' && $output['store_slide_url'][$i] != 'http://'){?>href="URL주소 입력"<?php }?>><img src="<?php echo UPLOAD_SITE_URL.ATTACH_SLIDE.DS.$output['store_slide'][$i];?>"></a></li>
      <?php }?>
      <?php }?>
      <?php }else{?>
      <li> <img src="<?php echo UPLOAD_SITE_URL.ATTACH_SLIDE.DS;?>f01.jpg"> </li>
      <li> <img src="<?php echo UPLOAD_SITE_URL.ATTACH_SLIDE.DS;?>f02.jpg"> </li>
      <li> <img src="<?php echo UPLOAD_SITE_URL.ATTACH_SLIDE.DS;?>f03.jpg"> </li>
      <li> <img src="<?php echo UPLOAD_SITE_URL.ATTACH_SLIDE.DS;?>f04.jpg"> </li>
      <?php }?>
    </ul>
  </div>
  <form action="index.php?act=store_setting&op=store_slide" id="store_slide_form" method="post" onsubmit="ajaxpost('store_slide_form', '', '', 'onerror');return false;">
    <input type="hidden" name="form_submit" value="ok" />
    <!-- 업로드部分 -->
    <ul class="ncsc-store-slider" id="goods_images">
      <?php for($i=0;$i<5;$i++){?>
      <li nc_type="handle_pic" id="thumbnail_<?php echo $i;?>">
        <div class="picture" cnbiztype="file_<?php echo $i;?>">
          <?php if (empty($output['store_slide'][$i])) {?>
          <i class="icon-picture"></i>
          <?php } else {?>
          <img cnbiztype="file_<?php echo $i;?>" src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_SLIDE.DS.$output['store_slide'][$i];?>" />
          <?php }?>
          <input type="hidden" name="image_path[]" cnbiztype="file_<?php echo $i;?>" value="<?php echo $output['store_slide'][$i];?>" /><a href="javascript:void(0)" cnbiztype="del" class="del" title="삭제">X</a></div>
        
        <div class="url">
          <label>URL주소 입력</label>
          <input type="text" class="text w150" name="image_url[]" value="<?php if($output['store_slide_url'][$i] == ''){  echo 'http://';}else{echo $output['store_slide_url'][$i];}?>" />
        </div>
         <div class="ncsc-upload-btn"> <a href="javascript:void(0);"><span>
          <input type="file" hidefocus="true" size="1" class="input-file" name="file_<?php echo $i;?>" id="file_<?php echo $i;?>"/>
          </span>
          <p><i class="icon-upload-alt"></i>업로드</p>
          </a></div></li>
      <?php } ?>
    </ul>
   <div class="bottom"><label class="submit-border"><input type="submit" class="submit" value="완료"></label></div>
  </form>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js" charset="utf-8"></script> 
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_slide.js" charset="utf-8"></script>
<!-- 引入幻灯片JS --> 
<script type="text/javascript" src="<?php echo DIR_RESOURCE;?>/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript">
var SITEURL = "<?php echo SHOP_SITE_URL;?>";
var SHOP_TEMPLATES_URL = '<?php echo SHOP_TEMPLATES_URL;?>';
var UPLOAD_SITE_URL = '<?php echo UPLOAD_SITE_URL;?>';
var ATTACH_COMMON = '<?php echo ATTACH_COMMON;?>';
var ATTACH_STORE = '<?php echo ATTACH_STORE;?>';
var SHOP_RESOURCE_SITE_URL = '<?php echo SHOP_RESOURCE_SITE_URL;?>';
</script> 
