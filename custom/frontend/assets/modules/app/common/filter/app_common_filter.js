(function($, angular, _, APP){
  'use strict';

  angular.module('App.common')
    .filter('datestrfilter',function($filter){
      return function(input){
        if(input == null){ return ""; }
        return input.substr(0,4)+'-'+input.substr(5,2)+'-'+input.substr(8,2);
      };
    });

  angular.module('App.common')
    .filter('convertServiceName', function () {
      return function (str) {
        var _str = str.split(',');
        var _strLength = _str.length;
        var serviceName = "";
        for (var i = 0; i < _strLength; i++) {
          var _s = _str[i].trim();
          serviceName += _s == '0' ? 'Shop' : _s == '1' ? 'CAT' : _s == '2' ? 'FX Mirror' : '';
          serviceName += _str.length != (i + 1) ? ', ' : '';
        }
        return serviceName;
      }
  });

  angular.module('App.common')
    .filter('convertStrToNumberUTCValue', function () {
      return function (str) {
        if(str){
          return 0;
        }else{
          return Number( str.substr(0,3) );
        }

      }
    });

})(window.jQuery, window.angular, window._, window.APP);
