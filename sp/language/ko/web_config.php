<?php
defined('InCNBIZ') or exit('Access Invalid!');

/**
 * 模板页
 */
$lang['index_index_store_goods_price']		= '商城价';

/**
 * 리스트页和수정页
 */
$lang['web_config_index']			= '메인설정';
$lang['web_config_index_help1']			= '작은 순위로 정렬됩니다.';
$lang['web_config_index_help2']			= '칼라 스타일은 프론트와 일치됩니다, 기본설정에서 변경 가능합니다.';
$lang['web_config_index_help3']			= '칼라 스타일은 CSS양식중 이미 존재합니다, 해당 프로그램을 수정해야만 유효됩니다.';
$lang['web_config_update_time']	= '업데이트 시간';
$lang['web_config_web_name']				= '채널이름';
$lang['web_config_style_name']				= '칼라 스타일';
$lang['web_config_web_edit']				= '기본설정';
$lang['web_config_code_edit']				= '채널수정';
$lang['web_config_web_name_tips']				= '채널 이름은 관리자 페이지의 탬플릿 설정에서만 노출됩니다.';
$lang['web_config_style_name_tips']				= '칼라 스타일 선택하시면 메인 페이지의 배경,폰트 칼라등에 영향을 받습니다.';
$lang['web_config_style_red']				= '레드';
$lang['web_config_style_pink']				= '핑크';
$lang['web_config_style_orange']				= '오렌지';
$lang['web_config_style_green']				= '그린';
$lang['web_config_style_blue']				= '블루';
$lang['web_config_style_purple']				= '퍼플러';
$lang['web_config_style_brown']				= '브라운';
$lang['web_config_style_gray']				= '그레이';
$lang['web_config_add_name_null']				= '채널 이름을 입력하세요';
$lang['web_config_sort_int']		= '순위는 반드시 숫자만 가능합니다.';
$lang['web_config_sort_tips']	= '0~255이내의 숫자를 입력하세요';

/**
 * 板块수정页
 */
$lang['web_config_save']			= '저장';
$lang['web_config_web_html']			= '채널내용 업데이트';
$lang['web_config_edit_help1']			= '모든 설정 완료, 하단의 "채널내용 업데이트"를 눌러야만 프론트단에 변경됩니다.';
$lang['web_config_edit_help2']			= '왼쪽 "추천 카테고리"는 제한이 없습ㄴ지다, 하지만 많을시 일부만 노출됩니다.(더블클릭으로 삭제)';
$lang['web_config_edit_help3']			= '중앙 "상품 추천 모듈"은 최대 4개까지 추가됩니다, 상품수는 8개;  오른쪽은 최대 12개입니다(더블클릭으로 삭제)';
$lang['web_config_edit_html']			= '채널내용 설정';
$lang['web_config_picture_tit']			= '제목 이미지';
$lang['web_config_edit_category']			= '추천 카테고리';
$lang['web_config_category_name']			= '카테고리명';
$lang['web_config_gc_name']			= '서브 카테고리';
$lang['web_config_picture_act']			= '프로모션 이미지';
$lang['web_config_add_recommend']			= '상품 추천 모듈 추가';
$lang['web_config_recommend_max']			= '(최대4개)';
$lang['web_config_goods_order']			= '상품정렬';
$lang['web_config_goods_name']			= '랭킹 상품 이름';
$lang['web_config_goods_price']			= '가격';
$lang['web_config_picture_adv']			= '광고 이미지';
$lang['web_config_brand_list']			= '브랜드 추천';

$lang['web_config_upload_tit']			= '제목이미지 업로드';
$lang['web_config_prompt_tit']			= '해당 채널에 노출할 타이틀을 설정하세요';
$lang['web_config_upload_tit_tips']			= '추천 사이즈: 210X40픽셀인 GIF\JPG\PNG유형 이미지를 업로드하세요';
$lang['web_config_upload_url']			= '이미지링크주소';
$lang['web_config_upload_url_tips']			= '이미지 클릭후 이동할 링크주소 혹은 공백으로 설정할 수 있습니다.';
$lang['web_config_category_title']			= '추천 카테고리 추가';
$lang['web_config_category_note']			= '왼쪽 카테고리를 클릭하여 추천 카테고리에 바로 추가할 수 있습니다.';
$lang['web_config_category_tips']			= '알림: 카테고리명을 더블 클릭하여 삭제할 수 있습니다.';

$lang['web_config_upload_act']			= '프로모션 이미지 업로드';
$lang['web_config_prompt_act']			= '주석 요구로 좌측의 프로모션 이미지를 업로드하세요';
$lang['web_config_upload_type']			= '유형선택';
$lang['web_config_upload_pic']			= '이미지 업로드';
$lang['web_config_upload_adv']			= '광고 불러오기';
$lang['web_config_upload_act_tips']			= '추천 사이즈 212*250픽셀, GIF\JPG\PNG파일 유형, 범위 초과시 부분 이미지만 보여집니다.';
$lang['web_config_upload_act_url']			= '이미지 클릭시 링크 주소를 입력하세요';

$lang['web_config_recommend_goods']			= '추천상품';
$lang['web_config_recommend_title']			= '상품 추천 모듈 제목 이름';
$lang['web_config_recommend_tips']			= '본 모듈 이름 수정은 4-8자로 입력하세요';
$lang['web_config_recommend_goods_tips']			= '알림: 더블클릭하여 삭제 설정을 할 수 있습니다.';
$lang['web_config_recommend_add_goods']			= '노출될 추천 상품을 선택하세요';
$lang['web_config_recommend_gcategory']			= '카테고리 선택';
$lang['web_config_recommend_goods_name']			= '상품이름';

$lang['web_config_goods_order']			= '상품정렬';
$lang['web_config_goods_order_title']			= '상품 정렬 모듈 제목 이름';
$lang['web_config_goods_order_tips']			= '본 모듈 이름 수정은 4-8자로 입력하세요';
$lang['web_config_goods_list']			= '랭킹상품';
$lang['web_config_goods_list_tips']			= '알림: 더블클릭하여 삭제 설정을 할 수 있습니다. 최대 5개 노출할 수 있습니다.';
$lang['web_config_goods_order_add']			= '노출할 랭킹 상품을 선택하세요';
$lang['web_config_goods_order_gcategory']		= '카테고리 선택';
$lang['web_config_goods_order_type']			= '정렬방식';
$lang['web_config_goods_order_sale']			= '판매수';
$lang['web_config_goods_order_click']			= '조회수';
$lang['web_config_goods_order_comment']			= '평가수량';
$lang['web_config_goods_order_collect']			= '찜한수량';
$lang['web_config_goods_order_name']			= '상품이름';

$lang['web_config_brand_title']			= '추천상품';
$lang['web_config_brand_tips']			= '알림: 더블클릭하여 삭제 설정을 할 수 있습니다. 최대 12개 노출할 수 있습니다.';
$lang['web_config_brand_list']			= '후보 추천 브랜드 리스트';

$lang['web_config_upload_adv_tips']			= '주석 요구에 따라 광고 이미지를 업로드하세요';
$lang['web_config_upload_adv_pic']			= '광고 이미지 업로드';
$lang['web_config_upload_pic_tips']			= '추천 사이즈 248*211픽셀, GIF\JPG\PNG파일 유형, 범위 초과시 부분 이미지만 보여집니다.';
$lang['web_config_upload_adv_url']			= '광고 링크';
$lang['web_config_upload_pic_url_tips']		= '이미지 클릭후 이동할 링크';