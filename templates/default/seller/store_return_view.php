<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<?php
  $refund_s_state_text = array(
      1=>"심사대기",
      2=>"동의",
      3=>"거절"
    );
  $refund_r_state_text = array(
      1=>"처리중",
      2=>"관리자심사중",
      3=>"완료"
    );

?>
<div class="ncsc-flow-layout">
  <div class="ncsc-flow-container">
    <div class="title">
      <h3>반품환불 서비스 </h3>
    </div>
    <div id="saleRefundReturn">
      <div class="ncsc-flow-step">
        <dl class="step-first current">
          <dt>구매자 반품 신청 </dt>
          <dd class="bg"></dd>
        </dl>
        <dl class="<?php echo $output['return']['seller_time'] > 0 ? 'current':'';?>">
          <dt>판매자 반품 신청 처리</dt>
          <dd class="bg"> </dd>
        </dl>
        <dl class="<?php echo ($output['return']['ship_time'] > 0 || $output['return']['return_type']==1) ? 'current':'';?>">
          <dt>구매자 상품 반품</dt>
          <dd class="bg"> </dd>
        </dl>
        <dl class="<?php echo $output['return']['admin_time'] > 0 ? 'current':'';?>">
          <dt>상품 인수, 관리자심사</dt>
          <dd class="bg"> </dd>
        </dl>
      </div>
      <div class="ncsc-form-default">
        <h3>구매자 반품환불 신청 </h3>
        <dl>
          <dt>반품환불번호 : </dt>
          <dd><?php echo $output['return']['refund_sn']; ?> </dd>
        </dl>
        <dl>
          <dt>신청인(구매자) : </dt>
          <dd><?php echo $output['return']['buyer_name']; ?></dd>
        </dl>
        <dl>
          <dt>반품원인<?php echo $lang['nc_colon'];?></dt>
          <dd> <?php echo $output['return']['reason_info']; ?> </dd>
        </dl>
        <dl>
          <dt>환불금액 : </dt>
          <dd><?php echo number_format($output['return']['refund_amount_ko']); ?>원</dd>
        </dl>
        <dl>
          <dt>반품수량<?php echo $lang['nc_colon'];?></dt>
          <dd><?php echo $output['return']['return_type']==2 ? $output['return']['goods_num']:'없음'; ?></dd>
        </dl>
        <dl>
          <dt>반품설명：</dt>
          <dd> <?php echo $output['return']['buyer_message']; ?> </dd>
        </dl>
        <dl>
          <dt>증빙서류 업로드 : </dt>
          <dd>
            <?php if (is_array($output['pic_list']) && !empty($output['pic_list'])) { ?>
            <ul class="ncsc-evidence-pic">
              <?php foreach ($output['pic_list'] as $key => $val) { ?>
              <?php if(!empty($val)){ ?>
              <li><a href="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>" cnbiztype="nyroModal" rel="gal" target="_blank"> <img class="show_image" src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$val;?>"></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
            <?php } ?>
          </dd>
        </dl>
        <h3>판매자 처리 의견 </h3>
        <dl>
          <dt>처리상태<?php echo $lang['nc_colon'];?></dt>
          <dd> <?php echo $refund_s_state_text[$output['return']['seller_state']]; ?> </dd>
        </dl>
        <?php if ($output['return']['seller_time'] > 0) { ?>
        <dl>
          <dt>비고<?php echo $lang['nc_colon'];?></dt>
          <dd> <?php echo $output['return']['seller_message']; ?> </dd>
        </dl>
        <?php } ?>
        <?php if ($output['return']['express_id'] > 0 && !empty($output['return']['invoice_no'])) { ?>
        <dl>
          <dt>물류정보<?php echo $lang['nc_colon'];?></dt>
          <dd> <?php echo $output['e_name'].' , '.$output['return']['invoice_no']; ?> </dd>
        </dl>
        <?php } ?>
        <?php if ($output['return']['receive_time'] > 0) { ?>
        <dl>
          <dt>인수 메세지<?php echo $lang['nc_colon'];?></dt>
          <dd> <?php echo $output['return']['receive_message']; ?> </dd>
        </dl>
        <?php } ?>
        <?php if ($output['return']['seller_state'] == 2 && $output['return']['refund_state'] >= 2) { ?>
        <h3>쇼핑몰관리자 심사 처리</h3>
        <dl>
          <dt>관리자 승인<?php echo $lang['nc_colon'];?></dt>
          <dd><?php echo $refund_r_state_text[$output['return']['refund_state']]; ?></dd>
        </dl>
        <?php } ?>
        <?php if ($output['return']['admin_time'] > 0) { ?>
        <dl>
          <dt>관리자 메세지<?php echo $lang['nc_colon'];?></dt>
          <dd> <?php echo $output['return']['admin_message']; ?> </dd>
        </dl>
        <?php } ?>
        <?php if ($output['return']['express_id'] > 0 && !empty($output['return']['invoice_no'])) { ?>
        <ul class="express-log" id="express_list">
          <li class="loading"><?php echo $lang['nc_common_loading'];?></li>
        </ul>
        <?php } ?>
        <div class="bottom">
            <label class=""><a href="javascript:history.go(-1);" class="ncsc-btn"><i class="icon-reply"></i>뒤로가기</a></label>
        </div>
      </div>
    </div>
  </div>
  <?php require template('seller/store_refund_right');?>
</div>


<?php if ($output['return']['express_id'] > 0 && !empty($output['return']['invoice_no'])) { ?>
<script type="text/javascript">
$(function(){
	$.getJSON('index.php?act=store_deliver&op=get_express&e_code=<?php echo $output['e_code'];?>&shipping_code=<?php echo $output['return']['invoice_no'];?>&t=<?php echo random(7);?>',function(data){
		if(data){
			$('#express_list').html('<li>물류정보데이터</li>'+data);
		} else {
			$('#express_list').html('<li>관련 물류 정보 데이터가 없습니다.</li>');
		}
	});
});
</script>
<?php } ?>
