<?php
/**
 * 商家中心抢购管理
 *
 *
 *
 **by J.K*/


defined('InCNBIZ') or exit('Access Invalid!');

class store_hscodeControl extends BaseSellerControl {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 选择活动商品
     **/
    public function search_hscodeOp() {

        $model_article = Model('hscode_article');

        $condition = array();
        if(intval($_GET['hsclass'])>0):
        $condition['article_class_id'] = intval($_GET['hsclass']);
    endif;
        $condition['article_abstract'] = array('like', '%'.$_GET['hscode_name'].'%');

        $article_list = $model_article->getList($condition, 10, 'article_id desc');

        Tpl::output('show_page',$model_article->showpage(10));
        Tpl::output('list',$article_list);
        Tpl::showpage('store_hscode.article', 'null_layout');
    }

    public function hscode_infoOp(){
        $hid = intval($_GET['hid']);

        $data = array();
        $data['result'] = true;

        $model_goods = Model('hscode_article');

        $condition = array();
        $condition['article_id'] = $hid;
        $result = $model_goods->getOne($condition);

        if(empty($result)) {
            $data['result'] = false;
            $data['message'] = L('param_error');
            echo json_encode($data);die;
        }

        $data['article_id'] = $result['article_id'];
        $data['article_title'] = $result['article_title'];
        $data['article_abstract'] = $result['article_abstract'];
        $data['article_content'] = $result['article_content'];

        echo json_encode($data);die;
    }

}
?>