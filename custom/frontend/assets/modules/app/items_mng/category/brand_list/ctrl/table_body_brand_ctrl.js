
(function($, angular, _, APP){

  APP.modules.ctrl.tableBodyBrandCtrl = tableBodyBrandCtrl;

  //tableBodyBrandCtrl
  function tableBodyBrandCtrl($scope, _, $timeout, XHR, PubSub, API_URL_INFO, COMMON_UTIL, sha256){
    var ctrl = this;
    ctrl._filterResults;

    ctrl.getTotal;
    ctrl.getStart;
    ctrl.getCount;
    ctrl.getResults;
    ctrl.brandAuth;
    ctrl.itemAuth;

    ctrl.updateBrand = _updateBrand;

    _init();

    function _init(){
      $scope.$on('$destroy', function () {
      });

      console.log('ctrl.brandAuth: ', ctrl.brandAuth , typeof ctrl.brandAuth);
      console.log('ctrl.itemAuth: ', ctrl.itemAuth , typeof ctrl.itemAuth);
    }

    function _updateBrand(__data){
      //console.log('__data: ' , sha256 );
      //console.log('__data.account_id: ' , sha256.hash(__data.account_id) );
      //console.log('APP.info.user.account_id_hash: ' , APP.info.user.account_id_hash );
      var action = '/'+(APP.info.gLocale)+APP.URL_INFO.menu.ITEMS_MNG_CATEGORY_BRAND_MODIFICATION.link;
      if(APP.info.user.account_type==='admin'){
        COMMON_UTIL.reqForm(action, 'post', _.extend({},__data,{name:encodeURIComponent(__data.name)}) );  //__action, __method, __data
      }else{
        COMMON_UTIL.reqForm(action, 'post', _.extend({},__data,{name:encodeURIComponent(__data.name)}) );  //__action, __method, __data

        /*if( sha256.hash(__data.account_id)===APP.info.user.account_id_hash){
          COMMON_UTIL.reqForm(action, 'post', _.extend({mode:'edit'}, __data));  //__action, __method, __data
        }else{
          var tmpMsg = ('등록한 브랜드만 수정 가능합니다.\n브랜드를 다시 선택해 주세요').replace('&lt;/br&gt;','\n'); //'등록한 브랜드만 수정 가능합니다.\n브랜드를 다시 선택해 주세요'
          alert(tmpMsg);
        }*/
      }
    }

  }

  tableBodyBrandCtrl.$inject = ['$scope', '_', '$timeout', 'XHR', 'PubSub', 'API_URL_INFO', 'COMMON_UTIL', 'sha256'];
  tableBodyBrandCtrl.link = function (PubSub) {
    return function (scope, elem, attrs, ctrl, transclude) {
      PubSub.trigger('tableComponent:bootstrap',{tableBodyCtrl:true});
      APP.modules.directive.util
        .checkBoxHelper(scope, elem, attrs, ctrl, 'tableBodyCtrl.getResults()');
    }
  };
  tableBodyBrandCtrl.bindToController= {
    getResults:'&',
    getTotal:'&',
    getCount:'&',
    getStart:'&',
    brandAuth:'@',
    itemAuth:'@'
  };


})(window.jQuery, window.angular, window._, window.APP);
