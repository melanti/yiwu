<?php
/**
 * 手机短信类
 *
 *
 *
 * @package    library* www.cnbiz.co.kr
 */
defined('InCNBIZ') or exit('Access Invalid!');

class Sms {
    /**
     * 发送手机短信
     * @param unknown $mobile 手机号
     * @param unknown $content 短信内容
     */
    public function send($mobile,$content) {
        return $this->_sendSms($mobile,$content);
    }

    /**
     * 亿美短信发送接口
     * @param unknown $mobile 手机号
     * @param unknown $content 短信内容
     */
    private function _sendSms($mobile,$content) {
        set_time_limit(0);
        define('SCRIPT_ROOT',  BASE_DATA_PATH.'/api/sms/');
        require_once SCRIPT_ROOT.'include/Client.php';
        /**
         * 网关地址
         */
        $gwUrl = C('sms.gwUrl');
        /**
         * 序列号
         */
        $serialNumber = C('sms.serialNumber');
        /**
         * 密码
         */
        $password = C('sms.password');
        /**
         * 登录后所持有的SESSION KEY，即可通过login方法时创建
         */
        $sessionKey = C('sms.sessionKey');
        /**
         * 连接超时时间，单位为秒
         */
        $connectTimeOut = 2;
        /**
         * 远程信息读取超时时间，单位为秒
         */
        $readTimeOut = 10;

        $client = new Client($gwUrl,$serialNumber,$password,$sessionKey,$connectTimeOut,$readTimeOut);

        $statusCode = $client->sendSMS(array($mobile),$content);
        if ($statusCode!=null && $statusCode=="0") {
            return true;
        } else {
            return false;
             print_R($statusCode);
             echo "处理状态码:".$statusCode;
        }
    }
}
