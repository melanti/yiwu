<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['member_index_is_admin']		= '관리자입니다,삭제하실수 없습니다.';
$lang['member_index_manage']		= '회원관리';
$lang['member_index_name']			= '회원아이디';
$lang['member_index_email']			= 'E-MAIL';
$lang['member_index_true_name']		= '회원명';
$lang['member_index_reg_time']		= '등록시간';
$lang['member_index_last_login']	= '마지막등록';
$lang['member_index_login_time']	= '등록횟수';
$lang['member_index_im']			= '온라인 메시지 ';
$lang['member_index_if_admin']		= '관리자여부확인';
$lang['member_index_set_admin']		= '권리자권한 부여';
$lang['member_index_store']			= '미니샵';
$lang['member_index_to_store']		= '미니샵 방문 ';
$lang['member_index_edit_store']		= '미니샵편집';
$lang['member_index_to_message']	= '통지 ';
$lang['member_index_points']		= '포인트 ';
$lang['member_index_inform']		= '상품신고';
$lang['member_index_prestore']		= '현금잔고';
$lang['member_index_available']	    = '사용가능';
$lang['member_index_frozen']		= '동결금액';
$lang['member_index_help1']			= '회원 정보 조회, 회원 자료 편집 등을 하실 수 있습니다.';
$lang['member_index_help2']			= '';
$lang['member_index_null']	    	= '비워두실 수 없습니다 ';
$lang['member_index_state']	    	= '회원상태';
$lang['member_index_inform_deny']	= '신고금지 ';
$lang['member_index_buy_deny']		= '구매금지';
$lang['member_index_talk_deny']		= '발언금지';
$lang['member_index_login_deny']	= '로그인금지';
$lang['member_index_login']			= '로그인';
/**
 * 수정
 */
$lang['member_edit_valid_email']	= '정확한 메일주소를 입력하세요';
$lang['member_edit_back_to_list']	= '돌아가기';
$lang['member_edit_again']			= '본 회원 재편집';
$lang['member_edit_succ']			= '회원 편집 성공';
$lang['member_edit_fail']			= '회원 편집 실패';
$lang['member_edit_password']		= '비밀번호';
$lang['member_edit_password_keep']	= '공백으로 설정시 비밀번호는 변경이 안됩니다.';
$lang['member_edit_password_tip']	= '비밀번호는 6~20자 이내로 설정해주세요';
$lang['member_edit_email_null']		= '메일주소를 입력하세요';
$lang['member_edit_email_exists']	= '이미 등록된 메일주소입니다.';
$lang['member_edit_support']		= '허용양식';
$lang['member_edit_sex']			= '성별';
$lang['member_edit_secret']			= '비밀';
$lang['member_edit_male']			= '남';
$lang['member_edit_female']			= '여';
$lang['member_edit_allow']			= '허용';
$lang['member_edit_deny']			= '금지';
$lang['member_edit_pic']			= '사진';
$lang['member_edit_qq_wrong']       = '정확인 QQ번호를 입력하세요';
$lang['member_edit_wangwang']		= 'WANGWANG';
$lang['member_edit_allowbuy']		= '상품구매 허용';
$lang['member_edit_allowbuy_tip']	= '금지시 본 회원은 상품을 구매할 수 없습니다.';
$lang['member_edit_allowtalk']		= '쪽지쓰기 허용';
$lang['member_edit_allowtalk_tip']	= '금지시 본 회원 쪽지를 보낼 수가 없습니다.';
$lang['member_edit_allowlogin']		= '로그인 허용';
/**
 * 会员추가
 */
$lang['member_add_back_to_list']	= '돌아가기';
$lang['member_add_again']			= '계속추가';
$lang['member_add_succ']			= '회원 추가 성공';
$lang['member_add_fail']			= '회원 추가 실패';
$lang['member_add_name_exists']		= '중복된 회원명이 존재합니다.';
$lang['member_add_name_null']		= '회원명을 입력하세요';
$lang['member_add_name_length']     = '회원명은 3~20자 이내로 입력하세요';