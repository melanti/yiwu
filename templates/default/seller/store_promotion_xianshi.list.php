<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
<?php if ($output['isOwnShop']) { ?>
  <a class="ncsc-btn ncsc-btn-green" href="<?php echo urlShop('store_promotion_xianshi', 'xianshi_add');?>"><i class="icon-plus-sign"></i><?php echo $lang['xianshi_add'];?></a>

<?php } else { ?>

  <?php if(!empty($output['current_xianshi_quota'])) { ?>
  <a class="ncsc-btn ncsc-btn-green" style="right:100px" href="<?php echo urlShop('store_promotion_xianshi', 'xianshi_add');?>"><i class="icon-plus-sign"></i>프로모션 추가</a> <a class="ncsc-btn ncsc-btn-acidblue" href="<?php echo urlShop('store_promotion_xianshi', 'xianshi_quota_add');?>" title=""><i class="icon-money"></i>패키지갱신</a>
  <?php } else { ?>
  <a class="ncsc-btn ncsc-btn-acidblue" href="<?php echo urlShop('store_promotion_xianshi', 'xianshi_quota_add');?>" title=""><i class="icon-money"></i><?php echo $lang['xianshi_quota_add'];?></a>
  <?php } ?>

<?php } ?>
</div>

<?php if ($output['isOwnShop']) { ?>
<div class="alert alert-block mt10">
  <ul>
    <li>1. 이벤트 추가를 클릭하시면 기간한정세일 이벤트를 추가하실 수 있습니다. "관리"를 클릭하셔서 기간한정세일 이벤트 내의 상품을 관리하실 수 있습니다.</li>
    <li>2. "삭제"를 클릭하시면 기간한정세일 이벤트를 삭제하실 수 있습니다.</li>
 </ul>
</div>
<?php } else { ?>
<div class="alert alert-block mt10">
  <?php if(!empty($output['current_xianshi_quota'])) { ?>
  <strong>패키지 유효기간<?php echo $lang['nc_colon'];?></strong><strong style="color:#F00;"><?php echo date('Y-m-d H:i:s', $output['current_xianshi_quota']['end_time']);?></strong>
  <?php } else { ?>
  <strong>가능한 패키지가 없습니다. 패키지를 먼저 구매하여 주십시오.</strong>
  <?php } ?>
  <ul>
    <li>1. 패키지 구매 혹은 패키지 갱신을 클릭하셔서 구매/갱신을 하실 수 있습니다.</li>
    <li>2. 프로모션 추가를 클릭하시면 기간한정세일 이벤트를 추가하실 수 있습니다.</li>
    <li>3. 편집/관리/삭제가 가능합니다.</li>
    <li>4. <strong style="color: red">관련 비용은 정산시 차감됩니다.</strong></li>
 </ul>
</div>
<?php } ?>

<form method="get">
  <table class="search-form">
    <input type="hidden" name="act" value="store_promotion_xianshi" />
    <input type="hidden" name="op" value="xianshi_list" />
    <tr>
      <td>&nbsp;</td>
      <th>상태</th>
      <td class="w100"><select name="state">
          <?php if(is_array($output['xianshi_state_array'])) { ?>
          <?php foreach($output['xianshi_state_array'] as $key=>$val) { ?>
          <option value="<?php echo $key;?>" <?php if(intval($key) === intval($_GET['state'])) echo 'selected';?>><?php echo $val;?></option>
          <?php } ?>
          <?php } ?>
        </select></td>
      <th class="w110">이벤트명</th>
      <td class="w160"><input type="text" class="text w150" name="xianshi_name" value="<?php echo $_GET['xianshi_name_ko'];?>"/></td>
      <td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="검색" /></label></td>
    </tr>
  </table>
</form>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w30"></th>
      <th class="tl">이벤트명</th>
      <th class="w180">시작시간</th>
      <th class="w180">종료시간</th>
      <th class="w80">구매한정</th>
      <th class="w80">상태</th>
      <th class="w150">편집</th>
    </tr>
  </thead>
  <?php if(!empty($output['list']) && is_array($output['list'])){?>
  <?php foreach($output['list'] as $key=>$val){?>
  <tbody id="xianshi_list">
    <tr class="bd-line">
      <td></td>
      <td class="tl"><dl class="goods-name">
          <dt><?php echo $val['xianshi_name_ko'];?></dt>
        </dl></td>
      <td class="goods-time"><?php echo date("Y-m-d H:i",$val['start_time']);?></td>
      <td class="goods-time"><?php echo date("Y-m-d H:i",$val['end_time']);?></td>
      <td><?php echo $val['lower_limit'];?></td>
      <td><?php echo $val['xianshi_state_text'];?></td>
      <td class="nscs-table-handle tr">
          <?php if($val['editable']) { ?>
          <span>
              <a href="index.php?act=store_promotion_xianshi&op=xianshi_edit&xianshi_id=<?php echo $val['xianshi_id'];?>" class="btn-blue">
                  <i class="icon-edit"></i>
                  <p>편집</p>
              </a>
          </span>
          <?php } ?>
          <span>
              <a href="index.php?act=store_promotion_xianshi&op=xianshi_manage&xianshi_id=<?php echo $val['xianshi_id'];?>" class="btn-green">
                  <i class="icon-cog"></i>
                  <p>관리</p>
              </a>
          </span>
          <span>
              <a href="javascript:;" cnbiztype="btn_del_xianshi" data-xianshi-id=<?php echo $val['xianshi_id'];?> class="btn-red">
                  <i class="icon-trash"></i>
                  <p>삭제</p>
              </a>
          </span>
      </td>
  </tr>
  <?php }?>
  <?php }else{?>
  <tr id="xianshi_list_norecord">
      <td class="norecord" colspan="20"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
  </tr>
  <?php }?>
  </tbody>
  <tfoot>
    <?php if(!empty($output['list']) && is_array($output['list'])){?>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
    <?php } ?>
  </tfoot>
</table>
<form id="submit_form" action="" method="post" >
  <input type="hidden" id="xianshi_id" name="xianshi_id" value="">
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('[cnbiztype="btn_del_xianshi"]').on('click', function() {
            if(confirm('정말 삭제 하시겠습니까?')) {
                var action = "<?php echo urlShop('store_promotion_xianshi', 'xianshi_del');?>";
                var xianshi_id = $(this).attr('data-xianshi-id');
                $('#submit_form').attr('action', action);
                $('#xianshi_id').val(xianshi_id);
                ajaxpost('submit_form', '', '', 'onerror');
            }
        });
    });
</script>
