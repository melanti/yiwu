/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */
var lib_env = require("../libs/env.js"),
env = lib_env.get();

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  ftp: {
    host: env.FTP_HOST,
    username: env.FTP_USERNAME,
    password: env.FTP_PASSWORD,
    protocol: env.FTP_PROTOCOL,
    port: env.FTP_PORT,
    escape: env.FTP_ESCAPE,
    retries: env.FTP_RETRIES,
    timeout: env.FTP_TIMEOUT,
    requiresPassword: env.FTP_REQUIRES_PASSWORD,
    autoConfirm: env.FTP_AUTO_CONFIRM,
    cwd: env.FTP_CWD,
    domain: env.FTP_DOMAIN
  },

  url : {
    site : "http://dev-mall.fitnshop.com"
  },

  redis: {
    host: env.REDIS_HOST,
    port: env.REDIS_PORT
  },

  env: {
    mode: "staging",
    host: "stg-api-mall.fitnshop.com"
  },

  auth: {
    key: "fxgear2445!!1234",
    iv: "1234567890123456"
  },

  sqlmapper: {
    dir: "config/sqlmapper"
  },
  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/
  log: {
    level: "info"
  }

};
