<?php defined('InCNBIZ') or exit('Access Invalid!');?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SCM - 판매관리자</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/seller_center.css" rel="stylesheet" type="text/css">
  
    <!-- basic styles -->
    <link href="<?php echo RESOURCE_SITE_URL;?>/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/font-awesome.min.css" />

  <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo SHOP_RESOURCE_SITE_URL;?>/font/font-awesome/css/font-awesome-ie7.min.css">
  <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->


    <!-- ace styles -->

    <link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace.min.css" />
    <link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace-skins.min.css" />

    <!--[if lte IE 8]>
      <link rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/assets/css/ace-ie.min.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->

    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/ace-extra.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/html5shiv.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/respond.min.js"></script>
    <![endif]-->

<script>
var COOKIE_PRE = '<?php echo COOKIE_PRE;?>';var _CHARSET = '<?php echo strtolower(CHARSET);?>';var SITEURL = '<?php echo SHOP_SITE_URL;?>';var RESOURCE_SITE_URL = '<?php echo RESOURCE_SITE_URL;?>';var SHOP_RESOURCE_SITE_URL = '<?php echo SHOP_RESOURCE_SITE_URL;?>';var SHOP_TEMPLATES_URL = '<?php echo SHOP_TEMPLATES_URL;?>';</script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
    
<script type="text/javascript" src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/seller.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/waypoints.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_kr.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/member.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog_ko.js" id="dialog_js" charset="utf-8"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?php echo RESOURCE_SITE_URL;?>/js/html5shiv.js"></script>
      <script src="<?php echo RESOURCE_SITE_URL;?>/js/respond.min.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/IE6_MAXMIX.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/IE6_PNG.js"></script>
<script>
DD_belatedPNG.fix('.pngFix');
</script>
<script>
// <![CDATA[
if((window.navigator.appName.toUpperCase().indexOf("MICROSOFT")>=0)&&(document.execCommand))
try{
document.execCommand("BackgroundImageCache", false, true);
   }
catch(e){}
// ]]>
</script>
<![endif]-->

</head>

<body>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ToolTip.js"></script>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<?php if (!empty($output['store_closed'])) { ?>
<div class="store-closed"><i class="icon-warning-sign"></i>
  <dl>
    <dt>점포는 이미 패점상태입니다.</dt>
    <dd>패점이유：<?php echo $output['store_close_info'];?></dd>
    <dd>잠시 방문할 수 없습니다.；고객센터와 연락하세요.</dd>
  </dl>
</div>
<?php } ?>

<?php
log::record("seller admin account_uuid ". $output['store_info']['account_uuid'], LOG::ERR);
log::record("seller admin account_type ". $output['store_info']['account_type'], LOG::ERR);
log::record("seller admin org_id ". $output['store_info']['org_id'], LOG::ERR);
log::record("seller admin org_name ". $output['store_info']['org_name'], LOG::ERR);
?>

    <!-- Top navigation -->
    <div class="navbar navbar-default" id="navbar">
      <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
      </script>

      <div class="navbar-container container" id="navbar-container">
        <div class="navbar-header pull-left">
          <a href="index.php" class="navbar-brand">
            <small>
              <i class="icon-leaf"></i>
              판매관리자
            </small>
          </a><!-- /.brand -->
        </div><!-- /.navbar-header -->



        <div class="navbar-header pull-right" role="navigation">

          <ul class="nav ace-nav">
            <li class="light-blue">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <img class="nav-user-photo" src="<?php echo getMemberAvatarForID($_SESSION['member_id']);?>" alt="Jason's Photo" />
                <span class="user-info">
                  <small>Hello</small>
                  <?php echo $_SESSION['seller_name'];?>
                </span>

                <i class="icon-caret-down"></i>
              </a>

              <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                <li>
                  <a href="<?php echo urlShop('show_store', 'index', array('store_id'=>$_SESSION['store_id']), $output['store_info']['store_domain']);?>" target="_blank">
                    <i class="icon-home"></i>
                    미니샵 바로가기
                  </a>
                </li>

                <li>
                  <a href="index.php?act=mypage&op=index">
                    <i class="icon-user"></i>
                    마이페이지
                  </a>
                </li>
                <li class="divider"></li>

                <li>
                  <a href="<?php echo urlShop('seller_logout', 'logout');;?>">
                    <i class="icon-off"></i>
                     로그아웃
                  </a>
                </li>
              </ul>
            </li>
          </ul><!-- /.ace-nav -->
        </div><!-- /.navbar-header -->
        </div><!--/.navbar-end-->
      </div>



<div class="main-container container" id="main-container">
  <div class="main-container-inner">

          <!-- End of Top navigation -->
          <!-- Main navigation -->
<?php if(!$output['seller_layout_no_menu']) { ?>


        <a class="menu-toggler" id="menu-toggler" href="#">
          <span class="menu-text"></span>
        </a>


        <div class="sidebar" id="sidebar">
          <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
          </script>

          <div class="sidebar-shortcuts" id="sidebar-shortcuts">

            <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
              <span class="btn btn-success"></span>

              <span class="btn btn-info"></span>

              <span class="btn btn-warning"></span>

              <span class="btn btn-danger"></span>
            </div>
          </div><!-- #sidebar-shortcuts -->



          <ul class="nav nav-list open">

            <li class="<?php echo $output['current_menu']['model'] == 'index'?'open':'';?>">
              <a href="index.php?act=seller_center&op=index">
                      <i class="icon-dashboard"></i>
              홈
              </a>
            </li>
            <?php if(!empty($output['menu']) && is_array($output['menu'])) {?>
            <?php foreach($output['menu'] as $key => $menu_value) {?>
          <?php if($key!="mypage"):?>
            <li class="<?php echo $output['current_menu']['model'] == $key?'open':'';?>">
              <a class="dropdown-toggle" href="index.php?act=<?php echo $menu_value['child'][key($menu_value['child'])]['act'];?>&op=<?php echo $menu_value['child'][key($menu_value['child'])]['op'];?>">
                      <i class="icon-<?php echo $menu_value['icon'];?>"></i>

              <?php echo $menu_value['name'];?>
                      <b class="arrow icon-angle-down"></b>
              </a>

                <ul class="submenu"<?php echo $output['current_menu']['model'] == $key?'style="display:block"':'';?>>
                  <?php if(!empty($menu_value['child']) && is_array($menu_value['child'])) {?>
                  <?php foreach($menu_value['child'] as $submenu_value) {?>
                  <li <?php echo $_GET['act'] == 'seller_center'?"id='quicklink_".$submenu_value['act']."'":'';?>class="<?php echo $submenu_value['act'] == $_GET['act']?'active ':'';?>">
                  <a href="index.php?act=<?php echo $submenu_value['act'];?>&op=<?php echo $submenu_value['op'];?>">
                          <i class="icon-double-angle-right"></i>
                    <?php echo $submenu_value['name'];?>
                    </a>
                  </li>
                  <?php } ?>
                  <?php } ?>
                </ul>
              </li>
            <?php endif; ?>

            <?php } ?>
            <?php } ?>
            <li>
                <a href="index.php?act=show_help&op=index" target="_blank" style="color:#44b50">
                      <i class="icon-question"></i>
              판매자가이드
              </a>
            </li>
            </ul>


            <div class="sidebar-collapse" id="sidebar-collapse">
              <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
            </div>

</div>
<?php }?>





<?php if(!$output['seller_layout_no_menu']) { ?>
<div class="ncsc-layout-right" id="layoutRight">
    <div class="ncsc-path"><i class="icon-desktop"></i>셀러센터<i class="icon-angle-right"></i><?php echo $output['current_menu']['model_name'];?><i class="icon-angle-right"></i><?php echo $output['current_menu']['name'];?></div>
      <?php require_once($tpl_file); ?>
  </div>
<?php } else { ?>
  <?php require_once($tpl_file); ?>
<?php } ?>


          <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
          </script>
        </div><!-- /#ace-settings-container -->

</div>


<script type="text/javascript">
$(document).ready(function(){
    //添加删除快捷操作
    $('[cnbiztype="btn_add_quicklink"]').on('click', function() {
        var $quicklink_item = $(this).parent();
        var item = $(this).attr('data-quicklink-act');
        if($quicklink_item.hasClass('selected')) {
            $.post("<?php echo urlShop('seller_center', 'quicklink_del');?>", { item: item }, function(data) {
                $quicklink_item.removeClass('selected');
                $('#quicklinkq_' + item).remove();
            }, "json");
        } else {
            var count = $('#add-quickmenu').find('dd.selected').length;
            if(count >= 8) {
                showError('최대8개 까지만 가능합니다.');
            } else {
                $.post("<?php echo urlShop('seller_center', 'quicklink_add');?>", { item: item }, function(data) {
                    $quicklink_item.addClass('selected');
                    <?php if ($_GET['act'] == 'seller_center') { ?>
                        var $link = $quicklink_item.find('a');
                        var menu_name = $link.text();
                        var menu_link = $link.attr('href');
                        var menu_item = '<li id="quicklinkq_' + item + '"><a href="' + menu_link + '">' + menu_name + '</a></li>';
                        $(menu_item).appendTo('#seller_center_left_menu').hide().fadeIn();
                    <?php } ?>
                }, "json");
            }
        }
    });
    //浮动导航  waypoints.js
    $("#sidebar,#mainContent").waypoint(function(event, direction) {
        $(this).parent().toggleClass('sticky', direction === "down");
        event.stopPropagation();
        });
    });
    // 搜索商品不能为空
    $('input[cnbiztype="search_submit"]').click(function(){
        if ($('input[cnbiztype="search_text"]').val() == '') {
            return false;
        }
    });
</script>

<div id="tbox">
  <div class="btn" id="msg"><a href="<?php echo urlShop('store_msg', 'index');?>"><i class="msg"><?php if ($output['store_msg_num'] > 0) { ?><em><?php echo $output['store_msg_num'];?></em><?php } ?></i>공지뉴스</a></div>
  <div class="btn" id="im"><i class="im"><em id="new_msg" style="display:none;"></em></i><a href="javascript:void(0);">상담센터</a></div>
  <div class="btn" id="gotop" style="display:none;"><i class="top"></i><a href="javascript:void(0);">위로가기</a></div>
</div>


    <script type="text/javascript">
      if("ontouchend" in document) document.write("<script src='<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
    </script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/typeahead-bs2.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/excanvas.min.js"></script>
    <![endif]-->

    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.easy-pie-chart.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/jquery.sparkline.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/flot/jquery.flot.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/flot/jquery.flot.pie.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/flot/jquery.flot.resize.min.js"></script>

    <!-- ace scripts -->

    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/ace-elements.min.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/assets/js/ace.min.js"></script>
<?php echo getChat($layout);?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.cookie.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/qtip/jquery.qtip.min.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/compare.js"></script>
<script type="text/javascript">
$(function(){
  // Membership card
  $('[cnbiztype="mcard"]').membershipCard({type:'shop'});
});
</script>
</body>
</html>
