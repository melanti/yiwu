<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * index
 */
$lang['goods_class_index_choose_edit']		= '수정할 내용을 선택하세요';
$lang['goods_class_index_in_homepage']		= '메인';
$lang['goods_class_index_display']			= '진열';
$lang['goods_class_index_hide']				= '미진열';
$lang['goods_class_index_succ']				= '성공';
$lang['goods_class_index_choose_in_homepage']	= '메인 내역을 선택하세요';
$lang['goods_class_index_content']				= '의 내용!';
$lang['goods_class_index_class']				= '상품 카테고리';
$lang['goods_class_index_export']				= '불러내기';
$lang['goods_class_index_import']				= '불러오기';
$lang['goods_class_index_tag']					= 'TAG관리';
$lang['goods_class_index_name']					= '카테고리명';
//$lang['goods_class_index_display_in_homepage']	= '메인노출';
$lang['goods_class_index_recommended']			= '추천';
$lang['goods_class_index_ensure_del']			= '본 카테고리 삭제시 서브 카테고리까지 삭제됩니다, 정말 삭제하시겠습니까?';
$lang['goods_class_index_display_tip']			= '메인은 기본 2차 카테고리까지 노출됩니다.';
$lang['goods_class_index_help1']				= '점장이 상품을 추가시 상품 카테고리를 선택할 수 있습니다, 사용자는 해당 카테고리로 검색을 할 수 있습니다.';
$lang['goods_class_index_help2']				= '카테고리 “+”부호를 클릭하여 하위 카테고리를 확인할 수 있습니다.';
$lang['goods_class_index_help3'] 				= '<a>카테고리 설정후 설정 -> 캐쉬비우기를 하셔야 적용됩니다.</a>';
/**
 * 批量수정
 */
$lang['goods_class_batch_edit_succ']			= '편집성공';
$lang['goods_class_batch_edit_wrong_content']	= '편집내용 오류';
$lang['goods_class_batch_edit_batch']	= '동시 편집';
$lang['goods_class_batch_edit_keep']	= '편집유지';
$lang['goods_class_batch_edit_again']	= '해당카테고리 재편집';
$lang['goods_class_batch_edit_ok']	= '카테고리 편집성공';
$lang['goods_class_batch_edit_fail']	= '카테고리 편집실패 ';
$lang['goods_class_batch_edit_paramerror']	= 'error';
$lang['goods_class_batch_order_empty_tip']	= ',비워두시면 수정되지 않습니다.';
/**
 * 카테고리추가
 */
$lang['goods_class_add_name_null']		= '카테고리명을 입력하세요';
$lang['goods_class_add_sort_int']		= '카테고리 순위는 반드시 숫자로 입력하세요';
$lang['goods_class_add_back_to_list']	= '카테고리 리스트 돌아가기';
$lang['goods_class_add_again']			= '계속 카테고리 추가';
$lang['goods_class_add_name_exists']	= '본 카테고리명은 이미 존재합니다, 다른 이름을 사용하세요';
$lang['goods_class_add_sup_class']		= '상위 카테고리';
$lang['goods_class_add_sup_class_notice']	= '상위 카테고리 선택시 해당 서브 카테고리로 지정됩니다.';
$lang['goods_class_add_update_sort']	= '숫자 범위는 0~255이내, 숫자가 작은 순위로 정렬됩니다.';
$lang['goods_class_add_display_tip']	= '카테고리명 노출 여부';
$lang['goods_class_add_type']			= '유형';
$lang['goods_class_null_type']			= '유형없음';
$lang['goods_class_add_type_desc_one']	= '만약 아래 선택사항이 유형이 없을시';
$lang['goods_class_add_type_desc_two']	= '기능중 새로운 유형을 추가하면됩니다.';
$lang['goods_class_edit_prompts_one']	= '"유형"은 상품 발표시 상품 규칙에 추가됩니다, 유형이 없는 상품 카테고리는 규칙을 추가할 수 없습니다.';
$lang['goods_class_edit_prompts_two']	= '기본선택은 "서브 카테고리에 관련"됩니다, 만약 서브 카테고리가 상위 카테고리 유형과 다를시 선택을 취소하여 해당 서브 카테고리에 대한 유형을 선택할 수 있습니다.';
$lang['goods_class_edit_prompts_three']	= '"유형 "수정과 "관련된 카테고리" 체크시 "위반 내림"처리로 진행합니다, 상품을 재 수정후 정상적으로 사용할 수 있습니다.<span style="color:#F30">심중하게</span> 설정하세요.';
$lang['goods_class_edit_related_to_subclass']	= '관련된 카테고리';
/**
 * 카테고리불러오기
 */
$lang['goods_class_import_csv_null']	= '불러올 csv파일은 비어있을 수 없습니다.';
$lang['goods_class_import_data']		= '데이터 불러오기';
$lang['goods_class_import_choose_file']	= '파일을 선택하세요.';
$lang['goods_class_import_file_tip']	= '불러오는 속도가 느리다면 파일을 분할하여 나누어 업로드 하시면 됩니다.';
$lang['goods_class_import_choose_code']	= '파일 코드를 선택해 주세요.';
$lang['goods_class_import_code_tip']	= '파일 용량이 크면 utf-8코드로 변경하신 후 업로드 하시면 됩니다.';
$lang['goods_class_import_file_type']	= '파일형식';
$lang['goods_class_import_first_class']	= '메인 카테고리';
$lang['goods_class_import_second_class']		= 'Second 카테고리';
$lang['goods_class_import_third_class']			= 'Third 카테고리';
$lang['goods_class_import_example_download']	= '샘플파일 다운로드';
$lang['goods_class_import_example_tip']			= '샘플파일 다운 업로드 클릭';
$lang['goods_class_import_import']				= '업로드';
/**
 * 카테고리불러내기
 */
$lang['goods_class_export_data']		= '데이터 불러내기';
$lang['goods_class_export_if_trans']	= '상품 카테고리 더이터 불러내기';
$lang['goods_class_export_trans_tip']	= '';
$lang['goods_class_export_export']		= '불러내기';
$lang['goods_class_export_help1']		= '불러내기 내용은 카테고리정보.csv파일 로 해주십시오.';
/**
 * TAG index
 */
$lang['goods_class_tag_name']			= 'TAG이름';
$lang['goods_class_tag_value']			= 'TAG값';
$lang['goods_class_tag_update']			= 'TAG이름 업데이트';
$lang['goods_class_tag_update_prompt']	= 'TAG이름 업데이트시 많은 시간이 필요합니다, 잠시 기다려주세요.';
$lang['goods_class_tag_reset']			= '불러오기/TAG재설정';
$lang['goods_class_tag_reset_confirm']	= '정말 TAG를 다시 불러오시겠습니까? 다시 불러오면 모든 TAG정보는 초기화됩니다.';
$lang['goods_class_tag_prompts_two']	= 'TAG값은 카테고리 검색의 키워드가 됩니다, 정확하게 TAG값을 입력하세요, 많을 시 ","를 사용하세요.';
$lang['goods_class_tag_prompts_three']	= '불러오기/TAG초기화 기능은 상품 TAG의 초기설정을 하게 됩니다.';
$lang['goods_class_tag_choose_data']	= '설정하실 데이터 항목을 선택하세요';
/**
 * 重置TAG
 */
$lang['goods_class_reset_tag_fail_no_class']	= 'TAG재설정 실패, 해당 카테고리 정보가 없습니다.';
/**
 * 업데이트TAG이름
 */
$lang['goods_class_update_tag_fail_no_class']	= 'TAG명 업데이트 실패, 해당 카테고리 정보가 없습니다.';
/**
 * 삭제TAG
 */
$lang['goods_class_tag_del_confirm']= '정말 상품 카테고리 태그를 삭제하시겠습니까?';