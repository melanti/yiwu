<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="ncsc-oredr-show">
  <div class="ncsc-order-info">
    <div class="ncsc-order-details">
      <div class="title"><?php echo $lang['store_show_order_info'];?></div>
      <div class="content">
        <dl>
          <dt>받는 사람<?php echo $lang['nc_colon'];?></dt>
          <dd><?php echo $output['order_info']['extend_order_common']['reciver_name'];?>&nbsp; <?php echo @$output['order_info']['extend_order_common']['reciver_info']['phone'];?>&nbsp; <?php echo @$output['order_info']['extend_order_common']['reciver_info']['address'];?><?php echo $output['order_info']['extend_order_common']['reciver_info']['dlyp'] ? '[인포메이션]' : '';?></dd>
        </dl>
        <dl>
          <dt><?php echo '구매 메모'.$lang['nc_colon'];?></dt>
          <dd><?php echo $output['order_info']['extend_order_common']['order_message']; ?></dd>
        </dl>
        <dl class="line">
          <dt><?php echo '주문번호'.$lang['nc_colon'];?></dt>
          <dd><?php echo $output['order_info']['order_sn']; ?><a href="javascript:void(0);">더보기<i class="icon-angle-down"></i>
            <div class="more"><span class="arrow"></span>
              <ul>
                <?php if($output['order_info']['payment_name']) { ?>
                <?php
                  switch ($output['order_info']['payment_name']) {
                    case '支付宝':
                      $pay_type = "Alipay";
                      break;
                    
                    case '在线付款':
                      $pay_type = "온라인결제";
                      break;
                    
                    default:
                      $pay_type = $output['order_info']['payment_name'];
                      break;
                  }
                  ?>
                <li><?php echo '결제방식'.$lang['nc_colon'];?><span><?php echo $pay_type; ?>
                  <?php if($output['order_info']['payment_code'] != 'offline' && !in_array($output['order_info']['order_state'],array(ORDER_STATE_CANCEL,ORDER_STATE_NEW))) { ?>
                  (<?php echo '결제번호'.$lang['nc_colon'];?><?php echo $output['order_info']['pay_sn']; ?>)
                  <?php } ?>
                  </span></li>
                <?php } ?>
                <li><?php echo '주문시간'.$lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order_info']['add_time']); ?></span></li>
                <?php if(intval($output['order_info']['payment_time'])) { ?>
                <li><?php echo '결제시간'.$lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order_info']['payment_time']); ?></span></li>
                <?php } ?>
                <?php if($output['order_info']['extend_order_common']['shipping_time']) { ?>
                <li><?php echo '발송시간'.$lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order_info']['extend_order_common']['shipping_time']); ?></span></li>
                <?php } ?>
                <?php if(intval($output['order_info']['finnshed_time'])) { ?>
                <li><?php echo '완료시간'.$lang['nc_colon'];?><span><?php echo date("Y-m-d H:i:s",$output['order_info']['finnshed_time']); ?></span></li>
                <?php } ?>
              </ul>
            </div>
            </a></dd>
        </dl>
        <dl>
          <dt></dt>
          <dd></dd>
        </dl>
      </div>
    </div>
    <?php if ($output['order_info']['order_state'] == ORDER_STATE_CANCEL) { ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-off orange"></i>주문현황：</dt>
        <dd>취소완료</dd>
      </dl>
      <ul>
        <li>업체에서 <?php echo date('Y-m-d H:i:s',$output['order_info']['close_info']['log_time']);?><?php echo $output['order_info']['close_info']['log_msg'];?></li>
      </ul>
    </div>
    <?php } ?>
    <?php if ($output['order_info']['order_state'] == ORDER_STATE_NEW) { ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>주문이 완료되었습니다. 구매자 입금대기 상태입니다.</dd>
      </dl>
      <ul>
        <li>1. 구매자가 아직 결제를 하지 않았습니다.</li>
        <li>2. 만약 구매자가 해당 주문의 결제를 완료하지 않으면, 
          <time><?php echo date('Y-m-d H:i:s',$output['order_info']['order_cancel_day']);?> 에</time>
          자동으로 주문이 취소됩니다.</li>
      </ul>
    </div>
    <?php } ?>
    <?php if ($output['order_info']['order_state'] == ORDER_STATE_PAY) { ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>
          <?php if ($output['order_info']['payment_code'] == 'offline') { ?>
          주문이 완료되었습니다. 발송을 기다려 주십시오.
          <?php } else { ?>
          결제성공!
          <?php } ?>
        </dd>
      </dl>
      <ul>
        <?php if ($output['order_info']['payment_code'] == 'offline') { ?>
        <li>1. 구매자가 결제수단을 선택하여 주문을 완료하였습니다.</li>
        <li>2. 주문이 이미 완료되었습니다. 판매자가 상품준비 및 발송 준비중입니다.</li>
        <?php } else { ?>
        <li>1. 구매자 사용“<?php echo orderPaymentName($output['order_info']['payment_code']);?>”지불수단이 등록되었습니다. 지불코드를 입력하여 주십시오. “<?php echo $output['order_info']['pay_sn'];?>”。</li>
        <li>2. 주문이 완료되었습니다. 판매자가 상품준비 및 발송준비 중입니다.</li>
        <?php } ?>
      </ul>
    </div>
    <?php } ?>
    <?php if ($output['order_info']['order_state'] == ORDER_STATE_SEND) { ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>이미발송</dd>
      </dl>
      <ul>
        <li>1. 상품출고；
          <?php if ($output['order_info']['shipping_code'] != '') { ?>
          물류회사：<?php echo $output['order_info']['express_info']['e_name']?>；송장번호：<?php echo $output['order_info']['shipping_code'];?>。
          <?php if ($output['order_info']['if_deliver']) { ?>
          찾아보기 <a href="#order-step" class="blue">“배송추적”</a> 상황
          <?php } ?>
          <?php } else { ?>
          배송필요없음
          <?php } ?>
        </li>
        <li>2. 구매자가 수취 확인을 하지 않았을 시, 
          <time><?php echo date('Y-m-d H:i:s',$output['order_info']['order_confirm_day']);?></time>
          에 자동으로 수취확인 상태로 바뀌게 됩니다.</li>
      </ul>
    </div>
    <?php } ?>
    <?php if ($output['order_info']['order_state'] == ORDER_STATE_SUCCESS) { ?>
    <?php if ($output['order_info']['evaluation_state'] == 1) { ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>평가완료</dd>
      </dl>
      <ul>
        <li>1. 구매자가 이미 상품과 거래 평가를 완료하였습니다.</li>
        <li>2. <a href="index.php?act=store_evaluate&op=list" class="ncsc-btn-mini">평가관리</a>상세내용보기</li>
      </ul>
    </div>
    <?php } else { ?>
    <div class="ncsc-order-condition">
      <dl>
        <dt><i class="icon-ok-circle green"></i>주문현황：</dt>
        <dd>발송완료</dd>
      </dl>
      <ul>
        <li>1. 거래가 완료되었습니다. 구매자는 상품과 서비스에 대한 평가를 진행할 수 있습니다.</li>
        <li>2. 구매자 평가는 해당 상품의 상세페이지에 나타나게 됩니다.</li>
      </ul>
    </div>
    <?php } ?>
    <?php } ?>
  </div>
  <?php if ($output['order_info']['order_state'] != ORDER_STATE_CANCEL) { ?>
  <div id="order-step" class="ncsc-order-step">
    <dl class="step-first <?php if ($output['order_info']['order_state'] != ORDER_STATE_CANCEL) echo 'current';?>">
      <dt>주문등록</dt>
      <dd class="bg"></dd>
      <dd class="date" title="<?php echo $lang['store_order_add_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['add_time']); ?></dd>
    </dl>
    <?php if ($output['order_info']['payment_code'] != 'offline') { ?>
    <dl class="<?php if(intval($output['order_info']['payment_time'])) echo 'current'; ?>">
      <dt>주문결제</dt>
      <dd class="bg"> </dd>
      <dd class="date" title="<?php echo $lang['store_show_order_pay_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['payment_time']); ?></dd>
    </dl>
    <?php } ?>
    <dl class="<?php if($output['order_info']['extend_order_common']['shipping_time']) echo 'current'; ?>">
      <dt>판매자 발송완료</dt>
      <dd class="bg"> </dd>
      <dd class="date" title="<?php echo $lang['store_show_order_send_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['extend_order_common']['shipping_time']); ?></dd>
    </dl>
    <dl class="<?php if(intval($output['order_info']['finnshed_time'])) { ?>current<?php } ?>">
      <dt>수취확인</dt>
      <dd class="bg"> </dd>
      <dd class="date" title="<?php echo $lang['store_show_order_finish_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['finnshed_time']); ?></dd>
    </dl>
    <dl class="<?php if($output['order_info']['evaluation_state'] == 1) { ?>current<?php } ?>">
      <dt>평가</dt>
      <dd class="bg"></dd>
      <dd class="date" title="<?php echo $lang['store_show_order_finish_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['extend_order_common']['evaluation_time']); ?></dd>
    </dl>
  </div>
  <?php } ?>
  <div class="ncsc-order-contnet">
    <table class="ncsc-default-table order">
      <thead>
        <tr>
          <th class="w10">&nbsp;</th>
          <th colspan="2">상품명</th>
          <th class="w120">단가(원)</th>
          <th class="w60">주문금액</th>
          <th class="w100">이벤트</th>
          <th class=""><strong>실제지불 * 수수료비율 = 기본수수료(원)</strong></th>
          <th>거래</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($output['order_info']['shipping_code'])) { ?>
        <tr>
          <th colspan="6" style="border-right: 0;"> <div class="order-deliver"> <span>물류회사： <a target="_blank" href="<?php echo $output['order_info']['express_info']['e_url'];?>"><?php echo $output['order_info']['express_info']['e_name'];?></a></span> <span><?php echo '송장번호'.$lang['nc_colon'];?> <?php echo $output['order_info']['shipping_code']; ?></span><span><a href="javascript:void(0);" id="show_shipping">배송추적<i class="icon-angle-down"></i>
              <div class="more"><span class="arrow"></span>
                <ul id="shipping_ul">
                  <li>다운로드중</li>
                </ul>
              </div>
              </a></span> </div></th>
          <th colspan="3" style=" border-left: 0;"><?php if(!empty($output['daddress_info'])) { ?>
            <dl class="daddress-info">
              <dt>발&nbsp;&nbsp;송&nbsp;&nbsp;인：</dt>
              <dd><?php echo $output['daddress_info']['seller_name']; ?><a href="javascript:void(0);">더보기<i class="icon-angle-down"></i>
                <div class="more"><span class="arrow"></span>
                  <ul>
                    <li>회&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp사：<span><?php echo $output['daddress_info']['company'];?></span></li>
                    <li>연락처：<span><?php echo $output['daddress_info']['telphone'];?></span></li>
                    <li>발송지：<span><?php echo $output['daddress_info']['area_info'];?>&nbsp;<?php echo $output['daddress_info']['address'];?></span></li>
                  </ul>
                </div>
                </a></dd>
            </dl>
            <?php } ?>
          </th>
        </tr>
        <?php } ?>
        <?php $i = 0;?>
        <?php foreach($output['order_info']['goods_list'] as $k => $goods) { ?>
        <?php $i++;?>
        <tr class="bd-line">
          <td>&nbsp;</td>
          <td class="w50"><div class="pic-thumb"><a target="_blank" href="<?php echo $goods['goods_url'];?>"><img src="<?php echo $goods['image_60_url']; ?>" /></a></div></td>
          <td class="tl"><dl class="goods-name">
              <dt><a target="_blank" href="<?php echo $goods['goods_url']; ?>"><?php echo $goods['goods_name']; ?></a></dt>
              <dd>
                <?php if (is_array($output['refund_all']) && !empty($output['refund_all'])) {?>
                환불번호：<a target="_blank" href="index.php?act=store_refund&op=view&refund_id=<?php echo $output['refund_all']['refund_id'];?>"><?php echo $output['refund_all']['refund_sn'];?></a>
                <?php }else if($goods['extend_refund']['refund_type'] == 1) {?>
                환불번호：<a target="_blank" href="index.php?act=store_refund&op=view&refund_id=<?php echo $goods['extend_refund']['refund_id'];?>"><?php echo $goods['extend_refund']['refund_sn'];?></a></dd>
              <?php }else if($goods['extend_refund']['refund_type'] == 2) {?>
              반품번호：<a target="_blank" href="index.php?act=store_return&op=view&return_id=<?php echo $goods['extend_refund']['refund_id'];?>"><?php echo $goods['extend_refund']['refund_sn'];?></a>
              </dd>
              <?php } ?>
            </dl></td>
          <td><?php echo number_format($goods['goods_price']); ?>원
            <p class="green">
              <?php if (is_array($output['refund_all']) && !empty($output['refund_all']) && $output['refund_all']['admin_time'] > 0) {?>
              <?php echo $goods['goods_pay_price'];?><span>환불</span>
              <?php } elseif ($goods['extend_refund']['admin_time'] > 0) { ?>
              <?php echo $goods['extend_refund']['refund_amount'];?><span>환불</span>
              <?php } ?>
            </p></td>
          <td><?php echo $goods['goods_num']; ?></td>
          <td><?php echo $goods['goods_type_cn']; ?></td>
          <td class="commis bdl bdr">
          <?php if ($goods['commis_rate'] != 200) { ?>
          <?php echo number_format($goods['goods_pay_price']*C('site_cur')); ?> * <?php echo $goods['commis_rate']; ?>% = <b><?php echo ncPriceFormat(number_format($goods['goods_pay_price']*C('site_cur'))*$goods['commis_rate']/100); ?></b>
          <?php } ?>
          </td>

          <!-- S 合并TD -->
          <?php if (($output['order_info']['goods_count'] > 1 && $k ==0) || ($output['order_info']['goods_count'] == 1)){?>
                 <?php
                 if (strstr($output['order_info']['state_desc'],'待付款')) {
                      $order_type = "결제대기중";
                 } else if (strstr($output['order_info']['state_desc'],'待收货')) {
                      $order_type = "수취확인중";
                 } else if (strstr($output['order_info']['state_desc'],'已完成')) {
                      $order_type = "수취확인";
                 } else if (strstr($output['order_info']['state_desc'],'交易完成')) {
                      $order_type = "거래완료";
                 } else {
                      $order_type = "취소됨";
                 }

                  ?>
          <td class="bdl bdr" rowspan="<?php echo $output['order_info']['goods_count'];?>"><?php echo $order_type; ?>
            <?php if ($output['order_info']['if_lock']) { ?>
            <p>환불반품중</p>
            <?php } ?>

            <!-- 가격변경 -->
            <?php if ($output['order_info']['if_modify_price']) { ?>
            <p><a href="javascript:void(0)" class="ncsc-btn" uri="index.php?act=store_order&op=change_state&state_type=modify_price&order_sn=<?php echo $output['order_info']['order_sn']; ?>&order_id=<?php echo $output['order_info']['order_id']; ?>" dialog_width="480" dialog_title="<?php echo $lang['store_order_modify_price'];?>" nc_type="dialog"  dialog_id="seller_order_adjust_fee" id="order<?php echo $output['order_info']['order_id']; ?>_action_adjust_fee" />배송료수정</a></p>
            <?php }?>

            <!-- 取消订单 -->
            <?php if ($output['order_info']['if_cancel']) { ?>
            <p><a href="javascript:void(0)" style="color:#ed008c; text-decoration:underline;" nc_type="dialog" uri="index.php?act=store_order&op=change_state&state_type=order_cancel&order_sn=<?php echo $output['order_info']['order_sn']; ?>&order_id=<?php echo $output['order_info']['order_id']; ?>" dialog_title="<?php echo $lang['store_order_cancel_order'];?>" dialog_id="seller_order_cancel_order" dialog_width="400" id="order<?php echo $output['order_info']['order_id']; ?>_action_cancel" />주문취소</a></p>
            <?php } ?>

            <!-- 发货 -->
            <?php if ($output['order_info']['if_send']) { ?>
            <p><a class="ncsc-btn" href="index.php?act=store_deliver&op=send&order_id=<?php echo $output['order_info']['order_id']; ?>"/><i class="icon-truck"></i>배송하기</a></p>
            <?php } ?></td>
          <?php } ?>
          <!-- E 合并TD -->
        </tr>

        <!-- S 赠品列表 -->
        <?php if (!empty($output['order_info']['zengpin_list']) && $i == count($output['order_info']['goods_list'])) { ?>
        <tr class="bd-line">
          <td>&nbsp;</td>
          <td colspan="6" class="tl"><div class="ncsc-goods-gift">사은품：
          <ul><?php foreach($output['order_info']['zengpin_list'] as $zengpin_info) {?>
          <li><a title="사은품：<?php echo $zengpin_info['goods_name'];?> * <?php echo $zengpin_info['goods_num'];?>" target="_blank" href="<?php echo $zengpin_info['goods_url'];?>"><img src="<?php echo $zengpin_info['image_60_url']; ?>" /></a></li>
          <?php } ?></ul></div>
          </td>
        </tr>
        <?php } ?>
        <!-- E 赠品列表 -->

        <?php } ?>
      </tbody>
      <tfoot>
        <?php if(!empty($output['order_info']['extend_order_common']['promotion_info']) || !empty($output['order_info']['extend_order_common']['voucher_code'])){ ?>
        <tr>
          <td colspan="20"><dl class="ncsc-store-sales">
              <dt>기타정보<?php echo $lang['nc_colon'];?></dt>
              <?php if(!empty($output['order_info']['extend_order_common']['promotion_info'])){ ?>
              <dd><?php echo $output['order_info']['extend_order_common']['promotion_info'];?></dd>
              <?php } ?>
              <?php if(!empty($output['order_info']['extend_order_common']['voucher_code'])){ ?>
              <dd>사용금액<?php echo $output['order_info']['extend_order_common']['voucher_price'];?> 원의 쿠폰，코드：<?php echo $output['order_info']['extend_order_common']['voucher_code'];?></span></dd>
              <?php } ?>
            </dl></td>
        </tr>
        <?php } ?>
        <tr>
          <td colspan="20"><dl class="freight">
              <dd>
                <?php if(!empty($output['order_info']['shipping_fee']) && $output['order_info']['shipping_fee'] != '0.00'){ ?>
                배송료: <span><?php echo number_format($output['order_info']['shipping_fee_ko']); ?>원</span>
                <?php }else{?>
                (배송료)
                <?php }?>
                <?php if($output['order_info']['refund_amount'] > 0) { ?>
                (환불:<?php echo $lang['currency'].$output['order_info']['refund_amount'];?>)
                <?php } ?>
              </dd>
            </dl>
            <dl class="sum">
              <dt><?php echo'주문금액'.$lang['nc_colon'];?></dt>
              <dd><em><?php echo number_format($output['order_info']['order_amount_ko']); ?></em>원</dd>
            </dl></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
<script type="text/javascript">
$(function(){
    $('#show_shipping').on('hover',function(){
        var_send = '<?php echo date("Y-m-d H:i:s",$output['order_info']['extend_order_common']['shipping_time']); ?>&nbsp;&nbsp;상품 발송<br/>';
    	$.getJSON('index.php?act=store_deliver&op=get_express&e_code=<?php echo $output['order_info']['express_info']['e_code']?>&shipping_code=<?php echo $output['order_info']['shipping_code']?>&t=<?php echo random(7);?>',function(data){
    		if(data){
    			data = var_send+data;
    			$('#shipping_ul').html(data);
    			$('#show_shipping').unbind('hover');
    		}else{
    			$('#shipping_ul').html(var_send);
    			$('#show_shipping').unbind('hover');
    		}
    	});
    });
});
</script>
