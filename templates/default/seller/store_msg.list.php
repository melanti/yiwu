<?php defined('InCNBIZ') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<div class="alert alert-block mt10"> <strong>TIP：</strong>
  <ul class="mt5">
    <li>1. 관리자는 모든 메시지를 확인 가능 합니다.</li>
    <li>2. 관리자만 메시지 삭제 가능합니다.삭제후 기타 아이디의 메시지도 같이 삭제 됩니다.</li>
  </ul>
</div>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w30"></th>
      <th>메시지 내용</th>
      <th class="w200">발송시간</th>
      <th class="w70">설정</th>
    </tr>
    <tr>
      <td class="tc"><input id="all" class="checkall" type="checkbox" /></td>
      <td colspan="20"><label for="all">전부선택</label>
        <a href="javascript:void(0);" nc_type="batchbutton" uri="<?php echo urlShop('store_msg', 'mark_as_read');?>" name="smids" class="ncsc-btn-mini"><i class="icon-flag"></i>읽음 상태로 표시</a>
        <?php if ($_SESSION['seller_is_admin']) {?>
        <a href="javascript:void(0);" nc_type="batchbutton" uri="<?php echo urlShop('store_msg', 'del_msg')?>" name="smids" class="ncsc-btn-mini"><i class="icon-trash"></i>삭제</a>
        <?php }?>
      </td>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($output['msg_list'])) { ?>
    <?php foreach($output['msg_list'] as $val) { ?>
    <tr class="bd-line">
      <td class="tc"><input class="checkitem" type="checkbox" value="<?php echo $val['sm_id'];?>" /></td>
      <td class="tl <?php if (empty($val['sm_readids']) || !in_array($_SESSION['seller_id'], $val['sm_readids'])) {?>fb dark<?php }?>"><?php echo $val['sm_content']?></td>
      <td><?php echo date('Y-m-d H:i:s', $val['sm_addtime']);?></td>
      <td class="nscs-table-handle"><span><a href="javascript:void(0);" class="btn-blue" dialog_id="store_msg_info" dialog_title="시스템 소식" dialog_width="480" nc_type="dialog" uri="<?php echo urlShop('store_msg', 'msg_info', array('sm_id' => $val['sm_id']));?>"><i class="icon-eye-open"></i>
        <p>보기</p>
        </a></span></td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>내용이 없습니다.</span></div></td>
    </tr>
    <?php } ?>
  </tbody>
  <tfoot>
    <?php if (!empty($output['msg_list'])) { ?>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
    <?php } ?>
  </tfoot>
</table>
<script>
$(function(){
    $('a[nc_type="dialog"]').click(function(){
        $(this).parents('tr:first').children('.tl').removeClass('fb dark');
    });
});
</script>