<?php
defined('InCNBIZ') or exit('Access Invalid!');
/**
 * 欢迎페이지
 */
$lang['dashboard_wel_welcome']		= '안녕하세요';
$lang['dashboard_wel_lase_login']	= '마지막 로그인 시간';
$lang['dashboard_wel_system_info']	= '시스템관리';
$lang['dashboard_wel_member_des']	= '신규회원/현금잔고';
$lang['dashboard_wel_store_des']	= '신규미니샵 심사';
$lang['dashboard_wel_goods_des']	= '신규상품/브랜드신청 심사';
$lang['dashboard_wel_trade_des']	= '거래주문 및 신고';
$lang['dashboard_wel_stat_des']		= '시스템 운영 설정 심사';
$lang['dashboard_wel_new_add']		= '신규상품';
$lang['dashboard_wel_predeposit_get'] = '현금잔고인출';
$lang['dashboard_wel_offline_pay']	= '오프라인 충전';
$lang['dashboard_wel_check']		= '심사';
$lang['dashboard_wel_expire']		= '만기예정';
$lang['dashboard_wel_expired']		= '만기';
$lang['dashboard_wel_outh']			= '인증';
$lang['dashboard_wel_update']		= '업데이트';
$lang['dashboard_wel_brnad_applay']	= '브랜드 신청';
$lang['dashboard_wel_inform']		= '신고';
$lang['dashboard_wel_complain']		= '컴플레인';
$lang['dashboard_wel_complain_handle']	= '중재대기';
$lang['dashboard_wel_buy_gold']		= '금화구매';
$lang['dashboard_wel_ztc']			= '직통차';
$lang['dashboard_wel_groupbuy']		= '공동구매';
$lang['dashboard_wel_point_order']	= '주문포인트';
$lang['dashboard_wel_check_billno']	= '명세서 심사';
$lang['dashboard_wel_pay_billno']	= '명세서 결제';
$lang['dashboard_wel_total_member']	= '총 회원수';
$lang['dashboard_wel_total_store']	= '총 미니샵수';
$lang['dashboard_wel_total_apply']	= '총 신청수';
$lang['dashboard_wel_total_goods']	= '총 상품수';
$lang['dashboard_wel_total_order']	= '총 주문수';
$lang['dashboard_wel_sys_info']		= '시스템 정보';
$lang['dashboard_wel_server_os']	= '서버OS';
$lang['dashboard_wel_server']		= '서버';
$lang['dashboard_wel_version']		= '버전';
$lang['dashboard_wel_install_date']	= '설치시간';
$lang['dashboard_wel_count_store_add']	= '신규 미니샵수';
//$lang['dashboard_wel_count_store_applay']	= '신청수';
//$lang['dashboard_wel_total_price'] = '주문 총 금액';


/**
 * 关于我们
 */
$lang['dashboard_aboutus_idea']			= '경영이념';
$lang['dashboard_aboutus_idea_content']	= '열심히 합니다.';
$lang['dashboard_aboutus_team']			= '팀구성';
$lang['dashboard_aboutus_manager']		= 'PM';
$lang['dashboard_aboutus_manager_name']		= '<a href="http://t.qq.com/youyihanguo" target="_blank" class="tqq">J.K</a>';
$lang['dashboard_aboutus_developer']	= '개발팀';
$lang['dashboard_aboutus_developer_name']	= 'J.K등등';
$lang['dashboard_aboutus_designer']		= '디자이너';
$lang['dashboard_aboutus_designer_name']	= 'M.L';
$lang['dashboard_aboutus_near_us']		= '상세정보';
$lang['dashboard_aboutus_bbs']			= '공식사이트';
$lang['dashboard_aboutus_bbs_tip']		= 'BBS TIP';
$lang['dashboard_aboutus_website']		= '공식사이트';
$lang['dashboard_aboutus_website_tip']	= '공식사이트TIP';
$lang['dashboard_aboutus_thanks']		= 'Thanks';
$lang['dashboard_aboutus_thanks_content']	= '감사합니다.';
$lang['dashboard_aboutus_thanks_developer_name'] = '개발자이름';
$lang['dashboard_aboutus_notice']		= 'Notice';
$lang['dashboard_aboutus_notice1']		= '한국정품가';
$lang['dashboard_aboutus_notice2']		= '소유';
$lang['dashboard_aboutus_notice3']		= '의 저작권';
$lang['dashboard_aboutus_notice4']		= '개발된 프로젝트';
$lang['dashboard_aboutus_notice5']		= '등';
$lang['dashboard_aboutus_notice6']		= '원 저작권 소유';

