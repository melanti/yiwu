<?php
defined('InCNBIZ') or exit('Access Invalid!');
$lang['voucher_unavailable']    = '쿠폰 기능이 닫힌 상태입니다.';
$lang['voucher_applystate_new']    = '심사대기';
$lang['voucher_applystate_verify']    = '심사완료';
$lang['voucher_applystate_cancel']    = '심사취소';
$lang['voucher_quotastate_activity']	= '정상';
$lang['voucher_quotastate_cancel']    = '취소';
$lang['voucher_quotastate_expire']    = '마감';
$lang['voucher_templatestate_usable']	= '유효';
$lang['voucher_templatestate_disabled']= '무효';
$lang['voucher_quotalist']= '패키지 리스트';
$lang['voucher_applyquota']= '패키지 신청';
$lang['voucher_applyadd']= '구매';
$lang['voucher_templateadd']= '쿠폰추가';
$lang['voucher_templateedit']= '쿠폰수정';
$lang['voucher_templateinfo']= '쿠폰상세';
/**
 * 套餐申请
 */
$lang['voucher_apply_num_error']= '1~12이내의 숫자를 입력하세요';
$lang['voucher_apply_goldnotenough']= "当前您拥有金币数为%s，不足以支付此次交易，请先充值";
$lang['voucher_apply_fail']= '套餐申请失败';
$lang['voucher_apply_succ']= '套餐申请成功，请等待审核';
$lang['voucher_apply_date']= '申请日期';
$lang['voucher_apply_num']    		= '申请数量';
$lang['voucher_apply_addnum']    		= '套餐购买数量';
$lang['voucher_apply_add_tip1']    		= '购买单位为月(30天)，一次最多购买12个月，您可以在所购买周期内以月为单位发布代金券活动';
$lang['voucher_apply_add_tip2']    		= '%s원/월';
$lang['voucher_apply_add_tip3']    		= ' 최대 등록: %s번/월';
$lang['voucher_apply_add_tip4']    		= '套餐时间从审批后开始计算';
$lang['voucher_apply_add_confirm1']    	= '구매하시겠습니까? 총 지불금액: ';
$lang['voucher_apply_add_confirm2']    	= '원';
$lang['voucher_apply_goldlog']    		= '购买代金券活动%s个月，单价%s金币，总共花费%s金币';
$lang['voucher_apply_buy_succ']			= '구매완료';

/**
 * 套餐
 */
$lang['voucher_quota_startdate']    	= '开始时间';
$lang['voucher_quota_enddate']    		= '结束时间';
$lang['voucher_quota_timeslimit']    	= '活动次数限制';
$lang['voucher_quota_publishedtimes']   = '已发布活动次数';
$lang['voucher_quota_residuetimes']    	= '剩余活动次数';
/**
 * 代金券模板
 */
$lang['voucher_template_quotanull']			= '사용 할 패키지가 없습니다.';
$lang['voucher_template_noresidual']		= "본 패키지에서 최대 %s개 한도를 초과하였습니다.";
$lang['voucher_template_pricelisterror']	= '플랫폼 쿠폰 금액 설정 오류, 관리자에게 문의하세요.';
$lang['voucher_template_title_error'] 		= "반드시 50자 이내로 입력하세요.";
$lang['voucher_template_total_error'] 		= "발표하실 쿠폰 수량을 입력하세요.";
$lang['voucher_template_price_error']		= "구매가격은 반드시 숫자로 입력하세요, 또한 제한 금액을 초과할 수 없습니다.";
$lang['voucher_template_limit_error'] 		= "구매가격을 입력하세요.";
$lang['voucher_template_describe_error'] 	= "반드시 255자 이내로 입력하세요.";
$lang['voucher_template_title']			= '쿠폰명';
$lang['voucher_template_enddate']		= '유효기간';
$lang['voucher_template_enddate_tip']		= '有效期应在套餐有效期内，正使用的套餐有效期为';
$lang['voucher_template_price']			= '금액';
$lang['voucher_template_total']			= '쿠폰수';
$lang['voucher_template_eachlimit']		= '매인제한';
$lang['voucher_template_eachlimit_item']= '무제한';
$lang['voucher_template_eachlimit_unit']= '장';
$lang['voucher_template_orderpricelimit']	= '소비금액';
$lang['voucher_template_describe']		= '쿠폰설명';
$lang['voucher_template_styleimg']		= '选择代金券皮肤';
$lang['voucher_template_styleimg_text']	= '店铺优惠券';
$lang['voucher_template_image']			= '쿠폰이미지';
$lang['voucher_template_image_tip']		= '该图片将在积分中心的代金券模块中显示，建议尺寸为160*160px。';
$lang['voucher_template_list_tip1'] = "1、手工设置代金券失效后,用户将不能领取该代金券,但是已经领取的代金券仍然可以使用";
$lang['voucher_template_list_tip2'] = "2、代金券模版和已发放的代金券过期后自动失效";
$lang['voucher_template_backlist'] 	= "返回列表";
$lang['voucher_template_giveoutnum']= '획득수';
$lang['voucher_template_usednum']	= '사용수';
/**
 * 代金券
 */
$lang['voucher_voucher_state'] = "状态";
$lang['voucher_voucher_state_unused'] = "未使用";
$lang['voucher_voucher_state_used'] = "已使用";
$lang['voucher_voucher_state_expire'] = "已过期";
$lang['voucher_voucher_price'] = "金额";
$lang['voucher_voucher_storename'] = "适用店铺";
$lang['voucher_voucher_indate'] = "有效期";
$lang['voucher_voucher_usecondition'] = "使用条件";
$lang['voucher_voucher_usecondition_desc'] = "订单满";
$lang['voucher_voucher_vieworder'] = "查看订单";
$lang['voucher_voucher_readytouse'] = "马上使用";
$lang['voucher_voucher_code'] = "编码";
?>
